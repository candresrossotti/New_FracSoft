﻿Imports System.Threading
Public Class ErrorInfo

    Private Shared ErrorDictionary_English As New Dictionary(Of IEnumerable(Of Integer), String)() From {}

    Private Shared ErrorDictionary_Español As New Dictionary(Of IEnumerable(Of Integer), String)() From {
        {{516428, 31}, "Sensores de presión de aire - Error de plausibilidad"},
        {{108, 2}, "Sensor de presión ambiental - Error de plausibilidad"},
        {{108, 31}, "Sensor de presión ambiental - Tensión de alimentación fuera del alcance"},
        {{1176, 1}, "Aumentar la presión - Presión demasiado alta"},
        {{1176, 0}, "Aumentar la presión - Presión demasiado baja"},
        {{973, 2}, "Frenos - Controlador de sistema - Error de comunicación"},
        {{636, 2}, "Posición del árbol de levas - Error de plausibilidad"},
        {{636, 14}, "Sensor de posición del árbol de levas - Error de tierra"},
        {{636, 5}, "Sensor de posición de árbol de levas - Circuito abierto"},
        {{636, 3}, "Sensor de posición del árbol de levas - Cortocircuito a la batería o circuito abierto"},
        {{636, 4}, "Sensor de posición de árbol de levas - Cortocircuito a tierra"},
        {{636, 31}, "Sensor de posición del árbol de levas - Tensión de alimentación fuera del alcance"},
        {{636, 8}, "Voltaje del sensor de velocidad del árbol de levas - Error de verosimilitud"},
        {{157, 0}, "Common Rail - Sobrepresión crítica"},
        {{1239, 2}, "Common Rail - Leakage"},
        {{157, 15}, "Common Rail - Advertencia de sobrepresión"},
        {{516117, 11}, "Common Rail - PCV abierto debido a la sobrepresión"},
        {{1075, 2}, "Common Rail - Error de plausibilidad"},
        {{516118, 11}, "Common Rail - Error de plausibilidad de la corriente VCV"},
        {{516121, 11}, "Common Rail - La presión permanece por encima del punto de ajuste"},
        {{516122, 11}, "Common Rail - La presión permanece por debajo del punto de ajuste"},
        {{516123, 11}, "Common Rail - Presión de arranque demasiado baja"},
        {{516131, 11}, "Sensor de presión Common Rail - Sin variación de señal"},
        {{157, 3}, "Sensor de presión de Common Rail - Cortocircuito a batería o circuito abierto"},
        {{157, 4}, "Sensor de presión Common Rail - Cortocircuito a tierra"},
        {{157, 31}, "Sensor de presión Common Rail - Tensión de alimentación fuera de rango"},
        {{110, 5}, "Sensor de temperatura del refrigerante - Circuito abierto"},
        {{110, 2}, "Sensor de temperatura del refrigerante - Error de plausibilidad"},
        {{110, 3}, "Sensor de temperatura del refrigerante - Cortocircuito a la batería"},
        {{110, 4}, "Sensor de temperatura del refrigerante - Cortocircuito a tierra"},
        {{110, 31}, "Sensor de temperatura del refrigerante - Tensión de alimentación fuera del alcance"},
        {{190, 2}, "Posición del cigüeñal - Error de plausibilidad"},
        {{190, 14}, "Sensor de posición del cigüeñal - Error de tierra"},
        {{190, 5}, "Sensor de posición del cigüeñal - Circuito abierto"},
        {{190, 3}, "Sensor de posición del cigüeñal - Cortocircuito a la batería o circuito abierto"},
        {{190, 4}, "Sensor de posición del cigüeñal - Cortocircuito a tierra"},
        {{190, 31}, "Sensor de posición del cigüeñal - Tensión de alimentación fuera del alcance"},
        {{190, 8}, "Tensión del sensor de velocidad del cigüeñal - Error de plausibilidad"},
        {{516184, 2}, "Panel de control - Error de comunicación"},
        {{516564, 2}, "Control climático entre pasajeros y operadores de dispositivos - Error de comunicación"},
        {{516542, 2}, "Controlador de cabina principal del dispositivo - Error de comunicación"},
        {{1042, 2}, "Puente tractor-remolque del dispositivo - Error de comunicación"},
        {{516576, 31}, "ECU - Abrazadera 30 Desactivada antes del apagado de la ECU"},
        {{1136, 0}, "ECU - Sobretemperatura crítica"},
        {{516543, 31}, "ECU - Se activó la grabadora de datos"},
        {{1136, 15}, "ECU - Advertencia de sobretemperatura"},
        {{516495, 31}, "ECU - El tiempo de ejecución alcanzó el tiempo máximo sin restablecer"},
        {{1136, 31}, "Sensor de temperatura ecu 1 - Tensión de alimentación fuera del alcance"},
        {{516140, 31}, "Sensor de temperatura ecu 2 - Tensión de alimentación fuera del alcance"},
        {{516141, 31}, "Sensor de temperatura ecu 3 - Tensión de alimentación fuera del alcance"},
        {{190, 0}, "Motor - Exceso de velocidad crítico"},
        {{190, 15}, "Motor - Advertencia de exceso de velocidad"},
        {{516544, 31}, "Motor - Arrancador activado pero sin rotación del motor detectada"},
        {{516356, 31}, "Unidad de control del motor - Error de comunicación al módulo de control de inyección secundario"},
        {{516494, 31}, "Unidad de control del motor CAN - mensaje CAN1 recibido después del final de la unidad de post"},
        {{639, 5}, "Unidad de control del motor CAN 1 - Circuito abierto"},
        {{639, 11}, "Unidad de control del motor CAN 1 - Cortocircuito"},
        {{1668, 5}, "Unidad de control del motor CAN 4 - Circuito abierto"},
        {{1668, 11}, "Unidad de control del motor CAN 4 - Cortocircuito"},
        {{110, 0}, "Refrigerante del motor - Sobretemperatura crítica"},
        {{110, 15}, "Refrigerante del motor - Advertencia de sobretemperatura"},
        {{516532, 15}, "Monitorización del motor - Par medio del motor durante la vida útil del motor por encima del umbral"},
        {{516530, 15}, "Monitorización del motor - Par medio del motor durante el último día por encima del umbral"},
        {{516531, 15}, "Monitorización del motor - Par medio del motor durante la última hora por encima del umbral"},
        {{516566, 0}, "Aceite del motor - Tasa crítica de dilución"},
        {{100, 1}, "Aceite del motor - Subpresión crítica"},
        {{516566, 15}, "Aceite del motor - Advertencia de dilución"},
        {{100, 2}, "Aceite del motor - Error de plausibilidad"},
        {{100, 17}, "Aceite del motor - Advertencia de subpresión"},
        {{516283, 15}, "Reducción de potencia de protección del motor - La presión de aumento es demasiado alta"},
        {{516284, 17}, "Reducción de potencia de protección del motor - La presión de aumento es demasiado baja"},
        {{516150, 15}, "Reducción de potencia de protección del motor - Temperatura múltiple de admisión demasiado alta"},
        {{516354, 0}, "Reducción de potencia de protección del motor - Protección del turbocompresor"},
        {{4413, 0}, "Temperatura del gas de escape (SCR aguas arriba) 2 - Sobretemperatura crítica"},
        {{4413, 15}, "Temperatura del gas de escape (SCR ascendente) 2 - Advertencia de sobretemperatura"},
        {{1639, 3}, "Entrada de velocidad del ventilador 1 - Cortocircuito a la batería o circuito abierto"},
        {{1639, 4}, "Entrada de velocidad del ventilador 1 - Cortocircuito a tierra"},
        {{94, 0}, "Suministro de combustible - Sobrepresión crítica"},
        {{174, 0}, "Suministro de combustible - Sobretemperatura crítica"},
        {{94, 1}, "Suministro de combustible - Subpresión crítica"},
        {{94, 15}, "Suministro de combustible - Advertencia de sobrepresión"},
        {{174, 15}, "Suministro de combustible - Advertencia de sobretemperatura"},
        {{94, 17}, "Suministro de combustible - Advertencia de subpresión"},
        {{94, 3}, "Sensor de presión de suministro de combustible - Cortocircuito a la batería"},
        {{94, 4}, "Sensor de presión de suministro de combustible - Cortocircuito a tierra o circuito abierto"},
        {{94, 31}, "Sensor de presión de suministro de combustible - Tensión de alimentación fuera del alcance"},
        {{174, 5}, "Sensor de temperatura del combustible - Circuito abierto"},
        {{174, 2}, "Sensor de temperatura del combustible - Error de plausibilidad"},
        {{174, 3}, "Sensor de temperatura del combustible - Cortocircuito a la batería"},
        {{174, 4}, "Sensor de temperatura del combustible - Cortocircuito a tierra"},
        {{174, 31}, "Sensor de temperatura del combustible - Tensión de alimentación fuera del alcance"},
        {{516195, 2}, "Controlador de conjunto de generadores - Error de comunicación"},
        {{729, 6}, "Brida de calentamiento 1 - Corriente crítica de alta salida"},
        {{729, 11}, "Brida de calefacción 1 - Error eléctrico"},
        {{729, 5}, "Brida de calefacción 1 - Circuito abierto"},
        {{729, 3}, "Brida de calefacción 1 - Cortocircuito a la batería"},
        {{729, 4}, "Brida de calefacción 1 - Cortocircuito a tierra"},
        {{516346, 5}, "Entrada de estado de la brida de calefacción 1 - Circuito abierto"},
        {{516346, 3}, "Entrada de estado de la brida de calefacción 1 - Cortocircuito a la batería"},
        {{516346, 4}, "Entrada de estado de la brida de calefacción 1 - Cortocircuito a tierra"},
        {{516577, 31}, "Bomba de alta presión 1 - Error de instalación"},
        {{516578, 31}, "Bomba de alta presión 2 - Error de instalación"},
        {{651, 0}, "Inyector 1 - Tiempo de aumento actual demasiado largo"},
        {{651, 8}, "Inyector 1 - No se mide el tiempo de aumento actual"},
        {{651, 5}, "Inyector 1 - Circuito Abierto"},
        {{651, 3}, "Inyector 1 - Cortocircuito a la batería"},
        {{651, 4}, "Inyector 1 - Cortocircuito a tierra"},
        {{516401, 31}, "Inyector 1 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{660, 0}, "Inyector 10 - Tiempo de aumento de corriente demasiado largo"},
        {{660, 8}, "Inyector 10 - No se mide el tiempo de aumento actual"},
        {{660, 5}, "Inyector 10 - Circuito Abierto"},
        {{660, 3}, "Inyector 10 - Cortocircuito a la batería"},
        {{660, 4}, "Inyector 10 - Cortocircuito a tierra"},
        {{516410, 31}, "Inyector 10 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{661, 0}, "Inyector 11 - Tiempo de aumento de corriente demasiado largo"},
        {{661, 8}, "Inyector 11 - No se mide el tiempo de aumento actual"},
        {{661, 5}, "Inyector 11 - Circuito Abierto"},
        {{661, 3}, "Inyector 11 - Cortocircuito a la batería"},
        {{661, 4}, "Inyector 11 - Cortocircuito a tierra"},
        {{516411, 31}, "Inyector 11 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{662, 0}, "Inyector 12 - Tiempo de aumento actual demasiado largo"},
        {{662, 8}, "Inyector 12 - No se mide el tiempo de aumento actual"},
        {{662, 5}, "Inyector 12 - Circuito Abierto"},
        {{662, 3}, "Inyector 12 - Cortocircuito a la batería"},
        {{662, 4}, "Inyector 12 - Cortocircuito a tierra"},
        {{516412, 31}, "Inyector 12 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{663, 0}, "Inyector 13 - Tiempo de aumento de corriente demasiado largo"},
        {{663, 8}, "Inyector 13 - No se mide el tiempo de aumento actual"},
        {{663, 5}, "Inyector 13 - Circuito Abierto"},
        {{663, 3}, "Inyector 13 - Cortocircuito a la batería"},
        {{663, 4}, "Inyector 13 - Cortocircuito a tierra"},
        {{516413, 31}, "Inyector 13 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{664, 0}, "Inyector 14 - Tiempo de aumento actual demasiado largo"},
        {{664, 8}, "Inyector 14 - No se mide el tiempo de aumento actual"},
        {{664, 5}, "Inyector 14 - Circuito Abierto"},
        {{664, 3}, "Inyector 14 - Cortocircuito a la batería"},
        {{664, 4}, "Inyector 14 - Cortocircuito a tierra"},
        {{516414, 31}, "Inyector 14 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{665, 0}, "Inyector 15 - Tiempo de aumento actual demasiado largo"},
        {{665, 8}, "Inyector 15 - No se mide el tiempo de aumento actual"},
        {{665, 5}, "Inyector 15 - Circuito Abierto"},
        {{665, 3}, "Inyector 15 - Cortocircuito a la batería"},
        {{665, 4}, "Inyector 15 - Cortocircuito a tierra"},
        {{516415, 31}, "Inyector 15 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{666, 0}, "Inyector 16 - Tiempo de aumento de corriente demasiado largo"},
        {{666, 8}, "Inyector 16 - No se mide el tiempo de aumento actual"},
        {{666, 5}, "Inyector 16 - Circuito Abierto"},
        {{666, 3}, "Inyector 16 - Cortocircuito a la batería"},
        {{666, 4}, "Inyector 16 - Cortocircuito a tierra"},
        {{516416, 31}, "Inyector 16 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{667, 0}, "Inyector 17 - Tiempo de aumento actual demasiado largo"},
        {{667, 8}, "Inyector 17 - No se mide el tiempo de aumento actual"},
        {{667, 5}, "Inyector 17 - Circuito Abierto"},
        {{667, 3}, "Inyector 17 - Cortocircuito a la batería"},
        {{667, 4}, "Inyector 17 - Cortocircuito a tierra"},
        {{516417, 31}, "Inyector 17 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{668, 0}, "Inyector 18 - Tiempo de aumento actual demasiado largo"},
        {{668, 8}, "Inyector 18 - No se mide el tiempo de ascenso actual"},
        {{668, 5}, "Inyector 18 - Circuito Abierto"},
        {{668, 3}, "Inyector 18 - Cortocircuito a la batería"},
        {{668, 4}, "Inyector 18 - Cortocircuito a tierra"},
        {{516418, 31}, "Inyector 18 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{669, 0}, "Inyector 19 - Tiempo de aumento actual demasiado largo"},
        {{669, 8}, "Inyector 19 - No se mide el tiempo de aumento actual"},
        {{669, 5}, "Inyector 19 - Circuito Abierto"},
        {{669, 3}, "Inyector 19 - Cortocircuito a la batería"},
        {{669, 4}, "Inyector 19 - Cortocircuito a tierra"},
        {{516419, 31}, "Inyector 19 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{652, 0}, "Inyector 2 - Tiempo de aumento de corriente demasiado largo"},
        {{652, 8}, "Inyector 2 - No se mide el tiempo de aumento actual"},
        {{652, 5}, "Inyector 2 - Circuito Abierto"},
        {{652, 3}, "Inyector 2 - Cortocircuito a la batería"},
        {{652, 4}, "Inyector 2 - Cortocircuito a tierra"},
        {{516402, 31}, "Inyector 2 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{670, 0}, "Inyector 20 - Tiempo de aumento de corriente demasiado largo"},
        {{670, 8}, "Inyector 20 - No se mide el tiempo de aumento actual"},
        {{670, 5}, "Inyector 20 - Circuito Abierto"},
        {{670, 3}, "Inyector 20 - Cortocircuito a la batería"},
        {{670, 4}, "Inyector 20 - Cortocircuito a tierra"},
        {{516420, 31}, "Inyector 20 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{653, 0}, "Inyector 3 - Tiempo de aumento de corriente demasiado largo"},
        {{653, 8}, "Inyector 3 - No se mide el tiempo de aumento actual"},
        {{653, 5}, "Inyector 3 - Circuito Abierto"},
        {{653, 3}, "Inyector 3 - Cortocircuito a la batería"},
        {{653, 4}, "Inyector 3 - Cortocircuito a tierra"},
        {{516403, 31}, "Inyector 3 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{654, 0}, "Inyector 4 - Tiempo de aumento de corriente demasiado largo"},
        {{654, 8}, "Inyector 4 - No se mide el tiempo de aumento actual"},
        {{654, 5}, "Inyector 4 - Circuito Abierto"},
        {{654, 3}, "Inyector 4 - Cortocircuito a la batería"},
        {{654, 4}, "Inyector 4 - Cortocircuito a tierra"},
        {{516404, 31}, "Inyector 4 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{655, 0}, "Inyector 5 - Tiempo de aumento de corriente demasiado largo"},
        {{655, 8}, "Inyector 5 - No se mide el tiempo de aumento actual"},
        {{655, 5}, "Inyector 5 - Circuito Abierto"},
        {{655, 3}, "Inyector 5 - Cortocircuito a la batería"},
        {{655, 4}, "Inyector 5 - Cortocircuito a tierra"},
        {{516405, 31}, "Inyector 5 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{656, 0}, "Inyector 6 - Tiempo de aumento de corriente demasiado largo"},
        {{656, 8}, "Inyector 6 - No se mide el tiempo de aumento actual"},
        {{656, 5}, "Inyector 6 - Circuito Abierto"},
        {{656, 3}, "Inyector 6 - Cortocircuito a la batería"},
        {{656, 4}, "Inyector 6 - Cortocircuito a tierra"},
        {{516406, 31}, "Inyector 6 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{657, 0}, "Inyector 7 - Tiempo de aumento de corriente demasiado largo"},
        {{657, 8}, "Inyector 7 - No se mide el tiempo de aumento actual"},
        {{657, 5}, "Inyector 7 - Circuito Abierto"},
        {{657, 3}, "Inyector 7 - Cortocircuito a la batería"},
        {{657, 4}, "Inyector 7 - Cortocircuito a tierra"},
        {{516407, 31}, "Inyector 7 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{658, 0}, "Inyector 8 - Tiempo de aumento de corriente demasiado largo"},
        {{658, 8}, "Inyector 8 - No se mide el tiempo de ascenso actual"},
        {{658, 5}, "Inyector 8 - Circuito Abierto"},
        {{658, 3}, "Inyector 8 - Cortocircuito a la batería"},
        {{658, 4}, "Inyector 8 - Cortocircuito a tierra"},
        {{516408, 31}, "Inyector 8 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{659, 0}, "Inyector 9 - Tiempo de aumento actual demasiado largo"},
        {{659, 8}, "Inyector 9 - No se mide el tiempo de ascenso actual"},
        {{659, 5}, "Inyector 9 - Circuito Abierto"},
        {{659, 3}, "Inyector 9 - Cortocircuito a la batería"},
        {{659, 4}, "Inyector 9 - Cortocircuito a la batería"},
        {{516409, 31}, "Inyector 9 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{516191, 17}, "Convertidor de impulso de suministro de inyector - Advertencia de subtensión"},
        {{102, 0}, "Colector de admisión - Sobrepresión crítica"},
        {{105, 0}, "Colector de admisión - Sobretemperatura crítica"},
        {{102, 1}, "Colector de admisión - Subpresión crítica"},
        {{102, 15}, "Colector de admisión - Advertencia de sobrepresión"},
        {{105, 15}, "Colector de admisión - Advertencia de sobretemperatura"},
        {{102, 17}, "Colector de admisión - Advertencia de subpresión"},
        {{102, 2}, "Sensor de presión múltiple de admisión - Error de plausibilidad"},
        {{102, 3}, "Sensor de presión del colector de admisión - Cortocircuito a la batería"},
        {{102, 4}, "Sensor de presión múltiple de admisión - Cortocircuito a tierra o circuito abierto"},
        {{102, 31}, "Sensor de presión del colector de admisión - Tensión de alimentación fuera del alcance"},
        {{105, 5}, "Sensor de temperatura múltiple de admisión - Circuito abierto"},
        {{105, 2}, "Sensor de temperatura múltiple de admisión - Error de plausibilidad"},
        {{105, 3}, "Sensor de temperatura múltiple de admisión - Cortocircuito a la batería"},
        {{105, 4}, "Sensor de temperatura múltiple de admisión - Cortocircuito a tierra"},
        {{105, 31}, "Sensor de temperatura múltiple de admisión - Tensión de alimentación fuera del alcance"},
        {{695, 11}, "J1939 (CTL) - Error de comunicación"},
        {{516196, 2}, "J1939 (Prop3) - Error de comunicación"},
        {{695, 2}, "Equipo de gestión - Error de comunicación"},
        {{516486, 5}, "Activación manual del potenciómetro de velocidad - Circuito abierto"},
        {{516486, 3}, "Activación manual del potenciómetro de velocidad - Cortocircuito a batería o circuito abierto"},
        {{516486, 4}, "Activación manual del potenciómetro de velocidad - Cortocircuito a tierra o circuito abierto"},
        {{516486, 1}, "Activación manual del potenciómetro de velocidad - Valor fuera del alcance"},
        {{516282, 11}, "Sistema de monitoreo - Error de plausibilidad del inyector"},
        {{516227, 11}, "Sistema de monitoreo - Error de seguridad interna de la ECU inicial"},
        {{516223, 11}, "Sistema de monitoreo - Terminal 15 Ecu error de seguridad interna"},
        {{99, 0}, "Presión del filtro de aceite 1 - Sobrepresión crítica"},
        {{99, 15}, "Presión del filtro de aceite 1 - Advertencia de sobrepresión"},
        {{100, 3}, "Sensor de presión de aceite - Cortocircuito a la batería"},
        {{100, 4}, "Sensor de presión de aceite - Cortocircuito a tierra o circuito abierto"},
        {{100, 31}, "Sensor de presión de aceite - Tensión de alimentación fuera del alcance"},
        {{3597, 1}, "Fuente de alimentación - Subtensión crítica"},
        {{3597, 15}, "Fuente de alimentación - Advertencia de sobretensión"},
        {{3597, 17}, "Fuente de alimentación - Advertencia de subtensión"},
        {{5571, 6}, "Válvula de control de presión - Corriente crítica de alta salida"},
        {{5571, 0}, "Válvula de control de presión - Sobrecorriente crítica"},
        {{5571, 1}, "Válvula de control de presión - Corriente subyacente crítica"},
        {{516209, 15}, "Válvula de control de presión - La corriente permanece por encima del punto de ajuste"},
        {{516209, 17}, "Válvula de control de presión - La corriente permanece por debajo del punto de ajuste"},
        {{5571, 11}, "Válvula de control de presión - Error eléctrico"},
        {{516422, 11}, "Válvula de control de presión - La carga es cortocircuitada"},
        {{5571, 5}, "Válvula de control de presión - Circuito abierto"},
        {{516211, 11}, "Válvula de control de presión - Señal PWM - Límite alto alcanzado"},
        {{516212, 11}, "Válvula de control de presión - Señal PWM - Error de plausibilidad"},
        {{5571, 3}, "Válvula de control de presión - Cortocircuito a batería (lado alto)"},
        {{516331, 3}, "Válvula de control de presión - Cortocircuito a la batería (lado bajo)"},
        {{5571, 4}, "Válvula de control de presión - Cortocircuito a tierra (lado alto)"},
        {{516331, 4}, "Válvula de control de presión - Cortocircuito a tierra (lado bajo)"},
        {{900, 2}, "Retardador - Línea motrante - Error de comunicación"},
        {{516239, 5}, "Pasador de selección de modo ECU secundario - Circuito abierto"},
        {{516239, 3}, "Pasador de selección de modo ECU secundario - Cortocircuito a batería o circuito abierto"},
        {{516239, 4}, "Pasador de selección de modo ECU secundario - Cortocircuito a tierra o circuito abierto"},
        {{516239, 11}, "Pin de selección de modo ECU secundario - Valor fuera del rango"},
        {{516197, 2}, "Unidad de control de inyección secundaria - Error de comunicación"},
        {{516510, 31}, "Sensores de velocidad - Error de inversión de posición de los sensores"},
        {{677, 6}, "Arrancador - Corriente crítica de alta salida"},
        {{677, 11}, "Arrancador - Error Eléctrico"},
        {{677, 31}, "Arrancador - Bloqueado debido a la sobretemperatura"},
        {{677, 5}, "Arrancador - Circuito Abierto"},
        {{677, 3}, "Arrancador - Cortocircuito a la batería"},
        {{677, 4}, "Arrancador - Cortocircuito a tierra"},
        {{516424, 11}, "Entrada digital de arranque - Inhibición del arranque debido al cortocircuito"},
        {{516511, 31}, "Arrancadores - No conectados en la salida 1 a ecu"},
        {{1620, 2}, "Tacógrafo - Error de comunicación"},
        {{898, 2}, "Módulo de control de transmisión - Error de comunicación"},
        {{1076, 6}, "Válvula de control de volumen - Corriente crítica de alta salida"},
        {{1076, 0}, "Válvula de control de volumen - Sobrecorriente crítica"},
        {{1076, 1}, "Válvula de control de volumen - Corriente subyacente crítica"},
        {{516251, 0}, "Válvula de control de volumen - La corriente permanece por encima del punto de ajuste"},
        {{516251, 1}, "Válvula de control de volumen - La corriente permanece por debajo del punto de ajuste"},
        {{516330, 11}, "Válvula de control de volumen - Error eléctrico"},
        {{1076, 11}, "Válvula de control de volumen - La carga es cortocircuitada"},
        {{1076, 5}, "Válvula de control de volumen - Circuito abierto"},
        {{516253, 0}, "Válvula de control de volumen - Señal PWM - Límite alto alcanzado"},
        {{516253, 31}, "Válvula de control de volumen - Señal PWM - Error de plausibilidad"},
        {{1076, 3}, "Válvula de control de volumen - Cortocircuito a la batería (lado alto)"},
        {{516328, 3}, "Válvula de control de volumen - Cortocircuito a la batería (lado bajo)"},
        {{1076, 4}, "Válvula de control de volumen - Cortocircuito a tierra (lado alto)"},
        {{516328, 4}, "Válvula de control de volumen - Cortocircuito a tierra (lado bajo)"},
        {{5386, 6}, "Válvula de descarga - Corriente crítica de alta salida"},
        {{5386, 11}, "Válvula de descarga - Error eléctrico"},
        {{516318, 31}, "Válvula de descarga - La carga es cortocircuitada"},
        {{5386, 5}, "Válvula de descarga - Circuito abierto"},
        {{5386, 3}, "Válvula de descarga - Cortocircuito a batería (lado alto)"},
        {{516266, 3}, "Válvula de descarga - Cortocircuito a batería (lado bajo)"},
        {{5386, 4}, "Válvula de descarga - Cortocircuito a tierra (lado alto)"},
        {{516266, 4}, "Válvula de descarga - Cortocircuito a tierra (lado bajo)"},
        {{5386, 31}, "Válvula de descarga - Tensión de alimentación fuera del alcance"},
        {{97, 5}, "Agua en el sensor de combustible - Circuito abierto"},
        {{97, 3}, "Agua en el sensor de combustible - Cortocircuito a la batería"},
        {{97, 4}, "Agua en el sensor de combustible - Cortocircuito a tierra"},
        {{97, 31}, "Agua en el sensor de combustible - Tensión de suministro fuera de rango"},
        {{97, 11}, "Agua en el sensor de combustible - Valor fuera de alcance"},
        {{97, 0}, "Agua en el sensor de combustible - Agua en el combustible detectado"},
        {{516262, 11}, "Bomba de agua - Incapaz de alcanzar la velocidad deseada"},
        {{5201, 3}, "El regulador de presión A (DR_A) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5202, 3}, "El regulador de presión B (DR_B) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5203, 3}, "El regulador de presión D (DR_D) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5204, 3}, "El regulador de presión E (DR_E) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5205, 3}, "El regulador de presión F (DR_F) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5206, 3}, "El regulador de presión C (DR_C) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5207, 3}, "El regulador de presión principal (DR_HD) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5211, 11}, "Durante el proceso de despegue, ya sea la velocidad ha descendido demasiado o el número de intentos permitidos para despegar ha"},
        {{5212, 11}, "El filtro de presión tiene demasiada presión diferencial, compruebe el filtro"},
        {{5221, 4}, "El regulador de presión A (DR_A) tiene un cortocircuito"},
        {{5222, 4}, "El regulador de presión B (DR_B) tiene un cortocircuito"},
        {{5223, 4}, "El regulador de presión D (DR_D) tiene un cortocircuito"},
        {{5224, 4}, "El regulador de presión E (DR_E) tiene un cortocircuito"},
        {{5225, 4}, "El regulador de presión F (DR_F) tiene un cortocircuito"},
        {{5226, 4}, "El regulador de presión C (DR_C) tiene un cortocircuito"},
        {{5227, 4}, "El regulador de presión principal (DR_HD) tiene un cortocircuito"},
        {{5231, 5}, "Circuito del regulador de presión A (DR_A) se interrumpió"},
        {{5232, 5}, "Circuito del regulador de presión B (DR_B) se interrumpió"},
        {{5233, 5}, "Circuito del regulador de presión D (DR_D) se interrumpió"},
        {{5234, 5}, "Circuito del regulador de presión E (DR_E) se interrumpió"},
        {{5235, 5}, "Circuito del regulador de presión F (DR_F) se interrumpió"},
        {{5236, 5}, "Circuito del regulador de presión C (DR_C) se interrumpió"},
        {{5237, 5}, "Circuito del regulador de presión principal (DR_HD) se interrumpió"},
        {{5241, 14}, "El regulador de presión A (DR_A) tiene un cortocircuito en otra salida (AIM)"},
        {{5242, 14}, "El regulador de presión B (DR_B) tiene un cortocircuito en otra salida (AIM)."},
        {{5243, 14}, "El regulador de presión D (DR_D) tiene un cortocircuito en otra salida (AIM)"},
        {{5244, 14}, "El regulador de presión E (DR_E) tiene un cortocircuito en otra salida (AIM)."},
        {{5245, 14}, "El regulador de presión F (DR_F) tiene un cortocircuito en otra salida (AIM)"},
        {{5246, 14}, "El regulador de presión C (DR_C) tiene un cortocircuito en otra salida (AIM)"},
        {{5247, 14}, "El regulador de presión principal (DR_HD) tiene un cortocircuito en otra salida (AIM)"},
        {{5252, 4}, "Los pasadores de alimentación positivos de todas las válvulas proporcionales (VPS) tienen un cortocircuito"},
        {{5253, 14}, "Fallo eléctrico en el sensor de velocidad de la turbina"},
        {{5254, 14}, "Mal funcionamiento de la entrada de hardware para el sensor de velocidad de la turbina"},
        {{5255, 5}, "Cambio poco realista en la velocidad de la turbina en EF0"},
        {{5256, 14}, "Fallo eléctrico en el sensor de velocidad de salida"},
        {{5257, 14}, "Mal funcionamiento de la entrada de hardware para el sensor de velocidad de salida"},
        {{5258, 5}, "Cambio poco realista en la velocidad de salida en EF1"},
        {{5260, 14}, "Configuración del programa de unidades activas no válida"},
        {{5262, 14}, "La unidad central de corte (ZAE) ya no se corta."},
        {{5264, 1}, "Fuente de alimentación TCU (KL30) subvoltage"},
        {{5265, 0}, "Fuente de alimentación TCU (KL30) sobretensión"},
        {{5266, 14}, "La señal de dirección de la tecla y la conducción del mensaje selector de rango de velocidad no son plausibles"},
        {{5273, 1}, "Fallo del sumidero del sensor de temperatura (ER1) bajo -> corresponde a alta temperatura"},
        {{5274, 0}, "Fallo de sumidero del sensor de temperatura (ER1) alto -> corresponde a baja temperatura"},
        {{5280, 4}, "El sensor de velocidad de la turbina de alimentación (AU1) tiene cortocircuito a tierra"},
        {{5281, 3}, "El sensor de velocidad de la turbina de alimentación (AU1) tiene cortocircuito para una fuente positiva (VPS)"},
        {{5283, 4}, "El sensor de velocidad de la turbina de alimentación (AU2) tiene cortocircuito a tierra"},
        {{5284, 3}, "El sensor de velocidad de la turbina de alimentación (AU2) tiene cortocircuito para una fuente positiva (VPS)"},
        {{5292, 14}, "CAN_A o CAN_B tiene un error durante la inicialización"},
        {{5293, 14}, "CAN_A - BUSOFF (vehículo CAN)"},
        {{5295, 14}, "CAN_A - error pasivo (vehículo CAN)"},
        {{5296, 14}, "CAN_A - advertencia de error (vehículo CAN)"},
        {{5310, 14}, "Fallo del mensaje CAN EEC1 - controlador de motor electrónico 1"},
        {{5311, 14}, "Fallo del mensaje CAN EEC2 - controlador de motor electrónico 2"},
        {{5313, 14}, "Fallo del mensaje CAN EC - configuración del motor"},
        {{5316, 14}, "Error del mensaje CAN TC1 - control de transmisión 1 (de otro)"},
        {{5327, 14}, "El par del motor actual no está disponible (SAE)"},
        {{5328, 14}, "La velocidad del motor no está disponible (SAE)"},
        {{5330, 14}, "La posición del pedal del acelerador no está disponible (SAE)"},
        {{5331, 14}, "La relación entre el par actual del motor y el par máximo posible del motor no está disponible a la velocidad actual (SAE)"},
        {{5332, 14}, "El par de fricción no está disponible (SAE)"},
        {{5333, 14}, "El par de referencia del motor no está disponible ni fuera de la ventana de especificación (SAE)"},
        {{5343, 14}, "CAN_A error de envío"},
        {{5355, 14}, "Checksum EEPROM incorrecto"},
        {{5356, 14}, "Error compuesto para hardware TCU (interno)"},
        {{5357, 14}, "Error compuesto para el software TCU (interno)"},
        {{5363, 14}, "Fallo de la velocidad de la turbina (n_Tu) y la velocidad de salida (n_Ab)"},
        {{5364, 14}, "Fallo de la velocidad de la turbina (n_Tu) y la velocidad del motor (n_Mot)"},
        {{5365, 14}, "Fallo de la velocidad del motor (n_Mot) y la velocidad de salida (n_Ab)"},
        {{5388, 14}, "Deslizamiento en piñón fijo"},
        {{5432, 14}, "Fallo de la velocidad del motor (n_Mot), la velocidad de la turbina (n_Tu) y la velocidad de salida (n_Ab)"},
        {{5435, 14}, "Advertencia aumento de la temperatura del sumidero"},
        {{5436, 14}, "Sobrecalentamiento de la temperatura del sumidero"},
        {{5460, 14}, "El modo de escritorio está activo"},
        {{5572, 14}, "TCU hardware (interno)"},
        {{5585, 14}, "Error del convertidor digital analógico"},
        {{5590, 14}, "Los valores de calibración de corriente eléctrica no son plausibles"},
        {{5591, 14}, "Los valores de EEprom no son plausibles"}
    }

    Private Shared Property _errordictionary As New Dictionary(Of IEnumerable(Of Integer), String)() From {}
    Public Shared Property ErrorDictionary() As Dictionary(Of IEnumerable(Of Integer), String)
        Get
            Return _errordictionary
        End Get
        Set(value As Dictionary(Of IEnumerable(Of Integer), String))
            If value IsNot _errordictionary Then
                _errordictionary = value
            End If
        End Set
    End Property


    Private Shared Event SourceChanged()
    Private Shared _source As String
    Private Shared Property Source() As String
        Get
            Return _source
        End Get
        Set(value As String)
            If value <> _source Then
                _source = value
                RaiseEvent SourceChanged()
            End If
        End Set
    End Property

    Private Shared Event UseSourceChanged()
    Private Shared _usesource As Boolean
    Private Shared Property UseSource() As Boolean
        Get
            Return _usesource
        End Get
        Set(value As Boolean)
            If value <> _usesource Then
                _usesource = value
                RaiseEvent UseSourceChanged()
            End If
        End Set
    End Property

    Private Shared _currentfrac As Integer
    Private Shared Property CurrentFrac() As Integer
        Get
            Return _currentfrac
        End Get
        Set(value As Integer)

        End Set
    End Property

    Private Shared Event MotorSourceChange()
    Private Shared _motorsource As Integer
    Private Shared Property MotorSource() As Integer
        Get
            Return _motorsource
        End Get
        Set(value As Integer)
            If value <> _motorsource Then
                _motorsource = value
                RaiseEvent MotorSourceChange()
            End If
        End Set
    End Property

    Private Shared Event TransSourceChange()
    Private Shared _transsource As Integer
    Private Shared Property TransSource() As Integer
        Get
            Return _transsource
        End Get
        Set(value As Integer)
            If value <> _transsource Then
                _transsource = value
                RaiseEvent TransSourceChange()
            End If
        End Set
    End Property

    Public Sub Update()
        Select Case LanguageSelect.CurrentLanguage
            Case "Español"
                ErrorDictionary = ErrorDictionary_Español
            Case "Ingles"
                ErrorDictionary = ErrorDictionary_English
        End Select
    End Sub


    ''' <summary>
    ''' Funcion para buscar un error por SPN y FMI.
    ''' </summary>
    ''' <param name="SPN">Parametro SPN para errores en ECU.</param>
    ''' <param name="FMI">Parametro FMI para errores en ECU.</param>
    ''' <remarks></remarks>
    Public Function Search(ByVal SPN As Integer, ByVal FMI As Integer)
        For Each currentKey As IEnumerable(Of Integer) In ErrorDictionary.Keys
            If currentKey(0) = SPN And currentKey(1) = FMI Then
                Return ErrorDictionary(currentKey)
            End If
        Next
        Return "Error desconocido"
    End Function

    ''' <summary>
    ''' Funcion para saber si existe un error segun SPN y FMI.
    ''' </summary>
    ''' <param name="SPN">Parametro SPN para errores en ECU.</param>
    ''' <param name="FMI">Parametro FMI para errores en ECU.</param> 
    ''' <remarks></remarks>
    Public Function ContainsKey(ByVal SPN As Integer, ByVal FMI As Integer)
        For Each currentKey As IEnumerable(Of Integer) In ErrorDictionary.Keys
            If currentKey(0) = SPN And currentKey(1) = FMI Then
                Return True
            End If
        Next
        Return False
    End Function


    ''' <summary>
    ''' Funcion para configurar los parametros para la busqueda de errores.
    ''' </summary>
    ''' <param name="Source">Nombre de la fuente, "Motor" o "Transmision".</param>
    ''' <param name="UseSource">Variable para uso de fuente, si es True se puede optar por usar fuentes especificas.</param>
    ''' <param name="CurrentFrac">ID del Fracturador actual.</param>
    ''' <param name="MotorSource">Source del Motor.</param>
    ''' <param name="TransSource">Source de la transmision.</param>
    '''  <remarks></remarks>
    Public Sub Settings(ByVal Source As String, ByVal UseSource As Boolean, ByVal CurrentFrac As Integer, Optional ByVal MotorSource As Integer = 5, Optional ByVal TransSource As Integer = 3)
        ''Se puede cambiar a my.settings, o leer alguna configuracion del moxa q nos de el plus+1
        ErrorInfo.Source = Source
        ErrorInfo.UseSource = UseSource
        ErrorInfo.CurrentFrac = CurrentFrac
        ErrorInfo.MotorSource = MotorSource
        ErrorInfo.TransSource = TransSource
    End Sub

    ''' <summary>
    ''' Funcion para buscar el siguiente error.
    ''' </summary>
    Public Async Sub FindNextError()
        Dim NextError As New Task(AddressOf NextErrorDo)
        NextError.Start()
        Await NextError
    End Sub
    Private Sub NextErrorDo()
        'moxaSend(MoxaSendStartAddress("ErrorAnterior2StartAddress"), 0, CurrentFrac)
        'moxaSend(MoxaSendStartAddress("ErrorAnterior1StartAddress"), 1, CurrentFrac)
        Thread.Sleep(1000)
        'moxaSend(MoxaSendStartAddress("ErrorAnterior1StartAddress"), 0, CurrentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para buscar el error previo.
    ''' </summary>
    Public Async Sub FindPrevError()
        Dim PrevError As New Task(AddressOf PrevErrorDo)
        PrevError.Start()
        Await PrevError
    End Sub
    Private Sub PrevErrorDo()
        'moxaSend(MoxaSendStartAddress("ErrorAnterior1StartAddress"), 0, currentFrac)
        'moxaSend(MoxaSendStartAddress("ErrorAnterior2StartAddress"), 1, currentFrac)
        Thread.Sleep(1000)
        'moxaSend(MoxaSendStartAddress("ErrorAnterior2StartAddress"), 0, currentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para borrar el error actual.
    ''' </summary>
    Public Async Sub DeleteCurrentError()
        Dim DelError As New Task(AddressOf DelErrorDo)
        DelError.Start()
        Await DelError
    End Sub
    Private Sub DelErrorDo()
        'moxaSend(MoxaSendStartAddress("ErrorDeleteActualStartAddress"), 1, currentFrac)
        Thread.Sleep(1000)
        'moxaSend(MoxaSendStartAddress("ErrorDeleteActualStartAddress"), 0, currentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para borrar todos los errores.
    ''' </summary>
    Public Async Sub DeleteAllErrors()
        Dim DelAllError As New Task(AddressOf DelAllErrorDo)
        DelAllError.Start()
        Await DelAllError
    End Sub
    Private Sub DelAllErrorDo()
        'moxaSend(MoxaSendStartAddress("ErrorDeleteAllStartAddress"), 1, currentFrac)
        Thread.Sleep(1000)
        'moxaSend(MoxaSendStartAddress("ErrorDeleteAllStartAddress"), 0, currentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para leer la descripcion del error.
    ''' </summary>
    Public Function GetError() As String
        If DblVFrac("TotalErrores_" + CurrentFrac.ToString()) <> 0 Then
            Return Search(DblVFrac("SPN_" + CurrentFrac.ToString()), DblVFrac("FMI_" + CurrentFrac.ToString()))
        Else
            Return "No se registro ningun error"
        End If
    End Function

    ''' <summary>
    ''' Funcion para obtener la fuente del error.
    ''' </summary>
    Public Function GetSource() As String
        Select Case DblVFrac("FuenteError_" + CurrentFrac.ToString())
            Case 3
                Return "Transmisión"
            Case 0
                Return "Motor"
        End Select
        Return "Fuente desconocida"
    End Function


    ''' <summary>
    ''' Funcion para saber si un error esta activo.
    ''' </summary>
    Public Function GetActive() As String
        If DblVFrac("TotalErrores_" + CurrentFrac.ToString()) <> 0 Then
            Return "Si"
        Else
            Return "No"
        End If
    End Function

    ''' <summary>
    ''' Funcion para setear el use source.
    ''' </summary>
    Private Sub SetUseSource() Handles MyClass.UseSourceChanged
        ''SENDE A DIRECCION DE MOXA EL USE SOUR
    End Sub

    ''' <summary>
    ''' Funcion para setear source del motor.
    ''' </summary>
    Private Sub SetMotorSource() Handles MyClass.MotorSourceChange
        ''SEND A MOXA EL NUEVO MOTOR SOURCE
    End Sub

    ''' <summary>
    ''' Funcion para setear source de la transmision.
    ''' </summary>
    Private Sub SetTransSource() Handles MyClass.TransSourceChange

    End Sub

    ''' <summary>
    ''' Funcion para setear vbctrl.
    ''' </summary>
    Private Sub SetVBCTRL() Handles MyClass.SourceChanged
        ''Send a moxa VbCtrl
        Select Case Source
            Case "Motor"
            Case "Transmision"
        End Select
    End Sub


End Class