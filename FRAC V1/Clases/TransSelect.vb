﻿Public Class TransSelect

    Private Shared _transmission As String
    Public Shared Event TransmissionChange()
    Public Shared SelectedTransmisison As Dictionary(Of String, Double) = Transmission1


    Public Shared Property Transmission() As String
        Get
            Return _transmission
        End Get
        Set(ByVal value As String)
            If value <> _transmission Then
                _transmission = value
                RaiseEvent TransmissionChange()
            End If
        End Set
    End Property

    Private Sub TransChange() Handles MyClass.TransmissionChange
        Select Case Transmission
            Case "Transmission1"
                SelectedTransmisison = Transmission1
        End Select
    End Sub

    Private Shared ReadOnly Transmission1 As New Dictionary(Of String, Double)() From {
        {"RelacionGear1", 3.674},
        {"RelacionGear2", 2.667},
        {"RelacionGear3", 2.156},
        {"RelacionGear4", 1.708},
        {"RelacionGear5", 1.378},
        {"RelacionGear6", 1.217},
        {"RelacionGear7", 1.005}
    }

End Class
