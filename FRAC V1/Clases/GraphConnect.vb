﻿Public Class GraphConnect

    Public DataNombre() As String = {"uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "uno1", "dos1", "tres1", "cuatro1", "cinco1", "seis1", "siete1", "ocho1"}
    Public fileReader As String()
    Public saveDLG As SaveFileDialog = New SaveFileDialog()
    Public datos As Integer = 8
    Dim test1 As Integer = 0
    Private WithEvents TmrRead As New Timer() With {.Interval = 1000, .Enabled = False}
    Private WithEvents TmrWrite As New Timer() With {.Interval = 500, .Enabled = False}

    Dim TotalFracs As Integer

    Public Sub StartRead(ByVal nFrac As Integer)
        TotalFracs = nFrac
        TmrRead.Enabled = True
    End Sub

    Public Sub StopRead(ByVal nFrac As Integer)
        TotalFracs = nFrac
        TmrRead.Enabled = False
    End Sub

    Public Sub StartWrite(ByVal nFrac As Integer)
        TotalFracs = nFrac
        TmrWrite.Enabled = True
    End Sub

    Public Sub StopWrite(ByVal nFrac As Integer)
        TotalFracs = nFrac
        TmrWrite.Enabled = False
    End Sub

    Private Sub TmrReadTick(sender As Object, e As EventArgs) Handles TmrRead.Tick
        Try

            Dim totalLines As Integer = 0
        Dim objReader As New System.IO.StreamReader("C:\QMSoftware\FracData.graph")

        For thisFrac = 0 To TotalFracs - 1
            Do While objReader.Peek() <> -1
                ReDim Preserve fileReader(totalLines)
                fileReader(totalLines) = objReader.ReadLine() & vbNewLine
                fileReader(totalLines) = fileReader(totalLines).Replace(vbCr, "").Replace(vbLf, "")
                totalLines += 1
            Loop

            Dim Datos As Integer = GetInteger(fileReader(0 + (thisFrac * (Datos + 2))))
            ReDim Preserve DataNombre(Datos * (thisFrac + 1) - 1)

            For i = 0 To Datos - 1
                Dim FirstOne As Integer = fileReader(1 + (thisFrac * (Datos + 2))).IndexOf(";")
                fileReader(1 + (thisFrac * (Datos + 2))) = Replace(fileReader(1 + (thisFrac * (Datos + 2))), ";", Nothing, FirstOne + 1, 1)
                Dim NextOne As Integer = fileReader(1 + (thisFrac * (Datos + 2))).IndexOf(";")
                DataNombre(i) = fileReader(1 + (thisFrac * (Datos + 2))).Substring(0, NextOne)
            Next

            For l = 0 To Datos - 1
                Dim FirstOne As Integer = fileReader(l + 2 + (thisFrac * (Datos + 2))).IndexOf(";")
                fileReader(l + 2 + (thisFrac * (Datos + 2))) = Replace(fileReader(l + 2 + (thisFrac * (Datos + 2))), ";", Nothing, FirstOne + 1, 1)
                Dim NextOne As Integer = fileReader(l + 2 + (thisFrac * (Datos + 2))).IndexOf(";")
                    DataSensor(l + (thisFrac * (Datos + 2))) = fileReader(l + 2 + (thisFrac * (Datos + 2))).Substring(0, NextOne)
                Next
        Next
            objReader.Close()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub TmrWriteTick(sender As Object, e As EventArgs) Handles TmrWrite.Tick
        Try
            Dim objWriter As New System.IO.StreamWriter("C:\QMSoftware\FracData.graph")
            For ThisFrac = 0 To TotalFracs - 1
                objWriter.WriteLine(datos)
                For i = 0 To datos - 1
                    objWriter.Write(";" + DataNombre(i + (ThisFrac * datos)))
                Next
                objWriter.WriteLine(";")

                For i = 0 To datos - 1
                    objWriter.Write(";")
                    objWriter.Write(test1 + i + ThisFrac)      ''aca en realidad va el dato posta
                    objWriter.WriteLine(";")
                Next
            Next
            objWriter.Close()
            test1 += 1
        Catch ex As Exception

        End Try
    End Sub
End Class
