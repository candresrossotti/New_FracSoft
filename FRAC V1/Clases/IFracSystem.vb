﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading

Public Class IFracSystem

    Private ReadOnly ES As New EngineSelect
    Private ReadOnly NAF As New NAutomaticFrac
    Private ReadOnly PS As New PumpSelect
    Private ReadOnly TS As New TransSelect
    Private ReadOnly EI As New ErrorInfo

    Private Shared _deviceiplist As New Dictionary(Of Integer, String)
    Private Shared _frac As New Dictionary(Of Integer, Integer)
    Private Shared _msocket As Socket() = Nothing
    Private Shared _fracid As New Dictionary(Of Integer, Integer)
    Private Shared _totalfracs As Integer = 8
    Private Shared _ipscanstartaddress As Integer = 1
    Private Shared _ipscanendaddress As Integer = _ipscanstartaddress + TotalFracs - 1
    Private Shared _fracname As New Dictionary(Of Integer, String)

    Private Shared ReadOnly devicePORT As Integer = 502
    Private Shared ReadOnly functionCodeRX As Integer = 3
    Private Shared ReadOnly functionCodeTX As Integer = 6
    Private Shared ReadOnly id As Integer = 3
    Private Shared ReadOnly slaveAddress As Integer = 1
    Private Shared ReadOnly numberOfPoints As Integer = 40
    Private Shared devices As Integer = 0
    Private Shared deviceIP As String

    ''----------------------------------------------------------------------------------------------------------''
    ''---------------------------------      PROPIEDADES PARA TODA LA CLASE     --------------------------------''
    ''----------------------------------------------------------------------------------------------------------''
    Private Shared Property FracName() As Dictionary(Of Integer, String)
        Get
            Return _fracname
        End Get
        Set(value As Dictionary(Of Integer, String))
            If value IsNot _fracname Then
                _fracname = value
            End If
        End Set
    End Property
    Private Shared Property TotalFracs() As Integer
        Get
            Return _totalfracs
        End Get
        Set(value As Integer)
            If value <> _totalfracs Then
                _totalfracs = value
            End If
        End Set
    End Property
    Private Shared Property FracID() As Dictionary(Of Integer, Integer)
        Get
            Return _fracid
        End Get
        Set(value As Dictionary(Of Integer, Integer))
            If value IsNot _fracid Then
                _fracid = value
            End If
        End Set
    End Property
    Private Shared Property MSocket() As Socket()
        Get
            Return _msocket
        End Get
        Set(value As Socket())
            If value IsNot _msocket Then
                _msocket = value
            End If
        End Set
    End Property
    Private Shared Property Frac() As Dictionary(Of Integer, Integer)
        Get
            Return _frac
        End Get
        Set(value As Dictionary(Of Integer, Integer))
            If value IsNot _frac Then
                _frac = value
            End If
        End Set
    End Property
    Private Shared Property DeviceIPLIST As Dictionary(Of Integer, String)
        Get
            Return _deviceiplist
        End Get
        Set(value As Dictionary(Of Integer, String))
            If value IsNot _deviceiplist Then
                _deviceiplist = value
            End If
        End Set
    End Property
    Private Shared Property IPScanStartAddress As Integer
        Get
            Return _ipscanstartaddress
        End Get
        Set(value As Integer)
            If value <> _ipscanstartaddress Then
                _ipscanstartaddress = value
            End If
        End Set
    End Property
    Private Shared Property IPScanEndAddress As Integer
        Get
            Return _ipscanendaddress
        End Get
        Set(value As Integer)
            If value <> _ipscanendaddress Then
                _ipscanendaddress = value
            End If
        End Set
    End Property


    ''----------------------------------------------------------------------------------------------------------''
    ''-----------------------------------      FUNCIONES PARA REFERENCIAS     ----------------------------------''
    ''----------------------------------------------------------------------------------------------------------''
    ''' <summary>
    ''' Funcion que devuelve el nombre del frac segun su ID.
    ''' </summary>
    ''' <param name="ID">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Function GetFracNamyById(ByVal ID As Integer) As String
        Return FracName(ID)
    End Function

    ''' <summary>
    ''' Funcion que devuelve el numero del fracturador segun su ID.
    ''' </summary>
    ''' <param name="ID">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Function GetFracById(ByVal ID As Integer) As Integer
        Return Frac(ID)
    End Function

    ''' <summary>
    ''' Funcion que devuelve el ID segun el numero de fracturador.
    ''' </summary>
    ''' <param name="Frac">Numero del Fracturador.</param>
    '''  <remarks></remarks>
    Public Function GetIdByFrac(ByVal Frac As Integer) As Integer
        Return FracID(Frac)
    End Function


    ''----------------------------------------------------------------------------------------------------------''
    ''-------------------------------      FUNCIONES DE CONEXION/DESCONEXION     -------------------------------''
    ''----------------------------------------------------------------------------------------------------------''

    ''' <summary>
    ''' Funcion para escanear fracturadores en un rango de IPs
    ''' </summary>
    Public Function AutoScan() As Boolean

        ReDim MSocket(TotalFracs - 1)

        Try
            For i = IPScanStartAddress To IPScanEndAddress      ''17 a 24
                deviceIP = "127.0.0." + i.ToString()
                If My.Computer.Network.Ping(deviceIP, 10) Then
                    For Each Bn As Guna.UI2.WinForms.Guna2Button In FracSelect.Controls.OfType(Of Guna.UI2.WinForms.Guna2Button)
                        If Bn.Tag = devices.ToString() Then
                            Bn.Show()
                            FracName.Add(devices, "LAT-2" + (i - (IPScanStartAddress - 1)).ToString.PadLeft(2, "0"))
                            Frac.Add(devices, i - (IPScanStartAddress - 1))
                            FracID.Add(i - (IPScanStartAddress - 1), devices)
                            DeviceIPLIST.Add(devices, deviceIP)
                            Bn.Text = FracName(devices)
                        End If
                    Next
                    devices += 1
                End If
                Inicio.PbInicio.Value = (i - (IPScanStartAddress - 1))
            Next
            Return True
        Catch ex As Exception
            avisoTitulo = "Error"
            avisoStr = "Ocurrio un error en la deteccion de los fracturadores"
            AvisoGeneral.OpenDialog()
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Funcion para conectarse a un fracturador.
    ''' </summary>
    ''' <param name="frac">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Function Connect(frac As Integer) As Boolean
        Dim state As Boolean
        Dim frame As Byte()
        ReDim frame(7)


        deviceIP = DeviceIPLIST(frac)

        Try
            MSocket(frac) = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp) With {
                .ReceiveBufferSize = 256,
                .SendBufferSize = 256,
                .ReceiveTimeout = 500,
                .SendTimeout = 500
            }
            Dim server As IPEndPoint = New IPEndPoint(IPAddress.Parse(deviceIP), devicePORT)
            MSocket(frac).Connect(server)
            state = True
            Return state
        Catch ex As Exception
            avisoTitulo = "Error"
            avisoStr = "Se produjo un error al intentar conectarse al fracturador."
            AvisoGeneral.OpenDialog()
            state = False
            Return state
        End Try
    End Function

    ''' <summary>
    ''' Funcion para desconectarse de un fracturador.
    ''' </summary>
    Public Sub Disconnect()
        Try
            For i = 0 To devices - 1
                If (MSocket(i).Connected) Then
                    MSocket(i).Close()
                End If
            Next
        Catch ex As Exception
            avisoTitulo = "Error"
            avisoStr = "Se produjo un error al desconectarse. Intente en un momento"
            AvisoGeneral.OpenDialog()
        End Try
    End Sub


    ''----------------------------------------------------------------------------------------------------------''
    ''-----------------------------------      FUNCIONES PARA SOCKET TCP     -----------------------------------''
    ''----------------------------------------------------------------------------------------------------------''
    Private Function Read(socket As Integer) As Byte()
        Dim result As Byte() = New Byte(255) {}
        Dim ns As NetworkStream = New NetworkStream(MSocket(socket))
        If ns.CanRead Then
            Dim rs As Integer = MSocket(socket).Receive(result, result.Length, SocketFlags.None)
        End If
        Return result
    End Function
    ''FUNCION PARA ESCRIBIT SOCKET
    Private Function Write(frame As Byte(), socket As Integer) As Integer
        Return MSocket(socket).Send(frame, frame.Length, SocketFlags.None)
    End Function
    ''FUNCTION QUE CREA FRAME PARA SOLICITAR QUE SE TE ENVIEN DATOS DE TCP/MODBUS
    Private Function ReadHoldingRegisters(id As Byte, slaveAddress As Byte, startAddress As UShort, functionCode As Byte, numberOfPoint As UShort) As Byte()
        Dim frame As Byte() = New Byte(11) {} ' Total 12 Bytes
        frame(0) = CByte(id / 256) ' Transaction Identifier High
        frame(1) = CByte(id Mod 256) ' Transaction Identifier Low
        frame(2) = 0 ' Protocol Identifier High
        frame(3) = 0 ' Protocol Identifier Low
        frame(4) = 0 ' Message Length High.
        frame(5) = 6 ' Message Length Low(6 bytes to follow)
        frame(6) = slaveAddress ' The Unit Identifier(slave Address/Slave Id).
        frame(7) = functionCode ' Function.
        frame(8) = CByte(startAddress / 256) ' Starting Address High.
        frame(9) = CByte(startAddress Mod 256) ' Starting Address Low.
        frame(10) = CByte(numberOfPoint / 256) ' Quantity of Registers High
        frame(11) = CByte(numberOfPoint Mod 256) 'Quantity of Registers Low
        Return frame
    End Function
    ''FUNCION QUE CREA FRAME PARA ESCRIBIR DATOS EN TCP/MODBUS
    Private Function WriteSingleCoil(id As UShort, slaveAddress As Byte, startAddress As UShort, functionCode As Byte, data As UShort) As Byte()
        Dim frame1 As Byte() = New Byte(11) {} ' Total 12 Bytes.
        Dim byteArray As Byte() = BitConverter.GetBytes(data)
        frame1(0) = CByte(id / 256) ' Transaction Identifier High. 
        frame1(1) = CByte(id Mod 256) ' Transaction Identifier Low.
        frame1(2) = 0 ' Protocol Identifier High.
        frame1(3) = 0 ' Protocol Identifier Low.
        frame1(4) = 0 ' Message Length High.
        frame1(5) = 6 ' Message Length Low(6 bytes to follow).
        frame1(6) = slaveAddress ' The Unit Identifier.
        frame1(7) = functionCode ' Function.
        frame1(8) = CByte(startAddress / 256) ' Starting Address High.
        frame1(9) = CByte(startAddress Mod 256) ' Starting Address Low.
        frame1(10) = byteArray(1) ' Write Data High.
        frame1(11) = byteArray(0) ' Write Data Low.
        Return frame1
    End Function



    ''----------------------------------------------------------------------------------------------------------''
    ''---------------------------------      FUNCIONES DE ESCRITURA/LECTURA     --------------------------------''
    ''----------------------------------------------------------------------------------------------------------''

    ''' <summary>
    ''' Funcion para enviar datos al moxa.
    ''' </summary>
    ''' <param name="startAddressTX">Address inicial donde se van a escribir los datos.</param>
    ''' <param name="data">Datos a escribir.</param>
    ''' <param name="socket">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Sub MoxaSend(startAddressTX As Integer, data As Integer, socket As Integer)
        Try
            Dim dataSend As UShort = UShort.Parse(data) ' Write Data.
            Dim frameSend As Byte() = WriteSingleCoil(id, slaveAddress, startAddressTX, functionCodeTX, dataSend)
            Write(frameSend, socket) ' Write frame to device.
            Thread.Sleep(10)

            ''ACA HAY QUE REVISAR SI ESTAS LINEAS SON NECESARIAS
            'Dim buffRecei1 As Byte() = Read(socket) ' Read data from device.
            'Dim byteMsg1 As Byte() = New Byte(11) {} ' Total 12 Bytes
            'Array.Copy(buffRecei1, 0, byteMsg1, 0, byteMsg1.Length)
        Catch ex As Exception
            avisoTitulo = "Error"
            avisoStr = "Se produjo un error en el envio de datos al fracturador"
            If AvisoGeneral.ShowDialog() = DialogResult.OK Then
                ''Application.Exit()
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Funcion para leer datos del moxa.
    ''' </summary>
    ''' <param name="startAddressRX">Address inicial donde se van a leer los datos.</param>
    ''' <param name="numberOfPoints">Cantidad de datos a leer.</param>
    ''' <param name="socket">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Function MoxaRead(startAddressRX As Integer, numberOfPoints As Integer, socket As Integer) As Byte()
        Try
            Dim frame As Byte() = ReadHoldingRegisters(id, slaveAddress, startAddressRX, functionCodeRX, numberOfPoints)
            Write(frame, socket)
            Thread.Sleep(10)
            Dim buffReceived As Byte() = Read(socket)
            If IsDBNull(buffReceived) Then
                Return {0}
            End If

            Dim sizeBytes = buffReceived(8)

            If (functionCodeRX = buffReceived(7)) Then
                Dim byteMsg As Byte() = New Byte(8 + sizeBytes) {}
                Array.Copy(buffReceived, 0, byteMsg, 0, byteMsg.Length)
                Dim rData As Byte() = New Byte(sizeBytes - 1) {}
                Array.Copy(buffReceived, 9, rData, 0, rData.Length)
                Return rData
            End If
        Catch ex As Exception
            avisoTitulo = "Error"
            avisoStr = "Se produjo un error en la recepcion de datos del fracturador"
            AvisoGeneral.Show()
            Return {0}
        End Try
        Return {0}
    End Function



    ''----------------------------------------------------------------------------------------------------------''
    ''--------------------------------      FUNCIONES PARA EL INICIO DEL FRAC     ------------------------------''
    ''----------------------------------------------------------------------------------------------------------''

    ''' <summary>
    ''' Funcion para inicializar el fracturador.
    ''' </summary>
    Public Sub InitializeSystem()

        AddHandler EngineSelect.EngineChange, AddressOf NAF.Update
        AddHandler PumpSelect.PumpChange, AddressOf NAF.Update
        AddHandler TransSelect.TransmissionChange, AddressOf NAF.Update
        AddHandler LanguageSelect.LanguageChange, AddressOf EI.Update

        PumpSelect.Pump() = "NA"
        TransSelect.Transmission() = "Transmission1"
        EngineSelect.Engine() = "LiebherrD9620A7"
        LanguageSelect.CurrentLanguage = "Ingles"
    End Sub

    ''' <summary>
    ''' Funcion para cargar datos al fracturador.
    ''' </summary>
    ''' <param name="frac">Numero del fracturador.</param>
    Public Sub FracLoad(frac As Integer) '' , socket As Integer

        ''CARGA Y ENVIO DE PARAMETROS VIATRAN
        Dim minInViatran As Integer = My.Settings.Item("minInViatran_Frac" + frac.ToString())
        Dim maxInViatran As Integer = My.Settings.Item("maxInViatran_Frac" + frac.ToString())
        Dim minOutViatran As Integer = My.Settings.Item("minOutViatran_Frac" + frac.ToString())
        Dim maxOutViatran As Integer = My.Settings.Item("maxOutViatran_Frac" + frac.ToString())
        Dim overrideViatran As Integer = CInt(My.Settings.Item("overrideViatran_Frac" + frac.ToString()))

        'moxaSend(MoxaSendStartAddress("minInViatranStartAddress"), minInViatran, frac)
        'moxaSend(MoxaSendStartAddress("maxInViatranStartAddress"), maxInViatran, frac)
        'moxaSend(MoxaSendStartAddress("minOutViatranStartAddress"), minOutViatran, frac)
        'moxaSend(MoxaSendStartAddress("maxOutViatranStartAddress"), maxOutViatran, frac)
        'moxaSend(MoxaSendStartAddress("overrideViatranStartAddress"), overrideViatran, frac)


        ''CARGA Y ENVIO DE PARAMETROS TEMP LUB
        Dim minInTempLub As Integer = My.Settings.Item("minInTempLub_Frac" + frac.ToString())
        Dim maxInTempLub As Integer = My.Settings.Item("maxInTempLub_Frac" + frac.ToString())
        Dim minOutTempLub As Integer = My.Settings.Item("minOutTempLub_Frac" + frac.ToString())
        Dim maxOutTempLub As Integer = My.Settings.Item("maxOutTempLub_Frac" + frac.ToString())
        Dim overrideTempLub As Integer = CInt(My.Settings.Item("overrideTempLub_Frac" + frac.ToString()))

        'moxaSend(MoxaSendStartAddress("minInTempLubStartAddress"), minInTempLub, frac)
        'moxaSend(MoxaSendStartAddress("maxInTempLubStartAddress"), maxInTempLub, frac)
        'moxaSend(MoxaSendStartAddress("minOutTempLubStartAddress"), minOutTempLub, frac)
        'moxaSend(MoxaSendStartAddress("maxOutTempLubStartAddress"), maxOutTempLub, frac)
        'moxaSend(MoxaSendStartAddress("overrideTempLubStartAddress"), overrideViatran, frac)

        ''CARGA Y ENVIO DE PARAMETROS PRES LUB
        Dim minInPresLub As Integer = My.Settings.Item("minInPresLub_Frac" + frac.ToString())
        Dim maxInPresLub As Integer = My.Settings.Item("maxInPresLub_Frac" + frac.ToString())
        Dim minOutPresLub As Integer = My.Settings.Item("minOutPresLub_Frac" + frac.ToString())
        Dim maxOutPresLub As Integer = My.Settings.Item("maxOutPresLub_Frac" + frac.ToString())
        Dim overridePresLub As Integer = CInt(My.Settings.Item("overridePresLub_Frac" + frac.ToString()))

        'moxaSend(MoxaSendStartAddress("minInPresLubStartAddress"), minInPresLub, frac)
        'moxaSend(MoxaSendStartAddress("maxInPresLubStartAddress"), maxInPresLub, frac)
        'moxaSend(MoxaSendStartAddress("minOutPresLubStartAddress"), minOutPresLub, frac)
        'moxaSend(MoxaSendStartAddress("maxOutPresLubStartAddress"), maxOutPresLub, frac)
        'moxaSend(MoxaSendStartAddress("overridePresLubStartAddress"), overrideViatran, frac)

        ''CARGA Y ENVIO DE PARAMETROS SUCCION
        Dim minInPresSuc As Integer = My.Settings.Item("minInPresSuc_Frac" + frac.ToString())
        Dim maxInPresSuc As Integer = My.Settings.Item("maxInPresSuc_Frac" + frac.ToString())
        Dim minOutPresSuc As Integer = My.Settings.Item("minOutPresSuc_Frac" + frac.ToString())
        Dim maxOutPresSuc As Integer = My.Settings.Item("maxOutPresSuc_Frac" + frac.ToString())
        Dim overridePresSuc As Integer = CInt(My.Settings.Item("overridePresSuc_Frac" + frac.ToString()))

        'moxaSend(MoxaSendStartAddress("minInSuccStartAddress"), minInPresSuc, frac)
        'moxaSend(MoxaSendStartAddress("maxInSuccStartAddress"), maxInPresSuc, frac)
        'moxaSend(MoxaSendStartAddress("minOutSuccStartAddress"), minOutPresSuc, frac)
        'moxaSend(MoxaSendStartAddress("maxOutSuccStartAddress"), maxOutPresSuc, frac)
        'moxaSend(MoxaSendStartAddress("overridePresSucStartAddress"), overrideViatran, frac)

        ''CARGA Y ENVIO DE PARAMETROS PRES TRANS
        Dim minInPresTrans As Integer = My.Settings.Item("minInPresTrans_Frac" + frac.ToString())
        Dim maxInPresTrans As Integer = My.Settings.Item("maxInPresTrans_Frac" + frac.ToString())
        Dim minOutPresTras As Integer = My.Settings.Item("minOutPresTrans_Frac" + frac.ToString())
        Dim maxOutPresTrans As Integer = My.Settings.Item("maxOutPresTrans_Frac" + frac.ToString())
        Dim overridePresTrans As Integer = CInt(My.Settings.Item("overridePresTrans_Frac" + frac.ToString()))

        'moxaSend(MoxaSendStartAddress("minInPresTransStartAddress"), minInPresTrans, frac)
        'moxaSend(MoxaSendStartAddress("maxInPresTransStartAddress"), maxInPresTrans, frac)
        'moxaSend(MoxaSendStartAddress("minOutPresTrasStartAddress"), minOutPresTras, frac)
        'moxaSend(MoxaSendStartAddress("maxOutPresTransStartAddress"), maxOutPresTrans, frac)
        'moxaSend(MoxaSendStartAddress("overridePresTransStartAddress"), overrideViatran, frac)

        ''CARGA Y ENVIO DE TRIP, RELACION BOMBA, HABILITAR PROMEDIO, TMUESTREO Y NSAMPLE
        Dim TRIP As Integer = My.Settings.Item("TRIP_Frac" + frac.ToString())
        Dim rBomba As Integer = My.Settings.Item("rBomba_Frac" + frac.ToString()) * 100
        Dim sPromedio As Integer = 1
        Dim tMuestreo As Integer = My.Settings.Item("tMuestreo_Frac" + frac.ToString())
        Dim nMuestras As Integer = My.Settings.Item("nMuestras_Frac" + frac.ToString())

        'moxaSend(MoxaSendStartAddress("TRIPStartAddress"), TRIP, frac)
        'moxaSend(MoxaSendStartAddress("rBombaStartAddress"), rBomba, frac)
        'moxaSend(MoxaSendStartAddress("sPromedioStartAddress"), sPromedio, frac)
        'moxaSend(MoxaSendStartAddress("tMuestreoStartAddress"), tMuestreo, frac)
        'moxaSend(MoxaSendStartAddress("nMuestrasStartAddress"), nMuestras, frac)


        ''CARGA Y ENVIO DE PARAMETROS MOTOR EN IDLE Y MARCHA NEUTRO
        Dim RPMIdle As Integer = 1100
        Dim MarchaN As Integer = 125
        'moxaSend(MoxaSendStartAddress("RPMStartAddress"), RPMIdle, frac)
        'moxaSend(MoxaSendStartAddress("MarchaStartAddress"), MarchaN, frac)


        ''AGREGAR DATA ENABLE BLABLABLA
        ''y conectar
    End Sub

End Class
