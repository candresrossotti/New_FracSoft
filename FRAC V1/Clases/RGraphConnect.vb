﻿Imports System.IO


Public Class RGraphConnect

    Private Shared ReadOnly fileReadType As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite)
    Private Shared ReadOnly objTypeReader As New StreamReader(fileReadType)

    Private ReadOnly FGC As New FracGC
    Private ReadOnly BGC As New BlenderGC
    Private ReadOnly BhGC As New BahiaGC

    Private Shared _count As Integer
    Private Shared _type As String
    Private Shared _active As Boolean = False

    Private Shared fileReader As String()

    Private Shared Event ActiveChange()

    Private Shared Property NData() As Integer
        Get
            Return _count
        End Get
        Set(ByVal value As Integer)
            _count = value
        End Set
    End Property
    Private Shared Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property
    Private Shared Property Active() As Boolean
        Get
            Return _active
        End Get
        Set(ByVal value As Boolean)
            If value <> _active Then
                _active = value
                RaiseEvent ActiveChange()
            End If
        End Set
    End Property

    Public Sub StartRead()
        Type = objTypeReader.ReadLine()
        Select Case Type
            Case "FRAC"
                FGC.StartRead()
            Case "BAHIA"
                BhGC.StartRead()
            Case "BLENDER"
                BGC.StartRead()
        End Select
        objTypeReader.Close()
        fileReadType.Close()
    End Sub
    Public Sub StopWrite()
        Select Case Type
            Case "FRAC"
                FGC.StopRead()
            Case "BAHIA"
                BhGC.StopRead()
            Case "BLENDER"
                BGC.StopRead()
        End Select
    End Sub


    Private Class BlenderGC
        Private WithEvents FsWatcher As FileSystemWatcher
        Public Sub StartRead()
            FsWatcher = New FileSystemWatcher("C:\QMSoftware\") With {
                    .EnableRaisingEvents = True,
                    .Filter = "GraphData.graph",
                    .NotifyFilter = NotifyFilters.LastWrite
                }
        End Sub
        Public Sub StopRead()
            FsWatcher.EnableRaisingEvents = False
            FsWatcher.Dispose()
        End Sub

        Private Sub OnChanged(sender As Object, e As FileSystemEventArgs) Handles FsWatcher.Changed
            Dim fileRead As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite)
            Dim objReader As New System.IO.StreamReader(fileRead)
            objReader.ReadLine()
            Dim totalLines As Integer = 0
            Do While objReader.Peek() <> -1
                ReDim Preserve fileReader(totalLines)
                fileReader(totalLines) = objReader.ReadLine() & vbNewLine
                fileReader(totalLines) = fileReader(totalLines).Replace(vbCr, "").Replace(vbLf, "")
                totalLines += 1
            Loop

            NData = fileReader(0).Substring(0, 1)
            If Active = False And fileReader(0).Substring(2, fileReader(0).Length - 2) = "True" Then
                Active = True
            ElseIf Active = True And fileReader(0).Substring(2, fileReader(0).Length - 2) = "False" Then
                Active = False
            End If

            For i = 0 To NData - 1
                Dim FirstOne As Integer = fileReader(1).IndexOf(";")
                fileReader(1) = Replace(fileReader(1), ";", Nothing, FirstOne + 1, 1)
                Dim NextOne As Integer = fileReader(1).IndexOf(";")
                DataNombre(i) = fileReader(1).Substring(0, NextOne)
            Next

            For l = 0 To NData - 1
                Dim FirstOne As Integer = fileReader(l + 2).IndexOf(";")
                fileReader(l + 2) = Replace(fileReader(l + 2), ";", Nothing, FirstOne + 1, 1)
                Dim NextOne As Integer = fileReader(l + 2).IndexOf(";")
                DataSensor(l) = fileReader(l + 2).Substring(0, NextOne)
            Next
            objReader.Close()
        End Sub
    End Class
    Private Class FracGC
        Private WithEvents FsWatcher As FileSystemWatcher
        Public Sub StartRead()
            FsWatcher = New FileSystemWatcher("C:\QMSoftware\") With {
                    .EnableRaisingEvents = True,
                    .Filter = "GraphData.graph",
                    .NotifyFilter = NotifyFilters.LastWrite
                }
        End Sub

        Public Sub StopRead()
            FsWatcher.EnableRaisingEvents = False
            FsWatcher.Dispose()
        End Sub

        Private Sub OnChanged(sender As Object, e As FileSystemEventArgs) Handles FsWatcher.Changed

        End Sub
    End Class
    Private Class BahiaGC
        Private WithEvents FsWatcher As FileSystemWatcher
        Public Sub StartRead()
            FsWatcher = New FileSystemWatcher("C:\QMSoftware\") With {
                    .EnableRaisingEvents = True,
                    .Filter = "GraphData.graph",
                    .NotifyFilter = NotifyFilters.LastWrite
                }
        End Sub

        Public Sub StopRead()
            FsWatcher.EnableRaisingEvents = False
            FsWatcher.Dispose()
        End Sub


        Private Sub OnChanged(sender As Object, e As FileSystemEventArgs) Handles FsWatcher.Changed
            Dim fileRead As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite)
            Dim objReader As New System.IO.StreamReader(fileRead)
            objReader.ReadLine()
            Dim totalLines As Integer = 0
            Do While objReader.Peek() <> -1
                ReDim Preserve fileReader(totalLines)
                fileReader(totalLines) = objReader.ReadLine() & vbNewLine
                fileReader(totalLines) = fileReader(totalLines).Replace(vbCr, "").Replace(vbLf, "")
                totalLines += 1
            Loop

            NData = fileReader(0).Substring(0, 1)
            If Active = False And fileReader(0).Substring(2, fileReader(0).Length - 2) = "True" Then
                Active = True
            ElseIf Active = True And fileReader(0).Substring(2, fileReader(0).Length - 2) = "False" Then
                Active = False
            End If

            For i = 0 To NData - 1
                Dim FirstOne As Integer = fileReader(1).IndexOf(";")
                fileReader(1) = Replace(fileReader(1), ";", Nothing, FirstOne + 1, 1)
                Dim NextOne As Integer = fileReader(1).IndexOf(";")
                DataNombre(i) = fileReader(1).Substring(0, NextOne)
            Next

            For l = 0 To NData - 1
                Dim FirstOne As Integer = fileReader(l + 2).IndexOf(";")
                fileReader(l + 2) = Replace(fileReader(l + 2), ";", Nothing, FirstOne + 1, 1)
                Dim NextOne As Integer = fileReader(l + 2).IndexOf(";")
                DataSensor(l) = fileReader(l + 2).Substring(0, NextOne)
            Next
            objReader.Close()
        End Sub
    End Class

    Private Sub ActiveChanged() Handles MyClass.ActiveChange
        If Active = True Then
            ''TMrExcel start
            ''Set nombres
        Else
            ''TmrExcel stop
        End If
    End Sub
End Class
