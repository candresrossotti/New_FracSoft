﻿Public Class PumpSelect

    ''Variables de conversion
    Public Const rGalRev As Double = 2.75
    Public Shared rTransBomba As Double = 6.353


    Private Shared _pump As String

    Public Shared Event PumpChange()

    Public Shared Property Pump() As String
        Get
            Return _pump
        End Get
        Set(ByVal value As String)
            If value <> _pump Then
                _pump = value
                RaiseEvent PumpChange()
            End If
        End Set
    End Property


    Private Sub InitPump() Handles Me.PumpChange
        Select Case Pump
            Case Else
                rTransBomba = 6.353
        End Select
    End Sub


End Class
