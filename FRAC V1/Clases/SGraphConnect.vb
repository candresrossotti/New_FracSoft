﻿Imports System.IO
Public Class SGraphConnect

    Private ReadOnly FGC As New FracGC
    Private ReadOnly BGC As New BlenderGC
    Private ReadOnly BhGC As New BahiaGC

    Private Shared _count As Integer
    Private Shared _type As String
    Private Shared _nFrac As Integer

    Public Shared Property NData() As Integer
        Get
            Return _count
        End Get
        Set(ByVal value As Integer)
            _count = value
        End Set
    End Property
    Public Shared Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property
    Public Shared Property NFrac() As Integer
        Get
            Return _nFrac
        End Get
        Set(ByVal value As Integer)
            _nFrac = value
        End Set
    End Property

    Public Sub StartWrite()
        Select Case Type
            Case "FRAC"
                FGC.StartWrite()
            Case "BLENDER"
                BGC.StartWrite()
            Case "BAHIA"
                BhGC.StartWrite()
        End Select
    End Sub
    Public Sub StopWrite()
        Select Case Type
            Case "FRAC"
                FGC.StopWrite()
            Case "BLENDER"
                BGC.StopWrite()
            Case "BAHIA"
                BhGC.StopWrite()
        End Select
    End Sub

    Private Class BlenderGC
        Private WithEvents TmrWrite As New Timer() With {.Interval = 1000, .Enabled = False}
        Public Sub StartWrite()
            TmrWrite.Enabled = True
        End Sub
        Public Sub StopWrite()
            TmrWrite.Enabled = False
            Dim fileWrite As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            Dim objWriter As New System.IO.StreamWriter(fileWrite)
            objWriter.BaseStream.Seek(4 + Len("BLENDER"), SeekOrigin.Begin)
            objWriter.Write("False")
            objWriter.Close()
            fileWrite.Close()
        End Sub
        Dim test As Integer = 0
        Private Sub TmrWriteTick(sender As Object, e As EventArgs) Handles TmrWrite.Tick
            Dim fileWrite As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            Dim objWriter As New StreamWriter(fileWrite)
            objWriter.BaseStream.Seek(0, SeekOrigin.Begin)
            objWriter.WriteLine(Type)
            objWriter.Write(NData)
            objWriter.Write(";")
            objWriter.WriteLine("True")
            For i = 0 To NData - 1
                objWriter.Write(";" + DataNombre(i))
            Next
            objWriter.WriteLine(";")
            For j = 0 To NData - 1
                objWriter.Write(";")
                objWriter.Write(test)      ''aca en realidad va el dato posta
                objWriter.WriteLine(";")
            Next
            test += 1
            objWriter.Flush()
            objWriter.Close()
        End Sub
    End Class

    Private Class FracGC
        Private WithEvents TmrWrite As New Timer() With {.Interval = 1000, .Enabled = False}
        Public Sub StartWrite()
            TmrWrite.Enabled = True
        End Sub

        Public Sub StopWrite()
            TmrWrite.Enabled = False
            Dim fileWrite As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            Dim objWriter As New StreamWriter(fileWrite)
            objWriter.BaseStream.Seek(4 + Len("FRAC"), SeekOrigin.Begin)
            objWriter.Write("False")
            objWriter.Close()
            fileWrite.Close()
        End Sub
        Dim counter As Integer = 0
        Private Sub TmrWriteTick(sender As Object, e As EventArgs) Handles TmrWrite.Tick
            Dim fileWrite As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            Dim objWriter As New StreamWriter(fileWrite)
            objWriter.BaseStream.Seek(0, SeekOrigin.Begin)
            objWriter.WriteLine(Type)
            objWriter.WriteLine(NFrac)
            objWriter.Write(NData)
            objWriter.Write(";")
            objWriter.WriteLine("True")
            For ThisFrac = 0 To NFrac - 1
                For i = 0 To NData - 1
                    objWriter.Write(";" + DataNombre(i + ThisFrac * NData))
                Next
                objWriter.WriteLine(";")
                For j = 0 To NData - 1
                    objWriter.Write(";")
                    objWriter.Write(j + counter)      ''aca en realidad va el dato posta
                    objWriter.WriteLine(";")
                Next
                counter += 1
            Next
            objWriter.Flush()
            objWriter.Close()
        End Sub
    End Class
    Private Class BahiaGC
        Private WithEvents TmrWrite As New Timer() With {.Interval = 1000, .Enabled = False}
        Public Sub StartWrite()
            TmrWrite.Enabled = True
        End Sub
        Public Sub StopWrite()
            TmrWrite.Enabled = False
            Dim fileWrite As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            Dim objWriter As New StreamWriter(fileWrite)
            objWriter.BaseStream.Seek(4 + Len("BAHIA"), SeekOrigin.Begin)
            objWriter.Write("False")
            objWriter.Close()
            fileWrite.Close()
        End Sub
        Private Sub TmrWriteTick(sender As Object, e As EventArgs) Handles TmrWrite.Tick
            Dim fileWrite As FileStream = File.Open("C:\QMSoftware\GraphData.graph", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            Dim objWriter As New StreamWriter(fileWrite)
            objWriter.BaseStream.Seek(0, SeekOrigin.Begin)
            objWriter.WriteLine(Type)
            objWriter.Write(NData)
            objWriter.Write(";")
            objWriter.WriteLine("True")
            For i = 0 To NData - 1
                objWriter.Write(";" + DataNombre(i))
            Next
            objWriter.WriteLine(";")
            For j = 0 To NData - 1
                objWriter.Write(";")
                objWriter.Write(j)      ''aca en realidad va el dato posta
                objWriter.WriteLine(";")
            Next
            objWriter.Flush()
            objWriter.Close()
        End Sub
    End Class
End Class
