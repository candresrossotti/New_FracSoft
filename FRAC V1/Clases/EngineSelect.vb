﻿Public Class EngineSelect

    Private Shared _engine As String
    Public Shared Event EngineChange()
    Public Shared SelectedEngine As New Dictionary(Of String, Double)() From {}

    Private ReadOnly TS As New TransSelect

    Public Shared Property Engine() As String
        Get
            Return _engine
        End Get
        Set(ByVal value As String)
            If value <> _engine Then
                _engine = value
                'RaiseEvent EngineChange()
            End If
        End Set
    End Property


    ''' <summary>
    ''' Funcion para inicializar el motor.
    ''' </summary>
    Private Sub InitEngine() Handles MyClass.EngineChange
        Select Case Engine
            Case "LiebherrD9620A7"
                SelectedEngine = LiebherrD9620A7
        End Select

        For i = 1 To 7
            SelectedEngine.Add("PresionMax" + (i).ToString(), 40.8 * SelectedEngine("EfficientHP") / (1300 * PumpSelect.rGalRev / (42 * TransSelect.SelectedTransmisison("RelacionGear" + i.ToString()) * 6.353)))
            SelectedEngine.Add("PresionMin" + (i).ToString(), 40.8 * SelectedEngine("EfficientHP") / (1900 * PumpSelect.rGalRev / (42 * TransSelect.SelectedTransmisison("RelacionGear" + i.ToString()) * 6.353)))
        Next
    End Sub

    Private ReadOnly LiebherrD9620A7 As New Dictionary(Of String, Double)() From {
        {"EfficientRPM", 1800},
        {"EfficientHP", 2250 * 0.7},
        {"MaxHP", 2250}
    }


End Class
