﻿Public Class NAutomaticFrac

    Private WithEvents TmrNStart As New Timer() With {.Interval = 1000, .Enabled = False}
    Private ReadOnly NStopWatch As New Stopwatch

    Public Shared Event EtapaChange()
    Public Shared Event AutomationStart()

    Private Const rGalRev As Double = PumpSelect.rGalRev
    Private Shared rTransBomba As Double = PumpSelect.rTransBomba
    Private Shared SelectedEngine As Dictionary(Of String, Double) = EngineSelect.SelectedEngine
    Private Shared SelectedTransmission As Dictionary(Of String, Double) = TransSelect.SelectedTransmisison

    ''----------------------------------------------------------------------------------------------------------''
    ''---------------------------------      PROPIEDADES PARA TODA LA CLASE     --------------------------------''
    ''----------------------------------------------------------------------------------------------------------''

    ''Propiedades para las funciones de inicio de automatizacion

    Private Shared _totaletapas As Integer
    Private Shared _etapaactualtimer As Integer
    Private Shared _duracionetapa As Decimal()
    Private Shared _nrpm As Double()
    Private Shared _nfracset As Integer()
    Private Shared _nmarcha As Integer()()
    Private Shared _duraciontotal As Decimal
    Private Shared _autostart As Boolean = False

    Public Shared Property TotalEtapas() As Integer
        Get
            Return _totaletapas
        End Get
        Set(ByVal value As Integer)
            If value <> _totaletapas Then
                _totaletapas = value
            End If
        End Set
    End Property
    Public Shared Property EtapaActualTimer() As Integer
        Get
            Return _etapaactualtimer
        End Get
        Set(ByVal value As Integer)
            If value <> _etapaactualtimer Then
                _etapaactualtimer = value
                RaiseEvent EtapaChange()
            End If
        End Set
    End Property
    Public Shared Property DuracionTotal() As Decimal
        Get
            Return _duraciontotal
        End Get
        Set(ByVal value As Decimal)
            If value <> _duraciontotal Then
                _duraciontotal = value
            End If
        End Set
    End Property
    Public Shared Property DuracionEtapa() As Decimal()
        Get
            Return _duracionetapa
        End Get
        Set(ByVal value As Decimal())
            If value IsNot _duracionetapa Then
                _duracionetapa = value
            End If
        End Set
    End Property
    Public Shared Property NRPM() As Double()
        Get
            Return _nrpm
        End Get
        Set(ByVal value As Double())
            If value IsNot _nrpm Then
                _nrpm = value
            End If
        End Set
    End Property
    Public Shared Property NMarcha() As Integer()()
        Get
            Return _nmarcha
        End Get
        Set(ByVal value As Integer()())
            If value IsNot _nmarcha Then
                _nmarcha = value
            End If
        End Set
    End Property
    Public Shared Property NFracSet() As Integer()
        Get
            Return _nfracset
        End Get
        Set(ByVal value As Integer())
            If value IsNot _nfracset Then
                _nfracset = value
            End If
        End Set
    End Property
    Public Shared Property AutoStart() As Boolean
        Get
            Return _autostart
        End Get
        Set(ByVal value As Boolean)
            If value <> _autostart Then
                _autostart = value
                RaiseEvent AutomationStart()
            End If
        End Set
    End Property
    ''Propiedades para el calculo de rpm, marcha, carga, etc

    Private Shared _thisrpm As Integer
    Private Shared _thiscaudal As Double
    Private Shared _thismarcha As Integer()
    Private Shared _cargapromedio As Double
    Private Shared _presiondeseada As Double
    Private Shared _observaciones As String
    Private Shared _ncarga As Double()
    Private Shared _lastvaluedouble As Double()

    Public Shared Property ThisRPM() As Integer
        Get
            Return _thisrpm
        End Get
        Set(ByVal value As Integer)
            If value <> _thisrpm Then
                _thisrpm = value
            End If
        End Set
    End Property
    Public Shared Property ThisCaudal() As Double
        Get
            Return _thiscaudal
        End Get
        Set(ByVal value As Double)
            If value <> _thiscaudal Then
                _thiscaudal = value
            End If
        End Set
    End Property
    Public Shared Property ThisMarcha() As Integer()
        Get
            Return _thismarcha
        End Get
        Set(ByVal value As Integer())
            If value IsNot _thismarcha Then
                _thismarcha = value
            End If
        End Set
    End Property
    Public Shared Property CargaPromedio() As Double
        Get
            Return _cargapromedio
        End Get
        Set(ByVal value As Double)
            If value <> _cargapromedio Then
                _cargapromedio = value
            End If
        End Set
    End Property
    Public Shared Property PresionDeseada() As Double
        Get
            Return _presiondeseada
        End Get
        Set(ByVal value As Double)
            If value <> _presiondeseada Then
                _presiondeseada = value
            End If
        End Set
    End Property
    Public Shared Property NCarga() As Double()
        Get
            Return _ncarga
        End Get
        Set(ByVal value As Double())
            If value IsNot _ncarga Then
                _ncarga = value
            End If
        End Set
    End Property
    Public Shared Property Observaciones() As String
        Get
            Return _observaciones
        End Get
        Set(ByVal value As String)
            If value <> _observaciones Then
                _observaciones = value
            End If
        End Set
    End Property
    Public Shared Property LastValueDouble() As Double()
        Get
            Return _lastvaluedouble
        End Get
        Set(ByVal value As Double())
            If value IsNot _lastvaluedouble Then
                _lastvaluedouble = value
            End If
        End Set
    End Property

    ''----------------------------------------------------------------------------------------------------------''
    ''--------------------------          FUNCIONES PARA EL CALCULO DE N FRACS         -------------------------''
    ''----------------------------------------------------------------------------------------------------------''

    ''' <summary>
    ''' Funcion para actualizar las variables de la clase.
    ''' </summary>
    Public Sub Update()
        rTransBomba = PumpSelect.rTransBomba
        SelectedEngine = EngineSelect.SelectedEngine
        SelectedTransmission = TransSelect.SelectedTransmisison
    End Sub


    Public Function GetNSettings(ByVal nFrac As Integer, ByVal CaudalDeseado As Decimal, ByVal PresionDeseadaSet As Double, ByVal Tolerancia As Decimal) As Object
        Try

            Dim DataSet As Double()
            Dim ThisIndex As Integer
            Dim PowerRecursiveStage As Double = 1

            Dim MinGearCond As Integer = 1
            Dim MaxGearCond As Integer = 7

            ReDim ThisMarcha(nFrac - 1)
            ReDim DataSet(6)

            ThisRPM = SelectedEngine("EfficientRPM")
            PresionDeseada = PresionDeseadaSet
            ThisCaudal = 0
            NCarga = {0}

            For currentGear = 1 To 7
                If PresionDeseada < SelectedEngine("PresionMin" + currentGear.ToString()) Then
                    MinGearCond += 1
                End If
                If PresionDeseada > SelectedEngine("PresionMax" + currentGear.ToString()) Then
                    MaxGearCond -= 1
                End If
            Next

            Dim CaudalMinimo As Double = nFrac * (1300 * rGalRev / (42 * SelectedTransmission("RelacionGear" + MinGearCond.ToString()) * rTransBomba))
            Dim CaudalMaximo As Double = nFrac * (1900 * rGalRev / (42 * SelectedTransmission("RelacionGear" + MaxGearCond.ToString()) * rTransBomba))

            If CaudalDeseado < CaudalMinimo Then
                Return "El caudal minimo generado por " + nFrac.ToString() + " fracturadores a " + PresionDeseada.ToString() + " PSI es de: " + CaudalMinimo.ToString("F2") + " [BBL/MIN]"
            ElseIf CaudalDeseado > CaudalMaximo Then
                Return "El caudal maximo generado por " + nFrac.ToString() + " fracturadores a " + PresionDeseada.ToString() + " PSI es de: " + CaudalMaximo.ToString("F2") + " [BBL/MIN]"
            ElseIf ((CaudalDeseado * PresionDeseada) / 40.8) > (nFrac * SelectedEngine("MaxHP")) Then
                Return "La potencia necesitada es de " + ((CaudalDeseado * PresionDeseada) / 40.8).ToString("F2") + " HP, pero solo se cuenta con " + (nFrac * SelectedEngine("MaxHP")).ToString() + " HP"
            End If

            While (NCarga.Max > 70 Or NCarga.Min < 50)
                While (Math.Abs(CaudalDeseado - ThisCaudal) > Tolerancia)
                    For i = MinGearCond To MaxGearCond
                        DataSet(i - 1) = ThisRPM * rGalRev / (42 * SelectedTransmission("RelacionGear" + i.ToString()) * rTransBomba)
                    Next
                    ThisCaudal = GetNearestCaudal(nFrac, DataSet, CaudalDeseado, MinGearCond, MaxGearCond)
                    If CaudalDeseado - ThisCaudal > Tolerancia Then
                        ThisRPM += (CaudalDeseado - ThisCaudal) * 42 * rTransBomba * SelectedTransmission("RelacionGear" + MaxGearCond.ToString()) / (rGalRev * nFrac)
                        If ThisRPM >= 1900 Then
                            Exit While
                        End If
                    ElseIf CaudalDeseado - ThisCaudal < -Tolerancia Then
                        ThisRPM += (CaudalDeseado - ThisCaudal) * 42 * rTransBomba * SelectedTransmission("RelacionGear" + MinGearCond.ToString()) / (rGalRev * nFrac)
                        If ThisRPM <= 1300 Then
                            Exit While
                        End If
                    End If
                End While

                ThisIndex = GetClosestByIndex(LastValueDouble, CaudalDeseado)
                For i = 0 To nFrac - 1
                    If (ThisIndex - 2 ^ i) >= 0 Then
                        ThisMarcha(i) = MaxGearCond
                        ThisIndex -= 2 ^ i
                    Else
                        ThisMarcha(i) = MinGearCond
                    End If
                Next

                GetNCarga()

                CargaPromedio = ((CaudalDeseado * PresionDeseada) / 40.8 / nFrac / 2250 * 100)
                If CargaPromedio > 70 Or CargaPromedio < 50 Then
                    Exit While
                End If

                If NCarga.Max > 70 Then
                    ThisRPM -= 100 * PowerRecursiveStage
                    PowerRecursiveStage += 1
                    ThisCaudal = 0
                    ReDim ThisMarcha(nFrac - 1)
                ElseIf NCarga.Min < 50 Then
                    ThisRPM += 100 * PowerRecursiveStage
                    PowerRecursiveStage += 1
                    ThisCaudal = 0
                    ReDim ThisMarcha(nFrac - 1)
                End If

            End While
            GetNObserv()
            Return True
        Catch ex As Exception
            avisoStr = "Se produjo un error inesperado"
            avisoTitulo = "Error"
            AvisoGeneral.OpenDialog()
            Return False
        End Try
    End Function
    Public Function Get1RPM() As Integer
        Return ThisRPM
    End Function

    Public Function Get1Marcha() As Integer
        Return ThisMarcha(0)
    End Function

    Public Function GetRPMEficiente() As Double
        Dim RPMEficiente As Double
        RPMEficiente = SelectedEngine("EfficientRPM") / SelectedTransmission("RelacionGear" + ThisMarcha(0).ToString()) / rTransBomba / 42 * rGalRev
        Return RPMEficiente
    End Function

    Public Function GetNCaudal() As Double()
        Dim CurrentCaudal As Double()
        ReDim CurrentCaudal(ThisMarcha.Length() - 1)
        For i = 0 To ThisMarcha.Length() - 1
            CurrentCaudal(i) = (ThisRPM * rGalRev / (42 * SelectedTransmission("RelacionGear" + ThisMarcha(i).ToString()) * rTransBomba))
        Next
        Return CurrentCaudal
    End Function

    Private Sub GetNCarga()
        ReDim NCarga(ThisMarcha.Length() - 1)
        For i = 0 To ThisMarcha.Length() - 1
            NCarga(i) = ThisRPM * rGalRev / (42 * SelectedTransmission("RelacionGear" + ThisMarcha(i).ToString()) * rTransBomba)
            NCarga(i) = (NCarga(i) * PresionDeseada) / 40.8 / SelectedEngine("MaxHP") * 100
        Next
    End Sub

    Private Sub GetNObserv()
        If NCarga.Max > 70 Then
            Observaciones = "La carga del motor " + (GetClosestByIndex(NCarga, 100) + 1).ToString("F0") + " esta en el " + NCarga.Max.ToString("F1") + " %, evalue agregar un fracturador."
        ElseIf NCarga.Min < 50 Then
            Observaciones = "La carga del motor " + (GetClosestByIndex(NCarga, 0) + 1).ToString("F0") + " esta en el " + NCarga.Min.ToString("F1") + " %, evalue eliminar un fracturador."
        End If
    End Sub

    Private Function GetNearestCaudal(nFrac As Integer, DataSet As Double(), CaudalDeseado As Decimal, MinGearCond As Integer, MaxGearCond As Integer) As Double
        Dim Caudal
        Dim LastValue As New Object
        Using TryCast(LastValue, IDisposable)
            Dim currentSum As Integer = 0
            Dim items As Double()()
            Dim ItemDataSet2 As Double()
            Dim ItemDataSet1 As Double()
            Dim ItemDataSet As Double()

            ReDim ItemDataSet1(MaxGearCond - 1)
            ReDim ItemDataSet(MaxGearCond - MinGearCond)

            ItemDataSet2 = New Double() {DataSet(0), DataSet(1), DataSet(2), DataSet(3), DataSet(4), DataSet(5), DataSet(6)}

            Array.Copy(ItemDataSet2, ItemDataSet1, MaxGearCond)
            ItemDataSet1 = ItemDataSet1.Reverse.ToArray()
            Array.Copy(ItemDataSet1, ItemDataSet, MaxGearCond - (MinGearCond - 1))
            ItemDataSet = ItemDataSet.Reverse().ToArray()

            ReDim LastValueDouble((ItemDataSet.Length() ^ nFrac) - 1)
            ReDim LastValue((ItemDataSet.Length() ^ nFrac) - 1)

            items = New Double()() {ItemDataSet}
            For i = 1 To nFrac - 1
                ReDim Preserve items(i)
                ReDim items(i)(ItemDataSet.Length() - 1)
                items(i - 1).CopyTo(items(i), 0)
            Next

            Dim RecursiveStage As IEnumerable(Of IEnumerable(Of Double)) = CartesianProduct(items)
            For Each TreeNode As IEnumerable(Of Double) In RecursiveStage
                For Each CurrentItem As Double In TreeNode
                    LastValue(currentSum) += CurrentItem
                Next
                currentSum += 1
            Next

            Array.Copy(LastValue, LastValueDouble, LastValue.length)
            Caudal = GetClosestByValue(LastValueDouble, CaudalDeseado)
        End Using
        Return Caudal
    End Function
    Public Function CartesianProduct(Of T)(
        ByVal sequences As IEnumerable(Of IEnumerable(Of T))) _
        As IEnumerable(Of IEnumerable(Of T))
        Dim emptyProduct As IEnumerable(Of IEnumerable(Of T)) =
            New IEnumerable(Of T)() {Enumerable.Empty(Of T)()}
        Return sequences.Aggregate(
            emptyProduct,
            Function(accumulator, sequence)
                Return (From accseq In accumulator
                        From item In sequence
                        Select accseq.Concat(New T() {item}))
            End Function)
    End Function

    ''----------------------------------------------------------------------------------------------------------''
    ''------------------------          FIN FUNCIONES PARA EL CALCULO DE N FRACS         -----------------------''
    ''----------------------------------------------------------------------------------------------------------''



    ''----------------------------------------------------------------------------------------------------------''
    ''------------------------      FUNCIONES PARA EL COMIENZO DE LA AUTOMATIZACION     ------------------------''
    ''----------------------------------------------------------------------------------------------------------''
    Private Sub NTmrStartTick(sender As Object, e As EventArgs) Handles TmrNStart.Tick
        Dim ThisTime As Decimal = NStopWatch.ElapsedTicks / Stopwatch.Frequency
        If ThisTime >= DuracionEtapa(EtapaActualTimer - 1) * 60 Then
            EtapaActualTimer += 1
            If EtapaActualTimer = TotalEtapas + 1 Then
                TmrNStart.Enabled = False
                NStopWatch.Stop()
                AutoStart = False
                Exit Sub
            End If
            For thisFrac = 0 To NFracSet(EtapaActualTimer)
                ''moxaSend(MoxaSendStartAddress("BrakeStartAddress"), FracGeneralValues("BrakeOFF"), fracID(thisFrac))
                ''moxaSend(MoxaSendStartAddress("RPMStartAddress"), NRPM(EtapaActualTimer), fracID(thisFrac))
                ''moxaSend(MoxaSendStartAddress("MarchaStartAddress"), NMarcha(EtapaActualTimer)(thisFrac), fracID(thisFrac))     ''REVISAR EN FUNCIONAMIENTO
            Next
            NStopWatch.Restart()
        End If
    End Sub
    Public Sub NStart(ByVal Etapas As Integer, ByVal Caudal As Double(), ByVal Barriles As Double(), ByVal RPM As Double(), ByVal Marcha As Integer()(), ByVal FracSet As Integer())
        TotalEtapas = Etapas
        ReDim NRPM(TotalEtapas - 1)
        ReDim NMarcha(TotalEtapas - 1)
        ReDim DuracionEtapa(TotalEtapas - 1)
        NFracSet = FracSet
        NRPM = RPM
        NMarcha = Marcha
        For EtapaActual = 0 To TotalEtapas - 1
            DuracionEtapa(EtapaActual) = Barriles(EtapaActual) / Caudal(EtapaActual)
            DuracionTotal += DuracionEtapa(EtapaActual)     ''En MIN
        Next
        EtapaActualTimer = 1
        TmrNStart.Enabled = True
        NStopWatch.Start()
        AutoStart = True

        ''moxaSend(MoxaSendStartAddress("BrakeStartAddress"), FracGeneralValues("BrakeOFF"), fracID(thisFrac))
        ''moxaSend(MoxaSendStartAddress("RPMStartAddress"), NRPM(EtapaActualTimer), fracID(thisFrac))
        ''moxaSend(MoxaSendStartAddress("MarchaStartAddress"), NMarcha(EtapaActualTimer)(thisFrac), fracID(thisFrac))     ''ENVIAMOS PARAMETROS DE PRIMERA ETAPA

    End Sub

    ''----------------------------------------------------------------------------------------------------------''
    ''------------------------------      FIN DE FUNCIONES PARA AUTOMATIZACION     -----------------------------''
    ''----------------------------------------------------------------------------------------------------------''


End Class
