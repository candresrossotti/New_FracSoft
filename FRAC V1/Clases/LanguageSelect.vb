﻿Imports System.Resources

Public Class LanguageSelect


    Private Shared _dynamiclanguagerm As ResourceManager
    Public Shared Property DynamicLanguageRM As ResourceManager
        Get
            Return _dynamiclanguagerm
        End Get
        Set(value As ResourceManager)
            If value IsNot _dynamiclanguagerm Then
                _dynamiclanguagerm = value
            End If
        End Set
    End Property
    Private Shared _languagerm As ResourceManager
    Public Shared Property LanguageRM() As ResourceManager
        Get
            Return _languagerm
        End Get
        Set(value As ResourceManager)
            If value IsNot _languagerm Then
                _languagerm = value
            End If
        End Set
    End Property


    Private Shared _currentlanguage As String
    Public Shared Event LanguageChange()

    Public Shared Property CurrentLanguage() As String
        Get
            Return _currentlanguage
        End Get
        Set(value As String)
            If value <> _currentlanguage Then
                _currentlanguage = value
                RaiseEvent LanguageChange()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Funcion para definir un idioma.
    ''' </summary>
    Public Sub SetLanguage(ByVal Language As String)
        CurrentLanguage = Language
    End Sub

    Private Sub LanguageSet() Handles MyClass.LanguageChange
        Select Case CurrentLanguage
            Case "Ingles"
                LanguageRM = My.Resources.English.ResourceManager
                DynamicLanguageRM = My.Resources.DynamicEnglish.ResourceManager
            Case "Español"
                LanguageRM = My.Resources.Español.ResourceManager
                DynamicLanguageRM = My.Resources.DynamicEspañol.ResourceManager
        End Select
        InterfaceUpdate()
    End Sub

    Private Sub RightAlign(ByVal sender As Object, ByVal Z As Integer)
        sender.Left = Z - sender.Width
    End Sub

    Private Sub InterfaceUpdate()

        ''Principal
        Principal.Title.Text = LanguageRM.GetString("Principal.Title")

        ''Frac Select
        FracSelect.Title.Text = LanguageRM.GetString("FracSelect.Title")

        ''Ajustes Fracturador
        AjustesFracturador.Title.Text = LanguageRM.GetString("AjustesFracturador.Title")
        AjustesFracturador.CbCentrifuga.Text = LanguageRM.GetString("AjustesFracturador.CbCentrifuga")
        AjustesFracturador.LblCentrifuga.Text = LanguageRM.GetString("AjustesFracturador.LblCentrifuga")
        AjustesFracturador.LblCentrifugaD.Text = LanguageRM.GetString("AjustesFracturador.LblCentrifugaD")
        AjustesFracturador.LblMaximoEntradaD.Text = LanguageRM.GetString("AjustesFracturador.LblMaximoEntradaD")
        AjustesFracturador.LblMaximoSalidaD.Text = LanguageRM.GetString("AjustesFracturador.LblMaximoSalidaD")
        AjustesFracturador.LblMinimoEntradaD.Text = LanguageRM.GetString("AjustesFracturador.LblMinimoEntradaD")
        AjustesFracturador.LblMinimoSalidaD.Text = LanguageRM.GetString("AjustesFracturador.LblMinimoSalidaD")
        AjustesFracturador.LblPeriodoD.Text = LanguageRM.GetString("AjustesFracturador.LblPeriodoD")
        AjustesFracturador.LblSensores.Text = LanguageRM.GetString("AjustesFracturador.LblSensores")
        AjustesFracturador.LblSensoresD.Text = LanguageRM.GetString("AjustesFracturador.LblSensoresD")
        AjustesFracturador.LblTimersD.Text = LanguageRM.GetString("AjustesFracturador.LblTimersD")
        AjustesFracturador.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        AjustesFracturador.BtnAdmin.Text = LanguageRM.GetString("AjustesFracturador.BtnAdmin")
        AjustesFracturador.BtnReset.Text = LanguageRM.GetString("AjustesFracturador.BtnReset")

        AjustesFracturador.CbSensores.Items.Clear()
        AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[0]"))
        AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[1]"))
        AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[2]"))
        AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[3]"))
        AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[4]"))

        AjustesFracturador.CbTimers.Items.Clear()
        AjustesFracturador.CbTimers.Items.Add(LanguageRM.GetString("AjustesFracturador.CbTimers[0]"))
        AjustesFracturador.CbTimers.Items.Add(LanguageRM.GetString("AjustesFracturador.CbTimers[1]"))

        RightAlign(AjustesFracturador.LblSensoresD, 164)
        RightAlign(AjustesFracturador.LblMaximoEntradaD, 164)
        RightAlign(AjustesFracturador.LblMaximoSalidaD, 164)
        RightAlign(AjustesFracturador.LblMinimoEntradaD, 164)
        RightAlign(AjustesFracturador.LblMinimoSalidaD, 164)

        ''Frac Errores
        FracErrores.Title.Text = LanguageRM.GetString("FracErrores.Title")
        FracErrores.LblInfo.Text = LanguageRM.GetString("FracErrores.LblInfo")
        FracErrores.LblErroresTotales.Text = LanguageRM.GetString("FracErrores.LblErroresTotales")
        FracErrores.LblProcedenciaD.Text = LanguageRM.GetString("FracErrores.LblProcedenciaD")
        FracErrores.LblActivoD.Text = LanguageRM.GetString("FracErrores.LblActivoD")
        FracErrores.LblUnidadD.Text = LanguageRM.GetString("FracErrores.LblUnidadD")
        FracErrores.LblDescripcionD.Text = LanguageRM.GetString("FracErrores.LblDescripcionD")
        FracErrores.BtnAnterior.Text = LanguageRM.GetString("FracErrores.BtnAnterior")
        FracErrores.BtnSiguiente.Text = LanguageRM.GetString("FracErrores.BtnSiguiente")
        FracErrores.BtnBorrarActual.Text = LanguageRM.GetString("FracErrores.BtnBorrarActual")
        FracErrores.BtnBorrarTodo.Text = LanguageRM.GetString("FracErrores.BtnBorrarTodo")

        RightAlign(FracErrores.LblErroresTotales, 599)
        RightAlign(FracErrores.LblProcedenciaD, 115)
        RightAlign(FracErrores.LblActivoD, 360)
        RightAlign(FracErrores.LblUnidadD, 550)
        RightAlign(FracErrores.LblDescripcionD, 112)

        ''SetFC
        SetFC.Title.Text = LanguageRM.GetString("SetFC.Title")
        SetFC.LblTB.Text = LanguageRM.GetString("SetFC.LblTB")
        SetFC.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        RightAlign(SetFC.LblTB, 188)

        ''SetGEAR
        SetGEAR.Title.Text = LanguageRM.GetString("SetGEAR.Title")
        SetGEAR.LblTB.Text = LanguageRM.GetString("SetGEAR.LblTB")
        SetGEAR.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        RightAlign(SetGEAR.LblTB, 195)

        ''SetRPM
        SetRPM.Title.Text = LanguageRM.GetString("SetRPM.Title")
        SetRPM.LblTB.Text = LanguageRM.GetString("SetRPM.LblTB")
        SetRPM.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        RightAlign(SetRPM.LblTB, 208)

        ''SetTRIP
        SetTRIP.Title.Text = LanguageRM.GetString("SetTRIP.Title")
        SetTRIP.LblTB.Text = LanguageRM.GetString("SetTRIP.LblTB")
        SetTRIP.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        RightAlign(SetTRIP.LblTB, 204)

        ''SetZERO
        SetZERO.Title.Text = LanguageRM.GetString("SetZERO.Title")
        SetZERO.LblTB.Text = LanguageRM.GetString("SetZERO.LblTB")
        SetZERO.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        RightAlign(SetZERO.LblTB, 204)

        ''Admin Control
        AdminControl.Title.Text = LanguageRM.GetString("AdminControl.Title")
        AdminControl.LblTB.Text = LanguageRM.GetString("AdminControl.LblTB")
        AdminControl.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        RightAlign(AdminControl.LblTB, 187)

        ''New Password
        NewPassword.Title.Text = LanguageRM.GetString("NewPassword.Title")
        NewPassword.LblTB.Text = LanguageRM.GetString("NewPassword.LblTB")
        NewPassword.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        RightAlign(NewPassword.LblTB, 206)

        ''FracMantenimiento
        FracMantenimiento.Title.Text = LanguageRM.GetString("FracMantenimiento.Title")
        FracMantenimiento.LblTB.Text = LanguageRM.GetString("FracMantenimiento.LblTB")
        FracMantenimiento.CbPassMem.Text = LanguageRM.GetString("FracMantenimiento.CbPassMem")
        FracMantenimiento.CbMant.Text = LanguageRM.GetString("FracMantenimiento.CbMant")
        FracMantenimiento.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        RightAlign(FracMantenimiento.LblTB, 165)

        ''FracDiagnostico
        FracDiagnostico.LblMantD.Text = LanguageRM.GetString("FracDiagnostico.LblMantD")
        FracDiagnostico.LblMCombustible.Text = LanguageRM.GetString("FracDiagnostico.LblMCombustible")
        FracDiagnostico.LblMHoras.Text = LanguageRM.GetString("FracDiagnostico.LblMHoras")
        FracDiagnostico.LblMPresAceite.Text = LanguageRM.GetString("FracDiagnostico.LblMPresAceite")
        FracDiagnostico.LblMTempAceite.Text = LanguageRM.GetString("FracDiagnostico.LblMTempAceite")
        FracDiagnostico.LblMTempAgua.Text = LanguageRM.GetString("FracDiagnostico.LblMTempAgua")
        FracDiagnostico.LblSPresDescarga.Text = LanguageRM.GetString("FracDiagnostico.LblSPresDescarga")
        FracDiagnostico.LblSPresLub.Text = LanguageRM.GetString("FracDiagnostico.LblSPresLub")
        FracDiagnostico.LblSPresSuccion.Text = LanguageRM.GetString("FracDiagnostico.LblSPresSuccion")
        FracDiagnostico.LblSTempLub.Text = LanguageRM.GetString("FracDiagnostico.LblSTempLub")
        FracDiagnostico.LblTareas1.Text = LanguageRM.GetString("FracDiagnostico.LblTareas1")
        FracDiagnostico.LblTareas10.Text = LanguageRM.GetString("FracDiagnostico.LblTareas10")
        FracDiagnostico.LblTareas11.Text = LanguageRM.GetString("FracDiagnostico.LblTareas11")
        FracDiagnostico.LblTareas2.Text = LanguageRM.GetString("FracDiagnostico.LblTareas2")
        FracDiagnostico.LblTareas3.Text = LanguageRM.GetString("FracDiagnostico.LblTareas3")
        FracDiagnostico.LblTareas4.Text = LanguageRM.GetString("FracDiagnostico.LblTareas4")
        FracDiagnostico.LblTareas5.Text = LanguageRM.GetString("FracDiagnostico.LblTareas5")
        FracDiagnostico.LblTareas6.Text = LanguageRM.GetString("FracDiagnostico.LblTareas6")
        FracDiagnostico.LblTareas7.Text = LanguageRM.GetString("FracDiagnostico.LblTareas7")
        FracDiagnostico.LblTareas8.Text = LanguageRM.GetString("FracDiagnostico.LblTareas8")
        FracDiagnostico.LblTareas9.Text = LanguageRM.GetString("FracDiagnostico.LblTareas9")
        FracDiagnostico.LblTPresAceite.Text = LanguageRM.GetString("FracDiagnostico.LblTPresAceite")
        FracDiagnostico.LblTTempAceite.Text = LanguageRM.GetString("FracDiagnostico.LblTTempAceite")
        FracDiagnostico.LblUnidadD.Text = LanguageRM.GetString("FracDiagnostico.LblUnidadD")
        FracDiagnostico.Title.Text = LanguageRM.GetString("FracDiagnostico.Title")
        FracDiagnostico.LblSensores.Text = LanguageRM.GetString("FracDiagnostico.LblSensores")
        FracDiagnostico.LblMotor.Text = LanguageRM.GetString("FracDiagnostico.LblMotor")
        FracDiagnostico.LblTransmision.Text = LanguageRM.GetString("FracDiagnostico.LblTransmision")
        FracDiagnostico.LblTarea.Text = LanguageRM.GetString("FracDiagnostico.LblTarea")

        ''FracAutomatico
        FracAutomatico.Title.Text = LanguageRM.GetString("FracAutomatico.Title")
        FracAutomatico.BtnMarchaRecomendada.Text = LanguageRM.GetString("FracAutomatico.BtnMarchaRecomendada")
        FracAutomatico.BtnTesteoLinea.Text = LanguageRM.GetString("FracAutomatico.BtnTesteoLinea")
        FracAutomatico.BtnFracProgramada.Text = LanguageRM.GetString("FracAutomatico.BtnFracProgramada")

        ''FracturaAutomatica
        FracturaAutomatica.Title.Text = LanguageRM.GetString("FracturaAutomatica.Title")
        FracturaAutomatica.LblFA1.Text = LanguageRM.GetString("FracturaAutomatica.LblFA1")
        FracturaAutomatica.LblFA2.Text = LanguageRM.GetString("FracturaAutomatica.LblFA2")
        FracturaAutomatica.LblFA3.Text = LanguageRM.GetString("FracturaAutomatica.LblFA3")
        FracturaAutomatica.LblPaso.Text = LanguageRM.GetString("FracturaAutomatica.LblPaso")
        FracturaAutomatica.BtnAnterior.Text = LanguageRM.GetString("FracturaAutomatica.BtnAnterior")
        FracturaAutomatica.BtnStart.Text = LanguageRM.GetString("FracturaAutomatica.BtnStart")
        FracturaAutomatica.BtnSiguiente.Text = LanguageRM.GetString("FracturaAutomatica.BtnSiguiente")

        ''MarchaRecomendada
        MarchaRecomendada.Title.Text = LanguageRM.GetString("MarchaRecomendada.Title")
        MarchaRecomendada.LblMR1.Text = LanguageRM.GetString("MarchaRecomendada.LblMR1")
        MarchaRecomendada.LblMR2.Text = LanguageRM.GetString("MarchaRecomendada.LblMR2")
        MarchaRecomendada.LblMR3.Text = LanguageRM.GetString("MarchaRecomendada.LblMR3")
        MarchaRecomendada.LblMR4.Text = LanguageRM.GetString("MarchaRecomendada.LblMR4")
        MarchaRecomendada.LblMR5.Text = LanguageRM.GetString("MarchaRecomendada.LblMR5")
        MarchaRecomendada.BtnMarchaRecomendada.Text = LanguageRM.GetString("MarchaRecomendada.BtnMarchaRecomendada")

        ''TesteoLinea
        TesteoLinea.Title.Text = LanguageRM.GetString("TesteoLinea.Title")
        TesteoLinea.LblAjustes.Text = LanguageRM.GetString("TesteoLinea.LblAjustes")
        TesteoLinea.LblTL1.Text = LanguageRM.GetString("TesteoLinea.LblTL1")
        TesteoLinea.LblTL2.Text = LanguageRM.GetString("TesteoLinea.LblTL2")
        TesteoLinea.LblTL3.Text = LanguageRM.GetString("TesteoLinea.LblTL3")
        TesteoLinea.LblTL4.Text = LanguageRM.GetString("TesteoLinea.LblTL4")
        TesteoLinea.LblTL5.Text = LanguageRM.GetString("TesteoLinea.LblTL5")
        TesteoLinea.LblTL6.Text = LanguageRM.GetString("TesteoLinea.LblTL6")
        TesteoLinea.LblTL7.Text = LanguageRM.GetString("TesteoLinea.LblTL7")
        TesteoLinea.LblDetalle.Text = LanguageRM.GetString("TesteoLinea.LblDetalle")
        TesteoLinea.BtnComenzar.Text = LanguageRM.GetString("TesteoLinea.BtnComenzar")

        ''InfoShow
        InfoShow.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")

        ''FullAutomation
        FullAutomation.Title.Text = LanguageRM.GetString("FullAutomation.Title")
        FullAutomation.LblPaso.Text = LanguageRM.GetString("FullAutomation.LblPaso")
        FullAutomation.LblFA1.Text = LanguageRM.GetString("FullAutomation.LblFA1")
        FullAutomation.LblFA2.Text = LanguageRM.GetString("FullAutomation.LblFA2")
        FullAutomation.LblFA3.Text = LanguageRM.GetString("FullAutomation.LblFA3")
        FullAutomation.LblFA4.Text = LanguageRM.GetString("FullAutomation.LblFA4")
        FullAutomation.LblFA5.Text = LanguageRM.GetString("FullAutomation.LblFA5")
        FullAutomation.BtnAnterior.Text = LanguageRM.GetString("FullAutomation.BtnAnterior")
        FullAutomation.BtnStart.Text = LanguageRM.GetString("FullAutomation.BtnStart")
        FullAutomation.BtnSiguiente.Text = LanguageRM.GetString("FullAutomation.BtnSiguiente")


        ''Container
        Container1.Title1.Text = LanguageRM.GetString("Container.Title")
        Container2.Title1.Text = LanguageRM.GetString("Container.Title")
        Container2.Title2.Text = LanguageRM.GetString("Container.Title")
        Container4.Title1.Text = LanguageRM.GetString("Container.Title")
        Container4.Title2.Text = LanguageRM.GetString("Container.Title")
        Container4.Title3.Text = LanguageRM.GetString("Container.Title")
        Container4.Title4.Text = LanguageRM.GetString("Container.Title")
        Container8.Title1.Text = LanguageRM.GetString("Container.Title")
        Container8.Title2.Text = LanguageRM.GetString("Container.Title")
        Container8.Title3.Text = LanguageRM.GetString("Container.Title")
        Container8.Title4.Text = LanguageRM.GetString("Container.Title")
        Container8.Title5.Text = LanguageRM.GetString("Container.Title")
        Container8.Title6.Text = LanguageRM.GetString("Container.Title")
        Container8.Title7.Text = LanguageRM.GetString("Container.Title")
        Container8.Title8.Text = LanguageRM.GetString("Container.Title")
        Container1.LblTotalizador.Text = LanguageRM.GetString("Container.LblTotalizador")
        Container1.LblCombustibleTotal.Text = LanguageRM.GetString("Container.LblCombustibleTotal")
        Container1.LblCaudalTotal.Text = LanguageRM.GetString("Container.LblCaudalTotal")
        Container2.LblTotalizador.Text = LanguageRM.GetString("Container.LblTotalizador")
        Container2.LblCombustibleTotal.Text = LanguageRM.GetString("Container.LblCombustibleTotal")
        Container2.LblCaudalTotal.Text = LanguageRM.GetString("Container.LblCaudalTotal")
        Container4.LblTotalizador.Text = LanguageRM.GetString("Container.LblTotalizador")
        Container4.LblCombustibleTotal.Text = LanguageRM.GetString("Container.LblCombustibleTotal")
        Container4.LblCaudalTotal.Text = LanguageRM.GetString("Container.LblCaudalTotal")
        Container8.LblTotalizador.Text = LanguageRM.GetString("Container.LblTotalizador")
        Container8.LblCombustibleTotal.Text = LanguageRM.GetString("Container.LblCombustibleTotal")
        Container8.LblCaudalTotal.Text = LanguageRM.GetString("Container.LblCaudalTotal")

    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en EightFrac.
    ''' </summary>
    Public Sub EightFracLanguageSet(ByVal sender As EightFrac)
        With sender
            .BtnErrores.Text = LanguageRM.GetString("EightFrac.BtnErrores")
            .LblRPM1.Text = LanguageRM.GetString("EightFrac.LblRPM1")
            .LblRPM3.Text = LanguageRM.GetString("EightFrac.LblRPM3")
            .LblMarcha.Text = LanguageRM.GetString("EightFrac.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("EightFrac.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("EightFrac.LblMarcha3")
            .LblPCorte.Text = LanguageRM.GetString("EightFrac.LblPCorte")
            .LblPCorte1.Text = LanguageRM.GetString("EightFrac.LblPCorte1")
            .LblPCorte3.Text = LanguageRM.GetString("EightFrac.LblPCorte3")
            .LblCaudal.Text = LanguageRM.GetString("EightFrac.LblCaudal")

            .CenterEverything()
        End With
    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en FourFrac.
    ''' </summary>
    Public Sub FourFracLanguageSet(ByVal sender As FourFrac)
        With sender
            .LblErrorGrave.Text = LanguageRM.GetString("FourFrac.LblErrorGrave")
            .LblErrorAdmisible.Text = LanguageRM.GetString("FourFrac.LblErrorAdmisible")
            .BtnErrores.Text = LanguageRM.GetString("FourFrac.BtnErrores")
            .LblRPM1.Text = LanguageRM.GetString("FourFrac.LblRPM1")
            .LblRPM3.Text = LanguageRM.GetString("FourFrac.LblRPM3")
            .LblMarcha.Text = LanguageRM.GetString("FourFrac.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("FourFrac.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("FourFrac.LblMarcha3")
            .LblCaudal.Text = LanguageRM.GetString("FourFrac.LblCaudal")
            .LblPCorte.Text = LanguageRM.GetString("FourFrac.LblPCorte")
            .LblPCorte1.Text = LanguageRM.GetString("FourFrac.LblPCorte1")
            .LblPCorte3.Text = LanguageRM.GetString("FourFrac.LblPCorte3")
            .LblOpciones.Text = LanguageRM.GetString("FourFrac.LblOpciones")
            .BtnEmpezarTrabajo.Text = LanguageRM.GetString("FourFrac.BtnEmpezarTrabajo")
            .BtnEncenderLuces.Text = LanguageRM.GetString("FourFrac.BtnEncenderLuces")
            .BtnSemiAuto.Text = LanguageRM.GetString("FourFrac.BtnSemiAuto")

            .CenterEverything()
        End With
    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en TwoFrac.
    ''' </summary>
    Public Sub TwoFracLanguageSet(ByVal sender As TwoFrac)
        With sender
            .LblErrorGrave.Text = LanguageRM.GetString("TwoFrac.LblErrorGrave")
            .LblErrorAdmisible.Text = LanguageRM.GetString("TwoFrac.LblErrorAdmisible")
            .BtnErrores.Text = LanguageRM.GetString("TwoFrac.BtnErrores")
            .LblRPM1.Text = LanguageRM.GetString("TwoFrac.LblRPM1")
            .LblRPM3.Text = LanguageRM.GetString("TwoFrac.LblRPM3")
            .LblMarcha.Text = LanguageRM.GetString("TwoFrac.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("TwoFrac.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("TwoFrac.LblMarcha3")
            .LblCaudal.Text = LanguageRM.GetString("TwoFrac.LblCaudal")
            .LblPCorte.Text = LanguageRM.GetString("TwoFrac.LblPCorte")
            .LblPCorte1.Text = LanguageRM.GetString("TwoFrac.LblPCorte1")
            .LblPCorte3.Text = LanguageRM.GetString("TwoFrac.LblPCorte3")
            .LblOpciones.Text = LanguageRM.GetString("TwoFrac.LblOpciones")
            .BtnEmpezarTrabajo.Text = LanguageRM.GetString("TwoFrac.BtnEmpezarTrabajo")
            .BtnEncenderLuces.Text = LanguageRM.GetString("TwoFrac.BtnEncenderLuces")
            .BtnSemiAuto.Text = LanguageRM.GetString("TwoFrac.BtnSemiAuto")
            .LblTOp.Text = LanguageRM.GetString("TwoFrac.LblTOp")
            .LblMotor.Text = LanguageRM.GetString("TwoFrac.LblMotor")
            .LblTempAgua.Text = LanguageRM.GetString("TwoFrac.LblTempAgua")
            .LblTempAceite.Text = LanguageRM.GetString("TwoFrac.LblTempAceite")
            .LblPresion.Text = LanguageRM.GetString("TwoFrac.LblPresion")
            .LblCombustible.Text = LanguageRM.GetString("TwoFrac.LblCombustible")
            .LblVoltaje.Text = LanguageRM.GetString("TwoFrac.LblVoltaje")
            .LblCombustible1.Text = LanguageRM.GetString("TwoFrac.LblCombustible1")
            .LblTransmision.Text = LanguageRM.GetString("TwoFrac.LblTransmision")
            .LblTrans1.Text = LanguageRM.GetString("TwoFrac.LblTrans1")
            .LblTrans3.Text = LanguageRM.GetString("TwoFrac.LblTrans3")
            .LblTrans5.Text = LanguageRM.GetString("TwoFrac.LblTrans5")
            .LblBomba.Text = LanguageRM.GetString("TwoFrac.LblBomba")
            .LblTempLub.Text = LanguageRM.GetString("TwoFrac.LblTempLub")
            .LblPresSuc.Text = LanguageRM.GetString("TwoFrac.LblPresSuc")
            .LblPresLub.Text = LanguageRM.GetString("TwoFrac.LblPresLub")
            .LblCargaMotor.Text = LanguageRM.GetString("TwoFrac.LblCargaMotor")
            .LblHM.Text = LanguageRM.GetString("TwoFrac.LblHM")

            .CenterEverything()
        End With
    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en OneFrac.
    ''' </summary>
    Public Sub OneFracLanguageSet(ByVal sender As OneFrac)
        With sender
            .BtnErrores.Text = LanguageRM.GetString("OneFrac.BtnErrores")
            .LblRPM1.Text = LanguageRM.GetString("OneFrac.LblRPM1")
            .LblRPM3.Text = LanguageRM.GetString("OneFrac.LblRPM3")
            .LblMarcha.Text = LanguageRM.GetString("OneFrac.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("OneFrac.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("OneFrac.LblMarcha3")
            .LblCaudal.Text = LanguageRM.GetString("OneFrac.LblCaudal")
            .LblPCorte1.Text = LanguageRM.GetString("OneFrac.LblPCorte1")
            .LblPCorte3.Text = LanguageRM.GetString("OneFrac.LblPCorte3")
            .LblOpciones.Text = LanguageRM.GetString("OneFrac.LblOpciones")
            .BtnEmpezarTrabajo.Text = LanguageRM.GetString("OneFrac.BtnEmpezarTrabajo")
            .BtnEncenderLuces.Text = LanguageRM.GetString("OneFrac.BtnEncenderLuces")
            .BtnSemiAuto.Text = LanguageRM.GetString("OneFrac.BtnSemiAuto")
            .LblTOp.Text = LanguageRM.GetString("OneFrac.LblTOp")
            .LblMotor.Text = LanguageRM.GetString("OneFrac.LblMotor")
            .LblTempAgua.Text = LanguageRM.GetString("OneFrac.LblTempAgua")
            .LblTempAceite.Text = LanguageRM.GetString("OneFrac.LblTempAceite")
            .LblPresion.Text = LanguageRM.GetString("OneFrac.LblPresion")
            .LblCombustible.Text = LanguageRM.GetString("OneFrac.LblCombustible")
            .LblVoltaje.Text = LanguageRM.GetString("OneFrac.LblVoltaje")
            .LblCombustible1.Text = LanguageRM.GetString("OneFrac.LblCombustible1")
            .LblTransmision.Text = LanguageRM.GetString("OneFrac.LblTransmision")
            .LblTrans1.Text = LanguageRM.GetString("OneFrac.LblTrans1")
            .LblTrans3.Text = LanguageRM.GetString("OneFrac.LblTrans3")
            .LblTrans5.Text = LanguageRM.GetString("OneFrac.LblTrans5")
            .LblBomba.Text = LanguageRM.GetString("OneFrac.LblBomba")
            .LblTempLub.Text = LanguageRM.GetString("OneFrac.LblTempLub")
            .LblPresSuc.Text = LanguageRM.GetString("OneFrac.LblPresSuc")
            .LblPresLub.Text = LanguageRM.GetString("OneFrac.LblPresLub")
            .LblCargaMotor.Text = LanguageRM.GetString("OneFrac.LblCargaMotor")
            .LblHM.Text = LanguageRM.GetString("OneFrac.LblHM")

            .centerEverything()
        End With
    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en OneFracMaximize.
    ''' </summary>
    Public Sub MaximizeLanguageSet(ByVal sender As OneFracMaximize)
        ''OneFracMaximize

        With sender

            ''General
            .Motor.Text = LanguageRM.GetString("OneFracMaximize.Motor")
            .Transmision.Text = LanguageRM.GetString("OneFracMaximize.Transmision")
            .Bomba.Text = LanguageRM.GetString("OneFracMaximize.Bomba")
            .LblRPM3.Text = LanguageRM.GetString("OneFracMaximize.LblRPM3")
            .LblRPM1.Text = LanguageRM.GetString("OneFracMaximize.LblRPM1")
            .LblMotorGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorGeneral")
            .LblMotorTempAguaGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorTempAguaGeneral")
            .LblMotorTempAceiteGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorTempAceiteGeneral")
            .LblMotorPresionGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorPresionGeneral")
            .LblMotorCombustible1General.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCombustible1General")
            .LblMotorVoltajeGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorVoltajeGeneral")
            .LblMotorCombustible2General.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCombustible2General")
            .LblTransmisionGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionGeneral")
            .LblTransTempGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblTransTempGeneral")
            .LblTransPresionGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblTransPresionGeneral")
            .LblTransRPMGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblTransRPMGeneral")
            .LblErrorGrave.Text = LanguageRM.GetString("OneFracMaximize.LblErrorGrave")
            .LblErrorAdmisible.Text = LanguageRM.GetString("OneFracMaximize.LblErrorAdmisible")
            .BtnErrores.Text = LanguageRM.GetString("OneFracMaximize.BtnErrores")
            .LblCaudal.Text = LanguageRM.GetString("OneFracMaximize.LblCaudal")
            .LblMarcha.Text = LanguageRM.GetString("OneFracMaximize.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("OneFracMaximize.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("OneFracMaximize.LblMarcha3")
            .LblBombaGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaGeneral")
            .LblBombaTempGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaTempGeneral")
            .LblBombaPresGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresGeneral")
            .LblBombaPres1General.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPres1General")
            .LblBombaCargaMotorGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaCargaMotorGeneral")
            .LblBombaHMGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaHMGeneral")
            .LblPCorte3.Text = LanguageRM.GetString("OneFracMaximize.LblPCorte3")
            .LblPCorte1.Text = LanguageRM.GetString("OneFracMaximize.LblPCorte1")
            .BtnDiagnostico.Text = LanguageRM.GetString("OneFracMaximize.BtnDiagnostico")
            .LblTOp.Text = LanguageRM.GetString("OneFracMaximize.LblTOp")

            ''Motor
            .Title1.Text = LanguageRM.GetString("OneFracMaximize.Title1")
            .LblMotorTempAgua.Text = LanguageRM.GetString("OneFracMaximize.LblMotorTempAgua")
            .LblMotorTempAceite.Text = LanguageRM.GetString("OneFracMaximize.LblMotorTempAceite")
            .LblBombaHorasMotorMotor.Text = LanguageRM.GetString("OneFracMaximize.LblBombaHorasMotorMotor")
            .LblMotorCombustible1Motor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCombustible1Motor")
            .LblRPM1M.Text = LanguageRM.GetString("OneFracMaximize.LblRPM1M")
            .LblRPM2.Text = LanguageRM.GetString("OneFracMaximize.LblRPM2")
            .LblMotorCombustibleMotor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCombustibleMotor")
            .LblMotorCargaMotor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCargaMotor")
            .LblMotorVoltajeMotor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorVoltajeMotor")
            .LblMotorPresionMotor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorPresionMotor")
            .LblErroresMotor.Text = LanguageRM.GetString("OneFracMaximize.LblErroresMotor")
            .LblInfoUnidadM.Text = LanguageRM.GetString("OneFracMaximize.LblInfoUnidadM")
            .LblInfoActivoM.Text = LanguageRM.GetString("OneFracMaximize.LblInfoActivoM")
            .LblInfoErrTotM.Text = LanguageRM.GetString("OneFracMaximize.LblInfoErrTotM")
            .LblInfoDescripM.Text = LanguageRM.GetString("OneFracMaximize.LblInfoDescripM")
            .LblDiagnosticoMotor.Text = LanguageRM.GetString("OneFracMaximize.LblDiagnosticoMotor")
            .LblMCombustible.Text = LanguageRM.GetString("OneFracMaximize.LblMCombustible")
            .LblMTempAceite.Text = LanguageRM.GetString("OneFracMaximize.LblMTempAceite")
            .LblMHoras.Text = LanguageRM.GetString("OneFracMaximize.LblMHoras")
            .LblMTempAgua.Text = LanguageRM.GetString("OneFracMaximize.LblMTempAgua")

            ''Transmision
            .Title2.Text = LanguageRM.GetString("OneFracMaximize.Title2")
            .LblTransmisionMarchaActual.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionMarchaActual")
            .LblTransmisionMarchaDeseada.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionMarchaDeseada")
            .LblTransmisionPresion.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionPresion")
            .LblTransmisionRPMOut.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionRPMOut")
            .LblTransmisionTemp.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionTemp")
            .LblErroresTrans.Text = LanguageRM.GetString("OneFracMaximize.LblErroresTrans")
            .LblInfoUnidadT.Text = LanguageRM.GetString("OneFracMaximize.LblInfoUnidadT")
            .LblInfoActivoT.Text = LanguageRM.GetString("OneFracMaximize.LblInfoActivoT")
            .LblInfoErrTotT.Text = LanguageRM.GetString("OneFracMaximize.LblInfoErrTotT")
            .LblInfoDescripT.Text = LanguageRM.GetString("OneFracMaximize.LblInfoDescripT")
            .LblDiagnosticoTrans.Text = LanguageRM.GetString("OneFracMaximize.LblDiagnosticoTrans")
            .LblTTempAceite.Text = LanguageRM.GetString("OneFracMaximize.LblTTempAceite")
            .LblTPresAceite.Text = LanguageRM.GetString("OneFracMaximize.LblTPresAceite")
            .LblTransmisionDiag1.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionDiag1")

            ''Bomba
            .Title3.Text = LanguageRM.GetString("OneFracMaximize.Title3")
            .LblBombaPresLub.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresLub")
            .LblBombaCargaMotor.Text = LanguageRM.GetString("OneFracMaximize.LblBombaCargaMotor")
            .LblBombaPresionActual.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresionActual")
            .LblBombaPresionCorte.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresionCorte")
            .LblBombaCaudal.Text = LanguageRM.GetString("OneFracMaximize.LblBombaCaudal")
            .LblBombaPresionSuccion.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresionSuccion")
            .LblBombaTempLub.Text = LanguageRM.GetString("OneFracMaximize.LblBombaTempLub")
            .LblDiagnosticoBomba.Text = LanguageRM.GetString("OneFracMaximize.LblDiagnosticoBomba")
            .LblSPresDescarga.Text = LanguageRM.GetString("OneFracMaximize.LblSPresDescarga")
            .LblSPresSuccion.Text = LanguageRM.GetString("OneFracMaximize.LblSPresSuccion")
            .LblSPresLub.Text = LanguageRM.GetString("OneFracMaximize.LblSPresLub")
            .LblSTempLub.Text = LanguageRM.GetString("OneFracMaximize.LblSTempLub")

            .CenterEverything()
        End With
    End Sub

End Class
