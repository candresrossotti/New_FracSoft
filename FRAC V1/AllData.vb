﻿Public Class AllData
    Private DataList As List(Of String) = New List(Of String) From {
        "TRIP [PSI]",
        "Viatran [PSI]",
        "Temp. Lub. LP [°C]",
        "Temp. Lub. HP [°C]",
        "Pres. Lub. LP [PSI]",
        "Pres. Lub. HP [PSI]",
        "Pres. Suc. [PSI]",
        "Pres. Trans. [PSI]",
        "Caudal",
        "RPM Deseada",
        "RPM Motor",
        "RPM Transmision",
        "Temperatura agua motor",
        "Temperatura aceite motor",
        "Presion aceite motor",
        "Fuel rate",
        "Voltaje",
        "Combustible [%]",
        "Horas motor",
        "Carga motor [%]",
        "HHP",
        "Temperatura aceite trans.",
        "Marcha deseada",
        "Marcha actual",
        "Alarma Viatran",
        "Alarma Temp. Lub. LP",
        "Alarma Temp. Lub. HP",
        "Alarma Pres. Lub. LP",
        "Alarma Pres. Lub. HP",
        "Alarma Pres. Suc.",
        "Alarma Pres. Trans.",
        "Alarma Caudal",
        "LockUp"
    }
    Private Sub AllData_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        DGV1.RowHeadersVisible = True
        DGV1.RowHeadersWidth = 100
        DGV1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DGV1.AlternatingRowsDefaultCellStyle.BackColor = Color.White
        DGV1.CellBorderStyle = DataGridViewCellBorderStyle.RaisedVertical


        For i = 1 To 24
            DGV1.Columns.Add("F" + i.ToString(), "Frac " + i.ToString())
            DGV1.Columns.Item(i - 1).SortMode = DataGridViewColumnSortMode.NotSortable
        Next
        DGV1.ColumnHeadersDefaultCellStyle.Font = New Font("Montserrat", 13, FontStyle.Regular)

        DGV1.ReadOnly = True
        For Each item In DataList
            Dim RowHeader As New DataGridViewRow
            RowHeader.HeaderCell.Value = item
            RowHeader.Height = 30
            DGV1.Rows.Add(RowHeader)
        Next

        For i = 0 To 23
            DGV1.Item(i, 0).Value = FracData.DblVFrac("TRIP_" + (i + 1).ToString())
            DGV1.Item(i, 1).Value = FracData.DblVFrac("Viatran_" + (i + 1).ToString())
            DGV1.Item(i, 2).Value = FracData.DblVFrac("TemperaturaLUB_LP_" + (i + 1).ToString())
            DGV1.Item(i, 3).Value = FracData.DblVFrac("TemperaturaLUB_HP_" + (i + 1).ToString())
            DGV1.Item(i, 4).Value = FracData.DblVFrac("PresionLUB_LP_" + (i + 1).ToString())
            DGV1.Item(i, 5).Value = FracData.DblVFrac("PresionLUB_HP_" + (i + 1).ToString())
            DGV1.Item(i, 6).Value = FracData.DblVFrac("PresSuc_" + (i + 1).ToString())
            DGV1.Item(i, 7).Value = FracData.DblVFrac("PresTrans_" + (i + 1).ToString())
            DGV1.Item(i, 8).Value = FracData.DblVFrac("Caudal_" + (i + 1).ToString())
            DGV1.Item(i, 9).Value = FracData.DblVFrac("RPMDeseada_" + (i + 1).ToString())
            DGV1.Item(i, 10).Value = FracData.DblVFrac("RPMMotor_" + (i + 1).ToString())
            DGV1.Item(i, 11).Value = FracData.DblVFrac("RPMSalidaTransmision_" + (i + 1).ToString())
            DGV1.Item(i, 12).Value = FracData.DblVFrac("TemperaturaAguaMotor_" + (i + 1).ToString())
            DGV1.Item(i, 13).Value = FracData.DblVFrac("TemperaturaAceiteMotor_" + (i + 1).ToString())
            DGV1.Item(i, 14).Value = FracData.DblVFrac("PresionAceiteMotor_" + (i + 1).ToString())
            DGV1.Item(i, 15).Value = FracData.DblVFrac("FuelRate_" + (i + 1).ToString())
            DGV1.Item(i, 16).Value = FracData.DblVFrac("Voltaje_" + (i + 1).ToString())
            DGV1.Item(i, 17).Value = FracData.DblVFrac("CombustibleTanque_" + (i + 1).ToString())
            DGV1.Item(i, 18).Value = FracData.DblVFrac("HorasMotor_" + (i + 1).ToString())
            DGV1.Item(i, 19).Value = FracData.DblVFrac("PorcentajeCarga_" + (i + 1).ToString())
            DGV1.Item(i, 20).Value = FracData.DblVFrac("HHPMotor_" + (i + 1).ToString())
            DGV1.Item(i, 21).Value = FracData.DblVFrac("TemperaturaAceiteTransmision_" + (i + 1).ToString())
            DGV1.Item(i, 22).Value = FracData.StrVFrac("MarchaDeseada_" + (i + 1).ToString())
            DGV1.Item(i, 23).Value = FracData.StrVFrac("MarchaActual_" + (i + 1).ToString())
            DGV1.Item(i, 24).Value = FracData.DblVFrac("Viatran_Alarm_Active_" + (i + 1).ToString())
            DGV1.Item(i, 25).Value = FracData.DblVFrac("TemperaturaLUB_LP_Alarm_Active_" + (i + 1).ToString())
            DGV1.Item(i, 26).Value = FracData.DblVFrac("TemperaturaLUB_HP_Alarm_Active_" + (i + 1).ToString())
            DGV1.Item(i, 27).Value = FracData.DblVFrac("PresionLUB_LP_Alarm_Active_" + (i + 1).ToString())
            DGV1.Item(i, 28).Value = FracData.DblVFrac("PresionLUB_HP_Alarm_Active_" + (i + 1).ToString())
            DGV1.Item(i, 29).Value = FracData.DblVFrac("PresSuc_Alarm_Active_" + (i + 1).ToString())
            DGV1.Item(i, 30).Value = FracData.DblVFrac("PresTrans_Alarm_Active_" + (i + 1).ToString())
            DGV1.Item(i, 31).Value = FracData.DblVFrac("Caudal_Scaled_Alarm_Active_" + (i + 1).ToString())
            DGV1.Item(i, 32).Value = FracData.DblVFrac("LockUp_" + (i + 1).ToString())
        Next

        DGV1.ClearSelection()
    End Sub

    ''Si clickea en el panel/titulo dockeamos el form
    Private Sub Maximize(sender As Object, e As EventArgs) Handles TitlePanel.MouseDoubleClick, Title.MouseDoubleClick
        Dock = DockStyle.Fill
        CenterToScreen()
    End Sub

    Private Sub TmrDisplay_Tick(sender As Object, e As EventArgs) Handles TmrDisplay.Tick
        For i = 0 To 23
            DGV1.Item(i, 0).Value = FracData.DblVFrac("TRIP_" + (i + 1).ToString())
            DGV1.Item(i, 1).Value = FracData.DblVFrac("Viatran_" + (i + 1).ToString())
            DGV1.Item(i, 2).Value = FracData.DblVFrac("TemperaturaLUB_LP_" + (i + 1).ToString())
            DGV1.Item(i, 3).Value = FracData.DblVFrac("TemperaturaLUB_HP_" + (i + 1).ToString())
            DGV1.Item(i, 4).Value = FracData.DblVFrac("PresionLUB_LP_" + (i + 1).ToString())
            DGV1.Item(i, 5).Value = FracData.DblVFrac("PresionLUB_HP_" + (i + 1).ToString())
            DGV1.Item(i, 6).Value = FracData.DblVFrac("PresSuc_" + (i + 1).ToString())
            DGV1.Item(i, 7).Value = FracData.DblVFrac("PresTrans_" + (i + 1).ToString())
            DGV1.Item(i, 8).Value = FracData.DblVFrac("Caudal_" + (i + 1).ToString())
            DGV1.Item(i, 9).Value = FracData.DblVFrac("RPMDeseada_" + (i + 1).ToString())
            DGV1.Item(i, 10).Value = FracData.DblVFrac("RPMMotor_" + (i + 1).ToString())
            DGV1.Item(i, 11).Value = FracData.DblVFrac("RPMSalidaTransmision_" + (i + 1).ToString())
            DGV1.Item(i, 12).Value = FracData.DblVFrac("TemperaturaAguaMotor_" + (i + 1).ToString())
            DGV1.Item(i, 13).Value = FracData.DblVFrac("TemperaturaAceiteMotor_" + (i + 1).ToString())
            DGV1.Item(i, 14).Value = FracData.DblVFrac("PresionAceiteMotor_" + (i + 1).ToString())
            DGV1.Item(i, 15).Value = FracData.DblVFrac("FuelRate_" + (i + 1).ToString())
            DGV1.Item(i, 16).Value = FracData.DblVFrac("Voltaje_" + (i + 1).ToString())
            DGV1.Item(i, 17).Value = FracData.DblVFrac("CombustibleTanque_" + (i + 1).ToString())
            DGV1.Item(i, 18).Value = FracData.DblVFrac("HorasMotor_" + (i + 1).ToString())
            DGV1.Item(i, 19).Value = FracData.DblVFrac("PorcentajeCarga_" + (i + 1).ToString())
            DGV1.Item(i, 20).Value = FracData.DblVFrac("HHPMotor_" + (i + 1).ToString())
            DGV1.Item(i, 21).Value = FracData.DblVFrac("TemperaturaAceiteTransmision_" + (i + 1).ToString())
            DGV1.Item(i, 22).Value = FracData.StrVFrac("MarchaDeseada_" + (i + 1).ToString())
            DGV1.Item(i, 23).Value = FracData.StrVFrac("MarchaActual_" + (i + 1).ToString())

            DGV1.Item(i, 24).Value = FracData.DblVFrac("Viatran_Alarm_Active_" + (i + 1).ToString())
            If FracData.DblVFrac("Viatran_Alarm_Active_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 24).Style.BackColor = Color.Red Else DGV1.Item(i, 24).Style.BackColor = Color.White
            DGV1.Item(i, 25).Value = FracData.DblVFrac("TemperaturaLUB_LP_Alarm_Active_" + (i + 1).ToString())
            If FracData.DblVFrac("TemperaturaLUB_LP_Alarm_Active_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 25).Style.BackColor = Color.Red Else DGV1.Item(i, 25).Style.BackColor = Color.White
            DGV1.Item(i, 26).Value = FracData.DblVFrac("TemperaturaLUB_HP_Alarm_Active_" + (i + 1).ToString())
            If FracData.DblVFrac("TemperaturaLUB_HP_Alarm_Active_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 26).Style.BackColor = Color.Red Else DGV1.Item(i, 26).Style.BackColor = Color.White
            DGV1.Item(i, 27).Value = FracData.DblVFrac("PresionLUB_LP_Alarm_Active_" + (i + 1).ToString())
            If FracData.DblVFrac("PresionLUB_LP_Alarm_Active_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 27).Style.BackColor = Color.Red Else DGV1.Item(i, 27).Style.BackColor = Color.White
            DGV1.Item(i, 28).Value = FracData.DblVFrac("PresionLUB_HP_Alarm_Active_" + (i + 1).ToString())
            If FracData.DblVFrac("PresionLUB_HP_Alarm_Active_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 28).Style.BackColor = Color.Red Else DGV1.Item(i, 28).Style.BackColor = Color.White
            DGV1.Item(i, 29).Value = FracData.DblVFrac("PresSuc_Alarm_Active_" + (i + 1).ToString())
            If FracData.DblVFrac("PresSuc_Alarm_Active_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 29).Style.BackColor = Color.Red Else DGV1.Item(i, 29).Style.BackColor = Color.White
            DGV1.Item(i, 30).Value = FracData.DblVFrac("PresTrans_Alarm_Active_" + (i + 1).ToString())
            If FracData.DblVFrac("PresTrans_Alarm_Active_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 30).Style.BackColor = Color.Red Else DGV1.Item(i, 30).Style.BackColor = Color.White
            DGV1.Item(i, 31).Value = FracData.DblVFrac("Caudal_Scaled_Alarm_Active_" + (i + 1).ToString())
            If FracData.DblVFrac("Caudal_Scaled_Alarm_Active_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 31).Style.BackColor = Color.Red Else DGV1.Item(i, 31).Style.BackColor = Color.White
            DGV1.Item(i, 32).Value = FracData.DblVFrac("LockUp_" + (i + 1).ToString())
            If FracData.DblVFrac("LockUp_" + (i + 1).ToString()) = 1 Then DGV1.Item(i, 32).Style.BackColor = Color.Red Else DGV1.Item(i, 32).Style.BackColor = Color.White
        Next
    End Sub
End Class