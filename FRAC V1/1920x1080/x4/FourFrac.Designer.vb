﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FourFrac_1920x1080
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2CustomGradientPanel10 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCaudal = New System.Windows.Forms.Label()
        Me.TmrCheck = New System.Windows.Forms.Timer(Me.components)
        Me.LblMarchaActual = New System.Windows.Forms.Label()
        Me.LblMarchaDeseada = New System.Windows.Forms.Label()
        Me.LblMarcha3 = New System.Windows.Forms.Label()
        Me.LblMarcha1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMarcha = New System.Windows.Forms.Label()
        Me.BtnResetRPM = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnIdle = New Guna.UI2.WinForms.Guna2Button()
        Me.LblRPMActual = New System.Windows.Forms.Label()
        Me.LblRPMDeseada = New System.Windows.Forms.Label()
        Me.LblRPM3 = New System.Windows.Forms.Label()
        Me.LblRPM1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblRpm = New System.Windows.Forms.Label()
        Me.PbAutomatic = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.BtnErrores = New Guna.UI2.WinForms.Guna2Button()
        Me.TmrReconnect = New System.Windows.Forms.Timer(Me.components)
        Me.TmrErrorHandle = New System.Windows.Forms.Timer(Me.components)
        Me.TmrOp = New System.Windows.Forms.Timer(Me.components)
        Me.TmrRead = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtnAjustes = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel10 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnFC = New Guna.UI2.WinForms.Guna2Button()
        Me.LblCaudal2 = New System.Windows.Forms.Label()
        Me.LblCaudalActual = New System.Windows.Forms.Label()
        Me.BtnMinusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnStartEngine = New Guna.UI2.WinForms.Guna2Button()
        Me.LblErrorAdmisible = New System.Windows.Forms.Label()
        Me.LblErrorGrave = New System.Windows.Forms.Label()
        Me.Guna2Panel9 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnTripZero = New Guna.UI2.WinForms.Guna2Button()
        Me.LblPCorteActual = New System.Windows.Forms.Label()
        Me.LblPCorte3 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel9 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblPCorte = New System.Windows.Forms.Label()
        Me.BtnPlusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnRpmMinus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmMinus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.PanelRPM = New Guna.UI2.WinForms.Guna2Panel()
        Me.TmrBackUp = New System.Windows.Forms.Timer(Me.components)
        Me.BtnEKill = New Guna.UI2.WinForms.Guna2Button()
        Me.PbAlarms = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.PbLockUp = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.PbMoxaStatus = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.BtnParking = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel10.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2Panel10.SuspendLayout()
        Me.Guna2Panel9.SuspendLayout()
        Me.Guna2CustomGradientPanel9.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.PanelRPM.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2CustomGradientPanel10
        '
        Me.Guna2CustomGradientPanel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.BorderRadius = 6
        Me.Guna2CustomGradientPanel10.Controls.Add(Me.LblCaudal)
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel10.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel10.Name = "Guna2CustomGradientPanel10"
        Me.Guna2CustomGradientPanel10.Size = New System.Drawing.Size(213, 26)
        Me.Guna2CustomGradientPanel10.TabIndex = 19
        '
        'LblCaudal
        '
        Me.LblCaudal.AutoSize = True
        Me.LblCaudal.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblCaudal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCaudal.Location = New System.Drawing.Point(75, 3)
        Me.LblCaudal.Name = "LblCaudal"
        Me.LblCaudal.Size = New System.Drawing.Size(63, 21)
        Me.LblCaudal.TabIndex = 19
        Me.LblCaudal.Text = "Caudal"
        '
        'TmrCheck
        '
        Me.TmrCheck.Interval = 1000
        '
        'LblMarchaActual
        '
        Me.LblMarchaActual.AutoSize = True
        Me.LblMarchaActual.Font = New System.Drawing.Font("Montserrat", 18.0!)
        Me.LblMarchaActual.ForeColor = System.Drawing.Color.White
        Me.LblMarchaActual.Location = New System.Drawing.Point(69, 172)
        Me.LblMarchaActual.Name = "LblMarchaActual"
        Me.LblMarchaActual.Size = New System.Drawing.Size(24, 33)
        Me.LblMarchaActual.TabIndex = 23
        Me.LblMarchaActual.Text = "-"
        '
        'LblMarchaDeseada
        '
        Me.LblMarchaDeseada.AutoSize = True
        Me.LblMarchaDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblMarchaDeseada.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblMarchaDeseada.ForeColor = System.Drawing.Color.White
        Me.LblMarchaDeseada.Location = New System.Drawing.Point(71, 96)
        Me.LblMarchaDeseada.Name = "LblMarchaDeseada"
        Me.LblMarchaDeseada.Size = New System.Drawing.Size(20, 27)
        Me.LblMarchaDeseada.TabIndex = 22
        Me.LblMarchaDeseada.Text = "-"
        '
        'LblMarcha3
        '
        Me.LblMarcha3.AutoSize = True
        Me.LblMarcha3.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblMarcha3.ForeColor = System.Drawing.Color.White
        Me.LblMarcha3.Location = New System.Drawing.Point(10, 149)
        Me.LblMarcha3.Name = "LblMarcha3"
        Me.LblMarcha3.Size = New System.Drawing.Size(143, 25)
        Me.LblMarcha3.TabIndex = 21
        Me.LblMarcha3.Text = "Marcha Actual"
        '
        'LblMarcha1
        '
        Me.LblMarcha1.AutoSize = True
        Me.LblMarcha1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblMarcha1.ForeColor = System.Drawing.Color.White
        Me.LblMarcha1.Location = New System.Drawing.Point(10, 63)
        Me.LblMarcha1.Name = "LblMarcha1"
        Me.LblMarcha1.Size = New System.Drawing.Size(143, 22)
        Me.LblMarcha1.TabIndex = 20
        Me.LblMarcha1.Text = "Marcha Deseada"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblMarcha)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(163, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblMarcha
        '
        Me.LblMarcha.AutoSize = True
        Me.LblMarcha.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMarcha.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMarcha.Location = New System.Drawing.Point(48, 3)
        Me.LblMarcha.Name = "LblMarcha"
        Me.LblMarcha.Size = New System.Drawing.Size(66, 21)
        Me.LblMarcha.TabIndex = 19
        Me.LblMarcha.Text = "Marcha"
        '
        'BtnResetRPM
        '
        Me.BtnResetRPM.Animated = True
        Me.BtnResetRPM.BorderColor = System.Drawing.Color.White
        Me.BtnResetRPM.BorderRadius = 5
        Me.BtnResetRPM.BorderThickness = 1
        Me.BtnResetRPM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnResetRPM.FillColor = System.Drawing.Color.Transparent
        Me.BtnResetRPM.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnResetRPM.ForeColor = System.Drawing.Color.White
        Me.BtnResetRPM.Location = New System.Drawing.Point(231, 131)
        Me.BtnResetRPM.Name = "BtnResetRPM"
        Me.BtnResetRPM.Size = New System.Drawing.Size(210, 45)
        Me.BtnResetRPM.TabIndex = 97
        Me.BtnResetRPM.Text = "Reset RPM"
        Me.BtnResetRPM.Visible = False
        '
        'BtnIdle
        '
        Me.BtnIdle.Animated = True
        Me.BtnIdle.BorderColor = System.Drawing.Color.White
        Me.BtnIdle.BorderRadius = 5
        Me.BtnIdle.BorderThickness = 1
        Me.BtnIdle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdle.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdle.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnIdle.ForeColor = System.Drawing.Color.White
        Me.BtnIdle.Location = New System.Drawing.Point(6, 197)
        Me.BtnIdle.Name = "BtnIdle"
        Me.BtnIdle.Size = New System.Drawing.Size(151, 57)
        Me.BtnIdle.TabIndex = 69
        Me.BtnIdle.Text = "INSTANT NEUTRAL"
        '
        'LblRPMActual
        '
        Me.LblRPMActual.AutoSize = True
        Me.LblRPMActual.Font = New System.Drawing.Font("Montserrat", 18.0!)
        Me.LblRPMActual.ForeColor = System.Drawing.Color.White
        Me.LblRPMActual.Location = New System.Drawing.Point(69, 141)
        Me.LblRPMActual.Name = "LblRPMActual"
        Me.LblRPMActual.Size = New System.Drawing.Size(24, 33)
        Me.LblRPMActual.TabIndex = 23
        Me.LblRPMActual.Text = "-"
        '
        'LblRPMDeseada
        '
        Me.LblRPMDeseada.AutoSize = True
        Me.LblRPMDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblRPMDeseada.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRPMDeseada.ForeColor = System.Drawing.Color.White
        Me.LblRPMDeseada.Location = New System.Drawing.Point(73, 70)
        Me.LblRPMDeseada.Name = "LblRPMDeseada"
        Me.LblRPMDeseada.Size = New System.Drawing.Size(16, 22)
        Me.LblRPMDeseada.TabIndex = 22
        Me.LblRPMDeseada.Text = "-"
        '
        'LblRPM3
        '
        Me.LblRPM3.AutoSize = True
        Me.LblRPM3.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRPM3.ForeColor = System.Drawing.Color.White
        Me.LblRPM3.Location = New System.Drawing.Point(21, 113)
        Me.LblRPM3.Name = "LblRPM3"
        Me.LblRPM3.Size = New System.Drawing.Size(120, 22)
        Me.LblRPM3.TabIndex = 21
        Me.LblRPM3.Text = "RPM Actuales"
        '
        'LblRPM1
        '
        Me.LblRPM1.AutoSize = True
        Me.LblRPM1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRPM1.ForeColor = System.Drawing.Color.White
        Me.LblRPM1.Location = New System.Drawing.Point(16, 42)
        Me.LblRPM1.Name = "LblRPM1"
        Me.LblRPM1.Size = New System.Drawing.Size(130, 22)
        Me.LblRPM1.TabIndex = 20
        Me.LblRPM1.Text = "RPM Deseadas"
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblRpm)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(163, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblRpm
        '
        Me.LblRpm.AutoSize = True
        Me.LblRpm.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblRpm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblRpm.Location = New System.Drawing.Point(58, 4)
        Me.LblRpm.Name = "LblRpm"
        Me.LblRpm.Size = New System.Drawing.Size(46, 21)
        Me.LblRpm.TabIndex = 19
        Me.LblRpm.Text = "RPM"
        '
        'PbAutomatic
        '
        Me.PbAutomatic.BackColor = System.Drawing.Color.Transparent
        Me.PbAutomatic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAutomatic.Image = Global.sFRAC.My.Resources.Resources.automatic1
        Me.PbAutomatic.ImageRotate = 0!
        Me.PbAutomatic.Location = New System.Drawing.Point(132, 47)
        Me.PbAutomatic.Name = "PbAutomatic"
        Me.PbAutomatic.Size = New System.Drawing.Size(70, 70)
        Me.PbAutomatic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAutomatic.TabIndex = 114
        Me.PbAutomatic.TabStop = False
        Me.PbAutomatic.UseTransparentBackground = True
        Me.PbAutomatic.Visible = False
        '
        'BtnErrores
        '
        Me.BtnErrores.Animated = True
        Me.BtnErrores.BorderColor = System.Drawing.Color.White
        Me.BtnErrores.BorderRadius = 5
        Me.BtnErrores.BorderThickness = 1
        Me.BtnErrores.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnErrores.FillColor = System.Drawing.Color.Transparent
        Me.BtnErrores.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnErrores.ForeColor = System.Drawing.Color.White
        Me.BtnErrores.Location = New System.Drawing.Point(186, 12)
        Me.BtnErrores.Name = "BtnErrores"
        Me.BtnErrores.Size = New System.Drawing.Size(84, 29)
        Me.BtnErrores.TabIndex = 112
        Me.BtnErrores.Text = "Errores"
        '
        'TmrReconnect
        '
        Me.TmrReconnect.Interval = 5000
        '
        'TmrErrorHandle
        '
        Me.TmrErrorHandle.Interval = 600
        '
        'TmrOp
        '
        Me.TmrOp.Interval = 1000
        '
        'TmrRead
        '
        Me.TmrRead.Interval = 1000
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(155, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 20)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "PSI"
        '
        'BtnAjustes
        '
        Me.BtnAjustes.Animated = True
        Me.BtnAjustes.BorderColor = System.Drawing.Color.White
        Me.BtnAjustes.BorderRadius = 5
        Me.BtnAjustes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAjustes.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAjustes.ForeColor = System.Drawing.Color.White
        Me.BtnAjustes.Image = Global.sFRAC.My.Resources.Resources.settings_1
        Me.BtnAjustes.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnAjustes.ImageSize = New System.Drawing.Size(69, 69)
        Me.BtnAjustes.Location = New System.Drawing.Point(12, 47)
        Me.BtnAjustes.Name = "BtnAjustes"
        Me.BtnAjustes.Size = New System.Drawing.Size(70, 70)
        Me.BtnAjustes.TabIndex = 110
        '
        'Guna2Panel10
        '
        Me.Guna2Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.BorderRadius = 6
        Me.Guna2Panel10.BorderThickness = 1
        Me.Guna2Panel10.Controls.Add(Me.BtnFC)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudal2)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudalActual)
        Me.Guna2Panel10.Controls.Add(Me.Guna2CustomGradientPanel10)
        Me.Guna2Panel10.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.Location = New System.Drawing.Point(12, 131)
        Me.Guna2Panel10.Name = "Guna2Panel10"
        Me.Guna2Panel10.Size = New System.Drawing.Size(213, 154)
        Me.Guna2Panel10.TabIndex = 105
        '
        'BtnFC
        '
        Me.BtnFC.Animated = True
        Me.BtnFC.BorderColor = System.Drawing.Color.White
        Me.BtnFC.BorderRadius = 5
        Me.BtnFC.BorderThickness = 1
        Me.BtnFC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFC.FillColor = System.Drawing.Color.Transparent
        Me.BtnFC.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnFC.ForeColor = System.Drawing.Color.White
        Me.BtnFC.Location = New System.Drawing.Point(138, 74)
        Me.BtnFC.Name = "BtnFC"
        Me.BtnFC.Size = New System.Drawing.Size(45, 40)
        Me.BtnFC.TabIndex = 95
        Me.BtnFC.Text = "FC"
        Me.BtnFC.Visible = False
        '
        'LblCaudal2
        '
        Me.LblCaudal2.AutoSize = True
        Me.LblCaudal2.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCaudal2.ForeColor = System.Drawing.Color.White
        Me.LblCaudal2.Location = New System.Drawing.Point(40, 114)
        Me.LblCaudal2.Name = "LblCaudal2"
        Me.LblCaudal2.Size = New System.Drawing.Size(70, 20)
        Me.LblCaudal2.TabIndex = 23
        Me.LblCaudal2.Text = "BBL/Min"
        '
        'LblCaudalActual
        '
        Me.LblCaudalActual.AutoSize = True
        Me.LblCaudalActual.Font = New System.Drawing.Font("Montserrat", 18.0!)
        Me.LblCaudalActual.ForeColor = System.Drawing.Color.White
        Me.LblCaudalActual.Location = New System.Drawing.Point(63, 67)
        Me.LblCaudalActual.Name = "LblCaudalActual"
        Me.LblCaudalActual.Size = New System.Drawing.Size(24, 33)
        Me.LblCaudalActual.TabIndex = 22
        Me.LblCaudalActual.Text = "-"
        '
        'BtnMinusGear
        '
        Me.BtnMinusGear.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnMinusGear.Animated = True
        Me.BtnMinusGear.BorderColor = System.Drawing.Color.White
        Me.BtnMinusGear.BorderRadius = 5
        Me.BtnMinusGear.BorderThickness = 1
        Me.BtnMinusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMinusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnMinusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnMinusGear.ForeColor = System.Drawing.Color.White
        Me.BtnMinusGear.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnMinusGear.Location = New System.Drawing.Point(181, 399)
        Me.BtnMinusGear.Name = "BtnMinusGear"
        Me.BtnMinusGear.Size = New System.Drawing.Size(44, 44)
        Me.BtnMinusGear.TabIndex = 104
        '
        'BtnStartEngine
        '
        Me.BtnStartEngine.Animated = True
        Me.BtnStartEngine.BorderColor = System.Drawing.Color.White
        Me.BtnStartEngine.BorderRadius = 5
        Me.BtnStartEngine.CheckedState.Image = Global.sFRAC.My.Resources.Resources.Engine_STOP
        Me.BtnStartEngine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStartEngine.FillColor = System.Drawing.Color.Transparent
        Me.BtnStartEngine.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnStartEngine.ForeColor = System.Drawing.Color.White
        Me.BtnStartEngine.Image = Global.sFRAC.My.Resources.Resources.Engine_START
        Me.BtnStartEngine.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnStartEngine.ImageSize = New System.Drawing.Size(69, 69)
        Me.BtnStartEngine.Location = New System.Drawing.Point(372, 47)
        Me.BtnStartEngine.Name = "BtnStartEngine"
        Me.BtnStartEngine.Size = New System.Drawing.Size(70, 70)
        Me.BtnStartEngine.TabIndex = 109
        '
        'LblErrorAdmisible
        '
        Me.LblErrorAdmisible.AutoSize = True
        Me.LblErrorAdmisible.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErrorAdmisible.ForeColor = System.Drawing.Color.Orange
        Me.LblErrorAdmisible.Location = New System.Drawing.Point(61, 16)
        Me.LblErrorAdmisible.Name = "LblErrorAdmisible"
        Me.LblErrorAdmisible.Size = New System.Drawing.Size(119, 20)
        Me.LblErrorAdmisible.TabIndex = 108
        Me.LblErrorAdmisible.Text = "Error Admisible"
        Me.LblErrorAdmisible.Visible = False
        '
        'LblErrorGrave
        '
        Me.LblErrorGrave.AutoSize = True
        Me.LblErrorGrave.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErrorGrave.ForeColor = System.Drawing.Color.Red
        Me.LblErrorGrave.Location = New System.Drawing.Point(276, 16)
        Me.LblErrorGrave.Name = "LblErrorGrave"
        Me.LblErrorGrave.Size = New System.Drawing.Size(89, 20)
        Me.LblErrorGrave.TabIndex = 107
        Me.LblErrorGrave.Text = "Error Grave"
        Me.LblErrorGrave.Visible = False
        '
        'Guna2Panel9
        '
        Me.Guna2Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.BorderRadius = 6
        Me.Guna2Panel9.BorderThickness = 1
        Me.Guna2Panel9.Controls.Add(Me.Label2)
        Me.Guna2Panel9.Controls.Add(Me.BtnTripZero)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorteActual)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorte3)
        Me.Guna2Panel9.Controls.Add(Me.Guna2CustomGradientPanel9)
        Me.Guna2Panel9.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.Location = New System.Drawing.Point(13, 543)
        Me.Guna2Panel9.Name = "Guna2Panel9"
        Me.Guna2Panel9.Size = New System.Drawing.Size(213, 151)
        Me.Guna2Panel9.TabIndex = 106
        '
        'BtnTripZero
        '
        Me.BtnTripZero.Animated = True
        Me.BtnTripZero.BorderColor = System.Drawing.Color.White
        Me.BtnTripZero.BorderRadius = 5
        Me.BtnTripZero.BorderThickness = 1
        Me.BtnTripZero.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTripZero.FillColor = System.Drawing.Color.Transparent
        Me.BtnTripZero.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnTripZero.ForeColor = System.Drawing.Color.White
        Me.BtnTripZero.Location = New System.Drawing.Point(38, 95)
        Me.BtnTripZero.Name = "BtnTripZero"
        Me.BtnTripZero.Size = New System.Drawing.Size(137, 39)
        Me.BtnTripZero.TabIndex = 53
        Me.BtnTripZero.Text = "ZERO"
        Me.BtnTripZero.Visible = False
        '
        'LblPCorteActual
        '
        Me.LblPCorteActual.AutoSize = True
        Me.LblPCorteActual.Font = New System.Drawing.Font("Montserrat", 18.0!)
        Me.LblPCorteActual.ForeColor = System.Drawing.Color.White
        Me.LblPCorteActual.Location = New System.Drawing.Point(94, 58)
        Me.LblPCorteActual.Name = "LblPCorteActual"
        Me.LblPCorteActual.Size = New System.Drawing.Size(24, 33)
        Me.LblPCorteActual.TabIndex = 23
        Me.LblPCorteActual.Text = "-"
        '
        'LblPCorte3
        '
        Me.LblPCorte3.AutoSize = True
        Me.LblPCorte3.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblPCorte3.ForeColor = System.Drawing.Color.White
        Me.LblPCorte3.Location = New System.Drawing.Point(35, 37)
        Me.LblPCorte3.Name = "LblPCorte3"
        Me.LblPCorte3.Size = New System.Drawing.Size(142, 25)
        Me.LblPCorte3.TabIndex = 21
        Me.LblPCorte3.Text = "Presión Actual"
        '
        'Guna2CustomGradientPanel9
        '
        Me.Guna2CustomGradientPanel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.BorderRadius = 6
        Me.Guna2CustomGradientPanel9.Controls.Add(Me.LblPCorte)
        Me.Guna2CustomGradientPanel9.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel9.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel9.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel9.Name = "Guna2CustomGradientPanel9"
        Me.Guna2CustomGradientPanel9.Size = New System.Drawing.Size(213, 26)
        Me.Guna2CustomGradientPanel9.TabIndex = 19
        '
        'LblPCorte
        '
        Me.LblPCorte.AutoSize = True
        Me.LblPCorte.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblPCorte.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblPCorte.Location = New System.Drawing.Point(39, 3)
        Me.LblPCorte.Name = "LblPCorte"
        Me.LblPCorte.Size = New System.Drawing.Size(134, 21)
        Me.LblPCorte.TabIndex = 19
        Me.LblPCorte.Text = "Presión de Corte"
        '
        'BtnPlusGear
        '
        Me.BtnPlusGear.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnPlusGear.Animated = True
        Me.BtnPlusGear.BorderColor = System.Drawing.Color.White
        Me.BtnPlusGear.BorderRadius = 5
        Me.BtnPlusGear.BorderThickness = 1
        Me.BtnPlusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPlusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnPlusGear.ForeColor = System.Drawing.Color.White
        Me.BtnPlusGear.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnPlusGear.Location = New System.Drawing.Point(181, 349)
        Me.BtnPlusGear.Name = "BtnPlusGear"
        Me.BtnPlusGear.Size = New System.Drawing.Size(44, 44)
        Me.BtnPlusGear.TabIndex = 103
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaActual)
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaDeseada)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha3)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(12, 322)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(163, 215)
        Me.Guna2Panel1.TabIndex = 102
        '
        'BtnRpmMinus50
        '
        Me.BtnRpmMinus50.Animated = True
        Me.BtnRpmMinus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.BorderRadius = 5
        Me.BtnRpmMinus50.BorderThickness = 1
        Me.BtnRpmMinus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnRpmMinus50.Location = New System.Drawing.Point(401, 507)
        Me.BtnRpmMinus50.Name = "BtnRpmMinus50"
        Me.BtnRpmMinus50.Size = New System.Drawing.Size(44, 44)
        Me.BtnRpmMinus50.TabIndex = 101
        '
        'BtnRpmPlus50
        '
        Me.BtnRpmPlus50.Animated = True
        Me.BtnRpmPlus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.BorderRadius = 5
        Me.BtnRpmPlus50.BorderThickness = 1
        Me.BtnRpmPlus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnRpmPlus50.Location = New System.Drawing.Point(401, 385)
        Me.BtnRpmPlus50.Name = "BtnRpmPlus50"
        Me.BtnRpmPlus50.Size = New System.Drawing.Size(44, 44)
        Me.BtnRpmPlus50.TabIndex = 100
        '
        'BtnRpmMinus25
        '
        Me.BtnRpmMinus25.Animated = True
        Me.BtnRpmMinus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.BorderRadius = 5
        Me.BtnRpmMinus25.BorderThickness = 1
        Me.BtnRpmMinus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnRpmMinus25.Location = New System.Drawing.Point(408, 471)
        Me.BtnRpmMinus25.Name = "BtnRpmMinus25"
        Me.BtnRpmMinus25.Size = New System.Drawing.Size(30, 30)
        Me.BtnRpmMinus25.TabIndex = 99
        '
        'BtnRpmPlus25
        '
        Me.BtnRpmPlus25.Animated = True
        Me.BtnRpmPlus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.BorderRadius = 5
        Me.BtnRpmPlus25.BorderThickness = 1
        Me.BtnRpmPlus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnRpmPlus25.Location = New System.Drawing.Point(408, 435)
        Me.BtnRpmPlus25.Name = "BtnRpmPlus25"
        Me.BtnRpmPlus25.Size = New System.Drawing.Size(30, 30)
        Me.BtnRpmPlus25.TabIndex = 98
        '
        'PanelRPM
        '
        Me.PanelRPM.BackColor = System.Drawing.Color.Transparent
        Me.PanelRPM.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelRPM.BorderRadius = 6
        Me.PanelRPM.BorderThickness = 1
        Me.PanelRPM.Controls.Add(Me.BtnIdle)
        Me.PanelRPM.Controls.Add(Me.LblRPMActual)
        Me.PanelRPM.Controls.Add(Me.LblRPMDeseada)
        Me.PanelRPM.Controls.Add(Me.LblRPM3)
        Me.PanelRPM.Controls.Add(Me.LblRPM1)
        Me.PanelRPM.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.PanelRPM.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelRPM.FillColor = System.Drawing.Color.Transparent
        Me.PanelRPM.Location = New System.Drawing.Point(232, 322)
        Me.PanelRPM.Name = "PanelRPM"
        Me.PanelRPM.Size = New System.Drawing.Size(163, 257)
        Me.PanelRPM.TabIndex = 97
        '
        'TmrBackUp
        '
        Me.TmrBackUp.Interval = 1000
        '
        'BtnEKill
        '
        Me.BtnEKill.Animated = True
        Me.BtnEKill.BorderColor = System.Drawing.Color.White
        Me.BtnEKill.BorderRadius = 5
        Me.BtnEKill.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BtnEKill.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEKill.FillColor = System.Drawing.Color.Transparent
        Me.BtnEKill.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnEKill.ForeColor = System.Drawing.Color.White
        Me.BtnEKill.Image = Global.sFRAC.My.Resources.Resources.E_STOP
        Me.BtnEKill.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnEKill.ImageSize = New System.Drawing.Size(69, 69)
        Me.BtnEKill.Location = New System.Drawing.Point(252, 47)
        Me.BtnEKill.Name = "BtnEKill"
        Me.BtnEKill.Size = New System.Drawing.Size(70, 70)
        Me.BtnEKill.TabIndex = 188
        '
        'PbAlarms
        '
        Me.PbAlarms.FillColor = System.Drawing.Color.Silver
        Me.PbAlarms.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.PbAlarms.ForeColor = System.Drawing.Color.Transparent
        Me.PbAlarms.Location = New System.Drawing.Point(305, 708)
        Me.PbAlarms.Name = "PbAlarms"
        Me.PbAlarms.ProgressColor = System.Drawing.Color.DarkGray
        Me.PbAlarms.ProgressColor2 = System.Drawing.Color.DarkGray
        Me.PbAlarms.Size = New System.Drawing.Size(140, 10)
        Me.PbAlarms.TabIndex = 195
        Me.PbAlarms.Text = "LU"
        Me.PbAlarms.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.PbAlarms.Value = 100
        '
        'PbLockUp
        '
        Me.PbLockUp.FillColor = System.Drawing.Color.Silver
        Me.PbLockUp.Location = New System.Drawing.Point(159, 708)
        Me.PbLockUp.Name = "PbLockUp"
        Me.PbLockUp.ProgressColor = System.Drawing.Color.DarkGray
        Me.PbLockUp.ProgressColor2 = System.Drawing.Color.DarkGray
        Me.PbLockUp.Size = New System.Drawing.Size(140, 10)
        Me.PbLockUp.TabIndex = 194
        Me.PbLockUp.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.PbLockUp.Value = 100
        '
        'PbMoxaStatus
        '
        Me.PbMoxaStatus.Enabled = False
        Me.PbMoxaStatus.Location = New System.Drawing.Point(13, 708)
        Me.PbMoxaStatus.Name = "PbMoxaStatus"
        Me.PbMoxaStatus.ProgressColor = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.PbMoxaStatus.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.PbMoxaStatus.Size = New System.Drawing.Size(140, 10)
        Me.PbMoxaStatus.TabIndex = 193
        Me.PbMoxaStatus.Text = "Guna2ProgressBar1"
        Me.PbMoxaStatus.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.PbMoxaStatus.Value = 100
        '
        'BtnParking
        '
        Me.BtnParking.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnParking.Animated = True
        Me.BtnParking.BackColor = System.Drawing.Color.Transparent
        Me.BtnParking.BorderColor = System.Drawing.Color.White
        Me.BtnParking.BorderRadius = 5
        Me.BtnParking.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnParking.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnParking.FillColor = System.Drawing.Color.Transparent
        Me.BtnParking.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnParking.ForeColor = System.Drawing.Color.White
        Me.BtnParking.Image = Global.sFRAC.My.Resources.Resources.brake
        Me.BtnParking.ImageSize = New System.Drawing.Size(38, 38)
        Me.BtnParking.Location = New System.Drawing.Point(181, 449)
        Me.BtnParking.Name = "BtnParking"
        Me.BtnParking.Size = New System.Drawing.Size(44, 47)
        Me.BtnParking.TabIndex = 203
        '
        'FourFrac_1920x1080
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Controls.Add(Me.BtnParking)
        Me.Controls.Add(Me.PbAlarms)
        Me.Controls.Add(Me.PbLockUp)
        Me.Controls.Add(Me.PbMoxaStatus)
        Me.Controls.Add(Me.BtnEKill)
        Me.Controls.Add(Me.BtnResetRPM)
        Me.Controls.Add(Me.PbAutomatic)
        Me.Controls.Add(Me.BtnErrores)
        Me.Controls.Add(Me.BtnAjustes)
        Me.Controls.Add(Me.Guna2Panel10)
        Me.Controls.Add(Me.BtnMinusGear)
        Me.Controls.Add(Me.BtnStartEngine)
        Me.Controls.Add(Me.LblErrorAdmisible)
        Me.Controls.Add(Me.LblErrorGrave)
        Me.Controls.Add(Me.Guna2Panel9)
        Me.Controls.Add(Me.BtnPlusGear)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.BtnRpmMinus50)
        Me.Controls.Add(Me.BtnRpmPlus50)
        Me.Controls.Add(Me.BtnRpmMinus25)
        Me.Controls.Add(Me.BtnRpmPlus25)
        Me.Controls.Add(Me.PanelRPM)
        Me.DoubleBuffered = True
        Me.Name = "FourFrac_1920x1080"
        Me.Size = New System.Drawing.Size(456, 721)
        Me.Guna2CustomGradientPanel10.ResumeLayout(False)
        Me.Guna2CustomGradientPanel10.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2Panel10.ResumeLayout(False)
        Me.Guna2Panel10.PerformLayout()
        Me.Guna2Panel9.ResumeLayout(False)
        Me.Guna2Panel9.PerformLayout()
        Me.Guna2CustomGradientPanel9.ResumeLayout(False)
        Me.Guna2CustomGradientPanel9.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.PanelRPM.ResumeLayout(False)
        Me.PanelRPM.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public WithEvents Guna2CustomGradientPanel10 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblCaudal As Label
    Public WithEvents TmrCheck As Timer
    Public WithEvents LblMarchaActual As Label
    Public WithEvents LblMarchaDeseada As Label
    Public WithEvents LblMarcha3 As Label
    Public WithEvents LblMarcha1 As Label
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblMarcha As Label
    Public WithEvents BtnResetRPM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnIdle As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPMActual As Label
    Public WithEvents LblRPMDeseada As Label
    Public WithEvents LblRPM3 As Label
    Public WithEvents LblRPM1 As Label
    Public WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblRpm As Label
    Public WithEvents PbAutomatic As Guna.UI2.WinForms.Guna2PictureBox
    Public WithEvents BtnErrores As Guna.UI2.WinForms.Guna2Button
    Public WithEvents TmrReconnect As Timer
    Public WithEvents TmrErrorHandle As Timer
    Public WithEvents TmrOp As Timer
    Public WithEvents TmrRead As Timer
    Public WithEvents Label2 As Label
    Public WithEvents BtnAjustes As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel10 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnFC As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblCaudal2 As Label
    Public WithEvents LblCaudalActual As Label
    Public WithEvents BtnMinusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnStartEngine As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblErrorAdmisible As Label
    Public WithEvents LblErrorGrave As Label
    Public WithEvents Guna2Panel9 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnTripZero As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblPCorteActual As Label
    Public WithEvents LblPCorte3 As Label
    Public WithEvents Guna2CustomGradientPanel9 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblPCorte As Label
    Public WithEvents BtnPlusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnRpmMinus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmMinus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PanelRPM As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents TmrBackUp As Timer
    Public WithEvents BtnEKill As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents PbAlarms As Guna.UI2.WinForms.Guna2ProgressBar
    Friend WithEvents PbLockUp As Guna.UI2.WinForms.Guna2ProgressBar
    Friend WithEvents PbMoxaStatus As Guna.UI2.WinForms.Guna2ProgressBar
    Public WithEvents BtnParking As Guna.UI2.WinForms.Guna2Button
End Class
