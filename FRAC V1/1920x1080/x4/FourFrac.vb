﻿Imports System.Threading
Public Class FourFrac_1920x1080

    Public RPM As Integer
    Public Frac As Integer
    Public totalError As Integer

    Private MyEngine As Dictionary(Of String, Object)
    Private MyTransmission As Dictionary(Of String, Object)

    Private CSV_BACKUP As CSV
    Private CSV_OPERATION As CSV

    Private Sub FourFrac_1920x1080_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Se guarda en Frac (variable local para el UC) el fracturador de esta pantalla, el motor y backup de csv
        Frac = currentFrac
        MyEngine = FracData.SelectedEngines("SelectedEngine_" + Frac.ToString())
        MyTransmission = FracData.SelectedTransmissions("SelectedTransmission_" + Frac.ToString())
        If GenerateCSVBackUp Then
            CSV_BACKUP_INIT()
        End If

        ''Si estamos en una fractura automatica mostramos la pestaña para saberlo
        If FracAutomation.AutoStart = True Then
            PbHandle()
        End If

        ''Iniciamos lectura de datos y control de errores/diagnostico
        TmrRead.Start()
        TmrCheck.Start()
        TmrReconnect.Start()

        ''Agregamos handler por si se agrega un fracturador a la pantalla despues de iniciar una operacion/grafico
        AddHandler FracAutomation.AutomationStart, AddressOf PbHandleInvoke
        AddHandler IFracSystem.OperationStart, AddressOf OperationStart

        ''Chequeamos por diagnostico del fracturador
        FracDiagnostico.CheckFrac()
    End Sub

    ''Sub para centrar/alinear todos los objetos
    Public Sub CenterEverything()

        ''Panel RPM
        CenterObject(LblRpm)
        CenterObject(LblRPM1)
        CenterObject(LblRPMDeseada)
        CenterObject(LblRPM3)
        CenterObject(LblRPMActual)
        CenterObject(BtnIdle)

        ''Panel Marcha
        CenterObject(LblMarcha)
        CenterObject(LblMarcha1)
        CenterObject(LblMarchaDeseada)
        CenterObject(LblMarcha3)
        CenterObject(LblMarchaActual)

        ''Panel Presion de Corte
        CenterObject(LblPCorte)
        CenterObject(LblPCorte3)
        CenterObject(LblPCorteActual)
        CenterObject(BtnTripZero)

        ''Caudal
        CenterObject(LblCaudalActual)
        CenterObject(LblCaudal)

        ''Reset RPM
        CenterObject(BtnResetRPM)

    End Sub

    ''Region dedicada a la lectura y presentacion de los datos
#Region "READ - DISPLAY"
    Delegate Sub ReadThreadDelegate()
    Private Sub TmrRead_Tick(sender As Object, e As EventArgs) Handles TmrRead.Tick
        Dim ReadThread As New Task(AddressOf ReadThreadDo)
        ReadThread.Start()
    End Sub

    Dim IsAlive As Boolean = True
    Private Sub ReadThreadDo()
        Dim readResult As Integer() = ISystem.MoxaRead(0, TotalMoxaData, Frac)
        If readResult.Length() = TotalMoxaData Then
            FracDataAssign(readResult, Frac)
            GetMaintenance(readResult, Frac)
        ElseIf readResult.Length = 1 And IsAlive Then
            IsAlive = False
            TmrReconnect_Tick(TmrReconnect, EventArgs.Empty)
        End If
        Invoke(New ReadThreadDelegate(AddressOf DisplayData))
    End Sub

    Private Sub DisplayData()

        ''Leer errores
        totalError = FracData.DblVFrac("TotalErr_" + Frac.ToString())

        ''Display RPM
        LblRPMActual.Text = FracData.DblVFrac("RPMMotor_" + Frac.ToString())
        LblRPMDeseada.Text = FracData.DblVFrac("RPMDeseada_" + Frac.ToString())
        CenterObject(LblRPMActual)
        CenterObject(LblRPMDeseada)

        ''Display Marcha
        LblMarchaDeseada.Text = FracData.StrVFrac("MarchaDeseada_" + Frac.ToString())
        LblMarchaActual.Text = FracData.StrVFrac("MarchaActual_" + Frac.ToString())
        CenterObject(LblMarchaDeseada)
        CenterObject(LblMarchaActual)

        ''Display Caudal
        LblCaudalActual.Text = FracData.DblVFrac("Caudal_" + Frac.ToString())
        CenterObject(LblCaudalActual)

        ''Display Presion de Corte
        LblPCorteActual.Text = FracData.DblVFrac("Viatran_" + Frac.ToString())
        CenterObject(LblPCorteActual)

        ''Display Errores
        LblErrorGrave.Visible = FracData.DblVFrac("LamparaRoja_" + Frac.ToString())
        LblErrorAdmisible.Visible = FracData.DblVFrac("LamparaAmbar_" + Frac.ToString())

        If FracData.StrVFrac("MarchaDeseada_" + Frac.ToString()) = "N" Then
            BtnParking.Visible = True
        Else
            BtnParking.Visible = False
        End If

    End Sub

#End Region

    Private Sub RpmPlus(sender As Object, e As EventArgs) Handles BtnRpmPlus50.Click
        Dim CurrentRPM As Integer = FracData.DblVFrac("RPMMotor_" + Frac.ToString())
        Dim RpmPlusTh As New Task(Sub() ChangeRPM(CurrentRPM + 50, Frac))
        RpmPlusTh.Start()
    End Sub

    Private Sub RpmMinus(sender As Object, e As EventArgs) Handles BtnRpmMinus50.Click
        Dim CurrentRPM As Integer = FracData.DblVFrac("RPMMotor_" + Frac.ToString())
        Dim RpmMinusTh As New Task(Sub() ChangeRPM(CurrentRPM - 50, Frac))
        RpmMinusTh.Start()
    End Sub

    Private Sub BtnIdle_Click(sender As Object, e As EventArgs) Handles BtnIdle.Click
        Dim RpmIdleTh As New Task(Sub() IDLE(Frac))
        RpmIdleTh.Start()
    End Sub

    Private Sub LblRPMDeseada_Click(sender As Object, e As EventArgs) Handles LblRPMDeseada.Click
        currentFrac = Frac
        SetRPM.ShowDialog(Me)
    End Sub


    ''Region dedicada al control de Gear
#Region "Gear Select"
    Private Sub BtnPlusGear_Click(sender As Object, e As EventArgs) Handles BtnPlusGear.Click
        Dim GearPlusTh As New Task(Sub() GearPlus(Frac))
        GearPlusTh.Start()
    End Sub

    Private Sub BtnMinusGear_Click(sender As Object, e As EventArgs) Handles BtnMinusGear.Click
        Dim GearMinusTh As New Task(Sub() GearMinus(Frac))
        GearMinusTh.Start()
    End Sub

    Private Sub LblMarchaDeseada_Click(sender As Object, e As EventArgs) Handles LblMarchaDeseada.Click
        CurrentFrac = Frac
        SetGEAR.ShowDialog(Me)
    End Sub
#End Region

    ''Region para encender motor
#Region "EngineStart"
    Private Sub BtnStartEngine_Click(sender As Object, e As EventArgs) Handles BtnStartEngine.Click
        Dim EngineStartTh As New Task(AddressOf EngineStart)
        EngineStartTh.Start()
    End Sub

    Private Async Sub EngineStart()
        If BtnStartEngine.Checked = False Then
            ISystem.MoxaSend(FracMoxaSendStartAddress("Arranque_StartAddress"), FracGeneralValues("ArranqueON"), Frac)
            Await Task.Delay(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("Arranque_StartAddress"), FracGeneralValues("ArranqueOFF"), Frac)
        Else
            ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerON"), Frac)
            Await Task.Delay(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerOFF"), Frac)
        End If
    End Sub
#End Region

    ''Sub con Handler al control de TRIP
    Private Sub BtnTrip_Click(sender As Object, e As EventArgs)
        CurrentFrac = Frac
        SetTRIP.ShowDialog(Me)
    End Sub

    ''Sub para controlar el factor caudal
    Private Sub BtnFC_Click(sender As Object, e As EventArgs) Handles BtnFC.Click
        currentFrac = Frac
        SetFC.ShowDialog(Me)
    End Sub

    ''Sub para el control del offset de TRIP
    Private Sub BtnTripZero_Click(sender As Object, e As EventArgs) Handles BtnTripZero.Click
        currentFrac = Frac
        SetZERO.ShowDialog(Me)
    End Sub

    ''Encender luces del Fracturador
    Private Sub BtnEncenderLuces_Click(sender As Object, e As EventArgs)
        sender.Checked = Not sender.Checked
        HandleLuces(sender, Frac)
    End Sub

    ''Region para control de errores/diagnostico (TmrCheck)
#Region "Control de errores/diagnostico"
    Delegate Sub BtnDelegate()

    Dim ResetRPM As Boolean = True
    Dim EngineStatus As Boolean = False
    Dim ActiveError As Boolean = False

    Private Sub TmrCheck_Tick(sender As Object, e As EventArgs) Handles TmrCheck.Tick
        Dim CheckHandler As New Task(AddressOf BgCheck)
        CheckHandler.Start()
    End Sub

    Private Sub BgCheck()

        ''Vemos si el motor esta prendido
        If FracData.DblVFrac("RPMMotor_" + Frac.ToString) > 100 Then
            EngineStatus = True
        Else
            EngineStatus = False
        End If

        ''Vemos que errores hay
        If totalError <> 0 Then
            ActiveError = True
            TmrErrorHandle.Start()
        Else
            ActiveError = False
            TmrErrorHandle.Stop()
        End If

        If FracData.DblVFrac("HHPMotor_" + Frac.ToString()) >= 2240 Then
            BtnPlusGear.FillColor = Color.FromArgb(255, 64, 64)
        Else
            BtnPlusGear.FillColor = Color.Transparent
        End If

        If FracData.DblVFrac("ActiveAlarm_" + Frac.ToString()) Then
            PbAlarms.ProgressColor = Color.FromArgb(199, 55, 47)
            PbAlarms.ProgressColor2 = Color.FromArgb(199, 55, 47)
        Else
            PbAlarms.ProgressColor = Color.FromArgb(91, 194, 54)
            PbAlarms.ProgressColor2 = Color.FromArgb(91, 194, 54)
        End If

        If FracData.DblVFrac("LockUp_" + Frac.ToString()) Then
            PbLockUp.ProgressColor = Color.FromArgb(91, 194, 54)
            PbLockUp.ProgressColor2 = Color.FromArgb(91, 194, 54)
        Else
            PbLockUp.ProgressColor = Color.FromArgb(199, 55, 47)
            PbLockUp.ProgressColor2 = Color.FromArgb(199, 55, 47)
        End If

        Invoke(New BtnDelegate(AddressOf CheckUpdate))
    End Sub

    Private Sub CheckUpdate()

        BtnStartEngine.Checked = EngineStatus

        BtnErrores.Visible = ActiveError

        If FracData.DblVFrac("EstadoAcelerador_" + Frac.ToString()) = 0 And ResetRPM = False Then
            ResetRPM = True
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Advertencia")
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("PresionExcedida")
            BtnResetRPM.Visible = True
            If Application.OpenForms().OfType(Of AvisoGeneral).Any = False Then
                My.Computer.Audio.Play(My.Application.Info.DirectoryPath + "\alarm1.wav", AudioPlayMode.BackgroundLoop)
                If AvisoGeneral.ShowDialog(Me) = DialogResult.OK Then
                    BtnIdle.PerformClick()
                    My.Computer.Audio.Stop()
                    BtnResetRPM.FillColor = Color.FromArgb(255, 128, 128)
                End If
            End If
        ElseIf FracData.DblVFrac("EstadoAcelerador_" + Frac.ToString()) = 0 Then
            BtnResetRPM.Visible = True
            BtnResetRPM.FillColor = Color.FromArgb(255, 128, 128)
        ElseIf FracData.DblVFrac("EstadoAcelerador_" + Frac.ToString()) <> 0 Then
            BtnResetRPM.Visible = False
            ResetRPM = False
        End If

    End Sub
#End Region

    ''Sub para abrir ajustes de Fracturador
    Private Sub BtnAjustes_Click(sender As Object, e As EventArgs) Handles BtnAjustes.Click
        CurrentFrac = Frac
        AjustesFracturador.ShowDialog(Me)
    End Sub

    ''Sub para iniciar trabajo
    Private Sub BtnEmpezarTrabajo_Click(sender As Object, e As EventArgs)
        'If BtnEmpezarTrabajo.Checked = False Then
        '    OperationStartTime = DateTime.Now()                          ''Tiempo de inicio
        '    IFracSystem.ActiveOP = True
        'Else
        '    IFracSystem.ActiveOP = False
        'End If
    End Sub

    ''Sub para abrir pantalla de control semiautomatico
    Private Sub BtnSemiAuto_Click(sender As Object, e As EventArgs)
        CurrentFrac = Frac
        TesteoLinea.ShowDialog(Me)
    End Sub

    ''Sub para abrir pantalla de errores
    Private Sub BtnErrores_Click(sender As Object, e As EventArgs) Handles BtnErrores.Click
        currentFrac = Frac
        FracErrores.ShowDialog(Me)
    End Sub

    ''Sub para el control/informacion de fractura automatica
    Private Sub PbAutomatic_Click(sender As Object, e As EventArgs) Handles PbAutomatic.Click
        InfoShow.ShowDialog(Me)
    End Sub

    ''Sub para reconexion 
    Private Async Sub TmrReconnect_Tick(sender As Object, e As EventArgs) Handles TmrReconnect.Tick
        Try
            Dim MoxaStatus As Boolean = ISystem.CheckConnection(Frac)
            PbMoxaStatus.Enabled = MoxaStatus
            If Not IsAlive Then
                TmrReconnect.Stop()
                If Not MoxaStatus Then
                    TmrCheck.Stop()
                    TmrRead.Stop()
                    For ReconnectTry = 0 To 19
                        If Await Task.Run(AddressOf TryReconnect) Then
                            TmrCheck.Start()
                            TmrRead.Start()
                            TmrReconnect.Start()
                            IsAlive = True
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            'TmrReconnect.Stop()
            'avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Reconnect")
            'avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            'AvisoGeneral.ShowDialog(Me)
            'Principal.Close()
        End Try
    End Sub

    Public Async Function TryReconnect() As Task(Of Boolean)
        Await Task.Delay(2500)
        Return ISystem.Connect(Frac)
    End Function

    ''Seccion para activar el control/informacion de fractura automatica
    Delegate Sub PbHandleDelegate()
    Private Sub PbHandleInvoke()
        PbAutomatic.Invoke(New PbHandleDelegate(AddressOf PbHandle))
    End Sub
    Private Sub PbHandle()
        If FracAutomation.FracList.Contains(Frac) Then
            'BtnEmpezarTrabajo.PerformClick()
            PbAutomatic.Visible = FracAutomation.AutoStart
        Else
            PbAutomatic.Visible = False
        End If
    End Sub

    ''Seccion para avisar sobre errores presentes en el fracturador
    Private Sub TmrErrorHandle_Tick(sender As Object, e As EventArgs) Handles TmrErrorHandle.Tick
        Dim ErrorHandler As New Task(AddressOf ErrorHandle)
        ErrorHandler.Start()
    End Sub
    Dim ErrorI As Integer = 0
    Private Sub ErrorHandle()
        If ErrorI Mod 2 <> 0 Then
            BtnErrores.BackColor = Color.Maroon
        Else
            BtnErrores.BackColor = Color.FromArgb(49, 66, 82)
        End If
        ErrorI += 1
    End Sub

    ''Control del tiempo de operacion
    Private Async Sub TmrOp_Tick(sender As Object, e As EventArgs) Handles TmrOp.Tick
        Await Task.Run(Sub() WriteCSVRecord(CSV_OPERATION, Frac))
    End Sub

    ''Si se inicia grafico en otro fracturador con este prendido, se grafica tambien
    Private Sub OperationStart()
        If IFracSystem.ActiveOP = False Then
            TmrOp.Stop()
            If CSV_OPERATION IsNot Nothing Then
                CSV_OPERATION.CloseCSV()
                ActiveCSV.Remove(Frac)
            End If
            'BtnEmpezarTrabajo.Checked = False
            'BtnEmpezarTrabajo.Text = LanguageSelect.DynamicLanguageRM.GetString("EmpezarTrabajo")
        Else
            If Not ActiveCSV.Contains(Frac) Then
                If CSV_OPERATION_INIT(CSV_OPERATION, Frac) Then
                    TmrOp.Start()
                    'BtnEmpezarTrabajo.Checked = True
                    'BtnEmpezarTrabajo.Text = LanguageSelect.DynamicLanguageRM.GetString("FinalizarTrabajo")
                End If
            End If
        End If
    End Sub

    ''Seccion para hacer ResetRPM al Fracturador
    Private Sub BtnResetRPM_Click(sender As Object, e As EventArgs) Handles BtnResetRPM.Click
        Dim ResetRPMTh As New Task(Sub() ResetRPMDo(Frac))
        ResetRPMTh.Start()
    End Sub

    ''Sub para apagar todos los timers del fracturador
    Public Sub StopTimers()
        TmrCheck.Stop()
        TmrRead.Stop()
        TmrReconnect.Stop()
        TmrErrorHandle.Stop()
        TmrOp.Stop()
        TmrBackUp.Stop()

        If CSV_BACKUP IsNot Nothing Then
            CSV_BACKUP.CloseCSV()
        End If

        If CSV_OPERATION IsNot Nothing Then
            CSV_OPERATION.CloseCSV()
        End If

        RemoveHandler FracAutomation.AutomationStart, AddressOf PbHandleInvoke
        RemoveHandler IFracSystem.OperationStart, AddressOf OperationStart

    End Sub

#Region "BackUpCSV"
    Private Sub TmrBackUp_Tick(sender As Object, e As EventArgs) Handles TmrBackUp.Tick
        Dim BUThread As New Thread(Sub() WriteCSVRecord(CSV_BACKUP, Frac))
        BUThread.Start()
    End Sub

    Private Sub CSV_BACKUP_INIT()
        If Not GetDuplicateFracs(Frac) Then
            CSV_BACKUP = New CSV(ISystem.GetFracName(Frac), CSVBackUpPath, GenerateExcel, CSVFlushInterval)
            Dim Fields As ICollection(Of String) = {"Hora", "RPM Motor", "RPM Trans.", "Presion", "Caudal", "Temp. Agua Motor", "Temp. Aceite Motor",
                                                    "Presion Motor", "Temp. Trans.", "Presion Trans.", "Temp. LUB", "Presion Succ.", "Presion LUB", "Carga Motor"}
            CSV_BACKUP.WriteFields(Fields)
            TmrBackUp.Start()
        End If
    End Sub

    ''EKill Handle
    Private Sub BtnEKill_Click(sender As Object, e As EventArgs) Handles BtnEKill.Click
        Dim EKillTask As New Task(Sub() EKill(Frac))
        EKillTask.Start()
    End Sub

    ''' <summary>
    ''' Sub para mandar fracturador a parking
    ''' </summary>
    Private Sub BtnParking_Click(sender As Object, e As EventArgs) Handles BtnParking.Click
        'BtnParking.Checked = Not BtnParking.Checked
        Dim ParkTask As New Task(Sub() SetParking(Frac))
        ParkTask.Start()
    End Sub
#End Region

End Class
