﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Container4_1920x1080
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Container4_1920x1080))
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Guna2CustomGradientPanel5 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize4 = New Guna.UI2.WinForms.Guna2Button()
        Me.Title4 = New System.Windows.Forms.Label()
        Me.CloseButton4 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.M4 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.BtnAdd4 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd1 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize1 = New Guna.UI2.WinForms.Guna2Button()
        Me.M1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Title1 = New System.Windows.Forms.Label()
        Me.CloseButton1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd2 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize2 = New Guna.UI2.WinForms.Guna2Button()
        Me.Title2 = New System.Windows.Forms.Label()
        Me.CloseButton2 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.M2 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel5 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel3 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd3 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize3 = New Guna.UI2.WinForms.Guna2Button()
        Me.Title3 = New System.Windows.Forms.Label()
        Me.CloseButton3 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.M3 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel9 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2Panel3 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2VSeparator4 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.BtnLockAllFracs = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Separator1 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2VSeparator3 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator2 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator1 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.BtnEncenderLuces = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnEmpezarTrabajo = New Guna.UI2.WinForms.Guna2Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BtnMinusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPlusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.LblRPMActual = New System.Windows.Forms.Label()
        Me.BtnRpmMinus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnTRIPAll = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnLockFrac4 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnLockFrac3 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnLockFrac2 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnLockFrac1 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel5.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.Guna2Panel2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        Me.Guna2Panel5.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.Guna2Panel9.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Guna2Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.HasFormShadow = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Guna2CustomGradientPanel5
        '
        Me.Guna2CustomGradientPanel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.BorderRadius = 6
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.Maximize4)
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.Title4)
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.CloseButton4)
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.M4)
        Me.Guna2CustomGradientPanel5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Guna2CustomGradientPanel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel5.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel5.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel5.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel5.Name = "Guna2CustomGradientPanel5"
        Me.Guna2CustomGradientPanel5.Size = New System.Drawing.Size(458, 23)
        Me.Guna2CustomGradientPanel5.TabIndex = 19
        '
        'Maximize4
        '
        Me.Maximize4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize4.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize4.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize4.Enabled = False
        Me.Maximize4.FillColor = System.Drawing.Color.Transparent
        Me.Maximize4.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize4.ForeColor = System.Drawing.Color.White
        Me.Maximize4.Location = New System.Drawing.Point(409, 1)
        Me.Maximize4.Name = "Maximize4"
        Me.Maximize4.Size = New System.Drawing.Size(20, 20)
        Me.Maximize4.TabIndex = 76
        Me.Maximize4.UseTransparentBackground = True
        '
        'Title4
        '
        Me.Title4.AutoSize = True
        Me.Title4.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Title4.ForeColor = System.Drawing.Color.White
        Me.Title4.Location = New System.Drawing.Point(201, 1)
        Me.Title4.Name = "Title4"
        Me.Title4.Size = New System.Drawing.Size(57, 21)
        Me.Title4.TabIndex = 19
        Me.Title4.Text = "Titulo1"
        '
        'CloseButton4
        '
        Me.CloseButton4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton4.Enabled = False
        Me.CloseButton4.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton4.IconColor = System.Drawing.Color.White
        Me.CloseButton4.Location = New System.Drawing.Point(435, 1)
        Me.CloseButton4.Name = "CloseButton4"
        Me.CloseButton4.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton4.TabIndex = 72
        '
        'M4
        '
        Me.M4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M4.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M4.Enabled = False
        Me.M4.FillColor = System.Drawing.Color.Transparent
        Me.M4.IconColor = System.Drawing.Color.White
        Me.M4.Location = New System.Drawing.Point(409, 1)
        Me.M4.Name = "M4"
        Me.M4.Size = New System.Drawing.Size(20, 20)
        Me.M4.TabIndex = 75
        '
        'BtnAdd4
        '
        Me.BtnAdd4.AutoRoundedCorners = True
        Me.BtnAdd4.BorderRadius = 49
        Me.BtnAdd4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd4.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd4.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd4.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd4.ForeColor = System.Drawing.Color.White
        Me.BtnAdd4.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd4.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd4.Location = New System.Drawing.Point(179, 334)
        Me.BtnAdd4.Name = "BtnAdd4"
        Me.BtnAdd4.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd4.TabIndex = 0
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.Panel1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(458, 745)
        Me.Guna2Panel1.TabIndex = 80
        '
        'Panel1
        '
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel1.Controls.Add(Me.BtnAdd1)
        Me.Panel1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Panel1.Location = New System.Drawing.Point(1, 23)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(456, 722)
        Me.Panel1.TabIndex = 57
        '
        'BtnAdd1
        '
        Me.BtnAdd1.AutoRoundedCorners = True
        Me.BtnAdd1.BorderRadius = 49
        Me.BtnAdd1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd1.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd1.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd1.ForeColor = System.Drawing.Color.White
        Me.BtnAdd1.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd1.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd1.Location = New System.Drawing.Point(179, 334)
        Me.BtnAdd1.Name = "BtnAdd1"
        Me.BtnAdd1.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd1.TabIndex = 0
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.Maximize1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.M1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.Title1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.CloseButton1)
        Me.Guna2CustomGradientPanel1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Guna2CustomGradientPanel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel1.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(458, 23)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'Maximize1
        '
        Me.Maximize1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize1.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize1.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize1.Enabled = False
        Me.Maximize1.FillColor = System.Drawing.Color.Transparent
        Me.Maximize1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize1.ForeColor = System.Drawing.Color.White
        Me.Maximize1.Location = New System.Drawing.Point(409, 1)
        Me.Maximize1.Name = "Maximize1"
        Me.Maximize1.Size = New System.Drawing.Size(20, 20)
        Me.Maximize1.TabIndex = 73
        Me.Maximize1.UseTransparentBackground = True
        '
        'M1
        '
        Me.M1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M1.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M1.Enabled = False
        Me.M1.FillColor = System.Drawing.Color.Transparent
        Me.M1.IconColor = System.Drawing.Color.White
        Me.M1.Location = New System.Drawing.Point(409, 1)
        Me.M1.Name = "M1"
        Me.M1.Size = New System.Drawing.Size(20, 20)
        Me.M1.TabIndex = 1
        '
        'Title1
        '
        Me.Title1.AutoSize = True
        Me.Title1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Title1.ForeColor = System.Drawing.Color.White
        Me.Title1.Location = New System.Drawing.Point(201, 1)
        Me.Title1.Name = "Title1"
        Me.Title1.Size = New System.Drawing.Size(57, 21)
        Me.Title1.TabIndex = 19
        Me.Title1.Text = "Titulo1"
        '
        'CloseButton1
        '
        Me.CloseButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton1.Enabled = False
        Me.CloseButton1.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton1.IconColor = System.Drawing.Color.White
        Me.CloseButton1.Location = New System.Drawing.Point(435, 1)
        Me.CloseButton1.Name = "CloseButton1"
        Me.CloseButton1.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton1.TabIndex = 72
        '
        'Guna2Panel2
        '
        Me.Guna2Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel2.BorderRadius = 6
        Me.Guna2Panel2.BorderThickness = 1
        Me.Guna2Panel2.Controls.Add(Me.Panel2)
        Me.Guna2Panel2.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.Guna2Panel2.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.Location = New System.Drawing.Point(476, 12)
        Me.Guna2Panel2.Name = "Guna2Panel2"
        Me.Guna2Panel2.Size = New System.Drawing.Size(458, 745)
        Me.Guna2Panel2.TabIndex = 81
        '
        'Panel2
        '
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel2.Controls.Add(Me.BtnAdd2)
        Me.Panel2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Panel2.Location = New System.Drawing.Point(1, 23)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(456, 722)
        Me.Panel2.TabIndex = 57
        '
        'BtnAdd2
        '
        Me.BtnAdd2.AutoRoundedCorners = True
        Me.BtnAdd2.BorderRadius = 49
        Me.BtnAdd2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd2.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd2.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd2.ForeColor = System.Drawing.Color.White
        Me.BtnAdd2.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd2.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd2.Location = New System.Drawing.Point(179, 334)
        Me.BtnAdd2.Name = "BtnAdd2"
        Me.BtnAdd2.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd2.TabIndex = 0
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.Maximize2)
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.Title2)
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.CloseButton2)
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.M2)
        Me.Guna2CustomGradientPanel2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Guna2CustomGradientPanel2.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel2.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(458, 23)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'Maximize2
        '
        Me.Maximize2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize2.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize2.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize2.Enabled = False
        Me.Maximize2.FillColor = System.Drawing.Color.Transparent
        Me.Maximize2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize2.ForeColor = System.Drawing.Color.White
        Me.Maximize2.Location = New System.Drawing.Point(409, 1)
        Me.Maximize2.Name = "Maximize2"
        Me.Maximize2.Size = New System.Drawing.Size(20, 20)
        Me.Maximize2.TabIndex = 74
        Me.Maximize2.UseTransparentBackground = True
        '
        'Title2
        '
        Me.Title2.AutoSize = True
        Me.Title2.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Title2.ForeColor = System.Drawing.Color.White
        Me.Title2.Location = New System.Drawing.Point(201, 1)
        Me.Title2.Name = "Title2"
        Me.Title2.Size = New System.Drawing.Size(57, 21)
        Me.Title2.TabIndex = 19
        Me.Title2.Text = "Titulo1"
        '
        'CloseButton2
        '
        Me.CloseButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton2.Enabled = False
        Me.CloseButton2.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton2.IconColor = System.Drawing.Color.White
        Me.CloseButton2.Location = New System.Drawing.Point(435, 1)
        Me.CloseButton2.Name = "CloseButton2"
        Me.CloseButton2.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton2.TabIndex = 72
        Me.CloseButton2.UseTransparentBackground = True
        '
        'M2
        '
        Me.M2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M2.Enabled = False
        Me.M2.FillColor = System.Drawing.Color.Transparent
        Me.M2.IconColor = System.Drawing.Color.White
        Me.M2.Location = New System.Drawing.Point(409, 1)
        Me.M2.Name = "M2"
        Me.M2.Size = New System.Drawing.Size(20, 20)
        Me.M2.TabIndex = 73
        '
        'Guna2Panel5
        '
        Me.Guna2Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel5.BorderRadius = 6
        Me.Guna2Panel5.BorderThickness = 1
        Me.Guna2Panel5.Controls.Add(Me.Panel3)
        Me.Guna2Panel5.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.Guna2Panel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.Location = New System.Drawing.Point(940, 12)
        Me.Guna2Panel5.Name = "Guna2Panel5"
        Me.Guna2Panel5.Size = New System.Drawing.Size(458, 745)
        Me.Guna2Panel5.TabIndex = 82
        '
        'Panel3
        '
        Me.Panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel3.Controls.Add(Me.BtnAdd3)
        Me.Panel3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Panel3.Location = New System.Drawing.Point(1, 23)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(456, 722)
        Me.Panel3.TabIndex = 57
        '
        'BtnAdd3
        '
        Me.BtnAdd3.AutoRoundedCorners = True
        Me.BtnAdd3.BorderRadius = 49
        Me.BtnAdd3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd3.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd3.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd3.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd3.ForeColor = System.Drawing.Color.White
        Me.BtnAdd3.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd3.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd3.Location = New System.Drawing.Point(179, 334)
        Me.BtnAdd3.Name = "BtnAdd3"
        Me.BtnAdd3.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd3.TabIndex = 0
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.Maximize3)
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.Title3)
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.CloseButton3)
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.M3)
        Me.Guna2CustomGradientPanel4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Guna2CustomGradientPanel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel4.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(458, 23)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'Maximize3
        '
        Me.Maximize3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize3.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize3.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize3.Enabled = False
        Me.Maximize3.FillColor = System.Drawing.Color.Transparent
        Me.Maximize3.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize3.ForeColor = System.Drawing.Color.White
        Me.Maximize3.Location = New System.Drawing.Point(409, 1)
        Me.Maximize3.Name = "Maximize3"
        Me.Maximize3.Size = New System.Drawing.Size(20, 20)
        Me.Maximize3.TabIndex = 75
        Me.Maximize3.UseTransparentBackground = True
        '
        'Title3
        '
        Me.Title3.AutoSize = True
        Me.Title3.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Title3.ForeColor = System.Drawing.Color.White
        Me.Title3.Location = New System.Drawing.Point(201, 1)
        Me.Title3.Name = "Title3"
        Me.Title3.Size = New System.Drawing.Size(57, 21)
        Me.Title3.TabIndex = 19
        Me.Title3.Text = "Titulo1"
        '
        'CloseButton3
        '
        Me.CloseButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton3.Enabled = False
        Me.CloseButton3.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton3.IconColor = System.Drawing.Color.White
        Me.CloseButton3.Location = New System.Drawing.Point(435, 1)
        Me.CloseButton3.Name = "CloseButton3"
        Me.CloseButton3.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton3.TabIndex = 72
        '
        'M3
        '
        Me.M3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M3.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M3.Enabled = False
        Me.M3.FillColor = System.Drawing.Color.Transparent
        Me.M3.IconColor = System.Drawing.Color.White
        Me.M3.Location = New System.Drawing.Point(409, 1)
        Me.M3.Name = "M3"
        Me.M3.Size = New System.Drawing.Size(20, 20)
        Me.M3.TabIndex = 74
        '
        'Guna2Panel9
        '
        Me.Guna2Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel9.BorderRadius = 6
        Me.Guna2Panel9.BorderThickness = 1
        Me.Guna2Panel9.Controls.Add(Me.Panel4)
        Me.Guna2Panel9.Controls.Add(Me.Guna2CustomGradientPanel5)
        Me.Guna2Panel9.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.Location = New System.Drawing.Point(1404, 12)
        Me.Guna2Panel9.Name = "Guna2Panel9"
        Me.Guna2Panel9.Size = New System.Drawing.Size(458, 745)
        Me.Guna2Panel9.TabIndex = 83
        '
        'Panel4
        '
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel4.Controls.Add(Me.BtnAdd4)
        Me.Panel4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Panel4.Location = New System.Drawing.Point(1, 23)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(456, 722)
        Me.Panel4.TabIndex = 57
        '
        'Guna2Panel3
        '
        Me.Guna2Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel3.BorderRadius = 6
        Me.Guna2Panel3.BorderThickness = 1
        Me.Guna2Panel3.Controls.Add(Me.Guna2VSeparator4)
        Me.Guna2Panel3.Controls.Add(Me.BtnLockAllFracs)
        Me.Guna2Panel3.Controls.Add(Me.Guna2Separator1)
        Me.Guna2Panel3.Controls.Add(Me.Guna2VSeparator3)
        Me.Guna2Panel3.Controls.Add(Me.Guna2VSeparator2)
        Me.Guna2Panel3.Controls.Add(Me.Guna2VSeparator1)
        Me.Guna2Panel3.Controls.Add(Me.BtnEncenderLuces)
        Me.Guna2Panel3.Controls.Add(Me.BtnEmpezarTrabajo)
        Me.Guna2Panel3.Controls.Add(Me.Label1)
        Me.Guna2Panel3.Controls.Add(Me.BtnMinusGear)
        Me.Guna2Panel3.Controls.Add(Me.BtnPlusGear)
        Me.Guna2Panel3.Controls.Add(Me.LblRPMActual)
        Me.Guna2Panel3.Controls.Add(Me.BtnRpmMinus50)
        Me.Guna2Panel3.Controls.Add(Me.BtnRpmPlus50)
        Me.Guna2Panel3.Controls.Add(Me.BtnTRIPAll)
        Me.Guna2Panel3.Controls.Add(Me.BtnLockFrac4)
        Me.Guna2Panel3.Controls.Add(Me.BtnLockFrac3)
        Me.Guna2Panel3.Controls.Add(Me.BtnLockFrac2)
        Me.Guna2Panel3.Controls.Add(Me.BtnLockFrac1)
        Me.Guna2Panel3.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel3.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel3.Location = New System.Drawing.Point(12, 763)
        Me.Guna2Panel3.Name = "Guna2Panel3"
        Me.Guna2Panel3.Size = New System.Drawing.Size(1851, 151)
        Me.Guna2Panel3.TabIndex = 93
        '
        'Guna2VSeparator4
        '
        Me.Guna2VSeparator4.Location = New System.Drawing.Point(153, 52)
        Me.Guna2VSeparator4.Name = "Guna2VSeparator4"
        Me.Guna2VSeparator4.Size = New System.Drawing.Size(10, 90)
        Me.Guna2VSeparator4.TabIndex = 216
        '
        'BtnLockAllFracs
        '
        Me.BtnLockAllFracs.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnLockAllFracs.Animated = True
        Me.BtnLockAllFracs.BorderColor = System.Drawing.Color.White
        Me.BtnLockAllFracs.BorderRadius = 5
        Me.BtnLockAllFracs.BorderThickness = 1
        Me.BtnLockAllFracs.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.BtnLockAllFracs.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnLockAllFracs.FillColor = System.Drawing.Color.Transparent
        Me.BtnLockAllFracs.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.BtnLockAllFracs.ForeColor = System.Drawing.Color.White
        Me.BtnLockAllFracs.ImageSize = New System.Drawing.Size(60, 60)
        Me.BtnLockAllFracs.Location = New System.Drawing.Point(18, 62)
        Me.BtnLockAllFracs.Name = "BtnLockAllFracs"
        Me.BtnLockAllFracs.Size = New System.Drawing.Size(101, 70)
        Me.BtnLockAllFracs.TabIndex = 215
        Me.BtnLockAllFracs.Text = "Activar Todos"
        '
        'Guna2Separator1
        '
        Me.Guna2Separator1.Location = New System.Drawing.Point(17, 36)
        Me.Guna2Separator1.Name = "Guna2Separator1"
        Me.Guna2Separator1.Size = New System.Drawing.Size(1812, 10)
        Me.Guna2Separator1.TabIndex = 214
        '
        'Guna2VSeparator3
        '
        Me.Guna2VSeparator3.Location = New System.Drawing.Point(746, 52)
        Me.Guna2VSeparator3.Name = "Guna2VSeparator3"
        Me.Guna2VSeparator3.Size = New System.Drawing.Size(10, 90)
        Me.Guna2VSeparator3.TabIndex = 213
        '
        'Guna2VSeparator2
        '
        Me.Guna2VSeparator2.Location = New System.Drawing.Point(1545, 52)
        Me.Guna2VSeparator2.Name = "Guna2VSeparator2"
        Me.Guna2VSeparator2.Size = New System.Drawing.Size(10, 90)
        Me.Guna2VSeparator2.TabIndex = 212
        '
        'Guna2VSeparator1
        '
        Me.Guna2VSeparator1.Location = New System.Drawing.Point(1154, 52)
        Me.Guna2VSeparator1.Name = "Guna2VSeparator1"
        Me.Guna2VSeparator1.Size = New System.Drawing.Size(10, 90)
        Me.Guna2VSeparator1.TabIndex = 211
        '
        'BtnEncenderLuces
        '
        Me.BtnEncenderLuces.Animated = True
        Me.BtnEncenderLuces.BorderColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.BorderRadius = 5
        Me.BtnEncenderLuces.BorderThickness = 1
        Me.BtnEncenderLuces.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEncenderLuces.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnEncenderLuces.FillColor = System.Drawing.Color.Transparent
        Me.BtnEncenderLuces.Font = New System.Drawing.Font("Montserrat", 18.0!)
        Me.BtnEncenderLuces.ForeColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.Location = New System.Drawing.Point(472, 62)
        Me.BtnEncenderLuces.Name = "BtnEncenderLuces"
        Me.BtnEncenderLuces.Size = New System.Drawing.Size(239, 70)
        Me.BtnEncenderLuces.TabIndex = 210
        Me.BtnEncenderLuces.Text = "Encender Luces"
        Me.BtnEncenderLuces.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        '
        'BtnEmpezarTrabajo
        '
        Me.BtnEmpezarTrabajo.Animated = True
        Me.BtnEmpezarTrabajo.BorderColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.BorderRadius = 5
        Me.BtnEmpezarTrabajo.BorderThickness = 1
        Me.BtnEmpezarTrabajo.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEmpezarTrabajo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEmpezarTrabajo.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnEmpezarTrabajo.FillColor = System.Drawing.Color.Transparent
        Me.BtnEmpezarTrabajo.Font = New System.Drawing.Font("Montserrat", 18.0!)
        Me.BtnEmpezarTrabajo.ForeColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.Location = New System.Drawing.Point(198, 62)
        Me.BtnEmpezarTrabajo.Name = "BtnEmpezarTrabajo"
        Me.BtnEmpezarTrabajo.Size = New System.Drawing.Size(239, 70)
        Me.BtnEmpezarTrabajo.TabIndex = 209
        Me.BtnEmpezarTrabajo.Text = "Empezar Trabajo"
        Me.BtnEmpezarTrabajo.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(896, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(118, 47)
        Me.Label1.TabIndex = 208
        Me.Label1.Text = "GEAR"
        '
        'BtnMinusGear
        '
        Me.BtnMinusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnMinusGear.Animated = True
        Me.BtnMinusGear.BorderColor = System.Drawing.Color.White
        Me.BtnMinusGear.BorderRadius = 5
        Me.BtnMinusGear.BorderThickness = 1
        Me.BtnMinusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMinusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnMinusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnMinusGear.ForeColor = System.Drawing.Color.White
        Me.BtnMinusGear.Image = CType(resources.GetObject("BtnMinusGear.Image"), System.Drawing.Image)
        Me.BtnMinusGear.ImageSize = New System.Drawing.Size(70, 70)
        Me.BtnMinusGear.Location = New System.Drawing.Point(792, 62)
        Me.BtnMinusGear.Name = "BtnMinusGear"
        Me.BtnMinusGear.Size = New System.Drawing.Size(70, 70)
        Me.BtnMinusGear.TabIndex = 207
        '
        'BtnPlusGear
        '
        Me.BtnPlusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnPlusGear.Animated = True
        Me.BtnPlusGear.BorderColor = System.Drawing.Color.White
        Me.BtnPlusGear.BorderRadius = 5
        Me.BtnPlusGear.BorderThickness = 1
        Me.BtnPlusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPlusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnPlusGear.ForeColor = System.Drawing.Color.White
        Me.BtnPlusGear.Image = CType(resources.GetObject("BtnPlusGear.Image"), System.Drawing.Image)
        Me.BtnPlusGear.ImageSize = New System.Drawing.Size(70, 70)
        Me.BtnPlusGear.Location = New System.Drawing.Point(1050, 62)
        Me.BtnPlusGear.Name = "BtnPlusGear"
        Me.BtnPlusGear.Size = New System.Drawing.Size(70, 70)
        Me.BtnPlusGear.TabIndex = 206
        '
        'LblRPMActual
        '
        Me.LblRPMActual.AutoSize = True
        Me.LblRPMActual.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.LblRPMActual.ForeColor = System.Drawing.Color.White
        Me.LblRPMActual.Location = New System.Drawing.Point(1304, 74)
        Me.LblRPMActual.Name = "LblRPMActual"
        Me.LblRPMActual.Size = New System.Drawing.Size(101, 47)
        Me.LblRPMActual.TabIndex = 205
        Me.LblRPMActual.Text = "RPM"
        '
        'BtnRpmMinus50
        '
        Me.BtnRpmMinus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmMinus50.Animated = True
        Me.BtnRpmMinus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.BorderRadius = 5
        Me.BtnRpmMinus50.BorderThickness = 1
        Me.BtnRpmMinus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.Image = CType(resources.GetObject("BtnRpmMinus50.Image"), System.Drawing.Image)
        Me.BtnRpmMinus50.ImageSize = New System.Drawing.Size(70, 70)
        Me.BtnRpmMinus50.Location = New System.Drawing.Point(1200, 62)
        Me.BtnRpmMinus50.Name = "BtnRpmMinus50"
        Me.BtnRpmMinus50.Size = New System.Drawing.Size(70, 70)
        Me.BtnRpmMinus50.TabIndex = 204
        '
        'BtnRpmPlus50
        '
        Me.BtnRpmPlus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmPlus50.Animated = True
        Me.BtnRpmPlus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.BorderRadius = 5
        Me.BtnRpmPlus50.BorderThickness = 1
        Me.BtnRpmPlus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.Image = CType(resources.GetObject("BtnRpmPlus50.Image"), System.Drawing.Image)
        Me.BtnRpmPlus50.ImageSize = New System.Drawing.Size(70, 70)
        Me.BtnRpmPlus50.Location = New System.Drawing.Point(1441, 62)
        Me.BtnRpmPlus50.Name = "BtnRpmPlus50"
        Me.BtnRpmPlus50.Size = New System.Drawing.Size(70, 70)
        Me.BtnRpmPlus50.TabIndex = 203
        '
        'BtnTRIPAll
        '
        Me.BtnTRIPAll.Animated = True
        Me.BtnTRIPAll.BorderColor = System.Drawing.Color.White
        Me.BtnTRIPAll.BorderRadius = 5
        Me.BtnTRIPAll.BorderThickness = 1
        Me.BtnTRIPAll.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTRIPAll.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnTRIPAll.FillColor = System.Drawing.Color.Transparent
        Me.BtnTRIPAll.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.BtnTRIPAll.ForeColor = System.Drawing.Color.White
        Me.BtnTRIPAll.Location = New System.Drawing.Point(1590, 62)
        Me.BtnTRIPAll.Name = "BtnTRIPAll"
        Me.BtnTRIPAll.Size = New System.Drawing.Size(239, 70)
        Me.BtnTRIPAll.TabIndex = 202
        Me.BtnTRIPAll.Text = "TRIP"
        Me.BtnTRIPAll.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        '
        'BtnLockFrac4
        '
        Me.BtnLockFrac4.BorderRadius = 5
        Me.BtnLockFrac4.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.BtnLockFrac4.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac4.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.BtnLockFrac4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.BtnLockFrac4.Enabled = False
        Me.BtnLockFrac4.FillColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac4.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnLockFrac4.ForeColor = System.Drawing.Color.White
        Me.BtnLockFrac4.Location = New System.Drawing.Point(1572, 15)
        Me.BtnLockFrac4.Name = "BtnLockFrac4"
        Me.BtnLockFrac4.Size = New System.Drawing.Size(100, 10)
        Me.BtnLockFrac4.TabIndex = 201
        '
        'BtnLockFrac3
        '
        Me.BtnLockFrac3.BorderRadius = 5
        Me.BtnLockFrac3.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.BtnLockFrac3.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac3.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.BtnLockFrac3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.BtnLockFrac3.Enabled = False
        Me.BtnLockFrac3.FillColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac3.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnLockFrac3.ForeColor = System.Drawing.Color.White
        Me.BtnLockFrac3.Location = New System.Drawing.Point(1108, 15)
        Me.BtnLockFrac3.Name = "BtnLockFrac3"
        Me.BtnLockFrac3.Size = New System.Drawing.Size(100, 10)
        Me.BtnLockFrac3.TabIndex = 200
        '
        'BtnLockFrac2
        '
        Me.BtnLockFrac2.BorderRadius = 5
        Me.BtnLockFrac2.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.BtnLockFrac2.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac2.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.BtnLockFrac2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.BtnLockFrac2.Enabled = False
        Me.BtnLockFrac2.FillColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnLockFrac2.ForeColor = System.Drawing.Color.White
        Me.BtnLockFrac2.Location = New System.Drawing.Point(644, 15)
        Me.BtnLockFrac2.Name = "BtnLockFrac2"
        Me.BtnLockFrac2.Size = New System.Drawing.Size(100, 10)
        Me.BtnLockFrac2.TabIndex = 199
        '
        'BtnLockFrac1
        '
        Me.BtnLockFrac1.BorderRadius = 5
        Me.BtnLockFrac1.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.BtnLockFrac1.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac1.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.BtnLockFrac1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.BtnLockFrac1.FillColor = System.Drawing.Color.DarkGray
        Me.BtnLockFrac1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnLockFrac1.ForeColor = System.Drawing.Color.White
        Me.BtnLockFrac1.Location = New System.Drawing.Point(180, 15)
        Me.BtnLockFrac1.Name = "BtnLockFrac1"
        Me.BtnLockFrac1.Size = New System.Drawing.Size(100, 10)
        Me.BtnLockFrac1.TabIndex = 198
        '
        'Container4_1920x1080
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1875, 926)
        Me.Controls.Add(Me.Guna2Panel3)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.Guna2Panel2)
        Me.Controls.Add(Me.Guna2Panel5)
        Me.Controls.Add(Me.Guna2Panel9)
        Me.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Container4_1920x1080"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Container4"
        Me.Guna2CustomGradientPanel5.ResumeLayout(False)
        Me.Guna2CustomGradientPanel5.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.Guna2Panel2.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        Me.Guna2Panel5.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.Guna2Panel9.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Guna2Panel3.ResumeLayout(False)
        Me.Guna2Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Public WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnAdd1 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Maximize1 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents M1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Title1 As Label
    Public WithEvents CloseButton1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Guna2Panel2 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel2 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnAdd2 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Maximize2 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Title2 As Label
    Public WithEvents CloseButton2 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M2 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Guna2Panel5 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel3 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnAdd3 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Maximize3 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Title3 As Label
    Public WithEvents CloseButton3 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M3 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Guna2Panel9 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel4 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnAdd4 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel5 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Maximize4 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Title4 As Label
    Public WithEvents CloseButton4 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M4 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Guna2Panel3 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2VSeparator4 As Guna.UI2.WinForms.Guna2VSeparator
    Public WithEvents BtnLockAllFracs As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Guna2Separator1 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2VSeparator3 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator2 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator1 As Guna.UI2.WinForms.Guna2VSeparator
    Public WithEvents BtnEncenderLuces As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnEmpezarTrabajo As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Label1 As Label
    Public WithEvents BtnMinusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnPlusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPMActual As Label
    Public WithEvents BtnRpmMinus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnTRIPAll As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents BtnLockFrac4 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents BtnLockFrac3 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents BtnLockFrac2 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents BtnLockFrac1 As Guna.UI2.WinForms.Guna2Button
End Class
