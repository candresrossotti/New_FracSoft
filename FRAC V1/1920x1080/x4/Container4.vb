﻿Imports System.Threading
Public Class Container4_1920x1080

    Private LockedFracs As New List(Of Integer)
    Private Sub Container4_1920x1080_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Centramos el boton de add
        CenterObject(BtnAdd1)
        CenterObject(BtnAdd2)
        CenterObject(BtnAdd3)
        CenterObject(BtnAdd4)

        ''Centramos el resto de los controles
        CenterEverything()

        ''Centramos verticalmente el control de add
        BtnAdd1.Top = (BtnAdd1.Parent.Height \ 2) - (BtnAdd1.Height \ 2)
        BtnAdd2.Top = (BtnAdd2.Parent.Height \ 2) - (BtnAdd2.Height \ 2)
        BtnAdd3.Top = (BtnAdd3.Parent.Height \ 2) - (BtnAdd3.Height \ 2)
        BtnAdd4.Top = (BtnAdd4.Parent.Height \ 2) - (BtnAdd4.Height \ 2)


        AddHandler IFracSystem.OperationStart, AddressOf OperationStart
    End Sub

    ''Sub para centrar/alinear todos los objetos
    Public Sub CenterEverything()
        CenterObject(Title1)
        CenterObject(Title2)
        CenterObject(Title3)
        CenterObject(Title4)
    End Sub

    ''Sub para agregar fracturador a la pantalla
    Private Sub BtnAdd(sender As Object, e As EventArgs) Handles BtnAdd1.Click, BtnAdd2.Click, BtnAdd3.Click, BtnAdd4.Click
        If IFracSystem.ActiveOP = False Then
            If FracSelect.ShowDialog(Me) = DialogResult.OK Then
                Dim CurrentScreen As Integer = GetInteger(sender.Name)

                Dim LockButton As Control() = Controls.Find("BtnLockFrac" + CurrentScreen.ToString, True)
                LockButton(0).Enabled = True

                Dim MaximizeButton As Control() = Me.Controls.Find("Maximize" + CurrentScreen.ToString(), True)
                MaximizeButton(0).Enabled = True

                Dim CloseButton As Control() = Me.Controls.Find("CloseButton" + CurrentScreen.ToString, True)
                CloseButton(0).Enabled = True

                Dim AnotherFrac = FourFrac.GetClass()
                Dim CurrentPanel As Control() = Me.Controls.Find("Panel" + CurrentScreen.ToString(), True)
                CurrentPanel(0).Controls.Add(AnotherFrac)
                AnotherFrac.Location = New Point(1, 0)
                LSelect.FourFracLanguageSet(AnotherFrac)
                AnotherFrac.Show()

                sender.Visible = False

                Dim Title As Control() = Me.Controls.Find("Title" + CurrentScreen.ToString(), True)
                Title(0).Text = CurrentFracName
                CenterObject(Title(0))
            End If
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TrabajoActivo")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
        End If
    End Sub

    ''Sub para cerrar y desconectar un fracturador
    Private Sub CloseButton(sender As Object, e As EventArgs) Handles CloseButton1.Click, CloseButton2.Click, CloseButton3.Click, CloseButton4.Click
        If IFracSystem.ActiveOP = False Then
            Dim CurrentScreen As Integer = GetInteger(sender.Name)
            Dim CurrentPanel As Control() = Me.Controls.Find("Panel" + CurrentScreen.ToString(), True)
            If CurrentPanel(0).Controls.Find("FourFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                Dim CurrentFrac As Control() = CurrentPanel(0).Controls.Find("FourFrac_" + My.Settings.Resolucion, True)
                Dim C1 As Object = CurrentFrac(0)

                ISystem.Disconnect(C1.Frac)
                C1.StopTimers()

                Dim LockButton As Control() = Controls.Find("BtnLockFrac" + CurrentScreen.ToString, True)
                LockButton(0).Enabled = False

                Dim MaximizeButton As Control() = Me.Controls.Find("Maximize" + CurrentScreen.ToString(), True)
                MaximizeButton(0).Enabled = False
                sender.Enabled = False

                CurrentPanel(0).Controls.RemoveAt(1)


                Dim BtnAdd As Control() = Me.Controls.Find("BtnAdd" + CurrentScreen.ToString(), True)
                BtnAdd(0).Visible = True

                Dim Title As Control() = Me.Controls.Find("Title" + CurrentScreen.ToString(), True)
                Title(0).Text = LanguageSelect.LanguageRM.GetString("Container.Title")
                CenterObject(Title(0))

                If Not GetDuplicateFracs(C1.Frac) Then
                    MaxFracs -= 1
                    FracData.BoolVFrac("ActiveFrac_" + C1.Frac.ToString()) = False
                End If
                ConnectedFracsID.Remove(C1.Frac)
            End If
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TrabajoActivo")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
        End If
    End Sub

    ''FormClose sub, para no cerrar el programa con el boton de cerrar fracturador
    Private Sub Formclose(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
    End Sub

    ''Sub para ir a OneFracMaximize y ver mejor todo
    Private Sub Maximize(sender As Object, e As EventArgs) Handles Maximize1.Click, Maximize2.Click, Maximize3.Click, Maximize4.Click

        Dim CurrentOne As Integer = GetInteger(sender.Name)
        Dim CurrentPanel As Control() = Me.Controls.Find("Panel" + CurrentOne.ToString(), True)
        Dim FourFrac As Object = CurrentPanel(0).Controls(1)
        CurrentFrac = FourFrac.Frac
        CurrentFracName = ISystem.GetFracName(CurrentFrac)
        MaximizeForm.Show(Me)
    End Sub




#Region "Barra Inferior"

    ''Encender luces
    Private Sub BtnEncenderLuces_Click(sender As Object, e As EventArgs) Handles BtnEncenderLuces.Click
        sender.Checked = Not sender.Checked
        If sender.Checked = True Then
            For ThisFrac = 0 To LockedFracs.Count - 1
                ISystem.MoxaSend(FracMoxaSendStartAddress("Luces_StartAddress"), FracGeneralValues("LucesON"), LockedFracs.Item(ThisFrac))
            Next
            sender.Text = LanguageSelect.DynamicLanguageRM.GetString("Luces.Apagar")
        Else
            For ThisFrac = 0 To LockedFracs.Count - 1
                ISystem.MoxaSend(FracMoxaSendStartAddress("Luces_StartAddress"), FracGeneralValues("LucesOFF"), LockedFracs.Item(ThisFrac))
            Next
            sender.Text = LanguageSelect.DynamicLanguageRM.GetString("Luces.Encender")
        End If
    End Sub

    ''Abrimos ventana para mandar trip a todos los frac
    Private Sub BtnIdleALL_Click(sender As Object, e As EventArgs) Handles BtnTRIPAll.Click
        SetALLTRIP.Show()
    End Sub

    ''Sub para lockear fracturadores
    Private Sub BtnLockFrac8_Click(sender As Object, e As EventArgs) Handles BtnLockFrac4.Click, BtnLockFrac3.Click, BtnLockFrac2.Click, BtnLockFrac1.Click

        sender.Checked = Not sender.Checked
        Dim CurrentScreen As Integer = GetInteger(sender.Name)
        Dim CurrentPanel As Control() = Controls.Find("Panel" + CurrentScreen.ToString(), True)
        Dim CurrentFrac As Control() = CurrentPanel(0).Controls.Find("FourFrac_" + My.Settings.Resolucion, True)
        Dim C1 As Object = CurrentFrac(0)
        If sender.Checked Then
            If Not LockedFracs.Contains(C1.Frac) Then LockedFracs.Add(C1.Frac)
        Else
            If LockedFracs.Contains(C1.Frac) Then LockedFracs.Remove(C1.Frac)
        End If
    End Sub

    ''Click a empezar trabajo
    Private Sub BtnEmpezarTrabajo_Click(sender As Object, e As EventArgs) Handles BtnEmpezarTrabajo.Click
        If BtnEmpezarTrabajo.Checked = False Then
            OperationStartTime = DateTime.Now()                          ''Tiempo de inicio
            IFracSystem.ActiveOP = True
        Else
            IFracSystem.ActiveOP = False
        End If
    End Sub

    ''Handler de operation start
    Private Sub OperationStart()
        BtnEmpezarTrabajo.Checked = IFracSystem.ActiveOP
        If IFracSystem.ActiveOP Then
            BtnEmpezarTrabajo.Text = LanguageSelect.DynamicLanguageRM.GetString("FinalizarTrabajo")
        Else
            BtnEmpezarTrabajo.Text = LanguageSelect.DynamicLanguageRM.GetString("EmpezarTrabajo")
        End If
    End Sub

    ''Lockeamos todos los frac
    Private Sub BtnLockAllFracs_Click(sender As Object, e As EventArgs) Handles BtnLockAllFracs.Click
        BtnLockAllFracs.Checked = Not BtnLockAllFracs.Checked
        For ThisFrac = 1 To 4
            ''Checkeamos o no todos los botones disponibles
            Dim BtnLock As Guna.UI2.WinForms.Guna2Button = Controls.Find("BtnLockFrac" + ThisFrac.ToString(), True)(0)
            If BtnLock.Enabled Then
                BtnLock.Checked = BtnLockAllFracs.Checked
                ''Agregamos los fracs a la lista de lockedfracs
                Dim CurrentPanel As Control() = Controls.Find("Panel" + ThisFrac.ToString(), True)
                Dim CurrentFrac As Control() = CurrentPanel(0).Controls.Find("FourFrac_" + My.Settings.Resolucion, True)
                Dim C1 As Object = CurrentFrac(0)
                If BtnLockAllFracs.Checked Then
                    If Not LockedFracs.Contains(C1.Frac) Then LockedFracs.Add(C1.Frac)
                Else
                    If LockedFracs.Contains(C1.Frac) Then LockedFracs.Remove(C1.Frac)
                End If
            End If
        Next
    End Sub

#Region "Gear Control"
    Private Sub BtnPlusGear_Click(sender As Object, e As EventArgs) Handles BtnPlusGear.Click
        Dim GearPlusTh As New Task(AddressOf GearPlus)
        GearPlusTh.Start()
    End Sub

    Private Sub GearPlus()
        For ThisFrac = 0 To LockedFracs.Count - 1
            Dim marcha As Integer
            Select Case FracData.StrVFrac("MarchaActual_" + LockedFracs.Item(ThisFrac).ToString())
                Case "N"
                    marcha = FracGeneralValues("Marcha1")
                Case "P"
                    Exit Sub
                Case Else
                    Dim MarchaReal As Integer = GetInteger(FracData.StrVFrac("MarchaActual_" + LockedFracs.Item(ThisFrac).ToString()))
                    marcha = FracGeneralValues("Marcha" + Math.Min(MarchaReal + 1, FracData.SelectedTransmissions("SelectedTransmission_" + LockedFracs.Item(ThisFrac).ToString()).Item("Gears")).ToString())
            End Select
            ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, LockedFracs.Item(ThisFrac))
        Next
    End Sub

    Private Sub BtnMinusGear_Click(sender As Object, e As EventArgs) Handles BtnMinusGear.Click
        Dim GearMinusTh As New Task(AddressOf GearMinus)
        GearMinusTh.Start()
    End Sub

    Private Sub GearMinus()
        For ThisFrac = 0 To LockedFracs.Count - 1
            Dim marcha As Integer
            Select Case FracData.StrVFrac("MarchaActual_" + LockedFracs.Item(ThisFrac).ToString())
                Case "N"
                    marcha = FracGeneralValues("Marcha0")
                Case "P"
                    Exit Sub
                Case Else
                    Dim MarchaReal As Integer = GetInteger(FracData.StrVFrac("MarchaActual_" + LockedFracs.Item(ThisFrac).ToString()))
                    marcha = FracGeneralValues("Marcha" + (MarchaReal - 1).ToString())
            End Select
            ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, LockedFracs.Item(ThisFrac))
        Next
    End Sub
#End Region

#Region "RPM Control"
    Private Sub RpmPlus(sender As Object, e As EventArgs) Handles BtnRpmPlus50.Click
        Dim RpmPlusTh As New Task(AddressOf RpmPlusDo)
        RpmPlusTh.Start()
    End Sub

    Private Sub RpmPlusDo()
        For ThisFrac = 0 To LockedFracs.Count - 1
            Dim Frac As Integer = LockedFracs.Item(ThisFrac)
            Dim MyEngine = FracData.SelectedEngines("SelectedEngine_" + Frac.ToString())

            Dim RpmPlus As Integer = 50
            Dim RPM As Integer
            If MyEngine("LimitRPM") = True Then
                RPM = Clamp(MyEngine("RPM_LMin"), MyEngine("RPM_LMax"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) + RpmPlus)
            Else
                RPM = Clamp(MyEngine("MinRPM"), MyEngine("MaxRPM"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) + RpmPlus)
            End If
            ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), RPM, Frac)

        Next
    End Sub

    Private Sub RpmMinus(sender As Object, e As EventArgs) Handles BtnRpmMinus50.Click
        Dim RpmMinusTh As New Task(AddressOf RpmMinusDo)
        RpmMinusTh.Start()
    End Sub

    Private Sub RpmMinusDo()
        For ThisFrac = 0 To LockedFracs.Count - 1
            Dim Frac As Integer = LockedFracs.Item(ThisFrac)
            Dim MyEngine = FracData.SelectedEngines("SelectedEngine_" + Frac.ToString())

            Dim RpmMinus As Integer = 50
            Dim RPM
            If MyEngine("LimitRPM") = True Then
                RPM = Clamp(MyEngine("RPM_LMin"), MyEngine("RPM_LMax"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) - RpmMinus)
            Else
                RPM = Clamp(MyEngine("MinRPM"), MyEngine("MaxRPM"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) - RpmMinus)
            End If
            ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), RPM, Frac)
        Next
    End Sub


#End Region


#End Region

End Class