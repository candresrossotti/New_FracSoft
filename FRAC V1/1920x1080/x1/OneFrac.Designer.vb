﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class OneFrac_1920x1080
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OneFrac_1920x1080))
        Me.LblTrans3 = New System.Windows.Forms.Label()
        Me.LblMarchaActual = New System.Windows.Forms.Label()
        Me.LblMarchaDeseada = New System.Windows.Forms.Label()
        Me.LblMarcha3 = New System.Windows.Forms.Label()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMarcha1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMarcha = New System.Windows.Forms.Label()
        Me.LblRPMActual = New System.Windows.Forms.Label()
        Me.LblRPMDeseada = New System.Windows.Forms.Label()
        Me.TmrErrorHandle = New System.Windows.Forms.Timer(Me.components)
        Me.BtnIdle = New Guna.UI2.WinForms.Guna2Button()
        Me.LblMotor = New System.Windows.Forms.Label()
        Me.TmrCheck = New System.Windows.Forms.Timer(Me.components)
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.LblRPM3 = New System.Windows.Forms.Label()
        Me.LblRPM1 = New System.Windows.Forms.Label()
        Me.RgRPM = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2Panel13 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblCombustible1 = New System.Windows.Forms.Label()
        Me.LblMotorCombustibleP100 = New System.Windows.Forms.Label()
        Me.RgCombP100 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel14 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMotorCombustible = New System.Windows.Forms.Label()
        Me.RgCombustible = New Guna.UI2.WinForms.Guna2CircleProgressBar()
        Me.LblCombustible = New System.Windows.Forms.Label()
        Me.Guna2Panel15 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblVoltaje = New System.Windows.Forms.Label()
        Me.LblMotorVoltaje = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.RgVoltaje = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel9 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblPresion = New System.Windows.Forms.Label()
        Me.LblMotorPresion = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.RgPresionMotor = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel8 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblTempAceite = New System.Windows.Forms.Label()
        Me.LblMotorTempAceite = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.RgTempAceite = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel6 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblTempAgua = New System.Windows.Forms.Label()
        Me.LblMotorTempAgua = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.RgTempAgua = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTransmision = New System.Windows.Forms.Label()
        Me.LblTrans1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel5 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.TmrReconnect = New System.Windows.Forms.Timer(Me.components)
        Me.LblTransTemp = New System.Windows.Forms.Label()
        Me.TmrRead = New System.Windows.Forms.Timer(Me.components)
        Me.TmrOp = New System.Windows.Forms.Timer(Me.components)
        Me.BtnResetRPM = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnRpmMinus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmMinus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblPCorte1 = New System.Windows.Forms.Label()
        Me.BtnTrip = New Guna.UI2.WinForms.Guna2Button()
        Me.LblPCorteDeseada = New System.Windows.Forms.Label()
        Me.LblPCorte3 = New System.Windows.Forms.Label()
        Me.LblPCorteActual = New System.Windows.Forms.Label()
        Me.RgPresion = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel3 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BtnTripZero = New Guna.UI2.WinForms.Guna2Button()
        Me.LblTransPresion = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.LblBombaHHP = New System.Windows.Forms.Label()
        Me.LblHHP = New System.Windows.Forms.Label()
        Me.LblBombaCargaMotor = New System.Windows.Forms.Label()
        Me.LblBombaPresSuccion = New System.Windows.Forms.Label()
        Me.LblCargaMotor = New System.Windows.Forms.Label()
        Me.LblPresSuc = New System.Windows.Forms.Label()
        Me.Guna2Panel7 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2Panel19 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2RadialGauge10 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel20 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblBombaHorasMotor = New System.Windows.Forms.Label()
        Me.RgHorasMotor = New Guna.UI2.WinForms.Guna2CircleProgressBar()
        Me.LblHM = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Guna2Panel21 = New Guna.UI2.WinForms.Guna2Panel()
        Me.RgCargaMotor = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel22 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblPresLub = New System.Windows.Forms.Label()
        Me.LblBombaPresLUB = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.RgPresLub = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel23 = New Guna.UI2.WinForms.Guna2Panel()
        Me.RgPresSucc = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel24 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblTempLub = New System.Windows.Forms.Label()
        Me.LblBombaTempLUB = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.RgTempLub = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2CustomGradientPanel7 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblBomba = New System.Windows.Forms.Label()
        Me.LblCaudal = New System.Windows.Forms.Label()
        Me.Guna2Panel10 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.LblCaudalActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel10 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Guna2CustomGradientPanel11 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblOpciones = New System.Windows.Forms.Label()
        Me.BtnEmpezarTrabajo = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnSemiAuto = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnEncenderLuces = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel11 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnFC = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnDiagnostico = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnErrores = New Guna.UI2.WinForms.Guna2Button()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.LblTransRpmOut = New System.Windows.Forms.Label()
        Me.LblTrans5 = New System.Windows.Forms.Label()
        Me.Guna2Panel5 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2Panel18 = New Guna.UI2.WinForms.Guna2Panel()
        Me.RgRPMTransmision = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel17 = New Guna.UI2.WinForms.Guna2Panel()
        Me.RgPresionTransmision = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel16 = New Guna.UI2.WinForms.Guna2Panel()
        Me.RgTempTransmision = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.LblErrorAdmisible = New System.Windows.Forms.Label()
        Me.LblErrorGrave = New System.Windows.Forms.Label()
        Me.TmrBackUp = New System.Windows.Forms.Timer(Me.components)
        Me.BtnEKill = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnMinusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPlusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.PbAutomatic = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.BtnAjustes = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnStartEngine = New Guna.UI2.WinForms.Guna2Button()
        Me.PbAlarms = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.PbLockUp = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.PbMoxaStatus = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.BtnParking = New Guna.UI2.WinForms.Guna2Button()
        Me.TmrConnectionCheck = New System.Windows.Forms.Timer(Me.components)
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.Guna2Panel4.SuspendLayout()
        Me.Guna2Panel13.SuspendLayout()
        Me.Guna2Panel14.SuspendLayout()
        Me.RgCombustible.SuspendLayout()
        Me.Guna2Panel15.SuspendLayout()
        Me.Guna2Panel9.SuspendLayout()
        Me.Guna2Panel8.SuspendLayout()
        Me.Guna2Panel6.SuspendLayout()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.Guna2CustomGradientPanel5.SuspendLayout()
        Me.Guna2Panel2.SuspendLayout()
        Me.Guna2Panel3.SuspendLayout()
        Me.Guna2Panel7.SuspendLayout()
        Me.Guna2Panel19.SuspendLayout()
        Me.Guna2Panel20.SuspendLayout()
        Me.RgHorasMotor.SuspendLayout()
        Me.Guna2Panel21.SuspendLayout()
        Me.Guna2Panel22.SuspendLayout()
        Me.Guna2Panel23.SuspendLayout()
        Me.Guna2Panel24.SuspendLayout()
        Me.Guna2CustomGradientPanel7.SuspendLayout()
        Me.Guna2Panel10.SuspendLayout()
        Me.Guna2CustomGradientPanel10.SuspendLayout()
        Me.Guna2CustomGradientPanel11.SuspendLayout()
        Me.Guna2Panel11.SuspendLayout()
        Me.Guna2Panel5.SuspendLayout()
        Me.Guna2Panel18.SuspendLayout()
        Me.Guna2Panel17.SuspendLayout()
        Me.Guna2Panel16.SuspendLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblTrans3
        '
        Me.LblTrans3.AutoSize = True
        Me.LblTrans3.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTrans3.ForeColor = System.Drawing.Color.White
        Me.LblTrans3.Location = New System.Drawing.Point(62, 45)
        Me.LblTrans3.Name = "LblTrans3"
        Me.LblTrans3.Size = New System.Drawing.Size(70, 22)
        Me.LblTrans3.TabIndex = 21
        Me.LblTrans3.Text = "Presión"
        '
        'LblMarchaActual
        '
        Me.LblMarchaActual.AutoSize = True
        Me.LblMarchaActual.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblMarchaActual.ForeColor = System.Drawing.Color.White
        Me.LblMarchaActual.Location = New System.Drawing.Point(151, 163)
        Me.LblMarchaActual.Name = "LblMarchaActual"
        Me.LblMarchaActual.Size = New System.Drawing.Size(39, 37)
        Me.LblMarchaActual.TabIndex = 23
        Me.LblMarchaActual.Text = "N"
        '
        'LblMarchaDeseada
        '
        Me.LblMarchaDeseada.AutoSize = True
        Me.LblMarchaDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblMarchaDeseada.Font = New System.Drawing.Font("Montserrat", 18.0!)
        Me.LblMarchaDeseada.ForeColor = System.Drawing.Color.White
        Me.LblMarchaDeseada.Location = New System.Drawing.Point(155, 83)
        Me.LblMarchaDeseada.Name = "LblMarchaDeseada"
        Me.LblMarchaDeseada.Size = New System.Drawing.Size(35, 33)
        Me.LblMarchaDeseada.TabIndex = 22
        Me.LblMarchaDeseada.Text = "N"
        '
        'LblMarcha3
        '
        Me.LblMarcha3.AutoSize = True
        Me.LblMarcha3.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblMarcha3.ForeColor = System.Drawing.Color.White
        Me.LblMarcha3.Location = New System.Drawing.Point(96, 130)
        Me.LblMarcha3.Name = "LblMarcha3"
        Me.LblMarcha3.Size = New System.Drawing.Size(156, 27)
        Me.LblMarcha3.TabIndex = 21
        Me.LblMarcha3.Text = "Marcha Actual"
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaActual)
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaDeseada)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha3)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(765, 244)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(324, 210)
        Me.Guna2Panel1.TabIndex = 178
        '
        'LblMarcha1
        '
        Me.LblMarcha1.AutoSize = True
        Me.LblMarcha1.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblMarcha1.ForeColor = System.Drawing.Color.White
        Me.LblMarcha1.Location = New System.Drawing.Point(87, 47)
        Me.LblMarcha1.Name = "LblMarcha1"
        Me.LblMarcha1.Size = New System.Drawing.Size(182, 27)
        Me.LblMarcha1.TabIndex = 20
        Me.LblMarcha1.Text = "Marcha Deseada"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblMarcha)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(325, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblMarcha
        '
        Me.LblMarcha.AutoSize = True
        Me.LblMarcha.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMarcha.Location = New System.Drawing.Point(132, 4)
        Me.LblMarcha.Name = "LblMarcha"
        Me.LblMarcha.Size = New System.Drawing.Size(61, 20)
        Me.LblMarcha.TabIndex = 19
        Me.LblMarcha.Text = "Marcha"
        '
        'LblRPMActual
        '
        Me.LblRPMActual.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPMActual.AutoSize = True
        Me.LblRPMActual.Font = New System.Drawing.Font("Montserrat", 30.0!)
        Me.LblRPMActual.ForeColor = System.Drawing.Color.White
        Me.LblRPMActual.Location = New System.Drawing.Point(204, 142)
        Me.LblRPMActual.Name = "LblRPMActual"
        Me.LblRPMActual.Size = New System.Drawing.Size(39, 55)
        Me.LblRPMActual.TabIndex = 145
        Me.LblRPMActual.Text = "-"
        '
        'LblRPMDeseada
        '
        Me.LblRPMDeseada.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPMDeseada.AutoSize = True
        Me.LblRPMDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblRPMDeseada.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblRPMDeseada.ForeColor = System.Drawing.Color.White
        Me.LblRPMDeseada.Location = New System.Drawing.Point(126, 245)
        Me.LblRPMDeseada.Name = "LblRPMDeseada"
        Me.LblRPMDeseada.Size = New System.Drawing.Size(27, 37)
        Me.LblRPMDeseada.TabIndex = 144
        Me.LblRPMDeseada.Text = "-"
        '
        'TmrErrorHandle
        '
        Me.TmrErrorHandle.Interval = 600
        '
        'BtnIdle
        '
        Me.BtnIdle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnIdle.Animated = True
        Me.BtnIdle.BorderColor = System.Drawing.Color.White
        Me.BtnIdle.BorderRadius = 5
        Me.BtnIdle.BorderThickness = 1
        Me.BtnIdle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdle.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdle.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnIdle.ForeColor = System.Drawing.Color.White
        Me.BtnIdle.Location = New System.Drawing.Point(156, 312)
        Me.BtnIdle.Name = "BtnIdle"
        Me.BtnIdle.Size = New System.Drawing.Size(138, 63)
        Me.BtnIdle.TabIndex = 146
        Me.BtnIdle.Text = "INSTANT NEUTRAL"
        '
        'LblMotor
        '
        Me.LblMotor.AutoSize = True
        Me.LblMotor.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMotor.Location = New System.Drawing.Point(275, 3)
        Me.LblMotor.Name = "LblMotor"
        Me.LblMotor.Size = New System.Drawing.Size(54, 21)
        Me.LblMotor.TabIndex = 19
        Me.LblMotor.Text = "Motor"
        '
        'TmrCheck
        '
        Me.TmrCheck.Interval = 1500
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(64, 108)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(21, 20)
        Me.Label23.TabIndex = 37
        Me.Label23.Text = "%"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(45, 98)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(39, 20)
        Me.Label24.TabIndex = 36
        Me.Label24.Text = "L/Hr"
        '
        'LblRPM3
        '
        Me.LblRPM3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPM3.AutoSize = True
        Me.LblRPM3.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblRPM3.ForeColor = System.Drawing.Color.White
        Me.LblRPM3.Location = New System.Drawing.Point(156, 112)
        Me.LblRPM3.Name = "LblRPM3"
        Me.LblRPM3.Size = New System.Drawing.Size(138, 25)
        Me.LblRPM3.TabIndex = 143
        Me.LblRPM3.Text = "RPM Actuales"
        '
        'LblRPM1
        '
        Me.LblRPM1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPM1.AutoSize = True
        Me.LblRPM1.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblRPM1.ForeColor = System.Drawing.Color.White
        Me.LblRPM1.Location = New System.Drawing.Point(65, 218)
        Me.LblRPM1.Name = "LblRPM1"
        Me.LblRPM1.Size = New System.Drawing.Size(149, 25)
        Me.LblRPM1.TabIndex = 142
        Me.LblRPM1.Text = "RPM Deseadas"
        '
        'RgRPM
        '
        Me.RgRPM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgRPM.ArrowColor = System.Drawing.Color.Silver
        Me.RgRPM.ArrowThickness = 0
        Me.RgRPM.ArrowVisible = False
        Me.RgRPM.BackColor = System.Drawing.Color.Transparent
        Me.RgRPM.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgRPM.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgRPM.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgRPM.Location = New System.Drawing.Point(0, 0)
        Me.RgRPM.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgRPM.Name = "RgRPM"
        Me.RgRPM.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgRPM.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgRPM.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPM.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPM.ProgressThickness = 12
        Me.RgRPM.ShowPercentage = False
        Me.RgRPM.Size = New System.Drawing.Size(451, 451)
        Me.RgRPM.TabIndex = 151
        Me.RgRPM.UseTransparentBackground = True
        Me.RgRPM.Value = 100
        '
        'Guna2Panel4
        '
        Me.Guna2Panel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.BorderRadius = 6
        Me.Guna2Panel4.BorderThickness = 1
        Me.Guna2Panel4.Controls.Add(Me.Guna2Panel13)
        Me.Guna2Panel4.Controls.Add(Me.Guna2Panel14)
        Me.Guna2Panel4.Controls.Add(Me.Guna2Panel15)
        Me.Guna2Panel4.Controls.Add(Me.Guna2Panel9)
        Me.Guna2Panel4.Controls.Add(Me.Guna2Panel8)
        Me.Guna2Panel4.Controls.Add(Me.Guna2Panel6)
        Me.Guna2Panel4.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.Guna2Panel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.Location = New System.Drawing.Point(15, 535)
        Me.Guna2Panel4.Name = "Guna2Panel4"
        Me.Guna2Panel4.Size = New System.Drawing.Size(604, 340)
        Me.Guna2Panel4.TabIndex = 166
        '
        'Guna2Panel13
        '
        Me.Guna2Panel13.Controls.Add(Me.LblCombustible1)
        Me.Guna2Panel13.Controls.Add(Me.Label23)
        Me.Guna2Panel13.Controls.Add(Me.LblMotorCombustibleP100)
        Me.Guna2Panel13.Controls.Add(Me.RgCombP100)
        Me.Guna2Panel13.Location = New System.Drawing.Point(453, 186)
        Me.Guna2Panel13.Name = "Guna2Panel13"
        Me.Guna2Panel13.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel13.TabIndex = 191
        '
        'LblCombustible1
        '
        Me.LblCombustible1.AutoSize = True
        Me.LblCombustible1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCombustible1.ForeColor = System.Drawing.Color.White
        Me.LblCombustible1.Location = New System.Drawing.Point(25, 35)
        Me.LblCombustible1.Name = "LblCombustible1"
        Me.LblCombustible1.Size = New System.Drawing.Size(99, 20)
        Me.LblCombustible1.TabIndex = 33
        Me.LblCombustible1.Text = "Combustible"
        '
        'LblMotorCombustibleP100
        '
        Me.LblMotorCombustibleP100.AutoSize = True
        Me.LblMotorCombustibleP100.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCombustibleP100.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustibleP100.Location = New System.Drawing.Point(67, 64)
        Me.LblMotorCombustibleP100.Name = "LblMotorCombustibleP100"
        Me.LblMotorCombustibleP100.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorCombustibleP100.TabIndex = 34
        Me.LblMotorCombustibleP100.Text = "-"
        '
        'RgCombP100
        '
        Me.RgCombP100.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgCombP100.ArrowColor = System.Drawing.Color.Silver
        Me.RgCombP100.ArrowThickness = 0
        Me.RgCombP100.ArrowVisible = False
        Me.RgCombP100.BackColor = System.Drawing.Color.Transparent
        Me.RgCombP100.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgCombP100.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgCombP100.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgCombP100.Location = New System.Drawing.Point(0, 0)
        Me.RgCombP100.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgCombP100.Name = "RgCombP100"
        Me.RgCombP100.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgCombP100.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgCombP100.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCombP100.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCombP100.ProgressThickness = 4
        Me.RgCombP100.ShowPercentage = False
        Me.RgCombP100.Size = New System.Drawing.Size(148, 148)
        Me.RgCombP100.TabIndex = 185
        Me.RgCombP100.UseTransparentBackground = True
        Me.RgCombP100.Value = 100
        '
        'Guna2Panel14
        '
        Me.Guna2Panel14.Controls.Add(Me.LblMotorCombustible)
        Me.Guna2Panel14.Controls.Add(Me.RgCombustible)
        Me.Guna2Panel14.Location = New System.Drawing.Point(227, 186)
        Me.Guna2Panel14.Name = "Guna2Panel14"
        Me.Guna2Panel14.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel14.TabIndex = 190
        '
        'LblMotorCombustible
        '
        Me.LblMotorCombustible.AutoSize = True
        Me.LblMotorCombustible.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCombustible.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustible.Location = New System.Drawing.Point(66, 64)
        Me.LblMotorCombustible.Name = "LblMotorCombustible"
        Me.LblMotorCombustible.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorCombustible.TabIndex = 32
        Me.LblMotorCombustible.Text = "-"
        '
        'RgCombustible
        '
        Me.RgCombustible.Animated = True
        Me.RgCombustible.AnimationSpeed = 0.1!
        Me.RgCombustible.Controls.Add(Me.LblCombustible)
        Me.RgCombustible.Controls.Add(Me.Label24)
        Me.RgCombustible.FillColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.RgCombustible.FillThickness = 0
        Me.RgCombustible.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.RgCombustible.ForeColor = System.Drawing.Color.White
        Me.RgCombustible.InnerColor = System.Drawing.Color.Transparent
        Me.RgCombustible.Location = New System.Drawing.Point(9, 10)
        Me.RgCombustible.Maximum = 1000
        Me.RgCombustible.Minimum = 0
        Me.RgCombustible.Name = "RgCombustible"
        Me.RgCombustible.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgCombustible.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgCombustible.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCombustible.ProgressThickness = 4
        Me.RgCombustible.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle
        Me.RgCombustible.Size = New System.Drawing.Size(129, 129)
        Me.RgCombustible.TabIndex = 188
        Me.RgCombustible.Text = "Guna2CircleProgressBar2"
        Me.RgCombustible.Value = 650
        '
        'LblCombustible
        '
        Me.LblCombustible.AutoSize = True
        Me.LblCombustible.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCombustible.ForeColor = System.Drawing.Color.White
        Me.LblCombustible.Location = New System.Drawing.Point(13, 25)
        Me.LblCombustible.Name = "LblCombustible"
        Me.LblCombustible.Size = New System.Drawing.Size(99, 20)
        Me.LblCombustible.TabIndex = 30
        Me.LblCombustible.Text = "Combustible"
        '
        'Guna2Panel15
        '
        Me.Guna2Panel15.Controls.Add(Me.LblVoltaje)
        Me.Guna2Panel15.Controls.Add(Me.LblMotorVoltaje)
        Me.Guna2Panel15.Controls.Add(Me.Label22)
        Me.Guna2Panel15.Controls.Add(Me.RgVoltaje)
        Me.Guna2Panel15.Location = New System.Drawing.Point(3, 186)
        Me.Guna2Panel15.Name = "Guna2Panel15"
        Me.Guna2Panel15.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel15.TabIndex = 189
        '
        'LblVoltaje
        '
        Me.LblVoltaje.AutoSize = True
        Me.LblVoltaje.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblVoltaje.ForeColor = System.Drawing.Color.White
        Me.LblVoltaje.Location = New System.Drawing.Point(46, 35)
        Me.LblVoltaje.Name = "LblVoltaje"
        Me.LblVoltaje.Size = New System.Drawing.Size(57, 20)
        Me.LblVoltaje.TabIndex = 24
        Me.LblVoltaje.Text = "Voltaje"
        '
        'LblMotorVoltaje
        '
        Me.LblMotorVoltaje.AutoSize = True
        Me.LblMotorVoltaje.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorVoltaje.ForeColor = System.Drawing.Color.White
        Me.LblMotorVoltaje.Location = New System.Drawing.Point(67, 64)
        Me.LblMotorVoltaje.Name = "LblMotorVoltaje"
        Me.LblMotorVoltaje.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorVoltaje.TabIndex = 25
        Me.LblMotorVoltaje.Text = "-"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(65, 108)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(19, 20)
        Me.Label22.TabIndex = 28
        Me.Label22.Text = "V"
        '
        'RgVoltaje
        '
        Me.RgVoltaje.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgVoltaje.ArrowColor = System.Drawing.Color.Silver
        Me.RgVoltaje.ArrowThickness = 0
        Me.RgVoltaje.ArrowVisible = False
        Me.RgVoltaje.BackColor = System.Drawing.Color.Transparent
        Me.RgVoltaje.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgVoltaje.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgVoltaje.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgVoltaje.Location = New System.Drawing.Point(0, 0)
        Me.RgVoltaje.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgVoltaje.Name = "RgVoltaje"
        Me.RgVoltaje.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgVoltaje.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgVoltaje.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgVoltaje.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgVoltaje.ProgressThickness = 4
        Me.RgVoltaje.ShowPercentage = False
        Me.RgVoltaje.Size = New System.Drawing.Size(148, 148)
        Me.RgVoltaje.TabIndex = 185
        Me.RgVoltaje.UseTransparentBackground = True
        Me.RgVoltaje.Value = 100
        '
        'Guna2Panel9
        '
        Me.Guna2Panel9.Controls.Add(Me.LblPresion)
        Me.Guna2Panel9.Controls.Add(Me.LblMotorPresion)
        Me.Guna2Panel9.Controls.Add(Me.Label21)
        Me.Guna2Panel9.Controls.Add(Me.RgPresionMotor)
        Me.Guna2Panel9.Location = New System.Drawing.Point(453, 32)
        Me.Guna2Panel9.Name = "Guna2Panel9"
        Me.Guna2Panel9.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel9.TabIndex = 188
        '
        'LblPresion
        '
        Me.LblPresion.AutoSize = True
        Me.LblPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPresion.ForeColor = System.Drawing.Color.White
        Me.LblPresion.Location = New System.Drawing.Point(43, 35)
        Me.LblPresion.Name = "LblPresion"
        Me.LblPresion.Size = New System.Drawing.Size(62, 20)
        Me.LblPresion.TabIndex = 21
        Me.LblPresion.Text = "Presión"
        '
        'LblMotorPresion
        '
        Me.LblMotorPresion.AutoSize = True
        Me.LblMotorPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorPresion.ForeColor = System.Drawing.Color.White
        Me.LblMotorPresion.Location = New System.Drawing.Point(67, 64)
        Me.LblMotorPresion.Name = "LblMotorPresion"
        Me.LblMotorPresion.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorPresion.TabIndex = 23
        Me.LblMotorPresion.Text = "-"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(58, 108)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(32, 20)
        Me.Label21.TabIndex = 27
        Me.Label21.Text = "PSI"
        '
        'RgPresionMotor
        '
        Me.RgPresionMotor.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgPresionMotor.ArrowColor = System.Drawing.Color.Silver
        Me.RgPresionMotor.ArrowThickness = 0
        Me.RgPresionMotor.ArrowVisible = False
        Me.RgPresionMotor.BackColor = System.Drawing.Color.Transparent
        Me.RgPresionMotor.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgPresionMotor.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgPresionMotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgPresionMotor.Location = New System.Drawing.Point(0, 0)
        Me.RgPresionMotor.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgPresionMotor.Name = "RgPresionMotor"
        Me.RgPresionMotor.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgPresionMotor.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgPresionMotor.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresionMotor.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresionMotor.ProgressThickness = 4
        Me.RgPresionMotor.ShowPercentage = False
        Me.RgPresionMotor.Size = New System.Drawing.Size(148, 148)
        Me.RgPresionMotor.TabIndex = 185
        Me.RgPresionMotor.UseTransparentBackground = True
        Me.RgPresionMotor.Value = 100
        '
        'Guna2Panel8
        '
        Me.Guna2Panel8.Controls.Add(Me.LblTempAceite)
        Me.Guna2Panel8.Controls.Add(Me.LblMotorTempAceite)
        Me.Guna2Panel8.Controls.Add(Me.Label25)
        Me.Guna2Panel8.Controls.Add(Me.RgTempAceite)
        Me.Guna2Panel8.Location = New System.Drawing.Point(227, 32)
        Me.Guna2Panel8.Name = "Guna2Panel8"
        Me.Guna2Panel8.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel8.TabIndex = 187
        '
        'LblTempAceite
        '
        Me.LblTempAceite.AutoSize = True
        Me.LblTempAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblTempAceite.Location = New System.Drawing.Point(26, 35)
        Me.LblTempAceite.Name = "LblTempAceite"
        Me.LblTempAceite.Size = New System.Drawing.Size(96, 20)
        Me.LblTempAceite.TabIndex = 29
        Me.LblTempAceite.Text = "Temp Aceite"
        '
        'LblMotorTempAceite
        '
        Me.LblMotorTempAceite.AutoSize = True
        Me.LblMotorTempAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAceite.Location = New System.Drawing.Point(67, 64)
        Me.LblMotorTempAceite.Name = "LblMotorTempAceite"
        Me.LblMotorTempAceite.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorTempAceite.TabIndex = 31
        Me.LblMotorTempAceite.Text = "-"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(62, 108)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(25, 20)
        Me.Label25.TabIndex = 35
        Me.Label25.Text = "ºC"
        '
        'RgTempAceite
        '
        Me.RgTempAceite.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgTempAceite.ArrowColor = System.Drawing.Color.Silver
        Me.RgTempAceite.ArrowThickness = 0
        Me.RgTempAceite.ArrowVisible = False
        Me.RgTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.RgTempAceite.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgTempAceite.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgTempAceite.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgTempAceite.Location = New System.Drawing.Point(0, 0)
        Me.RgTempAceite.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgTempAceite.Name = "RgTempAceite"
        Me.RgTempAceite.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgTempAceite.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgTempAceite.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempAceite.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempAceite.ProgressThickness = 4
        Me.RgTempAceite.ShowPercentage = False
        Me.RgTempAceite.Size = New System.Drawing.Size(148, 148)
        Me.RgTempAceite.TabIndex = 185
        Me.RgTempAceite.UseTransparentBackground = True
        Me.RgTempAceite.Value = 100
        '
        'Guna2Panel6
        '
        Me.Guna2Panel6.Controls.Add(Me.LblTempAgua)
        Me.Guna2Panel6.Controls.Add(Me.LblMotorTempAgua)
        Me.Guna2Panel6.Controls.Add(Me.Label20)
        Me.Guna2Panel6.Controls.Add(Me.RgTempAgua)
        Me.Guna2Panel6.Location = New System.Drawing.Point(3, 32)
        Me.Guna2Panel6.Name = "Guna2Panel6"
        Me.Guna2Panel6.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel6.TabIndex = 186
        '
        'LblTempAgua
        '
        Me.LblTempAgua.AutoSize = True
        Me.LblTempAgua.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblTempAgua.Location = New System.Drawing.Point(30, 35)
        Me.LblTempAgua.Name = "LblTempAgua"
        Me.LblTempAgua.Size = New System.Drawing.Size(89, 20)
        Me.LblTempAgua.TabIndex = 20
        Me.LblTempAgua.Text = "Temp Agua"
        '
        'LblMotorTempAgua
        '
        Me.LblMotorTempAgua.AutoSize = True
        Me.LblMotorTempAgua.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAgua.Location = New System.Drawing.Point(67, 64)
        Me.LblMotorTempAgua.Name = "LblMotorTempAgua"
        Me.LblMotorTempAgua.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorTempAgua.TabIndex = 22
        Me.LblMotorTempAgua.Text = "-"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(62, 108)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(25, 20)
        Me.Label20.TabIndex = 26
        Me.Label20.Text = "ºC"
        '
        'RgTempAgua
        '
        Me.RgTempAgua.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgTempAgua.ArrowColor = System.Drawing.Color.Silver
        Me.RgTempAgua.ArrowThickness = 0
        Me.RgTempAgua.ArrowVisible = False
        Me.RgTempAgua.BackColor = System.Drawing.Color.Transparent
        Me.RgTempAgua.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgTempAgua.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgTempAgua.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgTempAgua.Location = New System.Drawing.Point(0, 0)
        Me.RgTempAgua.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgTempAgua.Name = "RgTempAgua"
        Me.RgTempAgua.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgTempAgua.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgTempAgua.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempAgua.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempAgua.ProgressThickness = 4
        Me.RgTempAgua.ShowPercentage = False
        Me.RgTempAgua.Size = New System.Drawing.Size(148, 148)
        Me.RgTempAgua.TabIndex = 185
        Me.RgTempAgua.UseTransparentBackground = True
        Me.RgTempAgua.Value = 100
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.LblMotor)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(604, 26)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'LblTransmision
        '
        Me.LblTransmision.AutoSize = True
        Me.LblTransmision.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTransmision.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTransmision.Location = New System.Drawing.Point(252, 3)
        Me.LblTransmision.Name = "LblTransmision"
        Me.LblTransmision.Size = New System.Drawing.Size(100, 21)
        Me.LblTransmision.TabIndex = 19
        Me.LblTransmision.Text = "Transmisión"
        '
        'LblTrans1
        '
        Me.LblTrans1.AutoSize = True
        Me.LblTrans1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTrans1.ForeColor = System.Drawing.Color.White
        Me.LblTrans1.Location = New System.Drawing.Point(42, 45)
        Me.LblTrans1.Name = "LblTrans1"
        Me.LblTrans1.Size = New System.Drawing.Size(111, 22)
        Me.LblTrans1.TabIndex = 20
        Me.LblTrans1.Text = "Temperatura"
        '
        'Guna2CustomGradientPanel5
        '
        Me.Guna2CustomGradientPanel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.BorderRadius = 6
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.LblTransmision)
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel5.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel5.Name = "Guna2CustomGradientPanel5"
        Me.Guna2CustomGradientPanel5.Size = New System.Drawing.Size(604, 26)
        Me.Guna2CustomGradientPanel5.TabIndex = 19
        '
        'TmrReconnect
        '
        Me.TmrReconnect.Interval = 1000
        '
        'LblTransTemp
        '
        Me.LblTransTemp.AutoSize = True
        Me.LblTransTemp.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransTemp.ForeColor = System.Drawing.Color.White
        Me.LblTransTemp.Location = New System.Drawing.Point(90, 87)
        Me.LblTransTemp.Name = "LblTransTemp"
        Me.LblTransTemp.Size = New System.Drawing.Size(14, 20)
        Me.LblTransTemp.TabIndex = 22
        Me.LblTransTemp.Text = "-"
        '
        'TmrRead
        '
        Me.TmrRead.Interval = 1000
        '
        'TmrOp
        '
        Me.TmrOp.Interval = 1000
        '
        'BtnResetRPM
        '
        Me.BtnResetRPM.Animated = True
        Me.BtnResetRPM.BorderColor = System.Drawing.Color.White
        Me.BtnResetRPM.BorderRadius = 5
        Me.BtnResetRPM.BorderThickness = 1
        Me.BtnResetRPM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnResetRPM.FillColor = System.Drawing.Color.Transparent
        Me.BtnResetRPM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnResetRPM.ForeColor = System.Drawing.Color.White
        Me.BtnResetRPM.Location = New System.Drawing.Point(735, 42)
        Me.BtnResetRPM.Name = "BtnResetRPM"
        Me.BtnResetRPM.Size = New System.Drawing.Size(87, 65)
        Me.BtnResetRPM.TabIndex = 184
        Me.BtnResetRPM.Text = "Reset RPM"
        '
        'Guna2Panel2
        '
        Me.Guna2Panel2.Controls.Add(Me.LblRPMActual)
        Me.Guna2Panel2.Controls.Add(Me.BtnRpmMinus50)
        Me.Guna2Panel2.Controls.Add(Me.LblRPMDeseada)
        Me.Guna2Panel2.Controls.Add(Me.BtnRpmPlus25)
        Me.Guna2Panel2.Controls.Add(Me.BtnIdle)
        Me.Guna2Panel2.Controls.Add(Me.LblRPM3)
        Me.Guna2Panel2.Controls.Add(Me.LblRPM1)
        Me.Guna2Panel2.Controls.Add(Me.BtnRpmPlus50)
        Me.Guna2Panel2.Controls.Add(Me.BtnRpmMinus25)
        Me.Guna2Panel2.Controls.Add(Me.RgRPM)
        Me.Guna2Panel2.Location = New System.Drawing.Point(92, 8)
        Me.Guna2Panel2.Name = "Guna2Panel2"
        Me.Guna2Panel2.Size = New System.Drawing.Size(451, 451)
        Me.Guna2Panel2.TabIndex = 182
        '
        'BtnRpmMinus50
        '
        Me.BtnRpmMinus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmMinus50.Animated = True
        Me.BtnRpmMinus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.BorderRadius = 5
        Me.BtnRpmMinus50.BorderThickness = 1
        Me.BtnRpmMinus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.Image = CType(resources.GetObject("BtnRpmMinus50.Image"), System.Drawing.Image)
        Me.BtnRpmMinus50.Location = New System.Drawing.Point(266, 254)
        Me.BtnRpmMinus50.Name = "BtnRpmMinus50"
        Me.BtnRpmMinus50.Size = New System.Drawing.Size(40, 40)
        Me.BtnRpmMinus50.TabIndex = 150
        '
        'BtnRpmPlus25
        '
        Me.BtnRpmPlus25.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmPlus25.Animated = True
        Me.BtnRpmPlus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.BorderRadius = 5
        Me.BtnRpmPlus25.BorderThickness = 1
        Me.BtnRpmPlus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.Image = CType(resources.GetObject("BtnRpmPlus25.Image"), System.Drawing.Image)
        Me.BtnRpmPlus25.Location = New System.Drawing.Point(312, 218)
        Me.BtnRpmPlus25.Name = "BtnRpmPlus25"
        Me.BtnRpmPlus25.Size = New System.Drawing.Size(30, 30)
        Me.BtnRpmPlus25.TabIndex = 147
        '
        'BtnRpmPlus50
        '
        Me.BtnRpmPlus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmPlus50.Animated = True
        Me.BtnRpmPlus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.BorderRadius = 5
        Me.BtnRpmPlus50.BorderThickness = 1
        Me.BtnRpmPlus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.Image = CType(resources.GetObject("BtnRpmPlus50.Image"), System.Drawing.Image)
        Me.BtnRpmPlus50.Location = New System.Drawing.Point(266, 208)
        Me.BtnRpmPlus50.Name = "BtnRpmPlus50"
        Me.BtnRpmPlus50.Size = New System.Drawing.Size(40, 40)
        Me.BtnRpmPlus50.TabIndex = 149
        '
        'BtnRpmMinus25
        '
        Me.BtnRpmMinus25.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmMinus25.Animated = True
        Me.BtnRpmMinus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.BorderRadius = 5
        Me.BtnRpmMinus25.BorderThickness = 1
        Me.BtnRpmMinus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.Image = CType(resources.GetObject("BtnRpmMinus25.Image"), System.Drawing.Image)
        Me.BtnRpmMinus25.Location = New System.Drawing.Point(312, 254)
        Me.BtnRpmMinus25.Name = "BtnRpmMinus25"
        Me.BtnRpmMinus25.Size = New System.Drawing.Size(30, 30)
        Me.BtnRpmMinus25.TabIndex = 148
        '
        'LblPCorte1
        '
        Me.LblPCorte1.AutoSize = True
        Me.LblPCorte1.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblPCorte1.ForeColor = System.Drawing.Color.White
        Me.LblPCorte1.Location = New System.Drawing.Point(145, 234)
        Me.LblPCorte1.Name = "LblPCorte1"
        Me.LblPCorte1.Size = New System.Drawing.Size(161, 25)
        Me.LblPCorte1.TabIndex = 156
        Me.LblPCorte1.Text = "Presión de Corte"
        '
        'BtnTrip
        '
        Me.BtnTrip.Animated = True
        Me.BtnTrip.BorderColor = System.Drawing.Color.White
        Me.BtnTrip.BorderRadius = 5
        Me.BtnTrip.BorderThickness = 1
        Me.BtnTrip.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTrip.FillColor = System.Drawing.Color.Transparent
        Me.BtnTrip.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnTrip.ForeColor = System.Drawing.Color.White
        Me.BtnTrip.Location = New System.Drawing.Point(170, 340)
        Me.BtnTrip.Name = "BtnTrip"
        Me.BtnTrip.Size = New System.Drawing.Size(111, 40)
        Me.BtnTrip.TabIndex = 160
        Me.BtnTrip.Text = "TRIP"
        '
        'LblPCorteDeseada
        '
        Me.LblPCorteDeseada.AutoSize = True
        Me.LblPCorteDeseada.Font = New System.Drawing.Font("Montserrat", 30.0!)
        Me.LblPCorteDeseada.ForeColor = System.Drawing.Color.White
        Me.LblPCorteDeseada.Location = New System.Drawing.Point(206, 266)
        Me.LblPCorteDeseada.Name = "LblPCorteDeseada"
        Me.LblPCorteDeseada.Size = New System.Drawing.Size(39, 55)
        Me.LblPCorteDeseada.TabIndex = 158
        Me.LblPCorteDeseada.Text = "-"
        '
        'LblPCorte3
        '
        Me.LblPCorte3.AutoSize = True
        Me.LblPCorte3.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblPCorte3.ForeColor = System.Drawing.Color.White
        Me.LblPCorte3.Location = New System.Drawing.Point(154, 54)
        Me.LblPCorte3.Name = "LblPCorte3"
        Me.LblPCorte3.Size = New System.Drawing.Size(142, 25)
        Me.LblPCorte3.TabIndex = 157
        Me.LblPCorte3.Text = "Presión Actual"
        '
        'LblPCorteActual
        '
        Me.LblPCorteActual.AutoSize = True
        Me.LblPCorteActual.Font = New System.Drawing.Font("Montserrat", 35.0!)
        Me.LblPCorteActual.ForeColor = System.Drawing.Color.White
        Me.LblPCorteActual.Location = New System.Drawing.Point(202, 89)
        Me.LblPCorteActual.Name = "LblPCorteActual"
        Me.LblPCorteActual.Size = New System.Drawing.Size(46, 65)
        Me.LblPCorteActual.TabIndex = 159
        Me.LblPCorteActual.Text = "-"
        '
        'RgPresion
        '
        Me.RgPresion.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgPresion.ArrowColor = System.Drawing.Color.Silver
        Me.RgPresion.ArrowThickness = 0
        Me.RgPresion.ArrowVisible = False
        Me.RgPresion.BackColor = System.Drawing.Color.Transparent
        Me.RgPresion.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgPresion.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgPresion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgPresion.Location = New System.Drawing.Point(0, 0)
        Me.RgPresion.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgPresion.Name = "RgPresion"
        Me.RgPresion.ProgressColor = System.Drawing.Color.Lime
        Me.RgPresion.ProgressColor2 = System.Drawing.Color.DarkGreen
        Me.RgPresion.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresion.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresion.ProgressThickness = 12
        Me.RgPresion.ShowPercentage = False
        Me.RgPresion.Size = New System.Drawing.Size(451, 451)
        Me.RgPresion.TabIndex = 162
        Me.RgPresion.UseTransparentBackground = True
        Me.RgPresion.Value = 100
        '
        'Guna2Panel3
        '
        Me.Guna2Panel3.Controls.Add(Me.Label2)
        Me.Guna2Panel3.Controls.Add(Me.Label1)
        Me.Guna2Panel3.Controls.Add(Me.BtnTripZero)
        Me.Guna2Panel3.Controls.Add(Me.LblPCorte1)
        Me.Guna2Panel3.Controls.Add(Me.BtnTrip)
        Me.Guna2Panel3.Controls.Add(Me.LblPCorteDeseada)
        Me.Guna2Panel3.Controls.Add(Me.LblPCorte3)
        Me.Guna2Panel3.Controls.Add(Me.LblPCorteActual)
        Me.Guna2Panel3.Controls.Add(Me.RgPresion)
        Me.Guna2Panel3.Location = New System.Drawing.Point(1312, 3)
        Me.Guna2Panel3.Name = "Guna2Panel3"
        Me.Guna2Panel3.Size = New System.Drawing.Size(451, 451)
        Me.Guna2Panel3.TabIndex = 183
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(315, 283)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 20)
        Me.Label2.TabIndex = 164
        Me.Label2.Text = "[PSI]"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(315, 111)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 20)
        Me.Label1.TabIndex = 163
        Me.Label1.Text = "[PSI]"
        '
        'BtnTripZero
        '
        Me.BtnTripZero.Animated = True
        Me.BtnTripZero.BorderColor = System.Drawing.Color.White
        Me.BtnTripZero.BorderRadius = 5
        Me.BtnTripZero.BorderThickness = 1
        Me.BtnTripZero.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTripZero.FillColor = System.Drawing.Color.Transparent
        Me.BtnTripZero.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnTripZero.ForeColor = System.Drawing.Color.White
        Me.BtnTripZero.Location = New System.Drawing.Point(170, 158)
        Me.BtnTripZero.Name = "BtnTripZero"
        Me.BtnTripZero.Size = New System.Drawing.Size(111, 40)
        Me.BtnTripZero.TabIndex = 161
        Me.BtnTripZero.Text = "ZERO"
        Me.BtnTripZero.Visible = False
        '
        'LblTransPresion
        '
        Me.LblTransPresion.AutoSize = True
        Me.LblTransPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransPresion.ForeColor = System.Drawing.Color.White
        Me.LblTransPresion.Location = New System.Drawing.Point(90, 87)
        Me.LblTransPresion.Name = "LblTransPresion"
        Me.LblTransPresion.Size = New System.Drawing.Size(14, 20)
        Me.LblTransPresion.TabIndex = 23
        Me.LblTransPresion.Text = "-"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(54, 108)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(41, 20)
        Me.Label54.TabIndex = 37
        Me.Label54.Text = "HHP"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(64, 108)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(21, 20)
        Me.Label55.TabIndex = 36
        Me.Label55.Text = "%"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label56.ForeColor = System.Drawing.Color.White
        Me.Label56.Location = New System.Drawing.Point(58, 108)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(32, 20)
        Me.Label56.TabIndex = 35
        Me.Label56.Text = "PSI"
        '
        'LblBombaHHP
        '
        Me.LblBombaHHP.AutoSize = True
        Me.LblBombaHHP.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaHHP.ForeColor = System.Drawing.Color.White
        Me.LblBombaHHP.Location = New System.Drawing.Point(67, 64)
        Me.LblBombaHHP.Name = "LblBombaHHP"
        Me.LblBombaHHP.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaHHP.TabIndex = 34
        Me.LblBombaHHP.Text = "-"
        '
        'LblHHP
        '
        Me.LblHHP.AutoSize = True
        Me.LblHHP.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblHHP.ForeColor = System.Drawing.Color.White
        Me.LblHHP.Location = New System.Drawing.Point(54, 35)
        Me.LblHHP.Name = "LblHHP"
        Me.LblHHP.Size = New System.Drawing.Size(41, 20)
        Me.LblHHP.TabIndex = 33
        Me.LblHHP.Text = "HHP"
        '
        'LblBombaCargaMotor
        '
        Me.LblBombaCargaMotor.AutoSize = True
        Me.LblBombaCargaMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaCargaMotor.ForeColor = System.Drawing.Color.White
        Me.LblBombaCargaMotor.Location = New System.Drawing.Point(67, 64)
        Me.LblBombaCargaMotor.Name = "LblBombaCargaMotor"
        Me.LblBombaCargaMotor.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaCargaMotor.TabIndex = 32
        Me.LblBombaCargaMotor.Text = "-"
        '
        'LblBombaPresSuccion
        '
        Me.LblBombaPresSuccion.AutoSize = True
        Me.LblBombaPresSuccion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresSuccion.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresSuccion.Location = New System.Drawing.Point(67, 64)
        Me.LblBombaPresSuccion.Name = "LblBombaPresSuccion"
        Me.LblBombaPresSuccion.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaPresSuccion.TabIndex = 31
        Me.LblBombaPresSuccion.Text = "-"
        '
        'LblCargaMotor
        '
        Me.LblCargaMotor.AutoSize = True
        Me.LblCargaMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCargaMotor.ForeColor = System.Drawing.Color.White
        Me.LblCargaMotor.Location = New System.Drawing.Point(17, 35)
        Me.LblCargaMotor.Name = "LblCargaMotor"
        Me.LblCargaMotor.Size = New System.Drawing.Size(114, 20)
        Me.LblCargaMotor.TabIndex = 30
        Me.LblCargaMotor.Text = "% Carga Motor"
        '
        'LblPresSuc
        '
        Me.LblPresSuc.AutoSize = True
        Me.LblPresSuc.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPresSuc.ForeColor = System.Drawing.Color.White
        Me.LblPresSuc.Location = New System.Drawing.Point(23, 35)
        Me.LblPresSuc.Name = "LblPresSuc"
        Me.LblPresSuc.Size = New System.Drawing.Size(103, 20)
        Me.LblPresSuc.TabIndex = 29
        Me.LblPresSuc.Text = "Pres. Succión"
        '
        'Guna2Panel7
        '
        Me.Guna2Panel7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel7.BorderRadius = 6
        Me.Guna2Panel7.BorderThickness = 1
        Me.Guna2Panel7.Controls.Add(Me.Guna2Panel19)
        Me.Guna2Panel7.Controls.Add(Me.Guna2Panel20)
        Me.Guna2Panel7.Controls.Add(Me.Guna2Panel21)
        Me.Guna2Panel7.Controls.Add(Me.Guna2Panel22)
        Me.Guna2Panel7.Controls.Add(Me.Guna2Panel23)
        Me.Guna2Panel7.Controls.Add(Me.Guna2Panel24)
        Me.Guna2Panel7.Controls.Add(Me.Guna2CustomGradientPanel7)
        Me.Guna2Panel7.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel7.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel7.Location = New System.Drawing.Point(1235, 535)
        Me.Guna2Panel7.Name = "Guna2Panel7"
        Me.Guna2Panel7.Size = New System.Drawing.Size(604, 340)
        Me.Guna2Panel7.TabIndex = 169
        '
        'Guna2Panel19
        '
        Me.Guna2Panel19.Controls.Add(Me.Label54)
        Me.Guna2Panel19.Controls.Add(Me.LblHHP)
        Me.Guna2Panel19.Controls.Add(Me.LblBombaHHP)
        Me.Guna2Panel19.Controls.Add(Me.Guna2RadialGauge10)
        Me.Guna2Panel19.Location = New System.Drawing.Point(453, 186)
        Me.Guna2Panel19.Name = "Guna2Panel19"
        Me.Guna2Panel19.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel19.TabIndex = 197
        '
        'Guna2RadialGauge10
        '
        Me.Guna2RadialGauge10.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge10.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge10.ArrowThickness = 0
        Me.Guna2RadialGauge10.ArrowVisible = False
        Me.Guna2RadialGauge10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge10.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2RadialGauge10.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.Guna2RadialGauge10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge10.Location = New System.Drawing.Point(0, 0)
        Me.Guna2RadialGauge10.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge10.Name = "Guna2RadialGauge10"
        Me.Guna2RadialGauge10.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.Guna2RadialGauge10.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.Guna2RadialGauge10.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge10.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge10.ProgressThickness = 4
        Me.Guna2RadialGauge10.ShowPercentage = False
        Me.Guna2RadialGauge10.Size = New System.Drawing.Size(148, 148)
        Me.Guna2RadialGauge10.TabIndex = 185
        Me.Guna2RadialGauge10.UseTransparentBackground = True
        Me.Guna2RadialGauge10.Value = 100
        '
        'Guna2Panel20
        '
        Me.Guna2Panel20.Controls.Add(Me.LblBombaHorasMotor)
        Me.Guna2Panel20.Controls.Add(Me.RgHorasMotor)
        Me.Guna2Panel20.Location = New System.Drawing.Point(227, 186)
        Me.Guna2Panel20.Name = "Guna2Panel20"
        Me.Guna2Panel20.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel20.TabIndex = 196
        '
        'LblBombaHorasMotor
        '
        Me.LblBombaHorasMotor.AutoSize = True
        Me.LblBombaHorasMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaHorasMotor.ForeColor = System.Drawing.Color.White
        Me.LblBombaHorasMotor.Location = New System.Drawing.Point(66, 64)
        Me.LblBombaHorasMotor.Name = "LblBombaHorasMotor"
        Me.LblBombaHorasMotor.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaHorasMotor.TabIndex = 25
        Me.LblBombaHorasMotor.Text = "-"
        '
        'RgHorasMotor
        '
        Me.RgHorasMotor.Animated = True
        Me.RgHorasMotor.AnimationSpeed = 0.1!
        Me.RgHorasMotor.Controls.Add(Me.LblHM)
        Me.RgHorasMotor.Controls.Add(Me.Label63)
        Me.RgHorasMotor.FillColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.RgHorasMotor.FillThickness = 0
        Me.RgHorasMotor.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.RgHorasMotor.ForeColor = System.Drawing.Color.White
        Me.RgHorasMotor.InnerColor = System.Drawing.Color.Transparent
        Me.RgHorasMotor.Location = New System.Drawing.Point(9, 10)
        Me.RgHorasMotor.Maximum = 1000
        Me.RgHorasMotor.Minimum = 0
        Me.RgHorasMotor.Name = "RgHorasMotor"
        Me.RgHorasMotor.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgHorasMotor.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgHorasMotor.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgHorasMotor.ProgressThickness = 4
        Me.RgHorasMotor.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle
        Me.RgHorasMotor.Size = New System.Drawing.Size(129, 129)
        Me.RgHorasMotor.TabIndex = 187
        Me.RgHorasMotor.Text = "Guna2CircleProgressBar2"
        Me.RgHorasMotor.Value = 450
        '
        'LblHM
        '
        Me.LblHM.AutoSize = True
        Me.LblHM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblHM.ForeColor = System.Drawing.Color.White
        Me.LblHM.Location = New System.Drawing.Point(26, 30)
        Me.LblHM.Name = "LblHM"
        Me.LblHM.Size = New System.Drawing.Size(77, 20)
        Me.LblHM.TabIndex = 24
        Me.LblHM.Text = "Hs. Motor"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label63.ForeColor = System.Drawing.Color.White
        Me.Label63.Location = New System.Drawing.Point(50, 88)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(26, 20)
        Me.Label63.TabIndex = 28
        Me.Label63.Text = "Hr"
        '
        'Guna2Panel21
        '
        Me.Guna2Panel21.Controls.Add(Me.LblCargaMotor)
        Me.Guna2Panel21.Controls.Add(Me.Label55)
        Me.Guna2Panel21.Controls.Add(Me.LblBombaCargaMotor)
        Me.Guna2Panel21.Controls.Add(Me.RgCargaMotor)
        Me.Guna2Panel21.Location = New System.Drawing.Point(3, 186)
        Me.Guna2Panel21.Name = "Guna2Panel21"
        Me.Guna2Panel21.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel21.TabIndex = 195
        '
        'RgCargaMotor
        '
        Me.RgCargaMotor.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgCargaMotor.ArrowColor = System.Drawing.Color.Silver
        Me.RgCargaMotor.ArrowThickness = 0
        Me.RgCargaMotor.ArrowVisible = False
        Me.RgCargaMotor.BackColor = System.Drawing.Color.Transparent
        Me.RgCargaMotor.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgCargaMotor.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgCargaMotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgCargaMotor.Location = New System.Drawing.Point(0, 0)
        Me.RgCargaMotor.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgCargaMotor.Name = "RgCargaMotor"
        Me.RgCargaMotor.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgCargaMotor.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgCargaMotor.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCargaMotor.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCargaMotor.ProgressThickness = 4
        Me.RgCargaMotor.ShowPercentage = False
        Me.RgCargaMotor.Size = New System.Drawing.Size(148, 148)
        Me.RgCargaMotor.TabIndex = 185
        Me.RgCargaMotor.UseTransparentBackground = True
        Me.RgCargaMotor.Value = 100
        '
        'Guna2Panel22
        '
        Me.Guna2Panel22.Controls.Add(Me.LblPresLub)
        Me.Guna2Panel22.Controls.Add(Me.LblBombaPresLUB)
        Me.Guna2Panel22.Controls.Add(Me.Label64)
        Me.Guna2Panel22.Controls.Add(Me.RgPresLub)
        Me.Guna2Panel22.Location = New System.Drawing.Point(453, 32)
        Me.Guna2Panel22.Name = "Guna2Panel22"
        Me.Guna2Panel22.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel22.TabIndex = 194
        '
        'LblPresLub
        '
        Me.LblPresLub.AutoSize = True
        Me.LblPresLub.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPresLub.ForeColor = System.Drawing.Color.White
        Me.LblPresLub.Location = New System.Drawing.Point(26, 35)
        Me.LblPresLub.Name = "LblPresLub"
        Me.LblPresLub.Size = New System.Drawing.Size(96, 20)
        Me.LblPresLub.TabIndex = 21
        Me.LblPresLub.Text = "Presión LUB"
        '
        'LblBombaPresLUB
        '
        Me.LblBombaPresLUB.AutoSize = True
        Me.LblBombaPresLUB.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresLUB.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresLUB.Location = New System.Drawing.Point(67, 64)
        Me.LblBombaPresLUB.Name = "LblBombaPresLUB"
        Me.LblBombaPresLUB.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaPresLUB.TabIndex = 23
        Me.LblBombaPresLUB.Text = "-"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(58, 108)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(32, 20)
        Me.Label64.TabIndex = 27
        Me.Label64.Text = "PSI"
        '
        'RgPresLub
        '
        Me.RgPresLub.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgPresLub.ArrowColor = System.Drawing.Color.Silver
        Me.RgPresLub.ArrowThickness = 0
        Me.RgPresLub.ArrowVisible = False
        Me.RgPresLub.BackColor = System.Drawing.Color.Transparent
        Me.RgPresLub.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgPresLub.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgPresLub.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgPresLub.Location = New System.Drawing.Point(0, 0)
        Me.RgPresLub.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgPresLub.Name = "RgPresLub"
        Me.RgPresLub.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgPresLub.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgPresLub.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresLub.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresLub.ProgressThickness = 4
        Me.RgPresLub.ShowPercentage = False
        Me.RgPresLub.Size = New System.Drawing.Size(148, 148)
        Me.RgPresLub.TabIndex = 185
        Me.RgPresLub.UseTransparentBackground = True
        Me.RgPresLub.Value = 100
        '
        'Guna2Panel23
        '
        Me.Guna2Panel23.Controls.Add(Me.LblPresSuc)
        Me.Guna2Panel23.Controls.Add(Me.LblBombaPresSuccion)
        Me.Guna2Panel23.Controls.Add(Me.Label56)
        Me.Guna2Panel23.Controls.Add(Me.RgPresSucc)
        Me.Guna2Panel23.Location = New System.Drawing.Point(227, 32)
        Me.Guna2Panel23.Name = "Guna2Panel23"
        Me.Guna2Panel23.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel23.TabIndex = 193
        '
        'RgPresSucc
        '
        Me.RgPresSucc.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgPresSucc.ArrowColor = System.Drawing.Color.Silver
        Me.RgPresSucc.ArrowThickness = 0
        Me.RgPresSucc.ArrowVisible = False
        Me.RgPresSucc.BackColor = System.Drawing.Color.Transparent
        Me.RgPresSucc.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgPresSucc.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgPresSucc.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgPresSucc.Location = New System.Drawing.Point(0, 0)
        Me.RgPresSucc.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgPresSucc.Name = "RgPresSucc"
        Me.RgPresSucc.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgPresSucc.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgPresSucc.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresSucc.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresSucc.ProgressThickness = 4
        Me.RgPresSucc.ShowPercentage = False
        Me.RgPresSucc.Size = New System.Drawing.Size(148, 148)
        Me.RgPresSucc.TabIndex = 185
        Me.RgPresSucc.UseTransparentBackground = True
        Me.RgPresSucc.Value = 100
        '
        'Guna2Panel24
        '
        Me.Guna2Panel24.Controls.Add(Me.LblTempLub)
        Me.Guna2Panel24.Controls.Add(Me.LblBombaTempLUB)
        Me.Guna2Panel24.Controls.Add(Me.Label65)
        Me.Guna2Panel24.Controls.Add(Me.RgTempLub)
        Me.Guna2Panel24.Location = New System.Drawing.Point(3, 32)
        Me.Guna2Panel24.Name = "Guna2Panel24"
        Me.Guna2Panel24.Size = New System.Drawing.Size(148, 148)
        Me.Guna2Panel24.TabIndex = 192
        '
        'LblTempLub
        '
        Me.LblTempLub.AutoSize = True
        Me.LblTempLub.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTempLub.ForeColor = System.Drawing.Color.White
        Me.LblTempLub.Location = New System.Drawing.Point(32, 35)
        Me.LblTempLub.Name = "LblTempLub"
        Me.LblTempLub.Size = New System.Drawing.Size(85, 20)
        Me.LblTempLub.TabIndex = 20
        Me.LblTempLub.Text = "Temp. LUB"
        '
        'LblBombaTempLUB
        '
        Me.LblBombaTempLUB.AutoSize = True
        Me.LblBombaTempLUB.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaTempLUB.ForeColor = System.Drawing.Color.White
        Me.LblBombaTempLUB.Location = New System.Drawing.Point(67, 64)
        Me.LblBombaTempLUB.Name = "LblBombaTempLUB"
        Me.LblBombaTempLUB.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaTempLUB.TabIndex = 22
        Me.LblBombaTempLUB.Text = "-"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(62, 108)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(25, 20)
        Me.Label65.TabIndex = 26
        Me.Label65.Text = "ºC"
        '
        'RgTempLub
        '
        Me.RgTempLub.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgTempLub.ArrowColor = System.Drawing.Color.Silver
        Me.RgTempLub.ArrowThickness = 0
        Me.RgTempLub.ArrowVisible = False
        Me.RgTempLub.BackColor = System.Drawing.Color.Transparent
        Me.RgTempLub.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgTempLub.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgTempLub.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgTempLub.Location = New System.Drawing.Point(0, 0)
        Me.RgTempLub.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgTempLub.Name = "RgTempLub"
        Me.RgTempLub.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgTempLub.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgTempLub.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempLub.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempLub.ProgressThickness = 4
        Me.RgTempLub.ShowPercentage = False
        Me.RgTempLub.Size = New System.Drawing.Size(148, 148)
        Me.RgTempLub.TabIndex = 185
        Me.RgTempLub.UseTransparentBackground = True
        Me.RgTempLub.Value = 100
        '
        'Guna2CustomGradientPanel7
        '
        Me.Guna2CustomGradientPanel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.BorderRadius = 6
        Me.Guna2CustomGradientPanel7.Controls.Add(Me.LblBomba)
        Me.Guna2CustomGradientPanel7.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel7.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel7.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel7.Name = "Guna2CustomGradientPanel7"
        Me.Guna2CustomGradientPanel7.Size = New System.Drawing.Size(604, 26)
        Me.Guna2CustomGradientPanel7.TabIndex = 19
        '
        'LblBomba
        '
        Me.LblBomba.AutoSize = True
        Me.LblBomba.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblBomba.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblBomba.Location = New System.Drawing.Point(270, 3)
        Me.LblBomba.Name = "LblBomba"
        Me.LblBomba.Size = New System.Drawing.Size(65, 21)
        Me.LblBomba.TabIndex = 19
        Me.LblBomba.Text = "Bomba"
        '
        'LblCaudal
        '
        Me.LblCaudal.AutoSize = True
        Me.LblCaudal.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblCaudal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCaudal.Location = New System.Drawing.Point(102, 3)
        Me.LblCaudal.Name = "LblCaudal"
        Me.LblCaudal.Size = New System.Drawing.Size(63, 21)
        Me.LblCaudal.TabIndex = 19
        Me.LblCaudal.Text = "Caudal"
        '
        'Guna2Panel10
        '
        Me.Guna2Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.BorderRadius = 6
        Me.Guna2Panel10.BorderThickness = 1
        Me.Guna2Panel10.Controls.Add(Me.Label81)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudalActual)
        Me.Guna2Panel10.Controls.Add(Me.Guna2CustomGradientPanel10)
        Me.Guna2Panel10.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.Location = New System.Drawing.Point(1404, 460)
        Me.Guna2Panel10.Name = "Guna2Panel10"
        Me.Guna2Panel10.Size = New System.Drawing.Size(267, 69)
        Me.Guna2Panel10.TabIndex = 170
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.Label81.ForeColor = System.Drawing.Color.White
        Me.Label81.Location = New System.Drawing.Point(164, 36)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(92, 25)
        Me.Label81.TabIndex = 23
        Me.Label81.Text = "BBL/Min"
        '
        'LblCaudalActual
        '
        Me.LblCaudalActual.AutoSize = True
        Me.LblCaudalActual.Font = New System.Drawing.Font("Montserrat", 18.0!)
        Me.LblCaudalActual.ForeColor = System.Drawing.Color.White
        Me.LblCaudalActual.Location = New System.Drawing.Point(120, 31)
        Me.LblCaudalActual.Name = "LblCaudalActual"
        Me.LblCaudalActual.Size = New System.Drawing.Size(24, 33)
        Me.LblCaudalActual.TabIndex = 22
        Me.LblCaudalActual.Text = "-"
        '
        'Guna2CustomGradientPanel10
        '
        Me.Guna2CustomGradientPanel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.BorderRadius = 6
        Me.Guna2CustomGradientPanel10.Controls.Add(Me.LblCaudal)
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel10.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel10.Name = "Guna2CustomGradientPanel10"
        Me.Guna2CustomGradientPanel10.Size = New System.Drawing.Size(267, 26)
        Me.Guna2CustomGradientPanel10.TabIndex = 19
        '
        'Guna2CustomGradientPanel11
        '
        Me.Guna2CustomGradientPanel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.BorderRadius = 6
        Me.Guna2CustomGradientPanel11.Controls.Add(Me.LblOpciones)
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel11.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel11.Name = "Guna2CustomGradientPanel11"
        Me.Guna2CustomGradientPanel11.Size = New System.Drawing.Size(604, 26)
        Me.Guna2CustomGradientPanel11.TabIndex = 19
        '
        'LblOpciones
        '
        Me.LblOpciones.AutoSize = True
        Me.LblOpciones.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblOpciones.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblOpciones.Location = New System.Drawing.Point(259, 3)
        Me.LblOpciones.Name = "LblOpciones"
        Me.LblOpciones.Size = New System.Drawing.Size(87, 21)
        Me.LblOpciones.TabIndex = 19
        Me.LblOpciones.Text = "Operación"
        '
        'BtnEmpezarTrabajo
        '
        Me.BtnEmpezarTrabajo.Animated = True
        Me.BtnEmpezarTrabajo.BorderColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.BorderRadius = 5
        Me.BtnEmpezarTrabajo.BorderThickness = 1
        Me.BtnEmpezarTrabajo.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEmpezarTrabajo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEmpezarTrabajo.FillColor = System.Drawing.Color.Transparent
        Me.BtnEmpezarTrabajo.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnEmpezarTrabajo.ForeColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.Location = New System.Drawing.Point(6, 30)
        Me.BtnEmpezarTrabajo.Name = "BtnEmpezarTrabajo"
        Me.BtnEmpezarTrabajo.Size = New System.Drawing.Size(176, 35)
        Me.BtnEmpezarTrabajo.TabIndex = 54
        Me.BtnEmpezarTrabajo.Text = "Empezar Trabajo"
        '
        'BtnSemiAuto
        '
        Me.BtnSemiAuto.Animated = True
        Me.BtnSemiAuto.BorderColor = System.Drawing.Color.White
        Me.BtnSemiAuto.BorderRadius = 5
        Me.BtnSemiAuto.BorderThickness = 1
        Me.BtnSemiAuto.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSemiAuto.FillColor = System.Drawing.Color.Transparent
        Me.BtnSemiAuto.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnSemiAuto.ForeColor = System.Drawing.Color.White
        Me.BtnSemiAuto.Location = New System.Drawing.Point(370, 30)
        Me.BtnSemiAuto.Name = "BtnSemiAuto"
        Me.BtnSemiAuto.Size = New System.Drawing.Size(228, 35)
        Me.BtnSemiAuto.TabIndex = 53
        Me.BtnSemiAuto.Text = "Modo Semi-Automatico"
        '
        'BtnEncenderLuces
        '
        Me.BtnEncenderLuces.Animated = True
        Me.BtnEncenderLuces.BorderColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.BorderRadius = 5
        Me.BtnEncenderLuces.BorderThickness = 1
        Me.BtnEncenderLuces.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEncenderLuces.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEncenderLuces.FillColor = System.Drawing.Color.Transparent
        Me.BtnEncenderLuces.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnEncenderLuces.ForeColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.Location = New System.Drawing.Point(188, 30)
        Me.BtnEncenderLuces.Name = "BtnEncenderLuces"
        Me.BtnEncenderLuces.Size = New System.Drawing.Size(176, 35)
        Me.BtnEncenderLuces.TabIndex = 52
        Me.BtnEncenderLuces.Text = "Encender Luces"
        '
        'Guna2Panel11
        '
        Me.Guna2Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.BorderRadius = 6
        Me.Guna2Panel11.BorderThickness = 1
        Me.Guna2Panel11.Controls.Add(Me.BtnEmpezarTrabajo)
        Me.Guna2Panel11.Controls.Add(Me.BtnSemiAuto)
        Me.Guna2Panel11.Controls.Add(Me.BtnEncenderLuces)
        Me.Guna2Panel11.Controls.Add(Me.Guna2CustomGradientPanel11)
        Me.Guna2Panel11.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.Location = New System.Drawing.Point(15, 460)
        Me.Guna2Panel11.Name = "Guna2Panel11"
        Me.Guna2Panel11.Size = New System.Drawing.Size(604, 69)
        Me.Guna2Panel11.TabIndex = 174
        '
        'BtnFC
        '
        Me.BtnFC.Animated = True
        Me.BtnFC.BorderColor = System.Drawing.Color.White
        Me.BtnFC.BorderRadius = 5
        Me.BtnFC.BorderThickness = 1
        Me.BtnFC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFC.FillColor = System.Drawing.Color.Transparent
        Me.BtnFC.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnFC.ForeColor = System.Drawing.Color.White
        Me.BtnFC.Location = New System.Drawing.Point(1677, 484)
        Me.BtnFC.Name = "BtnFC"
        Me.BtnFC.Size = New System.Drawing.Size(45, 45)
        Me.BtnFC.TabIndex = 172
        Me.BtnFC.Text = "FC"
        Me.BtnFC.Visible = False
        '
        'BtnDiagnostico
        '
        Me.BtnDiagnostico.Animated = True
        Me.BtnDiagnostico.BorderColor = System.Drawing.Color.White
        Me.BtnDiagnostico.BorderRadius = 5
        Me.BtnDiagnostico.BorderThickness = 1
        Me.BtnDiagnostico.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDiagnostico.FillColor = System.Drawing.Color.Transparent
        Me.BtnDiagnostico.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.BtnDiagnostico.ForeColor = System.Drawing.Color.White
        Me.BtnDiagnostico.Location = New System.Drawing.Point(849, 193)
        Me.BtnDiagnostico.Name = "BtnDiagnostico"
        Me.BtnDiagnostico.Size = New System.Drawing.Size(168, 45)
        Me.BtnDiagnostico.TabIndex = 171
        Me.BtnDiagnostico.Text = "Diagnóstico"
        '
        'BtnErrores
        '
        Me.BtnErrores.Animated = True
        Me.BtnErrores.BorderColor = System.Drawing.Color.White
        Me.BtnErrores.BorderRadius = 5
        Me.BtnErrores.BorderThickness = 1
        Me.BtnErrores.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnErrores.FillColor = System.Drawing.Color.Transparent
        Me.BtnErrores.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnErrores.ForeColor = System.Drawing.Color.White
        Me.BtnErrores.Location = New System.Drawing.Point(876, 150)
        Me.BtnErrores.Name = "BtnErrores"
        Me.BtnErrores.Size = New System.Drawing.Size(97, 36)
        Me.BtnErrores.TabIndex = 173
        Me.BtnErrores.Text = "Errores"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(76, 145)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(42, 20)
        Me.Label41.TabIndex = 28
        Me.Label41.Text = "RPM"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(81, 145)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(32, 20)
        Me.Label42.TabIndex = 27
        Me.Label42.Text = "PSI"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label43.ForeColor = System.Drawing.Color.White
        Me.Label43.Location = New System.Drawing.Point(85, 145)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(25, 20)
        Me.Label43.TabIndex = 26
        Me.Label43.Text = "ºC"
        '
        'LblTransRpmOut
        '
        Me.LblTransRpmOut.AutoSize = True
        Me.LblTransRpmOut.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransRpmOut.ForeColor = System.Drawing.Color.White
        Me.LblTransRpmOut.Location = New System.Drawing.Point(90, 87)
        Me.LblTransRpmOut.Name = "LblTransRpmOut"
        Me.LblTransRpmOut.Size = New System.Drawing.Size(14, 20)
        Me.LblTransRpmOut.TabIndex = 25
        Me.LblTransRpmOut.Text = "-"
        '
        'LblTrans5
        '
        Me.LblTrans5.AutoSize = True
        Me.LblTrans5.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTrans5.ForeColor = System.Drawing.Color.White
        Me.LblTrans5.Location = New System.Drawing.Point(56, 45)
        Me.LblTrans5.Name = "LblTrans5"
        Me.LblTrans5.Size = New System.Drawing.Size(82, 22)
        Me.LblTrans5.TabIndex = 24
        Me.LblTrans5.Text = "RPM Out"
        '
        'Guna2Panel5
        '
        Me.Guna2Panel5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.BorderRadius = 6
        Me.Guna2Panel5.BorderThickness = 1
        Me.Guna2Panel5.Controls.Add(Me.Guna2Panel18)
        Me.Guna2Panel5.Controls.Add(Me.Guna2Panel17)
        Me.Guna2Panel5.Controls.Add(Me.Guna2Panel16)
        Me.Guna2Panel5.Controls.Add(Me.Guna2CustomGradientPanel5)
        Me.Guna2Panel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.Location = New System.Drawing.Point(625, 535)
        Me.Guna2Panel5.Name = "Guna2Panel5"
        Me.Guna2Panel5.Size = New System.Drawing.Size(604, 340)
        Me.Guna2Panel5.TabIndex = 167
        '
        'Guna2Panel18
        '
        Me.Guna2Panel18.Controls.Add(Me.Label41)
        Me.Guna2Panel18.Controls.Add(Me.LblTrans5)
        Me.Guna2Panel18.Controls.Add(Me.LblTransRpmOut)
        Me.Guna2Panel18.Controls.Add(Me.RgRPMTransmision)
        Me.Guna2Panel18.Location = New System.Drawing.Point(204, 30)
        Me.Guna2Panel18.Name = "Guna2Panel18"
        Me.Guna2Panel18.Size = New System.Drawing.Size(195, 195)
        Me.Guna2Panel18.TabIndex = 191
        '
        'RgRPMTransmision
        '
        Me.RgRPMTransmision.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgRPMTransmision.ArrowColor = System.Drawing.Color.Silver
        Me.RgRPMTransmision.ArrowThickness = 0
        Me.RgRPMTransmision.ArrowVisible = False
        Me.RgRPMTransmision.BackColor = System.Drawing.Color.Transparent
        Me.RgRPMTransmision.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgRPMTransmision.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgRPMTransmision.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgRPMTransmision.Location = New System.Drawing.Point(0, 0)
        Me.RgRPMTransmision.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgRPMTransmision.Name = "RgRPMTransmision"
        Me.RgRPMTransmision.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgRPMTransmision.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgRPMTransmision.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPMTransmision.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPMTransmision.ProgressThickness = 4
        Me.RgRPMTransmision.ShowPercentage = False
        Me.RgRPMTransmision.Size = New System.Drawing.Size(195, 195)
        Me.RgRPMTransmision.TabIndex = 185
        Me.RgRPMTransmision.UseTransparentBackground = True
        Me.RgRPMTransmision.Value = 100
        '
        'Guna2Panel17
        '
        Me.Guna2Panel17.Controls.Add(Me.LblTrans3)
        Me.Guna2Panel17.Controls.Add(Me.Label42)
        Me.Guna2Panel17.Controls.Add(Me.LblTransPresion)
        Me.Guna2Panel17.Controls.Add(Me.RgPresionTransmision)
        Me.Guna2Panel17.Location = New System.Drawing.Point(405, 139)
        Me.Guna2Panel17.Name = "Guna2Panel17"
        Me.Guna2Panel17.Size = New System.Drawing.Size(195, 195)
        Me.Guna2Panel17.TabIndex = 190
        '
        'RgPresionTransmision
        '
        Me.RgPresionTransmision.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgPresionTransmision.ArrowColor = System.Drawing.Color.Silver
        Me.RgPresionTransmision.ArrowThickness = 0
        Me.RgPresionTransmision.ArrowVisible = False
        Me.RgPresionTransmision.BackColor = System.Drawing.Color.Transparent
        Me.RgPresionTransmision.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgPresionTransmision.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgPresionTransmision.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgPresionTransmision.Location = New System.Drawing.Point(0, 0)
        Me.RgPresionTransmision.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgPresionTransmision.Name = "RgPresionTransmision"
        Me.RgPresionTransmision.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgPresionTransmision.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgPresionTransmision.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresionTransmision.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresionTransmision.ProgressThickness = 4
        Me.RgPresionTransmision.ShowPercentage = False
        Me.RgPresionTransmision.Size = New System.Drawing.Size(195, 195)
        Me.RgPresionTransmision.TabIndex = 185
        Me.RgPresionTransmision.UseTransparentBackground = True
        Me.RgPresionTransmision.Value = 100
        '
        'Guna2Panel16
        '
        Me.Guna2Panel16.Controls.Add(Me.LblTrans1)
        Me.Guna2Panel16.Controls.Add(Me.LblTransTemp)
        Me.Guna2Panel16.Controls.Add(Me.Label43)
        Me.Guna2Panel16.Controls.Add(Me.RgTempTransmision)
        Me.Guna2Panel16.Location = New System.Drawing.Point(3, 139)
        Me.Guna2Panel16.Name = "Guna2Panel16"
        Me.Guna2Panel16.Size = New System.Drawing.Size(195, 195)
        Me.Guna2Panel16.TabIndex = 189
        '
        'RgTempTransmision
        '
        Me.RgTempTransmision.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgTempTransmision.ArrowColor = System.Drawing.Color.Silver
        Me.RgTempTransmision.ArrowThickness = 0
        Me.RgTempTransmision.ArrowVisible = False
        Me.RgTempTransmision.BackColor = System.Drawing.Color.Transparent
        Me.RgTempTransmision.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgTempTransmision.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgTempTransmision.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgTempTransmision.Location = New System.Drawing.Point(0, 0)
        Me.RgTempTransmision.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgTempTransmision.Name = "RgTempTransmision"
        Me.RgTempTransmision.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgTempTransmision.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgTempTransmision.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempTransmision.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempTransmision.ProgressThickness = 4
        Me.RgTempTransmision.ShowPercentage = False
        Me.RgTempTransmision.Size = New System.Drawing.Size(195, 195)
        Me.RgTempTransmision.TabIndex = 185
        Me.RgTempTransmision.UseTransparentBackground = True
        Me.RgTempTransmision.Value = 100
        '
        'LblErrorAdmisible
        '
        Me.LblErrorAdmisible.AutoSize = True
        Me.LblErrorAdmisible.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblErrorAdmisible.ForeColor = System.Drawing.Color.Orange
        Me.LblErrorAdmisible.Location = New System.Drawing.Point(714, 156)
        Me.LblErrorAdmisible.Name = "LblErrorAdmisible"
        Me.LblErrorAdmisible.Size = New System.Drawing.Size(152, 25)
        Me.LblErrorAdmisible.TabIndex = 186
        Me.LblErrorAdmisible.Text = "Error Admisible"
        Me.LblErrorAdmisible.Visible = False
        '
        'LblErrorGrave
        '
        Me.LblErrorGrave.AutoSize = True
        Me.LblErrorGrave.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblErrorGrave.ForeColor = System.Drawing.Color.Red
        Me.LblErrorGrave.Location = New System.Drawing.Point(979, 156)
        Me.LblErrorGrave.Name = "LblErrorGrave"
        Me.LblErrorGrave.Size = New System.Drawing.Size(113, 25)
        Me.LblErrorGrave.TabIndex = 185
        Me.LblErrorGrave.Text = "Error Grave"
        Me.LblErrorGrave.Visible = False
        '
        'TmrBackUp
        '
        Me.TmrBackUp.Interval = 1000
        '
        'BtnEKill
        '
        Me.BtnEKill.Animated = True
        Me.BtnEKill.BorderColor = System.Drawing.Color.White
        Me.BtnEKill.BorderRadius = 5
        Me.BtnEKill.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BtnEKill.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEKill.FillColor = System.Drawing.Color.Transparent
        Me.BtnEKill.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnEKill.ForeColor = System.Drawing.Color.White
        Me.BtnEKill.Image = Global.sFRAC.My.Resources.Resources.E_STOP
        Me.BtnEKill.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnEKill.ImageSize = New System.Drawing.Size(69, 69)
        Me.BtnEKill.Location = New System.Drawing.Point(3, 3)
        Me.BtnEKill.Name = "BtnEKill"
        Me.BtnEKill.Size = New System.Drawing.Size(70, 70)
        Me.BtnEKill.TabIndex = 187
        '
        'BtnMinusGear
        '
        Me.BtnMinusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnMinusGear.Animated = True
        Me.BtnMinusGear.BorderColor = System.Drawing.Color.White
        Me.BtnMinusGear.BorderRadius = 5
        Me.BtnMinusGear.BorderThickness = 1
        Me.BtnMinusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMinusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnMinusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnMinusGear.ForeColor = System.Drawing.Color.White
        Me.BtnMinusGear.Image = CType(resources.GetObject("BtnMinusGear.Image"), System.Drawing.Image)
        Me.BtnMinusGear.Location = New System.Drawing.Point(1095, 389)
        Me.BtnMinusGear.Name = "BtnMinusGear"
        Me.BtnMinusGear.Size = New System.Drawing.Size(40, 40)
        Me.BtnMinusGear.TabIndex = 180
        '
        'BtnPlusGear
        '
        Me.BtnPlusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnPlusGear.Animated = True
        Me.BtnPlusGear.BackColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.BorderColor = System.Drawing.Color.White
        Me.BtnPlusGear.BorderRadius = 5
        Me.BtnPlusGear.BorderThickness = 1
        Me.BtnPlusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPlusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnPlusGear.ForeColor = System.Drawing.Color.White
        Me.BtnPlusGear.Image = CType(resources.GetObject("BtnPlusGear.Image"), System.Drawing.Image)
        Me.BtnPlusGear.Location = New System.Drawing.Point(1095, 343)
        Me.BtnPlusGear.Name = "BtnPlusGear"
        Me.BtnPlusGear.Size = New System.Drawing.Size(40, 40)
        Me.BtnPlusGear.TabIndex = 179
        '
        'PbAutomatic
        '
        Me.PbAutomatic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAutomatic.Image = Global.sFRAC.My.Resources.Resources.automatic1
        Me.PbAutomatic.ImageRotate = 0!
        Me.PbAutomatic.Location = New System.Drawing.Point(1030, 39)
        Me.PbAutomatic.Name = "PbAutomatic"
        Me.PbAutomatic.Size = New System.Drawing.Size(70, 70)
        Me.PbAutomatic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAutomatic.TabIndex = 177
        Me.PbAutomatic.TabStop = False
        Me.PbAutomatic.Visible = False
        '
        'BtnAjustes
        '
        Me.BtnAjustes.Animated = True
        Me.BtnAjustes.BorderColor = System.Drawing.Color.White
        Me.BtnAjustes.BorderRadius = 5
        Me.BtnAjustes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAjustes.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAjustes.ForeColor = System.Drawing.Color.White
        Me.BtnAjustes.Image = Global.sFRAC.My.Resources.Resources.settings_1
        Me.BtnAjustes.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnAjustes.ImageSize = New System.Drawing.Size(69, 69)
        Me.BtnAjustes.Location = New System.Drawing.Point(897, 39)
        Me.BtnAjustes.Name = "BtnAjustes"
        Me.BtnAjustes.Size = New System.Drawing.Size(70, 70)
        Me.BtnAjustes.TabIndex = 176
        '
        'BtnStartEngine
        '
        Me.BtnStartEngine.Animated = True
        Me.BtnStartEngine.BorderColor = System.Drawing.Color.White
        Me.BtnStartEngine.BorderRadius = 5
        Me.BtnStartEngine.CheckedState.Image = Global.sFRAC.My.Resources.Resources.Engine_STOP
        Me.BtnStartEngine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStartEngine.FillColor = System.Drawing.Color.Transparent
        Me.BtnStartEngine.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnStartEngine.ForeColor = System.Drawing.Color.White
        Me.BtnStartEngine.Image = Global.sFRAC.My.Resources.Resources.Engine_START
        Me.BtnStartEngine.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnStartEngine.ImageSize = New System.Drawing.Size(69, 69)
        Me.BtnStartEngine.Location = New System.Drawing.Point(1776, 3)
        Me.BtnStartEngine.Name = "BtnStartEngine"
        Me.BtnStartEngine.Size = New System.Drawing.Size(70, 70)
        Me.BtnStartEngine.TabIndex = 175
        '
        'PbAlarms
        '
        Me.PbAlarms.FillColor = System.Drawing.Color.Silver
        Me.PbAlarms.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.PbAlarms.ForeColor = System.Drawing.Color.Transparent
        Me.PbAlarms.Location = New System.Drawing.Point(1003, 496)
        Me.PbAlarms.Name = "PbAlarms"
        Me.PbAlarms.ProgressColor = System.Drawing.Color.DarkGray
        Me.PbAlarms.ProgressColor2 = System.Drawing.Color.DarkGray
        Me.PbAlarms.Size = New System.Drawing.Size(140, 10)
        Me.PbAlarms.TabIndex = 201
        Me.PbAlarms.Text = "LU"
        Me.PbAlarms.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.PbAlarms.Value = 100
        '
        'PbLockUp
        '
        Me.PbLockUp.FillColor = System.Drawing.Color.Silver
        Me.PbLockUp.Location = New System.Drawing.Point(857, 496)
        Me.PbLockUp.Name = "PbLockUp"
        Me.PbLockUp.ProgressColor = System.Drawing.Color.DarkGray
        Me.PbLockUp.ProgressColor2 = System.Drawing.Color.DarkGray
        Me.PbLockUp.Size = New System.Drawing.Size(140, 10)
        Me.PbLockUp.TabIndex = 200
        Me.PbLockUp.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.PbLockUp.Value = 100
        '
        'PbMoxaStatus
        '
        Me.PbMoxaStatus.Enabled = False
        Me.PbMoxaStatus.Location = New System.Drawing.Point(711, 496)
        Me.PbMoxaStatus.Name = "PbMoxaStatus"
        Me.PbMoxaStatus.ProgressColor = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.PbMoxaStatus.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.PbMoxaStatus.Size = New System.Drawing.Size(140, 10)
        Me.PbMoxaStatus.TabIndex = 199
        Me.PbMoxaStatus.Text = "Guna2ProgressBar1"
        Me.PbMoxaStatus.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.PbMoxaStatus.Value = 100
        '
        'BtnParking
        '
        Me.BtnParking.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnParking.Animated = True
        Me.BtnParking.BackColor = System.Drawing.Color.Transparent
        Me.BtnParking.BorderColor = System.Drawing.Color.White
        Me.BtnParking.BorderRadius = 5
        Me.BtnParking.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnParking.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnParking.FillColor = System.Drawing.Color.Transparent
        Me.BtnParking.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnParking.ForeColor = System.Drawing.Color.White
        Me.BtnParking.Image = Global.sFRAC.My.Resources.Resources.brake
        Me.BtnParking.ImageSize = New System.Drawing.Size(38, 38)
        Me.BtnParking.Location = New System.Drawing.Point(1095, 297)
        Me.BtnParking.Name = "BtnParking"
        Me.BtnParking.Size = New System.Drawing.Size(40, 40)
        Me.BtnParking.TabIndex = 202
        '
        'TmrConnectionCheck
        '
        Me.TmrConnectionCheck.Enabled = True
        Me.TmrConnectionCheck.Interval = 1000
        '
        'OneFrac_1920x1080
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Controls.Add(Me.BtnParking)
        Me.Controls.Add(Me.PbAlarms)
        Me.Controls.Add(Me.PbLockUp)
        Me.Controls.Add(Me.PbMoxaStatus)
        Me.Controls.Add(Me.BtnEKill)
        Me.Controls.Add(Me.LblErrorAdmisible)
        Me.Controls.Add(Me.LblErrorGrave)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.Guna2Panel4)
        Me.Controls.Add(Me.BtnResetRPM)
        Me.Controls.Add(Me.BtnMinusGear)
        Me.Controls.Add(Me.BtnPlusGear)
        Me.Controls.Add(Me.PbAutomatic)
        Me.Controls.Add(Me.BtnAjustes)
        Me.Controls.Add(Me.BtnStartEngine)
        Me.Controls.Add(Me.Guna2Panel2)
        Me.Controls.Add(Me.Guna2Panel3)
        Me.Controls.Add(Me.Guna2Panel7)
        Me.Controls.Add(Me.Guna2Panel10)
        Me.Controls.Add(Me.Guna2Panel11)
        Me.Controls.Add(Me.BtnFC)
        Me.Controls.Add(Me.BtnDiagnostico)
        Me.Controls.Add(Me.BtnErrores)
        Me.Controls.Add(Me.Guna2Panel5)
        Me.DoubleBuffered = True
        Me.Name = "OneFrac_1920x1080"
        Me.Size = New System.Drawing.Size(1849, 878)
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.Guna2Panel4.ResumeLayout(False)
        Me.Guna2Panel13.ResumeLayout(False)
        Me.Guna2Panel13.PerformLayout()
        Me.Guna2Panel14.ResumeLayout(False)
        Me.Guna2Panel14.PerformLayout()
        Me.RgCombustible.ResumeLayout(False)
        Me.RgCombustible.PerformLayout()
        Me.Guna2Panel15.ResumeLayout(False)
        Me.Guna2Panel15.PerformLayout()
        Me.Guna2Panel9.ResumeLayout(False)
        Me.Guna2Panel9.PerformLayout()
        Me.Guna2Panel8.ResumeLayout(False)
        Me.Guna2Panel8.PerformLayout()
        Me.Guna2Panel6.ResumeLayout(False)
        Me.Guna2Panel6.PerformLayout()
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.Guna2CustomGradientPanel5.ResumeLayout(False)
        Me.Guna2CustomGradientPanel5.PerformLayout()
        Me.Guna2Panel2.ResumeLayout(False)
        Me.Guna2Panel2.PerformLayout()
        Me.Guna2Panel3.ResumeLayout(False)
        Me.Guna2Panel3.PerformLayout()
        Me.Guna2Panel7.ResumeLayout(False)
        Me.Guna2Panel19.ResumeLayout(False)
        Me.Guna2Panel19.PerformLayout()
        Me.Guna2Panel20.ResumeLayout(False)
        Me.Guna2Panel20.PerformLayout()
        Me.RgHorasMotor.ResumeLayout(False)
        Me.RgHorasMotor.PerformLayout()
        Me.Guna2Panel21.ResumeLayout(False)
        Me.Guna2Panel21.PerformLayout()
        Me.Guna2Panel22.ResumeLayout(False)
        Me.Guna2Panel22.PerformLayout()
        Me.Guna2Panel23.ResumeLayout(False)
        Me.Guna2Panel23.PerformLayout()
        Me.Guna2Panel24.ResumeLayout(False)
        Me.Guna2Panel24.PerformLayout()
        Me.Guna2CustomGradientPanel7.ResumeLayout(False)
        Me.Guna2CustomGradientPanel7.PerformLayout()
        Me.Guna2Panel10.ResumeLayout(False)
        Me.Guna2Panel10.PerformLayout()
        Me.Guna2CustomGradientPanel10.ResumeLayout(False)
        Me.Guna2CustomGradientPanel10.PerformLayout()
        Me.Guna2CustomGradientPanel11.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.PerformLayout()
        Me.Guna2Panel11.ResumeLayout(False)
        Me.Guna2Panel5.ResumeLayout(False)
        Me.Guna2Panel18.ResumeLayout(False)
        Me.Guna2Panel18.PerformLayout()
        Me.Guna2Panel17.ResumeLayout(False)
        Me.Guna2Panel17.PerformLayout()
        Me.Guna2Panel16.ResumeLayout(False)
        Me.Guna2Panel16.PerformLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public WithEvents LblTrans3 As Label
    Public WithEvents LblMarchaActual As Label
    Public WithEvents LblMarchaDeseada As Label
    Public WithEvents LblMarcha3 As Label
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblMarcha1 As Label
    Public WithEvents LblMarcha As Label
    Public WithEvents LblRPMActual As Label
    Public WithEvents LblRPMDeseada As Label
    Public WithEvents TmrErrorHandle As Timer
    Public WithEvents BtnIdle As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblMotor As Label
    Public WithEvents TmrCheck As Timer
    Public WithEvents Label23 As Label
    Public WithEvents Label24 As Label
    Public WithEvents LblRPM3 As Label
    Public WithEvents LblRPM1 As Label
    Public WithEvents Guna2Panel4 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label25 As Label
    Public WithEvents LblMotorCombustibleP100 As Label
    Public WithEvents LblCombustible1 As Label
    Public WithEvents LblMotorCombustible As Label
    Public WithEvents LblMotorTempAceite As Label
    Public WithEvents LblCombustible As Label
    Public WithEvents LblTempAceite As Label
    Public WithEvents Label22 As Label
    Public WithEvents Label21 As Label
    Public WithEvents Label20 As Label
    Public WithEvents LblMotorVoltaje As Label
    Public WithEvents LblVoltaje As Label
    Public WithEvents LblMotorPresion As Label
    Public WithEvents LblMotorTempAgua As Label
    Public WithEvents LblPresion As Label
    Public WithEvents LblTransmision As Label
    Public WithEvents LblTrans1 As Label
    Public WithEvents TmrReconnect As Timer
    Public WithEvents LblTransTemp As Label
    Public WithEvents TmrRead As Timer
    Public WithEvents TmrOp As Timer
    Public WithEvents BtnResetRPM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnMinusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnPlusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PbAutomatic As Guna.UI2.WinForms.Guna2PictureBox
    Public WithEvents BtnAjustes As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnStartEngine As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmMinus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmMinus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel2 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblPCorte1 As Label
    Public WithEvents BtnTrip As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblPCorteDeseada As Label
    Public WithEvents LblPCorte3 As Label
    Public WithEvents LblPCorteActual As Label
    Public WithEvents Guna2Panel3 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnTripZero As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblTransPresion As Label
    Public WithEvents Label54 As Label
    Public WithEvents Label55 As Label
    Public WithEvents Label56 As Label
    Public WithEvents LblBombaHHP As Label
    Public WithEvents LblHHP As Label
    Public WithEvents LblBombaCargaMotor As Label
    Public WithEvents LblBombaPresSuccion As Label
    Public WithEvents LblCargaMotor As Label
    Public WithEvents LblPresSuc As Label
    Public WithEvents Guna2Panel7 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label63 As Label
    Public WithEvents Label64 As Label
    Public WithEvents Label65 As Label
    Public WithEvents LblBombaHorasMotor As Label
    Public WithEvents LblHM As Label
    Public WithEvents LblBombaPresLUB As Label
    Public WithEvents LblBombaTempLUB As Label
    Public WithEvents LblPresLub As Label
    Public WithEvents LblTempLub As Label
    Public WithEvents LblBomba As Label
    Public WithEvents LblCaudal As Label
    Public WithEvents Guna2Panel10 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblCaudalActual As Label
    Public WithEvents LblOpciones As Label
    Public WithEvents BtnEmpezarTrabajo As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnSemiAuto As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnEncenderLuces As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel11 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnFC As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnDiagnostico As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnErrores As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Label41 As Label
    Public WithEvents Label42 As Label
    Public WithEvents Label43 As Label
    Public WithEvents LblTransRpmOut As Label
    Public WithEvents LblTrans5 As Label
    Public WithEvents Guna2Panel5 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents RgRPM As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel5 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents RgPresion As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2CustomGradientPanel7 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Label81 As Label
    Public WithEvents Guna2CustomGradientPanel10 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel11 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents RgTempAgua As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel6 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2Panel13 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgCombP100 As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel14 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2Panel15 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgVoltaje As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel9 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgPresionMotor As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel8 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgTempAceite As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents LblTempAgua As Label
    Friend WithEvents Guna2Panel18 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgRPMTransmision As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel17 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgPresionTransmision As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel16 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgTempTransmision As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel19 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2RadialGauge10 As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel20 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2Panel21 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgCargaMotor As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel22 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgPresLub As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel23 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgPresSucc As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents Guna2Panel24 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents RgTempLub As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents LblErrorAdmisible As Label
    Public WithEvents LblErrorGrave As Label
    Friend WithEvents RgHorasMotor As Guna.UI2.WinForms.Guna2CircleProgressBar
    Friend WithEvents RgCombustible As Guna.UI2.WinForms.Guna2CircleProgressBar
    Public WithEvents Label2 As Label
    Public WithEvents Label1 As Label
    Friend WithEvents TmrBackUp As Timer
    Public WithEvents BtnEKill As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents PbAlarms As Guna.UI2.WinForms.Guna2ProgressBar
    Friend WithEvents PbLockUp As Guna.UI2.WinForms.Guna2ProgressBar
    Friend WithEvents PbMoxaStatus As Guna.UI2.WinForms.Guna2ProgressBar
    Public WithEvents BtnParking As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents TmrConnectionCheck As Timer
End Class
