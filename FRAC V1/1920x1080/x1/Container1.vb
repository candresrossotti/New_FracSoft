﻿Public Class Container1_1920x1080

    Private Sub Container1_1920x1080_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Centramos los controles
        CenterEverything()

        ''Centramos verticalmente el control de add
        BtnAdd.Top = (BtnAdd.Parent.Height \ 2) - (BtnAdd.Height \ 2)

    End Sub

    ''Sub para centrar/alinear todos los objetos
    Public Sub CenterEverything()
        CenterObject(BtnAdd)
        CenterObject(Title1)
    End Sub

    ''Sub para agregar fracturador a la pantalla
    Private Sub BtnAdd_Click(sender As Object, e As EventArgs) Handles BtnAdd.Click
        If IFracSystem.ActiveOP = False Then
            If FracSelect.ShowDialog(Me) = DialogResult.OK Then
                Dim C1 = OneFrac.GetClass()
                Panel1.Controls.Add(C1)
                LSelect.OneFracLanguageSet(C1)
                C1.Show()
                CloseButton1.Enabled = True

                BtnAdd.Visible = False
                Title1.Text = CurrentFracName
                CenterObject(Title1)
            End If
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TrabajoActivo")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
        End If
    End Sub

    ''Sub para cerrar y desconectar un fracturador
    Private Sub CloseButton_Click(sender As Object, e As EventArgs) Handles CloseButton1.Click
        If IFracSystem.ActiveOP = False Then
            Dim CurrentPanel As Control() = Me.Controls.Find("Panel1", True)
            Dim CurrentFrac As Control() = CurrentPanel(0).Controls.Find("OneFrac_" + My.Settings.Resolucion, True)
            Dim C1 As Object = CurrentFrac(0)
            If CurrentPanel(0).Controls.Find("OneFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                ISystem.Disconnect(C1.Frac)
                C1.StopTimers()
            End If
            Panel1.Controls.RemoveAt(1)
            BtnAdd.Visible = True
            CloseButton1.Enabled = False
            Title1.Text = LanguageSelect.LanguageRM.GetString("Container.Title")
            CenterObject(Title1)

            If Not GetDuplicateFracs(C1.Frac) Then
                MaxFracs -= 1
                FracData.BoolVFrac("ActiveFrac_" + C1.Frac.ToString()) = False
            End If
            ConnectedFracsID.Remove(C1.Frac)
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TrabajoActivo")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
        End If

    End Sub

    ''FormClose sub, para no cerrar el programa con el boton de cerrar fracturador
    Private Sub Formclose(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
    End Sub
End Class