﻿Public Class Secundaria_1920x1080

    ''Definicion local de los 4 posibles contenedores
    Private Frac_1 As Object
    Private Frac_2 As Object
    Private Frac_4 As Object
    Private Frac_8 As Object

    Private Sub Secundaria_1920x1080_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Pasamos todos los contenedores a Frac
        Frac_1 = Container1_2
        Frac_2 = Container2_2
        Frac_4 = Container4_2
        Frac_8 = Container8_2

        ''Centramos el titulo
        CenterObject(Title)
        CenterObject(TOPPanel)
        CenterObject(LblTOp1)

        ''Giramos el logo de QM
        Dim bmp As New Bitmap(QMLogo.Image)
        bmp.RotateFlip(RotateFlipType.Rotate270FlipNone)
        QMLogo.Image = bmp

        ''Agregamos todos los controles al panel principal
        AddControl(Frac_1, MainPanel)
        AddControl(Frac_2, MainPanel)
        AddControl(Frac_4, MainPanel)
        AddControl(Frac_8, MainPanel)

        ''Iniciamos el control segun la configuracion actual
        Select Case My.Settings.currentScreens_2
            Case 1
                Frac_1.Show()
            Case 2
                Frac_2.Show()
            Case 4
                Frac_4.Show()
            Case 8
                Frac_8.Show()
        End Select

        ''Agregamos handler cuando se inicia una operacion
        AddHandler IFracSystem.OperationStart, AddressOf OpStart

    End Sub

    ''Si clickea en el panel/titulo dockeamos el form
    Private Sub MaximizePrincipal(sender As Object, e As EventArgs) Handles TitlePanel.MouseDoubleClick, Title.MouseDoubleClick
        Dock = DockStyle.Fill
        CenterToScreen()
    End Sub

    ''Si cierran esta pestaña primero desconectamos todos los fracturadores
    Private Sub AfterClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IFracSystem.ActiveOP = False Then
            ISystem.DisconnectAll()
            Inicio.Close()
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TrabajoActivo")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
            e.Cancel = True
        End If
    End Sub

    ''Boton para seleccionar cantidad de pantallas
    Private Sub BtnSwitch_Click(sender As Object, e As EventArgs) Handles BtnSwitch.Click
        ScreenSelect.ScreenChange_Sender = 2
        ScreenSelect.ShowDialog(Me)
    End Sub

    ''Logo de QM con el link
    Private Sub QMLogo_Click(sender As Object, e As EventArgs) Handles QMLogo.Click
        QMInfo.ShowDialog(Me)
    End Sub

    ''Sub para mostrar las pantallas necesarias
    Public Sub ContainerShow()
        Select Case My.Settings.currentScreens_2
            Case 1
                Frac_1.Show()
            Case 2
                Frac_2.Show()
            Case 4
                Frac_4.Show()
            Case 8
                Frac_8.Show()
        End Select
    End Sub

    ''Timer de operacion
    Private Sub OpStart()
        If IFracSystem.ActiveOP = True Then
            TmrOp.Start()
        Else
            TmrOp.Stop()
        End If
    End Sub

    ''Mostramos el tiempo de operacion
    Private Sub TmrOp_Tick(sender As Object, e As EventArgs) Handles TmrOp.Tick
        LblTOp1.Text = TiempoOperacion
    End Sub

    ''Sub para hacer Stop a todos los frac
    Private Sub BtnStopAll_Click(sender As Object, e As EventArgs) Handles BtnStopAll.Click
        avisoStr = "¿Esta seguro de apagar todos los equipos?"
        avisoTitulo = "Aviso"
        If AvisoConfirmar.ShowDialog(Me) = DialogResult.OK Then
            Dim StopAllTask As New Task(AddressOf StopAll)
            StopAllTask.Start()
        End If
    End Sub

    ''IDLE ALL
    Private Sub BtnIdleALL_Click(sender As Object, e As EventArgs) Handles BtnIdleALL.Click
        Dim IdleAllTask As New Task(AddressOf IdleALL)
        IdleAllTask.Start()
    End Sub
End Class