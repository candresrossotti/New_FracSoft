﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principal_1920x1080
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principal_1920x1080))
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2ControlBox2 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.SidePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnSwitch = New Guna.UI2.WinForms.Guna2Button()
        Me.InfoButton = New Guna.UI2.WinForms.Guna2Button()
        Me.QMLogo = New System.Windows.Forms.PictureBox()
        Me.BtnAjustes = New Guna.UI2.WinForms.Guna2Button()
        Me.MainPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblPresionMaximaActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblPresionMaxima = New System.Windows.Forms.Label()
        Me.BtnAutomation = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnIdleALL = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel6 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.LblCombustibleTotalActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel6 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCombustibleTotal = New System.Windows.Forms.Label()
        Me.Guna2Panel8 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.LblCaudalTotalActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel8 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCaudalTotal = New System.Windows.Forms.Label()
        Me.Guna2Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.LblTotalizadorActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel3 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTotalizador = New System.Windows.Forms.Label()
        Me.TmrTotal = New System.Windows.Forms.Timer(Me.components)
        Me.TmrOp = New System.Windows.Forms.Timer(Me.components)
        Me.TitlePanel.SuspendLayout()
        Me.SidePanel.SuspendLayout()
        CType(Me.QMLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainPanel.SuspendLayout()
        Me.Guna2Panel2.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        Me.Guna2Panel6.SuspendLayout()
        Me.Guna2CustomGradientPanel6.SuspendLayout()
        Me.Guna2Panel8.SuspendLayout()
        Me.Guna2CustomGradientPanel8.SuspendLayout()
        Me.Guna2Panel4.SuspendLayout()
        Me.Guna2CustomGradientPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(877, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(167, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Fracturadores"
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox2)
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(1920, 38)
        Me.TitlePanel.TabIndex = 1
        '
        'Guna2ControlBox2
        '
        Me.Guna2ControlBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox
        Me.Guna2ControlBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox2.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox2.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox2.Location = New System.Drawing.Point(1830, 3)
        Me.Guna2ControlBox2.Name = "Guna2ControlBox2"
        Me.Guna2ControlBox2.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox2.TabIndex = 3
        Me.Guna2ControlBox2.UseTransparentBackground = True
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(1872, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'SidePanel
        '
        Me.SidePanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SidePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.SidePanel.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanel.BorderThickness = 1
        Me.SidePanel.Controls.Add(Me.BtnSwitch)
        Me.SidePanel.Controls.Add(Me.InfoButton)
        Me.SidePanel.Controls.Add(Me.QMLogo)
        Me.SidePanel.Controls.Add(Me.BtnAjustes)
        Me.SidePanel.Location = New System.Drawing.Point(0, 38)
        Me.SidePanel.Name = "SidePanel"
        Me.SidePanel.Size = New System.Drawing.Size(45, 1002)
        Me.SidePanel.TabIndex = 2
        '
        'BtnSwitch
        '
        Me.BtnSwitch.BackColor = System.Drawing.Color.Transparent
        Me.BtnSwitch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSwitch.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnSwitch.FillColor = System.Drawing.Color.Transparent
        Me.BtnSwitch.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnSwitch.ForeColor = System.Drawing.Color.White
        Me.BtnSwitch.Image = Global.sFRAC.My.Resources.Resources.swap
        Me.BtnSwitch.ImageSize = New System.Drawing.Size(39, 39)
        Me.BtnSwitch.Location = New System.Drawing.Point(0, 45)
        Me.BtnSwitch.Name = "BtnSwitch"
        Me.BtnSwitch.Size = New System.Drawing.Size(45, 45)
        Me.BtnSwitch.TabIndex = 3
        Me.BtnSwitch.UseTransparentBackground = True
        '
        'InfoButton
        '
        Me.InfoButton.BackColor = System.Drawing.Color.Transparent
        Me.InfoButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.InfoButton.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.InfoButton.FillColor = System.Drawing.Color.Transparent
        Me.InfoButton.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.InfoButton.ForeColor = System.Drawing.Color.White
        Me.InfoButton.Image = Global.sFRAC.My.Resources.Resources.info
        Me.InfoButton.ImageSize = New System.Drawing.Size(25, 25)
        Me.InfoButton.Location = New System.Drawing.Point(0, 135)
        Me.InfoButton.Name = "InfoButton"
        Me.InfoButton.Size = New System.Drawing.Size(45, 45)
        Me.InfoButton.TabIndex = 2
        Me.InfoButton.UseTransparentBackground = True
        '
        'QMLogo
        '
        Me.QMLogo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.QMLogo.Image = CType(resources.GetObject("QMLogo.Image"), System.Drawing.Image)
        Me.QMLogo.Location = New System.Drawing.Point(-29, 929)
        Me.QMLogo.Name = "QMLogo"
        Me.QMLogo.Size = New System.Drawing.Size(102, 73)
        Me.QMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.QMLogo.TabIndex = 0
        Me.QMLogo.TabStop = False
        '
        'BtnAjustes
        '
        Me.BtnAjustes.BackColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAjustes.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAjustes.ForeColor = System.Drawing.Color.White
        Me.BtnAjustes.Image = CType(resources.GetObject("BtnAjustes.Image"), System.Drawing.Image)
        Me.BtnAjustes.ImageSize = New System.Drawing.Size(25, 25)
        Me.BtnAjustes.Location = New System.Drawing.Point(0, 90)
        Me.BtnAjustes.Name = "BtnAjustes"
        Me.BtnAjustes.Size = New System.Drawing.Size(45, 45)
        Me.BtnAjustes.TabIndex = 1
        Me.BtnAjustes.UseTransparentBackground = True
        '
        'MainPanel
        '
        Me.MainPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MainPanel.BackColor = System.Drawing.Color.Transparent
        Me.MainPanel.Controls.Add(Me.Guna2Panel2)
        Me.MainPanel.Controls.Add(Me.BtnAutomation)
        Me.MainPanel.Controls.Add(Me.BtnIdleALL)
        Me.MainPanel.Controls.Add(Me.Guna2Panel6)
        Me.MainPanel.Controls.Add(Me.Guna2Panel8)
        Me.MainPanel.Controls.Add(Me.Guna2Panel4)
        Me.MainPanel.Location = New System.Drawing.Point(45, 38)
        Me.MainPanel.Name = "MainPanel"
        Me.MainPanel.Size = New System.Drawing.Size(1875, 1002)
        Me.MainPanel.TabIndex = 3
        '
        'Guna2Panel2
        '
        Me.Guna2Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.BorderRadius = 6
        Me.Guna2Panel2.BorderThickness = 1
        Me.Guna2Panel2.Controls.Add(Me.Label1)
        Me.Guna2Panel2.Controls.Add(Me.LblPresionMaximaActual)
        Me.Guna2Panel2.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.Guna2Panel2.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.Location = New System.Drawing.Point(1159, 920)
        Me.Guna2Panel2.Name = "Guna2Panel2"
        Me.Guna2Panel2.Size = New System.Drawing.Size(346, 70)
        Me.Guna2Panel2.TabIndex = 81
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(235, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 20)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "PSI"
        '
        'LblPresionMaximaActual
        '
        Me.LblPresionMaximaActual.AutoSize = True
        Me.LblPresionMaximaActual.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblPresionMaximaActual.ForeColor = System.Drawing.Color.White
        Me.LblPresionMaximaActual.Location = New System.Drawing.Point(156, 29)
        Me.LblPresionMaximaActual.Name = "LblPresionMaximaActual"
        Me.LblPresionMaximaActual.Size = New System.Drawing.Size(35, 37)
        Me.LblPresionMaximaActual.TabIndex = 22
        Me.LblPresionMaximaActual.Text = "0"
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblPresionMaxima)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(346, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblPresionMaxima
        '
        Me.LblPresionMaxima.AutoSize = True
        Me.LblPresionMaxima.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPresionMaxima.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblPresionMaxima.Location = New System.Drawing.Point(115, 4)
        Me.LblPresionMaxima.Name = "LblPresionMaxima"
        Me.LblPresionMaxima.Size = New System.Drawing.Size(121, 20)
        Me.LblPresionMaxima.TabIndex = 19
        Me.LblPresionMaxima.Text = "Presión Máxima"
        '
        'BtnAutomation
        '
        Me.BtnAutomation.Animated = True
        Me.BtnAutomation.BorderColor = System.Drawing.Color.White
        Me.BtnAutomation.BorderRadius = 5
        Me.BtnAutomation.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAutomation.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAutomation.FillColor = System.Drawing.Color.Transparent
        Me.BtnAutomation.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.BtnAutomation.ForeColor = System.Drawing.Color.White
        Me.BtnAutomation.Image = Global.sFRAC.My.Resources.Resources.automation2
        Me.BtnAutomation.ImageSize = New System.Drawing.Size(60, 60)
        Me.BtnAutomation.Location = New System.Drawing.Point(6, 920)
        Me.BtnAutomation.Name = "BtnAutomation"
        Me.BtnAutomation.Size = New System.Drawing.Size(93, 70)
        Me.BtnAutomation.TabIndex = 83
        Me.BtnAutomation.Visible = False
        '
        'BtnIdleALL
        '
        Me.BtnIdleALL.Animated = True
        Me.BtnIdleALL.BorderColor = System.Drawing.Color.White
        Me.BtnIdleALL.BorderRadius = 5
        Me.BtnIdleALL.BorderThickness = 1
        Me.BtnIdleALL.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdleALL.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdleALL.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdleALL.Font = New System.Drawing.Font("Montserrat", 24.0!)
        Me.BtnIdleALL.ForeColor = System.Drawing.Color.White
        Me.BtnIdleALL.Location = New System.Drawing.Point(1511, 920)
        Me.BtnIdleALL.Name = "BtnIdleALL"
        Me.BtnIdleALL.Size = New System.Drawing.Size(346, 70)
        Me.BtnIdleALL.TabIndex = 82
        Me.BtnIdleALL.Text = "INSTANT NEUTRAL"
        Me.BtnIdleALL.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        '
        'Guna2Panel6
        '
        Me.Guna2Panel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel6.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel6.BorderRadius = 6
        Me.Guna2Panel6.BorderThickness = 1
        Me.Guna2Panel6.Controls.Add(Me.Label51)
        Me.Guna2Panel6.Controls.Add(Me.LblCombustibleTotalActual)
        Me.Guna2Panel6.Controls.Add(Me.Guna2CustomGradientPanel6)
        Me.Guna2Panel6.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel6.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel6.Location = New System.Drawing.Point(455, 920)
        Me.Guna2Panel6.Name = "Guna2Panel6"
        Me.Guna2Panel6.Size = New System.Drawing.Size(346, 70)
        Me.Guna2Panel6.TabIndex = 79
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label51.ForeColor = System.Drawing.Color.White
        Me.Label51.Location = New System.Drawing.Point(235, 40)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(17, 20)
        Me.Label51.TabIndex = 23
        Me.Label51.Text = "L"
        '
        'LblCombustibleTotalActual
        '
        Me.LblCombustibleTotalActual.AutoSize = True
        Me.LblCombustibleTotalActual.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblCombustibleTotalActual.ForeColor = System.Drawing.Color.White
        Me.LblCombustibleTotalActual.Location = New System.Drawing.Point(156, 29)
        Me.LblCombustibleTotalActual.Name = "LblCombustibleTotalActual"
        Me.LblCombustibleTotalActual.Size = New System.Drawing.Size(35, 37)
        Me.LblCombustibleTotalActual.TabIndex = 22
        Me.LblCombustibleTotalActual.Text = "0"
        '
        'Guna2CustomGradientPanel6
        '
        Me.Guna2CustomGradientPanel6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.BorderRadius = 6
        Me.Guna2CustomGradientPanel6.Controls.Add(Me.LblCombustibleTotal)
        Me.Guna2CustomGradientPanel6.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel6.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel6.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel6.Name = "Guna2CustomGradientPanel6"
        Me.Guna2CustomGradientPanel6.Size = New System.Drawing.Size(346, 26)
        Me.Guna2CustomGradientPanel6.TabIndex = 19
        '
        'LblCombustibleTotal
        '
        Me.LblCombustibleTotal.AutoSize = True
        Me.LblCombustibleTotal.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCombustibleTotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCombustibleTotal.Location = New System.Drawing.Point(114, 4)
        Me.LblCombustibleTotal.Name = "LblCombustibleTotal"
        Me.LblCombustibleTotal.Size = New System.Drawing.Size(137, 20)
        Me.LblCombustibleTotal.TabIndex = 19
        Me.LblCombustibleTotal.Text = "Combustible Total"
        '
        'Guna2Panel8
        '
        Me.Guna2Panel8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel8.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel8.BorderRadius = 6
        Me.Guna2Panel8.BorderThickness = 1
        Me.Guna2Panel8.Controls.Add(Me.Label73)
        Me.Guna2Panel8.Controls.Add(Me.LblCaudalTotalActual)
        Me.Guna2Panel8.Controls.Add(Me.Guna2CustomGradientPanel8)
        Me.Guna2Panel8.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel8.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel8.Location = New System.Drawing.Point(807, 920)
        Me.Guna2Panel8.Name = "Guna2Panel8"
        Me.Guna2Panel8.Size = New System.Drawing.Size(346, 70)
        Me.Guna2Panel8.TabIndex = 80
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label73.ForeColor = System.Drawing.Color.White
        Me.Label73.Location = New System.Drawing.Point(235, 40)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(70, 20)
        Me.Label73.TabIndex = 23
        Me.Label73.Text = "BBL/Min"
        '
        'LblCaudalTotalActual
        '
        Me.LblCaudalTotalActual.AutoSize = True
        Me.LblCaudalTotalActual.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblCaudalTotalActual.ForeColor = System.Drawing.Color.White
        Me.LblCaudalTotalActual.Location = New System.Drawing.Point(156, 29)
        Me.LblCaudalTotalActual.Name = "LblCaudalTotalActual"
        Me.LblCaudalTotalActual.Size = New System.Drawing.Size(35, 37)
        Me.LblCaudalTotalActual.TabIndex = 22
        Me.LblCaudalTotalActual.Text = "0"
        '
        'Guna2CustomGradientPanel8
        '
        Me.Guna2CustomGradientPanel8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.BorderRadius = 6
        Me.Guna2CustomGradientPanel8.Controls.Add(Me.LblCaudalTotal)
        Me.Guna2CustomGradientPanel8.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel8.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel8.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel8.Name = "Guna2CustomGradientPanel8"
        Me.Guna2CustomGradientPanel8.Size = New System.Drawing.Size(346, 26)
        Me.Guna2CustomGradientPanel8.TabIndex = 19
        '
        'LblCaudalTotal
        '
        Me.LblCaudalTotal.AutoSize = True
        Me.LblCaudalTotal.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCaudalTotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCaudalTotal.Location = New System.Drawing.Point(115, 4)
        Me.LblCaudalTotal.Name = "LblCaudalTotal"
        Me.LblCaudalTotal.Size = New System.Drawing.Size(95, 20)
        Me.LblCaudalTotal.TabIndex = 19
        Me.LblCaudalTotal.Text = "Caudal Total"
        '
        'Guna2Panel4
        '
        Me.Guna2Panel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.BorderRadius = 6
        Me.Guna2Panel4.BorderThickness = 1
        Me.Guna2Panel4.Controls.Add(Me.Label10)
        Me.Guna2Panel4.Controls.Add(Me.LblTotalizadorActual)
        Me.Guna2Panel4.Controls.Add(Me.Guna2CustomGradientPanel3)
        Me.Guna2Panel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.Location = New System.Drawing.Point(103, 920)
        Me.Guna2Panel4.Name = "Guna2Panel4"
        Me.Guna2Panel4.Size = New System.Drawing.Size(346, 70)
        Me.Guna2Panel4.TabIndex = 78
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(235, 40)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(39, 20)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "BBL"
        '
        'LblTotalizadorActual
        '
        Me.LblTotalizadorActual.AutoSize = True
        Me.LblTotalizadorActual.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblTotalizadorActual.ForeColor = System.Drawing.Color.White
        Me.LblTotalizadorActual.Location = New System.Drawing.Point(156, 29)
        Me.LblTotalizadorActual.Name = "LblTotalizadorActual"
        Me.LblTotalizadorActual.Size = New System.Drawing.Size(35, 37)
        Me.LblTotalizadorActual.TabIndex = 22
        Me.LblTotalizadorActual.Text = "0"
        '
        'Guna2CustomGradientPanel3
        '
        Me.Guna2CustomGradientPanel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.BorderRadius = 6
        Me.Guna2CustomGradientPanel3.Controls.Add(Me.LblTotalizador)
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel3.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel3.Name = "Guna2CustomGradientPanel3"
        Me.Guna2CustomGradientPanel3.Size = New System.Drawing.Size(346, 26)
        Me.Guna2CustomGradientPanel3.TabIndex = 19
        '
        'LblTotalizador
        '
        Me.LblTotalizador.AutoSize = True
        Me.LblTotalizador.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTotalizador.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTotalizador.Location = New System.Drawing.Point(124, 4)
        Me.LblTotalizador.Name = "LblTotalizador"
        Me.LblTotalizador.Size = New System.Drawing.Size(86, 20)
        Me.LblTotalizador.TabIndex = 19
        Me.LblTotalizador.Text = "Totalizador"
        '
        'TmrTotal
        '
        Me.TmrTotal.Interval = 1000
        '
        'TmrOp
        '
        Me.TmrOp.Interval = 1000
        '
        'Principal_1920x1080
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1920, 1040)
        Me.Controls.Add(Me.MainPanel)
        Me.Controls.Add(Me.SidePanel)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Principal_1920x1080"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Fracturadores"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.SidePanel.ResumeLayout(False)
        CType(Me.QMLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainPanel.ResumeLayout(False)
        Me.Guna2Panel2.ResumeLayout(False)
        Me.Guna2Panel2.PerformLayout()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        Me.Guna2Panel6.ResumeLayout(False)
        Me.Guna2Panel6.PerformLayout()
        Me.Guna2CustomGradientPanel6.ResumeLayout(False)
        Me.Guna2CustomGradientPanel6.PerformLayout()
        Me.Guna2Panel8.ResumeLayout(False)
        Me.Guna2Panel8.PerformLayout()
        Me.Guna2CustomGradientPanel8.ResumeLayout(False)
        Me.Guna2CustomGradientPanel8.PerformLayout()
        Me.Guna2Panel4.ResumeLayout(False)
        Me.Guna2Panel4.PerformLayout()
        Me.Guna2CustomGradientPanel3.ResumeLayout(False)
        Me.Guna2CustomGradientPanel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents SidePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents QMLogo As PictureBox
    Friend WithEvents BtnAjustes As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents MainPanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox2 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Guna2Panel2 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label1 As Label
    Public WithEvents LblPresionMaximaActual As Label
    Public WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblPresionMaxima As Label
    Public WithEvents BtnAutomation As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnIdleALL As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel6 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label51 As Label
    Public WithEvents LblCombustibleTotalActual As Label
    Public WithEvents Guna2CustomGradientPanel6 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblCombustibleTotal As Label
    Public WithEvents Guna2Panel8 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label73 As Label
    Public WithEvents LblCaudalTotalActual As Label
    Public WithEvents Guna2CustomGradientPanel8 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblCaudalTotal As Label
    Public WithEvents Guna2Panel4 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label10 As Label
    Public WithEvents LblTotalizadorActual As Label
    Public WithEvents Guna2CustomGradientPanel3 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblTotalizador As Label
    Friend WithEvents TmrTotal As Timer
    Friend WithEvents TmrOp As Timer
    Friend WithEvents InfoButton As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnSwitch As Guna.UI2.WinForms.Guna2Button
End Class
