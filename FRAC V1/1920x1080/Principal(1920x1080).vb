﻿Public Class Principal_1920x1080

    ''Definicion local de los 4 posibles contenedores
    Private Frac_1 As Object
    Private Frac_2 As Object
    Private Frac_4 As Object
    Private Frac_8 As Object

    ''CSV de operacion general
    Private CSV_OPERATION As CSV

    Private Sub Principal_1920x1080_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Pasamos todos los contenedores a Frac
        Frac_1 = Container1_1
        Frac_2 = Container2_1
        Frac_4 = Container4_1
        Frac_8 = Container8_1

        ''Centramos todo
        CenterEverything()

        ''Iniciamos TmrTotal
        TmrTotal.Start()

        ''Giramos el logo de QM
        Dim bmp As New Bitmap(QMLogo.Image)
        bmp.RotateFlip(RotateFlipType.Rotate270FlipNone)
        QMLogo.Image = bmp


        ''Agregamos todos los controles al panel principal
        AddControl(Frac_1, MainPanel)
        AddControl(Frac_2, MainPanel)
        AddControl(Frac_4, MainPanel)
        AddControl(Frac_8, MainPanel)

        ''Iniciamos el control segun la configuracion actual
        Select Case My.Settings.Item("currentScreens")
            Case 1
                Frac_1.Show()
            Case 2
                Frac_2.Show()
            Case 4
                Frac_4.Show()
            Case 8
                Frac_8.Show()
        End Select

        ''Agregamos el control de fractura automatica para desabilitar boton
        AddHandler FracAutomation.AutomationStart, AddressOf PbHandleInvoke
        AddHandler IFracSystem.OperationStart, AddressOf GeneralCSVStart
    End Sub

    ''Sub para centrar todo
    Private Sub CenterEverything()
        CenterObject(Title)
        CenterObject(LblTotalizador)
        CenterObject(LblCombustibleTotal)
        CenterObject(LblCaudalTotal)
        CenterObject(LblPresionMaxima)
    End Sub

    ''Sub para cambiar el tipo de contenedor
    Private Sub BtnSwitch_Click(sender As Object, e As EventArgs) Handles BtnSwitch.Click
        ScreenSelect.ScreenChange_Sender = 1
        ScreenSelect.ShowDialog(Me)
    End Sub

    ''Sub que se ejecuta cuando cambian el contenedor
    Public Sub ContainerShow()
        Select Case My.Settings.Item("currentScreens")
            Case 1
                Frac_1.Show()
            Case 2
                Frac_2.Show()
            Case 4
                Frac_4.Show()
            Case 8
                Frac_8.Show()
        End Select
        BringToFront()
    End Sub

    ''Sub para abrir pantalla de ajustes generales
    Private Sub BtnAjustes_Click(sender As Object, e As EventArgs) Handles BtnAjustes.Click
        Ajustes.Show(Me)
    End Sub

    ''Si cierran esta pestaña primero desconectamos todos los fracturadores
    Private Sub AfterClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IFracSystem.ActiveOP = False Then
            ISystem.DisconnectAll()
            Inicio.Close()
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TrabajoActivo")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
            e.Cancel = True
        End If
    End Sub

    ''Informacion de la APP
    Private Sub QMLogo_Click(sender As Object, e As EventArgs) Handles QMLogo.Click
        QMInfo.ShowDialog(Me)
    End Sub

    ''Si clickea en el panel/titulo dockeamos el form
    Private Sub MaximizePrincipal(sender As Object, e As EventArgs) Handles TitlePanel.MouseDoubleClick, Title.MouseDoubleClick
        Dock = DockStyle.Fill
        CenterToScreen()
    End Sub

    ''Region para calculo de valores totales
#Region "TmrTotal"
    Delegate Sub GetTotalDelegate()
    Private Sub TmrTotal_Tick(sender As Object, e As EventArgs) Handles TmrTotal.Tick
        Dim TotalTask As New Task(AddressOf GetTotalInvoke)
        TotalTask.Start()
    End Sub

    ''Invoke para todos los textos
    Private Sub GetTotalInvoke()
        LblPresionMaximaActual.Invoke(New GetTotalDelegate(AddressOf PresionMaxInv))
        LblCaudalTotalActual.Invoke(New GetTotalDelegate(AddressOf CaudalTotInv))
        LblCombustibleTotalActual.Invoke(New GetTotalDelegate(AddressOf CombTotInv))
        LblTotalizadorActual.Invoke(New GetTotalDelegate(AddressOf TotalizadorInv))
    End Sub

    ''Sub del invoke PresionMax
    Private Sub PresionMaxInv()
        LblPresionMaximaActual.Text = PresionMaxima.ToString("F1")
        CenterObject(LblPresionMaximaActual)
    End Sub

    ''Sub del invoke CaudalTot
    Private Sub CaudalTotInv()
        LblCaudalTotalActual.Text = Caudal.ToString("F1")
        CenterObject(LblCaudalTotalActual)
    End Sub

    ''Sub del invoke Combustible
    Private Sub CombTotInv()
        LblCombustibleTotalActual.Text = Math.Round(CombustibleTotal, 1)
        CenterObject(LblCombustibleTotalActual)
    End Sub

    ''Sub del invoke Totalizador
    Private Sub TotalizadorInv()
        LblTotalizadorActual.Text = Math.Round(Totalizador, 1)
        CenterObject(LblTotalizadorActual)
    End Sub
#End Region

    ''Region para desactivar boton si estamos en modo automatico
#Region "Automatic Handle"

    ''Sub para pantalla de automatizacion de fracturador
    Private Sub BtnAutomation_Click(sender As Object, e As EventArgs) Handles BtnAutomation.Click
        If NFracLoad.ShowDialog(Me) = DialogResult.OK Then
            FullAutomation.ShowDialog(Me)
        End If
    End Sub

    Delegate Sub PbHandleDelegate()
    Private Sub PbHandleInvoke()
        BtnAutomation.Invoke(New PbHandleDelegate(AddressOf PbHandle))
    End Sub

    Private Sub PbHandle()
        BtnAutomation.Enabled = Not IFracSystem.ActiveOP
    End Sub

    ''Timer para escribir el CSV general
    Private Sub TmrOp_Tick(sender As Object, e As EventArgs) Handles TmrOp.Tick
        Dim CSVThread As New Task(Sub() WriteGeneralCSVRecord(CSV_OPERATION))
        CSVThread.Start()
    End Sub

    ''Sub de inicio de csv
    Private Sub GeneralCSVStart()
        If IFracSystem.ActiveOP = True Then
            GeneralCSV_OPERATION_INIT(CSV_OPERATION)
            TmrOp.Start()
        Else
            TmrOp.Stop()
            If CSV_OPERATION IsNot Nothing Then
                CSV_OPERATION.CloseCSV()
                If GeneratePDF Then
                    Dim PDFHandleTask As New Task(AddressOf PDFHandle)
                    PDFHandleTask.Start()
                End If
            End If
        End If
    End Sub

    ''Sub para hacer el pdf
    Private Async Sub PDFHandle()
        Try


            Dim NewPDF As New PDF

            ''Agregamos los nombres de los equipos utilizados
            Dim FracNames As New List(Of String)
            For i = 0 To ConnectedFracsID.Count - 1
                FracNames.Add(ISystem.GetFracName(ConnectedFracsID(i)))
            Next
            PDF.Equipo = FracNames.ToArray()

            Dim FracCaudal As New List(Of Double)
            For i = 0 To ConnectedFracsID.Count - 1
                FracCaudal.Add(FracData.DblVFrac("Caudal_" + ConnectedFracsID(i).ToString()))
            Next

            Dim FracBarriles As New List(Of Double)
            For i = 0 To ConnectedFracsID.Count - 1
                FracBarriles.Add(FracData.DblVFrac("BarrilesTotales_" + ConnectedFracsID(i).ToString()))
            Next

            PDF.CSVPath = CSV_OPERATION.Path()

            PDF.Yacimiento = Yacimiento
            PDF.Etapa = Etapa
            PDF.Locacion = Locacion
            PDF.TipoPozo = TipoPozo
            PDF.Distrito = Distrito
            PDF.Provincia = Provincia
            PDF.Cliente = Cliente

            Await Task.Delay(50)
            NewPDF.SavePDF(PDFPath + FracturaGeneralName, OperationStartTime, Now, PresionMaxima, CombustibleTotal, Caudal, Totalizador,
                          ConnectedFracsID.Count, FracCaudal.ToArray(), FracBarriles.ToArray())

        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
#End Region

    ''Informacion de la fractura
    Private Sub InfoButton_Click(sender As Object, e As EventArgs) Handles InfoButton.Click
        FractureInfo.Show(Me)
    End Sub

    ''IDLE ALL
    Private Sub BtnIdleALL_Click(sender As Object, e As EventArgs) Handles BtnIdleALL.Click
        Dim IdleAllTask As New Task(AddressOf IdleALL)
        IdleAllTask.Start()
    End Sub
End Class