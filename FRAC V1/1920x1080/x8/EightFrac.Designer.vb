﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EightFrac_1920x1080
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EightFrac_1920x1080))
        Me.LblMarchaActual = New System.Windows.Forms.Label()
        Me.LblMarcha3 = New System.Windows.Forms.Label()
        Me.LblMarcha1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMarcha = New System.Windows.Forms.Label()
        Me.BtnErrores = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAjustes = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnStartEngine = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel10 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnFC = New Guna.UI2.WinForms.Guna2Button()
        Me.LblCaudal2 = New System.Windows.Forms.Label()
        Me.LblCaudalActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel10 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCaudal = New System.Windows.Forms.Label()
        Me.Guna2Panel9 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtnTripZero = New Guna.UI2.WinForms.Guna2Button()
        Me.LblPCorteActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel9 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblPCorte = New System.Windows.Forms.Label()
        Me.LblMarchaDeseada = New System.Windows.Forms.Label()
        Me.TmrReconnect = New System.Windows.Forms.Timer(Me.components)
        Me.TmrErrorHandle = New System.Windows.Forms.Timer(Me.components)
        Me.TmrRead = New System.Windows.Forms.Timer(Me.components)
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnParking = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnMinusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPlusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.PanelRPM = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnResetRPM = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmMinus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnIdle = New Guna.UI2.WinForms.Guna2Button()
        Me.LblRPMActual = New System.Windows.Forms.Label()
        Me.LblRPMDeseada = New System.Windows.Forms.Label()
        Me.LblRPM3 = New System.Windows.Forms.Label()
        Me.LblRPM1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblRpm = New System.Windows.Forms.Label()
        Me.TmrCheck = New System.Windows.Forms.Timer(Me.components)
        Me.TmrBackUp = New System.Windows.Forms.Timer(Me.components)
        Me.TmrOp = New System.Windows.Forms.Timer(Me.components)
        Me.BtnEKill = New Guna.UI2.WinForms.Guna2Button()
        Me.PbMoxaStatus = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.PbLockUp = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.PbAlarms = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.ToolTip1 = New Guna.UI2.WinForms.Guna2HtmlToolTip()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.Guna2Panel10.SuspendLayout()
        Me.Guna2CustomGradientPanel10.SuspendLayout()
        Me.Guna2Panel9.SuspendLayout()
        Me.Guna2CustomGradientPanel9.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.PanelRPM.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblMarchaActual
        '
        Me.LblMarchaActual.AutoSize = True
        Me.LblMarchaActual.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblMarchaActual.ForeColor = System.Drawing.Color.White
        Me.LblMarchaActual.Location = New System.Drawing.Point(76, 117)
        Me.LblMarchaActual.Name = "LblMarchaActual"
        Me.LblMarchaActual.Size = New System.Drawing.Size(19, 25)
        Me.LblMarchaActual.TabIndex = 23
        Me.LblMarchaActual.Text = "-"
        '
        'LblMarcha3
        '
        Me.LblMarcha3.AutoSize = True
        Me.LblMarcha3.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMarcha3.ForeColor = System.Drawing.Color.White
        Me.LblMarcha3.Location = New System.Drawing.Point(25, 90)
        Me.LblMarcha3.Name = "LblMarcha3"
        Me.LblMarcha3.Size = New System.Drawing.Size(118, 21)
        Me.LblMarcha3.TabIndex = 21
        Me.LblMarcha3.Text = "Marcha Actual"
        '
        'LblMarcha1
        '
        Me.LblMarcha1.AutoSize = True
        Me.LblMarcha1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha1.ForeColor = System.Drawing.Color.White
        Me.LblMarcha1.Location = New System.Drawing.Point(25, 33)
        Me.LblMarcha1.Name = "LblMarcha1"
        Me.LblMarcha1.Size = New System.Drawing.Size(125, 20)
        Me.LblMarcha1.TabIndex = 20
        Me.LblMarcha1.Text = "Marcha Deseada"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblMarcha)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(200, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblMarcha
        '
        Me.LblMarcha.AutoSize = True
        Me.LblMarcha.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMarcha.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMarcha.Location = New System.Drawing.Point(67, 3)
        Me.LblMarcha.Name = "LblMarcha"
        Me.LblMarcha.Size = New System.Drawing.Size(66, 21)
        Me.LblMarcha.TabIndex = 19
        Me.LblMarcha.Text = "Marcha"
        '
        'BtnErrores
        '
        Me.BtnErrores.Animated = True
        Me.BtnErrores.BorderColor = System.Drawing.Color.White
        Me.BtnErrores.BorderRadius = 5
        Me.BtnErrores.BorderThickness = 1
        Me.BtnErrores.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnErrores.FillColor = System.Drawing.Color.Transparent
        Me.BtnErrores.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnErrores.ForeColor = System.Drawing.Color.White
        Me.BtnErrores.Location = New System.Drawing.Point(52, 7)
        Me.BtnErrores.Name = "BtnErrores"
        Me.BtnErrores.Size = New System.Drawing.Size(120, 25)
        Me.BtnErrores.TabIndex = 100
        Me.BtnErrores.Text = "Errores"
        '
        'BtnAjustes
        '
        Me.BtnAjustes.Animated = True
        Me.BtnAjustes.BorderColor = System.Drawing.Color.White
        Me.BtnAjustes.BorderRadius = 5
        Me.BtnAjustes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAjustes.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAjustes.ForeColor = System.Drawing.Color.White
        Me.BtnAjustes.Image = Global.sFRAC.My.Resources.Resources.settings_1
        Me.BtnAjustes.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnAjustes.ImageSize = New System.Drawing.Size(59, 59)
        Me.BtnAjustes.Location = New System.Drawing.Point(3, 39)
        Me.BtnAjustes.Name = "BtnAjustes"
        Me.BtnAjustes.Size = New System.Drawing.Size(60, 60)
        Me.BtnAjustes.TabIndex = 99
        '
        'BtnStartEngine
        '
        Me.BtnStartEngine.Animated = True
        Me.BtnStartEngine.BorderColor = System.Drawing.Color.White
        Me.BtnStartEngine.BorderRadius = 5
        Me.BtnStartEngine.CheckedState.Image = Global.sFRAC.My.Resources.Resources.Engine_STOP
        Me.BtnStartEngine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStartEngine.FillColor = System.Drawing.Color.Transparent
        Me.BtnStartEngine.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnStartEngine.ForeColor = System.Drawing.Color.White
        Me.BtnStartEngine.Image = Global.sFRAC.My.Resources.Resources.Engine_START
        Me.BtnStartEngine.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnStartEngine.ImageSize = New System.Drawing.Size(59, 59)
        Me.BtnStartEngine.Location = New System.Drawing.Point(161, 39)
        Me.BtnStartEngine.Name = "BtnStartEngine"
        Me.BtnStartEngine.Size = New System.Drawing.Size(60, 60)
        Me.BtnStartEngine.TabIndex = 98
        '
        'Guna2Panel10
        '
        Me.Guna2Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.BorderRadius = 6
        Me.Guna2Panel10.BorderThickness = 1
        Me.Guna2Panel10.Controls.Add(Me.BtnFC)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudal2)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudalActual)
        Me.Guna2Panel10.Controls.Add(Me.Guna2CustomGradientPanel10)
        Me.Guna2Panel10.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.Location = New System.Drawing.Point(12, 577)
        Me.Guna2Panel10.Name = "Guna2Panel10"
        Me.Guna2Panel10.Size = New System.Drawing.Size(200, 122)
        Me.Guna2Panel10.TabIndex = 97
        '
        'BtnFC
        '
        Me.BtnFC.Animated = True
        Me.BtnFC.BorderColor = System.Drawing.Color.White
        Me.BtnFC.BorderRadius = 5
        Me.BtnFC.BorderThickness = 1
        Me.BtnFC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFC.FillColor = System.Drawing.Color.Transparent
        Me.BtnFC.Font = New System.Drawing.Font("Montserrat", 8.25!)
        Me.BtnFC.ForeColor = System.Drawing.Color.White
        Me.BtnFC.Location = New System.Drawing.Point(78, 75)
        Me.BtnFC.Name = "BtnFC"
        Me.BtnFC.Size = New System.Drawing.Size(45, 40)
        Me.BtnFC.TabIndex = 95
        Me.BtnFC.Text = "FC"
        Me.BtnFC.Visible = False
        '
        'LblCaudal2
        '
        Me.LblCaudal2.AutoSize = True
        Me.LblCaudal2.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblCaudal2.ForeColor = System.Drawing.Color.White
        Me.LblCaudal2.Location = New System.Drawing.Point(144, 28)
        Me.LblCaudal2.Name = "LblCaudal2"
        Me.LblCaudal2.Size = New System.Drawing.Size(53, 44)
        Me.LblCaudal2.TabIndex = 23
        Me.LblCaudal2.Text = "[BBL/" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Min]"
        '
        'LblCaudalActual
        '
        Me.LblCaudalActual.AutoSize = True
        Me.LblCaudalActual.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.LblCaudalActual.ForeColor = System.Drawing.Color.White
        Me.LblCaudalActual.Location = New System.Drawing.Point(65, 35)
        Me.LblCaudalActual.Name = "LblCaudalActual"
        Me.LblCaudalActual.Size = New System.Drawing.Size(71, 30)
        Me.LblCaudalActual.TabIndex = 22
        Me.LblCaudalActual.Text = "10.00"
        '
        'Guna2CustomGradientPanel10
        '
        Me.Guna2CustomGradientPanel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.BorderRadius = 6
        Me.Guna2CustomGradientPanel10.Controls.Add(Me.LblCaudal)
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel10.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel10.Name = "Guna2CustomGradientPanel10"
        Me.Guna2CustomGradientPanel10.Size = New System.Drawing.Size(200, 26)
        Me.Guna2CustomGradientPanel10.TabIndex = 19
        '
        'LblCaudal
        '
        Me.LblCaudal.AutoSize = True
        Me.LblCaudal.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblCaudal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCaudal.Location = New System.Drawing.Point(69, 3)
        Me.LblCaudal.Name = "LblCaudal"
        Me.LblCaudal.Size = New System.Drawing.Size(63, 21)
        Me.LblCaudal.TabIndex = 19
        Me.LblCaudal.Text = "Caudal"
        '
        'Guna2Panel9
        '
        Me.Guna2Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.BorderRadius = 6
        Me.Guna2Panel9.BorderThickness = 1
        Me.Guna2Panel9.Controls.Add(Me.Label2)
        Me.Guna2Panel9.Controls.Add(Me.BtnTripZero)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorteActual)
        Me.Guna2Panel9.Controls.Add(Me.Guna2CustomGradientPanel9)
        Me.Guna2Panel9.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.Location = New System.Drawing.Point(12, 462)
        Me.Guna2Panel9.Name = "Guna2Panel9"
        Me.Guna2Panel9.Size = New System.Drawing.Size(200, 109)
        Me.Guna2Panel9.TabIndex = 96
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(152, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 21)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "[PSI]"
        '
        'BtnTripZero
        '
        Me.BtnTripZero.Animated = True
        Me.BtnTripZero.BorderColor = System.Drawing.Color.White
        Me.BtnTripZero.BorderRadius = 5
        Me.BtnTripZero.BorderThickness = 1
        Me.BtnTripZero.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTripZero.FillColor = System.Drawing.Color.Transparent
        Me.BtnTripZero.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnTripZero.ForeColor = System.Drawing.Color.White
        Me.BtnTripZero.Location = New System.Drawing.Point(40, 69)
        Me.BtnTripZero.Name = "BtnTripZero"
        Me.BtnTripZero.Size = New System.Drawing.Size(120, 30)
        Me.BtnTripZero.TabIndex = 53
        Me.BtnTripZero.Text = "ZERO"
        Me.BtnTripZero.Visible = False
        '
        'LblPCorteActual
        '
        Me.LblPCorteActual.AutoSize = True
        Me.LblPCorteActual.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblPCorteActual.ForeColor = System.Drawing.Color.White
        Me.LblPCorteActual.Location = New System.Drawing.Point(87, 29)
        Me.LblPCorteActual.Name = "LblPCorteActual"
        Me.LblPCorteActual.Size = New System.Drawing.Size(27, 37)
        Me.LblPCorteActual.TabIndex = 23
        Me.LblPCorteActual.Text = "-"
        '
        'Guna2CustomGradientPanel9
        '
        Me.Guna2CustomGradientPanel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.BorderRadius = 6
        Me.Guna2CustomGradientPanel9.Controls.Add(Me.LblPCorte)
        Me.Guna2CustomGradientPanel9.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel9.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel9.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel9.Name = "Guna2CustomGradientPanel9"
        Me.Guna2CustomGradientPanel9.Size = New System.Drawing.Size(200, 26)
        Me.Guna2CustomGradientPanel9.TabIndex = 19
        '
        'LblPCorte
        '
        Me.LblPCorte.AutoSize = True
        Me.LblPCorte.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblPCorte.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblPCorte.Location = New System.Drawing.Point(67, 3)
        Me.LblPCorte.Name = "LblPCorte"
        Me.LblPCorte.Size = New System.Drawing.Size(66, 21)
        Me.LblPCorte.TabIndex = 19
        Me.LblPCorte.Text = "Presión"
        '
        'LblMarchaDeseada
        '
        Me.LblMarchaDeseada.AutoSize = True
        Me.LblMarchaDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblMarchaDeseada.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblMarchaDeseada.ForeColor = System.Drawing.Color.White
        Me.LblMarchaDeseada.Location = New System.Drawing.Point(76, 59)
        Me.LblMarchaDeseada.Name = "LblMarchaDeseada"
        Me.LblMarchaDeseada.Size = New System.Drawing.Size(19, 25)
        Me.LblMarchaDeseada.TabIndex = 22
        Me.LblMarchaDeseada.Text = "-"
        '
        'TmrReconnect
        '
        Me.TmrReconnect.Interval = 5000
        '
        'TmrErrorHandle
        '
        Me.TmrErrorHandle.Interval = 600
        '
        'TmrRead
        '
        Me.TmrRead.Interval = 1000
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.BtnParking)
        Me.Guna2Panel1.Controls.Add(Me.BtnMinusGear)
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaActual)
        Me.Guna2Panel1.Controls.Add(Me.BtnPlusGear)
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaDeseada)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha3)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(12, 305)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(200, 151)
        Me.Guna2Panel1.TabIndex = 95
        '
        'BtnParking
        '
        Me.BtnParking.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnParking.Animated = True
        Me.BtnParking.BackColor = System.Drawing.Color.Transparent
        Me.BtnParking.BorderColor = System.Drawing.Color.White
        Me.BtnParking.BorderRadius = 5
        Me.BtnParking.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnParking.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnParking.FillColor = System.Drawing.Color.Transparent
        Me.BtnParking.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnParking.ForeColor = System.Drawing.Color.White
        Me.BtnParking.Image = Global.sFRAC.My.Resources.Resources.brake
        Me.BtnParking.ImageSize = New System.Drawing.Size(33, 33)
        Me.BtnParking.Location = New System.Drawing.Point(162, 33)
        Me.BtnParking.Name = "BtnParking"
        Me.BtnParking.Size = New System.Drawing.Size(35, 35)
        Me.BtnParking.TabIndex = 204
        '
        'BtnMinusGear
        '
        Me.BtnMinusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnMinusGear.Animated = True
        Me.BtnMinusGear.BorderColor = System.Drawing.Color.White
        Me.BtnMinusGear.BorderRadius = 5
        Me.BtnMinusGear.BorderThickness = 1
        Me.BtnMinusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMinusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnMinusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnMinusGear.ForeColor = System.Drawing.Color.White
        Me.BtnMinusGear.Image = CType(resources.GetObject("BtnMinusGear.Image"), System.Drawing.Image)
        Me.BtnMinusGear.Location = New System.Drawing.Point(162, 113)
        Me.BtnMinusGear.Name = "BtnMinusGear"
        Me.BtnMinusGear.Size = New System.Drawing.Size(35, 35)
        Me.BtnMinusGear.TabIndex = 183
        '
        'BtnPlusGear
        '
        Me.BtnPlusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnPlusGear.Animated = True
        Me.BtnPlusGear.BackColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.BorderColor = System.Drawing.Color.White
        Me.BtnPlusGear.BorderRadius = 5
        Me.BtnPlusGear.BorderThickness = 1
        Me.BtnPlusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPlusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnPlusGear.ForeColor = System.Drawing.Color.White
        Me.BtnPlusGear.Image = CType(resources.GetObject("BtnPlusGear.Image"), System.Drawing.Image)
        Me.BtnPlusGear.Location = New System.Drawing.Point(162, 72)
        Me.BtnPlusGear.Name = "BtnPlusGear"
        Me.BtnPlusGear.Size = New System.Drawing.Size(35, 35)
        Me.BtnPlusGear.TabIndex = 182
        '
        'PanelRPM
        '
        Me.PanelRPM.BackColor = System.Drawing.Color.Transparent
        Me.PanelRPM.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelRPM.BorderRadius = 6
        Me.PanelRPM.BorderThickness = 1
        Me.PanelRPM.Controls.Add(Me.BtnResetRPM)
        Me.PanelRPM.Controls.Add(Me.BtnRpmMinus50)
        Me.PanelRPM.Controls.Add(Me.BtnRpmPlus50)
        Me.PanelRPM.Controls.Add(Me.BtnIdle)
        Me.PanelRPM.Controls.Add(Me.LblRPMActual)
        Me.PanelRPM.Controls.Add(Me.LblRPMDeseada)
        Me.PanelRPM.Controls.Add(Me.LblRPM3)
        Me.PanelRPM.Controls.Add(Me.LblRPM1)
        Me.PanelRPM.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.PanelRPM.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelRPM.FillColor = System.Drawing.Color.Transparent
        Me.PanelRPM.Location = New System.Drawing.Point(12, 105)
        Me.PanelRPM.Name = "PanelRPM"
        Me.PanelRPM.Size = New System.Drawing.Size(200, 194)
        Me.PanelRPM.TabIndex = 94
        '
        'BtnResetRPM
        '
        Me.BtnResetRPM.Animated = True
        Me.BtnResetRPM.BorderColor = System.Drawing.Color.White
        Me.BtnResetRPM.BorderRadius = 5
        Me.BtnResetRPM.BorderThickness = 1
        Me.BtnResetRPM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnResetRPM.FillColor = System.Drawing.Color.Transparent
        Me.BtnResetRPM.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnResetRPM.ForeColor = System.Drawing.Color.White
        Me.BtnResetRPM.Location = New System.Drawing.Point(37, 29)
        Me.BtnResetRPM.Name = "BtnResetRPM"
        Me.BtnResetRPM.Size = New System.Drawing.Size(120, 120)
        Me.BtnResetRPM.TabIndex = 98
        Me.BtnResetRPM.Text = "Reset RPM"
        Me.BtnResetRPM.Visible = False
        '
        'BtnRpmMinus50
        '
        Me.BtnRpmMinus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmMinus50.Animated = True
        Me.BtnRpmMinus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.BorderRadius = 5
        Me.BtnRpmMinus50.BorderThickness = 1
        Me.BtnRpmMinus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.Image = CType(resources.GetObject("BtnRpmMinus50.Image"), System.Drawing.Image)
        Me.BtnRpmMinus50.Location = New System.Drawing.Point(162, 112)
        Me.BtnRpmMinus50.Name = "BtnRpmMinus50"
        Me.BtnRpmMinus50.Size = New System.Drawing.Size(35, 35)
        Me.BtnRpmMinus50.TabIndex = 152
        '
        'BtnRpmPlus50
        '
        Me.BtnRpmPlus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmPlus50.Animated = True
        Me.BtnRpmPlus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.BorderRadius = 5
        Me.BtnRpmPlus50.BorderThickness = 1
        Me.BtnRpmPlus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.Image = CType(resources.GetObject("BtnRpmPlus50.Image"), System.Drawing.Image)
        Me.BtnRpmPlus50.Location = New System.Drawing.Point(162, 71)
        Me.BtnRpmPlus50.Name = "BtnRpmPlus50"
        Me.BtnRpmPlus50.Size = New System.Drawing.Size(35, 35)
        Me.BtnRpmPlus50.TabIndex = 151
        '
        'BtnIdle
        '
        Me.BtnIdle.Animated = True
        Me.BtnIdle.BorderColor = System.Drawing.Color.White
        Me.BtnIdle.BorderRadius = 5
        Me.BtnIdle.BorderThickness = 1
        Me.BtnIdle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdle.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdle.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnIdle.ForeColor = System.Drawing.Color.White
        Me.BtnIdle.Location = New System.Drawing.Point(40, 155)
        Me.BtnIdle.Name = "BtnIdle"
        Me.BtnIdle.Size = New System.Drawing.Size(120, 30)
        Me.BtnIdle.TabIndex = 69
        Me.BtnIdle.Text = "IDLE"
        '
        'LblRPMActual
        '
        Me.LblRPMActual.AutoSize = True
        Me.LblRPMActual.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblRPMActual.ForeColor = System.Drawing.Color.White
        Me.LblRPMActual.Location = New System.Drawing.Point(90, 122)
        Me.LblRPMActual.Name = "LblRPMActual"
        Me.LblRPMActual.Size = New System.Drawing.Size(20, 27)
        Me.LblRPMActual.TabIndex = 23
        Me.LblRPMActual.Text = "-"
        '
        'LblRPMDeseada
        '
        Me.LblRPMDeseada.AutoSize = True
        Me.LblRPMDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblRPMDeseada.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.LblRPMDeseada.ForeColor = System.Drawing.Color.White
        Me.LblRPMDeseada.Location = New System.Drawing.Point(91, 60)
        Me.LblRPMDeseada.Name = "LblRPMDeseada"
        Me.LblRPMDeseada.Size = New System.Drawing.Size(19, 25)
        Me.LblRPMDeseada.TabIndex = 22
        Me.LblRPMDeseada.Text = "-"
        '
        'LblRPM3
        '
        Me.LblRPM3.AutoSize = True
        Me.LblRPM3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM3.ForeColor = System.Drawing.Color.White
        Me.LblRPM3.Location = New System.Drawing.Point(47, 94)
        Me.LblRPM3.Name = "LblRPM3"
        Me.LblRPM3.Size = New System.Drawing.Size(106, 20)
        Me.LblRPM3.TabIndex = 21
        Me.LblRPM3.Text = "RPM Actuales"
        '
        'LblRPM1
        '
        Me.LblRPM1.AutoSize = True
        Me.LblRPM1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM1.ForeColor = System.Drawing.Color.White
        Me.LblRPM1.Location = New System.Drawing.Point(44, 32)
        Me.LblRPM1.Name = "LblRPM1"
        Me.LblRPM1.Size = New System.Drawing.Size(113, 20)
        Me.LblRPM1.TabIndex = 20
        Me.LblRPM1.Text = "RPM Deseadas"
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblRpm)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(200, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblRpm
        '
        Me.LblRpm.AutoSize = True
        Me.LblRpm.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblRpm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblRpm.Location = New System.Drawing.Point(77, 3)
        Me.LblRpm.Name = "LblRpm"
        Me.LblRpm.Size = New System.Drawing.Size(46, 21)
        Me.LblRpm.TabIndex = 19
        Me.LblRpm.Text = "RPM"
        '
        'TmrCheck
        '
        Me.TmrCheck.Interval = 1000
        '
        'TmrBackUp
        '
        Me.TmrBackUp.Interval = 1000
        '
        'TmrOp
        '
        Me.TmrOp.Interval = 1000
        '
        'BtnEKill
        '
        Me.BtnEKill.Animated = True
        Me.BtnEKill.BorderColor = System.Drawing.Color.White
        Me.BtnEKill.BorderRadius = 5
        Me.BtnEKill.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BtnEKill.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEKill.FillColor = System.Drawing.Color.Transparent
        Me.BtnEKill.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnEKill.ForeColor = System.Drawing.Color.White
        Me.BtnEKill.Image = Global.sFRAC.My.Resources.Resources.E_STOP
        Me.BtnEKill.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnEKill.ImageSize = New System.Drawing.Size(59, 59)
        Me.BtnEKill.Location = New System.Drawing.Point(82, 39)
        Me.BtnEKill.Name = "BtnEKill"
        Me.BtnEKill.Size = New System.Drawing.Size(60, 60)
        Me.BtnEKill.TabIndex = 188
        '
        'PbMoxaStatus
        '
        Me.PbMoxaStatus.Enabled = False
        Me.PbMoxaStatus.Location = New System.Drawing.Point(12, 705)
        Me.PbMoxaStatus.Name = "PbMoxaStatus"
        Me.PbMoxaStatus.ProgressColor = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.PbMoxaStatus.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(91, Byte), Integer), CType(CType(194, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.PbMoxaStatus.Size = New System.Drawing.Size(62, 10)
        Me.PbMoxaStatus.TabIndex = 190
        Me.PbMoxaStatus.Text = "Guna2ProgressBar1"
        Me.PbMoxaStatus.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.ToolTip1.SetToolTip(Me.PbMoxaStatus, "Estado de conexión")
        Me.PbMoxaStatus.Value = 100
        '
        'PbLockUp
        '
        Me.PbLockUp.FillColor = System.Drawing.Color.Silver
        Me.PbLockUp.Location = New System.Drawing.Point(80, 705)
        Me.PbLockUp.Name = "PbLockUp"
        Me.PbLockUp.ProgressColor = System.Drawing.Color.DarkGray
        Me.PbLockUp.ProgressColor2 = System.Drawing.Color.DarkGray
        Me.PbLockUp.Size = New System.Drawing.Size(64, 10)
        Me.PbLockUp.TabIndex = 191
        Me.PbLockUp.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.ToolTip1.SetToolTip(Me.PbLockUp, "Estado de lock up")
        Me.PbLockUp.Value = 100
        '
        'PbAlarms
        '
        Me.PbAlarms.FillColor = System.Drawing.Color.Silver
        Me.PbAlarms.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.PbAlarms.ForeColor = System.Drawing.Color.Transparent
        Me.PbAlarms.Location = New System.Drawing.Point(150, 705)
        Me.PbAlarms.Name = "PbAlarms"
        Me.PbAlarms.ProgressColor = System.Drawing.Color.DarkGray
        Me.PbAlarms.ProgressColor2 = System.Drawing.Color.DarkGray
        Me.PbAlarms.Size = New System.Drawing.Size(62, 10)
        Me.PbAlarms.TabIndex = 192
        Me.PbAlarms.Text = "LU"
        Me.PbAlarms.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        Me.ToolTip1.SetToolTip(Me.PbAlarms, "Estado de alarmas")
        Me.PbAlarms.Value = 100
        '
        'ToolTip1
        '
        Me.ToolTip1.AllowLinksHandling = True
        Me.ToolTip1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.ToolTip1.MaximumSize = New System.Drawing.Size(0, 0)
        Me.ToolTip1.TitleFont = New System.Drawing.Font("Montserrat", 12.0!)
        '
        'EightFrac_1920x1080
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Controls.Add(Me.PbAlarms)
        Me.Controls.Add(Me.PbLockUp)
        Me.Controls.Add(Me.PbMoxaStatus)
        Me.Controls.Add(Me.BtnEKill)
        Me.Controls.Add(Me.BtnErrores)
        Me.Controls.Add(Me.BtnAjustes)
        Me.Controls.Add(Me.BtnStartEngine)
        Me.Controls.Add(Me.Guna2Panel10)
        Me.Controls.Add(Me.Guna2Panel9)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.PanelRPM)
        Me.DoubleBuffered = True
        Me.Name = "EightFrac_1920x1080"
        Me.Size = New System.Drawing.Size(224, 721)
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.Guna2Panel10.ResumeLayout(False)
        Me.Guna2Panel10.PerformLayout()
        Me.Guna2CustomGradientPanel10.ResumeLayout(False)
        Me.Guna2CustomGradientPanel10.PerformLayout()
        Me.Guna2Panel9.ResumeLayout(False)
        Me.Guna2Panel9.PerformLayout()
        Me.Guna2CustomGradientPanel9.ResumeLayout(False)
        Me.Guna2CustomGradientPanel9.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.PanelRPM.ResumeLayout(False)
        Me.PanelRPM.PerformLayout()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Public WithEvents LblMarchaActual As Label
    Public WithEvents LblMarcha3 As Label
    Public WithEvents LblMarcha1 As Label
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblMarcha As Label
    Public WithEvents BtnErrores As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAjustes As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnStartEngine As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel10 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnFC As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblCaudal2 As Label
    Public WithEvents LblCaudalActual As Label
    Public WithEvents Guna2CustomGradientPanel10 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblCaudal As Label
    Public WithEvents Guna2Panel9 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label2 As Label
    Public WithEvents BtnTripZero As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblPCorteActual As Label
    Public WithEvents Guna2CustomGradientPanel9 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblPCorte As Label
    Public WithEvents LblMarchaDeseada As Label
    Public WithEvents TmrReconnect As Timer
    Public WithEvents TmrErrorHandle As Timer
    Public WithEvents TmrRead As Timer
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents PanelRPM As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnResetRPM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnIdle As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPMActual As Label
    Public WithEvents LblRPMDeseada As Label
    Public WithEvents LblRPM3 As Label
    Public WithEvents LblRPM1 As Label
    Public WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblRpm As Label
    Public WithEvents TmrCheck As Timer
    Public WithEvents BtnMinusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnPlusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmMinus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus50 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents TmrBackUp As Timer
    Friend WithEvents TmrOp As Timer
    Public WithEvents BtnEKill As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents PbMoxaStatus As Guna.UI2.WinForms.Guna2ProgressBar
    Friend WithEvents PbLockUp As Guna.UI2.WinForms.Guna2ProgressBar
    Friend WithEvents PbAlarms As Guna.UI2.WinForms.Guna2ProgressBar
    Friend WithEvents ToolTip1 As Guna.UI2.WinForms.Guna2HtmlToolTip
    Public WithEvents BtnParking As Guna.UI2.WinForms.Guna2Button
End Class
