﻿Public Class AjustesFracturador
    ReadOnly sCentrifuga As Boolean = False
    Private Sub AjustesFracturador_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Centramos todo
        CenterEverything()

        ''Si este frac ya envia por SP, mostramos activo
        If SPFracs.Find(Function(value As Integer) value = currentFrac) <> 0 Then
            TSSP.Checked = True
        Else
            TSSP.Checked = False
        End If

        ''Si este frac ya envia por WITS, mostramos activo
        If WITSFracs.Find(Function(value As Integer) value = currentFrac) <> 0 Then
            TSWITS.Checked = True
        Else
            TSWITS.Checked = False
        End If

        PnCentrifuga.Enabled = FracEdit.GetFracCentrifuge(CurrentFrac)

    End Sub

    ''Sub para centrar todo
    Public Sub CenterEverything()
        CenterObject(Title)
        CenterObject(LblCentrifuga)
        CenterObject(CbCentrifuga)

        LeftAlign(LblCentrifugaD, TbCentrifuga.Left - 6)

        CenterAlign(TSSP, LblSP)
    End Sub


    ''Aceptamos, enviamos centrifuga y seteamos config
    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        Try

            If sCentrifuga = True Then
                ISystem.MoxaSend(FracMoxaSendStartAddress("CentrifugaStartAddress"), GetInteger(TbCentrifuga.Text), currentFrac)
            End If

            Me.DialogResult = DialogResult.OK
        Catch ex As Exception
            LogError(ex)
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.ErrorDatos")
            AvisoGeneral.ShowDialog(Me)
            Me.DialogResult = DialogResult.Cancel
        End Try
    End Sub

    ''CbChanged sobre centrifuga
    Private Sub CbCentrifuga_CheckedChanged(sender As Object, e As EventArgs) Handles CbCentrifuga.CheckedChanged
        'Dim HasCentrifuge = FracEdit.GetFracCentrifuge(CurrentFrac)
        'If HasCentrifuge = True Then
        '    If sender.CheckState = CheckState.Checked Then
        '        TbCentrifuga.Enabled = True
        '    Else
        '        TbCentrifuga.Enabled = False
        '    End If
        'Else
        '    sender.CheckState = CheckState.Unchecked
        '    avisoStr = LanguageSelect.DynamicLanguageRM.GetString("CentrifugaError")
        '    avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
        '    AvisoGeneral.ShowDialog(Me)
        'End If
    End Sub

    ''Maximo de centrifuga es 100, minimo es cero
    Private Sub TbCentrifuga_TextChanged(sender As Object, e As EventArgs) Handles TbCentrifuga.TextChanged
        If Not String.IsNullOrEmpty(TbCentrifuga.Text) Then
            If IsInteger(TbCentrifuga.Text) Then
                Dim PCentrifuga As Integer
                PCentrifuga = TbCentrifuga.Text
                PCentrifuga = Math.Min(100, PCentrifuga)
                PCentrifuga = Math.Max(0, PCentrifuga)
                TbCentrifuga.Text = PCentrifuga.ToString()
            End If
        End If
    End Sub

    '''Si cambia el sensor, mostramos los datos guardados en diccionairo de ese sensor
    'Private Sub CbSensores_SelectedIndexChanged(sender As Object, e As EventArgs)

    '    Select Case sender.Text
    '        Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[0]")
    '            SensorInMin.Text = FConfig.SettingsRM.Item("VIATRAN_SENSOR_SCALE_X0")
    '            SensorInMax.Text = FConfig.SettingsRM.Item("VIATRAN_SENSOR_SCALE_X1")
    '            SensorOutMin.Text = FConfig.SettingsRM.Item("VIATRAN_SENSOR_SCALE_Y0")
    '            SensorOutMax.Text = FConfig.SettingsRM.Item("VIATRAN_SENSOR_SCALE_Y1")
    '        Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[1]")
    '            SensorInMin.Text = FConfig.SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_X0")
    '            SensorInMax.Text = FConfig.SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_X1")
    '            SensorOutMin.Text = FConfig.SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_Y0")
    '            SensorOutMax.Text = FConfig.SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_Y1")
    '        Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[2]")
    '            SensorInMin.Text = FConfig.SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_X0")
    '            SensorInMax.Text = FConfig.SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_X1")
    '            SensorOutMin.Text = FConfig.SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_Y0")
    '            SensorOutMax.Text = FConfig.SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_Y1")
    '        Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[3]")
    '            SensorInMin.Text = FConfig.SettingsRM.Item("PRESSUC_SENSOR_SCALE_X0")
    '            SensorInMax.Text = FConfig.SettingsRM.Item("PRESSUC_SENSOR_SCALE_X1")
    '            SensorOutMin.Text = FConfig.SettingsRM.Item("PRESSUC_SENSOR_SCALE_Y0")
    '            SensorOutMax.Text = FConfig.SettingsRM.Item("PRESSUC_SENSOR_SCALE_Y1")
    '        Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[4]")
    '            SensorInMin.Text = FConfig.SettingsRM.Item("PRESTRANS_SENSOR_SCALE_X0")
    '            SensorInMax.Text = FConfig.SettingsRM.Item("PRESTRANS_SENSOR_SCALE_X1")
    '            SensorOutMin.Text = FConfig.SettingsRM.Item("PRESTRANS_SENSOR_SCALE_Y0")
    '            SensorOutMax.Text = FConfig.SettingsRM.Item("PRESTRANS_SENSOR_SCALE_Y1")
    '    End Select

    'End Sub


    ''Si quiere enviar por SP, agregamos al frac a la lista
    Private Sub TSSP_CheckedChanged(sender As Object, e As EventArgs) Handles TSSP.CheckedChanged
        If sender.Checked = True And Not SPFracs.Contains(currentFrac) Then
            SPFracs.Add(currentFrac)
            SPDevices += 1
        Else
            For i = SPFracs.Count - 1 To 0 Step -1
                If SPFracs(i) = currentFrac Then
                    SPFracs.RemoveAt(i)
                    SPDevices -= 1
                End If
            Next
        End If
    End Sub

    ''Si quiere enviar por WITS, agregamos al frac a la lista
    Private Sub TSWITS_CheckedChanged(sender As Object, e As EventArgs) Handles TSWITS.CheckedChanged
        If sender.Checked = True And Not WITSFracs.Contains(currentFrac) Then
            WITSFracs.Add(currentFrac)
            WITSDevices += 1
        Else
            For i = WITSFracs.Count - 1 To 0 Step -1
                If WITSFracs(i) = currentFrac Then
                    WITSFracs.RemoveAt(i)
                    WITSDevices -= 1
                End If
            Next

        End If
    End Sub

    Private Sub BtnEngineParams_Click(sender As Object, e As EventArgs) Handles BtnEngineParams.Click
        EngineParams.ShowDialog(Me)
    End Sub

    Private Sub BtnPumpParams_Click(sender As Object, e As EventArgs) Handles BtnPumpParams.Click
        PumpParams.ShowDialog(Me)
    End Sub

    Private Sub BtnTransParams_Click(sender As Object, e As EventArgs) Handles BtnTransParams.Click
        TransParams.ShowDialog(Me)
    End Sub

    'Private Sub Sensor_Leave(sender As Object, e As EventArgs)

    '    Dim currentSensorName As String = CbSensores.Text
    '    Dim CurrentChange As String = sender.Name
    '    Dim currentSensorMemory As String = ""

    '    If Not String.IsNullOrEmpty(sender.Text) Then
    '        If IsInteger(sender.Text) Then
    '            Select Case currentSensorName
    '                Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[0]")
    '                    currentSensorMemory = "VIATRAN"
    '                Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[1]")
    '                    currentSensorMemory = "TEMPLUB_LP"
    '                Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[2]")
    '                    currentSensorMemory = "PRESLUB_LP"
    '                Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[3]")
    '                    currentSensorMemory = "PRESSUC"
    '                Case LanguageSelect.LanguageRM.GetString("AjustesFracturador.CbSensores[4]")
    '                    currentSensorMemory = "PRESTRANS"
    '                Case Else
    '                    currentSensorMemory = ""
    '            End Select

    '            Select Case CurrentChange
    '                Case "SensorInMin"
    '                    FConfig.SettingsRM.Item(currentSensorMemory + "_SENSOR_SCALE_X0") = sender.Text
    '                    FConfig.SetSingleConfig(FracMoxaSendStartAddress("UpdateDirection_StartAddress"), FracMoxaUpdateDirection(currentSensorMemory + "_SENSOR_SCALE_X0_UD"),
    '                     FracMoxaSendStartAddress(currentSensorMemory + "_SENSOR_SCALE_X0_StartAddress"), FConfig.SettingsRM.Item(currentSensorMemory + "_SENSOR_SCALE_X0"),
    '                        currentFrac)
    '                Case "SensorInMax"
    '                    FConfig.SettingsRM.Item(currentSensorMemory + "_SENSOR_SCALE_X1") = sender.Text
    '                    FConfig.SetSingleConfig(FracMoxaSendStartAddress("UpdateDirection_StartAddress"), FracMoxaUpdateDirection(currentSensorMemory + "_SENSOR_SCALE_X1_UD"),
    '                     FracMoxaSendStartAddress(currentSensorMemory + "_SENSOR_SCALE_X1_StartAddress"), FConfig.SettingsRM.Item(currentSensorMemory + "_SENSOR_SCALE_X1"),
    '                        currentFrac)
    '                Case "SensorOutMin"
    '                    FConfig.SettingsRM.Item(currentSensorMemory + "_SENSOR_SCALE_Y0") = sender.Text
    '                    FConfig.SetSingleConfig(FracMoxaSendStartAddress("UpdateDirection_StartAddress"), FracMoxaUpdateDirection(currentSensorMemory + "_SENSOR_SCALE_Y0_UD"),
    '                     FracMoxaSendStartAddress(currentSensorMemory + "_SENSOR_SCALE_Y0_StartAddress"), FConfig.SettingsRM.Item(currentSensorMemory + "_SENSOR_SCALE_Y0"),
    '                        currentFrac)
    '                Case "SensorOutMax"
    '                    FConfig.SettingsRM.Item(currentSensorMemory + "_SENSOR_SCALE_Y1") = sender.Text
    '                    FConfig.SetSingleConfig(FracMoxaSendStartAddress("UpdateDirection_StartAddress"), FracMoxaUpdateDirection(currentSensorMemory + "_SENSOR_SCALE_Y1_UD"),
    '                     FracMoxaSendStartAddress(currentSensorMemory + "_SENSOR_SCALE_Y1_StartAddress"), FConfig.SettingsRM.Item(currentSensorMemory + "_SENSOR_SCALE_Y1"),
    '                        currentFrac)
    '            End Select
    '        End If
    '    End If
    'End Sub
End Class