﻿Public Class TransParams
    Private Sub TransParams_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Centramos todo
        CenterEverything()

        LblTransName.Text = FracData.SelectedTransmissions("SelectedTransmission_" + currentFrac.ToString()).Item("Name")
        Dim Gears As Integer = FracData.SelectedTransmissions("SelectedTransmission_" + currentFrac.ToString()).Item("Gears")
        LblGears.Text = Gears
        Height = 129 + 44 * Gears
        For i = 1 To Gears
            Controls.Find("LblRel" + i.ToString(), True)(0).Text = FracData.SelectedTransmissions("SelectedTransmission_" + currentFrac.ToString()).Item("RelacionGear" + i.ToString())
        Next
    End Sub

    ''Sub para centrar las cosas
    Private Sub CenterEverything()
        CenterObject(Title)
    End Sub
End Class