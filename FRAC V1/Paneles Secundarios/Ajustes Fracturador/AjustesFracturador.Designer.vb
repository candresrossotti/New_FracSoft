﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AjustesFracturador
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.PnCentrifuga = New Guna.UI2.WinForms.Guna2Panel()
        Me.TbCentrifuga = New Guna.UI2.WinForms.Guna2TextBox()
        Me.LblCentrifugaD = New System.Windows.Forms.Label()
        Me.CbCentrifuga = New Guna.UI2.WinForms.Guna2CheckBox()
        Me.Guna2CustomGradientPanel3 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCentrifuga = New System.Windows.Forms.Label()
        Me.TSSP = New Guna.UI2.WinForms.Guna2ToggleSwitch()
        Me.TSWITS = New Guna.UI2.WinForms.Guna2ToggleSwitch()
        Me.LblSP = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Guna2Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblComunicacion = New System.Windows.Forms.Label()
        Me.BtnEngineParams = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPumpParams = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnTransParams = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.TitlePanel.SuspendLayout()
        Me.PnCentrifuga.SuspendLayout()
        Me.Guna2CustomGradientPanel3.SuspendLayout()
        Me.Guna2Panel4.SuspendLayout()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(410, 38)
        Me.TitlePanel.TabIndex = 1
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(158, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(94, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Ajustes"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(362, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'PnCentrifuga
        '
        Me.PnCentrifuga.BackColor = System.Drawing.Color.Transparent
        Me.PnCentrifuga.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PnCentrifuga.BorderRadius = 6
        Me.PnCentrifuga.BorderThickness = 1
        Me.PnCentrifuga.Controls.Add(Me.TbCentrifuga)
        Me.PnCentrifuga.Controls.Add(Me.LblCentrifugaD)
        Me.PnCentrifuga.Controls.Add(Me.CbCentrifuga)
        Me.PnCentrifuga.Controls.Add(Me.Guna2CustomGradientPanel3)
        Me.PnCentrifuga.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PnCentrifuga.FillColor = System.Drawing.Color.Transparent
        Me.PnCentrifuga.Location = New System.Drawing.Point(14, 44)
        Me.PnCentrifuga.Name = "PnCentrifuga"
        Me.PnCentrifuga.Size = New System.Drawing.Size(382, 109)
        Me.PnCentrifuga.TabIndex = 71
        '
        'TbCentrifuga
        '
        Me.TbCentrifuga.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbCentrifuga.DefaultText = ""
        Me.TbCentrifuga.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbCentrifuga.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbCentrifuga.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCentrifuga.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCentrifuga.Enabled = False
        Me.TbCentrifuga.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCentrifuga.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.TbCentrifuga.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCentrifuga.Location = New System.Drawing.Point(157, 59)
        Me.TbCentrifuga.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TbCentrifuga.Name = "TbCentrifuga"
        Me.TbCentrifuga.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbCentrifuga.PlaceholderText = ""
        Me.TbCentrifuga.SelectedText = ""
        Me.TbCentrifuga.Size = New System.Drawing.Size(200, 36)
        Me.TbCentrifuga.TabIndex = 70
        Me.TbCentrifuga.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblCentrifugaD
        '
        Me.LblCentrifugaD.AutoSize = True
        Me.LblCentrifugaD.BackColor = System.Drawing.Color.Transparent
        Me.LblCentrifugaD.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblCentrifugaD.ForeColor = System.Drawing.Color.White
        Me.LblCentrifugaD.Location = New System.Drawing.Point(13, 66)
        Me.LblCentrifugaD.Name = "LblCentrifugaD"
        Me.LblCentrifugaD.Size = New System.Drawing.Size(137, 22)
        Me.LblCentrifugaD.TabIndex = 63
        Me.LblCentrifugaD.Text = "% de centrífuga:"
        '
        'CbCentrifuga
        '
        Me.CbCentrifuga.AutoSize = True
        Me.CbCentrifuga.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.CbCentrifuga.CheckedState.BorderRadius = 0
        Me.CbCentrifuga.CheckedState.BorderThickness = 0
        Me.CbCentrifuga.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.CbCentrifuga.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbCentrifuga.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.CbCentrifuga.ForeColor = System.Drawing.Color.White
        Me.CbCentrifuga.Location = New System.Drawing.Point(117, 30)
        Me.CbCentrifuga.Name = "CbCentrifuga"
        Me.CbCentrifuga.Size = New System.Drawing.Size(169, 26)
        Me.CbCentrifuga.TabIndex = 20
        Me.CbCentrifuga.Text = "Activar centrífuga"
        Me.CbCentrifuga.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbCentrifuga.UncheckedState.BorderRadius = 0
        Me.CbCentrifuga.UncheckedState.BorderThickness = 0
        Me.CbCentrifuga.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Guna2CustomGradientPanel3
        '
        Me.Guna2CustomGradientPanel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.BorderRadius = 6
        Me.Guna2CustomGradientPanel3.Controls.Add(Me.LblCentrifuga)
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel3.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel3.Name = "Guna2CustomGradientPanel3"
        Me.Guna2CustomGradientPanel3.Size = New System.Drawing.Size(382, 26)
        Me.Guna2CustomGradientPanel3.TabIndex = 19
        '
        'LblCentrifuga
        '
        Me.LblCentrifuga.AutoSize = True
        Me.LblCentrifuga.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblCentrifuga.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCentrifuga.Location = New System.Drawing.Point(146, 3)
        Me.LblCentrifuga.Name = "LblCentrifuga"
        Me.LblCentrifuga.Size = New System.Drawing.Size(90, 21)
        Me.LblCentrifuga.TabIndex = 19
        Me.LblCentrifuga.Text = "Centrífuga"
        '
        'TSSP
        '
        Me.TSSP.Animated = True
        Me.TSSP.CheckedState.FillColor = System.Drawing.Color.LimeGreen
        Me.TSSP.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TSSP.Location = New System.Drawing.Point(92, 69)
        Me.TSSP.Name = "TSSP"
        Me.TSSP.Size = New System.Drawing.Size(58, 20)
        Me.TSSP.TabIndex = 161
        Me.TSSP.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.TSSP.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.TSSP.UncheckedState.InnerBorderColor = System.Drawing.Color.White
        Me.TSSP.UncheckedState.InnerColor = System.Drawing.Color.White
        '
        'TSWITS
        '
        Me.TSWITS.Animated = True
        Me.TSWITS.CheckedState.FillColor = System.Drawing.Color.LimeGreen
        Me.TSWITS.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TSWITS.Location = New System.Drawing.Point(248, 69)
        Me.TSWITS.Name = "TSWITS"
        Me.TSWITS.Size = New System.Drawing.Size(58, 20)
        Me.TSWITS.TabIndex = 162
        Me.TSWITS.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.TSWITS.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.TSWITS.UncheckedState.InnerBorderColor = System.Drawing.Color.White
        Me.TSWITS.UncheckedState.InnerColor = System.Drawing.Color.White
        '
        'LblSP
        '
        Me.LblSP.AutoSize = True
        Me.LblSP.BackColor = System.Drawing.Color.Transparent
        Me.LblSP.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblSP.ForeColor = System.Drawing.Color.White
        Me.LblSP.Location = New System.Drawing.Point(67, 39)
        Me.LblSP.Name = "LblSP"
        Me.LblSP.Size = New System.Drawing.Size(108, 22)
        Me.LblSP.TabIndex = 163
        Me.LblSP.Text = "Puerto Serie"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(251, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 22)
        Me.Label2.TabIndex = 164
        Me.Label2.Text = "WITS"
        '
        'Guna2Panel4
        '
        Me.Guna2Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.BorderRadius = 6
        Me.Guna2Panel4.BorderThickness = 1
        Me.Guna2Panel4.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.Guna2Panel4.Controls.Add(Me.Label2)
        Me.Guna2Panel4.Controls.Add(Me.LblSP)
        Me.Guna2Panel4.Controls.Add(Me.TSSP)
        Me.Guna2Panel4.Controls.Add(Me.TSWITS)
        Me.Guna2Panel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.Location = New System.Drawing.Point(14, 159)
        Me.Guna2Panel4.Name = "Guna2Panel4"
        Me.Guna2Panel4.Size = New System.Drawing.Size(382, 109)
        Me.Guna2Panel4.TabIndex = 165
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.LblComunicacion)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(382, 26)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'LblComunicacion
        '
        Me.LblComunicacion.AutoSize = True
        Me.LblComunicacion.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblComunicacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblComunicacion.Location = New System.Drawing.Point(132, 3)
        Me.LblComunicacion.Name = "LblComunicacion"
        Me.LblComunicacion.Size = New System.Drawing.Size(118, 21)
        Me.LblComunicacion.TabIndex = 19
        Me.LblComunicacion.Text = "Comunicación"
        '
        'BtnEngineParams
        '
        Me.BtnEngineParams.Animated = True
        Me.BtnEngineParams.BorderColor = System.Drawing.Color.White
        Me.BtnEngineParams.BorderRadius = 5
        Me.BtnEngineParams.BorderThickness = 1
        Me.BtnEngineParams.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEngineParams.FillColor = System.Drawing.Color.Transparent
        Me.BtnEngineParams.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnEngineParams.ForeColor = System.Drawing.Color.White
        Me.BtnEngineParams.Location = New System.Drawing.Point(14, 274)
        Me.BtnEngineParams.Name = "BtnEngineParams"
        Me.BtnEngineParams.Size = New System.Drawing.Size(382, 50)
        Me.BtnEngineParams.TabIndex = 166
        Me.BtnEngineParams.Text = "Ver parametros de motor"
        '
        'BtnPumpParams
        '
        Me.BtnPumpParams.Animated = True
        Me.BtnPumpParams.BorderColor = System.Drawing.Color.White
        Me.BtnPumpParams.BorderRadius = 5
        Me.BtnPumpParams.BorderThickness = 1
        Me.BtnPumpParams.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPumpParams.FillColor = System.Drawing.Color.Transparent
        Me.BtnPumpParams.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnPumpParams.ForeColor = System.Drawing.Color.White
        Me.BtnPumpParams.Location = New System.Drawing.Point(14, 338)
        Me.BtnPumpParams.Name = "BtnPumpParams"
        Me.BtnPumpParams.Size = New System.Drawing.Size(382, 50)
        Me.BtnPumpParams.TabIndex = 167
        Me.BtnPumpParams.Text = "Ver parametros de bomba"
        '
        'BtnTransParams
        '
        Me.BtnTransParams.Animated = True
        Me.BtnTransParams.BorderColor = System.Drawing.Color.White
        Me.BtnTransParams.BorderRadius = 5
        Me.BtnTransParams.BorderThickness = 1
        Me.BtnTransParams.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTransParams.FillColor = System.Drawing.Color.Transparent
        Me.BtnTransParams.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnTransParams.ForeColor = System.Drawing.Color.White
        Me.BtnTransParams.Location = New System.Drawing.Point(14, 402)
        Me.BtnTransParams.Name = "BtnTransParams"
        Me.BtnTransParams.Size = New System.Drawing.Size(382, 50)
        Me.BtnTransParams.TabIndex = 168
        Me.BtnTransParams.Text = "Ver parametros de transmisión"
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 14.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(116, 471)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(179, 40)
        Me.BtnAceptar.TabIndex = 71
        Me.BtnAceptar.Text = "Aceptar"
        '
        'AjustesFracturador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(410, 523)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.BtnTransParams)
        Me.Controls.Add(Me.BtnPumpParams)
        Me.Controls.Add(Me.BtnEngineParams)
        Me.Controls.Add(Me.Guna2Panel4)
        Me.Controls.Add(Me.PnCentrifuga)
        Me.Controls.Add(Me.TitlePanel)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "AjustesFracturador"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AjustesFracturador"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.PnCentrifuga.ResumeLayout(False)
        Me.PnCentrifuga.PerformLayout()
        Me.Guna2CustomGradientPanel3.ResumeLayout(False)
        Me.Guna2CustomGradientPanel3.PerformLayout()
        Me.Guna2Panel4.ResumeLayout(False)
        Me.Guna2Panel4.PerformLayout()
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents PnCentrifuga As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel3 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Title As Label
    Public WithEvents LblCentrifuga As Label
    Public WithEvents TbCentrifuga As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents LblCentrifugaD As Label
    Public WithEvents CbCentrifuga As Guna.UI2.WinForms.Guna2CheckBox
    Friend WithEvents Guna2Panel4 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblComunicacion As Label
    Public WithEvents Label2 As Label
    Public WithEvents LblSP As Label
    Friend WithEvents TSSP As Guna.UI2.WinForms.Guna2ToggleSwitch
    Friend WithEvents TSWITS As Guna.UI2.WinForms.Guna2ToggleSwitch
    Public WithEvents BtnTransParams As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnPumpParams As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnEngineParams As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
End Class
