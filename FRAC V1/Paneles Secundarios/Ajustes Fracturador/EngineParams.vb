﻿Public Class EngineParams
    Private Sub EngineParams_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Centramos todo
        CenterEverything()

        LblEngineName.Text = FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString()).Item("Name")
        LblRPMEf.Text = FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString()).Item("EfficientRPM") + " [RPM]"
        LblHPEf.Text = FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString()).Item("EfficientHP") + " [HP]"
        LblHPMax.Text = FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString()).Item("MaxHP") + " [HP]"
        LblRPMMin.Text = FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString()).Item("MinRPM") + " [RPM]"
        LblRPMMax.Text = FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString()).Item("MaxRPM") + " [RPM]"
    End Sub

    ''Sub para centrar las cosas
    Private Sub CenterEverything()
        CenterObject(Title)
    End Sub

End Class