﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EngineParams
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.LblRPMMax = New System.Windows.Forms.Label()
        Me.LblRPMMin = New System.Windows.Forms.Label()
        Me.LblHPMax = New System.Windows.Forms.Label()
        Me.LblHPEf = New System.Windows.Forms.Label()
        Me.LblRPMEf = New System.Windows.Forms.Label()
        Me.LblEngineName = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(330, 38)
        Me.TitlePanel.TabIndex = 169
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(57, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(217, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Parametros motor"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(282, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(59, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 22)
        Me.Label1.TabIndex = 170
        Me.Label1.Text = "Nombre:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(12, 97)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 22)
        Me.Label2.TabIndex = 171
        Me.Label2.Text = "RPM eficiente:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(26, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 22)
        Me.Label3.TabIndex = 172
        Me.Label3.Text = "HP eficiente:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(32, 187)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(107, 22)
        Me.Label4.TabIndex = 173
        Me.Label4.Text = "HP maximo:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(21, 232)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(118, 22)
        Me.Label5.TabIndex = 174
        Me.Label5.Text = "RPM minimo:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(18, 277)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(121, 22)
        Me.Label6.TabIndex = 175
        Me.Label6.Text = "RPM maximo:"
        '
        'LblRPMMax
        '
        Me.LblRPMMax.AutoSize = True
        Me.LblRPMMax.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRPMMax.ForeColor = System.Drawing.Color.White
        Me.LblRPMMax.Location = New System.Drawing.Point(145, 277)
        Me.LblRPMMax.Name = "LblRPMMax"
        Me.LblRPMMax.Size = New System.Drawing.Size(83, 22)
        Me.LblRPMMax.TabIndex = 181
        Me.LblRPMMax.Text = "RPMmax"
        '
        'LblRPMMin
        '
        Me.LblRPMMin.AutoSize = True
        Me.LblRPMMin.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRPMMin.ForeColor = System.Drawing.Color.White
        Me.LblRPMMin.Location = New System.Drawing.Point(145, 232)
        Me.LblRPMMin.Name = "LblRPMMin"
        Me.LblRPMMin.Size = New System.Drawing.Size(80, 22)
        Me.LblRPMMin.TabIndex = 180
        Me.LblRPMMin.Text = "RPMmin"
        '
        'LblHPMax
        '
        Me.LblHPMax.AutoSize = True
        Me.LblHPMax.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblHPMax.ForeColor = System.Drawing.Color.White
        Me.LblHPMax.Location = New System.Drawing.Point(145, 187)
        Me.LblHPMax.Name = "LblHPMax"
        Me.LblHPMax.Size = New System.Drawing.Size(69, 22)
        Me.LblHPMax.TabIndex = 179
        Me.LblHPMax.Text = "HPmax"
        '
        'LblHPEf
        '
        Me.LblHPEf.AutoSize = True
        Me.LblHPEf.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblHPEf.ForeColor = System.Drawing.Color.White
        Me.LblHPEf.Location = New System.Drawing.Point(145, 142)
        Me.LblHPEf.Name = "LblHPEf"
        Me.LblHPEf.Size = New System.Drawing.Size(47, 22)
        Me.LblHPEf.TabIndex = 178
        Me.LblHPEf.Text = "hpef"
        '
        'LblRPMEf
        '
        Me.LblRPMEf.AutoSize = True
        Me.LblRPMEf.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRPMEf.ForeColor = System.Drawing.Color.White
        Me.LblRPMEf.Location = New System.Drawing.Point(145, 97)
        Me.LblRPMEf.Name = "LblRPMEf"
        Me.LblRPMEf.Size = New System.Drawing.Size(59, 22)
        Me.LblRPMEf.TabIndex = 177
        Me.LblRPMEf.Text = "rpmef"
        '
        'LblEngineName
        '
        Me.LblEngineName.AutoSize = True
        Me.LblEngineName.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblEngineName.ForeColor = System.Drawing.Color.White
        Me.LblEngineName.Location = New System.Drawing.Point(145, 52)
        Me.LblEngineName.Name = "LblEngineName"
        Me.LblEngineName.Size = New System.Drawing.Size(59, 22)
        Me.LblEngineName.TabIndex = 176
        Me.LblEngineName.Text = "motor"
        '
        'EngineParams
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(330, 318)
        Me.Controls.Add(Me.LblRPMMax)
        Me.Controls.Add(Me.LblRPMMin)
        Me.Controls.Add(Me.LblHPMax)
        Me.Controls.Add(Me.LblHPEf)
        Me.Controls.Add(Me.LblRPMEf)
        Me.Controls.Add(Me.LblEngineName)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "EngineParams"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EngineParams"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Label1 As Label
    Friend WithEvents LblRPMMax As Label
    Friend WithEvents LblRPMMin As Label
    Friend WithEvents LblHPMax As Label
    Friend WithEvents LblHPEf As Label
    Friend WithEvents LblRPMEf As Label
    Friend WithEvents LblEngineName As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
End Class
