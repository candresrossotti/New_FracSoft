﻿Public Class PumpParams
    Private Sub PumpParams_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Centramos todo
        CenterEverything()

        LblPumpName.Text = FracData.SelectedPumps("SelectedPump_" + currentFrac.ToString()).Item("Name")
        LblrTransBomba.Text = FracData.SelectedPumps("SelectedPump_" + currentFrac.ToString()).Item("rTransBomba")
        LblrGalRev.Text = FracData.SelectedPumps("SelectedPump_" + currentFrac.ToString()).Item("rGalRev").ToString + " [GAL/REV]"
    End Sub

    ''Sub para centrar las cosas
    Private Sub CenterEverything()
        CenterObject(Title)
    End Sub
End Class