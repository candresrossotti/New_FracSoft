﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TransParams
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LblRel4 = New System.Windows.Forms.Label()
        Me.LblRel3 = New System.Windows.Forms.Label()
        Me.LblRel2 = New System.Windows.Forms.Label()
        Me.LblRel1 = New System.Windows.Forms.Label()
        Me.LblGears = New System.Windows.Forms.Label()
        Me.LblTransName = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.LblRel8 = New System.Windows.Forms.Label()
        Me.LblRel7 = New System.Windows.Forms.Label()
        Me.LblRel6 = New System.Windows.Forms.Label()
        Me.LblRel5 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.LblRel9 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblRel4
        '
        Me.LblRel4.AutoSize = True
        Me.LblRel4.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel4.ForeColor = System.Drawing.Color.White
        Me.LblRel4.Location = New System.Drawing.Point(118, 272)
        Me.LblRel4.Name = "LblRel4"
        Me.LblRel4.Size = New System.Drawing.Size(41, 22)
        Me.LblRel4.TabIndex = 194
        Me.LblRel4.Text = "rel4"
        '
        'LblRel3
        '
        Me.LblRel3.AutoSize = True
        Me.LblRel3.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel3.ForeColor = System.Drawing.Color.White
        Me.LblRel3.Location = New System.Drawing.Point(118, 228)
        Me.LblRel3.Name = "LblRel3"
        Me.LblRel3.Size = New System.Drawing.Size(39, 22)
        Me.LblRel3.TabIndex = 193
        Me.LblRel3.Text = "rel3"
        '
        'LblRel2
        '
        Me.LblRel2.AutoSize = True
        Me.LblRel2.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel2.ForeColor = System.Drawing.Color.White
        Me.LblRel2.Location = New System.Drawing.Point(118, 184)
        Me.LblRel2.Name = "LblRel2"
        Me.LblRel2.Size = New System.Drawing.Size(39, 22)
        Me.LblRel2.TabIndex = 192
        Me.LblRel2.Text = "rel2"
        '
        'LblRel1
        '
        Me.LblRel1.AutoSize = True
        Me.LblRel1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel1.ForeColor = System.Drawing.Color.White
        Me.LblRel1.Location = New System.Drawing.Point(118, 140)
        Me.LblRel1.Name = "LblRel1"
        Me.LblRel1.Size = New System.Drawing.Size(36, 22)
        Me.LblRel1.TabIndex = 191
        Me.LblRel1.Text = "rel1"
        '
        'LblGears
        '
        Me.LblGears.AutoSize = True
        Me.LblGears.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblGears.ForeColor = System.Drawing.Color.White
        Me.LblGears.Location = New System.Drawing.Point(118, 96)
        Me.LblGears.Name = "LblGears"
        Me.LblGears.Size = New System.Drawing.Size(79, 22)
        Me.LblGears.TabIndex = 190
        Me.LblGears.Text = "marchas"
        '
        'LblTransName
        '
        Me.LblTransName.AutoSize = True
        Me.LblTransName.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTransName.ForeColor = System.Drawing.Color.White
        Me.LblTransName.Location = New System.Drawing.Point(118, 52)
        Me.LblTransName.Name = "LblTransName"
        Me.LblTransName.Size = New System.Drawing.Size(49, 22)
        Me.LblTransName.TabIndex = 189
        Me.LblTransName.Text = "trans"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(15, 272)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(97, 22)
        Me.Label6.TabIndex = 188
        Me.Label6.Text = "Relacion 4:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(17, 228)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 22)
        Me.Label5.TabIndex = 187
        Me.Label5.Text = "Relacion 3:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(17, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 22)
        Me.Label4.TabIndex = 186
        Me.Label4.Text = "Relacion 2:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(20, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 22)
        Me.Label3.TabIndex = 185
        Me.Label3.Text = "Relacion 1:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(32, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 22)
        Me.Label1.TabIndex = 183
        Me.Label1.Text = "Nombre:"
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(330, 38)
        Me.TitlePanel.TabIndex = 182
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(57, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(217, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Parametros motor"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(282, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(32, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 22)
        Me.Label2.TabIndex = 184
        Me.Label2.Text = "Marchas:"
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'LblRel8
        '
        Me.LblRel8.AutoSize = True
        Me.LblRel8.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel8.ForeColor = System.Drawing.Color.White
        Me.LblRel8.Location = New System.Drawing.Point(118, 448)
        Me.LblRel8.Name = "LblRel8"
        Me.LblRel8.Size = New System.Drawing.Size(40, 22)
        Me.LblRel8.TabIndex = 202
        Me.LblRel8.Text = "rel8"
        '
        'LblRel7
        '
        Me.LblRel7.AutoSize = True
        Me.LblRel7.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel7.ForeColor = System.Drawing.Color.White
        Me.LblRel7.Location = New System.Drawing.Point(118, 404)
        Me.LblRel7.Name = "LblRel7"
        Me.LblRel7.Size = New System.Drawing.Size(39, 22)
        Me.LblRel7.TabIndex = 201
        Me.LblRel7.Text = "rel7"
        '
        'LblRel6
        '
        Me.LblRel6.AutoSize = True
        Me.LblRel6.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel6.ForeColor = System.Drawing.Color.White
        Me.LblRel6.Location = New System.Drawing.Point(118, 360)
        Me.LblRel6.Name = "LblRel6"
        Me.LblRel6.Size = New System.Drawing.Size(40, 22)
        Me.LblRel6.TabIndex = 200
        Me.LblRel6.Text = "rel6"
        '
        'LblRel5
        '
        Me.LblRel5.AutoSize = True
        Me.LblRel5.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel5.ForeColor = System.Drawing.Color.White
        Me.LblRel5.Location = New System.Drawing.Point(118, 316)
        Me.LblRel5.Name = "LblRel5"
        Me.LblRel5.Size = New System.Drawing.Size(39, 22)
        Me.LblRel5.TabIndex = 199
        Me.LblRel5.Text = "rel5"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(16, 448)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 22)
        Me.Label11.TabIndex = 198
        Me.Label11.Text = "Relacion 8:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(17, 404)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(95, 22)
        Me.Label12.TabIndex = 197
        Me.Label12.Text = "Relacion 7:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(16, 360)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(96, 22)
        Me.Label13.TabIndex = 196
        Me.Label13.Text = "Relacion 6:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(17, 316)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(95, 22)
        Me.Label14.TabIndex = 195
        Me.Label14.Text = "Relacion 5:"
        '
        'LblRel9
        '
        Me.LblRel9.AutoSize = True
        Me.LblRel9.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblRel9.ForeColor = System.Drawing.Color.White
        Me.LblRel9.Location = New System.Drawing.Point(118, 492)
        Me.LblRel9.Name = "LblRel9"
        Me.LblRel9.Size = New System.Drawing.Size(40, 22)
        Me.LblRel9.TabIndex = 204
        Me.LblRel9.Text = "rel9"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(16, 492)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(96, 22)
        Me.Label16.TabIndex = 203
        Me.Label16.Text = "Relacion 9:"
        '
        'TransParams
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(330, 528)
        Me.Controls.Add(Me.LblRel9)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.LblRel8)
        Me.Controls.Add(Me.LblRel7)
        Me.Controls.Add(Me.LblRel6)
        Me.Controls.Add(Me.LblRel5)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.LblRel4)
        Me.Controls.Add(Me.LblRel3)
        Me.Controls.Add(Me.LblRel2)
        Me.Controls.Add(Me.LblRel1)
        Me.Controls.Add(Me.LblGears)
        Me.Controls.Add(Me.LblTransName)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "TransParams"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TransParams"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblRel4 As Label
    Friend WithEvents LblRel3 As Label
    Friend WithEvents LblRel2 As Label
    Friend WithEvents LblRel1 As Label
    Friend WithEvents LblGears As Label
    Friend WithEvents LblTransName As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Label2 As Label
    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents LblRel9 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents LblRel8 As Label
    Friend WithEvents LblRel7 As Label
    Friend WithEvents LblRel6 As Label
    Friend WithEvents LblRel5 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
End Class
