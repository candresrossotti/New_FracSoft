﻿Public Class MaximizeForm
    Private Sub MaximizeLoad(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        Dim C1 As New OneFracMaximize
        Controls.Add(C1)
        C1.Location = New Point(0, 23)
        C1.Show()
        LSelect.MaximizeLanguageSet(C1)
        Title1.Text = CurrentFracName
    End Sub
End Class