﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class OneFracMaximize
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OneFracMaximize))
        Me.MaximizeTabControl = New Guna.UI2.WinForms.Guna2TabControl()
        Me.General = New System.Windows.Forms.TabPage()
        Me.BtnDiagnostico = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnStartEngine = New Guna.UI2.WinForms.Guna2Button()
        Me.PbAutomatic = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.BtnErrores = New Guna.UI2.WinForms.Guna2Button()
        Me.LblErrorAdmisible = New System.Windows.Forms.Label()
        Me.LblErrorGrave = New System.Windows.Forms.Label()
        Me.Guna2Panel12 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.LblTOp1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel12 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTOp = New System.Windows.Forms.Label()
        Me.Guna2Panel10 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.LblCaudalActualG = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel10 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCaudal = New System.Windows.Forms.Label()
        Me.BtnFC = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAjustes = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel7 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.LblBHHPG = New System.Windows.Forms.Label()
        Me.LblBombaHHPGeneral = New System.Windows.Forms.Label()
        Me.LblBCargaMotorG = New System.Windows.Forms.Label()
        Me.LblBPresSuccionG = New System.Windows.Forms.Label()
        Me.LblBombaCargaMotorGeneral = New System.Windows.Forms.Label()
        Me.LblBombaPresGeneral = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.LblBHorasMotorG = New System.Windows.Forms.Label()
        Me.LblBombaHMGeneral = New System.Windows.Forms.Label()
        Me.LblBPresLUBG = New System.Windows.Forms.Label()
        Me.LblBTempLUBG = New System.Windows.Forms.Label()
        Me.LblBombaPres1General = New System.Windows.Forms.Label()
        Me.LblBombaTempGeneral = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel7 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblBombaGeneral = New System.Windows.Forms.Label()
        Me.Guna2Panel5 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.LblTRpmOutG = New System.Windows.Forms.Label()
        Me.LblTransRPMGeneral = New System.Windows.Forms.Label()
        Me.LblTPresionG = New System.Windows.Forms.Label()
        Me.LblTTempG = New System.Windows.Forms.Label()
        Me.LblTransPresionGeneral = New System.Windows.Forms.Label()
        Me.LblTransTempGeneral = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel5 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTransmisionGeneral = New System.Windows.Forms.Label()
        Me.BtnMinusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPlusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.LblMCombustibleP100G = New System.Windows.Forms.Label()
        Me.LblMotorCombustible2General = New System.Windows.Forms.Label()
        Me.LblMCombustibleG = New System.Windows.Forms.Label()
        Me.LblMTempAceiteG = New System.Windows.Forms.Label()
        Me.LblMotorCombustible1General = New System.Windows.Forms.Label()
        Me.LblMotorTempAceiteGeneral = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.LblMVoltajeG = New System.Windows.Forms.Label()
        Me.LblMotorVoltajeGeneral = New System.Windows.Forms.Label()
        Me.LblMPresionG = New System.Windows.Forms.Label()
        Me.LblMTempAguaG = New System.Windows.Forms.Label()
        Me.LblMotorPresionGeneral = New System.Windows.Forms.Label()
        Me.LblMotorTempAguaGeneral = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMotorGeneral = New System.Windows.Forms.Label()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMarchaActualG = New System.Windows.Forms.Label()
        Me.LblMarchaDeseadaG = New System.Windows.Forms.Label()
        Me.LblMarcha3 = New System.Windows.Forms.Label()
        Me.LblMarcha1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMarcha = New System.Windows.Forms.Label()
        Me.PresionPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnTripZero = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnTrip = New Guna.UI2.WinForms.Guna2Button()
        Me.LblPCorte1 = New System.Windows.Forms.Label()
        Me.LblPCorteActualG = New System.Windows.Forms.Label()
        Me.LblPCorteDeseadaG = New System.Windows.Forms.Label()
        Me.LblPCorte3 = New System.Windows.Forms.Label()
        Me.RgPresionG = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.RPMPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnIdle = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblRPMDeseadaG = New System.Windows.Forms.Label()
        Me.LblRPM3 = New System.Windows.Forms.Label()
        Me.BtnRpmMinus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblRPMActualG = New System.Windows.Forms.Label()
        Me.LblRPM1 = New System.Windows.Forms.Label()
        Me.BtnRpmPlus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmMinus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.RgRPMG = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Motor = New System.Windows.Forms.TabPage()
        Me.Title1 = New System.Windows.Forms.Label()
        Me.PanelDiagM = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgressM2 = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.LblMHoras = New System.Windows.Forms.Label()
        Me.PbMHorasMotor = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.Guna2CustomGradientPanel6 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblDiagnosticoMotor = New System.Windows.Forms.Label()
        Me.LblMCombustible = New System.Windows.Forms.Label()
        Me.PbMCombustible = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblMTempAceite = New System.Windows.Forms.Label()
        Me.PbMTempAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblMPresAceite = New System.Windows.Forms.Label()
        Me.PbMPresAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblMTempAgua = New System.Windows.Forms.Label()
        Me.PbMTempAgua = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PanelErroresM = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgressM1 = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.BtnBorrarActualM = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnSiguienteM = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAnteriorM = New Guna.UI2.WinForms.Guna2Button()
        Me.LblDescripcionM = New System.Windows.Forms.Label()
        Me.LblInfoDescripM = New System.Windows.Forms.Label()
        Me.LblUnidadM = New System.Windows.Forms.Label()
        Me.LblInfoUnidadM = New System.Windows.Forms.Label()
        Me.LblActivoM = New System.Windows.Forms.Label()
        Me.LblInfoActivoM = New System.Windows.Forms.Label()
        Me.LblErroresTotalesM = New System.Windows.Forms.Label()
        Me.LblErrorNM = New System.Windows.Forms.Label()
        Me.LblOCM = New System.Windows.Forms.Label()
        Me.LblFMIM = New System.Windows.Forms.Label()
        Me.LblSPNM = New System.Windows.Forms.Label()
        Me.LblInfoErrTotM = New System.Windows.Forms.Label()
        Me.LblInfoErrorNM = New System.Windows.Forms.Label()
        Me.LblInfoOCM = New System.Windows.Forms.Label()
        Me.LblInfoFMIM = New System.Windows.Forms.Label()
        Me.LblInfoSPNM = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel8 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblErroresMotor = New System.Windows.Forms.Label()
        Me.CpbHorasMotorM = New Guna.UI2.WinForms.Guna2CircleProgressBar()
        Me.LblBHorasMotorM = New System.Windows.Forms.Label()
        Me.LblBombaHorasMotorMotor = New System.Windows.Forms.Label()
        Me.LblUnidad1M = New System.Windows.Forms.Label()
        Me.CpbCombustibleM = New Guna.UI2.WinForms.Guna2CircleProgressBar()
        Me.LblMotorCombustible1Motor = New System.Windows.Forms.Label()
        Me.LblMCombustibleM = New System.Windows.Forms.Label()
        Me.LblUnidad2M = New System.Windows.Forms.Label()
        Me.CpbVoltajeM = New Guna.UI2.WinForms.Guna2CircleProgressBar()
        Me.LblMotorVoltajeMotor = New System.Windows.Forms.Label()
        Me.LblMVoltajeM = New System.Windows.Forms.Label()
        Me.LblUnidad3M = New System.Windows.Forms.Label()
        Me.CpbPresionM = New Guna.UI2.WinForms.Guna2CircleProgressBar()
        Me.LblMotorPresionMotor = New System.Windows.Forms.Label()
        Me.LblMPresionM = New System.Windows.Forms.Label()
        Me.LblUnidad4M = New System.Windows.Forms.Label()
        Me.PanelTempAguaM = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMotorTempAgua = New System.Windows.Forms.Label()
        Me.LblMTempAguaM = New System.Windows.Forms.Label()
        Me.LblMUnidad1 = New System.Windows.Forms.Label()
        Me.RgTempAguaM = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelTempAceiteM = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMotorTempAceite = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.LblMTempAceiteM = New System.Windows.Forms.Label()
        Me.RgTempAceiteM = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelCombustibleM = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMotorCombustibleMotor = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.LblMCombustibleP100M = New System.Windows.Forms.Label()
        Me.RgCombP100M = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelCargaM = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.LblBCargaMotorM = New System.Windows.Forms.Label()
        Me.LblMotorCargaMotor = New System.Windows.Forms.Label()
        Me.RgCargaM = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelRPMM = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblRPM2 = New System.Windows.Forms.Label()
        Me.LblRPMActualM = New System.Windows.Forms.Label()
        Me.LblRPM1M = New System.Windows.Forms.Label()
        Me.LblRPMDeseadaM = New System.Windows.Forms.Label()
        Me.RgRPMM = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Transmision = New System.Windows.Forms.TabPage()
        Me.Title2 = New System.Windows.Forms.Label()
        Me.PanelDiagT = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgressT2 = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.HSep1 = New Guna.UI2.WinForms.Guna2Separator()
        Me.PbAceiteTransmision = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblAceiteTransmision = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.LblTransmisionDiag1 = New System.Windows.Forms.Label()
        Me.LblTPresAceite = New System.Windows.Forms.Label()
        Me.LblTTempAceite = New System.Windows.Forms.Label()
        Me.PbTPresAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbTTempAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblDiagnosticoTrans = New System.Windows.Forms.Label()
        Me.PanelErroresT = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgressT1 = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.BtnBorrarActualT = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnSiguienteT = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAnteriorT = New Guna.UI2.WinForms.Guna2Button()
        Me.LblDescripcionT = New System.Windows.Forms.Label()
        Me.LblInfoDescripT = New System.Windows.Forms.Label()
        Me.LblUnidadT = New System.Windows.Forms.Label()
        Me.LblInfoUnidadT = New System.Windows.Forms.Label()
        Me.LblActivoT = New System.Windows.Forms.Label()
        Me.LblInfoActivoT = New System.Windows.Forms.Label()
        Me.LblErroresTotalesT = New System.Windows.Forms.Label()
        Me.LblErrorNT = New System.Windows.Forms.Label()
        Me.LblOCT = New System.Windows.Forms.Label()
        Me.LblFMIT = New System.Windows.Forms.Label()
        Me.LblSPNT = New System.Windows.Forms.Label()
        Me.LblInfoErrTotT = New System.Windows.Forms.Label()
        Me.LblInfoErrorNT = New System.Windows.Forms.Label()
        Me.LblInfoOCT = New System.Windows.Forms.Label()
        Me.LblInfoFMIT = New System.Windows.Forms.Label()
        Me.LblInfoSPNT = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel3 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblErroresTrans = New System.Windows.Forms.Label()
        Me.PanelRPMT = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.LblTransmisionRPMOut = New System.Windows.Forms.Label()
        Me.LblTRpmOutT = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge13 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelPresionT = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.LblTransmisionPresion = New System.Windows.Forms.Label()
        Me.LblTPresionT = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge14 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelMarchaT = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblTransmisionMarchaDeseada = New System.Windows.Forms.Label()
        Me.LblTransmisionMarchaActual = New System.Windows.Forms.Label()
        Me.LblMarchaDeseadaT = New System.Windows.Forms.Label()
        Me.LblMarchaActualT = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge15 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelTempT = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.LblTransmisionTemp = New System.Windows.Forms.Label()
        Me.LblTTempT = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge12 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Bomba = New System.Windows.Forms.TabPage()
        Me.Title3 = New System.Windows.Forms.Label()
        Me.PanelDiagB = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgressB = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.LblSPresLub = New System.Windows.Forms.Label()
        Me.PbSPresLUB = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblSTempLub = New System.Windows.Forms.Label()
        Me.PbSTempLub = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblSPresSuccion = New System.Windows.Forms.Label()
        Me.PbSPresSuccion = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblSPresDescarga = New System.Windows.Forms.Label()
        Me.PbSPresDescarga = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.Guna2CustomGradientPanel13 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblDiagnosticoBomba = New System.Windows.Forms.Label()
        Me.PanelCaudalB = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblCaudalActualB = New System.Windows.Forms.Label()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.LblBombaCaudal = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge17 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelPresion2B = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.LblBombaPresionSuccion = New System.Windows.Forms.Label()
        Me.LblBPresSuccionB = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge22 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelTempB = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblBombaTempLub = New System.Windows.Forms.Label()
        Me.LblBTempLUBB = New System.Windows.Forms.Label()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge20 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelCargaB = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblBombaCargaMotor = New System.Windows.Forms.Label()
        Me.LblBCargaMotorB = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge19 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelPresionB = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblBombaPresLub = New System.Windows.Forms.Label()
        Me.LblBPresLUBB = New System.Windows.Forms.Label()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge21 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelHHPB = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblBombaHHP = New System.Windows.Forms.Label()
        Me.LblBHHPB = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge18 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.PanelTRIPB = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblBombaPresionCorte = New System.Windows.Forms.Label()
        Me.LblPCorteDeseadaB = New System.Windows.Forms.Label()
        Me.LblBombaPresionActual = New System.Windows.Forms.Label()
        Me.LblPCorteActualB = New System.Windows.Forms.Label()
        Me.Guna2RadialGauge16 = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.TmrRead = New System.Windows.Forms.Timer(Me.components)
        Me.TmrDiagnostico = New System.Windows.Forms.Timer(Me.components)
        Me.TmrCheck = New System.Windows.Forms.Timer(Me.components)
        Me.TmrErrorCheck = New System.Windows.Forms.Timer(Me.components)
        Me.MaximizeTabControl.SuspendLayout()
        Me.General.SuspendLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2Panel12.SuspendLayout()
        Me.Guna2CustomGradientPanel12.SuspendLayout()
        Me.Guna2Panel10.SuspendLayout()
        Me.Guna2CustomGradientPanel10.SuspendLayout()
        Me.Guna2Panel7.SuspendLayout()
        Me.Guna2CustomGradientPanel7.SuspendLayout()
        Me.Guna2Panel5.SuspendLayout()
        Me.Guna2CustomGradientPanel5.SuspendLayout()
        Me.Guna2Panel4.SuspendLayout()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.PresionPanel.SuspendLayout()
        Me.RPMPanel.SuspendLayout()
        Me.Motor.SuspendLayout()
        Me.PanelDiagM.SuspendLayout()
        CType(Me.PbMHorasMotor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2CustomGradientPanel6.SuspendLayout()
        CType(Me.PbMCombustible, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbMTempAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbMPresAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbMTempAgua, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelErroresM.SuspendLayout()
        Me.Guna2CustomGradientPanel8.SuspendLayout()
        Me.CpbHorasMotorM.SuspendLayout()
        Me.CpbCombustibleM.SuspendLayout()
        Me.CpbVoltajeM.SuspendLayout()
        Me.CpbPresionM.SuspendLayout()
        Me.PanelTempAguaM.SuspendLayout()
        Me.PanelTempAceiteM.SuspendLayout()
        Me.PanelCombustibleM.SuspendLayout()
        Me.PanelCargaM.SuspendLayout()
        Me.PanelRPMM.SuspendLayout()
        Me.Transmision.SuspendLayout()
        Me.PanelDiagT.SuspendLayout()
        CType(Me.PbAceiteTransmision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbTPresAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbTTempAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        Me.PanelErroresT.SuspendLayout()
        Me.Guna2CustomGradientPanel3.SuspendLayout()
        Me.PanelRPMT.SuspendLayout()
        Me.PanelPresionT.SuspendLayout()
        Me.PanelMarchaT.SuspendLayout()
        Me.PanelTempT.SuspendLayout()
        Me.Bomba.SuspendLayout()
        Me.PanelDiagB.SuspendLayout()
        CType(Me.PbSPresLUB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbSTempLub, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbSPresSuccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbSPresDescarga, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2CustomGradientPanel13.SuspendLayout()
        Me.PanelCaudalB.SuspendLayout()
        Me.PanelPresion2B.SuspendLayout()
        Me.PanelTempB.SuspendLayout()
        Me.PanelCargaB.SuspendLayout()
        Me.PanelPresionB.SuspendLayout()
        Me.PanelHHPB.SuspendLayout()
        Me.PanelTRIPB.SuspendLayout()
        Me.SuspendLayout()
        '
        'MaximizeTabControl
        '
        Me.MaximizeTabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.MaximizeTabControl.Controls.Add(Me.General)
        Me.MaximizeTabControl.Controls.Add(Me.Motor)
        Me.MaximizeTabControl.Controls.Add(Me.Transmision)
        Me.MaximizeTabControl.Controls.Add(Me.Bomba)
        Me.MaximizeTabControl.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.MaximizeTabControl.HotTrack = True
        Me.MaximizeTabControl.ItemSize = New System.Drawing.Size(301, 40)
        Me.MaximizeTabControl.Location = New System.Drawing.Point(0, 0)
        Me.MaximizeTabControl.Margin = New System.Windows.Forms.Padding(0)
        Me.MaximizeTabControl.Name = "MaximizeTabControl"
        Me.MaximizeTabControl.Padding = New System.Drawing.Point(0, 0)
        Me.MaximizeTabControl.SelectedIndex = 0
        Me.MaximizeTabControl.ShowToolTips = True
        Me.MaximizeTabControl.Size = New System.Drawing.Size(1209, 678)
        Me.MaximizeTabControl.TabButtonHoverState.BorderColor = System.Drawing.Color.Empty
        Me.MaximizeTabControl.TabButtonHoverState.FillColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.MaximizeTabControl.TabButtonHoverState.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!)
        Me.MaximizeTabControl.TabButtonHoverState.ForeColor = System.Drawing.Color.White
        Me.MaximizeTabControl.TabButtonHoverState.InnerColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.MaximizeTabControl.TabButtonIdleState.BorderColor = System.Drawing.Color.Empty
        Me.MaximizeTabControl.TabButtonIdleState.FillColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MaximizeTabControl.TabButtonIdleState.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!)
        Me.MaximizeTabControl.TabButtonIdleState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(156, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(167, Byte), Integer))
        Me.MaximizeTabControl.TabButtonIdleState.InnerColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MaximizeTabControl.TabButtonSelectedState.BorderColor = System.Drawing.Color.Empty
        Me.MaximizeTabControl.TabButtonSelectedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(37, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.MaximizeTabControl.TabButtonSelectedState.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!)
        Me.MaximizeTabControl.TabButtonSelectedState.ForeColor = System.Drawing.Color.White
        Me.MaximizeTabControl.TabButtonSelectedState.InnerColor = System.Drawing.Color.Lime
        Me.MaximizeTabControl.TabButtonSize = New System.Drawing.Size(301, 40)
        Me.MaximizeTabControl.TabIndex = 3
        Me.MaximizeTabControl.TabMenuBackColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MaximizeTabControl.TabMenuOrientation = Guna.UI2.WinForms.TabMenuOrientation.HorizontalBottom
        '
        'General
        '
        Me.General.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.General.Controls.Add(Me.BtnDiagnostico)
        Me.General.Controls.Add(Me.BtnStartEngine)
        Me.General.Controls.Add(Me.PbAutomatic)
        Me.General.Controls.Add(Me.BtnErrores)
        Me.General.Controls.Add(Me.LblErrorAdmisible)
        Me.General.Controls.Add(Me.LblErrorGrave)
        Me.General.Controls.Add(Me.Guna2Panel12)
        Me.General.Controls.Add(Me.Guna2Panel10)
        Me.General.Controls.Add(Me.BtnAjustes)
        Me.General.Controls.Add(Me.Guna2Panel7)
        Me.General.Controls.Add(Me.Guna2Panel5)
        Me.General.Controls.Add(Me.BtnMinusGear)
        Me.General.Controls.Add(Me.BtnPlusGear)
        Me.General.Controls.Add(Me.Guna2Panel4)
        Me.General.Controls.Add(Me.Guna2Panel1)
        Me.General.Controls.Add(Me.PresionPanel)
        Me.General.Controls.Add(Me.RPMPanel)
        Me.General.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.General.Location = New System.Drawing.Point(4, 4)
        Me.General.Margin = New System.Windows.Forms.Padding(0)
        Me.General.Name = "General"
        Me.General.Size = New System.Drawing.Size(1201, 630)
        Me.General.TabIndex = 2
        Me.General.Text = "General"
        Me.General.ToolTipText = "Pantalla de parametros generales"
        '
        'BtnDiagnostico
        '
        Me.BtnDiagnostico.Animated = True
        Me.BtnDiagnostico.BorderColor = System.Drawing.Color.White
        Me.BtnDiagnostico.BorderRadius = 5
        Me.BtnDiagnostico.BorderThickness = 1
        Me.BtnDiagnostico.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDiagnostico.FillColor = System.Drawing.Color.Transparent
        Me.BtnDiagnostico.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnDiagnostico.ForeColor = System.Drawing.Color.White
        Me.BtnDiagnostico.Location = New System.Drawing.Point(1004, 514)
        Me.BtnDiagnostico.Name = "BtnDiagnostico"
        Me.BtnDiagnostico.Size = New System.Drawing.Size(138, 40)
        Me.BtnDiagnostico.TabIndex = 131
        Me.BtnDiagnostico.Text = "Diagnóstico"
        '
        'BtnStartEngine
        '
        Me.BtnStartEngine.Animated = True
        Me.BtnStartEngine.BorderColor = System.Drawing.Color.White
        Me.BtnStartEngine.BorderRadius = 5
        Me.BtnStartEngine.CheckedState.Image = Global.sFRAC.My.Resources.Resources.Engine_STOP
        Me.BtnStartEngine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStartEngine.FillColor = System.Drawing.Color.Transparent
        Me.BtnStartEngine.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnStartEngine.ForeColor = System.Drawing.Color.White
        Me.BtnStartEngine.Image = Global.sFRAC.My.Resources.Resources.Engine_START
        Me.BtnStartEngine.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnStartEngine.ImageSize = New System.Drawing.Size(59, 59)
        Me.BtnStartEngine.Location = New System.Drawing.Point(1140, 435)
        Me.BtnStartEngine.Name = "BtnStartEngine"
        Me.BtnStartEngine.Size = New System.Drawing.Size(60, 60)
        Me.BtnStartEngine.TabIndex = 130
        '
        'PbAutomatic
        '
        Me.PbAutomatic.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PbAutomatic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAutomatic.Image = Global.sFRAC.My.Resources.Resources.automatic1
        Me.PbAutomatic.ImageRotate = 0!
        Me.PbAutomatic.Location = New System.Drawing.Point(946, 435)
        Me.PbAutomatic.Name = "PbAutomatic"
        Me.PbAutomatic.Size = New System.Drawing.Size(60, 60)
        Me.PbAutomatic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAutomatic.TabIndex = 128
        Me.PbAutomatic.TabStop = False
        Me.PbAutomatic.Visible = False
        '
        'BtnErrores
        '
        Me.BtnErrores.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnErrores.Animated = True
        Me.BtnErrores.BorderColor = System.Drawing.Color.White
        Me.BtnErrores.BorderRadius = 5
        Me.BtnErrores.BorderThickness = 1
        Me.BtnErrores.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnErrores.FillColor = System.Drawing.Color.Transparent
        Me.BtnErrores.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnErrores.ForeColor = System.Drawing.Color.White
        Me.BtnErrores.Location = New System.Drawing.Point(666, 3)
        Me.BtnErrores.Name = "BtnErrores"
        Me.BtnErrores.Size = New System.Drawing.Size(92, 23)
        Me.BtnErrores.TabIndex = 124
        Me.BtnErrores.Text = "Errores"
        Me.BtnErrores.Visible = False
        '
        'LblErrorAdmisible
        '
        Me.LblErrorAdmisible.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblErrorAdmisible.AutoSize = True
        Me.LblErrorAdmisible.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErrorAdmisible.ForeColor = System.Drawing.Color.Orange
        Me.LblErrorAdmisible.Location = New System.Drawing.Point(541, 5)
        Me.LblErrorAdmisible.Name = "LblErrorAdmisible"
        Me.LblErrorAdmisible.Size = New System.Drawing.Size(119, 20)
        Me.LblErrorAdmisible.TabIndex = 126
        Me.LblErrorAdmisible.Text = "Error Admisible"
        Me.LblErrorAdmisible.Visible = False
        '
        'LblErrorGrave
        '
        Me.LblErrorGrave.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblErrorGrave.AutoSize = True
        Me.LblErrorGrave.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErrorGrave.ForeColor = System.Drawing.Color.Red
        Me.LblErrorGrave.Location = New System.Drawing.Point(449, 5)
        Me.LblErrorGrave.Name = "LblErrorGrave"
        Me.LblErrorGrave.Size = New System.Drawing.Size(89, 20)
        Me.LblErrorGrave.TabIndex = 115
        Me.LblErrorGrave.Text = "Error Grave"
        Me.LblErrorGrave.Visible = False
        '
        'Guna2Panel12
        '
        Me.Guna2Panel12.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel12.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel12.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel12.BorderRadius = 6
        Me.Guna2Panel12.BorderThickness = 1
        Me.Guna2Panel12.Controls.Add(Me.Label32)
        Me.Guna2Panel12.Controls.Add(Me.LblTOp1)
        Me.Guna2Panel12.Controls.Add(Me.Guna2CustomGradientPanel12)
        Me.Guna2Panel12.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel12.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel12.Location = New System.Drawing.Point(946, 567)
        Me.Guna2Panel12.Name = "Guna2Panel12"
        Me.Guna2Panel12.Size = New System.Drawing.Size(255, 63)
        Me.Guna2Panel12.TabIndex = 117
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label32.ForeColor = System.Drawing.Color.White
        Me.Label32.Location = New System.Drawing.Point(191, 36)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(35, 20)
        Me.Label32.TabIndex = 23
        Me.Label32.Text = "Min"
        '
        'LblTOp1
        '
        Me.LblTOp1.AutoSize = True
        Me.LblTOp1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTOp1.ForeColor = System.Drawing.Color.White
        Me.LblTOp1.Location = New System.Drawing.Point(123, 36)
        Me.LblTOp1.Name = "LblTOp1"
        Me.LblTOp1.Size = New System.Drawing.Size(26, 20)
        Me.LblTOp1.TabIndex = 22
        Me.LblTOp1.Text = "20"
        '
        'Guna2CustomGradientPanel12
        '
        Me.Guna2CustomGradientPanel12.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel12.BorderRadius = 6
        Me.Guna2CustomGradientPanel12.Controls.Add(Me.LblTOp)
        Me.Guna2CustomGradientPanel12.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel12.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel12.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel12.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel12.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel12.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel12.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel12.Name = "Guna2CustomGradientPanel12"
        Me.Guna2CustomGradientPanel12.Size = New System.Drawing.Size(255, 26)
        Me.Guna2CustomGradientPanel12.TabIndex = 19
        '
        'LblTOp
        '
        Me.LblTOp.AutoSize = True
        Me.LblTOp.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTOp.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTOp.Location = New System.Drawing.Point(66, 4)
        Me.LblTOp.Name = "LblTOp"
        Me.LblTOp.Size = New System.Drawing.Size(159, 20)
        Me.LblTOp.TabIndex = 19
        Me.LblTOp.Text = "Tiempo de Operación"
        '
        'Guna2Panel10
        '
        Me.Guna2Panel10.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.BorderRadius = 6
        Me.Guna2Panel10.BorderThickness = 1
        Me.Guna2Panel10.Controls.Add(Me.Label81)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudalActualG)
        Me.Guna2Panel10.Controls.Add(Me.Guna2CustomGradientPanel10)
        Me.Guna2Panel10.Controls.Add(Me.BtnFC)
        Me.Guna2Panel10.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.Location = New System.Drawing.Point(481, 57)
        Me.Guna2Panel10.Name = "Guna2Panel10"
        Me.Guna2Panel10.Size = New System.Drawing.Size(238, 179)
        Me.Guna2Panel10.TabIndex = 121
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.Label81.ForeColor = System.Drawing.Color.White
        Me.Label81.Location = New System.Drawing.Point(164, 59)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(61, 19)
        Me.Label81.TabIndex = 23
        Me.Label81.Text = "BBL/Min"
        '
        'LblCaudalActualG
        '
        Me.LblCaudalActualG.AutoSize = True
        Me.LblCaudalActualG.Font = New System.Drawing.Font("Montserrat", 30.0!)
        Me.LblCaudalActualG.ForeColor = System.Drawing.Color.White
        Me.LblCaudalActualG.Location = New System.Drawing.Point(68, 41)
        Me.LblCaudalActualG.Name = "LblCaudalActualG"
        Me.LblCaudalActualG.Size = New System.Drawing.Size(86, 55)
        Me.LblCaudalActualG.TabIndex = 22
        Me.LblCaudalActualG.Text = "10.1"
        '
        'Guna2CustomGradientPanel10
        '
        Me.Guna2CustomGradientPanel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.BorderRadius = 6
        Me.Guna2CustomGradientPanel10.Controls.Add(Me.LblCaudal)
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel10.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel10.Name = "Guna2CustomGradientPanel10"
        Me.Guna2CustomGradientPanel10.Size = New System.Drawing.Size(238, 26)
        Me.Guna2CustomGradientPanel10.TabIndex = 19
        '
        'LblCaudal
        '
        Me.LblCaudal.AutoSize = True
        Me.LblCaudal.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCaudal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCaudal.Location = New System.Drawing.Point(97, 4)
        Me.LblCaudal.Name = "LblCaudal"
        Me.LblCaudal.Size = New System.Drawing.Size(57, 20)
        Me.LblCaudal.TabIndex = 19
        Me.LblCaudal.Text = "Caudal"
        '
        'BtnFC
        '
        Me.BtnFC.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnFC.Animated = True
        Me.BtnFC.BorderColor = System.Drawing.Color.White
        Me.BtnFC.BorderRadius = 5
        Me.BtnFC.BorderThickness = 1
        Me.BtnFC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFC.FillColor = System.Drawing.Color.Transparent
        Me.BtnFC.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnFC.ForeColor = System.Drawing.Color.White
        Me.BtnFC.Location = New System.Drawing.Point(96, 121)
        Me.BtnFC.Name = "BtnFC"
        Me.BtnFC.Size = New System.Drawing.Size(52, 40)
        Me.BtnFC.TabIndex = 123
        Me.BtnFC.Text = "FC"
        '
        'BtnAjustes
        '
        Me.BtnAjustes.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnAjustes.Animated = True
        Me.BtnAjustes.BorderColor = System.Drawing.Color.White
        Me.BtnAjustes.BorderRadius = 5
        Me.BtnAjustes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAjustes.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAjustes.ForeColor = System.Drawing.Color.White
        Me.BtnAjustes.Image = Global.sFRAC.My.Resources.Resources.settings_1
        Me.BtnAjustes.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnAjustes.ImageSize = New System.Drawing.Size(59, 59)
        Me.BtnAjustes.Location = New System.Drawing.Point(1043, 435)
        Me.BtnAjustes.Name = "BtnAjustes"
        Me.BtnAjustes.Size = New System.Drawing.Size(60, 60)
        Me.BtnAjustes.TabIndex = 125
        '
        'Guna2Panel7
        '
        Me.Guna2Panel7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel7.BorderRadius = 6
        Me.Guna2Panel7.BorderThickness = 1
        Me.Guna2Panel7.Controls.Add(Me.Label54)
        Me.Guna2Panel7.Controls.Add(Me.Label55)
        Me.Guna2Panel7.Controls.Add(Me.Label56)
        Me.Guna2Panel7.Controls.Add(Me.LblBHHPG)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaHHPGeneral)
        Me.Guna2Panel7.Controls.Add(Me.LblBCargaMotorG)
        Me.Guna2Panel7.Controls.Add(Me.LblBPresSuccionG)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaCargaMotorGeneral)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaPresGeneral)
        Me.Guna2Panel7.Controls.Add(Me.Label63)
        Me.Guna2Panel7.Controls.Add(Me.Label64)
        Me.Guna2Panel7.Controls.Add(Me.Label65)
        Me.Guna2Panel7.Controls.Add(Me.LblBHorasMotorG)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaHMGeneral)
        Me.Guna2Panel7.Controls.Add(Me.LblBPresLUBG)
        Me.Guna2Panel7.Controls.Add(Me.LblBTempLUBG)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaPres1General)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaTempGeneral)
        Me.Guna2Panel7.Controls.Add(Me.Guna2CustomGradientPanel7)
        Me.Guna2Panel7.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel7.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Guna2Panel7.Location = New System.Drawing.Point(631, 431)
        Me.Guna2Panel7.Name = "Guna2Panel7"
        Me.Guna2Panel7.Size = New System.Drawing.Size(309, 199)
        Me.Guna2Panel7.TabIndex = 118
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(225, 175)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(41, 20)
        Me.Label54.TabIndex = 37
        Me.Label54.Text = "HHP"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(225, 117)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(21, 20)
        Me.Label55.TabIndex = 36
        Me.Label55.Text = "%"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label56.ForeColor = System.Drawing.Color.White
        Me.Label56.Location = New System.Drawing.Point(225, 58)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(32, 20)
        Me.Label56.TabIndex = 35
        Me.Label56.Text = "PSI"
        '
        'LblBHHPG
        '
        Me.LblBHHPG.AutoSize = True
        Me.LblBHHPG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBHHPG.ForeColor = System.Drawing.Color.White
        Me.LblBHHPG.Location = New System.Drawing.Point(180, 175)
        Me.LblBHHPG.Name = "LblBHHPG"
        Me.LblBHHPG.Size = New System.Drawing.Size(39, 20)
        Me.LblBHHPG.TabIndex = 34
        Me.LblBHHPG.Text = "1457"
        '
        'LblBombaHHPGeneral
        '
        Me.LblBombaHHPGeneral.AutoSize = True
        Me.LblBombaHHPGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaHHPGeneral.ForeColor = System.Drawing.Color.White
        Me.LblBombaHHPGeneral.Location = New System.Drawing.Point(180, 146)
        Me.LblBombaHHPGeneral.Name = "LblBombaHHPGeneral"
        Me.LblBombaHHPGeneral.Size = New System.Drawing.Size(41, 20)
        Me.LblBombaHHPGeneral.TabIndex = 33
        Me.LblBombaHHPGeneral.Text = "HHP"
        '
        'LblBCargaMotorG
        '
        Me.LblBCargaMotorG.AutoSize = True
        Me.LblBCargaMotorG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBCargaMotorG.ForeColor = System.Drawing.Color.White
        Me.LblBCargaMotorG.Location = New System.Drawing.Point(180, 117)
        Me.LblBCargaMotorG.Name = "LblBCargaMotorG"
        Me.LblBCargaMotorG.Size = New System.Drawing.Size(27, 20)
        Me.LblBCargaMotorG.TabIndex = 32
        Me.LblBCargaMotorG.Text = "68"
        '
        'LblBPresSuccionG
        '
        Me.LblBPresSuccionG.AutoSize = True
        Me.LblBPresSuccionG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBPresSuccionG.ForeColor = System.Drawing.Color.White
        Me.LblBPresSuccionG.Location = New System.Drawing.Point(180, 58)
        Me.LblBPresSuccionG.Name = "LblBPresSuccionG"
        Me.LblBPresSuccionG.Size = New System.Drawing.Size(18, 20)
        Me.LblBPresSuccionG.TabIndex = 31
        Me.LblBPresSuccionG.Text = "0"
        '
        'LblBombaCargaMotorGeneral
        '
        Me.LblBombaCargaMotorGeneral.AutoSize = True
        Me.LblBombaCargaMotorGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaCargaMotorGeneral.ForeColor = System.Drawing.Color.White
        Me.LblBombaCargaMotorGeneral.Location = New System.Drawing.Point(180, 86)
        Me.LblBombaCargaMotorGeneral.Name = "LblBombaCargaMotorGeneral"
        Me.LblBombaCargaMotorGeneral.Size = New System.Drawing.Size(114, 20)
        Me.LblBombaCargaMotorGeneral.TabIndex = 30
        Me.LblBombaCargaMotorGeneral.Text = "% Carga Motor"
        '
        'LblBombaPresGeneral
        '
        Me.LblBombaPresGeneral.AutoSize = True
        Me.LblBombaPresGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresGeneral.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresGeneral.Location = New System.Drawing.Point(180, 29)
        Me.LblBombaPresGeneral.Name = "LblBombaPresGeneral"
        Me.LblBombaPresGeneral.Size = New System.Drawing.Size(122, 20)
        Me.LblBombaPresGeneral.TabIndex = 29
        Me.LblBombaPresGeneral.Text = "Presión Succión"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label63.ForeColor = System.Drawing.Color.White
        Me.Label63.Location = New System.Drawing.Point(54, 175)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(26, 20)
        Me.Label63.TabIndex = 28
        Me.Label63.Text = "Hr"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(54, 117)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(32, 20)
        Me.Label64.TabIndex = 27
        Me.Label64.Text = "PSI"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(54, 58)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(25, 20)
        Me.Label65.TabIndex = 26
        Me.Label65.Text = "ºC"
        '
        'LblBHorasMotorG
        '
        Me.LblBHorasMotorG.AutoSize = True
        Me.LblBHorasMotorG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBHorasMotorG.ForeColor = System.Drawing.Color.White
        Me.LblBHorasMotorG.Location = New System.Drawing.Point(9, 175)
        Me.LblBHorasMotorG.Name = "LblBHorasMotorG"
        Me.LblBHorasMotorG.Size = New System.Drawing.Size(32, 20)
        Me.LblBHorasMotorG.TabIndex = 25
        Me.LblBHorasMotorG.Text = "100"
        '
        'LblBombaHMGeneral
        '
        Me.LblBombaHMGeneral.AutoSize = True
        Me.LblBombaHMGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaHMGeneral.ForeColor = System.Drawing.Color.White
        Me.LblBombaHMGeneral.Location = New System.Drawing.Point(9, 146)
        Me.LblBombaHMGeneral.Name = "LblBombaHMGeneral"
        Me.LblBombaHMGeneral.Size = New System.Drawing.Size(117, 20)
        Me.LblBombaHMGeneral.TabIndex = 24
        Me.LblBombaHMGeneral.Text = "Horas de Motor"
        '
        'LblBPresLUBG
        '
        Me.LblBPresLUBG.AutoSize = True
        Me.LblBPresLUBG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBPresLUBG.ForeColor = System.Drawing.Color.White
        Me.LblBPresLUBG.Location = New System.Drawing.Point(9, 117)
        Me.LblBPresLUBG.Name = "LblBPresLUBG"
        Me.LblBPresLUBG.Size = New System.Drawing.Size(18, 20)
        Me.LblBPresLUBG.TabIndex = 23
        Me.LblBPresLUBG.Text = "0"
        '
        'LblBTempLUBG
        '
        Me.LblBTempLUBG.AutoSize = True
        Me.LblBTempLUBG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBTempLUBG.ForeColor = System.Drawing.Color.White
        Me.LblBTempLUBG.Location = New System.Drawing.Point(9, 58)
        Me.LblBTempLUBG.Name = "LblBTempLUBG"
        Me.LblBTempLUBG.Size = New System.Drawing.Size(25, 20)
        Me.LblBTempLUBG.TabIndex = 22
        Me.LblBTempLUBG.Text = "74"
        '
        'LblBombaPres1General
        '
        Me.LblBombaPres1General.AutoSize = True
        Me.LblBombaPres1General.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPres1General.ForeColor = System.Drawing.Color.White
        Me.LblBombaPres1General.Location = New System.Drawing.Point(9, 86)
        Me.LblBombaPres1General.Name = "LblBombaPres1General"
        Me.LblBombaPres1General.Size = New System.Drawing.Size(96, 20)
        Me.LblBombaPres1General.TabIndex = 21
        Me.LblBombaPres1General.Text = "Presión LUB"
        '
        'LblBombaTempGeneral
        '
        Me.LblBombaTempGeneral.AutoSize = True
        Me.LblBombaTempGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaTempGeneral.ForeColor = System.Drawing.Color.White
        Me.LblBombaTempGeneral.Location = New System.Drawing.Point(9, 29)
        Me.LblBombaTempGeneral.Name = "LblBombaTempGeneral"
        Me.LblBombaTempGeneral.Size = New System.Drawing.Size(131, 20)
        Me.LblBombaTempGeneral.TabIndex = 20
        Me.LblBombaTempGeneral.Text = "Temperatura LUB"
        '
        'Guna2CustomGradientPanel7
        '
        Me.Guna2CustomGradientPanel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.BorderRadius = 6
        Me.Guna2CustomGradientPanel7.Controls.Add(Me.LblBombaGeneral)
        Me.Guna2CustomGradientPanel7.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel7.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel7.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel7.Name = "Guna2CustomGradientPanel7"
        Me.Guna2CustomGradientPanel7.Size = New System.Drawing.Size(309, 26)
        Me.Guna2CustomGradientPanel7.TabIndex = 19
        '
        'LblBombaGeneral
        '
        Me.LblBombaGeneral.AutoSize = True
        Me.LblBombaGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaGeneral.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblBombaGeneral.Location = New System.Drawing.Point(123, 4)
        Me.LblBombaGeneral.Name = "LblBombaGeneral"
        Me.LblBombaGeneral.Size = New System.Drawing.Size(61, 20)
        Me.LblBombaGeneral.TabIndex = 19
        Me.LblBombaGeneral.Text = "Bomba"
        '
        'Guna2Panel5
        '
        Me.Guna2Panel5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.BorderRadius = 6
        Me.Guna2Panel5.BorderThickness = 1
        Me.Guna2Panel5.Controls.Add(Me.Label41)
        Me.Guna2Panel5.Controls.Add(Me.Label42)
        Me.Guna2Panel5.Controls.Add(Me.Label43)
        Me.Guna2Panel5.Controls.Add(Me.LblTRpmOutG)
        Me.Guna2Panel5.Controls.Add(Me.LblTransRPMGeneral)
        Me.Guna2Panel5.Controls.Add(Me.LblTPresionG)
        Me.Guna2Panel5.Controls.Add(Me.LblTTempG)
        Me.Guna2Panel5.Controls.Add(Me.LblTransPresionGeneral)
        Me.Guna2Panel5.Controls.Add(Me.LblTransTempGeneral)
        Me.Guna2Panel5.Controls.Add(Me.Guna2CustomGradientPanel5)
        Me.Guna2Panel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Guna2Panel5.Location = New System.Drawing.Point(316, 431)
        Me.Guna2Panel5.Name = "Guna2Panel5"
        Me.Guna2Panel5.Size = New System.Drawing.Size(309, 199)
        Me.Guna2Panel5.TabIndex = 116
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(181, 175)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(42, 20)
        Me.Label41.TabIndex = 28
        Me.Label41.Text = "RPM"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(181, 117)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(32, 20)
        Me.Label42.TabIndex = 27
        Me.Label42.Text = "PSI"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label43.ForeColor = System.Drawing.Color.White
        Me.Label43.Location = New System.Drawing.Point(181, 58)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(25, 20)
        Me.Label43.TabIndex = 26
        Me.Label43.Text = "ºC"
        '
        'LblTRpmOutG
        '
        Me.LblTRpmOutG.AutoSize = True
        Me.LblTRpmOutG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTRpmOutG.ForeColor = System.Drawing.Color.White
        Me.LblTRpmOutG.Location = New System.Drawing.Point(116, 175)
        Me.LblTRpmOutG.Name = "LblTRpmOutG"
        Me.LblTRpmOutG.Size = New System.Drawing.Size(41, 20)
        Me.LblTRpmOutG.TabIndex = 25
        Me.LblTRpmOutG.Text = "1000"
        '
        'LblTransRPMGeneral
        '
        Me.LblTransRPMGeneral.AutoSize = True
        Me.LblTransRPMGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransRPMGeneral.ForeColor = System.Drawing.Color.White
        Me.LblTransRPMGeneral.Location = New System.Drawing.Point(116, 146)
        Me.LblTransRPMGeneral.Name = "LblTransRPMGeneral"
        Me.LblTransRPMGeneral.Size = New System.Drawing.Size(73, 20)
        Me.LblTransRPMGeneral.TabIndex = 24
        Me.LblTransRPMGeneral.Text = "RPM Out"
        '
        'LblTPresionG
        '
        Me.LblTPresionG.AutoSize = True
        Me.LblTPresionG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTPresionG.ForeColor = System.Drawing.Color.White
        Me.LblTPresionG.Location = New System.Drawing.Point(116, 117)
        Me.LblTPresionG.Name = "LblTPresionG"
        Me.LblTPresionG.Size = New System.Drawing.Size(18, 20)
        Me.LblTPresionG.TabIndex = 23
        Me.LblTPresionG.Text = "0"
        '
        'LblTTempG
        '
        Me.LblTTempG.AutoSize = True
        Me.LblTTempG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTTempG.ForeColor = System.Drawing.Color.White
        Me.LblTTempG.Location = New System.Drawing.Point(116, 58)
        Me.LblTTempG.Name = "LblTTempG"
        Me.LblTTempG.Size = New System.Drawing.Size(26, 20)
        Me.LblTTempG.TabIndex = 22
        Me.LblTTempG.Text = "70"
        '
        'LblTransPresionGeneral
        '
        Me.LblTransPresionGeneral.AutoSize = True
        Me.LblTransPresionGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransPresionGeneral.ForeColor = System.Drawing.Color.White
        Me.LblTransPresionGeneral.Location = New System.Drawing.Point(116, 86)
        Me.LblTransPresionGeneral.Name = "LblTransPresionGeneral"
        Me.LblTransPresionGeneral.Size = New System.Drawing.Size(62, 20)
        Me.LblTransPresionGeneral.TabIndex = 21
        Me.LblTransPresionGeneral.Text = "Presión"
        '
        'LblTransTempGeneral
        '
        Me.LblTransTempGeneral.AutoSize = True
        Me.LblTransTempGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransTempGeneral.ForeColor = System.Drawing.Color.White
        Me.LblTransTempGeneral.Location = New System.Drawing.Point(116, 29)
        Me.LblTransTempGeneral.Name = "LblTransTempGeneral"
        Me.LblTransTempGeneral.Size = New System.Drawing.Size(97, 20)
        Me.LblTransTempGeneral.TabIndex = 20
        Me.LblTransTempGeneral.Text = "Temperatura"
        '
        'Guna2CustomGradientPanel5
        '
        Me.Guna2CustomGradientPanel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.BorderRadius = 6
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.LblTransmisionGeneral)
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel5.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel5.Name = "Guna2CustomGradientPanel5"
        Me.Guna2CustomGradientPanel5.Size = New System.Drawing.Size(309, 26)
        Me.Guna2CustomGradientPanel5.TabIndex = 19
        '
        'LblTransmisionGeneral
        '
        Me.LblTransmisionGeneral.AutoSize = True
        Me.LblTransmisionGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransmisionGeneral.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTransmisionGeneral.Location = New System.Drawing.Point(123, 4)
        Me.LblTransmisionGeneral.Name = "LblTransmisionGeneral"
        Me.LblTransmisionGeneral.Size = New System.Drawing.Size(94, 20)
        Me.LblTransmisionGeneral.TabIndex = 19
        Me.LblTransmisionGeneral.Text = "Transmisión"
        '
        'BtnMinusGear
        '
        Me.BtnMinusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnMinusGear.Animated = True
        Me.BtnMinusGear.BorderColor = System.Drawing.Color.White
        Me.BtnMinusGear.BorderRadius = 5
        Me.BtnMinusGear.BorderThickness = 1
        Me.BtnMinusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMinusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnMinusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnMinusGear.ForeColor = System.Drawing.Color.White
        Me.BtnMinusGear.Image = CType(resources.GetObject("BtnMinusGear.Image"), System.Drawing.Image)
        Me.BtnMinusGear.Location = New System.Drawing.Point(690, 381)
        Me.BtnMinusGear.Name = "BtnMinusGear"
        Me.BtnMinusGear.Size = New System.Drawing.Size(40, 40)
        Me.BtnMinusGear.TabIndex = 120
        '
        'BtnPlusGear
        '
        Me.BtnPlusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnPlusGear.Animated = True
        Me.BtnPlusGear.BackColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.BorderColor = System.Drawing.Color.White
        Me.BtnPlusGear.BorderRadius = 5
        Me.BtnPlusGear.BorderThickness = 1
        Me.BtnPlusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPlusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnPlusGear.ForeColor = System.Drawing.Color.White
        Me.BtnPlusGear.Image = CType(resources.GetObject("BtnPlusGear.Image"), System.Drawing.Image)
        Me.BtnPlusGear.Location = New System.Drawing.Point(690, 335)
        Me.BtnPlusGear.Name = "BtnPlusGear"
        Me.BtnPlusGear.Size = New System.Drawing.Size(40, 40)
        Me.BtnPlusGear.TabIndex = 119
        '
        'Guna2Panel4
        '
        Me.Guna2Panel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.BorderRadius = 6
        Me.Guna2Panel4.BorderThickness = 1
        Me.Guna2Panel4.Controls.Add(Me.Label23)
        Me.Guna2Panel4.Controls.Add(Me.Label24)
        Me.Guna2Panel4.Controls.Add(Me.Label25)
        Me.Guna2Panel4.Controls.Add(Me.LblMCombustibleP100G)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorCombustible2General)
        Me.Guna2Panel4.Controls.Add(Me.LblMCombustibleG)
        Me.Guna2Panel4.Controls.Add(Me.LblMTempAceiteG)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorCombustible1General)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorTempAceiteGeneral)
        Me.Guna2Panel4.Controls.Add(Me.Label22)
        Me.Guna2Panel4.Controls.Add(Me.Label21)
        Me.Guna2Panel4.Controls.Add(Me.Label20)
        Me.Guna2Panel4.Controls.Add(Me.LblMVoltajeG)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorVoltajeGeneral)
        Me.Guna2Panel4.Controls.Add(Me.LblMPresionG)
        Me.Guna2Panel4.Controls.Add(Me.LblMTempAguaG)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorPresionGeneral)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorTempAguaGeneral)
        Me.Guna2Panel4.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.Guna2Panel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.Location = New System.Drawing.Point(1, 431)
        Me.Guna2Panel4.Name = "Guna2Panel4"
        Me.Guna2Panel4.Size = New System.Drawing.Size(309, 199)
        Me.Guna2Panel4.TabIndex = 114
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(253, 174)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(21, 20)
        Me.Label23.TabIndex = 37
        Me.Label23.Text = "%"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(240, 116)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(39, 20)
        Me.Label24.TabIndex = 36
        Me.Label24.Text = "L/Hr"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(249, 57)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(25, 20)
        Me.Label25.TabIndex = 35
        Me.Label25.Text = "ºC"
        '
        'LblMCombustibleP100G
        '
        Me.LblMCombustibleP100G.AutoSize = True
        Me.LblMCombustibleP100G.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMCombustibleP100G.ForeColor = System.Drawing.Color.White
        Me.LblMCombustibleP100G.Location = New System.Drawing.Point(196, 174)
        Me.LblMCombustibleP100G.Name = "LblMCombustibleP100G"
        Me.LblMCombustibleP100G.Size = New System.Drawing.Size(27, 20)
        Me.LblMCombustibleP100G.TabIndex = 34
        Me.LblMCombustibleP100G.Text = "90"
        '
        'LblMotorCombustible2General
        '
        Me.LblMotorCombustible2General.AutoSize = True
        Me.LblMotorCombustible2General.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCombustible2General.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustible2General.Location = New System.Drawing.Point(196, 145)
        Me.LblMotorCombustible2General.Name = "LblMotorCombustible2General"
        Me.LblMotorCombustible2General.Size = New System.Drawing.Size(99, 20)
        Me.LblMotorCombustible2General.TabIndex = 33
        Me.LblMotorCombustible2General.Text = "Combustible"
        '
        'LblMCombustibleG
        '
        Me.LblMCombustibleG.AutoSize = True
        Me.LblMCombustibleG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMCombustibleG.ForeColor = System.Drawing.Color.White
        Me.LblMCombustibleG.Location = New System.Drawing.Point(196, 116)
        Me.LblMCombustibleG.Name = "LblMCombustibleG"
        Me.LblMCombustibleG.Size = New System.Drawing.Size(44, 20)
        Me.LblMCombustibleG.TabIndex = 32
        Me.LblMCombustibleG.Text = "3600"
        '
        'LblMTempAceiteG
        '
        Me.LblMTempAceiteG.AutoSize = True
        Me.LblMTempAceiteG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMTempAceiteG.ForeColor = System.Drawing.Color.White
        Me.LblMTempAceiteG.Location = New System.Drawing.Point(196, 57)
        Me.LblMTempAceiteG.Name = "LblMTempAceiteG"
        Me.LblMTempAceiteG.Size = New System.Drawing.Size(25, 20)
        Me.LblMTempAceiteG.TabIndex = 31
        Me.LblMTempAceiteG.Text = "75"
        '
        'LblMotorCombustible1General
        '
        Me.LblMotorCombustible1General.AutoSize = True
        Me.LblMotorCombustible1General.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCombustible1General.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustible1General.Location = New System.Drawing.Point(196, 85)
        Me.LblMotorCombustible1General.Name = "LblMotorCombustible1General"
        Me.LblMotorCombustible1General.Size = New System.Drawing.Size(99, 20)
        Me.LblMotorCombustible1General.TabIndex = 30
        Me.LblMotorCombustible1General.Text = "Combustible"
        '
        'LblMotorTempAceiteGeneral
        '
        Me.LblMotorTempAceiteGeneral.AutoSize = True
        Me.LblMotorTempAceiteGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorTempAceiteGeneral.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAceiteGeneral.Location = New System.Drawing.Point(196, 28)
        Me.LblMotorTempAceiteGeneral.Name = "LblMotorTempAceiteGeneral"
        Me.LblMotorTempAceiteGeneral.Size = New System.Drawing.Size(96, 20)
        Me.LblMotorTempAceiteGeneral.TabIndex = 29
        Me.LblMotorTempAceiteGeneral.Text = "Temp Aceite"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(86, 174)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(19, 20)
        Me.Label22.TabIndex = 28
        Me.Label22.Text = "V"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(76, 116)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(32, 20)
        Me.Label21.TabIndex = 27
        Me.Label21.Text = "PSI"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(80, 57)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(25, 20)
        Me.Label20.TabIndex = 26
        Me.Label20.Text = "ºC"
        '
        'LblMVoltajeG
        '
        Me.LblMVoltajeG.AutoSize = True
        Me.LblMVoltajeG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMVoltajeG.ForeColor = System.Drawing.Color.White
        Me.LblMVoltajeG.Location = New System.Drawing.Point(32, 174)
        Me.LblMVoltajeG.Name = "LblMVoltajeG"
        Me.LblMVoltajeG.Size = New System.Drawing.Size(34, 20)
        Me.LblMVoltajeG.TabIndex = 25
        Me.LblMVoltajeG.Text = "24.1"
        '
        'LblMotorVoltajeGeneral
        '
        Me.LblMotorVoltajeGeneral.AutoSize = True
        Me.LblMotorVoltajeGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorVoltajeGeneral.ForeColor = System.Drawing.Color.White
        Me.LblMotorVoltajeGeneral.Location = New System.Drawing.Point(32, 145)
        Me.LblMotorVoltajeGeneral.Name = "LblMotorVoltajeGeneral"
        Me.LblMotorVoltajeGeneral.Size = New System.Drawing.Size(57, 20)
        Me.LblMotorVoltajeGeneral.TabIndex = 24
        Me.LblMotorVoltajeGeneral.Text = "Voltaje"
        '
        'LblMPresionG
        '
        Me.LblMPresionG.AutoSize = True
        Me.LblMPresionG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMPresionG.ForeColor = System.Drawing.Color.White
        Me.LblMPresionG.Location = New System.Drawing.Point(32, 116)
        Me.LblMPresionG.Name = "LblMPresionG"
        Me.LblMPresionG.Size = New System.Drawing.Size(18, 20)
        Me.LblMPresionG.TabIndex = 23
        Me.LblMPresionG.Text = "0"
        '
        'LblMTempAguaG
        '
        Me.LblMTempAguaG.AutoSize = True
        Me.LblMTempAguaG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMTempAguaG.ForeColor = System.Drawing.Color.White
        Me.LblMTempAguaG.Location = New System.Drawing.Point(32, 57)
        Me.LblMTempAguaG.Name = "LblMTempAguaG"
        Me.LblMTempAguaG.Size = New System.Drawing.Size(27, 20)
        Me.LblMTempAguaG.TabIndex = 22
        Me.LblMTempAguaG.Text = "80"
        '
        'LblMotorPresionGeneral
        '
        Me.LblMotorPresionGeneral.AutoSize = True
        Me.LblMotorPresionGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorPresionGeneral.ForeColor = System.Drawing.Color.White
        Me.LblMotorPresionGeneral.Location = New System.Drawing.Point(32, 85)
        Me.LblMotorPresionGeneral.Name = "LblMotorPresionGeneral"
        Me.LblMotorPresionGeneral.Size = New System.Drawing.Size(62, 20)
        Me.LblMotorPresionGeneral.TabIndex = 21
        Me.LblMotorPresionGeneral.Text = "Presión"
        '
        'LblMotorTempAguaGeneral
        '
        Me.LblMotorTempAguaGeneral.AutoSize = True
        Me.LblMotorTempAguaGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorTempAguaGeneral.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAguaGeneral.Location = New System.Drawing.Point(32, 28)
        Me.LblMotorTempAguaGeneral.Name = "LblMotorTempAguaGeneral"
        Me.LblMotorTempAguaGeneral.Size = New System.Drawing.Size(89, 20)
        Me.LblMotorTempAguaGeneral.TabIndex = 20
        Me.LblMotorTempAguaGeneral.Text = "Temp Agua"
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.LblMotorGeneral)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(309, 26)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'LblMotorGeneral
        '
        Me.LblMotorGeneral.AutoSize = True
        Me.LblMotorGeneral.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorGeneral.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMotorGeneral.Location = New System.Drawing.Point(123, 4)
        Me.LblMotorGeneral.Name = "LblMotorGeneral"
        Me.LblMotorGeneral.Size = New System.Drawing.Size(52, 20)
        Me.LblMotorGeneral.TabIndex = 19
        Me.LblMotorGeneral.Text = "Motor"
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaActualG)
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaDeseadaG)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha3)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(516, 265)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(168, 156)
        Me.Guna2Panel1.TabIndex = 113
        '
        'LblMarchaActualG
        '
        Me.LblMarchaActualG.AutoSize = True
        Me.LblMarchaActualG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarchaActualG.ForeColor = System.Drawing.Color.White
        Me.LblMarchaActualG.Location = New System.Drawing.Point(76, 65)
        Me.LblMarchaActualG.Name = "LblMarchaActualG"
        Me.LblMarchaActualG.Size = New System.Drawing.Size(20, 20)
        Me.LblMarchaActualG.TabIndex = 23
        Me.LblMarchaActualG.Text = "N"
        '
        'LblMarchaDeseadaG
        '
        Me.LblMarchaDeseadaG.AutoSize = True
        Me.LblMarchaDeseadaG.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblMarchaDeseadaG.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarchaDeseadaG.ForeColor = System.Drawing.Color.White
        Me.LblMarchaDeseadaG.Location = New System.Drawing.Point(76, 131)
        Me.LblMarchaDeseadaG.Name = "LblMarchaDeseadaG"
        Me.LblMarchaDeseadaG.Size = New System.Drawing.Size(20, 20)
        Me.LblMarchaDeseadaG.TabIndex = 22
        Me.LblMarchaDeseadaG.Text = "N"
        '
        'LblMarcha3
        '
        Me.LblMarcha3.AutoSize = True
        Me.LblMarcha3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha3.ForeColor = System.Drawing.Color.White
        Me.LblMarcha3.Location = New System.Drawing.Point(34, 32)
        Me.LblMarcha3.Name = "LblMarcha3"
        Me.LblMarcha3.Size = New System.Drawing.Size(110, 20)
        Me.LblMarcha3.TabIndex = 21
        Me.LblMarcha3.Text = "Marcha Actual"
        '
        'LblMarcha1
        '
        Me.LblMarcha1.AutoSize = True
        Me.LblMarcha1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha1.ForeColor = System.Drawing.Color.White
        Me.LblMarcha1.Location = New System.Drawing.Point(38, 98)
        Me.LblMarcha1.Name = "LblMarcha1"
        Me.LblMarcha1.Size = New System.Drawing.Size(125, 20)
        Me.LblMarcha1.TabIndex = 20
        Me.LblMarcha1.Text = "Marcha Deseada"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblMarcha)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(168, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblMarcha
        '
        Me.LblMarcha.AutoSize = True
        Me.LblMarcha.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMarcha.Location = New System.Drawing.Point(56, 4)
        Me.LblMarcha.Name = "LblMarcha"
        Me.LblMarcha.Size = New System.Drawing.Size(61, 20)
        Me.LblMarcha.TabIndex = 19
        Me.LblMarcha.Text = "Marcha"
        '
        'PresionPanel
        '
        Me.PresionPanel.Controls.Add(Me.BtnTripZero)
        Me.PresionPanel.Controls.Add(Me.BtnTrip)
        Me.PresionPanel.Controls.Add(Me.LblPCorte1)
        Me.PresionPanel.Controls.Add(Me.LblPCorteActualG)
        Me.PresionPanel.Controls.Add(Me.LblPCorteDeseadaG)
        Me.PresionPanel.Controls.Add(Me.LblPCorte3)
        Me.PresionPanel.Controls.Add(Me.RgPresionG)
        Me.PresionPanel.Location = New System.Drawing.Point(775, -1)
        Me.PresionPanel.Name = "PresionPanel"
        Me.PresionPanel.Size = New System.Drawing.Size(414, 414)
        Me.PresionPanel.TabIndex = 149
        '
        'BtnTripZero
        '
        Me.BtnTripZero.Animated = True
        Me.BtnTripZero.BorderColor = System.Drawing.Color.White
        Me.BtnTripZero.BorderRadius = 5
        Me.BtnTripZero.BorderThickness = 1
        Me.BtnTripZero.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTripZero.FillColor = System.Drawing.Color.Transparent
        Me.BtnTripZero.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnTripZero.ForeColor = System.Drawing.Color.White
        Me.BtnTripZero.Location = New System.Drawing.Point(152, 152)
        Me.BtnTripZero.Name = "BtnTripZero"
        Me.BtnTripZero.Size = New System.Drawing.Size(111, 40)
        Me.BtnTripZero.TabIndex = 147
        Me.BtnTripZero.Text = "ZERO"
        '
        'BtnTrip
        '
        Me.BtnTrip.Animated = True
        Me.BtnTrip.BorderColor = System.Drawing.Color.White
        Me.BtnTrip.BorderRadius = 5
        Me.BtnTrip.BorderThickness = 1
        Me.BtnTrip.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTrip.FillColor = System.Drawing.Color.Transparent
        Me.BtnTrip.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnTrip.ForeColor = System.Drawing.Color.White
        Me.BtnTrip.Location = New System.Drawing.Point(152, 326)
        Me.BtnTrip.Name = "BtnTrip"
        Me.BtnTrip.Size = New System.Drawing.Size(111, 40)
        Me.BtnTrip.TabIndex = 146
        Me.BtnTrip.Text = "TRIP"
        '
        'LblPCorte1
        '
        Me.LblPCorte1.AutoSize = True
        Me.LblPCorte1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPCorte1.ForeColor = System.Drawing.Color.White
        Me.LblPCorte1.Location = New System.Drawing.Point(144, 247)
        Me.LblPCorte1.Name = "LblPCorte1"
        Me.LblPCorte1.Size = New System.Drawing.Size(126, 20)
        Me.LblPCorte1.TabIndex = 142
        Me.LblPCorte1.Text = "Presión de Corte"
        '
        'LblPCorteActualG
        '
        Me.LblPCorteActualG.AutoSize = True
        Me.LblPCorteActualG.Font = New System.Drawing.Font("Montserrat", 30.0!)
        Me.LblPCorteActualG.ForeColor = System.Drawing.Color.White
        Me.LblPCorteActualG.Location = New System.Drawing.Point(144, 81)
        Me.LblPCorteActualG.Name = "LblPCorteActualG"
        Me.LblPCorteActualG.Size = New System.Drawing.Size(124, 55)
        Me.LblPCorteActualG.TabIndex = 145
        Me.LblPCorteActualG.Text = "8986"
        '
        'LblPCorteDeseadaG
        '
        Me.LblPCorteDeseadaG.AutoSize = True
        Me.LblPCorteDeseadaG.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblPCorteDeseadaG.ForeColor = System.Drawing.Color.White
        Me.LblPCorteDeseadaG.Location = New System.Drawing.Point(163, 274)
        Me.LblPCorteDeseadaG.Name = "LblPCorteDeseadaG"
        Me.LblPCorteDeseadaG.Size = New System.Drawing.Size(87, 37)
        Me.LblPCorteDeseadaG.TabIndex = 144
        Me.LblPCorteDeseadaG.Text = "9000"
        '
        'LblPCorte3
        '
        Me.LblPCorte3.AutoSize = True
        Me.LblPCorte3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPCorte3.ForeColor = System.Drawing.Color.White
        Me.LblPCorte3.Location = New System.Drawing.Point(152, 49)
        Me.LblPCorte3.Name = "LblPCorte3"
        Me.LblPCorte3.Size = New System.Drawing.Size(111, 20)
        Me.LblPCorte3.TabIndex = 143
        Me.LblPCorte3.Text = "Presión Actual"
        '
        'RgPresionG
        '
        Me.RgPresionG.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgPresionG.ArrowColor = System.Drawing.Color.Silver
        Me.RgPresionG.ArrowThickness = 0
        Me.RgPresionG.ArrowVisible = False
        Me.RgPresionG.BackColor = System.Drawing.Color.Transparent
        Me.RgPresionG.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgPresionG.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgPresionG.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgPresionG.Location = New System.Drawing.Point(1, 1)
        Me.RgPresionG.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgPresionG.Name = "RgPresionG"
        Me.RgPresionG.ProgressColor = System.Drawing.Color.Lime
        Me.RgPresionG.ProgressColor2 = System.Drawing.Color.DarkGreen
        Me.RgPresionG.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresionG.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresionG.ProgressThickness = 12
        Me.RgPresionG.ShowPercentage = False
        Me.RgPresionG.Size = New System.Drawing.Size(412, 412)
        Me.RgPresionG.TabIndex = 148
        Me.RgPresionG.UseTransparentBackground = True
        Me.RgPresionG.Value = 85
        '
        'RPMPanel
        '
        Me.RPMPanel.Controls.Add(Me.BtnIdle)
        Me.RPMPanel.Controls.Add(Me.BtnRpmPlus50)
        Me.RPMPanel.Controls.Add(Me.LblRPMDeseadaG)
        Me.RPMPanel.Controls.Add(Me.LblRPM3)
        Me.RPMPanel.Controls.Add(Me.BtnRpmMinus25)
        Me.RPMPanel.Controls.Add(Me.LblRPMActualG)
        Me.RPMPanel.Controls.Add(Me.LblRPM1)
        Me.RPMPanel.Controls.Add(Me.BtnRpmPlus25)
        Me.RPMPanel.Controls.Add(Me.BtnRpmMinus50)
        Me.RPMPanel.Controls.Add(Me.RgRPMG)
        Me.RPMPanel.Location = New System.Drawing.Point(11, -1)
        Me.RPMPanel.Name = "RPMPanel"
        Me.RPMPanel.Size = New System.Drawing.Size(414, 414)
        Me.RPMPanel.TabIndex = 150
        '
        'BtnIdle
        '
        Me.BtnIdle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnIdle.Animated = True
        Me.BtnIdle.BorderColor = System.Drawing.Color.White
        Me.BtnIdle.BorderRadius = 5
        Me.BtnIdle.BorderThickness = 1
        Me.BtnIdle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdle.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdle.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnIdle.ForeColor = System.Drawing.Color.White
        Me.BtnIdle.Location = New System.Drawing.Point(138, 326)
        Me.BtnIdle.Name = "BtnIdle"
        Me.BtnIdle.Size = New System.Drawing.Size(138, 40)
        Me.BtnIdle.TabIndex = 136
        Me.BtnIdle.Text = "IDLE"
        '
        'BtnRpmPlus50
        '
        Me.BtnRpmPlus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmPlus50.Animated = True
        Me.BtnRpmPlus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.BorderRadius = 5
        Me.BtnRpmPlus50.BorderThickness = 1
        Me.BtnRpmPlus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.Image = CType(resources.GetObject("BtnRpmPlus50.Image"), System.Drawing.Image)
        Me.BtnRpmPlus50.Location = New System.Drawing.Point(229, 163)
        Me.BtnRpmPlus50.Name = "BtnRpmPlus50"
        Me.BtnRpmPlus50.Size = New System.Drawing.Size(40, 40)
        Me.BtnRpmPlus50.TabIndex = 139
        '
        'LblRPMDeseadaG
        '
        Me.LblRPMDeseadaG.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPMDeseadaG.AutoSize = True
        Me.LblRPMDeseadaG.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblRPMDeseadaG.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblRPMDeseadaG.ForeColor = System.Drawing.Color.White
        Me.LblRPMDeseadaG.Location = New System.Drawing.Point(110, 226)
        Me.LblRPMDeseadaG.Name = "LblRPMDeseadaG"
        Me.LblRPMDeseadaG.Size = New System.Drawing.Size(75, 37)
        Me.LblRPMDeseadaG.TabIndex = 134
        Me.LblRPMDeseadaG.Text = "1550"
        '
        'LblRPM3
        '
        Me.LblRPM3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPM3.AutoSize = True
        Me.LblRPM3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM3.ForeColor = System.Drawing.Color.White
        Me.LblRPM3.Location = New System.Drawing.Point(154, 64)
        Me.LblRPM3.Name = "LblRPM3"
        Me.LblRPM3.Size = New System.Drawing.Size(106, 20)
        Me.LblRPM3.TabIndex = 133
        Me.LblRPM3.Text = "RPM Actuales"
        '
        'BtnRpmMinus25
        '
        Me.BtnRpmMinus25.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmMinus25.Animated = True
        Me.BtnRpmMinus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.BorderRadius = 5
        Me.BtnRpmMinus25.BorderThickness = 1
        Me.BtnRpmMinus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.Image = CType(resources.GetObject("BtnRpmMinus25.Image"), System.Drawing.Image)
        Me.BtnRpmMinus25.Location = New System.Drawing.Point(275, 238)
        Me.BtnRpmMinus25.Name = "BtnRpmMinus25"
        Me.BtnRpmMinus25.Size = New System.Drawing.Size(30, 30)
        Me.BtnRpmMinus25.TabIndex = 138
        '
        'LblRPMActualG
        '
        Me.LblRPMActualG.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPMActualG.AutoSize = True
        Me.LblRPMActualG.Font = New System.Drawing.Font("Montserrat", 30.0!)
        Me.LblRPMActualG.ForeColor = System.Drawing.Color.White
        Me.LblRPMActualG.Location = New System.Drawing.Point(153, 94)
        Me.LblRPMActualG.Name = "LblRPMActualG"
        Me.LblRPMActualG.Size = New System.Drawing.Size(107, 55)
        Me.LblRPMActualG.TabIndex = 135
        Me.LblRPMActualG.Text = "1553"
        '
        'LblRPM1
        '
        Me.LblRPM1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPM1.AutoSize = True
        Me.LblRPM1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM1.ForeColor = System.Drawing.Color.White
        Me.LblRPM1.Location = New System.Drawing.Point(86, 199)
        Me.LblRPM1.Name = "LblRPM1"
        Me.LblRPM1.Size = New System.Drawing.Size(113, 20)
        Me.LblRPM1.TabIndex = 132
        Me.LblRPM1.Text = "RPM Deseadas"
        '
        'BtnRpmPlus25
        '
        Me.BtnRpmPlus25.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmPlus25.Animated = True
        Me.BtnRpmPlus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.BorderRadius = 5
        Me.BtnRpmPlus25.BorderThickness = 1
        Me.BtnRpmPlus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.Image = CType(resources.GetObject("BtnRpmPlus25.Image"), System.Drawing.Image)
        Me.BtnRpmPlus25.Location = New System.Drawing.Point(275, 202)
        Me.BtnRpmPlus25.Name = "BtnRpmPlus25"
        Me.BtnRpmPlus25.Size = New System.Drawing.Size(30, 30)
        Me.BtnRpmPlus25.TabIndex = 137
        '
        'BtnRpmMinus50
        '
        Me.BtnRpmMinus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmMinus50.Animated = True
        Me.BtnRpmMinus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.BorderRadius = 5
        Me.BtnRpmMinus50.BorderThickness = 1
        Me.BtnRpmMinus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.Image = CType(resources.GetObject("BtnRpmMinus50.Image"), System.Drawing.Image)
        Me.BtnRpmMinus50.Location = New System.Drawing.Point(229, 262)
        Me.BtnRpmMinus50.Name = "BtnRpmMinus50"
        Me.BtnRpmMinus50.Size = New System.Drawing.Size(40, 40)
        Me.BtnRpmMinus50.TabIndex = 140
        '
        'RgRPMG
        '
        Me.RgRPMG.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgRPMG.ArrowColor = System.Drawing.Color.Silver
        Me.RgRPMG.ArrowThickness = 0
        Me.RgRPMG.ArrowVisible = False
        Me.RgRPMG.BackColor = System.Drawing.Color.Transparent
        Me.RgRPMG.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgRPMG.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgRPMG.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgRPMG.Location = New System.Drawing.Point(1, 1)
        Me.RgRPMG.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgRPMG.Name = "RgRPMG"
        Me.RgRPMG.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgRPMG.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgRPMG.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPMG.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPMG.ProgressThickness = 12
        Me.RgRPMG.ShowPercentage = False
        Me.RgRPMG.Size = New System.Drawing.Size(412, 412)
        Me.RgRPMG.TabIndex = 141
        Me.RgRPMG.UseTransparentBackground = True
        Me.RgRPMG.Value = 100
        '
        'Motor
        '
        Me.Motor.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Motor.Controls.Add(Me.Title1)
        Me.Motor.Controls.Add(Me.PanelDiagM)
        Me.Motor.Controls.Add(Me.Label48)
        Me.Motor.Controls.Add(Me.Label1)
        Me.Motor.Controls.Add(Me.PanelErroresM)
        Me.Motor.Controls.Add(Me.CpbHorasMotorM)
        Me.Motor.Controls.Add(Me.CpbCombustibleM)
        Me.Motor.Controls.Add(Me.CpbVoltajeM)
        Me.Motor.Controls.Add(Me.CpbPresionM)
        Me.Motor.Controls.Add(Me.PanelTempAguaM)
        Me.Motor.Controls.Add(Me.PanelTempAceiteM)
        Me.Motor.Controls.Add(Me.PanelCombustibleM)
        Me.Motor.Controls.Add(Me.PanelCargaM)
        Me.Motor.Controls.Add(Me.PanelRPMM)
        Me.Motor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Motor.Location = New System.Drawing.Point(4, 4)
        Me.Motor.Margin = New System.Windows.Forms.Padding(0)
        Me.Motor.Name = "Motor"
        Me.Motor.Size = New System.Drawing.Size(1201, 630)
        Me.Motor.TabIndex = 3
        Me.Motor.Text = "Motor"
        Me.Motor.ToolTipText = "Pestaña de parametros de motor"
        '
        'Title1
        '
        Me.Title1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Title1.AutoSize = True
        Me.Title1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Title1.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.Title1.ForeColor = System.Drawing.Color.White
        Me.Title1.Location = New System.Drawing.Point(540, 15)
        Me.Title1.Name = "Title1"
        Me.Title1.Size = New System.Drawing.Size(121, 47)
        Me.Title1.TabIndex = 142
        Me.Title1.Text = "Motor"
        '
        'PanelDiagM
        '
        Me.PanelDiagM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PanelDiagM.BackColor = System.Drawing.Color.Transparent
        Me.PanelDiagM.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelDiagM.BorderRadius = 6
        Me.PanelDiagM.BorderThickness = 1
        Me.PanelDiagM.Controls.Add(Me.ErrorProgressM2)
        Me.PanelDiagM.Controls.Add(Me.LblMHoras)
        Me.PanelDiagM.Controls.Add(Me.PbMHorasMotor)
        Me.PanelDiagM.Controls.Add(Me.Guna2CustomGradientPanel6)
        Me.PanelDiagM.Controls.Add(Me.LblMCombustible)
        Me.PanelDiagM.Controls.Add(Me.PbMCombustible)
        Me.PanelDiagM.Controls.Add(Me.LblMTempAceite)
        Me.PanelDiagM.Controls.Add(Me.PbMTempAceite)
        Me.PanelDiagM.Controls.Add(Me.LblMPresAceite)
        Me.PanelDiagM.Controls.Add(Me.PbMPresAceite)
        Me.PanelDiagM.Controls.Add(Me.LblMTempAgua)
        Me.PanelDiagM.Controls.Add(Me.PbMTempAgua)
        Me.PanelDiagM.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelDiagM.FillColor = System.Drawing.Color.Transparent
        Me.PanelDiagM.Location = New System.Drawing.Point(604, 466)
        Me.PanelDiagM.Name = "PanelDiagM"
        Me.PanelDiagM.Size = New System.Drawing.Size(597, 164)
        Me.PanelDiagM.TabIndex = 116
        '
        'ErrorProgressM2
        '
        Me.ErrorProgressM2.AnimationSpeed = 50
        Me.ErrorProgressM2.AutoStart = True
        Me.ErrorProgressM2.Location = New System.Drawing.Point(236, 51)
        Me.ErrorProgressM2.Name = "ErrorProgressM2"
        Me.ErrorProgressM2.Size = New System.Drawing.Size(90, 90)
        Me.ErrorProgressM2.TabIndex = 80
        Me.ErrorProgressM2.UseTransparentBackground = True
        '
        'LblMHoras
        '
        Me.LblMHoras.AutoSize = True
        Me.LblMHoras.BackColor = System.Drawing.Color.Transparent
        Me.LblMHoras.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMHoras.ForeColor = System.Drawing.Color.White
        Me.LblMHoras.Location = New System.Drawing.Point(67, 129)
        Me.LblMHoras.Name = "LblMHoras"
        Me.LblMHoras.Size = New System.Drawing.Size(96, 20)
        Me.LblMHoras.TabIndex = 69
        Me.LblMHoras.Text = "Horas Motor"
        '
        'PbMHorasMotor
        '
        Me.PbMHorasMotor.BackColor = System.Drawing.Color.Transparent
        Me.PbMHorasMotor.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMHorasMotor.ImageRotate = 0!
        Me.PbMHorasMotor.Location = New System.Drawing.Point(18, 127)
        Me.PbMHorasMotor.Name = "PbMHorasMotor"
        Me.PbMHorasMotor.Size = New System.Drawing.Size(25, 25)
        Me.PbMHorasMotor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMHorasMotor.TabIndex = 68
        Me.PbMHorasMotor.TabStop = False
        Me.PbMHorasMotor.UseTransparentBackground = True
        '
        'Guna2CustomGradientPanel6
        '
        Me.Guna2CustomGradientPanel6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.BorderRadius = 6
        Me.Guna2CustomGradientPanel6.Controls.Add(Me.LblDiagnosticoMotor)
        Me.Guna2CustomGradientPanel6.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel6.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel6.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel6.Name = "Guna2CustomGradientPanel6"
        Me.Guna2CustomGradientPanel6.Size = New System.Drawing.Size(597, 26)
        Me.Guna2CustomGradientPanel6.TabIndex = 19
        '
        'LblDiagnosticoMotor
        '
        Me.LblDiagnosticoMotor.AutoSize = True
        Me.LblDiagnosticoMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDiagnosticoMotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblDiagnosticoMotor.Location = New System.Drawing.Point(283, 4)
        Me.LblDiagnosticoMotor.Name = "LblDiagnosticoMotor"
        Me.LblDiagnosticoMotor.Size = New System.Drawing.Size(95, 20)
        Me.LblDiagnosticoMotor.TabIndex = 19
        Me.LblDiagnosticoMotor.Text = "Diagnóstico"
        '
        'LblMCombustible
        '
        Me.LblMCombustible.AutoSize = True
        Me.LblMCombustible.BackColor = System.Drawing.Color.Transparent
        Me.LblMCombustible.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMCombustible.ForeColor = System.Drawing.Color.White
        Me.LblMCombustible.Location = New System.Drawing.Point(67, 43)
        Me.LblMCombustible.Name = "LblMCombustible"
        Me.LblMCombustible.Size = New System.Drawing.Size(159, 20)
        Me.LblMCombustible.TabIndex = 67
        Me.LblMCombustible.Text = "Nivel de Combustible"
        '
        'PbMCombustible
        '
        Me.PbMCombustible.BackColor = System.Drawing.Color.Transparent
        Me.PbMCombustible.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMCombustible.ImageRotate = 0!
        Me.PbMCombustible.Location = New System.Drawing.Point(18, 41)
        Me.PbMCombustible.Name = "PbMCombustible"
        Me.PbMCombustible.Size = New System.Drawing.Size(25, 25)
        Me.PbMCombustible.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMCombustible.TabIndex = 66
        Me.PbMCombustible.TabStop = False
        Me.PbMCombustible.UseTransparentBackground = True
        '
        'LblMTempAceite
        '
        Me.LblMTempAceite.AutoSize = True
        Me.LblMTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblMTempAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblMTempAceite.Location = New System.Drawing.Point(67, 86)
        Me.LblMTempAceite.Name = "LblMTempAceite"
        Me.LblMTempAceite.Size = New System.Drawing.Size(166, 20)
        Me.LblMTempAceite.TabIndex = 61
        Me.LblMTempAceite.Text = "Temperatura de Aceite"
        '
        'PbMTempAceite
        '
        Me.PbMTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbMTempAceite.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMTempAceite.ImageRotate = 0!
        Me.PbMTempAceite.Location = New System.Drawing.Point(18, 84)
        Me.PbMTempAceite.Name = "PbMTempAceite"
        Me.PbMTempAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbMTempAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMTempAceite.TabIndex = 4
        Me.PbMTempAceite.TabStop = False
        Me.PbMTempAceite.UseTransparentBackground = True
        '
        'LblMPresAceite
        '
        Me.LblMPresAceite.AutoSize = True
        Me.LblMPresAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblMPresAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMPresAceite.ForeColor = System.Drawing.Color.White
        Me.LblMPresAceite.Location = New System.Drawing.Point(394, 57)
        Me.LblMPresAceite.Name = "LblMPresAceite"
        Me.LblMPresAceite.Size = New System.Drawing.Size(131, 20)
        Me.LblMPresAceite.TabIndex = 65
        Me.LblMPresAceite.Text = "Presión de Aceite"
        '
        'PbMPresAceite
        '
        Me.PbMPresAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbMPresAceite.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMPresAceite.ImageRotate = 0!
        Me.PbMPresAceite.Location = New System.Drawing.Point(345, 55)
        Me.PbMPresAceite.Name = "PbMPresAceite"
        Me.PbMPresAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbMPresAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMPresAceite.TabIndex = 64
        Me.PbMPresAceite.TabStop = False
        Me.PbMPresAceite.UseTransparentBackground = True
        '
        'LblMTempAgua
        '
        Me.LblMTempAgua.AutoSize = True
        Me.LblMTempAgua.BackColor = System.Drawing.Color.Transparent
        Me.LblMTempAgua.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblMTempAgua.Location = New System.Drawing.Point(394, 100)
        Me.LblMTempAgua.Name = "LblMTempAgua"
        Me.LblMTempAgua.Size = New System.Drawing.Size(159, 20)
        Me.LblMTempAgua.TabIndex = 63
        Me.LblMTempAgua.Text = "Temperatura de Agua"
        '
        'PbMTempAgua
        '
        Me.PbMTempAgua.BackColor = System.Drawing.Color.Transparent
        Me.PbMTempAgua.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMTempAgua.ImageRotate = 0!
        Me.PbMTempAgua.Location = New System.Drawing.Point(345, 98)
        Me.PbMTempAgua.Name = "PbMTempAgua"
        Me.PbMTempAgua.Size = New System.Drawing.Size(25, 25)
        Me.PbMTempAgua.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMTempAgua.TabIndex = 62
        Me.PbMTempAgua.TabStop = False
        Me.PbMTempAgua.UseTransparentBackground = True
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label48.ForeColor = System.Drawing.Color.White
        Me.Label48.Location = New System.Drawing.Point(671, 360)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(41, 20)
        Me.Label48.TabIndex = 133
        Me.Label48.Text = "1800"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(486, 360)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 20)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "0"
        '
        'PanelErroresM
        '
        Me.PanelErroresM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PanelErroresM.BackColor = System.Drawing.Color.Transparent
        Me.PanelErroresM.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelErroresM.BorderRadius = 6
        Me.PanelErroresM.BorderThickness = 1
        Me.PanelErroresM.Controls.Add(Me.ErrorProgressM1)
        Me.PanelErroresM.Controls.Add(Me.BtnBorrarActualM)
        Me.PanelErroresM.Controls.Add(Me.BtnSiguienteM)
        Me.PanelErroresM.Controls.Add(Me.BtnAnteriorM)
        Me.PanelErroresM.Controls.Add(Me.LblDescripcionM)
        Me.PanelErroresM.Controls.Add(Me.LblInfoDescripM)
        Me.PanelErroresM.Controls.Add(Me.LblUnidadM)
        Me.PanelErroresM.Controls.Add(Me.LblInfoUnidadM)
        Me.PanelErroresM.Controls.Add(Me.LblActivoM)
        Me.PanelErroresM.Controls.Add(Me.LblInfoActivoM)
        Me.PanelErroresM.Controls.Add(Me.LblErroresTotalesM)
        Me.PanelErroresM.Controls.Add(Me.LblErrorNM)
        Me.PanelErroresM.Controls.Add(Me.LblOCM)
        Me.PanelErroresM.Controls.Add(Me.LblFMIM)
        Me.PanelErroresM.Controls.Add(Me.LblSPNM)
        Me.PanelErroresM.Controls.Add(Me.LblInfoErrTotM)
        Me.PanelErroresM.Controls.Add(Me.LblInfoErrorNM)
        Me.PanelErroresM.Controls.Add(Me.LblInfoOCM)
        Me.PanelErroresM.Controls.Add(Me.LblInfoFMIM)
        Me.PanelErroresM.Controls.Add(Me.LblInfoSPNM)
        Me.PanelErroresM.Controls.Add(Me.Guna2CustomGradientPanel8)
        Me.PanelErroresM.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelErroresM.FillColor = System.Drawing.Color.Transparent
        Me.PanelErroresM.Location = New System.Drawing.Point(0, 466)
        Me.PanelErroresM.Name = "PanelErroresM"
        Me.PanelErroresM.Size = New System.Drawing.Size(598, 164)
        Me.PanelErroresM.TabIndex = 115
        '
        'ErrorProgressM1
        '
        Me.ErrorProgressM1.AnimationSpeed = 50
        Me.ErrorProgressM1.AutoStart = True
        Me.ErrorProgressM1.Location = New System.Drawing.Point(253, 51)
        Me.ErrorProgressM1.Name = "ErrorProgressM1"
        Me.ErrorProgressM1.Size = New System.Drawing.Size(90, 90)
        Me.ErrorProgressM1.TabIndex = 79
        Me.ErrorProgressM1.UseTransparentBackground = True
        '
        'BtnBorrarActualM
        '
        Me.BtnBorrarActualM.Animated = True
        Me.BtnBorrarActualM.BorderColor = System.Drawing.Color.White
        Me.BtnBorrarActualM.BorderRadius = 5
        Me.BtnBorrarActualM.BorderThickness = 1
        Me.BtnBorrarActualM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnBorrarActualM.FillColor = System.Drawing.Color.Transparent
        Me.BtnBorrarActualM.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnBorrarActualM.ForeColor = System.Drawing.Color.White
        Me.BtnBorrarActualM.Image = Global.sFRAC.My.Resources.Resources.white_delete
        Me.BtnBorrarActualM.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnBorrarActualM.ImageSize = New System.Drawing.Size(19, 19)
        Me.BtnBorrarActualM.Location = New System.Drawing.Point(30, 118)
        Me.BtnBorrarActualM.Name = "BtnBorrarActualM"
        Me.BtnBorrarActualM.Size = New System.Drawing.Size(40, 40)
        Me.BtnBorrarActualM.TabIndex = 78
        '
        'BtnSiguienteM
        '
        Me.BtnSiguienteM.Animated = True
        Me.BtnSiguienteM.BorderColor = System.Drawing.Color.White
        Me.BtnSiguienteM.BorderRadius = 5
        Me.BtnSiguienteM.BorderThickness = 1
        Me.BtnSiguienteM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSiguienteM.FillColor = System.Drawing.Color.Transparent
        Me.BtnSiguienteM.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnSiguienteM.ForeColor = System.Drawing.Color.White
        Me.BtnSiguienteM.Image = Global.sFRAC.My.Resources.Resources.right_arrow
        Me.BtnSiguienteM.Location = New System.Drawing.Point(316, 141)
        Me.BtnSiguienteM.Name = "BtnSiguienteM"
        Me.BtnSiguienteM.Size = New System.Drawing.Size(80, 20)
        Me.BtnSiguienteM.TabIndex = 77
        '
        'BtnAnteriorM
        '
        Me.BtnAnteriorM.Animated = True
        Me.BtnAnteriorM.BorderColor = System.Drawing.Color.White
        Me.BtnAnteriorM.BorderRadius = 5
        Me.BtnAnteriorM.BorderThickness = 1
        Me.BtnAnteriorM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAnteriorM.FillColor = System.Drawing.Color.Transparent
        Me.BtnAnteriorM.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnAnteriorM.ForeColor = System.Drawing.Color.White
        Me.BtnAnteriorM.Image = Global.sFRAC.My.Resources.Resources.left_arrow
        Me.BtnAnteriorM.Location = New System.Drawing.Point(230, 141)
        Me.BtnAnteriorM.Name = "BtnAnteriorM"
        Me.BtnAnteriorM.Size = New System.Drawing.Size(80, 20)
        Me.BtnAnteriorM.TabIndex = 76
        '
        'LblDescripcionM
        '
        Me.LblDescripcionM.BackColor = System.Drawing.Color.Transparent
        Me.LblDescripcionM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDescripcionM.ForeColor = System.Drawing.Color.White
        Me.LblDescripcionM.Location = New System.Drawing.Point(101, 93)
        Me.LblDescripcionM.Name = "LblDescripcionM"
        Me.LblDescripcionM.Size = New System.Drawing.Size(485, 45)
        Me.LblDescripcionM.TabIndex = 54
        Me.LblDescripcionM.Text = "Drescripcion"
        '
        'LblInfoDescripM
        '
        Me.LblInfoDescripM.AutoSize = True
        Me.LblInfoDescripM.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoDescripM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoDescripM.ForeColor = System.Drawing.Color.White
        Me.LblInfoDescripM.Location = New System.Drawing.Point(5, 93)
        Me.LblInfoDescripM.Name = "LblInfoDescripM"
        Me.LblInfoDescripM.Size = New System.Drawing.Size(96, 20)
        Me.LblInfoDescripM.TabIndex = 53
        Me.LblInfoDescripM.Text = "Descripción:"
        '
        'LblUnidadM
        '
        Me.LblUnidadM.AutoSize = True
        Me.LblUnidadM.BackColor = System.Drawing.Color.Transparent
        Me.LblUnidadM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidadM.ForeColor = System.Drawing.Color.White
        Me.LblUnidadM.Location = New System.Drawing.Point(529, 31)
        Me.LblUnidadM.Name = "LblUnidadM"
        Me.LblUnidadM.Size = New System.Drawing.Size(59, 20)
        Me.LblUnidadM.TabIndex = 52
        Me.LblUnidadM.Text = "Unidad"
        '
        'LblInfoUnidadM
        '
        Me.LblInfoUnidadM.AutoSize = True
        Me.LblInfoUnidadM.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoUnidadM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoUnidadM.ForeColor = System.Drawing.Color.White
        Me.LblInfoUnidadM.Location = New System.Drawing.Point(463, 31)
        Me.LblInfoUnidadM.Name = "LblInfoUnidadM"
        Me.LblInfoUnidadM.Size = New System.Drawing.Size(62, 20)
        Me.LblInfoUnidadM.TabIndex = 51
        Me.LblInfoUnidadM.Text = "Unidad:"
        '
        'LblActivoM
        '
        Me.LblActivoM.AutoSize = True
        Me.LblActivoM.BackColor = System.Drawing.Color.Transparent
        Me.LblActivoM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblActivoM.ForeColor = System.Drawing.Color.White
        Me.LblActivoM.Location = New System.Drawing.Point(69, 62)
        Me.LblActivoM.Name = "LblActivoM"
        Me.LblActivoM.Size = New System.Drawing.Size(32, 20)
        Me.LblActivoM.TabIndex = 50
        Me.LblActivoM.Text = "NO"
        '
        'LblInfoActivoM
        '
        Me.LblInfoActivoM.AutoSize = True
        Me.LblInfoActivoM.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoActivoM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoActivoM.ForeColor = System.Drawing.Color.White
        Me.LblInfoActivoM.Location = New System.Drawing.Point(5, 62)
        Me.LblInfoActivoM.Name = "LblInfoActivoM"
        Me.LblInfoActivoM.Size = New System.Drawing.Size(61, 20)
        Me.LblInfoActivoM.TabIndex = 49
        Me.LblInfoActivoM.Text = "Activo: "
        '
        'LblErroresTotalesM
        '
        Me.LblErroresTotalesM.AutoSize = True
        Me.LblErroresTotalesM.BackColor = System.Drawing.Color.Transparent
        Me.LblErroresTotalesM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErroresTotalesM.ForeColor = System.Drawing.Color.White
        Me.LblErroresTotalesM.Location = New System.Drawing.Point(232, 62)
        Me.LblErroresTotalesM.Name = "LblErroresTotalesM"
        Me.LblErroresTotalesM.Size = New System.Drawing.Size(41, 20)
        Me.LblErroresTotalesM.TabIndex = 47
        Me.LblErroresTotalesM.Text = "1000"
        '
        'LblErrorNM
        '
        Me.LblErrorNM.AutoSize = True
        Me.LblErrorNM.BackColor = System.Drawing.Color.Transparent
        Me.LblErrorNM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErrorNM.ForeColor = System.Drawing.Color.White
        Me.LblErrorNM.Location = New System.Drawing.Point(402, 31)
        Me.LblErrorNM.Name = "LblErrorNM"
        Me.LblErrorNM.Size = New System.Drawing.Size(41, 20)
        Me.LblErrorNM.TabIndex = 46
        Me.LblErrorNM.Text = "1000"
        '
        'LblOCM
        '
        Me.LblOCM.AutoSize = True
        Me.LblOCM.BackColor = System.Drawing.Color.Transparent
        Me.LblOCM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblOCM.ForeColor = System.Drawing.Color.White
        Me.LblOCM.Location = New System.Drawing.Point(264, 31)
        Me.LblOCM.Name = "LblOCM"
        Me.LblOCM.Size = New System.Drawing.Size(41, 20)
        Me.LblOCM.TabIndex = 45
        Me.LblOCM.Text = "1000"
        '
        'LblFMIM
        '
        Me.LblFMIM.AutoSize = True
        Me.LblFMIM.BackColor = System.Drawing.Color.Transparent
        Me.LblFMIM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblFMIM.ForeColor = System.Drawing.Color.White
        Me.LblFMIM.Location = New System.Drawing.Point(159, 31)
        Me.LblFMIM.Name = "LblFMIM"
        Me.LblFMIM.Size = New System.Drawing.Size(41, 20)
        Me.LblFMIM.TabIndex = 44
        Me.LblFMIM.Text = "1000"
        '
        'LblSPNM
        '
        Me.LblSPNM.AutoSize = True
        Me.LblSPNM.BackColor = System.Drawing.Color.Transparent
        Me.LblSPNM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblSPNM.ForeColor = System.Drawing.Color.White
        Me.LblSPNM.Location = New System.Drawing.Point(50, 31)
        Me.LblSPNM.Name = "LblSPNM"
        Me.LblSPNM.Size = New System.Drawing.Size(41, 20)
        Me.LblSPNM.TabIndex = 43
        Me.LblSPNM.Text = "1000"
        '
        'LblInfoErrTotM
        '
        Me.LblInfoErrTotM.AutoSize = True
        Me.LblInfoErrTotM.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoErrTotM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoErrTotM.ForeColor = System.Drawing.Color.White
        Me.LblInfoErrTotM.Location = New System.Drawing.Point(117, 62)
        Me.LblInfoErrTotM.Name = "LblInfoErrTotM"
        Me.LblInfoErrTotM.Size = New System.Drawing.Size(116, 20)
        Me.LblInfoErrTotM.TabIndex = 41
        Me.LblInfoErrTotM.Text = "Errores Totales:"
        '
        'LblInfoErrorNM
        '
        Me.LblInfoErrorNM.AutoSize = True
        Me.LblInfoErrorNM.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoErrorNM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoErrorNM.ForeColor = System.Drawing.Color.White
        Me.LblInfoErrorNM.Location = New System.Drawing.Point(331, 31)
        Me.LblInfoErrorNM.Name = "LblInfoErrorNM"
        Me.LblInfoErrorNM.Size = New System.Drawing.Size(69, 20)
        Me.LblInfoErrorNM.TabIndex = 40
        Me.LblInfoErrorNM.Text = "Error Nº:"
        '
        'LblInfoOCM
        '
        Me.LblInfoOCM.AutoSize = True
        Me.LblInfoOCM.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoOCM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoOCM.ForeColor = System.Drawing.Color.White
        Me.LblInfoOCM.Location = New System.Drawing.Point(226, 31)
        Me.LblInfoOCM.Name = "LblInfoOCM"
        Me.LblInfoOCM.Size = New System.Drawing.Size(34, 20)
        Me.LblInfoOCM.TabIndex = 39
        Me.LblInfoOCM.Text = "OC:"
        '
        'LblInfoFMIM
        '
        Me.LblInfoFMIM.AutoSize = True
        Me.LblInfoFMIM.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoFMIM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoFMIM.ForeColor = System.Drawing.Color.White
        Me.LblInfoFMIM.Location = New System.Drawing.Point(117, 31)
        Me.LblInfoFMIM.Name = "LblInfoFMIM"
        Me.LblInfoFMIM.Size = New System.Drawing.Size(38, 20)
        Me.LblInfoFMIM.TabIndex = 38
        Me.LblInfoFMIM.Text = "FMI:"
        '
        'LblInfoSPNM
        '
        Me.LblInfoSPNM.AutoSize = True
        Me.LblInfoSPNM.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoSPNM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoSPNM.ForeColor = System.Drawing.Color.White
        Me.LblInfoSPNM.Location = New System.Drawing.Point(5, 31)
        Me.LblInfoSPNM.Name = "LblInfoSPNM"
        Me.LblInfoSPNM.Size = New System.Drawing.Size(42, 20)
        Me.LblInfoSPNM.TabIndex = 37
        Me.LblInfoSPNM.Text = "SPN:"
        '
        'Guna2CustomGradientPanel8
        '
        Me.Guna2CustomGradientPanel8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.BorderRadius = 6
        Me.Guna2CustomGradientPanel8.Controls.Add(Me.LblErroresMotor)
        Me.Guna2CustomGradientPanel8.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel8.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel8.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel8.Name = "Guna2CustomGradientPanel8"
        Me.Guna2CustomGradientPanel8.Size = New System.Drawing.Size(598, 26)
        Me.Guna2CustomGradientPanel8.TabIndex = 19
        '
        'LblErroresMotor
        '
        Me.LblErroresMotor.AutoSize = True
        Me.LblErroresMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErroresMotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblErroresMotor.Location = New System.Drawing.Point(284, 4)
        Me.LblErroresMotor.Name = "LblErroresMotor"
        Me.LblErroresMotor.Size = New System.Drawing.Size(60, 20)
        Me.LblErroresMotor.TabIndex = 19
        Me.LblErroresMotor.Text = "Errores"
        '
        'CpbHorasMotorM
        '
        Me.CpbHorasMotorM.Controls.Add(Me.LblBHorasMotorM)
        Me.CpbHorasMotorM.Controls.Add(Me.LblBombaHorasMotorMotor)
        Me.CpbHorasMotorM.Controls.Add(Me.LblUnidad1M)
        Me.CpbHorasMotorM.FillColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.CpbHorasMotorM.FillThickness = 6
        Me.CpbHorasMotorM.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.CpbHorasMotorM.ForeColor = System.Drawing.Color.White
        Me.CpbHorasMotorM.InnerColor = System.Drawing.Color.Transparent
        Me.CpbHorasMotorM.Location = New System.Drawing.Point(12, 264)
        Me.CpbHorasMotorM.Minimum = 0
        Me.CpbHorasMotorM.Name = "CpbHorasMotorM"
        Me.CpbHorasMotorM.ProgressThickness = 6
        Me.CpbHorasMotorM.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle
        Me.CpbHorasMotorM.Size = New System.Drawing.Size(176, 176)
        Me.CpbHorasMotorM.TabIndex = 143
        Me.CpbHorasMotorM.Text = "Guna2CircleProgressBar1"
        '
        'LblBHorasMotorM
        '
        Me.LblBHorasMotorM.AutoSize = True
        Me.LblBHorasMotorM.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblBHorasMotorM.ForeColor = System.Drawing.Color.White
        Me.LblBHorasMotorM.Location = New System.Drawing.Point(72, 75)
        Me.LblBHorasMotorM.Name = "LblBHorasMotorM"
        Me.LblBHorasMotorM.Size = New System.Drawing.Size(38, 41)
        Me.LblBHorasMotorM.TabIndex = 117
        Me.LblBHorasMotorM.Text = "0"
        '
        'LblBombaHorasMotorMotor
        '
        Me.LblBombaHorasMotorMotor.AutoSize = True
        Me.LblBombaHorasMotorMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaHorasMotorMotor.ForeColor = System.Drawing.Color.White
        Me.LblBombaHorasMotorMotor.Location = New System.Drawing.Point(25, 43)
        Me.LblBombaHorasMotorMotor.Name = "LblBombaHorasMotorMotor"
        Me.LblBombaHorasMotorMotor.Size = New System.Drawing.Size(117, 20)
        Me.LblBombaHorasMotorMotor.TabIndex = 116
        Me.LblBombaHorasMotorMotor.Text = "Horas de Motor"
        '
        'LblUnidad1M
        '
        Me.LblUnidad1M.AutoSize = True
        Me.LblUnidad1M.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidad1M.ForeColor = System.Drawing.Color.White
        Me.LblUnidad1M.Location = New System.Drawing.Point(75, 130)
        Me.LblUnidad1M.Name = "LblUnidad1M"
        Me.LblUnidad1M.Size = New System.Drawing.Size(26, 20)
        Me.LblUnidad1M.TabIndex = 118
        Me.LblUnidad1M.Text = "Hr"
        '
        'CpbCombustibleM
        '
        Me.CpbCombustibleM.Controls.Add(Me.LblMotorCombustible1Motor)
        Me.CpbCombustibleM.Controls.Add(Me.LblMCombustibleM)
        Me.CpbCombustibleM.Controls.Add(Me.LblUnidad2M)
        Me.CpbCombustibleM.FillColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.CpbCombustibleM.FillThickness = 6
        Me.CpbCombustibleM.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.CpbCombustibleM.ForeColor = System.Drawing.Color.White
        Me.CpbCombustibleM.InnerColor = System.Drawing.Color.Transparent
        Me.CpbCombustibleM.Location = New System.Drawing.Point(207, 264)
        Me.CpbCombustibleM.Minimum = 0
        Me.CpbCombustibleM.Name = "CpbCombustibleM"
        Me.CpbCombustibleM.ProgressThickness = 6
        Me.CpbCombustibleM.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle
        Me.CpbCombustibleM.Size = New System.Drawing.Size(176, 176)
        Me.CpbCombustibleM.TabIndex = 144
        Me.CpbCombustibleM.Text = "Guna2CircleProgressBar2"
        '
        'LblMotorCombustible1Motor
        '
        Me.LblMotorCombustible1Motor.AutoSize = True
        Me.LblMotorCombustible1Motor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCombustible1Motor.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustible1Motor.Location = New System.Drawing.Point(38, 43)
        Me.LblMotorCombustible1Motor.Name = "LblMotorCombustible1Motor"
        Me.LblMotorCombustible1Motor.Size = New System.Drawing.Size(99, 20)
        Me.LblMotorCombustible1Motor.TabIndex = 30
        Me.LblMotorCombustible1Motor.Text = "Combustible"
        '
        'LblMCombustibleM
        '
        Me.LblMCombustibleM.AutoSize = True
        Me.LblMCombustibleM.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblMCombustibleM.ForeColor = System.Drawing.Color.White
        Me.LblMCombustibleM.Location = New System.Drawing.Point(43, 71)
        Me.LblMCombustibleM.Name = "LblMCombustibleM"
        Me.LblMCombustibleM.Size = New System.Drawing.Size(93, 41)
        Me.LblMCombustibleM.TabIndex = 32
        Me.LblMCombustibleM.Text = "3600"
        '
        'LblUnidad2M
        '
        Me.LblUnidad2M.AutoSize = True
        Me.LblUnidad2M.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidad2M.ForeColor = System.Drawing.Color.White
        Me.LblUnidad2M.Location = New System.Drawing.Point(71, 130)
        Me.LblUnidad2M.Name = "LblUnidad2M"
        Me.LblUnidad2M.Size = New System.Drawing.Size(39, 20)
        Me.LblUnidad2M.TabIndex = 36
        Me.LblUnidad2M.Text = "L/Hr"
        '
        'CpbVoltajeM
        '
        Me.CpbVoltajeM.Controls.Add(Me.LblMotorVoltajeMotor)
        Me.CpbVoltajeM.Controls.Add(Me.LblMVoltajeM)
        Me.CpbVoltajeM.Controls.Add(Me.LblUnidad3M)
        Me.CpbVoltajeM.FillColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.CpbVoltajeM.FillThickness = 6
        Me.CpbVoltajeM.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.CpbVoltajeM.ForeColor = System.Drawing.Color.White
        Me.CpbVoltajeM.InnerColor = System.Drawing.Color.Transparent
        Me.CpbVoltajeM.Location = New System.Drawing.Point(806, 264)
        Me.CpbVoltajeM.Minimum = 0
        Me.CpbVoltajeM.Name = "CpbVoltajeM"
        Me.CpbVoltajeM.ProgressThickness = 6
        Me.CpbVoltajeM.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle
        Me.CpbVoltajeM.Size = New System.Drawing.Size(176, 176)
        Me.CpbVoltajeM.TabIndex = 144
        Me.CpbVoltajeM.Text = "Guna2CircleProgressBar3"
        '
        'LblMotorVoltajeMotor
        '
        Me.LblMotorVoltajeMotor.AutoSize = True
        Me.LblMotorVoltajeMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorVoltajeMotor.ForeColor = System.Drawing.Color.White
        Me.LblMotorVoltajeMotor.Location = New System.Drawing.Point(64, 43)
        Me.LblMotorVoltajeMotor.Name = "LblMotorVoltajeMotor"
        Me.LblMotorVoltajeMotor.Size = New System.Drawing.Size(57, 20)
        Me.LblMotorVoltajeMotor.TabIndex = 24
        Me.LblMotorVoltajeMotor.Text = "Voltaje"
        '
        'LblMVoltajeM
        '
        Me.LblMVoltajeM.AutoSize = True
        Me.LblMVoltajeM.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblMVoltajeM.ForeColor = System.Drawing.Color.White
        Me.LblMVoltajeM.Location = New System.Drawing.Point(49, 71)
        Me.LblMVoltajeM.Name = "LblMVoltajeM"
        Me.LblMVoltajeM.Size = New System.Drawing.Size(72, 41)
        Me.LblMVoltajeM.TabIndex = 25
        Me.LblMVoltajeM.Text = "24.1"
        '
        'LblUnidad3M
        '
        Me.LblUnidad3M.AutoSize = True
        Me.LblUnidad3M.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidad3M.ForeColor = System.Drawing.Color.White
        Me.LblUnidad3M.Location = New System.Drawing.Point(81, 130)
        Me.LblUnidad3M.Name = "LblUnidad3M"
        Me.LblUnidad3M.Size = New System.Drawing.Size(19, 20)
        Me.LblUnidad3M.TabIndex = 28
        Me.LblUnidad3M.Text = "V"
        '
        'CpbPresionM
        '
        Me.CpbPresionM.Controls.Add(Me.LblMotorPresionMotor)
        Me.CpbPresionM.Controls.Add(Me.LblMPresionM)
        Me.CpbPresionM.Controls.Add(Me.LblUnidad4M)
        Me.CpbPresionM.FillColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.CpbPresionM.FillThickness = 6
        Me.CpbPresionM.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.CpbPresionM.ForeColor = System.Drawing.Color.White
        Me.CpbPresionM.InnerColor = System.Drawing.Color.Transparent
        Me.CpbPresionM.Location = New System.Drawing.Point(1012, 264)
        Me.CpbPresionM.Minimum = 0
        Me.CpbPresionM.Name = "CpbPresionM"
        Me.CpbPresionM.ProgressThickness = 6
        Me.CpbPresionM.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle
        Me.CpbPresionM.Size = New System.Drawing.Size(176, 176)
        Me.CpbPresionM.TabIndex = 145
        Me.CpbPresionM.Text = "Guna2CircleProgressBar4"
        '
        'LblMotorPresionMotor
        '
        Me.LblMotorPresionMotor.AutoSize = True
        Me.LblMotorPresionMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorPresionMotor.ForeColor = System.Drawing.Color.White
        Me.LblMotorPresionMotor.Location = New System.Drawing.Point(55, 43)
        Me.LblMotorPresionMotor.Name = "LblMotorPresionMotor"
        Me.LblMotorPresionMotor.Size = New System.Drawing.Size(62, 20)
        Me.LblMotorPresionMotor.TabIndex = 21
        Me.LblMotorPresionMotor.Text = "Presión"
        '
        'LblMPresionM
        '
        Me.LblMPresionM.AutoSize = True
        Me.LblMPresionM.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblMPresionM.ForeColor = System.Drawing.Color.White
        Me.LblMPresionM.Location = New System.Drawing.Point(69, 71)
        Me.LblMPresionM.Name = "LblMPresionM"
        Me.LblMPresionM.Size = New System.Drawing.Size(38, 41)
        Me.LblMPresionM.TabIndex = 23
        Me.LblMPresionM.Text = "0"
        '
        'LblUnidad4M
        '
        Me.LblUnidad4M.AutoSize = True
        Me.LblUnidad4M.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidad4M.ForeColor = System.Drawing.Color.White
        Me.LblUnidad4M.Location = New System.Drawing.Point(72, 130)
        Me.LblUnidad4M.Name = "LblUnidad4M"
        Me.LblUnidad4M.Size = New System.Drawing.Size(32, 20)
        Me.LblUnidad4M.TabIndex = 27
        Me.LblUnidad4M.Text = "PSI"
        '
        'PanelTempAguaM
        '
        Me.PanelTempAguaM.Controls.Add(Me.LblMotorTempAgua)
        Me.PanelTempAguaM.Controls.Add(Me.LblMTempAguaM)
        Me.PanelTempAguaM.Controls.Add(Me.LblMUnidad1)
        Me.PanelTempAguaM.Controls.Add(Me.RgTempAguaM)
        Me.PanelTempAguaM.Location = New System.Drawing.Point(2, 2)
        Me.PanelTempAguaM.Name = "PanelTempAguaM"
        Me.PanelTempAguaM.Size = New System.Drawing.Size(197, 197)
        Me.PanelTempAguaM.TabIndex = 146
        '
        'LblMotorTempAgua
        '
        Me.LblMotorTempAgua.AutoSize = True
        Me.LblMotorTempAgua.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAgua.Location = New System.Drawing.Point(54, 49)
        Me.LblMotorTempAgua.Name = "LblMotorTempAgua"
        Me.LblMotorTempAgua.Size = New System.Drawing.Size(89, 20)
        Me.LblMotorTempAgua.TabIndex = 20
        Me.LblMotorTempAgua.Text = "Temp Agua"
        '
        'LblMTempAguaM
        '
        Me.LblMTempAguaM.AutoSize = True
        Me.LblMTempAguaM.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblMTempAguaM.ForeColor = System.Drawing.Color.White
        Me.LblMTempAguaM.Location = New System.Drawing.Point(70, 80)
        Me.LblMTempAguaM.Name = "LblMTempAguaM"
        Me.LblMTempAguaM.Size = New System.Drawing.Size(57, 41)
        Me.LblMTempAguaM.TabIndex = 22
        Me.LblMTempAguaM.Text = "80"
        '
        'LblMUnidad1
        '
        Me.LblMUnidad1.AutoSize = True
        Me.LblMUnidad1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMUnidad1.ForeColor = System.Drawing.Color.White
        Me.LblMUnidad1.Location = New System.Drawing.Point(86, 135)
        Me.LblMUnidad1.Name = "LblMUnidad1"
        Me.LblMUnidad1.Size = New System.Drawing.Size(25, 20)
        Me.LblMUnidad1.TabIndex = 26
        Me.LblMUnidad1.Text = "ºC"
        '
        'RgTempAguaM
        '
        Me.RgTempAguaM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgTempAguaM.ArrowColor = System.Drawing.Color.Silver
        Me.RgTempAguaM.ArrowThickness = 0
        Me.RgTempAguaM.ArrowVisible = False
        Me.RgTempAguaM.BackColor = System.Drawing.Color.Transparent
        Me.RgTempAguaM.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgTempAguaM.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgTempAguaM.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgTempAguaM.Location = New System.Drawing.Point(1, 1)
        Me.RgTempAguaM.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgTempAguaM.Name = "RgTempAguaM"
        Me.RgTempAguaM.ProgressColor = System.Drawing.Color.LimeGreen
        Me.RgTempAguaM.ProgressColor2 = System.Drawing.Color.Green
        Me.RgTempAguaM.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempAguaM.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempAguaM.ProgressThickness = 6
        Me.RgTempAguaM.ShowPercentage = False
        Me.RgTempAguaM.Size = New System.Drawing.Size(195, 195)
        Me.RgTempAguaM.TabIndex = 138
        Me.RgTempAguaM.UseTransparentBackground = True
        Me.RgTempAguaM.Value = 80
        '
        'PanelTempAceiteM
        '
        Me.PanelTempAceiteM.Controls.Add(Me.LblMotorTempAceite)
        Me.PanelTempAceiteM.Controls.Add(Me.Label8)
        Me.PanelTempAceiteM.Controls.Add(Me.LblMTempAceiteM)
        Me.PanelTempAceiteM.Controls.Add(Me.RgTempAceiteM)
        Me.PanelTempAceiteM.Location = New System.Drawing.Point(197, 2)
        Me.PanelTempAceiteM.Name = "PanelTempAceiteM"
        Me.PanelTempAceiteM.Size = New System.Drawing.Size(197, 197)
        Me.PanelTempAceiteM.TabIndex = 148
        '
        'LblMotorTempAceite
        '
        Me.LblMotorTempAceite.AutoSize = True
        Me.LblMotorTempAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAceite.Location = New System.Drawing.Point(56, 49)
        Me.LblMotorTempAceite.Name = "LblMotorTempAceite"
        Me.LblMotorTempAceite.Size = New System.Drawing.Size(96, 20)
        Me.LblMotorTempAceite.TabIndex = 29
        Me.LblMotorTempAceite.Text = "Temp Aceite"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(92, 135)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(25, 20)
        Me.Label8.TabIndex = 35
        Me.Label8.Text = "ºC"
        '
        'LblMTempAceiteM
        '
        Me.LblMTempAceiteM.AutoSize = True
        Me.LblMTempAceiteM.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblMTempAceiteM.ForeColor = System.Drawing.Color.White
        Me.LblMTempAceiteM.Location = New System.Drawing.Point(78, 80)
        Me.LblMTempAceiteM.Name = "LblMTempAceiteM"
        Me.LblMTempAceiteM.Size = New System.Drawing.Size(52, 41)
        Me.LblMTempAceiteM.TabIndex = 31
        Me.LblMTempAceiteM.Text = "75"
        '
        'RgTempAceiteM
        '
        Me.RgTempAceiteM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgTempAceiteM.ArrowColor = System.Drawing.Color.Silver
        Me.RgTempAceiteM.ArrowThickness = 0
        Me.RgTempAceiteM.ArrowVisible = False
        Me.RgTempAceiteM.BackColor = System.Drawing.Color.Transparent
        Me.RgTempAceiteM.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgTempAceiteM.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgTempAceiteM.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgTempAceiteM.Location = New System.Drawing.Point(1, 1)
        Me.RgTempAceiteM.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgTempAceiteM.Name = "RgTempAceiteM"
        Me.RgTempAceiteM.ProgressColor = System.Drawing.Color.LimeGreen
        Me.RgTempAceiteM.ProgressColor2 = System.Drawing.Color.Green
        Me.RgTempAceiteM.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempAceiteM.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgTempAceiteM.ProgressThickness = 6
        Me.RgTempAceiteM.ShowPercentage = False
        Me.RgTempAceiteM.Size = New System.Drawing.Size(195, 195)
        Me.RgTempAceiteM.TabIndex = 140
        Me.RgTempAceiteM.UseTransparentBackground = True
        Me.RgTempAceiteM.Value = 60
        '
        'PanelCombustibleM
        '
        Me.PanelCombustibleM.Controls.Add(Me.LblMotorCombustibleMotor)
        Me.PanelCombustibleM.Controls.Add(Me.Label6)
        Me.PanelCombustibleM.Controls.Add(Me.LblMCombustibleP100M)
        Me.PanelCombustibleM.Controls.Add(Me.RgCombP100M)
        Me.PanelCombustibleM.Location = New System.Drawing.Point(806, 2)
        Me.PanelCombustibleM.Name = "PanelCombustibleM"
        Me.PanelCombustibleM.Size = New System.Drawing.Size(197, 197)
        Me.PanelCombustibleM.TabIndex = 147
        '
        'LblMotorCombustibleMotor
        '
        Me.LblMotorCombustibleMotor.AutoSize = True
        Me.LblMotorCombustibleMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCombustibleMotor.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustibleMotor.Location = New System.Drawing.Point(54, 49)
        Me.LblMotorCombustibleMotor.Name = "LblMotorCombustibleMotor"
        Me.LblMotorCombustibleMotor.Size = New System.Drawing.Size(99, 20)
        Me.LblMotorCombustibleMotor.TabIndex = 33
        Me.LblMotorCombustibleMotor.Text = "Combustible"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(93, 135)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(21, 20)
        Me.Label6.TabIndex = 37
        Me.Label6.Text = "%"
        '
        'LblMCombustibleP100M
        '
        Me.LblMCombustibleP100M.AutoSize = True
        Me.LblMCombustibleP100M.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblMCombustibleP100M.ForeColor = System.Drawing.Color.White
        Me.LblMCombustibleP100M.Location = New System.Drawing.Point(75, 80)
        Me.LblMCombustibleP100M.Name = "LblMCombustibleP100M"
        Me.LblMCombustibleP100M.Size = New System.Drawing.Size(56, 41)
        Me.LblMCombustibleP100M.TabIndex = 34
        Me.LblMCombustibleP100M.Text = "90"
        '
        'RgCombP100M
        '
        Me.RgCombP100M.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgCombP100M.ArrowColor = System.Drawing.Color.Silver
        Me.RgCombP100M.ArrowThickness = 0
        Me.RgCombP100M.ArrowVisible = False
        Me.RgCombP100M.BackColor = System.Drawing.Color.Transparent
        Me.RgCombP100M.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgCombP100M.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgCombP100M.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgCombP100M.Location = New System.Drawing.Point(1, 1)
        Me.RgCombP100M.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgCombP100M.Name = "RgCombP100M"
        Me.RgCombP100M.ProgressColor = System.Drawing.Color.LimeGreen
        Me.RgCombP100M.ProgressColor2 = System.Drawing.Color.Green
        Me.RgCombP100M.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCombP100M.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCombP100M.ProgressThickness = 6
        Me.RgCombP100M.ShowPercentage = False
        Me.RgCombP100M.Size = New System.Drawing.Size(195, 195)
        Me.RgCombP100M.TabIndex = 135
        Me.RgCombP100M.UseTransparentBackground = True
        Me.RgCombP100M.Value = 100
        '
        'PanelCargaM
        '
        Me.PanelCargaM.Controls.Add(Me.Label39)
        Me.PanelCargaM.Controls.Add(Me.LblBCargaMotorM)
        Me.PanelCargaM.Controls.Add(Me.LblMotorCargaMotor)
        Me.PanelCargaM.Controls.Add(Me.RgCargaM)
        Me.PanelCargaM.Location = New System.Drawing.Point(1002, 2)
        Me.PanelCargaM.Name = "PanelCargaM"
        Me.PanelCargaM.Size = New System.Drawing.Size(197, 197)
        Me.PanelCargaM.TabIndex = 148
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label39.ForeColor = System.Drawing.Color.White
        Me.Label39.Location = New System.Drawing.Point(88, 135)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(21, 20)
        Me.Label39.TabIndex = 121
        Me.Label39.Text = "%"
        '
        'LblBCargaMotorM
        '
        Me.LblBCargaMotorM.AutoSize = True
        Me.LblBCargaMotorM.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblBCargaMotorM.ForeColor = System.Drawing.Color.White
        Me.LblBCargaMotorM.Location = New System.Drawing.Point(71, 80)
        Me.LblBCargaMotorM.Name = "LblBCargaMotorM"
        Me.LblBCargaMotorM.Size = New System.Drawing.Size(55, 41)
        Me.LblBCargaMotorM.TabIndex = 120
        Me.LblBCargaMotorM.Text = "68"
        '
        'LblMotorCargaMotor
        '
        Me.LblMotorCargaMotor.AutoSize = True
        Me.LblMotorCargaMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCargaMotor.ForeColor = System.Drawing.Color.White
        Me.LblMotorCargaMotor.Location = New System.Drawing.Point(41, 49)
        Me.LblMotorCargaMotor.Name = "LblMotorCargaMotor"
        Me.LblMotorCargaMotor.Size = New System.Drawing.Size(114, 20)
        Me.LblMotorCargaMotor.TabIndex = 119
        Me.LblMotorCargaMotor.Text = "% Carga Motor"
        '
        'RgCargaM
        '
        Me.RgCargaM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgCargaM.ArrowColor = System.Drawing.Color.Silver
        Me.RgCargaM.ArrowThickness = 0
        Me.RgCargaM.ArrowVisible = False
        Me.RgCargaM.BackColor = System.Drawing.Color.Transparent
        Me.RgCargaM.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgCargaM.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgCargaM.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgCargaM.Location = New System.Drawing.Point(1, 1)
        Me.RgCargaM.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgCargaM.Name = "RgCargaM"
        Me.RgCargaM.ProgressColor = System.Drawing.Color.LimeGreen
        Me.RgCargaM.ProgressColor2 = System.Drawing.Color.Green
        Me.RgCargaM.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCargaM.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgCargaM.ProgressThickness = 6
        Me.RgCargaM.ShowPercentage = False
        Me.RgCargaM.Size = New System.Drawing.Size(195, 195)
        Me.RgCargaM.TabIndex = 139
        Me.RgCargaM.UseTransparentBackground = True
        Me.RgCargaM.Value = 68
        '
        'PanelRPMM
        '
        Me.PanelRPMM.Controls.Add(Me.LblRPM2)
        Me.PanelRPMM.Controls.Add(Me.LblRPMActualM)
        Me.PanelRPMM.Controls.Add(Me.LblRPM1M)
        Me.PanelRPMM.Controls.Add(Me.LblRPMDeseadaM)
        Me.PanelRPMM.Controls.Add(Me.RgRPMM)
        Me.PanelRPMM.Location = New System.Drawing.Point(404, 70)
        Me.PanelRPMM.Name = "PanelRPMM"
        Me.PanelRPMM.Size = New System.Drawing.Size(392, 392)
        Me.PanelRPMM.TabIndex = 149
        '
        'LblRPM2
        '
        Me.LblRPM2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPM2.AutoSize = True
        Me.LblRPM2.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM2.ForeColor = System.Drawing.Color.White
        Me.LblRPM2.Location = New System.Drawing.Point(140, 213)
        Me.LblRPM2.Name = "LblRPM2"
        Me.LblRPM2.Size = New System.Drawing.Size(113, 20)
        Me.LblRPM2.TabIndex = 20
        Me.LblRPM2.Text = "RPM Deseadas"
        '
        'LblRPMActualM
        '
        Me.LblRPMActualM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPMActualM.AutoSize = True
        Me.LblRPMActualM.Font = New System.Drawing.Font("Montserrat", 33.0!)
        Me.LblRPMActualM.ForeColor = System.Drawing.Color.White
        Me.LblRPMActualM.Location = New System.Drawing.Point(135, 103)
        Me.LblRPMActualM.Name = "LblRPMActualM"
        Me.LblRPMActualM.Size = New System.Drawing.Size(118, 61)
        Me.LblRPMActualM.TabIndex = 23
        Me.LblRPMActualM.Text = "1553"
        '
        'LblRPM1M
        '
        Me.LblRPM1M.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPM1M.AutoSize = True
        Me.LblRPM1M.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM1M.ForeColor = System.Drawing.Color.White
        Me.LblRPM1M.Location = New System.Drawing.Point(143, 78)
        Me.LblRPM1M.Name = "LblRPM1M"
        Me.LblRPM1M.Size = New System.Drawing.Size(106, 20)
        Me.LblRPM1M.TabIndex = 21
        Me.LblRPM1M.Text = "RPM Actuales"
        '
        'LblRPMDeseadaM
        '
        Me.LblRPMDeseadaM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPMDeseadaM.AutoSize = True
        Me.LblRPMDeseadaM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblRPMDeseadaM.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblRPMDeseadaM.ForeColor = System.Drawing.Color.White
        Me.LblRPMDeseadaM.Location = New System.Drawing.Point(159, 240)
        Me.LblRPMDeseadaM.Name = "LblRPMDeseadaM"
        Me.LblRPMDeseadaM.Size = New System.Drawing.Size(75, 37)
        Me.LblRPMDeseadaM.TabIndex = 22
        Me.LblRPMDeseadaM.Text = "1550"
        '
        'RgRPMM
        '
        Me.RgRPMM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgRPMM.ArrowColor = System.Drawing.Color.Silver
        Me.RgRPMM.ArrowThickness = 0
        Me.RgRPMM.ArrowVisible = False
        Me.RgRPMM.BackColor = System.Drawing.Color.Transparent
        Me.RgRPMM.FillColor = System.Drawing.Color.DimGray
        Me.RgRPMM.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgRPMM.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgRPMM.Location = New System.Drawing.Point(1, 1)
        Me.RgRPMM.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgRPMM.Name = "RgRPMM"
        Me.RgRPMM.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgRPMM.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgRPMM.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPMM.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPMM.ProgressThickness = 12
        Me.RgRPMM.ShowPercentage = False
        Me.RgRPMM.Size = New System.Drawing.Size(391, 391)
        Me.RgRPMM.TabIndex = 109
        Me.RgRPMM.UseTransparentBackground = True
        Me.RgRPMM.Value = 80
        '
        'Transmision
        '
        Me.Transmision.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Transmision.Controls.Add(Me.Title2)
        Me.Transmision.Controls.Add(Me.PanelDiagT)
        Me.Transmision.Controls.Add(Me.PanelErroresT)
        Me.Transmision.Controls.Add(Me.PanelRPMT)
        Me.Transmision.Controls.Add(Me.PanelPresionT)
        Me.Transmision.Controls.Add(Me.PanelMarchaT)
        Me.Transmision.Controls.Add(Me.PanelTempT)
        Me.Transmision.Location = New System.Drawing.Point(4, 4)
        Me.Transmision.Margin = New System.Windows.Forms.Padding(0)
        Me.Transmision.Name = "Transmision"
        Me.Transmision.Size = New System.Drawing.Size(1201, 630)
        Me.Transmision.TabIndex = 0
        Me.Transmision.Text = "Transmisión"
        Me.Transmision.ToolTipText = "Pestaña de parametros de transmisión"
        '
        'Title2
        '
        Me.Title2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Title2.AutoSize = True
        Me.Title2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Title2.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.Title2.ForeColor = System.Drawing.Color.White
        Me.Title2.Location = New System.Drawing.Point(528, 15)
        Me.Title2.Name = "Title2"
        Me.Title2.Size = New System.Drawing.Size(226, 47)
        Me.Title2.TabIndex = 143
        Me.Title2.Text = "Transmisión"
        '
        'PanelDiagT
        '
        Me.PanelDiagT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PanelDiagT.BackColor = System.Drawing.Color.Transparent
        Me.PanelDiagT.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelDiagT.BorderRadius = 6
        Me.PanelDiagT.BorderThickness = 1
        Me.PanelDiagT.Controls.Add(Me.ErrorProgressT2)
        Me.PanelDiagT.Controls.Add(Me.HSep1)
        Me.PanelDiagT.Controls.Add(Me.PbAceiteTransmision)
        Me.PanelDiagT.Controls.Add(Me.LblAceiteTransmision)
        Me.PanelDiagT.Controls.Add(Me.Label53)
        Me.PanelDiagT.Controls.Add(Me.LblTransmisionDiag1)
        Me.PanelDiagT.Controls.Add(Me.LblTPresAceite)
        Me.PanelDiagT.Controls.Add(Me.LblTTempAceite)
        Me.PanelDiagT.Controls.Add(Me.PbTPresAceite)
        Me.PanelDiagT.Controls.Add(Me.PbTTempAceite)
        Me.PanelDiagT.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.PanelDiagT.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelDiagT.FillColor = System.Drawing.Color.Transparent
        Me.PanelDiagT.Location = New System.Drawing.Point(604, 466)
        Me.PanelDiagT.Name = "PanelDiagT"
        Me.PanelDiagT.Size = New System.Drawing.Size(597, 164)
        Me.PanelDiagT.TabIndex = 118
        '
        'ErrorProgressT2
        '
        Me.ErrorProgressT2.AnimationSpeed = 50
        Me.ErrorProgressT2.AutoStart = True
        Me.ErrorProgressT2.Location = New System.Drawing.Point(253, 51)
        Me.ErrorProgressT2.Name = "ErrorProgressT2"
        Me.ErrorProgressT2.Size = New System.Drawing.Size(90, 90)
        Me.ErrorProgressT2.TabIndex = 109
        Me.ErrorProgressT2.UseTransparentBackground = True
        '
        'HSep1
        '
        Me.HSep1.Location = New System.Drawing.Point(6, 92)
        Me.HSep1.Name = "HSep1"
        Me.HSep1.Size = New System.Drawing.Size(586, 10)
        Me.HSep1.TabIndex = 108
        '
        'PbAceiteTransmision
        '
        Me.PbAceiteTransmision.BackColor = System.Drawing.Color.Transparent
        Me.PbAceiteTransmision.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAceiteTransmision.Image = CType(resources.GetObject("PbAceiteTransmision.Image"), System.Drawing.Image)
        Me.PbAceiteTransmision.ImageRotate = 0!
        Me.PbAceiteTransmision.Location = New System.Drawing.Point(384, 115)
        Me.PbAceiteTransmision.Name = "PbAceiteTransmision"
        Me.PbAceiteTransmision.Size = New System.Drawing.Size(25, 25)
        Me.PbAceiteTransmision.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAceiteTransmision.TabIndex = 107
        Me.PbAceiteTransmision.TabStop = False
        Me.PbAceiteTransmision.UseTransparentBackground = True
        '
        'LblAceiteTransmision
        '
        Me.LblAceiteTransmision.AutoSize = True
        Me.LblAceiteTransmision.BackColor = System.Drawing.Color.Transparent
        Me.LblAceiteTransmision.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblAceiteTransmision.ForeColor = System.Drawing.Color.White
        Me.LblAceiteTransmision.Location = New System.Drawing.Point(415, 118)
        Me.LblAceiteTransmision.Name = "LblAceiteTransmision"
        Me.LblAceiteTransmision.Size = New System.Drawing.Size(32, 20)
        Me.LblAceiteTransmision.TabIndex = 106
        Me.LblAceiteTransmision.Text = "100"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.Color.Transparent
        Me.Label53.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label53.ForeColor = System.Drawing.Color.White
        Me.Label53.Location = New System.Drawing.Point(458, 118)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(34, 20)
        Me.Label53.TabIndex = 105
        Me.Label53.Text = "[Hr]"
        '
        'LblTransmisionDiag1
        '
        Me.LblTransmisionDiag1.AutoSize = True
        Me.LblTransmisionDiag1.BackColor = System.Drawing.Color.Transparent
        Me.LblTransmisionDiag1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransmisionDiag1.ForeColor = System.Drawing.Color.White
        Me.LblTransmisionDiag1.Location = New System.Drawing.Point(81, 118)
        Me.LblTransmisionDiag1.Name = "LblTransmisionDiag1"
        Me.LblTransmisionDiag1.Size = New System.Drawing.Size(266, 20)
        Me.LblTransmisionDiag1.TabIndex = 104
        Me.LblTransmisionDiag1.Text = "Reemplazo de Aceite de Transmisión"
        '
        'LblTPresAceite
        '
        Me.LblTPresAceite.AutoSize = True
        Me.LblTPresAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblTPresAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTPresAceite.ForeColor = System.Drawing.Color.White
        Me.LblTPresAceite.Location = New System.Drawing.Point(402, 44)
        Me.LblTPresAceite.Name = "LblTPresAceite"
        Me.LblTPresAceite.Size = New System.Drawing.Size(131, 20)
        Me.LblTPresAceite.TabIndex = 74
        Me.LblTPresAceite.Text = "Presión de Aceite"
        '
        'LblTTempAceite
        '
        Me.LblTTempAceite.AutoSize = True
        Me.LblTTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblTTempAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblTTempAceite.Location = New System.Drawing.Point(108, 44)
        Me.LblTTempAceite.Name = "LblTTempAceite"
        Me.LblTTempAceite.Size = New System.Drawing.Size(166, 20)
        Me.LblTTempAceite.TabIndex = 75
        Me.LblTTempAceite.Text = "Temperatura de Aceite"
        '
        'PbTPresAceite
        '
        Me.PbTPresAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbTPresAceite.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbTPresAceite.ImageRotate = 0!
        Me.PbTPresAceite.Location = New System.Drawing.Point(353, 42)
        Me.PbTPresAceite.Name = "PbTPresAceite"
        Me.PbTPresAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbTPresAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbTPresAceite.TabIndex = 72
        Me.PbTPresAceite.TabStop = False
        Me.PbTPresAceite.UseTransparentBackground = True
        '
        'PbTTempAceite
        '
        Me.PbTTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbTTempAceite.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbTTempAceite.ImageRotate = 0!
        Me.PbTTempAceite.Location = New System.Drawing.Point(59, 42)
        Me.PbTTempAceite.Name = "PbTTempAceite"
        Me.PbTTempAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbTTempAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbTTempAceite.TabIndex = 73
        Me.PbTTempAceite.TabStop = False
        Me.PbTTempAceite.UseTransparentBackground = True
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblDiagnosticoTrans)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(597, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblDiagnosticoTrans
        '
        Me.LblDiagnosticoTrans.AutoSize = True
        Me.LblDiagnosticoTrans.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDiagnosticoTrans.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblDiagnosticoTrans.Location = New System.Drawing.Point(283, 4)
        Me.LblDiagnosticoTrans.Name = "LblDiagnosticoTrans"
        Me.LblDiagnosticoTrans.Size = New System.Drawing.Size(95, 20)
        Me.LblDiagnosticoTrans.TabIndex = 19
        Me.LblDiagnosticoTrans.Text = "Diagnóstico"
        '
        'PanelErroresT
        '
        Me.PanelErroresT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PanelErroresT.BackColor = System.Drawing.Color.Transparent
        Me.PanelErroresT.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelErroresT.BorderRadius = 6
        Me.PanelErroresT.BorderThickness = 1
        Me.PanelErroresT.Controls.Add(Me.ErrorProgressT1)
        Me.PanelErroresT.Controls.Add(Me.BtnBorrarActualT)
        Me.PanelErroresT.Controls.Add(Me.BtnSiguienteT)
        Me.PanelErroresT.Controls.Add(Me.BtnAnteriorT)
        Me.PanelErroresT.Controls.Add(Me.LblDescripcionT)
        Me.PanelErroresT.Controls.Add(Me.LblInfoDescripT)
        Me.PanelErroresT.Controls.Add(Me.LblUnidadT)
        Me.PanelErroresT.Controls.Add(Me.LblInfoUnidadT)
        Me.PanelErroresT.Controls.Add(Me.LblActivoT)
        Me.PanelErroresT.Controls.Add(Me.LblInfoActivoT)
        Me.PanelErroresT.Controls.Add(Me.LblErroresTotalesT)
        Me.PanelErroresT.Controls.Add(Me.LblErrorNT)
        Me.PanelErroresT.Controls.Add(Me.LblOCT)
        Me.PanelErroresT.Controls.Add(Me.LblFMIT)
        Me.PanelErroresT.Controls.Add(Me.LblSPNT)
        Me.PanelErroresT.Controls.Add(Me.LblInfoErrTotT)
        Me.PanelErroresT.Controls.Add(Me.LblInfoErrorNT)
        Me.PanelErroresT.Controls.Add(Me.LblInfoOCT)
        Me.PanelErroresT.Controls.Add(Me.LblInfoFMIT)
        Me.PanelErroresT.Controls.Add(Me.LblInfoSPNT)
        Me.PanelErroresT.Controls.Add(Me.Guna2CustomGradientPanel3)
        Me.PanelErroresT.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelErroresT.FillColor = System.Drawing.Color.Transparent
        Me.PanelErroresT.Location = New System.Drawing.Point(0, 466)
        Me.PanelErroresT.Name = "PanelErroresT"
        Me.PanelErroresT.Size = New System.Drawing.Size(598, 164)
        Me.PanelErroresT.TabIndex = 117
        '
        'ErrorProgressT1
        '
        Me.ErrorProgressT1.AnimationSpeed = 50
        Me.ErrorProgressT1.AutoStart = True
        Me.ErrorProgressT1.Location = New System.Drawing.Point(253, 51)
        Me.ErrorProgressT1.Name = "ErrorProgressT1"
        Me.ErrorProgressT1.Size = New System.Drawing.Size(90, 90)
        Me.ErrorProgressT1.TabIndex = 80
        Me.ErrorProgressT1.UseTransparentBackground = True
        '
        'BtnBorrarActualT
        '
        Me.BtnBorrarActualT.Animated = True
        Me.BtnBorrarActualT.BorderColor = System.Drawing.Color.White
        Me.BtnBorrarActualT.BorderRadius = 5
        Me.BtnBorrarActualT.BorderThickness = 1
        Me.BtnBorrarActualT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnBorrarActualT.FillColor = System.Drawing.Color.Transparent
        Me.BtnBorrarActualT.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnBorrarActualT.ForeColor = System.Drawing.Color.White
        Me.BtnBorrarActualT.Image = Global.sFRAC.My.Resources.Resources.white_delete
        Me.BtnBorrarActualT.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnBorrarActualT.ImageSize = New System.Drawing.Size(19, 19)
        Me.BtnBorrarActualT.Location = New System.Drawing.Point(30, 118)
        Me.BtnBorrarActualT.Name = "BtnBorrarActualT"
        Me.BtnBorrarActualT.Size = New System.Drawing.Size(40, 40)
        Me.BtnBorrarActualT.TabIndex = 78
        '
        'BtnSiguienteT
        '
        Me.BtnSiguienteT.Animated = True
        Me.BtnSiguienteT.BorderColor = System.Drawing.Color.White
        Me.BtnSiguienteT.BorderRadius = 5
        Me.BtnSiguienteT.BorderThickness = 1
        Me.BtnSiguienteT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSiguienteT.FillColor = System.Drawing.Color.Transparent
        Me.BtnSiguienteT.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnSiguienteT.ForeColor = System.Drawing.Color.White
        Me.BtnSiguienteT.Image = Global.sFRAC.My.Resources.Resources.right_arrow
        Me.BtnSiguienteT.Location = New System.Drawing.Point(316, 141)
        Me.BtnSiguienteT.Name = "BtnSiguienteT"
        Me.BtnSiguienteT.Size = New System.Drawing.Size(80, 20)
        Me.BtnSiguienteT.TabIndex = 77
        '
        'BtnAnteriorT
        '
        Me.BtnAnteriorT.Animated = True
        Me.BtnAnteriorT.BorderColor = System.Drawing.Color.White
        Me.BtnAnteriorT.BorderRadius = 5
        Me.BtnAnteriorT.BorderThickness = 1
        Me.BtnAnteriorT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAnteriorT.FillColor = System.Drawing.Color.Transparent
        Me.BtnAnteriorT.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnAnteriorT.ForeColor = System.Drawing.Color.White
        Me.BtnAnteriorT.Image = Global.sFRAC.My.Resources.Resources.left_arrow
        Me.BtnAnteriorT.Location = New System.Drawing.Point(230, 141)
        Me.BtnAnteriorT.Name = "BtnAnteriorT"
        Me.BtnAnteriorT.Size = New System.Drawing.Size(80, 20)
        Me.BtnAnteriorT.TabIndex = 76
        '
        'LblDescripcionT
        '
        Me.LblDescripcionT.BackColor = System.Drawing.Color.Transparent
        Me.LblDescripcionT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDescripcionT.ForeColor = System.Drawing.Color.White
        Me.LblDescripcionT.Location = New System.Drawing.Point(101, 93)
        Me.LblDescripcionT.Name = "LblDescripcionT"
        Me.LblDescripcionT.Size = New System.Drawing.Size(485, 45)
        Me.LblDescripcionT.TabIndex = 54
        Me.LblDescripcionT.Text = "Drescripcion"
        '
        'LblInfoDescripT
        '
        Me.LblInfoDescripT.AutoSize = True
        Me.LblInfoDescripT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoDescripT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoDescripT.ForeColor = System.Drawing.Color.White
        Me.LblInfoDescripT.Location = New System.Drawing.Point(5, 93)
        Me.LblInfoDescripT.Name = "LblInfoDescripT"
        Me.LblInfoDescripT.Size = New System.Drawing.Size(96, 20)
        Me.LblInfoDescripT.TabIndex = 53
        Me.LblInfoDescripT.Text = "Descripción:"
        '
        'LblUnidadT
        '
        Me.LblUnidadT.AutoSize = True
        Me.LblUnidadT.BackColor = System.Drawing.Color.Transparent
        Me.LblUnidadT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidadT.ForeColor = System.Drawing.Color.White
        Me.LblUnidadT.Location = New System.Drawing.Point(529, 31)
        Me.LblUnidadT.Name = "LblUnidadT"
        Me.LblUnidadT.Size = New System.Drawing.Size(59, 20)
        Me.LblUnidadT.TabIndex = 52
        Me.LblUnidadT.Text = "Unidad"
        '
        'LblInfoUnidadT
        '
        Me.LblInfoUnidadT.AutoSize = True
        Me.LblInfoUnidadT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoUnidadT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoUnidadT.ForeColor = System.Drawing.Color.White
        Me.LblInfoUnidadT.Location = New System.Drawing.Point(463, 31)
        Me.LblInfoUnidadT.Name = "LblInfoUnidadT"
        Me.LblInfoUnidadT.Size = New System.Drawing.Size(62, 20)
        Me.LblInfoUnidadT.TabIndex = 51
        Me.LblInfoUnidadT.Text = "Unidad:"
        '
        'LblActivoT
        '
        Me.LblActivoT.AutoSize = True
        Me.LblActivoT.BackColor = System.Drawing.Color.Transparent
        Me.LblActivoT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblActivoT.ForeColor = System.Drawing.Color.White
        Me.LblActivoT.Location = New System.Drawing.Point(69, 62)
        Me.LblActivoT.Name = "LblActivoT"
        Me.LblActivoT.Size = New System.Drawing.Size(32, 20)
        Me.LblActivoT.TabIndex = 50
        Me.LblActivoT.Text = "NO"
        '
        'LblInfoActivoT
        '
        Me.LblInfoActivoT.AutoSize = True
        Me.LblInfoActivoT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoActivoT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoActivoT.ForeColor = System.Drawing.Color.White
        Me.LblInfoActivoT.Location = New System.Drawing.Point(5, 62)
        Me.LblInfoActivoT.Name = "LblInfoActivoT"
        Me.LblInfoActivoT.Size = New System.Drawing.Size(61, 20)
        Me.LblInfoActivoT.TabIndex = 49
        Me.LblInfoActivoT.Text = "Activo: "
        '
        'LblErroresTotalesT
        '
        Me.LblErroresTotalesT.AutoSize = True
        Me.LblErroresTotalesT.BackColor = System.Drawing.Color.Transparent
        Me.LblErroresTotalesT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErroresTotalesT.ForeColor = System.Drawing.Color.White
        Me.LblErroresTotalesT.Location = New System.Drawing.Point(232, 62)
        Me.LblErroresTotalesT.Name = "LblErroresTotalesT"
        Me.LblErroresTotalesT.Size = New System.Drawing.Size(41, 20)
        Me.LblErroresTotalesT.TabIndex = 47
        Me.LblErroresTotalesT.Text = "1000"
        '
        'LblErrorNT
        '
        Me.LblErrorNT.AutoSize = True
        Me.LblErrorNT.BackColor = System.Drawing.Color.Transparent
        Me.LblErrorNT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErrorNT.ForeColor = System.Drawing.Color.White
        Me.LblErrorNT.Location = New System.Drawing.Point(402, 31)
        Me.LblErrorNT.Name = "LblErrorNT"
        Me.LblErrorNT.Size = New System.Drawing.Size(41, 20)
        Me.LblErrorNT.TabIndex = 46
        Me.LblErrorNT.Text = "1000"
        '
        'LblOCT
        '
        Me.LblOCT.AutoSize = True
        Me.LblOCT.BackColor = System.Drawing.Color.Transparent
        Me.LblOCT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblOCT.ForeColor = System.Drawing.Color.White
        Me.LblOCT.Location = New System.Drawing.Point(264, 31)
        Me.LblOCT.Name = "LblOCT"
        Me.LblOCT.Size = New System.Drawing.Size(41, 20)
        Me.LblOCT.TabIndex = 45
        Me.LblOCT.Text = "1000"
        '
        'LblFMIT
        '
        Me.LblFMIT.AutoSize = True
        Me.LblFMIT.BackColor = System.Drawing.Color.Transparent
        Me.LblFMIT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblFMIT.ForeColor = System.Drawing.Color.White
        Me.LblFMIT.Location = New System.Drawing.Point(159, 31)
        Me.LblFMIT.Name = "LblFMIT"
        Me.LblFMIT.Size = New System.Drawing.Size(41, 20)
        Me.LblFMIT.TabIndex = 44
        Me.LblFMIT.Text = "1000"
        '
        'LblSPNT
        '
        Me.LblSPNT.AutoSize = True
        Me.LblSPNT.BackColor = System.Drawing.Color.Transparent
        Me.LblSPNT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblSPNT.ForeColor = System.Drawing.Color.White
        Me.LblSPNT.Location = New System.Drawing.Point(50, 31)
        Me.LblSPNT.Name = "LblSPNT"
        Me.LblSPNT.Size = New System.Drawing.Size(41, 20)
        Me.LblSPNT.TabIndex = 43
        Me.LblSPNT.Text = "1000"
        '
        'LblInfoErrTotT
        '
        Me.LblInfoErrTotT.AutoSize = True
        Me.LblInfoErrTotT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoErrTotT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoErrTotT.ForeColor = System.Drawing.Color.White
        Me.LblInfoErrTotT.Location = New System.Drawing.Point(117, 62)
        Me.LblInfoErrTotT.Name = "LblInfoErrTotT"
        Me.LblInfoErrTotT.Size = New System.Drawing.Size(116, 20)
        Me.LblInfoErrTotT.TabIndex = 41
        Me.LblInfoErrTotT.Text = "Errores Totales:"
        '
        'LblInfoErrorNT
        '
        Me.LblInfoErrorNT.AutoSize = True
        Me.LblInfoErrorNT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoErrorNT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoErrorNT.ForeColor = System.Drawing.Color.White
        Me.LblInfoErrorNT.Location = New System.Drawing.Point(331, 31)
        Me.LblInfoErrorNT.Name = "LblInfoErrorNT"
        Me.LblInfoErrorNT.Size = New System.Drawing.Size(69, 20)
        Me.LblInfoErrorNT.TabIndex = 40
        Me.LblInfoErrorNT.Text = "Error Nº:"
        '
        'LblInfoOCT
        '
        Me.LblInfoOCT.AutoSize = True
        Me.LblInfoOCT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoOCT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoOCT.ForeColor = System.Drawing.Color.White
        Me.LblInfoOCT.Location = New System.Drawing.Point(226, 31)
        Me.LblInfoOCT.Name = "LblInfoOCT"
        Me.LblInfoOCT.Size = New System.Drawing.Size(34, 20)
        Me.LblInfoOCT.TabIndex = 39
        Me.LblInfoOCT.Text = "OC:"
        '
        'LblInfoFMIT
        '
        Me.LblInfoFMIT.AutoSize = True
        Me.LblInfoFMIT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoFMIT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoFMIT.ForeColor = System.Drawing.Color.White
        Me.LblInfoFMIT.Location = New System.Drawing.Point(117, 31)
        Me.LblInfoFMIT.Name = "LblInfoFMIT"
        Me.LblInfoFMIT.Size = New System.Drawing.Size(38, 20)
        Me.LblInfoFMIT.TabIndex = 38
        Me.LblInfoFMIT.Text = "FMI:"
        '
        'LblInfoSPNT
        '
        Me.LblInfoSPNT.AutoSize = True
        Me.LblInfoSPNT.BackColor = System.Drawing.Color.Transparent
        Me.LblInfoSPNT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfoSPNT.ForeColor = System.Drawing.Color.White
        Me.LblInfoSPNT.Location = New System.Drawing.Point(5, 31)
        Me.LblInfoSPNT.Name = "LblInfoSPNT"
        Me.LblInfoSPNT.Size = New System.Drawing.Size(42, 20)
        Me.LblInfoSPNT.TabIndex = 37
        Me.LblInfoSPNT.Text = "SPN:"
        '
        'Guna2CustomGradientPanel3
        '
        Me.Guna2CustomGradientPanel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.BorderRadius = 6
        Me.Guna2CustomGradientPanel3.Controls.Add(Me.LblErroresTrans)
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel3.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel3.Name = "Guna2CustomGradientPanel3"
        Me.Guna2CustomGradientPanel3.Size = New System.Drawing.Size(598, 26)
        Me.Guna2CustomGradientPanel3.TabIndex = 19
        '
        'LblErroresTrans
        '
        Me.LblErroresTrans.AutoSize = True
        Me.LblErroresTrans.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErroresTrans.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblErroresTrans.Location = New System.Drawing.Point(284, 4)
        Me.LblErroresTrans.Name = "LblErroresTrans"
        Me.LblErroresTrans.Size = New System.Drawing.Size(60, 20)
        Me.LblErroresTrans.TabIndex = 19
        Me.LblErroresTrans.Text = "Errores"
        '
        'PanelRPMT
        '
        Me.PanelRPMT.Controls.Add(Me.Label74)
        Me.PanelRPMT.Controls.Add(Me.LblTransmisionRPMOut)
        Me.PanelRPMT.Controls.Add(Me.LblTRpmOutT)
        Me.PanelRPMT.Controls.Add(Me.Guna2RadialGauge13)
        Me.PanelRPMT.Location = New System.Drawing.Point(604, 170)
        Me.PanelRPMT.Name = "PanelRPMT"
        Me.PanelRPMT.Size = New System.Drawing.Size(291, 291)
        Me.PanelRPMT.TabIndex = 145
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label74.ForeColor = System.Drawing.Color.White
        Me.Label74.Location = New System.Drawing.Point(124, 203)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(42, 20)
        Me.Label74.TabIndex = 28
        Me.Label74.Text = "RPM"
        '
        'LblTransmisionRPMOut
        '
        Me.LblTransmisionRPMOut.AutoSize = True
        Me.LblTransmisionRPMOut.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransmisionRPMOut.ForeColor = System.Drawing.Color.White
        Me.LblTransmisionRPMOut.Location = New System.Drawing.Point(109, 60)
        Me.LblTransmisionRPMOut.Name = "LblTransmisionRPMOut"
        Me.LblTransmisionRPMOut.Size = New System.Drawing.Size(73, 20)
        Me.LblTransmisionRPMOut.TabIndex = 24
        Me.LblTransmisionRPMOut.Text = "RPM Out"
        '
        'LblTRpmOutT
        '
        Me.LblTRpmOutT.AutoSize = True
        Me.LblTRpmOutT.Font = New System.Drawing.Font("Montserrat", 33.0!)
        Me.LblTRpmOutT.ForeColor = System.Drawing.Color.White
        Me.LblTRpmOutT.Location = New System.Drawing.Point(78, 108)
        Me.LblTRpmOutT.Name = "LblTRpmOutT"
        Me.LblTRpmOutT.Size = New System.Drawing.Size(130, 61)
        Me.LblTRpmOutT.TabIndex = 25
        Me.LblTRpmOutT.Text = "1000"
        '
        'Guna2RadialGauge13
        '
        Me.Guna2RadialGauge13.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge13.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge13.ArrowThickness = 0
        Me.Guna2RadialGauge13.ArrowVisible = False
        Me.Guna2RadialGauge13.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge13.FillColor = System.Drawing.Color.DimGray
        Me.Guna2RadialGauge13.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.Guna2RadialGauge13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge13.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge13.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge13.Name = "Guna2RadialGauge13"
        Me.Guna2RadialGauge13.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.Guna2RadialGauge13.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.Guna2RadialGauge13.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge13.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge13.ProgressThickness = 12
        Me.Guna2RadialGauge13.ShowPercentage = False
        Me.Guna2RadialGauge13.Size = New System.Drawing.Size(289, 289)
        Me.Guna2RadialGauge13.TabIndex = 133
        Me.Guna2RadialGauge13.UseTransparentBackground = True
        Me.Guna2RadialGauge13.Value = 80
        '
        'PanelPresionT
        '
        Me.PanelPresionT.Controls.Add(Me.Label75)
        Me.PanelPresionT.Controls.Add(Me.LblTransmisionPresion)
        Me.PanelPresionT.Controls.Add(Me.LblTPresionT)
        Me.PanelPresionT.Controls.Add(Me.Guna2RadialGauge14)
        Me.PanelPresionT.Location = New System.Drawing.Point(309, 170)
        Me.PanelPresionT.Name = "PanelPresionT"
        Me.PanelPresionT.Size = New System.Drawing.Size(291, 291)
        Me.PanelPresionT.TabIndex = 145
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label75.ForeColor = System.Drawing.Color.White
        Me.Label75.Location = New System.Drawing.Point(129, 203)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(32, 20)
        Me.Label75.TabIndex = 27
        Me.Label75.Text = "PSI"
        '
        'LblTransmisionPresion
        '
        Me.LblTransmisionPresion.AutoSize = True
        Me.LblTransmisionPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransmisionPresion.ForeColor = System.Drawing.Color.White
        Me.LblTransmisionPresion.Location = New System.Drawing.Point(114, 60)
        Me.LblTransmisionPresion.Name = "LblTransmisionPresion"
        Me.LblTransmisionPresion.Size = New System.Drawing.Size(62, 20)
        Me.LblTransmisionPresion.TabIndex = 21
        Me.LblTransmisionPresion.Text = "Presión"
        '
        'LblTPresionT
        '
        Me.LblTPresionT.AutoSize = True
        Me.LblTPresionT.Font = New System.Drawing.Font("Montserrat", 33.0!)
        Me.LblTPresionT.ForeColor = System.Drawing.Color.White
        Me.LblTPresionT.Location = New System.Drawing.Point(117, 108)
        Me.LblTPresionT.Name = "LblTPresionT"
        Me.LblTPresionT.Size = New System.Drawing.Size(56, 61)
        Me.LblTPresionT.TabIndex = 23
        Me.LblTPresionT.Text = "0"
        '
        'Guna2RadialGauge14
        '
        Me.Guna2RadialGauge14.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge14.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge14.ArrowThickness = 0
        Me.Guna2RadialGauge14.ArrowVisible = False
        Me.Guna2RadialGauge14.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge14.FillColor = System.Drawing.Color.DimGray
        Me.Guna2RadialGauge14.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.Guna2RadialGauge14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge14.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge14.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge14.Name = "Guna2RadialGauge14"
        Me.Guna2RadialGauge14.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.Guna2RadialGauge14.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.Guna2RadialGauge14.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge14.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge14.ProgressThickness = 12
        Me.Guna2RadialGauge14.ShowPercentage = False
        Me.Guna2RadialGauge14.Size = New System.Drawing.Size(289, 289)
        Me.Guna2RadialGauge14.TabIndex = 134
        Me.Guna2RadialGauge14.UseTransparentBackground = True
        Me.Guna2RadialGauge14.Value = 80
        '
        'PanelMarchaT
        '
        Me.PanelMarchaT.Controls.Add(Me.LblTransmisionMarchaDeseada)
        Me.PanelMarchaT.Controls.Add(Me.LblTransmisionMarchaActual)
        Me.PanelMarchaT.Controls.Add(Me.LblMarchaDeseadaT)
        Me.PanelMarchaT.Controls.Add(Me.LblMarchaActualT)
        Me.PanelMarchaT.Controls.Add(Me.Guna2RadialGauge15)
        Me.PanelMarchaT.Location = New System.Drawing.Point(14, 14)
        Me.PanelMarchaT.Name = "PanelMarchaT"
        Me.PanelMarchaT.Size = New System.Drawing.Size(291, 291)
        Me.PanelMarchaT.TabIndex = 145
        '
        'LblTransmisionMarchaDeseada
        '
        Me.LblTransmisionMarchaDeseada.AutoSize = True
        Me.LblTransmisionMarchaDeseada.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransmisionMarchaDeseada.ForeColor = System.Drawing.Color.White
        Me.LblTransmisionMarchaDeseada.Location = New System.Drawing.Point(83, 162)
        Me.LblTransmisionMarchaDeseada.Name = "LblTransmisionMarchaDeseada"
        Me.LblTransmisionMarchaDeseada.Size = New System.Drawing.Size(125, 20)
        Me.LblTransmisionMarchaDeseada.TabIndex = 20
        Me.LblTransmisionMarchaDeseada.Text = "Marcha Deseada"
        '
        'LblTransmisionMarchaActual
        '
        Me.LblTransmisionMarchaActual.AutoSize = True
        Me.LblTransmisionMarchaActual.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransmisionMarchaActual.ForeColor = System.Drawing.Color.White
        Me.LblTransmisionMarchaActual.Location = New System.Drawing.Point(90, 60)
        Me.LblTransmisionMarchaActual.Name = "LblTransmisionMarchaActual"
        Me.LblTransmisionMarchaActual.Size = New System.Drawing.Size(110, 20)
        Me.LblTransmisionMarchaActual.TabIndex = 21
        Me.LblTransmisionMarchaActual.Text = "Marcha Actual"
        '
        'LblMarchaDeseadaT
        '
        Me.LblMarchaDeseadaT.AutoSize = True
        Me.LblMarchaDeseadaT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblMarchaDeseadaT.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblMarchaDeseadaT.ForeColor = System.Drawing.Color.White
        Me.LblMarchaDeseadaT.Location = New System.Drawing.Point(129, 195)
        Me.LblMarchaDeseadaT.Name = "LblMarchaDeseadaT"
        Me.LblMarchaDeseadaT.Size = New System.Drawing.Size(32, 37)
        Me.LblMarchaDeseadaT.TabIndex = 22
        Me.LblMarchaDeseadaT.Text = "3"
        '
        'LblMarchaActualT
        '
        Me.LblMarchaActualT.AutoSize = True
        Me.LblMarchaActualT.Font = New System.Drawing.Font("Montserrat", 33.0!)
        Me.LblMarchaActualT.ForeColor = System.Drawing.Color.White
        Me.LblMarchaActualT.Location = New System.Drawing.Point(119, 93)
        Me.LblMarchaActualT.Name = "LblMarchaActualT"
        Me.LblMarchaActualT.Size = New System.Drawing.Size(52, 61)
        Me.LblMarchaActualT.TabIndex = 23
        Me.LblMarchaActualT.Text = "3"
        '
        'Guna2RadialGauge15
        '
        Me.Guna2RadialGauge15.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge15.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge15.ArrowThickness = 0
        Me.Guna2RadialGauge15.ArrowVisible = False
        Me.Guna2RadialGauge15.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge15.FillColor = System.Drawing.Color.DimGray
        Me.Guna2RadialGauge15.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.Guna2RadialGauge15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge15.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge15.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge15.Name = "Guna2RadialGauge15"
        Me.Guna2RadialGauge15.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.Guna2RadialGauge15.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.Guna2RadialGauge15.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge15.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge15.ProgressThickness = 12
        Me.Guna2RadialGauge15.ShowPercentage = False
        Me.Guna2RadialGauge15.Size = New System.Drawing.Size(289, 289)
        Me.Guna2RadialGauge15.TabIndex = 135
        Me.Guna2RadialGauge15.UseTransparentBackground = True
        Me.Guna2RadialGauge15.Value = 80
        '
        'PanelTempT
        '
        Me.PanelTempT.Controls.Add(Me.Label76)
        Me.PanelTempT.Controls.Add(Me.LblTransmisionTemp)
        Me.PanelTempT.Controls.Add(Me.LblTTempT)
        Me.PanelTempT.Controls.Add(Me.Guna2RadialGauge12)
        Me.PanelTempT.Location = New System.Drawing.Point(899, 14)
        Me.PanelTempT.Name = "PanelTempT"
        Me.PanelTempT.Size = New System.Drawing.Size(291, 291)
        Me.PanelTempT.TabIndex = 144
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label76.ForeColor = System.Drawing.Color.White
        Me.Label76.Location = New System.Drawing.Point(133, 203)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(25, 20)
        Me.Label76.TabIndex = 26
        Me.Label76.Text = "ºC"
        '
        'LblTransmisionTemp
        '
        Me.LblTransmisionTemp.AutoSize = True
        Me.LblTransmisionTemp.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransmisionTemp.ForeColor = System.Drawing.Color.White
        Me.LblTransmisionTemp.Location = New System.Drawing.Point(96, 60)
        Me.LblTransmisionTemp.Name = "LblTransmisionTemp"
        Me.LblTransmisionTemp.Size = New System.Drawing.Size(97, 20)
        Me.LblTransmisionTemp.TabIndex = 20
        Me.LblTransmisionTemp.Text = "Temperatura"
        '
        'LblTTempT
        '
        Me.LblTTempT.AutoSize = True
        Me.LblTTempT.Font = New System.Drawing.Font("Montserrat", 33.0!)
        Me.LblTTempT.ForeColor = System.Drawing.Color.White
        Me.LblTTempT.Location = New System.Drawing.Point(104, 108)
        Me.LblTTempT.Name = "LblTTempT"
        Me.LblTTempT.Size = New System.Drawing.Size(81, 61)
        Me.LblTTempT.TabIndex = 22
        Me.LblTTempT.Text = "70"
        '
        'Guna2RadialGauge12
        '
        Me.Guna2RadialGauge12.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge12.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge12.ArrowThickness = 0
        Me.Guna2RadialGauge12.ArrowVisible = False
        Me.Guna2RadialGauge12.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge12.FillColor = System.Drawing.Color.DimGray
        Me.Guna2RadialGauge12.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.Guna2RadialGauge12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge12.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge12.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge12.Name = "Guna2RadialGauge12"
        Me.Guna2RadialGauge12.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.Guna2RadialGauge12.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.Guna2RadialGauge12.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge12.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge12.ProgressThickness = 12
        Me.Guna2RadialGauge12.ShowPercentage = False
        Me.Guna2RadialGauge12.Size = New System.Drawing.Size(289, 289)
        Me.Guna2RadialGauge12.TabIndex = 132
        Me.Guna2RadialGauge12.UseTransparentBackground = True
        Me.Guna2RadialGauge12.Value = 80
        '
        'Bomba
        '
        Me.Bomba.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Bomba.Controls.Add(Me.Title3)
        Me.Bomba.Controls.Add(Me.PanelDiagB)
        Me.Bomba.Controls.Add(Me.PanelCaudalB)
        Me.Bomba.Controls.Add(Me.PanelPresion2B)
        Me.Bomba.Controls.Add(Me.PanelTempB)
        Me.Bomba.Controls.Add(Me.PanelCargaB)
        Me.Bomba.Controls.Add(Me.PanelPresionB)
        Me.Bomba.Controls.Add(Me.PanelHHPB)
        Me.Bomba.Controls.Add(Me.PanelTRIPB)
        Me.Bomba.Location = New System.Drawing.Point(4, 4)
        Me.Bomba.Margin = New System.Windows.Forms.Padding(0)
        Me.Bomba.Name = "Bomba"
        Me.Bomba.Size = New System.Drawing.Size(1201, 630)
        Me.Bomba.TabIndex = 1
        Me.Bomba.Text = "Bomba"
        Me.Bomba.ToolTipText = "Pestaña de parametros de bomba"
        '
        'Title3
        '
        Me.Title3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Title3.AutoSize = True
        Me.Title3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Title3.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.Title3.ForeColor = System.Drawing.Color.White
        Me.Title3.Location = New System.Drawing.Point(528, 10)
        Me.Title3.Name = "Title3"
        Me.Title3.Size = New System.Drawing.Size(146, 47)
        Me.Title3.TabIndex = 168
        Me.Title3.Text = "Bomba"
        '
        'PanelDiagB
        '
        Me.PanelDiagB.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PanelDiagB.BackColor = System.Drawing.Color.Transparent
        Me.PanelDiagB.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelDiagB.BorderRadius = 6
        Me.PanelDiagB.BorderThickness = 1
        Me.PanelDiagB.Controls.Add(Me.ErrorProgressB)
        Me.PanelDiagB.Controls.Add(Me.LblSPresLub)
        Me.PanelDiagB.Controls.Add(Me.PbSPresLUB)
        Me.PanelDiagB.Controls.Add(Me.LblSTempLub)
        Me.PanelDiagB.Controls.Add(Me.PbSTempLub)
        Me.PanelDiagB.Controls.Add(Me.LblSPresSuccion)
        Me.PanelDiagB.Controls.Add(Me.PbSPresSuccion)
        Me.PanelDiagB.Controls.Add(Me.LblSPresDescarga)
        Me.PanelDiagB.Controls.Add(Me.PbSPresDescarga)
        Me.PanelDiagB.Controls.Add(Me.Guna2CustomGradientPanel13)
        Me.PanelDiagB.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelDiagB.FillColor = System.Drawing.Color.Transparent
        Me.PanelDiagB.Location = New System.Drawing.Point(0, 512)
        Me.PanelDiagB.Name = "PanelDiagB"
        Me.PanelDiagB.Size = New System.Drawing.Size(1201, 99)
        Me.PanelDiagB.TabIndex = 155
        '
        'ErrorProgressB
        '
        Me.ErrorProgressB.AnimationSpeed = 50
        Me.ErrorProgressB.AutoStart = True
        Me.ErrorProgressB.Location = New System.Drawing.Point(555, 26)
        Me.ErrorProgressB.Name = "ErrorProgressB"
        Me.ErrorProgressB.Size = New System.Drawing.Size(71, 70)
        Me.ErrorProgressB.TabIndex = 80
        Me.ErrorProgressB.UseTransparentBackground = True
        '
        'LblSPresLub
        '
        Me.LblSPresLub.AutoSize = True
        Me.LblSPresLub.BackColor = System.Drawing.Color.Transparent
        Me.LblSPresLub.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblSPresLub.ForeColor = System.Drawing.Color.White
        Me.LblSPresLub.Location = New System.Drawing.Point(606, 49)
        Me.LblSPresLub.Name = "LblSPresLub"
        Me.LblSPresLub.Size = New System.Drawing.Size(169, 20)
        Me.LblSPresLub.TabIndex = 75
        Me.LblSPresLub.Text = "Presión de Lubricación"
        '
        'PbSPresLUB
        '
        Me.PbSPresLUB.BackColor = System.Drawing.Color.Transparent
        Me.PbSPresLUB.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbSPresLUB.ImageRotate = 0!
        Me.PbSPresLUB.Location = New System.Drawing.Point(575, 47)
        Me.PbSPresLUB.Name = "PbSPresLUB"
        Me.PbSPresLUB.Size = New System.Drawing.Size(25, 25)
        Me.PbSPresLUB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbSPresLUB.TabIndex = 74
        Me.PbSPresLUB.TabStop = False
        Me.PbSPresLUB.UseTransparentBackground = True
        '
        'LblSTempLub
        '
        Me.LblSTempLub.AutoSize = True
        Me.LblSTempLub.BackColor = System.Drawing.Color.Transparent
        Me.LblSTempLub.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblSTempLub.ForeColor = System.Drawing.Color.White
        Me.LblSTempLub.Location = New System.Drawing.Point(906, 49)
        Me.LblSTempLub.Name = "LblSTempLub"
        Me.LblSTempLub.Size = New System.Drawing.Size(204, 20)
        Me.LblSTempLub.TabIndex = 73
        Me.LblSTempLub.Text = "Temperatura de Lubricación"
        '
        'PbSTempLub
        '
        Me.PbSTempLub.BackColor = System.Drawing.Color.Transparent
        Me.PbSTempLub.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbSTempLub.ImageRotate = 0!
        Me.PbSTempLub.Location = New System.Drawing.Point(875, 47)
        Me.PbSTempLub.Name = "PbSTempLub"
        Me.PbSTempLub.Size = New System.Drawing.Size(25, 25)
        Me.PbSTempLub.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbSTempLub.TabIndex = 72
        Me.PbSTempLub.TabStop = False
        Me.PbSTempLub.UseTransparentBackground = True
        '
        'LblSPresSuccion
        '
        Me.LblSPresSuccion.AutoSize = True
        Me.LblSPresSuccion.BackColor = System.Drawing.Color.Transparent
        Me.LblSPresSuccion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblSPresSuccion.ForeColor = System.Drawing.Color.White
        Me.LblSPresSuccion.Location = New System.Drawing.Point(332, 49)
        Me.LblSPresSuccion.Name = "LblSPresSuccion"
        Me.LblSPresSuccion.Size = New System.Drawing.Size(143, 20)
        Me.LblSPresSuccion.TabIndex = 71
        Me.LblSPresSuccion.Text = "Presión de Succión"
        '
        'PbSPresSuccion
        '
        Me.PbSPresSuccion.BackColor = System.Drawing.Color.Transparent
        Me.PbSPresSuccion.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbSPresSuccion.ImageRotate = 0!
        Me.PbSPresSuccion.Location = New System.Drawing.Point(301, 47)
        Me.PbSPresSuccion.Name = "PbSPresSuccion"
        Me.PbSPresSuccion.Size = New System.Drawing.Size(25, 25)
        Me.PbSPresSuccion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbSPresSuccion.TabIndex = 70
        Me.PbSPresSuccion.TabStop = False
        Me.PbSPresSuccion.UseTransparentBackground = True
        '
        'LblSPresDescarga
        '
        Me.LblSPresDescarga.AutoSize = True
        Me.LblSPresDescarga.BackColor = System.Drawing.Color.Transparent
        Me.LblSPresDescarga.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblSPresDescarga.ForeColor = System.Drawing.Color.White
        Me.LblSPresDescarga.Location = New System.Drawing.Point(50, 49)
        Me.LblSPresDescarga.Name = "LblSPresDescarga"
        Me.LblSPresDescarga.Size = New System.Drawing.Size(151, 20)
        Me.LblSPresDescarga.TabIndex = 69
        Me.LblSPresDescarga.Text = "Presión de descarga"
        '
        'PbSPresDescarga
        '
        Me.PbSPresDescarga.BackColor = System.Drawing.Color.Transparent
        Me.PbSPresDescarga.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbSPresDescarga.ImageRotate = 0!
        Me.PbSPresDescarga.Location = New System.Drawing.Point(19, 47)
        Me.PbSPresDescarga.Name = "PbSPresDescarga"
        Me.PbSPresDescarga.Size = New System.Drawing.Size(25, 25)
        Me.PbSPresDescarga.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbSPresDescarga.TabIndex = 68
        Me.PbSPresDescarga.TabStop = False
        Me.PbSPresDescarga.UseTransparentBackground = True
        '
        'Guna2CustomGradientPanel13
        '
        Me.Guna2CustomGradientPanel13.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel13.BorderRadius = 6
        Me.Guna2CustomGradientPanel13.Controls.Add(Me.LblDiagnosticoBomba)
        Me.Guna2CustomGradientPanel13.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel13.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel13.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel13.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel13.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel13.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel13.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel13.Name = "Guna2CustomGradientPanel13"
        Me.Guna2CustomGradientPanel13.Size = New System.Drawing.Size(1201, 26)
        Me.Guna2CustomGradientPanel13.TabIndex = 19
        '
        'LblDiagnosticoBomba
        '
        Me.LblDiagnosticoBomba.AutoSize = True
        Me.LblDiagnosticoBomba.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDiagnosticoBomba.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblDiagnosticoBomba.Location = New System.Drawing.Point(552, 3)
        Me.LblDiagnosticoBomba.Name = "LblDiagnosticoBomba"
        Me.LblDiagnosticoBomba.Size = New System.Drawing.Size(95, 20)
        Me.LblDiagnosticoBomba.TabIndex = 19
        Me.LblDiagnosticoBomba.Text = "Diagnóstico"
        '
        'PanelCaudalB
        '
        Me.PanelCaudalB.Controls.Add(Me.LblCaudalActualB)
        Me.PanelCaudalB.Controls.Add(Me.Label115)
        Me.PanelCaudalB.Controls.Add(Me.LblBombaCaudal)
        Me.PanelCaudalB.Controls.Add(Me.Guna2RadialGauge17)
        Me.PanelCaudalB.Location = New System.Drawing.Point(807, 0)
        Me.PanelCaudalB.Name = "PanelCaudalB"
        Me.PanelCaudalB.Size = New System.Drawing.Size(192, 192)
        Me.PanelCaudalB.TabIndex = 170
        '
        'LblCaudalActualB
        '
        Me.LblCaudalActualB.AutoSize = True
        Me.LblCaudalActualB.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblCaudalActualB.ForeColor = System.Drawing.Color.White
        Me.LblCaudalActualB.Location = New System.Drawing.Point(63, 75)
        Me.LblCaudalActualB.Name = "LblCaudalActualB"
        Me.LblCaudalActualB.Size = New System.Drawing.Size(66, 41)
        Me.LblCaudalActualB.TabIndex = 22
        Me.LblCaudalActualB.Text = "10.1"
        '
        'Label115
        '
        Me.Label115.AutoSize = True
        Me.Label115.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label115.ForeColor = System.Drawing.Color.White
        Me.Label115.Location = New System.Drawing.Point(61, 133)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(70, 20)
        Me.Label115.TabIndex = 23
        Me.Label115.Text = "BBL/Min"
        '
        'LblBombaCaudal
        '
        Me.LblBombaCaudal.AutoSize = True
        Me.LblBombaCaudal.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaCaudal.ForeColor = System.Drawing.Color.White
        Me.LblBombaCaudal.Location = New System.Drawing.Point(68, 46)
        Me.LblBombaCaudal.Name = "LblBombaCaudal"
        Me.LblBombaCaudal.Size = New System.Drawing.Size(57, 20)
        Me.LblBombaCaudal.TabIndex = 19
        Me.LblBombaCaudal.Text = "Caudal"
        '
        'Guna2RadialGauge17
        '
        Me.Guna2RadialGauge17.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge17.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge17.ArrowThickness = 0
        Me.Guna2RadialGauge17.ArrowVisible = False
        Me.Guna2RadialGauge17.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge17.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2RadialGauge17.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!)
        Me.Guna2RadialGauge17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge17.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge17.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge17.Name = "Guna2RadialGauge17"
        Me.Guna2RadialGauge17.ProgressColor = System.Drawing.Color.LimeGreen
        Me.Guna2RadialGauge17.ProgressColor2 = System.Drawing.Color.Green
        Me.Guna2RadialGauge17.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge17.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge17.ProgressThickness = 6
        Me.Guna2RadialGauge17.ShowPercentage = False
        Me.Guna2RadialGauge17.Size = New System.Drawing.Size(190, 190)
        Me.Guna2RadialGauge17.TabIndex = 156
        Me.Guna2RadialGauge17.UseTransparentBackground = True
        Me.Guna2RadialGauge17.Value = 68
        '
        'PanelPresion2B
        '
        Me.PanelPresion2B.Controls.Add(Me.Label108)
        Me.PanelPresion2B.Controls.Add(Me.LblBombaPresionSuccion)
        Me.PanelPresion2B.Controls.Add(Me.LblBPresSuccionB)
        Me.PanelPresion2B.Controls.Add(Me.Guna2RadialGauge22)
        Me.PanelPresion2B.Location = New System.Drawing.Point(1003, 125)
        Me.PanelPresion2B.Name = "PanelPresion2B"
        Me.PanelPresion2B.Size = New System.Drawing.Size(192, 192)
        Me.PanelPresion2B.TabIndex = 170
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label108.ForeColor = System.Drawing.Color.White
        Me.Label108.Location = New System.Drawing.Point(80, 135)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(32, 20)
        Me.Label108.TabIndex = 166
        Me.Label108.Text = "PSI"
        '
        'LblBombaPresionSuccion
        '
        Me.LblBombaPresionSuccion.AutoSize = True
        Me.LblBombaPresionSuccion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresionSuccion.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresionSuccion.Location = New System.Drawing.Point(35, 46)
        Me.LblBombaPresionSuccion.Name = "LblBombaPresionSuccion"
        Me.LblBombaPresionSuccion.Size = New System.Drawing.Size(122, 20)
        Me.LblBombaPresionSuccion.TabIndex = 164
        Me.LblBombaPresionSuccion.Text = "Presión Succión"
        '
        'LblBPresSuccionB
        '
        Me.LblBPresSuccionB.AutoSize = True
        Me.LblBPresSuccionB.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblBPresSuccionB.ForeColor = System.Drawing.Color.White
        Me.LblBPresSuccionB.Location = New System.Drawing.Point(77, 75)
        Me.LblBPresSuccionB.Name = "LblBPresSuccionB"
        Me.LblBPresSuccionB.Size = New System.Drawing.Size(38, 41)
        Me.LblBPresSuccionB.TabIndex = 165
        Me.LblBPresSuccionB.Text = "0"
        '
        'Guna2RadialGauge22
        '
        Me.Guna2RadialGauge22.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge22.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge22.ArrowThickness = 0
        Me.Guna2RadialGauge22.ArrowVisible = False
        Me.Guna2RadialGauge22.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge22.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2RadialGauge22.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!)
        Me.Guna2RadialGauge22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge22.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge22.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge22.Name = "Guna2RadialGauge22"
        Me.Guna2RadialGauge22.ProgressColor = System.Drawing.Color.LimeGreen
        Me.Guna2RadialGauge22.ProgressColor2 = System.Drawing.Color.Green
        Me.Guna2RadialGauge22.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge22.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge22.ProgressThickness = 6
        Me.Guna2RadialGauge22.ShowPercentage = False
        Me.Guna2RadialGauge22.Size = New System.Drawing.Size(190, 190)
        Me.Guna2RadialGauge22.TabIndex = 167
        Me.Guna2RadialGauge22.UseTransparentBackground = True
        Me.Guna2RadialGauge22.Value = 68
        '
        'PanelTempB
        '
        Me.PanelTempB.Controls.Add(Me.LblBombaTempLub)
        Me.PanelTempB.Controls.Add(Me.LblBTempLUBB)
        Me.PanelTempB.Controls.Add(Me.Label117)
        Me.PanelTempB.Controls.Add(Me.Guna2RadialGauge20)
        Me.PanelTempB.Location = New System.Drawing.Point(807, 269)
        Me.PanelTempB.Name = "PanelTempB"
        Me.PanelTempB.Size = New System.Drawing.Size(192, 192)
        Me.PanelTempB.TabIndex = 170
        '
        'LblBombaTempLub
        '
        Me.LblBombaTempLub.AutoSize = True
        Me.LblBombaTempLub.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaTempLub.ForeColor = System.Drawing.Color.White
        Me.LblBombaTempLub.Location = New System.Drawing.Point(30, 55)
        Me.LblBombaTempLub.Name = "LblBombaTempLub"
        Me.LblBombaTempLub.Size = New System.Drawing.Size(131, 20)
        Me.LblBombaTempLub.TabIndex = 20
        Me.LblBombaTempLub.Text = "Temperatura LUB"
        '
        'LblBTempLUBB
        '
        Me.LblBTempLUBB.AutoSize = True
        Me.LblBTempLUBB.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblBTempLUBB.ForeColor = System.Drawing.Color.White
        Me.LblBTempLUBB.Location = New System.Drawing.Point(69, 84)
        Me.LblBTempLUBB.Name = "LblBTempLUBB"
        Me.LblBTempLUBB.Size = New System.Drawing.Size(54, 41)
        Me.LblBTempLUBB.TabIndex = 22
        Me.LblBTempLUBB.Text = "74"
        '
        'Label117
        '
        Me.Label117.AutoSize = True
        Me.Label117.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label117.ForeColor = System.Drawing.Color.White
        Me.Label117.Location = New System.Drawing.Point(84, 134)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(25, 20)
        Me.Label117.TabIndex = 26
        Me.Label117.Text = "ºC"
        '
        'Guna2RadialGauge20
        '
        Me.Guna2RadialGauge20.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge20.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge20.ArrowThickness = 0
        Me.Guna2RadialGauge20.ArrowVisible = False
        Me.Guna2RadialGauge20.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge20.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2RadialGauge20.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!)
        Me.Guna2RadialGauge20.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge20.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge20.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge20.Name = "Guna2RadialGauge20"
        Me.Guna2RadialGauge20.ProgressColor = System.Drawing.Color.LimeGreen
        Me.Guna2RadialGauge20.ProgressColor2 = System.Drawing.Color.Green
        Me.Guna2RadialGauge20.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge20.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge20.ProgressThickness = 6
        Me.Guna2RadialGauge20.ShowPercentage = False
        Me.Guna2RadialGauge20.Size = New System.Drawing.Size(190, 190)
        Me.Guna2RadialGauge20.TabIndex = 159
        Me.Guna2RadialGauge20.UseTransparentBackground = True
        Me.Guna2RadialGauge20.Value = 68
        '
        'PanelCargaB
        '
        Me.PanelCargaB.Controls.Add(Me.LblBombaCargaMotor)
        Me.PanelCargaB.Controls.Add(Me.LblBCargaMotorB)
        Me.PanelCargaB.Controls.Add(Me.Label107)
        Me.PanelCargaB.Controls.Add(Me.Guna2RadialGauge19)
        Me.PanelCargaB.Location = New System.Drawing.Point(207, 269)
        Me.PanelCargaB.Name = "PanelCargaB"
        Me.PanelCargaB.Size = New System.Drawing.Size(192, 192)
        Me.PanelCargaB.TabIndex = 170
        '
        'LblBombaCargaMotor
        '
        Me.LblBombaCargaMotor.AutoSize = True
        Me.LblBombaCargaMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaCargaMotor.ForeColor = System.Drawing.Color.White
        Me.LblBombaCargaMotor.Location = New System.Drawing.Point(39, 55)
        Me.LblBombaCargaMotor.Name = "LblBombaCargaMotor"
        Me.LblBombaCargaMotor.Size = New System.Drawing.Size(114, 20)
        Me.LblBombaCargaMotor.TabIndex = 30
        Me.LblBombaCargaMotor.Text = "% Carga Motor"
        '
        'LblBCargaMotorB
        '
        Me.LblBCargaMotorB.AutoSize = True
        Me.LblBCargaMotorB.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblBCargaMotorB.ForeColor = System.Drawing.Color.White
        Me.LblBCargaMotorB.Location = New System.Drawing.Point(69, 84)
        Me.LblBCargaMotorB.Name = "LblBCargaMotorB"
        Me.LblBCargaMotorB.Size = New System.Drawing.Size(55, 41)
        Me.LblBCargaMotorB.TabIndex = 32
        Me.LblBCargaMotorB.Text = "68"
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label107.ForeColor = System.Drawing.Color.White
        Me.Label107.Location = New System.Drawing.Point(86, 134)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(21, 20)
        Me.Label107.TabIndex = 36
        Me.Label107.Text = "%"
        '
        'Guna2RadialGauge19
        '
        Me.Guna2RadialGauge19.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge19.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge19.ArrowThickness = 0
        Me.Guna2RadialGauge19.ArrowVisible = False
        Me.Guna2RadialGauge19.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge19.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2RadialGauge19.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!)
        Me.Guna2RadialGauge19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge19.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge19.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge19.Name = "Guna2RadialGauge19"
        Me.Guna2RadialGauge19.ProgressColor = System.Drawing.Color.LimeGreen
        Me.Guna2RadialGauge19.ProgressColor2 = System.Drawing.Color.Green
        Me.Guna2RadialGauge19.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge19.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge19.ProgressThickness = 6
        Me.Guna2RadialGauge19.ShowPercentage = False
        Me.Guna2RadialGauge19.Size = New System.Drawing.Size(190, 190)
        Me.Guna2RadialGauge19.TabIndex = 158
        Me.Guna2RadialGauge19.UseTransparentBackground = True
        Me.Guna2RadialGauge19.Value = 68
        '
        'PanelPresionB
        '
        Me.PanelPresionB.Controls.Add(Me.LblBombaPresLub)
        Me.PanelPresionB.Controls.Add(Me.LblBPresLUBB)
        Me.PanelPresionB.Controls.Add(Me.Label116)
        Me.PanelPresionB.Controls.Add(Me.Guna2RadialGauge21)
        Me.PanelPresionB.Location = New System.Drawing.Point(207, 0)
        Me.PanelPresionB.Name = "PanelPresionB"
        Me.PanelPresionB.Size = New System.Drawing.Size(192, 192)
        Me.PanelPresionB.TabIndex = 170
        '
        'LblBombaPresLub
        '
        Me.LblBombaPresLub.AutoSize = True
        Me.LblBombaPresLub.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresLub.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresLub.Location = New System.Drawing.Point(48, 46)
        Me.LblBombaPresLub.Name = "LblBombaPresLub"
        Me.LblBombaPresLub.Size = New System.Drawing.Size(96, 20)
        Me.LblBombaPresLub.TabIndex = 161
        Me.LblBombaPresLub.Text = "Presión LUB"
        '
        'LblBPresLUBB
        '
        Me.LblBPresLUBB.AutoSize = True
        Me.LblBPresLUBB.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblBPresLUBB.ForeColor = System.Drawing.Color.White
        Me.LblBPresLUBB.Location = New System.Drawing.Point(77, 75)
        Me.LblBPresLUBB.Name = "LblBPresLUBB"
        Me.LblBPresLUBB.Size = New System.Drawing.Size(38, 41)
        Me.LblBPresLUBB.TabIndex = 162
        Me.LblBPresLUBB.Text = "0"
        '
        'Label116
        '
        Me.Label116.AutoSize = True
        Me.Label116.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label116.ForeColor = System.Drawing.Color.White
        Me.Label116.Location = New System.Drawing.Point(80, 133)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(32, 20)
        Me.Label116.TabIndex = 163
        Me.Label116.Text = "PSI"
        '
        'Guna2RadialGauge21
        '
        Me.Guna2RadialGauge21.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge21.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge21.ArrowThickness = 0
        Me.Guna2RadialGauge21.ArrowVisible = False
        Me.Guna2RadialGauge21.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge21.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2RadialGauge21.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!)
        Me.Guna2RadialGauge21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge21.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge21.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge21.Name = "Guna2RadialGauge21"
        Me.Guna2RadialGauge21.ProgressColor = System.Drawing.Color.LimeGreen
        Me.Guna2RadialGauge21.ProgressColor2 = System.Drawing.Color.Green
        Me.Guna2RadialGauge21.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge21.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge21.ProgressThickness = 6
        Me.Guna2RadialGauge21.ShowPercentage = False
        Me.Guna2RadialGauge21.Size = New System.Drawing.Size(190, 190)
        Me.Guna2RadialGauge21.TabIndex = 160
        Me.Guna2RadialGauge21.UseTransparentBackground = True
        Me.Guna2RadialGauge21.Value = 68
        '
        'PanelHHPB
        '
        Me.PanelHHPB.Controls.Add(Me.LblBombaHHP)
        Me.PanelHHPB.Controls.Add(Me.LblBHHPB)
        Me.PanelHHPB.Controls.Add(Me.Guna2RadialGauge18)
        Me.PanelHHPB.Location = New System.Drawing.Point(11, 125)
        Me.PanelHHPB.Name = "PanelHHPB"
        Me.PanelHHPB.Size = New System.Drawing.Size(192, 192)
        Me.PanelHHPB.TabIndex = 170
        '
        'LblBombaHHP
        '
        Me.LblBombaHHP.AutoSize = True
        Me.LblBombaHHP.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaHHP.ForeColor = System.Drawing.Color.White
        Me.LblBombaHHP.Location = New System.Drawing.Point(76, 46)
        Me.LblBombaHHP.Name = "LblBombaHHP"
        Me.LblBombaHHP.Size = New System.Drawing.Size(41, 20)
        Me.LblBombaHHP.TabIndex = 33
        Me.LblBombaHHP.Text = "HHP"
        '
        'LblBHHPB
        '
        Me.LblBHHPB.AutoSize = True
        Me.LblBHHPB.Font = New System.Drawing.Font("Montserrat", 22.0!)
        Me.LblBHHPB.ForeColor = System.Drawing.Color.White
        Me.LblBHHPB.Location = New System.Drawing.Point(54, 75)
        Me.LblBHHPB.Name = "LblBHHPB"
        Me.LblBHHPB.Size = New System.Drawing.Size(84, 41)
        Me.LblBHHPB.TabIndex = 34
        Me.LblBHHPB.Text = "1457"
        '
        'Guna2RadialGauge18
        '
        Me.Guna2RadialGauge18.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge18.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge18.ArrowThickness = 0
        Me.Guna2RadialGauge18.ArrowVisible = False
        Me.Guna2RadialGauge18.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge18.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2RadialGauge18.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!)
        Me.Guna2RadialGauge18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge18.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge18.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge18.Name = "Guna2RadialGauge18"
        Me.Guna2RadialGauge18.ProgressColor = System.Drawing.Color.LimeGreen
        Me.Guna2RadialGauge18.ProgressColor2 = System.Drawing.Color.Green
        Me.Guna2RadialGauge18.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge18.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge18.ProgressThickness = 6
        Me.Guna2RadialGauge18.ShowPercentage = False
        Me.Guna2RadialGauge18.Size = New System.Drawing.Size(190, 190)
        Me.Guna2RadialGauge18.TabIndex = 157
        Me.Guna2RadialGauge18.UseTransparentBackground = True
        Me.Guna2RadialGauge18.Value = 68
        '
        'PanelTRIPB
        '
        Me.PanelTRIPB.Controls.Add(Me.LblBombaPresionCorte)
        Me.PanelTRIPB.Controls.Add(Me.LblPCorteDeseadaB)
        Me.PanelTRIPB.Controls.Add(Me.LblBombaPresionActual)
        Me.PanelTRIPB.Controls.Add(Me.LblPCorteActualB)
        Me.PanelTRIPB.Controls.Add(Me.Guna2RadialGauge16)
        Me.PanelTRIPB.Location = New System.Drawing.Point(403, 61)
        Me.PanelTRIPB.Name = "PanelTRIPB"
        Me.PanelTRIPB.Size = New System.Drawing.Size(400, 400)
        Me.PanelTRIPB.TabIndex = 169
        '
        'LblBombaPresionCorte
        '
        Me.LblBombaPresionCorte.AutoSize = True
        Me.LblBombaPresionCorte.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresionCorte.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresionCorte.Location = New System.Drawing.Point(137, 227)
        Me.LblBombaPresionCorte.Name = "LblBombaPresionCorte"
        Me.LblBombaPresionCorte.Size = New System.Drawing.Size(126, 20)
        Me.LblBombaPresionCorte.TabIndex = 149
        Me.LblBombaPresionCorte.Text = "Presión de Corte"
        '
        'LblPCorteDeseadaB
        '
        Me.LblPCorteDeseadaB.AutoSize = True
        Me.LblPCorteDeseadaB.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblPCorteDeseadaB.ForeColor = System.Drawing.Color.White
        Me.LblPCorteDeseadaB.Location = New System.Drawing.Point(156, 254)
        Me.LblPCorteDeseadaB.Name = "LblPCorteDeseadaB"
        Me.LblPCorteDeseadaB.Size = New System.Drawing.Size(87, 37)
        Me.LblPCorteDeseadaB.TabIndex = 151
        Me.LblPCorteDeseadaB.Text = "9000"
        '
        'LblBombaPresionActual
        '
        Me.LblBombaPresionActual.AutoSize = True
        Me.LblBombaPresionActual.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresionActual.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresionActual.Location = New System.Drawing.Point(145, 49)
        Me.LblBombaPresionActual.Name = "LblBombaPresionActual"
        Me.LblBombaPresionActual.Size = New System.Drawing.Size(111, 20)
        Me.LblBombaPresionActual.TabIndex = 150
        Me.LblBombaPresionActual.Text = "Presión Actual"
        '
        'LblPCorteActualB
        '
        Me.LblPCorteActualB.AutoSize = True
        Me.LblPCorteActualB.Font = New System.Drawing.Font("Montserrat", 30.0!)
        Me.LblPCorteActualB.ForeColor = System.Drawing.Color.White
        Me.LblPCorteActualB.Location = New System.Drawing.Point(137, 81)
        Me.LblPCorteActualB.Name = "LblPCorteActualB"
        Me.LblPCorteActualB.Size = New System.Drawing.Size(124, 55)
        Me.LblPCorteActualB.TabIndex = 152
        Me.LblPCorteActualB.Text = "8986"
        '
        'Guna2RadialGauge16
        '
        Me.Guna2RadialGauge16.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2RadialGauge16.ArrowColor = System.Drawing.Color.Silver
        Me.Guna2RadialGauge16.ArrowThickness = 0
        Me.Guna2RadialGauge16.ArrowVisible = False
        Me.Guna2RadialGauge16.BackColor = System.Drawing.Color.Transparent
        Me.Guna2RadialGauge16.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2RadialGauge16.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.Guna2RadialGauge16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.Guna2RadialGauge16.Location = New System.Drawing.Point(1, 1)
        Me.Guna2RadialGauge16.MinimumSize = New System.Drawing.Size(30, 30)
        Me.Guna2RadialGauge16.Name = "Guna2RadialGauge16"
        Me.Guna2RadialGauge16.ProgressColor = System.Drawing.Color.Lime
        Me.Guna2RadialGauge16.ProgressColor2 = System.Drawing.Color.DarkGreen
        Me.Guna2RadialGauge16.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge16.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.Guna2RadialGauge16.ProgressThickness = 12
        Me.Guna2RadialGauge16.ShowPercentage = False
        Me.Guna2RadialGauge16.Size = New System.Drawing.Size(398, 398)
        Me.Guna2RadialGauge16.TabIndex = 153
        Me.Guna2RadialGauge16.UseTransparentBackground = True
        Me.Guna2RadialGauge16.Value = 85
        '
        'TmrRead
        '
        Me.TmrRead.Interval = 1000
        '
        'TmrDiagnostico
        '
        Me.TmrDiagnostico.Interval = 1000
        '
        'TmrCheck
        '
        Me.TmrCheck.Interval = 1000
        '
        'TmrErrorCheck
        '
        Me.TmrErrorCheck.Interval = 2000
        '
        'OneFracMaximize
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Controls.Add(Me.MaximizeTabControl)
        Me.DoubleBuffered = True
        Me.Name = "OneFracMaximize"
        Me.Size = New System.Drawing.Size(1209, 678)
        Me.MaximizeTabControl.ResumeLayout(False)
        Me.General.ResumeLayout(False)
        Me.General.PerformLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2Panel12.ResumeLayout(False)
        Me.Guna2Panel12.PerformLayout()
        Me.Guna2CustomGradientPanel12.ResumeLayout(False)
        Me.Guna2CustomGradientPanel12.PerformLayout()
        Me.Guna2Panel10.ResumeLayout(False)
        Me.Guna2Panel10.PerformLayout()
        Me.Guna2CustomGradientPanel10.ResumeLayout(False)
        Me.Guna2CustomGradientPanel10.PerformLayout()
        Me.Guna2Panel7.ResumeLayout(False)
        Me.Guna2Panel7.PerformLayout()
        Me.Guna2CustomGradientPanel7.ResumeLayout(False)
        Me.Guna2CustomGradientPanel7.PerformLayout()
        Me.Guna2Panel5.ResumeLayout(False)
        Me.Guna2Panel5.PerformLayout()
        Me.Guna2CustomGradientPanel5.ResumeLayout(False)
        Me.Guna2CustomGradientPanel5.PerformLayout()
        Me.Guna2Panel4.ResumeLayout(False)
        Me.Guna2Panel4.PerformLayout()
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.PresionPanel.ResumeLayout(False)
        Me.PresionPanel.PerformLayout()
        Me.RPMPanel.ResumeLayout(False)
        Me.RPMPanel.PerformLayout()
        Me.Motor.ResumeLayout(False)
        Me.Motor.PerformLayout()
        Me.PanelDiagM.ResumeLayout(False)
        Me.PanelDiagM.PerformLayout()
        CType(Me.PbMHorasMotor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2CustomGradientPanel6.ResumeLayout(False)
        Me.Guna2CustomGradientPanel6.PerformLayout()
        CType(Me.PbMCombustible, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbMTempAceite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbMPresAceite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbMTempAgua, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelErroresM.ResumeLayout(False)
        Me.PanelErroresM.PerformLayout()
        Me.Guna2CustomGradientPanel8.ResumeLayout(False)
        Me.Guna2CustomGradientPanel8.PerformLayout()
        Me.CpbHorasMotorM.ResumeLayout(False)
        Me.CpbHorasMotorM.PerformLayout()
        Me.CpbCombustibleM.ResumeLayout(False)
        Me.CpbCombustibleM.PerformLayout()
        Me.CpbVoltajeM.ResumeLayout(False)
        Me.CpbVoltajeM.PerformLayout()
        Me.CpbPresionM.ResumeLayout(False)
        Me.CpbPresionM.PerformLayout()
        Me.PanelTempAguaM.ResumeLayout(False)
        Me.PanelTempAguaM.PerformLayout()
        Me.PanelTempAceiteM.ResumeLayout(False)
        Me.PanelTempAceiteM.PerformLayout()
        Me.PanelCombustibleM.ResumeLayout(False)
        Me.PanelCombustibleM.PerformLayout()
        Me.PanelCargaM.ResumeLayout(False)
        Me.PanelCargaM.PerformLayout()
        Me.PanelRPMM.ResumeLayout(False)
        Me.PanelRPMM.PerformLayout()
        Me.Transmision.ResumeLayout(False)
        Me.Transmision.PerformLayout()
        Me.PanelDiagT.ResumeLayout(False)
        Me.PanelDiagT.PerformLayout()
        CType(Me.PbAceiteTransmision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbTPresAceite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbTTempAceite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        Me.PanelErroresT.ResumeLayout(False)
        Me.PanelErroresT.PerformLayout()
        Me.Guna2CustomGradientPanel3.ResumeLayout(False)
        Me.Guna2CustomGradientPanel3.PerformLayout()
        Me.PanelRPMT.ResumeLayout(False)
        Me.PanelRPMT.PerformLayout()
        Me.PanelPresionT.ResumeLayout(False)
        Me.PanelPresionT.PerformLayout()
        Me.PanelMarchaT.ResumeLayout(False)
        Me.PanelMarchaT.PerformLayout()
        Me.PanelTempT.ResumeLayout(False)
        Me.PanelTempT.PerformLayout()
        Me.Bomba.ResumeLayout(False)
        Me.Bomba.PerformLayout()
        Me.PanelDiagB.ResumeLayout(False)
        Me.PanelDiagB.PerformLayout()
        CType(Me.PbSPresLUB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbSTempLub, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbSPresSuccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbSPresDescarga, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2CustomGradientPanel13.ResumeLayout(False)
        Me.Guna2CustomGradientPanel13.PerformLayout()
        Me.PanelCaudalB.ResumeLayout(False)
        Me.PanelCaudalB.PerformLayout()
        Me.PanelPresion2B.ResumeLayout(False)
        Me.PanelPresion2B.PerformLayout()
        Me.PanelTempB.ResumeLayout(False)
        Me.PanelTempB.PerformLayout()
        Me.PanelCargaB.ResumeLayout(False)
        Me.PanelCargaB.PerformLayout()
        Me.PanelPresionB.ResumeLayout(False)
        Me.PanelPresionB.PerformLayout()
        Me.PanelHHPB.ResumeLayout(False)
        Me.PanelHHPB.PerformLayout()
        Me.PanelTRIPB.ResumeLayout(False)
        Me.PanelTRIPB.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents General As TabPage
    Friend WithEvents BtnStartEngine As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents PbAutomatic As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents Guna2Panel12 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents LblTOp1 As Label
    Friend WithEvents Guna2CustomGradientPanel12 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents Guna2Panel10 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Label81 As Label
    Friend WithEvents LblCaudalActualG As Label
    Friend WithEvents Guna2CustomGradientPanel10 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents BtnAjustes As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Guna2Panel7 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel7 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents Guna2Panel5 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel5 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents BtnMinusGear As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents BtnPlusGear As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Guna2Panel4 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents LblMarchaActualG As Label
    Friend WithEvents LblMarchaDeseadaG As Label
    Friend WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents LblUnidad2M As Label
    Friend WithEvents LblMCombustibleM As Label
    Friend WithEvents LblUnidad3M As Label
    Friend WithEvents LblUnidad4M As Label
    Friend WithEvents LblMVoltajeM As Label
    Friend WithEvents LblMPresionM As Label
    Friend WithEvents Guna2CustomGradientPanel8 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents LblRPMActualM As Label
    Friend WithEvents RgRPMM As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents PanelDiagM As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel6 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents PbMHorasMotor As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMCombustible As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMTempAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMPresAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMTempAgua As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents LblPCorteActualG As Label
    Friend WithEvents LblPCorteDeseadaG As Label
    Friend WithEvents RgPresionG As Guna.UI2.WinForms.Guna2RadialGauge
    Friend WithEvents PanelDiagT As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents PanelErroresT As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PbTPresAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbTTempAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents HSep1 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents PbAceiteTransmision As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents LblAceiteTransmision As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents Guna2CustomGradientPanel13 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents PbSPresLUB As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbSTempLub As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbSPresSuccion As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents TmrRead As Timer
    Friend WithEvents CpbVoltajeM As Guna.UI2.WinForms.Guna2CircleProgressBar
    Friend WithEvents CpbCombustibleM As Guna.UI2.WinForms.Guna2CircleProgressBar
    Friend WithEvents CpbHorasMotorM As Guna.UI2.WinForms.Guna2CircleProgressBar
    Friend WithEvents LblBHorasMotorM As Label
    Friend WithEvents LblUnidad1M As Label
    Friend WithEvents CpbPresionM As Guna.UI2.WinForms.Guna2CircleProgressBar
    Friend WithEvents TmrDiagnostico As Timer
    Protected Friend WithEvents PbSPresDescarga As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents TmrCheck As Timer
    Friend WithEvents TmrErrorCheck As Timer
    Friend WithEvents PresionPanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents RPMPanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Motor As TabPage
    Public WithEvents BtnErrores As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnFC As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblErrorAdmisible As Label
    Public WithEvents LblErrorGrave As Label
    Public WithEvents Label32 As Label
    Public WithEvents LblTOp As Label
    Public WithEvents LblCaudal As Label
    Public WithEvents Label54 As Label
    Public WithEvents Label55 As Label
    Public WithEvents Label56 As Label
    Public WithEvents LblBHHPG As Label
    Public WithEvents LblBombaHHPGeneral As Label
    Public WithEvents LblBCargaMotorG As Label
    Public WithEvents LblBPresSuccionG As Label
    Public WithEvents LblBombaCargaMotorGeneral As Label
    Public WithEvents LblBombaPresGeneral As Label
    Public WithEvents Label63 As Label
    Public WithEvents Label64 As Label
    Public WithEvents Label65 As Label
    Public WithEvents LblBHorasMotorG As Label
    Public WithEvents LblBombaHMGeneral As Label
    Public WithEvents LblBPresLUBG As Label
    Public WithEvents LblBTempLUBG As Label
    Public WithEvents LblBombaPres1General As Label
    Public WithEvents LblBombaTempGeneral As Label
    Public WithEvents LblBombaGeneral As Label
    Public WithEvents Label41 As Label
    Public WithEvents Label42 As Label
    Public WithEvents Label43 As Label
    Public WithEvents LblTRpmOutG As Label
    Public WithEvents LblTransRPMGeneral As Label
    Public WithEvents LblTPresionG As Label
    Public WithEvents LblTTempG As Label
    Public WithEvents LblTransPresionGeneral As Label
    Public WithEvents LblTransTempGeneral As Label
    Public WithEvents LblTransmisionGeneral As Label
    Public WithEvents Label23 As Label
    Public WithEvents Label24 As Label
    Public WithEvents Label25 As Label
    Public WithEvents LblMCombustibleP100G As Label
    Public WithEvents LblMotorCombustible2General As Label
    Public WithEvents LblMCombustibleG As Label
    Public WithEvents LblMTempAceiteG As Label
    Public WithEvents LblMotorCombustible1General As Label
    Public WithEvents LblMotorTempAceiteGeneral As Label
    Public WithEvents Label22 As Label
    Public WithEvents Label21 As Label
    Public WithEvents Label20 As Label
    Public WithEvents LblMVoltajeG As Label
    Public WithEvents LblMotorVoltajeGeneral As Label
    Public WithEvents LblMPresionG As Label
    Public WithEvents LblMTempAguaG As Label
    Public WithEvents LblMotorPresionGeneral As Label
    Public WithEvents LblMotorTempAguaGeneral As Label
    Public WithEvents LblMotorGeneral As Label
    Public WithEvents LblMarcha3 As Label
    Public WithEvents LblMarcha1 As Label
    Public WithEvents LblMarcha As Label
    Public WithEvents PanelErroresM As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label6 As Label
    Public WithEvents Label8 As Label
    Public WithEvents LblMCombustibleP100M As Label
    Public WithEvents LblMotorCombustibleMotor As Label
    Public WithEvents LblMTempAceiteM As Label
    Public WithEvents LblMotorCombustible1Motor As Label
    Public WithEvents LblMotorTempAceite As Label
    Public WithEvents LblMUnidad1 As Label
    Public WithEvents LblMotorVoltajeMotor As Label
    Public WithEvents LblMTempAguaM As Label
    Public WithEvents LblMotorPresionMotor As Label
    Public WithEvents LblMotorTempAgua As Label
    Public WithEvents LblErroresMotor As Label
    Public WithEvents LblRPMDeseadaM As Label
    Public WithEvents LblRPM2 As Label
    Public WithEvents LblRPM1M As Label
    Public WithEvents Label39 As Label
    Public WithEvents LblBCargaMotorM As Label
    Public WithEvents LblMotorCargaMotor As Label
    Public WithEvents BtnDiagnostico As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblDiagnosticoMotor As Label
    Public WithEvents RgTempAceiteM As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents RgTempAguaM As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents RgCombP100M As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Label48 As Label
    Public WithEvents Label1 As Label
    Public WithEvents RgCargaM As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents LblDescripcionM As Label
    Public WithEvents LblInfoDescripM As Label
    Public WithEvents LblUnidadM As Label
    Public WithEvents LblInfoUnidadM As Label
    Public WithEvents LblActivoM As Label
    Public WithEvents LblInfoActivoM As Label
    Public WithEvents LblErroresTotalesM As Label
    Public WithEvents LblErrorNM As Label
    Public WithEvents LblOCM As Label
    Public WithEvents LblFMIM As Label
    Public WithEvents LblSPNM As Label
    Public WithEvents LblInfoErrTotM As Label
    Public WithEvents LblInfoErrorNM As Label
    Public WithEvents LblInfoOCM As Label
    Public WithEvents LblInfoFMIM As Label
    Public WithEvents LblInfoSPNM As Label
    Public WithEvents BtnBorrarActualM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnSiguienteM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAnteriorM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblMHoras As Label
    Public WithEvents LblMCombustible As Label
    Public WithEvents LblMTempAceite As Label
    Public WithEvents LblMPresAceite As Label
    Public WithEvents LblMTempAgua As Label
    Public WithEvents BtnTripZero As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnTrip As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblPCorte3 As Label
    Public WithEvents LblPCorte1 As Label
    Public WithEvents BtnIdle As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPMDeseadaG As Label
    Public WithEvents LblRPMActualG As Label
    Public WithEvents BtnRpmMinus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPM1 As Label
    Public WithEvents BtnRpmMinus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPM3 As Label
    Public WithEvents RgRPMG As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Title1 As Label
    Public WithEvents LblBombaHorasMotorMotor As Label
    Public WithEvents ErrorProgressM1 As Guna.UI2.WinForms.Guna2WinProgressIndicator
    Public WithEvents MaximizeTabControl As Guna.UI2.WinForms.Guna2TabControl
    Public WithEvents LblDiagnosticoTrans As Label
    Public WithEvents BtnBorrarActualT As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnSiguienteT As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAnteriorT As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblDescripcionT As Label
    Public WithEvents LblInfoDescripT As Label
    Public WithEvents LblUnidadT As Label
    Public WithEvents LblInfoUnidadT As Label
    Public WithEvents LblActivoT As Label
    Public WithEvents LblInfoActivoT As Label
    Public WithEvents LblErroresTotalesT As Label
    Public WithEvents LblErrorNT As Label
    Public WithEvents LblOCT As Label
    Public WithEvents LblFMIT As Label
    Public WithEvents LblSPNT As Label
    Public WithEvents LblInfoErrTotT As Label
    Public WithEvents LblInfoErrorNT As Label
    Public WithEvents LblInfoOCT As Label
    Public WithEvents LblInfoFMIT As Label
    Public WithEvents LblInfoSPNT As Label
    Public WithEvents Guna2CustomGradientPanel3 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblErroresTrans As Label
    Public WithEvents LblTPresAceite As Label
    Public WithEvents LblTTempAceite As Label
    Public WithEvents LblTransmisionDiag1 As Label
    Public WithEvents Guna2RadialGauge12 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents LblMarchaActualT As Label
    Public WithEvents LblMarchaDeseadaT As Label
    Public WithEvents LblTransmisionMarchaActual As Label
    Public WithEvents LblTransmisionMarchaDeseada As Label
    Public WithEvents Label74 As Label
    Public WithEvents Label75 As Label
    Public WithEvents Label76 As Label
    Public WithEvents LblTRpmOutT As Label
    Public WithEvents LblTransmisionRPMOut As Label
    Public WithEvents LblTPresionT As Label
    Public WithEvents LblTTempT As Label
    Public WithEvents LblTransmisionPresion As Label
    Public WithEvents LblTransmisionTemp As Label
    Public WithEvents Guna2RadialGauge15 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2RadialGauge13 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2RadialGauge14 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents PanelDiagB As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblDiagnosticoBomba As Label
    Public WithEvents LblPCorteActualB As Label
    Public WithEvents LblBombaPresionActual As Label
    Public WithEvents LblPCorteDeseadaB As Label
    Public WithEvents LblBombaPresionCorte As Label
    Public WithEvents Guna2RadialGauge16 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Label115 As Label
    Public WithEvents LblCaudalActualB As Label
    Public WithEvents LblBombaCaudal As Label
    Public WithEvents Label107 As Label
    Public WithEvents LblBHHPB As Label
    Public WithEvents LblBombaHHP As Label
    Public WithEvents LblBCargaMotorB As Label
    Public WithEvents LblBombaCargaMotor As Label
    Public WithEvents Label117 As Label
    Public WithEvents LblBTempLUBB As Label
    Public WithEvents LblBombaTempLub As Label
    Public WithEvents Guna2RadialGauge17 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents LblSPresLub As Label
    Public WithEvents LblSTempLub As Label
    Public WithEvents LblSPresSuccion As Label
    Public WithEvents LblSPresDescarga As Label
    Public WithEvents Label108 As Label
    Public WithEvents LblBPresSuccionB As Label
    Public WithEvents LblBombaPresionSuccion As Label
    Public WithEvents Label116 As Label
    Public WithEvents LblBPresLUBB As Label
    Public WithEvents LblBombaPresLub As Label
    Public WithEvents Guna2RadialGauge19 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2RadialGauge20 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2RadialGauge18 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2RadialGauge22 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2RadialGauge21 As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Title2 As Label
    Public WithEvents Title3 As Label
    Public WithEvents ErrorProgressT1 As Guna.UI2.WinForms.Guna2WinProgressIndicator
    Public WithEvents Transmision As TabPage
    Public WithEvents Bomba As TabPage
    Friend WithEvents PanelTempAguaM As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelTempAceiteM As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelCombustibleM As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelCargaM As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelRPMM As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelRPMT As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelPresionT As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelMarchaT As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelTempT As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelCaudalB As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelPresion2B As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelTempB As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelCargaB As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelPresionB As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelHHPB As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PanelTRIPB As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents ErrorProgressM2 As Guna.UI2.WinForms.Guna2WinProgressIndicator
    Public WithEvents ErrorProgressT2 As Guna.UI2.WinForms.Guna2WinProgressIndicator
    Public WithEvents ErrorProgressB As Guna.UI2.WinForms.Guna2WinProgressIndicator
End Class
