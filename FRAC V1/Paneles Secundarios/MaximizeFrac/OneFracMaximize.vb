﻿Public Class OneFracMaximize

    Private Frac As Integer

    Private Shared Event NewError()
    Private Shared Event NewDiag(sender As String)

    Private RPM As Integer

    Private Shared _totalerror As Integer
    Private Shared Property TotalError() As Integer
        Get
            Return _totalerror
        End Get
        Set(value As Integer)
            If value <> _totalerror Then
                _totalerror = value
                If _totalerror <> 0 Then
                    'RaiseEvent NewError()
                End If
            End If
        End Set
    End Property
    Private Shared _presiondescarga As Integer
    Private Shared Property PresionDescarga(sender As Object) As Integer
        Get
            Return _presiondescarga
        End Get
        Set(value As Integer)
            If value <> _presiondescarga Then
                _presiondescarga = value
                If _presiondescarga < -1200 Then
                    sender.Image = My.Resources._error
                Else
                    sender.Image = My.Resources.check
                End If
            End If
        End Set
    End Property
    Private Shared _presionsuccion As Integer
    Private Shared Property PresionSuccion(sender As Object) As Integer
        Get
            Return _presionsuccion
        End Get
        Set(value As Integer)
            If value <> _presionsuccion Then
                _presionsuccion = value
                If _presionsuccion <= -10 Then
                    sender.Image = My.Resources._error
                ElseIf _presionsuccion < 30 And _presionsuccion > -10 Then
                    sender.Image = My.Resources.warning
                Else
                    sender.Image = My.Resources.check
                End If
            End If
        End Set
    End Property
    Private Shared _templub As Integer
    Private Shared Property TempLUB(sender As Object) As Integer
        Get
            Return _templub
        End Get
        Set(value As Integer)
            If value <> _templub Then
                _templub = value
                If _templub < -35 Then
                    sender.Image = My.Resources._error
                ElseIf _templub > 130 Then
                    sender.Image = My.Resources.warning
                Else
                    sender.Image = My.Resources.check
                End If
            End If
        End Set
    End Property
    Private Shared _preslub As Integer
    Private Shared Property PresLUB(sender As Object) As Integer
        Get
            Return _preslub
        End Get
        Set(value As Integer)
            If value <> _preslub Then
                _preslub = value
                If _preslub < -35 Then
                    sender.Image = My.Resources._error
                ElseIf _preslub > 150 Then
                    sender.Image = My.Resources.warning
                Else
                    sender.Image = My.Resources.check
                End If
            End If
        End Set
    End Property
    Private Shared _tempaceitemotor As Integer
    Private Shared Property TempAceiteMotor(sender As Object) As Integer
        Get
            Return _tempaceitemotor
        End Get
        Set(value As Integer)
            If value <> _tempaceitemotor Then
                _tempaceitemotor = value
            End If
        End Set
    End Property
    Private Shared _tempaguamotor As Integer
    Private Shared Property TempAguaMotor(sender As Object) As Integer
        Get
            Return _tempaguamotor
        End Get
        Set(value As Integer)
            If value <> _tempaguamotor Then
                _tempaguamotor = value
            End If
        End Set
    End Property
    Private Shared _presionaceitemotor As Integer
    Private Shared Property PresionAceiteMotor(sender As Object) As Integer
        Get
            Return _presionaceitemotor
        End Get
        Set(value As Integer)
            If value <> _presionaceitemotor Then
                _presionaceitemotor = value
            End If
        End Set
    End Property
    Private Shared _combustiblemotor As Integer
    Private Shared Property CombustibleMotor(sender As Object) As Integer
        Get
            Return _combustiblemotor
        End Get
        Set(value As Integer)
            If value <> _combustiblemotor Then
                _combustiblemotor = value
            End If
        End Set
    End Property
    Private Shared _horasmotor As Integer
    Private Shared Property HorasMotor() As Integer
        Get
            Return _horasmotor
        End Get
        Set(value As Integer)
            If value <> _horasmotor Then
                _horasmotor = value
            End If
        End Set
    End Property
    Private Shared _tempaceitetrans As Integer
    Private Shared Property TempAceiteTrans(sender As Object) As Integer
        Get
            Return _tempaceitetrans
        End Get
        Set(value As Integer)
            If value <> _tempaceitetrans Then
                _tempaceitetrans = value
                If _tempaceitetrans > 95 Then
                    sender.Image = My.Resources.warning
                Else
                    sender.Image = My.Resources.check
                End If
            End If
        End Set
    End Property
    Private Shared _presaceitetrans As Integer
    Private Shared Property PresAceiteTrans(sender As Object) As Integer
        Get
            Return _presaceitetrans
        End Get
        Set(value As Integer)
            If value <> _presaceitetrans Then
                _presaceitetrans = value
                If _presaceitetrans <= -10 Then
                    sender.Image = My.Resources._error
                ElseIf _presaceitetrans < 100 And _presaceitetrans > -10 Then
                    sender.Image = My.Resources.warning
                ElseIf _presaceitetrans > 320 Then
                    sender.Image = My.Resources.warning
                Else
                    sender.Image = My.Resources.check
                End If
            End If
        End Set
    End Property

    Private MyEngine As Dictionary(Of String, Object)
    Private MyTransmission As Dictionary(Of String, Object)
    Private Sub OneFracTest_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Frac = currentFrac
        MyEngine = FracData.SelectedEngines("SelectedEngine_" + Frac.ToString())
        MyTransmission = FracData.SelectedTransmissions("SelectedTransmission_" + Frac.ToString())

        If FracAutomation.AutoStart = True Then
            PbHandle()
        End If

        AddHandler FracAutomation.AutomationStart, AddressOf PbHandle

        TmrRead.Start()
        TmrDiagnostico.Start()
        TmrCheck.Start()

    End Sub

    Public Sub CenterEverything()

        CenterObject(Title1)
        CenterObject(Title2)
        CenterObject(Title3)

        ''Pantalla General
        CenterObject(LblRPM3)
        CenterObject(LblRPMActualG)
        CenterObject(BtnIdle)

        CenterObject(LblCaudal)
        CenterObject(LblCaudalActualG)
        CenterObject(BtnFC)

        CenterObject(LblMarcha)
        CenterObject(LblMarcha1)
        CenterObject(LblMarcha3)
        CenterObject(LblMarchaActualG)
        CenterObject(LblMarchaDeseadaG)

        CenterObject(LblTransmisionGeneral)
        CenterObject(LblTransTempGeneral)
        CenterObject(LblTransPresionGeneral)
        CenterObject(LblTransRPMGeneral)
        CenterObject(LblTPresionG)
        CenterObject(LblTTempG)
        CenterObject(LblTRpmOutG)

        CenterObject(LblBombaGeneral)

        CenterObject(LblTOp)
        CenterObject(LblTOp1)

        CenterObject(LblPCorte1)
        CenterObject(LblPCorte3)
        CenterObject(LblPCorteActualG)
        CenterObject(LblPCorteDeseadaG)
        CenterObject(BtnTrip)
        CenterObject(BtnTripZero)

        ''Pantalla Motor
        For Each Lb As Label In PanelTempAceiteM.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelTempAguaM.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelCombustibleM.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelCargaM.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelRPMM.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next

        CenterObject(LblErroresMotor)
        CenterObject(LblDiagnosticoMotor)

        CenterObject(LblBombaHorasMotorMotor)
        CenterObject(LblMotorCombustible1Motor)
        CenterObject(LblMotorVoltajeMotor)
        CenterObject(LblMotorPresionMotor)
        CenterObject(LblBHorasMotorM)
        CenterObject(LblMCombustibleM)
        CenterObject(LblMVoltajeM)
        CenterObject(LblMPresionM)
        CenterObject(LblUnidad1M)
        CenterObject(LblUnidad2M)
        CenterObject(LblUnidad3M)
        CenterObject(LblUnidad4M)

        ''Pantalla Trans
        CenterObject(LblErroresTrans)
        CenterObject(LblDiagnosticoTrans)
        For Each Lb As Label In PanelTempT.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelMarchaT.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelRPMT.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelPresionT.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next

        ''Pantalla Bomba
        CenterObject(LblDiagnosticoBomba)
        For Each Lb As Label In PanelPresionB.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelPresion2B.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelTRIPB.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelHHPB.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelCaudalB.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelTempB.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next
        For Each Lb As Label In PanelCargaB.Controls.OfType(Of Label)
            CenterObject(Lb)
        Next

        ''Errores
        CenterObject(ErrorProgressM1)
        CenterObject(ErrorProgressM2)
        CenterObject(ErrorProgressT1)
        CenterObject(ErrorProgressT2)
        CenterObject(ErrorProgressB)

        ''Panel Motor
        LeftAlign(LblMTempAguaG, LblMotorTempAguaGeneral)
        LeftAlign(LblMTempAceiteG, LblMotorTempAceiteGeneral)
        LeftAlign(LblMPresionG, LblMotorPresionGeneral)
        LeftAlign(LblMCombustibleG, LblMotorCombustible1General)
        LeftAlign(LblMVoltajeG, LblMotorVoltajeGeneral)
        LeftAlign(LblMCombustibleP100G, LblMotorCombustible2General)

        ''Panel Bomba
        LeftAlign(LblBTempLUBG, LblBombaTempGeneral)
        LeftAlign(LblBPresSuccionG, LblBombaPresGeneral)
        LeftAlign(LblBPresLUBG, LblBombaPres1General)
        LeftAlign(LblBCargaMotorG, LblBombaCargaMotorGeneral)
        LeftAlign(LblBHorasMotorG, LblBombaHMGeneral)
        LeftAlign(LblBHHPG, LblBombaHHPGeneral)

    End Sub

    Private Sub VisibleM(ByVal Visible As Boolean)
        For Each Lb As Label In PanelErroresM.Controls.OfType(Of Label)
            Lb.Visible = Visible
        Next
        For Each Bn As Guna.UI2.WinForms.Guna2Button In PanelErroresM.Controls.OfType(Of Guna.UI2.WinForms.Guna2Button)
            Bn.Visible = Visible
        Next

        For Each Lb As Label In PanelDiagM.Controls.OfType(Of Label)
            Lb.Visible = Visible
        Next
        For Each Pb As Guna.UI2.WinForms.Guna2PictureBox In PanelDiagM.Controls.OfType(Of Guna.UI2.WinForms.Guna2PictureBox)
            Pb.Visible = Visible
        Next

        ErrorProgressM1.Visible = Not Visible
        ErrorProgressM2.Visible = Not Visible
    End Sub

    Private Sub VisibleT(ByVal Visible As Boolean)
        For Each Lb As Label In PanelErroresT.Controls.OfType(Of Label)
            Lb.Visible = Visible
        Next
        For Each Bn As Guna.UI2.WinForms.Guna2Button In PanelErroresT.Controls.OfType(Of Guna.UI2.WinForms.Guna2Button)
            Bn.Visible = Visible
        Next

        For Each Lb As Label In PanelDiagT.Controls.OfType(Of Label)
            Lb.Visible = Visible
        Next
        For Each Pb As Guna.UI2.WinForms.Guna2PictureBox In PanelDiagT.Controls.OfType(Of Guna.UI2.WinForms.Guna2PictureBox)
            Pb.Visible = Visible
        Next
        HSep1.Visible = Visible
        ErrorProgressT1.Visible = Not Visible
        ErrorProgressT2.Visible = Not Visible
    End Sub

    Private Sub VisibleB(ByVal Visible As Boolean)
        For Each Lb As Label In PanelDiagB.Controls.OfType(Of Label)
            Lb.Visible = Visible
        Next
        For Each Pb As Guna.UI2.WinForms.Guna2PictureBox In PanelDiagB.Controls.OfType(Of Guna.UI2.WinForms.Guna2PictureBox)
            Pb.Visible = Visible
        Next
        ErrorProgressB.Visible = Not Visible
    End Sub

    Delegate Sub ReadThreadDelegate()
    Private Sub TmrRead_Tick(sender As Object, e As EventArgs) Handles TmrRead.Tick
        Dim ReadThread As New Task(AddressOf ReadThreadDo)
        ReadThread.Start()
    End Sub

    Private Sub ReadThreadDo()
        Invoke(New ReadThreadDelegate(AddressOf DisplayData))
    End Sub

    Private Sub DisplayData()

        TotalError = FracData.DblVFrac("TotalErr_" + Frac.ToString())

        ''Display RPM
        LblRPMActualG.Text = FracData.DblVFrac("RPMMotor_" + Frac.ToString())
        LblRPMDeseadaG.Text = FracData.DblVFrac("RPMDeseada_" + Frac.ToString())
        ''Display Marcha
        LblMarchaDeseadaG.Text = FracData.StrVFrac("MarchaDeseada_" + Frac.ToString())
        LblMarchaActualG.Text = FracData.StrVFrac("MarchaActual_" + Frac.ToString())
        ''Display Caudal
        LblCaudalActualG.Text = FracData.DblVFrac("Caudal_" + Frac.ToString())
        ''Display Presion de Corte
        LblPCorteDeseadaG.Text = FracData.DblVFrac("TRIP_" + Frac.ToString())
        LblPCorteActualG.Text = FracData.DblVFrac("Viatran_" + Frac.ToString())
        ''Display Motor
        LblMTempAguaG.Text = FracData.DblVFrac("TemperaturaAguaMotor_" + Frac.ToString())
        LblMTempAceiteG.Text = FracData.DblVFrac("TemperaturaAceiteMotor_" + Frac.ToString())
        LblMPresionG.Text = FracData.DblVFrac("PresionAceiteMotor_" + Frac.ToString())
        LblMCombustibleG.Text = FracData.DblVFrac("FuelRate_" + Frac.ToString())
        LblMVoltajeG.Text = FracData.DblVFrac("Voltaje_" + Frac.ToString())
        LblMCombustibleP100G.Text = FracData.DblVFrac("CombustibleTanque_" + Frac.ToString())
        ''Display Transmision
        LblTTempG.Text = FracData.DblVFrac("TemperaturaAceiteTransmision_" + Frac.ToString())
        LblTPresionG.Text = FracData.DblVFrac("PresTrans_" + Frac.ToString())
        LblTRpmOutG.Text = FracData.DblVFrac("RPMSalidaTransmision_" + Frac.ToString())
        ''Display Bomba
        LblBTempLUBG.Text = FracData.DblVFrac("TemperaturaLUB_LP_" + Frac.ToString())
        LblBPresSuccionG.Text = FracData.DblVFrac("PresSuc_" + Frac.ToString())
        LblBPresLUBG.Text = FracData.DblVFrac("PresionLUB_HP_" + Frac.ToString())
        LblBCargaMotorG.Text = FracData.DblVFrac("PorcentajeCarga_" + Frac.ToString())
        LblBHorasMotorG.Text = FracData.DblVFrac("HorasMotor_" + Frac.ToString())
        LblBHHPG.Text = FracData.DblVFrac("HHPMotor_" + Frac.ToString())
        ''Display Errores
        LblErrorGrave.Visible = FracData.DblVFrac("LamparaRoja_" + Frac.ToString())
        LblErrorAdmisible.Visible = FracData.DblVFrac("LamparaAmbar_" + Frac.ToString())

        ''Display RPM
        LblRPMActualM.Text = FracData.DblVFrac("RPMMotor_" + Frac.ToString())
        LblRPMDeseadaM.Text = FracData.DblVFrac("RPMDeseada_" + Frac.ToString())
        ''Display Motor
        LblMTempAguaM.Text = FracData.DblVFrac("TemperaturaAguaMotor_" + Frac.ToString())
        LblMTempAceiteM.Text = FracData.DblVFrac("TemperaturaAceiteMotor_" + Frac.ToString())
        LblMPresionM.Text = FracData.DblVFrac("PresionAceiteMotor_" + Frac.ToString())
        LblMCombustibleM.Text = FracData.DblVFrac("FuelRate_" + Frac.ToString())
        LblMVoltajeM.Text = FracData.DblVFrac("Voltaje_" + Frac.ToString())
        LblMCombustibleP100M.Text = FracData.DblVFrac("CombustibleTanque_" + Frac.ToString())
        LblBHorasMotorM.Text = FracData.DblVFrac("HorasMotor_" + Frac.ToString())
        LblBCargaMotorM.Text = FracData.DblVFrac("PorcentajeCarga_" + Frac.ToString())

        ''Display Marcha
        LblMarchaDeseadaT.Text = FracData.StrVFrac("MarchaDeseada_" + Frac.ToString())
        LblMarchaActualT.Text = FracData.StrVFrac("MarchaActual_" + Frac.ToString())
        ''Display Transmision
        LblTTempT.Text = FracData.DblVFrac("TemperaturaAceiteTransmision_" + Frac.ToString())
        LblTPresionT.Text = FracData.DblVFrac("PresTrans_" + Frac.ToString())
        LblTRpmOutT.Text = FracData.DblVFrac("RPMSalidaTransmision_" + Frac.ToString())

        ''Display Presion de Corte
        LblPCorteDeseadaB.Text = FracData.DblVFrac("TRIP_" + Frac.ToString())
        LblPCorteActualB.Text = FracData.DblVFrac("Viatran_" + Frac.ToString())
        ''Display Bomba
        LblBTempLUBB.Text = FracData.DblVFrac("TemperaturaLUB_LP_" + Frac.ToString())
        LblBPresSuccionB.Text = FracData.DblVFrac("PresSuc_" + Frac.ToString())
        LblBPresLUBB.Text = FracData.DblVFrac("PresionLUB_HP_" + Frac.ToString())
        LblBCargaMotorB.Text = FracData.DblVFrac("PorcentajeCarga_" + Frac.ToString())
        LblBHHPB.Text = FracData.DblVFrac("HHPMotor_" + Frac.ToString())
        ''Display Caudal
        LblCaudalActualB.Text = FracData.DblVFrac("Caudal_" + Frac.ToString())

        Try
            RgRPMG.Value = (FracData.DblVFrac("RPMMotor_" + Frac.ToString()) / 1950) * 100
            RgPresionG.Value = (FracData.DblVFrac("Viatran_" + Frac.ToString()) / FracData.DblVFrac("TRIP_" + Frac.ToString())) * 100
        Catch Ex As Exception
            LogError(Ex)
        End Try

        CenterEverything()
    End Sub

    Private Sub ErrorHandle() Handles MyClass.NewError
        Select Case MaximizeTabControl.SelectedIndex
            Case 0
                If TotalError <> 0 Then
                    BtnErrores.Visible = True
                Else
                    BtnErrores.Visible = False
                End If
            Case 1
                If TotalError <> 0 Then
                    If FracData.DblVFrac("FuenteError_" + currentFrac.ToString()) = 0 Then

                    Else
                        ''Ningun error en motor
                    End If
                End If
            Case 2
                If TotalError <> 0 Then
                    If FracData.DblVFrac("FuenteError_" + currentFrac.ToString()) = 3 Then

                    Else
                        ''NINGUN ERROR EN TRANSMISION
                    End If
                End If
        End Select
    End Sub

    Private Sub MaximizeTabControl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles MaximizeTabControl.SelectedIndexChanged

        TmrRead_Tick(Me, EventArgs.Empty)

        If MaximizeTabControl.SelectedIndex = 1 Then
            TmrErrorCheck.Start()
            ErrorProgressM1.Start()
            ErrorProgressM2.Start()
            VisibleM(False)
            LblErroresMotor.Visible = True
            LblDiagnosticoMotor.Visible = True
        ElseIf MaximizeTabControl.SelectedIndex = 2 Then
            TmrErrorCheck.Start()
            ErrorProgressT1.Start()
            ErrorProgressT2.Start()
            VisibleT(False)
            LblErroresTrans.Visible = True
            LblDiagnosticoTrans.Visible = True
        ElseIf MaximizeTabControl.SelectedIndex = 3 Then
            TmrErrorCheck.Start()
            ErrorProgressB.Start()
            VisibleB(False)
            LblDiagnosticoBomba.Visible = True
        End If
    End Sub

    Private Sub TmrDiagnostico_Tick(sender As Object, e As EventArgs) Handles TmrDiagnostico.Tick


        PresionDescarga(PbSPresDescarga) = FracData.DblVFrac("Viatran_" + currentFrac.ToString())
        PresionSuccion(PbSPresSuccion) = FracData.DblVFrac("PresSuc_" + currentFrac.ToString())
        TempLUB(PbSTempLub) = FracData.DblVFrac("TemperaturaLUB_LP_" + currentFrac.ToString())
        PresLUB(PbSPresLUB) = FracData.DblVFrac("PresionLUB_HP_" + currentFrac.ToString())
        TempAceiteMotor(PbMTempAceite) = FracData.DblVFrac("TemperaturaAceiteMotor_" + currentFrac.ToString())
        TempAguaMotor(PbMTempAgua) = FracData.DblVFrac("TemperaturaAguaMotor_" + currentFrac.ToString())
        PresionAceiteMotor(PbMPresAceite) = FracData.DblVFrac("PresionAceiteMotor_" + currentFrac.ToString())
        CombustibleMotor(PbMCombustible) = FracData.DblVFrac("FuelRate_" + currentFrac.ToString())
        HorasMotor() = FracData.DblVFrac("HorasMotor_" + currentFrac.ToString())
        TempAceiteTrans(PbTTempAceite) = FracData.DblVFrac("TemperaturaAceiteTransmision_" + currentFrac.ToString())
        PresAceiteTrans(PbTPresAceite) = FracData.DblVFrac("PresTrans_" + currentFrac.ToString())

        LblAceiteTransmision.Text = GetInteger(FracDiagnostico.TargetAceiteTransmision - HorasMotor)

        If GetInteger(LblAceiteTransmision.Text) < 24 Then
            PbAceiteTransmision.Image = My.Resources.warning
        ElseIf GetInteger(LblAceiteTransmision.Text) < 0 Then
            PbAceiteTransmision.Image = My.Resources._error
        Else
            PbAceiteTransmision.Image = My.Resources.check
        End If

    End Sub


    ''Region dedicada al control de RPM
#Region "RPM Control"
    Private Sub RpmPlus(sender As Object, e As EventArgs) Handles BtnRpmPlus50.Click, BtnRpmPlus25.Click
        Dim RpmPlusTh As New Task(Sub() RpmPlusDo(sender))
        RpmPlusTh.Start()
    End Sub

    Private Sub RpmPlusDo(ByVal sender As Object)
        Dim RpmPlus As Integer = GetInteger(sender.name)
        RPM = Clamp(MyEngine("MinRPM"), MyEngine("MaxRPM"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) + RpmPlus)
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), RPM, Frac)
    End Sub

    Private Sub RpmMinus(sender As Object, e As EventArgs) Handles BtnRpmMinus25.Click, BtnRpmMinus50.Click
        Dim RpmMinusTh As New Task(Sub() RpmMinusDo(sender))
        RpmMinusTh.Start()
    End Sub

    Private Sub RpmMinusDo(sender As Object)
        Dim RpmMinus As Integer = GetInteger(sender.name)
        RPM = Clamp(MyEngine("MinRPM"), MyEngine("MaxRPM"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) - RpmMinus)
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), RPM, Frac)
    End Sub

    Private Sub BtnIdle_Click(sender As Object, e As EventArgs) Handles BtnIdle.Click
        Dim RpmIdleTh As New Task(AddressOf RpmIdle)
        RpmIdleTh.Start()
    End Sub

    Private Async Sub RpmIdle()
        ''Motor en IDLE
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), MyEngine("MinRPM"), Frac)
        Await Task.Delay(100)
        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), FracGeneralValues("Marcha0"), Frac)
    End Sub

    Private Sub LblRPMDeseada_Click(sender As Object, e As EventArgs) Handles LblRPMDeseadaG.Click
        currentFrac = Frac
        SetRPM.ShowDialog(Me)
    End Sub
#End Region

    ''Region para encender motor
#Region "EngineStart"
    Private Sub BtnStartEngine_Click(sender As Object, e As EventArgs) Handles BtnStartEngine.Click
        Dim EngineStartTh As New Task(AddressOf EngineStart)
        EngineStartTh.Start()
    End Sub

    Private Async Sub EngineStart()
        If BtnStartEngine.Checked = False Then
            ISystem.MoxaSend(FracMoxaSendStartAddress("Arranque_StartAddress"), FracGeneralValues("ArranqueON"), Frac)
            Await Task.Delay(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("Arranque_StartAddress"), FracGeneralValues("ArranqueOFF"), Frac)
        Else
            ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerON"), Frac)
            Await Task.Delay(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerOFF"), Frac)
        End If
    End Sub
#End Region


    Private Sub BtnTrip_Click(sender As Object, e As EventArgs) Handles BtnTrip.Click
        currentFrac = Frac
        SetTRIP.ShowDialog(Me)
    End Sub

    Private Sub BtnFC_Click(sender As Object, e As EventArgs) Handles BtnFC.Click
        currentFrac = Frac
        SetFC.ShowDialog(Me)
    End Sub

    Private Sub BtnTripZero_Click(sender As Object, e As EventArgs) Handles BtnTripZero.Click
        currentFrac = Frac
        SetZERO.ShowDialog(Me)
    End Sub

    ''Region dedicada al control de Gear
#Region "Gear Select"
    Private Sub BtnPlusGear_Click(sender As Object, e As EventArgs) Handles BtnPlusGear.Click
        Dim GearPlusTh As New Task(AddressOf GearPlus)
        GearPlusTh.Start()
    End Sub

    Private Sub GearPlus()
        Dim marcha As Integer

        Select Case LblMarchaActualG.Text
            Case "N"
                marcha = FracGeneralValues("Marcha1")
            Case "1"
                marcha = FracGeneralValues("Marcha2")
            Case "2"
                marcha = FracGeneralValues("Marcha3")
            Case "3"
                marcha = FracGeneralValues("Marcha4")
            Case "4"
                marcha = FracGeneralValues("Marcha5")
            Case "5"
                marcha = FracGeneralValues("Marcha6")
            Case "6"
                marcha = FracGeneralValues("Marcha7")
            Case "7"
                marcha = FracGeneralValues("Marcha7")
        End Select

        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, Frac)
    End Sub

    Private Sub BtnMinusGear_Click(sender As Object, e As EventArgs) Handles BtnMinusGear.Click
        Dim GearMinusTh As New Task(AddressOf GearMinus)
        GearMinusTh.Start()
    End Sub

    Private Sub GearMinus()
        Dim marcha As Integer

        Select Case LblMarchaActualG.Text

            Case "N"
                marcha = FracGeneralValues("Marcha0")
            Case "1"
                marcha = FracGeneralValues("Marcha0")
            Case "2"
                marcha = FracGeneralValues("Marcha1")
            Case "3"
                marcha = FracGeneralValues("Marcha2")
            Case "4"
                marcha = FracGeneralValues("Marcha3")
            Case "5"
                marcha = FracGeneralValues("Marcha4")
            Case "6"
                marcha = FracGeneralValues("Marcha5")
            Case "7"
                marcha = FracGeneralValues("Marcha6")
        End Select

        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, Frac)
    End Sub

    Private Sub LblMarchaDeseada_Click(sender As Object, e As EventArgs) Handles LblMarchaDeseadaG.Click
        currentFrac = Frac
        SetGEAR.ShowDialog(Me)
    End Sub
#End Region

    Private Sub BtnAjustes_Click(sender As Object, e As EventArgs) Handles BtnAjustes.Click
        currentFrac = Frac
        AjustesFracturador.ShowDialog(Me)
    End Sub

    Private Sub BtnDiagnostico_Click(sender As Object, e As EventArgs) Handles BtnDiagnostico.Click
        currentFrac = Frac
        FracDiagnostico.ShowDialog(Me)
    End Sub

    Private Sub BtnErrores_Click(sender As Object, e As EventArgs) Handles BtnErrores.Click
        currentFrac = Frac
        FracErrores.ShowDialog(Me)
    End Sub

    Private Sub PbAutomatic_Click(sender As Object, e As EventArgs) Handles PbAutomatic.Click
        InfoShow.ShowDialog(Me)
    End Sub
    Private Sub PbHandle()
        PbAutomatic.Visible = FracAutomation.AutoStart
    End Sub

    Delegate Sub BtnDelegate()
    Private Sub TmrCheck_Tick(sender As Object, e As EventArgs) Handles TmrCheck.Tick
        Dim CheckHandler As New Task(AddressOf BgCheck)
        CheckHandler.Start()
        FracDiagnostico.CheckFrac()
    End Sub

    Private Sub BgCheck()
        Me.Invoke(New BtnDelegate(AddressOf CheckUpdate))
    End Sub

    Private Sub CheckUpdate()

        If GetInteger(LblRPMActualG.Text) > 100 Then
            BtnStartEngine.Checked = True
        Else
            BtnStartEngine.Checked = False
        End If

        If TotalError <> 0 Then
            BtnErrores.Visible = True
        Else
            BtnErrores.Visible = False
        End If

        If FracDiagnostico.sError = True Then
            BtnDiagnostico.FillColor = Color.FromArgb(255, 128, 128)
        ElseIf FracDiagnostico.sWarning = True Then
            BtnDiagnostico.FillColor = Color.FromArgb(255, 147, 74)
        Else
            BtnDiagnostico.FillColor = Color.Transparent
        End If

        If FracData.DblVFrac("HHPMotor_" + Frac.ToString()) >= 2240 Then
            BtnPlusGear.FillColor = Color.FromArgb(255, 64, 64)
        Else
            BtnPlusGear.FillColor = Color.Transparent
        End If

    End Sub

    Private Sub PbAceiteTransmision_Click(sender As Object, e As EventArgs) Handles PbAceiteTransmision.Click

        Dim senderName As String = Replace(sender.name, "Pb", Nothing)
        If FracMantenimiento.ShowDialog(Me) = DialogResult.OK Then
            My.Settings.Item("Target" + senderName + "_Frac" + currentFrac.ToString()) = HorasMotor + My.Settings.Item("Target" + senderName)
            My.Settings.Save()
            Dim Lb As Control() = Me.Controls.Find("Lbl" + senderName, True)
            Lb(0).Text = My.Settings.Item("Target" + senderName + "_Frac" + currentFrac.ToString())
        End If

    End Sub


    Private Sub TmrErrorCheck_Tick(sender As Object, e As EventArgs) Handles TmrErrorCheck.Tick
        Select Case MaximizeTabControl.SelectedIndex
            Case 1

                ISystem.MoxaSend(FracMoxaSendStartAddress("VB_CTRL_StartAddress"), 0, Frac)

                ErrorProgressM1.Stop()
                ErrorProgressM2.Stop()
                VisibleM(True)
            Case 2
                ISystem.MoxaSend(FracMoxaSendStartAddress("VB_CTRL_StartAddress"), 3, Frac)
                ErrorProgressT1.Stop()
                ErrorProgressT2.Stop()
                VisibleT(True)
            Case 3
                ErrorProgressB.Stop()
                VisibleB(True)
        End Select
        TmrErrorCheck.Stop()
    End Sub
End Class
