﻿Public Class SetALLTRIP
    Private Sub SetALLTRIP_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CenterEverything()
        ActiveControl = TbTRIP
    End Sub

    Public Sub CenterEverything()
        CenterObject(Title)
        LeftAlign(LblTB, LblTB.Width + 6)
        LeftAlign(TbTRIP, 12 + LblTB.Width + TbTRIP.Width)
        CenterObject(BtnAceptar)
        TRIPPanel.Width = 12 + LblTB.Width + 6 + TbTRIP.Width + 6
        CenterObject(TRIPPanel)
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If Not String.IsNullOrEmpty(TbTRIP.Text) Then
            If IsInteger(TbTRIP.Text) Then
                Try
                    ''TRIP a todos
                    For Fracs = 1 To ConnectedFracsID.Count
                        Dim CurrentFrac As Integer = ConnectedFracsID(Fracs - 1).ToString()
                        Dim TRIP As Integer = GetInteger(TbTRIP.Text) + PZero(CurrentFrac)       ''VER LO DE PZERO
                        TRIP = Math.Max(0, TRIP)
                        TRIP = Math.Min(MaxTRIP, TRIP)
                        ISystem.MoxaSend(FracMoxaSendStartAddress("TRIP_StartAddress"), TRIP, CurrentFrac)
                    Next
                Catch ex As Exception
                    LogError(ex)
                End Try

                Me.DialogResult = DialogResult.OK
            End If
        End If
        Close()
    End Sub
End Class