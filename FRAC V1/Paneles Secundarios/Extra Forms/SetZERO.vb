﻿Public Class SetZERO
    Private Sub SetZERO_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ActiveControl = TbZERO
        CenterEverything()
    End Sub

    Public Sub CenterEverything()
        CenterObject(Title)
        LeftAlign(LblTB, LblTB.Width + 6)
        LeftAlign(TbZERO, 12 + LblTB.Width + TbZERO.Width)
        CenterObject(BtnAceptar)
        SetZeroPanel.Width = 12 + LblTB.Width + 6 + TbZERO.Width + 6
        CenterObject(SetZeroPanel)
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If Not String.IsNullOrEmpty(TbZERO.Text) Then
            If IsInteger(TbZERO.Text) Then
                PZero(currentFrac) = GetInteger(TbZERO.Text)
                Me.DialogResult = DialogResult.OK
            End If
        End If
    End Sub
End Class