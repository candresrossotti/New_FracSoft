﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FracErrores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblDescripcion = New System.Windows.Forms.Label()
        Me.LblDescripcionD = New System.Windows.Forms.Label()
        Me.LblUnidad = New System.Windows.Forms.Label()
        Me.LblUnidadD = New System.Windows.Forms.Label()
        Me.LblActivo = New System.Windows.Forms.Label()
        Me.LblActivoD = New System.Windows.Forms.Label()
        Me.LblProcedencia = New System.Windows.Forms.Label()
        Me.LblErrorTotales = New System.Windows.Forms.Label()
        Me.LblErrorN = New System.Windows.Forms.Label()
        Me.LblOC = New System.Windows.Forms.Label()
        Me.LblFMI = New System.Windows.Forms.Label()
        Me.LblSPN = New System.Windows.Forms.Label()
        Me.LblProcedenciaD = New System.Windows.Forms.Label()
        Me.LblErroresTotales = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblInfo = New System.Windows.Forms.Label()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.BtnBorrarTodo = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnBorrarActual = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnSiguiente = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAnterior = New Guna.UI2.WinForms.Guna2Button()
        Me.TmrUpdate = New System.Windows.Forms.Timer(Me.components)
        Me.TitlePanel.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(700, 38)
        Me.TitlePanel.TabIndex = 62
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(304, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(93, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Errores"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(652, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.LblDescripcion)
        Me.Guna2Panel1.Controls.Add(Me.LblDescripcionD)
        Me.Guna2Panel1.Controls.Add(Me.LblUnidad)
        Me.Guna2Panel1.Controls.Add(Me.LblUnidadD)
        Me.Guna2Panel1.Controls.Add(Me.LblActivo)
        Me.Guna2Panel1.Controls.Add(Me.LblActivoD)
        Me.Guna2Panel1.Controls.Add(Me.LblProcedencia)
        Me.Guna2Panel1.Controls.Add(Me.LblErrorTotales)
        Me.Guna2Panel1.Controls.Add(Me.LblErrorN)
        Me.Guna2Panel1.Controls.Add(Me.LblOC)
        Me.Guna2Panel1.Controls.Add(Me.LblFMI)
        Me.Guna2Panel1.Controls.Add(Me.LblSPN)
        Me.Guna2Panel1.Controls.Add(Me.LblProcedenciaD)
        Me.Guna2Panel1.Controls.Add(Me.LblErroresTotales)
        Me.Guna2Panel1.Controls.Add(Me.Label4)
        Me.Guna2Panel1.Controls.Add(Me.Label3)
        Me.Guna2Panel1.Controls.Add(Me.Label2)
        Me.Guna2Panel1.Controls.Add(Me.Label1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(12, 44)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(676, 195)
        Me.Guna2Panel1.TabIndex = 71
        '
        'LblDescripcion
        '
        Me.LblDescripcion.BackColor = System.Drawing.Color.Transparent
        Me.LblDescripcion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDescripcion.ForeColor = System.Drawing.Color.White
        Me.LblDescripcion.Location = New System.Drawing.Point(112, 139)
        Me.LblDescripcion.Name = "LblDescripcion"
        Me.LblDescripcion.Size = New System.Drawing.Size(544, 45)
        Me.LblDescripcion.TabIndex = 36
        Me.LblDescripcion.Text = "Descripcion"
        '
        'LblDescripcionD
        '
        Me.LblDescripcionD.AutoSize = True
        Me.LblDescripcionD.BackColor = System.Drawing.Color.Transparent
        Me.LblDescripcionD.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDescripcionD.ForeColor = System.Drawing.Color.White
        Me.LblDescripcionD.Location = New System.Drawing.Point(16, 139)
        Me.LblDescripcionD.Name = "LblDescripcionD"
        Me.LblDescripcionD.Size = New System.Drawing.Size(96, 20)
        Me.LblDescripcionD.TabIndex = 35
        Me.LblDescripcionD.Text = "Descripción:"
        '
        'LblUnidad
        '
        Me.LblUnidad.AutoSize = True
        Me.LblUnidad.BackColor = System.Drawing.Color.Transparent
        Me.LblUnidad.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidad.ForeColor = System.Drawing.Color.White
        Me.LblUnidad.Location = New System.Drawing.Point(554, 90)
        Me.LblUnidad.Name = "LblUnidad"
        Me.LblUnidad.Size = New System.Drawing.Size(59, 20)
        Me.LblUnidad.TabIndex = 34
        Me.LblUnidad.Text = "Unidad"
        '
        'LblUnidadD
        '
        Me.LblUnidadD.AutoSize = True
        Me.LblUnidadD.BackColor = System.Drawing.Color.Transparent
        Me.LblUnidadD.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidadD.ForeColor = System.Drawing.Color.White
        Me.LblUnidadD.Location = New System.Drawing.Point(488, 90)
        Me.LblUnidadD.Name = "LblUnidadD"
        Me.LblUnidadD.Size = New System.Drawing.Size(62, 20)
        Me.LblUnidadD.TabIndex = 33
        Me.LblUnidadD.Text = "Unidad:"
        '
        'LblActivo
        '
        Me.LblActivo.AutoSize = True
        Me.LblActivo.BackColor = System.Drawing.Color.Transparent
        Me.LblActivo.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblActivo.ForeColor = System.Drawing.Color.White
        Me.LblActivo.Location = New System.Drawing.Point(363, 90)
        Me.LblActivo.Name = "LblActivo"
        Me.LblActivo.Size = New System.Drawing.Size(32, 20)
        Me.LblActivo.TabIndex = 32
        Me.LblActivo.Text = "NO"
        '
        'LblActivoD
        '
        Me.LblActivoD.AutoSize = True
        Me.LblActivoD.BackColor = System.Drawing.Color.Transparent
        Me.LblActivoD.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblActivoD.ForeColor = System.Drawing.Color.White
        Me.LblActivoD.Location = New System.Drawing.Point(299, 90)
        Me.LblActivoD.Name = "LblActivoD"
        Me.LblActivoD.Size = New System.Drawing.Size(61, 20)
        Me.LblActivoD.TabIndex = 31
        Me.LblActivoD.Text = "Activo: "
        '
        'LblProcedencia
        '
        Me.LblProcedencia.AutoSize = True
        Me.LblProcedencia.BackColor = System.Drawing.Color.Transparent
        Me.LblProcedencia.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblProcedencia.ForeColor = System.Drawing.Color.White
        Me.LblProcedencia.Location = New System.Drawing.Point(115, 90)
        Me.LblProcedencia.Name = "LblProcedencia"
        Me.LblProcedencia.Size = New System.Drawing.Size(96, 20)
        Me.LblProcedencia.TabIndex = 30
        Me.LblProcedencia.Text = "Procedencia"
        '
        'LblErrorTotales
        '
        Me.LblErrorTotales.AutoSize = True
        Me.LblErrorTotales.BackColor = System.Drawing.Color.Transparent
        Me.LblErrorTotales.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErrorTotales.ForeColor = System.Drawing.Color.White
        Me.LblErrorTotales.Location = New System.Drawing.Point(598, 41)
        Me.LblErrorTotales.Name = "LblErrorTotales"
        Me.LblErrorTotales.Size = New System.Drawing.Size(41, 20)
        Me.LblErrorTotales.TabIndex = 29
        Me.LblErrorTotales.Text = "1000"
        '
        'LblErrorN
        '
        Me.LblErrorN.AutoSize = True
        Me.LblErrorN.BackColor = System.Drawing.Color.Transparent
        Me.LblErrorN.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErrorN.ForeColor = System.Drawing.Color.White
        Me.LblErrorN.Location = New System.Drawing.Point(416, 41)
        Me.LblErrorN.Name = "LblErrorN"
        Me.LblErrorN.Size = New System.Drawing.Size(41, 20)
        Me.LblErrorN.TabIndex = 28
        Me.LblErrorN.Text = "1000"
        '
        'LblOC
        '
        Me.LblOC.AutoSize = True
        Me.LblOC.BackColor = System.Drawing.Color.Transparent
        Me.LblOC.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblOC.ForeColor = System.Drawing.Color.White
        Me.LblOC.Location = New System.Drawing.Point(278, 41)
        Me.LblOC.Name = "LblOC"
        Me.LblOC.Size = New System.Drawing.Size(41, 20)
        Me.LblOC.TabIndex = 27
        Me.LblOC.Text = "1000"
        '
        'LblFMI
        '
        Me.LblFMI.AutoSize = True
        Me.LblFMI.BackColor = System.Drawing.Color.Transparent
        Me.LblFMI.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblFMI.ForeColor = System.Drawing.Color.White
        Me.LblFMI.Location = New System.Drawing.Point(173, 41)
        Me.LblFMI.Name = "LblFMI"
        Me.LblFMI.Size = New System.Drawing.Size(41, 20)
        Me.LblFMI.TabIndex = 26
        Me.LblFMI.Text = "1000"
        '
        'LblSPN
        '
        Me.LblSPN.AutoSize = True
        Me.LblSPN.BackColor = System.Drawing.Color.Transparent
        Me.LblSPN.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblSPN.ForeColor = System.Drawing.Color.White
        Me.LblSPN.Location = New System.Drawing.Point(64, 41)
        Me.LblSPN.Name = "LblSPN"
        Me.LblSPN.Size = New System.Drawing.Size(41, 20)
        Me.LblSPN.TabIndex = 25
        Me.LblSPN.Text = "1000"
        '
        'LblProcedenciaD
        '
        Me.LblProcedenciaD.AutoSize = True
        Me.LblProcedenciaD.BackColor = System.Drawing.Color.Transparent
        Me.LblProcedenciaD.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblProcedenciaD.ForeColor = System.Drawing.Color.White
        Me.LblProcedenciaD.Location = New System.Drawing.Point(16, 90)
        Me.LblProcedenciaD.Name = "LblProcedenciaD"
        Me.LblProcedenciaD.Size = New System.Drawing.Size(99, 20)
        Me.LblProcedenciaD.TabIndex = 24
        Me.LblProcedenciaD.Text = "Procedencia:"
        '
        'LblErroresTotales
        '
        Me.LblErroresTotales.AutoSize = True
        Me.LblErroresTotales.BackColor = System.Drawing.Color.Transparent
        Me.LblErroresTotales.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblErroresTotales.ForeColor = System.Drawing.Color.White
        Me.LblErroresTotales.Location = New System.Drawing.Point(483, 41)
        Me.LblErroresTotales.Name = "LblErroresTotales"
        Me.LblErroresTotales.Size = New System.Drawing.Size(116, 20)
        Me.LblErroresTotales.TabIndex = 23
        Me.LblErroresTotales.Text = "Errores Totales:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(345, 41)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 20)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Error Nº:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(240, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 20)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "OC:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(131, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 20)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "FMI:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(19, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "SPN:"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblInfo)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(676, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblInfo
        '
        Me.LblInfo.AutoSize = True
        Me.LblInfo.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblInfo.Location = New System.Drawing.Point(287, 4)
        Me.LblInfo.Name = "LblInfo"
        Me.LblInfo.Size = New System.Drawing.Size(95, 20)
        Me.LblInfo.TabIndex = 19
        Me.LblInfo.Text = "Información"
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'BtnBorrarTodo
        '
        Me.BtnBorrarTodo.Animated = True
        Me.BtnBorrarTodo.BorderColor = System.Drawing.Color.White
        Me.BtnBorrarTodo.BorderRadius = 5
        Me.BtnBorrarTodo.BorderThickness = 1
        Me.BtnBorrarTodo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnBorrarTodo.FillColor = System.Drawing.Color.Transparent
        Me.BtnBorrarTodo.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnBorrarTodo.ForeColor = System.Drawing.Color.White
        Me.BtnBorrarTodo.Image = Global.sFRAC.My.Resources.Resources.white_delete
        Me.BtnBorrarTodo.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.BtnBorrarTodo.Location = New System.Drawing.Point(522, 245)
        Me.BtnBorrarTodo.Name = "BtnBorrarTodo"
        Me.BtnBorrarTodo.Size = New System.Drawing.Size(164, 40)
        Me.BtnBorrarTodo.TabIndex = 75
        Me.BtnBorrarTodo.Text = "Borrar Todo"
        Me.BtnBorrarTodo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'BtnBorrarActual
        '
        Me.BtnBorrarActual.Animated = True
        Me.BtnBorrarActual.BorderColor = System.Drawing.Color.White
        Me.BtnBorrarActual.BorderRadius = 5
        Me.BtnBorrarActual.BorderThickness = 1
        Me.BtnBorrarActual.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnBorrarActual.FillColor = System.Drawing.Color.Transparent
        Me.BtnBorrarActual.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnBorrarActual.ForeColor = System.Drawing.Color.White
        Me.BtnBorrarActual.Image = Global.sFRAC.My.Resources.Resources.white_delete
        Me.BtnBorrarActual.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.BtnBorrarActual.Location = New System.Drawing.Point(353, 245)
        Me.BtnBorrarActual.Name = "BtnBorrarActual"
        Me.BtnBorrarActual.Size = New System.Drawing.Size(164, 40)
        Me.BtnBorrarActual.TabIndex = 74
        Me.BtnBorrarActual.Text = "Borrar Actual"
        Me.BtnBorrarActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'BtnSiguiente
        '
        Me.BtnSiguiente.Animated = True
        Me.BtnSiguiente.BorderColor = System.Drawing.Color.White
        Me.BtnSiguiente.BorderRadius = 5
        Me.BtnSiguiente.BorderThickness = 1
        Me.BtnSiguiente.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSiguiente.FillColor = System.Drawing.Color.Transparent
        Me.BtnSiguiente.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnSiguiente.ForeColor = System.Drawing.Color.White
        Me.BtnSiguiente.Image = Global.sFRAC.My.Resources.Resources.right_arrow
        Me.BtnSiguiente.ImageAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.BtnSiguiente.Location = New System.Drawing.Point(184, 245)
        Me.BtnSiguiente.Name = "BtnSiguiente"
        Me.BtnSiguiente.Size = New System.Drawing.Size(164, 40)
        Me.BtnSiguiente.TabIndex = 73
        Me.BtnSiguiente.Text = "Siguiente"
        '
        'BtnAnterior
        '
        Me.BtnAnterior.Animated = True
        Me.BtnAnterior.BorderColor = System.Drawing.Color.White
        Me.BtnAnterior.BorderRadius = 5
        Me.BtnAnterior.BorderThickness = 1
        Me.BtnAnterior.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAnterior.FillColor = System.Drawing.Color.Transparent
        Me.BtnAnterior.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnAnterior.ForeColor = System.Drawing.Color.White
        Me.BtnAnterior.Image = Global.sFRAC.My.Resources.Resources.left_arrow
        Me.BtnAnterior.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.BtnAnterior.Location = New System.Drawing.Point(15, 245)
        Me.BtnAnterior.Name = "BtnAnterior"
        Me.BtnAnterior.Size = New System.Drawing.Size(164, 40)
        Me.BtnAnterior.TabIndex = 72
        Me.BtnAnterior.Text = "Anterior"
        '
        'TmrUpdate
        '
        Me.TmrUpdate.Interval = 5000
        '
        'FracErrores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(700, 302)
        Me.Controls.Add(Me.BtnBorrarTodo)
        Me.Controls.Add(Me.BtnBorrarActual)
        Me.Controls.Add(Me.BtnSiguiente)
        Me.Controls.Add(Me.BtnAnterior)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.TitlePanel)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FracErrores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FracErrores"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents TmrUpdate As Timer
    Public WithEvents Title As Label
    Public WithEvents LblProcedenciaD As Label
    Public WithEvents LblErroresTotales As Label
    Public WithEvents Label4 As Label
    Public WithEvents Label3 As Label
    Public WithEvents Label2 As Label
    Public WithEvents Label1 As Label
    Public WithEvents LblInfo As Label
    Public WithEvents LblActivo As Label
    Public WithEvents LblActivoD As Label
    Public WithEvents LblProcedencia As Label
    Public WithEvents LblErrorTotales As Label
    Public WithEvents LblErrorN As Label
    Public WithEvents LblOC As Label
    Public WithEvents LblFMI As Label
    Public WithEvents LblSPN As Label
    Public WithEvents LblDescripcion As Label
    Public WithEvents LblDescripcionD As Label
    Public WithEvents LblUnidad As Label
    Public WithEvents LblUnidadD As Label
    Public WithEvents BtnAnterior As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnBorrarTodo As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnBorrarActual As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnSiguiente As Guna.UI2.WinForms.Guna2Button
End Class
