﻿Public Class FracErrores

    Private FracCurrentError As ErrorInfo
    Private Sub FracErrores_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ISystem.UpdateISystem(Me)
        FracCurrentError = New ErrorInfo(FracData.DblVFrac, ISystem)
        FracCurrentError.Settings("NA", False, currentFrac)

        TmrUpdate.Start()
        TmrUpdate_Tick(Me, e)
    End Sub

    Public Sub CenterEverything()
        CenterObject(Title)

        RightAlign(LblErrorTotales, LblErroresTotales)
        RightAlign(LblProcedencia, LblProcedenciaD)
        RightAlign(LblActivo, LblActivoD)
        RightAlign(LblUnidad, LblUnidadD)
        RightAlign(LblDescripcion, LblDescripcionD)

    End Sub

    Private Sub FracErroresClose(sender As Object, e As FormClosingEventArgs) Handles Me.Closing
        TmrUpdate.Stop()
    End Sub

    Private Sub BtnAnterior_Click(sender As Object, e As EventArgs) Handles BtnAnterior.Click
        FracCurrentError.FindPrevError()
    End Sub

    Private Sub BtnSiguiente_Click(sender As Object, e As EventArgs) Handles BtnSiguiente.Click
        FracCurrentError.FindNextError()
    End Sub

    Private Sub BtnBorrarActual_Click(sender As Object, e As EventArgs) Handles BtnBorrarActual.Click
        FracCurrentError.DeleteCurrentError()
    End Sub

    Private Sub BtnBorrarTodo_Click(sender As Object, e As EventArgs) Handles BtnBorrarTodo.Click
        FracCurrentError.DeleteAllErrors()
    End Sub

    Private Sub TmrUpdate_Tick(sender As Object, e As EventArgs) Handles TmrUpdate.Tick

        LblSPN.Text = FracData.DblVFrac("SPN_" + currentFrac.ToString())
        LblFMI.Text = FracData.DblVFrac("FMI_" + currentFrac.ToString())
        LblOC.Text = FracData.DblVFrac("OC_" + currentFrac.ToString())
        LblErrorN.Text = FracData.DblVFrac("ErrActual_" + CurrentFrac.ToString())
        LblErrorTotales.Text = FracData.DblVFrac("TotalErr_" + CurrentFrac.ToString())
        LblUnidad.Text = ISystem.GetFracName(currentFrac)

        LblActivo.Text = FracCurrentError.GetActive()
        LblDescripcion.Text = FracCurrentError.GetError()
        LblProcedencia.Text = FracCurrentError.GetSource()

    End Sub
End Class