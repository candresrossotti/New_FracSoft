﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ScreenSelect
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.ExtraScreenC1 = New Guna.UI2.WinForms.Guna2TileButton()
        Me.ExtraScreenC2 = New Guna.UI2.WinForms.Guna2TileButton()
        Me.ExtraScreenC8 = New Guna.UI2.WinForms.Guna2TileButton()
        Me.ExtraScreenC4 = New Guna.UI2.WinForms.Guna2TileButton()
        Me.Guna2VSeparator1 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator2 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator3 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator4 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator5 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator6 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator7 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator8 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator9 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator10 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2VSeparator11 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.TitlePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(533, 38)
        Me.TitlePanel.TabIndex = 63
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(159, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(215, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Selección Pantalla"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(485, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'ExtraScreenC1
        '
        Me.ExtraScreenC1.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.ExtraScreenC1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.ExtraScreenC1.BorderThickness = 3
        Me.ExtraScreenC1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ExtraScreenC1.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.ExtraScreenC1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.ExtraScreenC1.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.ExtraScreenC1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.ExtraScreenC1.FillColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.ExtraScreenC1.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.ExtraScreenC1.ForeColor = System.Drawing.Color.White
        Me.ExtraScreenC1.Location = New System.Drawing.Point(12, 44)
        Me.ExtraScreenC1.Name = "ExtraScreenC1"
        Me.ExtraScreenC1.Size = New System.Drawing.Size(250, 250)
        Me.ExtraScreenC1.TabIndex = 64
        Me.ExtraScreenC1.Text = "x1"
        Me.ExtraScreenC1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.ExtraScreenC1.TextOffset = New System.Drawing.Point(-5, 105)
        '
        'ExtraScreenC2
        '
        Me.ExtraScreenC2.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.ExtraScreenC2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.ExtraScreenC2.BorderThickness = 3
        Me.ExtraScreenC2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ExtraScreenC2.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.ExtraScreenC2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.ExtraScreenC2.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.ExtraScreenC2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.ExtraScreenC2.FillColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.ExtraScreenC2.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.ExtraScreenC2.ForeColor = System.Drawing.Color.White
        Me.ExtraScreenC2.Location = New System.Drawing.Point(268, 44)
        Me.ExtraScreenC2.Name = "ExtraScreenC2"
        Me.ExtraScreenC2.Size = New System.Drawing.Size(250, 250)
        Me.ExtraScreenC2.TabIndex = 65
        Me.ExtraScreenC2.Text = "x2"
        Me.ExtraScreenC2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.ExtraScreenC2.TextOffset = New System.Drawing.Point(-5, 105)
        '
        'ExtraScreenC8
        '
        Me.ExtraScreenC8.BackColor = System.Drawing.Color.Transparent
        Me.ExtraScreenC8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.ExtraScreenC8.BorderThickness = 3
        Me.ExtraScreenC8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ExtraScreenC8.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.ExtraScreenC8.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.ExtraScreenC8.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.ExtraScreenC8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.ExtraScreenC8.FillColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.ExtraScreenC8.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.ExtraScreenC8.ForeColor = System.Drawing.Color.White
        Me.ExtraScreenC8.Location = New System.Drawing.Point(268, 300)
        Me.ExtraScreenC8.Name = "ExtraScreenC8"
        Me.ExtraScreenC8.Size = New System.Drawing.Size(250, 250)
        Me.ExtraScreenC8.TabIndex = 67
        Me.ExtraScreenC8.Text = "x8"
        Me.ExtraScreenC8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.ExtraScreenC8.TextOffset = New System.Drawing.Point(-5, 105)
        '
        'ExtraScreenC4
        '
        Me.ExtraScreenC4.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.ExtraScreenC4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.ExtraScreenC4.BorderThickness = 3
        Me.ExtraScreenC4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ExtraScreenC4.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.ExtraScreenC4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.ExtraScreenC4.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.ExtraScreenC4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.ExtraScreenC4.FillColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(94, Byte), Integer), CType(CType(116, Byte), Integer))
        Me.ExtraScreenC4.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.ExtraScreenC4.ForeColor = System.Drawing.Color.White
        Me.ExtraScreenC4.Location = New System.Drawing.Point(12, 300)
        Me.ExtraScreenC4.Name = "ExtraScreenC4"
        Me.ExtraScreenC4.Size = New System.Drawing.Size(250, 250)
        Me.ExtraScreenC4.TabIndex = 66
        Me.ExtraScreenC4.Text = "x4"
        Me.ExtraScreenC4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.ExtraScreenC4.TextOffset = New System.Drawing.Point(-5, 105)
        '
        'Guna2VSeparator1
        '
        Me.Guna2VSeparator1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator1.Location = New System.Drawing.Point(393, 69)
        Me.Guna2VSeparator1.Name = "Guna2VSeparator1"
        Me.Guna2VSeparator1.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator1.TabIndex = 68
        Me.Guna2VSeparator1.UseTransparentBackground = True
        '
        'Guna2VSeparator2
        '
        Me.Guna2VSeparator2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator2.Location = New System.Drawing.Point(74, 325)
        Me.Guna2VSeparator2.Name = "Guna2VSeparator2"
        Me.Guna2VSeparator2.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator2.TabIndex = 69
        Me.Guna2VSeparator2.UseTransparentBackground = True
        '
        'Guna2VSeparator3
        '
        Me.Guna2VSeparator3.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator3.Location = New System.Drawing.Point(137, 325)
        Me.Guna2VSeparator3.Name = "Guna2VSeparator3"
        Me.Guna2VSeparator3.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator3.TabIndex = 70
        Me.Guna2VSeparator3.UseTransparentBackground = True
        '
        'Guna2VSeparator4
        '
        Me.Guna2VSeparator4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator4.Location = New System.Drawing.Point(200, 325)
        Me.Guna2VSeparator4.Name = "Guna2VSeparator4"
        Me.Guna2VSeparator4.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator4.TabIndex = 71
        Me.Guna2VSeparator4.UseTransparentBackground = True
        '
        'Guna2VSeparator5
        '
        Me.Guna2VSeparator5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator5.Location = New System.Drawing.Point(361, 325)
        Me.Guna2VSeparator5.Name = "Guna2VSeparator5"
        Me.Guna2VSeparator5.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator5.TabIndex = 74
        Me.Guna2VSeparator5.UseTransparentBackground = True
        '
        'Guna2VSeparator6
        '
        Me.Guna2VSeparator6.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator6.Location = New System.Drawing.Point(330, 325)
        Me.Guna2VSeparator6.Name = "Guna2VSeparator6"
        Me.Guna2VSeparator6.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator6.TabIndex = 73
        Me.Guna2VSeparator6.UseTransparentBackground = True
        '
        'Guna2VSeparator7
        '
        Me.Guna2VSeparator7.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator7.Location = New System.Drawing.Point(299, 325)
        Me.Guna2VSeparator7.Name = "Guna2VSeparator7"
        Me.Guna2VSeparator7.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator7.TabIndex = 72
        Me.Guna2VSeparator7.UseTransparentBackground = True
        '
        'Guna2VSeparator8
        '
        Me.Guna2VSeparator8.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator8.Location = New System.Drawing.Point(393, 325)
        Me.Guna2VSeparator8.Name = "Guna2VSeparator8"
        Me.Guna2VSeparator8.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator8.TabIndex = 75
        Me.Guna2VSeparator8.UseTransparentBackground = True
        '
        'Guna2VSeparator9
        '
        Me.Guna2VSeparator9.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator9.Location = New System.Drawing.Point(424, 325)
        Me.Guna2VSeparator9.Name = "Guna2VSeparator9"
        Me.Guna2VSeparator9.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator9.TabIndex = 76
        Me.Guna2VSeparator9.UseTransparentBackground = True
        '
        'Guna2VSeparator10
        '
        Me.Guna2VSeparator10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator10.Location = New System.Drawing.Point(456, 325)
        Me.Guna2VSeparator10.Name = "Guna2VSeparator10"
        Me.Guna2VSeparator10.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator10.TabIndex = 77
        Me.Guna2VSeparator10.UseTransparentBackground = True
        '
        'Guna2VSeparator11
        '
        Me.Guna2VSeparator11.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator11.Location = New System.Drawing.Point(487, 325)
        Me.Guna2VSeparator11.Name = "Guna2VSeparator11"
        Me.Guna2VSeparator11.Size = New System.Drawing.Size(1, 200)
        Me.Guna2VSeparator11.TabIndex = 78
        Me.Guna2VSeparator11.UseTransparentBackground = True
        '
        'ScreenSelect
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(533, 560)
        Me.Controls.Add(Me.Guna2VSeparator11)
        Me.Controls.Add(Me.Guna2VSeparator10)
        Me.Controls.Add(Me.Guna2VSeparator9)
        Me.Controls.Add(Me.Guna2VSeparator8)
        Me.Controls.Add(Me.Guna2VSeparator5)
        Me.Controls.Add(Me.Guna2VSeparator6)
        Me.Controls.Add(Me.Guna2VSeparator7)
        Me.Controls.Add(Me.Guna2VSeparator4)
        Me.Controls.Add(Me.Guna2VSeparator3)
        Me.Controls.Add(Me.Guna2VSeparator2)
        Me.Controls.Add(Me.Guna2VSeparator1)
        Me.Controls.Add(Me.ExtraScreenC8)
        Me.Controls.Add(Me.ExtraScreenC4)
        Me.Controls.Add(Me.ExtraScreenC2)
        Me.Controls.Add(Me.ExtraScreenC1)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "ScreenSelect"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ScreenSelect"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents ExtraScreenC1 As Guna.UI2.WinForms.Guna2TileButton
    Friend WithEvents Guna2VSeparator11 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator10 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator9 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator8 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator5 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator6 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator7 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator4 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator3 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator2 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2VSeparator1 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents ExtraScreenC8 As Guna.UI2.WinForms.Guna2TileButton
    Friend WithEvents ExtraScreenC4 As Guna.UI2.WinForms.Guna2TileButton
    Friend WithEvents ExtraScreenC2 As Guna.UI2.WinForms.Guna2TileButton
End Class
