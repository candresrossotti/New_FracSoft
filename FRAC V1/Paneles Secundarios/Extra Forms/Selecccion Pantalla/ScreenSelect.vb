﻿Public Class ScreenSelect

    Public ScreenChange_Sender As Integer
    Private CurrentScreens As Integer
    Private WantedScreens As Integer
    Private Sub ScreenSelect_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Depende si estamos desde la segunda o tercera pantalla que es lo que cambiamos
        Select Case ScreenChange_Sender
            Case 1
                CurrentScreens = My.Settings.currentScreens
            Case 2
                CurrentScreens = My.Settings.currentScreens_2
            Case 3
                CurrentScreens = My.Settings.currentScreens_3
        End Select

        WantedScreens = CurrentScreens

        Select Case CurrentScreens
            Case 1
                ExtraScreenC1.Checked = True
                ExtraScreenC2.Checked = False
                ExtraScreenC4.Checked = False
                ExtraScreenC8.Checked = False
            Case 2
                ExtraScreenC1.Checked = False
                ExtraScreenC2.Checked = True
                ExtraScreenC4.Checked = False
                ExtraScreenC8.Checked = False
            Case 4
                ExtraScreenC1.Checked = False
                ExtraScreenC2.Checked = False
                ExtraScreenC4.Checked = True
                ExtraScreenC8.Checked = False
            Case 8
                ExtraScreenC1.Checked = False
                ExtraScreenC2.Checked = False
                ExtraScreenC4.Checked = False
                ExtraScreenC8.Checked = True
        End Select
    End Sub

    Private Sub FormClose(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        'Si se quiere cambiar la cantidad de pantallas lo hacemos
        Try
            If CurrentScreens <> WantedScreens Then
                If ScreenChange_Sender = 1 Then
                    Select Case CurrentScreens
                        Case 1
                            IdleALL()
                            If Container1_1.CloseButton1.Enabled = True Then
                                Container1_1.CloseButton1.PerformClick()
                            End If
                            Container1_1.Hide()
                        Case 2
                            IdleALL()
                            For i = 1 To 2
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container2_1.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container2_1.Hide()
                        Case 4
                            IdleALL()
                            For i = 1 To 4
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container4_1.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container4_1.Hide()
                        Case 8
                            IdleALL()
                            For i = 1 To 8
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container8_1.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container8_1.Hide()
                    End Select
                    ''VER QUE HACER CON MAXFRACS
                    My.Settings.currentScreens = WantedScreens
                    My.Settings.Save()
                    Principal.ContainerShow()
                ElseIf ScreenChange_Sender = 2 Then
                    Select Case CurrentScreens
                        Case 1
                            IdleALL()
                            If Container1_2.CloseButton1.Enabled = True Then
                                Container1_2.CloseButton1.PerformClick()
                            End If
                            Container1_2.Hide()
                        Case 2
                            IdleALL()
                            For i = 1 To 2
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container2_2.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container2_2.Hide()
                        Case 4
                            IdleALL()
                            For i = 1 To 4
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container4_2.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container4_2.Hide()
                        Case 8
                            IdleALL()
                            For i = 1 To 8
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container8_2.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container8_2.Hide()
                    End Select
                    ''VER QUE HACER CON MAXFRACS
                    My.Settings.currentScreens_2 = WantedScreens
                    My.Settings.Save()
                    Secundaria.ContainerShow()
                ElseIf ScreenChange_Sender = 3 Then
                    Select Case CurrentScreens
                        Case 1
                            IdleALL()
                            If Container1_3.CloseButton1.Enabled = True Then
                                Container1_3.CloseButton1.PerformClick()
                            End If
                            Container1_3.Hide()
                        Case 2
                            IdleALL()
                            For i = 1 To 2
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container2_3.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container2_3.Hide()
                        Case 4
                            IdleALL()
                            For i = 1 To 4
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container4_3.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container4_3.Hide()
                        Case 8
                            IdleALL()
                            For i = 1 To 8
                                Dim Btn As Guna.UI2.WinForms.Guna2ControlBox = TryCast(Container8_3.Controls.Find("CloseButton" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2ControlBox)
                                If Btn.Enabled = True Then
                                    Btn.PerformClick()
                                End If
                            Next
                            Container8_3.Hide()
                    End Select
                    ''VER QUE HACER CON MAXFRACS
                    My.Settings.currentScreens_3 = WantedScreens
                    My.Settings.Save()
                    Tercera.ContainerShow()
                End If
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Sub SecondScreenC1_Click(sender As Object, e As EventArgs) Handles ExtraScreenC1.Click
        WantedScreens = 1
        Close()
    End Sub

    Private Sub SecondScreenC2_Click(sender As Object, e As EventArgs) Handles ExtraScreenC2.Click
        WantedScreens = 2
        Close()
    End Sub

    Private Sub SecondScreenC4_Click(sender As Object, e As EventArgs) Handles ExtraScreenC4.Click
        WantedScreens = 4
        Close()
    End Sub

    Private Sub SecondScreenC8_Click(sender As Object, e As EventArgs) Handles ExtraScreenC8.Click
        WantedScreens = 8
        Close()
    End Sub
End Class