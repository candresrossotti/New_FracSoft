﻿Public Class SetGEAR
    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If Not String.IsNullOrEmpty(CbGear.Text) Then
            Dim marcha As Integer
            If FracData.StrVFrac("MarchaActual_" + CurrentFrac.ToString()) = "P" Then
                Close()
            End If

            If CbGear.Text = "N" Then
                marcha = FracGeneralValues("Marcha0")
            Else
                marcha = FracGeneralValues("Marcha" + CbGear.Text)
            End If

            ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, currentFrac)
        End If
        Close()
    End Sub

    Private Sub SetGEAR_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CbGear.Text = FracData.StrVFrac("MarchaActual_" + currentFrac.ToString())
        CbGear.Select()
        CenterEverything()
    End Sub

    Public Sub CenterEverything()
        CenterObject(Title)
        LeftAlign(LblTB, LblTB.Width + 6)
        LeftAlign(CbGear, 12 + LblTB.Width + CbGear.Width)
        CenterObject(BtnAceptar)
        GearPanel.Width = 12 + LblTB.Width + 6 + CbGear.Width + 6
        CenterObject(GearPanel)
    End Sub
End Class