﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SetGEAR
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.GearPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.CbGear = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.LblTB = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.GearPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(450, 38)
        Me.TitlePanel.TabIndex = 61
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(135, 3)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(181, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Ajustar Marcha"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(402, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.TitlePanel
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.Title
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'GearPanel
        '
        Me.GearPanel.Controls.Add(Me.CbGear)
        Me.GearPanel.Controls.Add(Me.BtnAceptar)
        Me.GearPanel.Controls.Add(Me.LblTB)
        Me.GearPanel.Location = New System.Drawing.Point(13, 44)
        Me.GearPanel.Name = "GearPanel"
        Me.GearPanel.Size = New System.Drawing.Size(424, 119)
        Me.GearPanel.TabIndex = 66
        '
        'CbGear
        '
        Me.CbGear.BackColor = System.Drawing.Color.Transparent
        Me.CbGear.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbGear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbGear.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbGear.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbGear.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbGear.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbGear.ItemHeight = 30
        Me.CbGear.Items.AddRange(New Object() {"N", "1", "2", "3", "4", "5", "6", "7"})
        Me.CbGear.Location = New System.Drawing.Point(212, 13)
        Me.CbGear.Name = "CbGear"
        Me.CbGear.Size = New System.Drawing.Size(200, 36)
        Me.CbGear.TabIndex = 68
        Me.CbGear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 14.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(152, 66)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(138, 40)
        Me.BtnAceptar.TabIndex = 67
        Me.BtnAceptar.Text = "Aceptar"
        '
        'LblTB
        '
        Me.LblTB.AutoSize = True
        Me.LblTB.BackColor = System.Drawing.Color.Transparent
        Me.LblTB.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTB.ForeColor = System.Drawing.Color.White
        Me.LblTB.Location = New System.Drawing.Point(13, 21)
        Me.LblTB.Name = "LblTB"
        Me.LblTB.Size = New System.Drawing.Size(187, 21)
        Me.LblTB.TabIndex = 66
        Me.LblTB.Text = "Ingresar nueva marcha:"
        '
        'SetGEAR
        '
        Me.AcceptButton = Me.BtnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(450, 175)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.GearPanel)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "SetGEAR"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SetGEAR"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.GearPanel.ResumeLayout(False)
        Me.GearPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Title As Label
    Friend WithEvents GearPanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents CbGear As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblTB As Label
End Class
