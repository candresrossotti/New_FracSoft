﻿Public Class QMInfo
    Private Sub QMInfo_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Centramos todo
        CenterEverything()

        LblApp.Text = "FracPump Control Software"
        LblVersion.Text = "Versión: 1.0.1"
        LblCompany.Text = "QM Equipment ®"
        LblLicense.Text = "Licencia: " + My.Settings.Licencia

    End Sub

    ''Sub para centrar las cosas
    Private Sub CenterEverything()
        CenterObject(Title)
    End Sub
End Class