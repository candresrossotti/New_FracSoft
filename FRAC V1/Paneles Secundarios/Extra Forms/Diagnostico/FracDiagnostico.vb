﻿Imports System.Threading
Public Class FracDiagnostico

    Public sError As Boolean = False
    Public sWarning As Boolean = False


    Public TargetMuestraAceite As Integer
    Public TargetRefrigerante As Integer
    Public TargetAceiteMotor As Integer
    Public TargetAceiteTransmision As Integer
    Public TargetFiltroCombustible As Integer
    Public TargetLuzValvulas As Integer
    Public TargetFiltroAire As Integer
    Public TargetLimpiarDucto As Integer
    Public TargetCambioCorrea As Integer
    Public TargetMangueraSilicona As Integer
    Public TargetFiltrosBlowBy As Integer

    Dim SetFormClose As Boolean = False


    Private Sub FracDiagnostico_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)


        ''Centramos todo
        CenterObject(Title)
        CenterObject(LblSensores)
        CenterObject(LblMotor)
        CenterObject(LblTransmision)
        CenterObject(LblTarea)

        ''Mostramos los progressbar
        For i = 1 To 4
            Dim ErrorPg As Guna.UI2.WinForms.Guna2WinProgressIndicator = TryCast(Controls.Find("ErrorProgress" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2WinProgressIndicator)
            ErrorPg.Start()
            ErrorPg.Visible = True
        Next

        TaskVisible(False)

        LblTarea.Visible = True
        LblSensores.Visible = True
        LblMotor.Visible = True
        LblTransmision.Visible = True

        LblUnidad.Text = ISystem.GetFracName(currentFrac)

        TmrUpdate.Start()
        TmrLoad.Start()
    End Sub

    ''Si tenemos una tarea pendiente, mostramos el signo de warning
    Private Sub TaskVisible(ByVal Visible As Boolean)
        For Each Pn As Guna.UI2.WinForms.Guna2Panel In Controls.OfType(Of Guna.UI2.WinForms.Guna2Panel)
            For Each Lb As Label In Pn.Controls.OfType(Of Label)
                Lb.Visible = Visible
            Next
            For Each Pb As Guna.UI2.WinForms.Guna2PictureBox In Pn.Controls.OfType(Of Guna.UI2.WinForms.Guna2PictureBox)
                Pb.Visible = Visible
            Next
        Next
        VSep.Visible = Visible
    End Sub

    Private Sub CloseForm(sender As Object, e As EventArgs) Handles Me.FormClosing
        SetFormClose = True
    End Sub

    ''Seccion para actualizar todo
#Region "Update"
    Delegate Sub UpdateDelegate()
    Private Sub TmrUpdate_Tick(sender As Object, e As EventArgs) Handles TmrUpdate.Tick
        Dim UpdateHandler As New Thread(AddressOf UpdateInvoke)
        UpdateHandler.Start()
    End Sub

    Private Sub UpdateInvoke()

        If SetFormClose = False Then
            Invoke(New UpdateDelegate(AddressOf UpdateHandle))
            UpdateFrac()
        End If
    End Sub

    Private Sub UpdateHandle()

        Dim Values As Integer() = {GetInteger(TargetMuestraAceite), GetInteger(TargetRefrigerante), GetInteger(TargetAceiteMotor), GetInteger(TargetAceiteTransmision),
            GetInteger(TargetFiltroCombustible), GetInteger(TargetLuzValvulas), GetInteger(TargetFiltroAire), GetInteger(TargetLimpiarDucto), GetInteger(TargetCambioCorrea),
            GetInteger(TargetMangueraSilicona), GetInteger(TargetFiltrosBlowBy)}

        LblMant.Text = Values.GetMin().ToString("F0")

        CheckFrac()

        LblMuestraAceite.Text = TargetMuestraAceite
        LblRefrigerante.Text = TargetRefrigerante
        LblAceiteMotor.Text = TargetAceiteMotor
        LblAceiteTransmision.Text = TargetAceiteTransmision
        LblFiltroCombustible.Text = TargetFiltroCombustible
        LblLuzValvulas.Text = TargetLuzValvulas
        LblFiltroAire.Text = TargetFiltroAire
        LblLimpiarDucto.Text = TargetLimpiarDucto
        LblCambioCorrea.Text = TargetCambioCorrea
        LblMangueraSilicona.Text = TargetMangueraSilicona
        LblFiltroBlowBy.Text = TargetFiltrosBlowBy
    End Sub

    Private Sub UpdateFrac()
        TargetMuestraAceite = FracData.DblVFrac("MantTargetMuestraAceite_" + currentFrac.ToString())
        TargetRefrigerante = FracData.DblVFrac("MantTargetRefrigerante_" + currentFrac.ToString())
        TargetAceiteMotor = FracData.DblVFrac("MantTargetAceiteMotor_" + currentFrac.ToString())
        TargetAceiteTransmision = FracData.DblVFrac("MantTargetAceiteTransmision_" + currentFrac.ToString())
        TargetFiltroCombustible = FracData.DblVFrac("MantTargetFiltroCombustible_" + currentFrac.ToString())
        TargetLuzValvulas = FracData.DblVFrac("MantTargetLuzValvulas_" + currentFrac.ToString())
        TargetFiltroAire = FracData.DblVFrac("MantTargetFiltroAire_" + currentFrac.ToString())
        TargetLimpiarDucto = FracData.DblVFrac("MantTargetLimpiarDucto_" + currentFrac.ToString())
        TargetCambioCorrea = FracData.DblVFrac("MantTargetCambioCorrea_" + currentFrac.ToString())
        TargetMangueraSilicona = FracData.DblVFrac("MantTargetMangueraSilicona_" + currentFrac.ToString())
        TargetFiltrosBlowBy = FracData.DblVFrac("MantTargetFiltrosBlowBy_" + currentFrac.ToString())
    End Sub
#End Region


    ''Checkeamos valores del frac
    Public Sub CheckFrac()

        sError = False
        sWarning = False

        If FracData.DblVFrac("Viatran_Alarm_Active_" + currentFrac.ToString()) = 1 Then
            PbSPresDescarga.Image = My.Resources._error
            sError = True
        ElseIf FracData.DblVFrac("Viatran_Alarm_Active_" + currentFrac.ToString()) = 2 Then
            PbSPresDescarga.Image = My.Resources.warning
            sWarning = True
        Else
            PbSPresDescarga.Image = My.Resources.check
        End If

        If FracData.DblVFrac("PresSuc_Alarm_Active_" + currentFrac.ToString()) = 1 Then
            PbSPresSuccion.Image = My.Resources._error
            sError = True
        ElseIf FracData.DblVFrac("PresSuc_Alarm_Active_" + currentFrac.ToString()) = 2 Then
            PbSPresSuccion.Image = My.Resources.warning
            sWarning = True
        Else
            PbSPresSuccion.Image = My.Resources.check
        End If

        If FracData.DblVFrac("TemperaturaLUB_LP_Alarm_Active_" + currentFrac.ToString()) = 1 Then
            PbSTempLub.Image = My.Resources._error
            sError = True
        ElseIf FracData.DblVFrac("TemperaturaLUB_LP_Alarm_Active_" + currentFrac.ToString()) = 2 Then
            PbSTempLub.Image = My.Resources.warning
            sWarning = True
        Else
            PbSTempLub.Image = My.Resources.check
        End If

        If FracData.DblVFrac("PresionLUB_LP_Alarm_Active_" + currentFrac.ToString()) = 1 Then
            PbSPresLUB.Image = My.Resources._error
            sError = True
        ElseIf FracData.DblVFrac("PresionLUB_LP_Alarm_Active_" + currentFrac.ToString()) = 2 Then
            PbSPresLUB.Image = My.Resources.warning
            sWarning = True
        Else
            PbSPresLUB.Image = My.Resources.check
        End If

        If FracData.DblVFrac("PresTrans_Alarm_Active_" + currentFrac.ToString()) = 1 Then
            PbTPresAceite.Image = My.Resources._error
            sError = True
        ElseIf FracData.DblVFrac("PresTrans_Alarm_Active_" + currentFrac.ToString()) = 2 Then
            PbTPresAceite.Image = My.Resources.warning
            sWarning = True
        Else
            PbTPresAceite.Image = My.Resources.check
        End If

        If FracData.DblVFrac("TemperaturaAceiteTransmision_" + currentFrac.ToString()) > 95 Then
            PbTTempAceite.Image = My.Resources.warning
            sWarning = True
        Else
            PbTTempAceite.Image = My.Resources.check
        End If


        If TargetMuestraAceite < 24 Then
            PbMuestraAceite.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetMuestraAceite <= 0 Then
            PbMuestraAceite.Image = My.Resources._error
            sError = True
        Else
            PbMuestraAceite.Image = My.Resources.check
        End If


        If TargetRefrigerante < 24 Then
            PbRefrigerante.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetRefrigerante <= 0 Then
            PbRefrigerante.Image = My.Resources._error
            sError = True
        Else
            PbRefrigerante.Image = My.Resources.check
        End If

        If TargetAceiteMotor < 24 Then
            PbAceiteMotor.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetAceiteMotor <= 0 Then
            PbAceiteMotor.Image = My.Resources._error
            sError = True
        Else
            PbAceiteMotor.Image = My.Resources.check
        End If


        If TargetAceiteTransmision < 24 Then
            PbAceiteTransmision.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetAceiteTransmision <= 0 Then
            PbAceiteTransmision.Image = My.Resources._error
            sError = True
        Else
            PbAceiteTransmision.Image = My.Resources.check
        End If

        If TargetFiltroCombustible < 24 Then
            PbFiltroCombustible.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetFiltroCombustible <= 0 Then
            PbFiltroCombustible.Image = My.Resources._error
            sError = True
        Else
            PbFiltroCombustible.Image = My.Resources.check
        End If

        If TargetLuzValvulas < 24 Then
            PbLuzValvulas.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetLuzValvulas <= 0 Then
            PbLuzValvulas.Image = My.Resources._error
            sError = True
        Else
            PbLuzValvulas.Image = My.Resources.check
        End If

        If TargetFiltroAire < 24 Then
            PbFiltroAire.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetFiltroAire <= 0 Then
            PbFiltroAire.Image = My.Resources._error
            sError = True
        Else
            PbFiltroAire.Image = My.Resources.check
        End If

        If TargetLimpiarDucto < 24 Then
            PbLimpiarDucto.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetLimpiarDucto <= 0 Then
            PbLimpiarDucto.Image = My.Resources._error
            sError = True
        Else
            PbLimpiarDucto.Image = My.Resources.check
        End If

        If TargetCambioCorrea < 24 Then
            PbCambioCorrea.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetCambioCorrea <= 0 Then
            PbCambioCorrea.Image = My.Resources._error
            sError = True
        Else
            PbCambioCorrea.Image = My.Resources.check
        End If

        If TargetMangueraSilicona < 24 Then
            PbMangueraSilicona.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetMangueraSilicona <= 0 Then
            PbMangueraSilicona.Image = My.Resources._error
            sError = True
        Else
            PbMangueraSilicona.Image = My.Resources.check
        End If

        If TargetFiltrosBlowBy < 24 Then
            PbFiltrosBlowBy.Image = My.Resources.warning
            sWarning = True
        ElseIf TargetFiltrosBlowBy <= 0 Then
            PbFiltrosBlowBy.Image = My.Resources._error
            sError = True
        Else
            PbFiltrosBlowBy.Image = My.Resources.check
        End If
    End Sub


    ''Mostramos mantenimiento si clickea para hacerlo
    Dim senderName As String
    Private Sub PbSPresDescarga_Click(sender As Object, e As EventArgs) Handles PbMuestraAceite.Click,
        PbRefrigerante.Click, PbAceiteMotor.Click, PbAceiteTransmision.Click, PbFiltroCombustible.Click,
        PbLuzValvulas.Click, PbFiltroAire.Click, PbLimpiarDucto.Click, PbCambioCorrea.Click, PbMangueraSilicona.Click,
        PbFiltrosBlowBy.Click

        senderName = Replace(sender.name, "Pb", Nothing)

        FracMantenimiento.ShowDialog(Me)

    End Sub

    ''Realizamos el mantenimiento
    Public Async Sub PerformMant()
        ISystem.MoxaSend(FracMoxaSendStartAddress("MantTarget" + senderName + "_StartAddress"), 1, currentFrac)
        Await Task.Delay(500)
        ISystem.MoxaSend(FracMoxaSendStartAddress("MantTarget" + senderName + "_StartAddress"), 0, currentFrac)
    End Sub

    ''Timer para load ProgressBar
    Private Sub TmrLoad_Tick(sender As Object, e As EventArgs) Handles TmrLoad.Tick
        For i = 1 To 4
            Dim ErrorPg As Guna.UI2.WinForms.Guna2WinProgressIndicator = TryCast(Controls.Find("ErrorProgress" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2WinProgressIndicator)
            ErrorPg.Stop()
            ErrorPg.Visible = False
        Next
        TaskVisible(True)
        TmrLoad.Stop()
    End Sub
End Class