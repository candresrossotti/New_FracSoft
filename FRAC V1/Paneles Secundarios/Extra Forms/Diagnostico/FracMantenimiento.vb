﻿Public Class FracMantenimiento
    Private Sub FracMantenimiento_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)


        ''ProgressBar = 0
        Pb1.Value = 0

        If sPassMem = True Then
            TbPassword.Text = adminPassword
            BtnAceptar.FillColor = Color.LimeGreen
        Else
            BtnAceptar.FillColor = Color.Maroon
            TbPassword.Text = Nothing
        End If
    End Sub

    Public Sub CenterEverything()
        CenterObject(Title)
        '  LeftAlign(LblTB, LblTB.Width + 34)
        'LeftAlign(TbPassword, 12 + LblTB.Width + TbPassword.Width)
        'RightAlign(CbPassMem, TbPassword)
        FMPanel.Width = 12 + LblTB.Width + 6 + TbPassword.Width + 6 + CbPassMem.Width + 6
        CenterObject(CbMant)
        CenterObject(BtnAceptar)
        CenterObject(FMPanel)
    End Sub

    Private Sub TbPassword_TextChanged(sender As Object, e As EventArgs) Handles TbPassword.TextChanged
        If Not String.IsNullOrEmpty(TbPassword.Text) Then
            If TbPassword.Text = adminPassword Then
                CbMant.Enabled = True
            Else
                CbMant.Enabled = False
            End If
        End If
    End Sub

    ''Aceptar, realizamos el mantenimiento
    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If CbPassMem.Checked = True Then
            sPassMem = True
        Else
            sPassMem = False
        End If

        If CbMant.Checked = True Then
            TmrPb.Start()
            Dim MantThread As New Task(AddressOf FracDiagnostico.PerformMant)
            MantThread.Start()
        End If

    End Sub

    ''Si da mantenimiento, mostramos esta barrita que hace literalmente nada solo para que crea que hace algo. Esperamos un toque para que 
    ''aparezcan los cambios en la otra pantalla
    Private Sub TmrPb_Tick(sender As Object, e As EventArgs) Handles TmrPb.Tick
        Pb1.Increment(1)
        If Pb1.Value = Pb1.Maximum Then
            TmrPb.Stop()
            Me.DialogResult = DialogResult.OK
        End If
    End Sub
End Class