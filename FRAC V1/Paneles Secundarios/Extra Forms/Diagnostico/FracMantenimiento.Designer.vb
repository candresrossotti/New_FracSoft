﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FracMantenimiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Pb1 = New Guna.UI2.WinForms.Guna2ProgressBar()
        Me.TmrPb = New System.Windows.Forms.Timer(Me.components)
        Me.FMPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.CbPassMem = New Guna.UI2.WinForms.Guna2CheckBox()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.LblTB = New System.Windows.Forms.Label()
        Me.TbPassword = New Guna.UI2.WinForms.Guna2TextBox()
        Me.CbMant = New Guna.UI2.WinForms.Guna2CheckBox()
        Me.TitlePanel.SuspendLayout()
        Me.FMPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(551, 38)
        Me.TitlePanel.TabIndex = 73
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(183, 3)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(185, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Mantenimiento"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(503, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.TitlePanel
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.Title
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'Pb1
        '
        Me.Pb1.FillColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Pb1.Location = New System.Drawing.Point(0, 221)
        Me.Pb1.Maximum = 300
        Me.Pb1.Name = "Pb1"
        Me.Pb1.Size = New System.Drawing.Size(551, 5)
        Me.Pb1.TabIndex = 78
        Me.Pb1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault
        '
        'TmrPb
        '
        Me.TmrPb.Interval = 1
        '
        'FMPanel
        '
        Me.FMPanel.Controls.Add(Me.CbPassMem)
        Me.FMPanel.Controls.Add(Me.BtnAceptar)
        Me.FMPanel.Controls.Add(Me.LblTB)
        Me.FMPanel.Controls.Add(Me.TbPassword)
        Me.FMPanel.Controls.Add(Me.CbMant)
        Me.FMPanel.Location = New System.Drawing.Point(4, 44)
        Me.FMPanel.Name = "FMPanel"
        Me.FMPanel.Size = New System.Drawing.Size(542, 170)
        Me.FMPanel.TabIndex = 79
        '
        'CbPassMem
        '
        Me.CbPassMem.AutoSize = True
        Me.CbPassMem.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.CbPassMem.CheckedState.BorderRadius = 0
        Me.CbPassMem.CheckedState.BorderThickness = 0
        Me.CbPassMem.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.CbPassMem.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbPassMem.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbPassMem.ForeColor = System.Drawing.Color.White
        Me.CbPassMem.Location = New System.Drawing.Point(237, 53)
        Me.CbPassMem.Name = "CbPassMem"
        Me.CbPassMem.Size = New System.Drawing.Size(173, 24)
        Me.CbPassMem.TabIndex = 89
        Me.CbPassMem.Text = "Recordar contraseña"
        Me.CbPassMem.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbPassMem.UncheckedState.BorderRadius = 0
        Me.CbPassMem.UncheckedState.BorderThickness = 0
        Me.CbPassMem.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 14.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(223, 123)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(111, 40)
        Me.BtnAceptar.TabIndex = 85
        Me.BtnAceptar.Text = "Aceptar"
        '
        'LblTB
        '
        Me.LblTB.AutoSize = True
        Me.LblTB.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTB.ForeColor = System.Drawing.Color.White
        Me.LblTB.Location = New System.Drawing.Point(34, 17)
        Me.LblTB.Name = "LblTB"
        Me.LblTB.Size = New System.Drawing.Size(183, 22)
        Me.LblTB.TabIndex = 88
        Me.LblTB.Text = "Ingrese la contraseña:"
        '
        'TbPassword
        '
        Me.TbPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbPassword.DefaultText = ""
        Me.TbPassword.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbPassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbPassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPassword.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPassword.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPassword.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbPassword.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPassword.Location = New System.Drawing.Point(223, 10)
        Me.TbPassword.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbPassword.Name = "TbPassword"
        Me.TbPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TbPassword.PlaceholderText = ""
        Me.TbPassword.SelectedText = ""
        Me.TbPassword.Size = New System.Drawing.Size(200, 36)
        Me.TbPassword.TabIndex = 87
        Me.TbPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TbPassword.UseSystemPasswordChar = True
        '
        'CbMant
        '
        Me.CbMant.AutoSize = True
        Me.CbMant.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.CbMant.CheckedState.BorderRadius = 0
        Me.CbMant.CheckedState.BorderThickness = 0
        Me.CbMant.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.CbMant.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbMant.Enabled = False
        Me.CbMant.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbMant.ForeColor = System.Drawing.Color.White
        Me.CbMant.Location = New System.Drawing.Point(107, 91)
        Me.CbMant.Name = "CbMant"
        Me.CbMant.Size = New System.Drawing.Size(354, 25)
        Me.CbMant.TabIndex = 86
        Me.CbMant.Text = "Se realizó correctamente el mantenimiento"
        Me.CbMant.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbMant.UncheckedState.BorderRadius = 0
        Me.CbMant.UncheckedState.BorderThickness = 0
        Me.CbMant.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'FracMantenimiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(551, 226)
        Me.Controls.Add(Me.Pb1)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.FMPanel)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FracMantenimiento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FracMantenimiento"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.FMPanel.ResumeLayout(False)
        Me.FMPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Title As Label
    Friend WithEvents Pb1 As Guna.UI2.WinForms.Guna2ProgressBar
    Friend WithEvents TmrPb As Timer
    Friend WithEvents FMPanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents CbPassMem As Guna.UI2.WinForms.Guna2CheckBox
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblTB As Label
    Friend WithEvents TbPassword As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents CbMant As Guna.UI2.WinForms.Guna2CheckBox
End Class
