﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FracDiagnostico
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FracDiagnostico))
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgress1 = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.LblSPresLub = New System.Windows.Forms.Label()
        Me.PbSPresLUB = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblSTempLub = New System.Windows.Forms.Label()
        Me.PbSTempLub = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblSPresSuccion = New System.Windows.Forms.Label()
        Me.PbSPresSuccion = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblSPresDescarga = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblSensores = New System.Windows.Forms.Label()
        Me.PbSPresDescarga = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgress2 = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.LblMHoras = New System.Windows.Forms.Label()
        Me.PbMHorasMotor = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblMCombustible = New System.Windows.Forms.Label()
        Me.PbMCombustible = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblMPresAceite = New System.Windows.Forms.Label()
        Me.PbMPresAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblMTempAgua = New System.Windows.Forms.Label()
        Me.PbMTempAgua = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblMTempAceite = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMotor = New System.Windows.Forms.Label()
        Me.PbMTempAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.Guna2Panel3 = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgress3 = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.LblTPresAceite = New System.Windows.Forms.Label()
        Me.LblTTempAceite = New System.Windows.Forms.Label()
        Me.PbTPresAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.Guna2CustomGradientPanel3 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTransmision = New System.Windows.Forms.Label()
        Me.PbTTempAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.TaskPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.ErrorProgress4 = New Guna.UI2.WinForms.Guna2WinProgressIndicator()
        Me.PbFiltrosBlowBy = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbMangueraSilicona = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbCambioCorrea = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbLimpiarDucto = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbFiltroAire = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbLuzValvulas = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbFiltroCombustible = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbAceiteTransmision = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbAceiteMotor = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbRefrigerante = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.PbMuestraAceite = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.LblLuzValvulas = New System.Windows.Forms.Label()
        Me.LblFiltroCombustible = New System.Windows.Forms.Label()
        Me.LblAceiteTransmision = New System.Windows.Forms.Label()
        Me.LblAceiteMotor = New System.Windows.Forms.Label()
        Me.LblRefrigerante = New System.Windows.Forms.Label()
        Me.LblMuestraAceite = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.LblFiltroBlowBy = New System.Windows.Forms.Label()
        Me.LblMangueraSilicona = New System.Windows.Forms.Label()
        Me.LblCambioCorrea = New System.Windows.Forms.Label()
        Me.LblLimpiarDucto = New System.Windows.Forms.Label()
        Me.LblFiltroAire = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.LblTareas11 = New System.Windows.Forms.Label()
        Me.LblTareas10 = New System.Windows.Forms.Label()
        Me.LblTareas9 = New System.Windows.Forms.Label()
        Me.LblTareas8 = New System.Windows.Forms.Label()
        Me.LblTareas7 = New System.Windows.Forms.Label()
        Me.LblTareas6 = New System.Windows.Forms.Label()
        Me.VSep = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.LblTareas5 = New System.Windows.Forms.Label()
        Me.LblTareas4 = New System.Windows.Forms.Label()
        Me.LblTareas3 = New System.Windows.Forms.Label()
        Me.LblTareas2 = New System.Windows.Forms.Label()
        Me.LblTareas1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTarea = New System.Windows.Forms.Label()
        Me.LblUnidadD = New System.Windows.Forms.Label()
        Me.LblMantD = New System.Windows.Forms.Label()
        Me.LblUnidad = New System.Windows.Forms.Label()
        Me.LblMant = New System.Windows.Forms.Label()
        Me.TmrUpdate = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.TmrLoad = New System.Windows.Forms.Timer(Me.components)
        Me.Guna2Panel1.SuspendLayout()
        CType(Me.PbSPresLUB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbSTempLub, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbSPresSuccion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        CType(Me.PbSPresDescarga, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TitlePanel.SuspendLayout()
        Me.Guna2Panel2.SuspendLayout()
        CType(Me.PbMHorasMotor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbMCombustible, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbMPresAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbMTempAgua, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        CType(Me.PbMTempAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2Panel3.SuspendLayout()
        CType(Me.PbTPresAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2CustomGradientPanel3.SuspendLayout()
        CType(Me.PbTTempAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TaskPanel.SuspendLayout()
        CType(Me.PbFiltrosBlowBy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbMangueraSilicona, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbCambioCorrea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbLimpiarDucto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbFiltroAire, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbLuzValvulas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbFiltroCombustible, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbAceiteTransmision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbAceiteMotor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbRefrigerante, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbMuestraAceite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.ErrorProgress1)
        Me.Guna2Panel1.Controls.Add(Me.LblSPresLub)
        Me.Guna2Panel1.Controls.Add(Me.PbSPresLUB)
        Me.Guna2Panel1.Controls.Add(Me.LblSTempLub)
        Me.Guna2Panel1.Controls.Add(Me.PbSTempLub)
        Me.Guna2Panel1.Controls.Add(Me.LblSPresSuccion)
        Me.Guna2Panel1.Controls.Add(Me.PbSPresSuccion)
        Me.Guna2Panel1.Controls.Add(Me.LblSPresDescarga)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.Controls.Add(Me.PbSPresDescarga)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(12, 44)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(290, 240)
        Me.Guna2Panel1.TabIndex = 71
        '
        'ErrorProgress1
        '
        Me.ErrorProgress1.AnimationSpeed = 50
        Me.ErrorProgress1.AutoStart = True
        Me.ErrorProgress1.Location = New System.Drawing.Point(100, 75)
        Me.ErrorProgress1.Name = "ErrorProgress1"
        Me.ErrorProgress1.Size = New System.Drawing.Size(90, 90)
        Me.ErrorProgress1.TabIndex = 112
        Me.ErrorProgress1.UseTransparentBackground = True
        '
        'LblSPresLub
        '
        Me.LblSPresLub.AutoSize = True
        Me.LblSPresLub.BackColor = System.Drawing.Color.Transparent
        Me.LblSPresLub.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSPresLub.ForeColor = System.Drawing.Color.White
        Me.LblSPresLub.Location = New System.Drawing.Point(77, 186)
        Me.LblSPresLub.Name = "LblSPresLub"
        Me.LblSPresLub.Size = New System.Drawing.Size(180, 21)
        Me.LblSPresLub.TabIndex = 67
        Me.LblSPresLub.Text = "Presión de Lubricación"
        '
        'PbSPresLUB
        '
        Me.PbSPresLUB.BackColor = System.Drawing.Color.Transparent
        Me.PbSPresLUB.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbSPresLUB.ImageRotate = 0!
        Me.PbSPresLUB.Location = New System.Drawing.Point(28, 186)
        Me.PbSPresLUB.Name = "PbSPresLUB"
        Me.PbSPresLUB.Size = New System.Drawing.Size(25, 25)
        Me.PbSPresLUB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbSPresLUB.TabIndex = 66
        Me.PbSPresLUB.TabStop = False
        Me.PbSPresLUB.UseTransparentBackground = True
        '
        'LblSTempLub
        '
        Me.LblSTempLub.AutoSize = True
        Me.LblSTempLub.BackColor = System.Drawing.Color.Transparent
        Me.LblSTempLub.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSTempLub.ForeColor = System.Drawing.Color.White
        Me.LblSTempLub.Location = New System.Drawing.Point(77, 138)
        Me.LblSTempLub.Name = "LblSTempLub"
        Me.LblSTempLub.Size = New System.Drawing.Size(220, 21)
        Me.LblSTempLub.TabIndex = 65
        Me.LblSTempLub.Text = "Temperatura de Lubricación"
        '
        'PbSTempLub
        '
        Me.PbSTempLub.BackColor = System.Drawing.Color.Transparent
        Me.PbSTempLub.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbSTempLub.ImageRotate = 0!
        Me.PbSTempLub.Location = New System.Drawing.Point(28, 138)
        Me.PbSTempLub.Name = "PbSTempLub"
        Me.PbSTempLub.Size = New System.Drawing.Size(25, 25)
        Me.PbSTempLub.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbSTempLub.TabIndex = 64
        Me.PbSTempLub.TabStop = False
        Me.PbSTempLub.UseTransparentBackground = True
        '
        'LblSPresSuccion
        '
        Me.LblSPresSuccion.AutoSize = True
        Me.LblSPresSuccion.BackColor = System.Drawing.Color.Transparent
        Me.LblSPresSuccion.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSPresSuccion.ForeColor = System.Drawing.Color.White
        Me.LblSPresSuccion.Location = New System.Drawing.Point(77, 90)
        Me.LblSPresSuccion.Name = "LblSPresSuccion"
        Me.LblSPresSuccion.Size = New System.Drawing.Size(151, 21)
        Me.LblSPresSuccion.TabIndex = 63
        Me.LblSPresSuccion.Text = "Presión de Succión"
        '
        'PbSPresSuccion
        '
        Me.PbSPresSuccion.BackColor = System.Drawing.Color.Transparent
        Me.PbSPresSuccion.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbSPresSuccion.ImageRotate = 0!
        Me.PbSPresSuccion.Location = New System.Drawing.Point(28, 90)
        Me.PbSPresSuccion.Name = "PbSPresSuccion"
        Me.PbSPresSuccion.Size = New System.Drawing.Size(25, 25)
        Me.PbSPresSuccion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbSPresSuccion.TabIndex = 62
        Me.PbSPresSuccion.TabStop = False
        Me.PbSPresSuccion.UseTransparentBackground = True
        '
        'LblSPresDescarga
        '
        Me.LblSPresDescarga.AutoSize = True
        Me.LblSPresDescarga.BackColor = System.Drawing.Color.Transparent
        Me.LblSPresDescarga.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSPresDescarga.ForeColor = System.Drawing.Color.White
        Me.LblSPresDescarga.Location = New System.Drawing.Point(77, 42)
        Me.LblSPresDescarga.Name = "LblSPresDescarga"
        Me.LblSPresDescarga.Size = New System.Drawing.Size(161, 21)
        Me.LblSPresDescarga.TabIndex = 61
        Me.LblSPresDescarga.Text = "Presión de descarga"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblSensores)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(290, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblSensores
        '
        Me.LblSensores.AutoSize = True
        Me.LblSensores.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSensores.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblSensores.Location = New System.Drawing.Point(107, 3)
        Me.LblSensores.Name = "LblSensores"
        Me.LblSensores.Size = New System.Drawing.Size(76, 21)
        Me.LblSensores.TabIndex = 19
        Me.LblSensores.Text = "Sensores"
        '
        'PbSPresDescarga
        '
        Me.PbSPresDescarga.BackColor = System.Drawing.Color.Transparent
        Me.PbSPresDescarga.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbSPresDescarga.ImageRotate = 0!
        Me.PbSPresDescarga.Location = New System.Drawing.Point(28, 42)
        Me.PbSPresDescarga.Name = "PbSPresDescarga"
        Me.PbSPresDescarga.Size = New System.Drawing.Size(25, 25)
        Me.PbSPresDescarga.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbSPresDescarga.TabIndex = 4
        Me.PbSPresDescarga.TabStop = False
        Me.PbSPresDescarga.UseTransparentBackground = True
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(900, 38)
        Me.TitlePanel.TabIndex = 72
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(377, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(146, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Diagnóstico"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(852, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2Panel2
        '
        Me.Guna2Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.BorderRadius = 6
        Me.Guna2Panel2.BorderThickness = 1
        Me.Guna2Panel2.Controls.Add(Me.ErrorProgress2)
        Me.Guna2Panel2.Controls.Add(Me.LblMHoras)
        Me.Guna2Panel2.Controls.Add(Me.PbMHorasMotor)
        Me.Guna2Panel2.Controls.Add(Me.LblMCombustible)
        Me.Guna2Panel2.Controls.Add(Me.PbMCombustible)
        Me.Guna2Panel2.Controls.Add(Me.LblMPresAceite)
        Me.Guna2Panel2.Controls.Add(Me.PbMPresAceite)
        Me.Guna2Panel2.Controls.Add(Me.LblMTempAgua)
        Me.Guna2Panel2.Controls.Add(Me.PbMTempAgua)
        Me.Guna2Panel2.Controls.Add(Me.LblMTempAceite)
        Me.Guna2Panel2.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.Guna2Panel2.Controls.Add(Me.PbMTempAceite)
        Me.Guna2Panel2.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.Location = New System.Drawing.Point(308, 44)
        Me.Guna2Panel2.Name = "Guna2Panel2"
        Me.Guna2Panel2.Size = New System.Drawing.Size(290, 240)
        Me.Guna2Panel2.TabIndex = 72
        '
        'ErrorProgress2
        '
        Me.ErrorProgress2.AnimationSpeed = 50
        Me.ErrorProgress2.AutoStart = True
        Me.ErrorProgress2.Location = New System.Drawing.Point(100, 75)
        Me.ErrorProgress2.Name = "ErrorProgress2"
        Me.ErrorProgress2.Size = New System.Drawing.Size(90, 90)
        Me.ErrorProgress2.TabIndex = 112
        Me.ErrorProgress2.UseTransparentBackground = True
        '
        'LblMHoras
        '
        Me.LblMHoras.AutoSize = True
        Me.LblMHoras.BackColor = System.Drawing.Color.Transparent
        Me.LblMHoras.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMHoras.ForeColor = System.Drawing.Color.White
        Me.LblMHoras.Location = New System.Drawing.Point(77, 198)
        Me.LblMHoras.Name = "LblMHoras"
        Me.LblMHoras.Size = New System.Drawing.Size(100, 21)
        Me.LblMHoras.TabIndex = 69
        Me.LblMHoras.Text = "Horas Motor"
        '
        'PbMHorasMotor
        '
        Me.PbMHorasMotor.BackColor = System.Drawing.Color.Transparent
        Me.PbMHorasMotor.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMHorasMotor.ImageRotate = 0!
        Me.PbMHorasMotor.Location = New System.Drawing.Point(28, 198)
        Me.PbMHorasMotor.Name = "PbMHorasMotor"
        Me.PbMHorasMotor.Size = New System.Drawing.Size(25, 25)
        Me.PbMHorasMotor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMHorasMotor.TabIndex = 68
        Me.PbMHorasMotor.TabStop = False
        Me.PbMHorasMotor.UseTransparentBackground = True
        '
        'LblMCombustible
        '
        Me.LblMCombustible.AutoSize = True
        Me.LblMCombustible.BackColor = System.Drawing.Color.Transparent
        Me.LblMCombustible.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMCombustible.ForeColor = System.Drawing.Color.White
        Me.LblMCombustible.Location = New System.Drawing.Point(77, 159)
        Me.LblMCombustible.Name = "LblMCombustible"
        Me.LblMCombustible.Size = New System.Drawing.Size(170, 21)
        Me.LblMCombustible.TabIndex = 67
        Me.LblMCombustible.Text = "Nivel de Combustible"
        '
        'PbMCombustible
        '
        Me.PbMCombustible.BackColor = System.Drawing.Color.Transparent
        Me.PbMCombustible.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMCombustible.ImageRotate = 0!
        Me.PbMCombustible.Location = New System.Drawing.Point(28, 159)
        Me.PbMCombustible.Name = "PbMCombustible"
        Me.PbMCombustible.Size = New System.Drawing.Size(25, 25)
        Me.PbMCombustible.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMCombustible.TabIndex = 66
        Me.PbMCombustible.TabStop = False
        Me.PbMCombustible.UseTransparentBackground = True
        '
        'LblMPresAceite
        '
        Me.LblMPresAceite.AutoSize = True
        Me.LblMPresAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblMPresAceite.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMPresAceite.ForeColor = System.Drawing.Color.White
        Me.LblMPresAceite.Location = New System.Drawing.Point(77, 120)
        Me.LblMPresAceite.Name = "LblMPresAceite"
        Me.LblMPresAceite.Size = New System.Drawing.Size(140, 21)
        Me.LblMPresAceite.TabIndex = 65
        Me.LblMPresAceite.Text = "Presión de Aceite"
        '
        'PbMPresAceite
        '
        Me.PbMPresAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbMPresAceite.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMPresAceite.ImageRotate = 0!
        Me.PbMPresAceite.Location = New System.Drawing.Point(28, 120)
        Me.PbMPresAceite.Name = "PbMPresAceite"
        Me.PbMPresAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbMPresAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMPresAceite.TabIndex = 64
        Me.PbMPresAceite.TabStop = False
        Me.PbMPresAceite.UseTransparentBackground = True
        '
        'LblMTempAgua
        '
        Me.LblMTempAgua.AutoSize = True
        Me.LblMTempAgua.BackColor = System.Drawing.Color.Transparent
        Me.LblMTempAgua.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblMTempAgua.Location = New System.Drawing.Point(77, 81)
        Me.LblMTempAgua.Name = "LblMTempAgua"
        Me.LblMTempAgua.Size = New System.Drawing.Size(173, 21)
        Me.LblMTempAgua.TabIndex = 63
        Me.LblMTempAgua.Text = "Temperatura de Agua"
        '
        'PbMTempAgua
        '
        Me.PbMTempAgua.BackColor = System.Drawing.Color.Transparent
        Me.PbMTempAgua.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMTempAgua.ImageRotate = 0!
        Me.PbMTempAgua.Location = New System.Drawing.Point(28, 81)
        Me.PbMTempAgua.Name = "PbMTempAgua"
        Me.PbMTempAgua.Size = New System.Drawing.Size(25, 25)
        Me.PbMTempAgua.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMTempAgua.TabIndex = 62
        Me.PbMTempAgua.TabStop = False
        Me.PbMTempAgua.UseTransparentBackground = True
        '
        'LblMTempAceite
        '
        Me.LblMTempAceite.AutoSize = True
        Me.LblMTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblMTempAceite.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblMTempAceite.Location = New System.Drawing.Point(77, 42)
        Me.LblMTempAceite.Name = "LblMTempAceite"
        Me.LblMTempAceite.Size = New System.Drawing.Size(180, 21)
        Me.LblMTempAceite.TabIndex = 61
        Me.LblMTempAceite.Text = "Temperatura de Aceite"
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblMotor)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(290, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblMotor
        '
        Me.LblMotor.AutoSize = True
        Me.LblMotor.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMotor.Location = New System.Drawing.Point(118, 3)
        Me.LblMotor.Name = "LblMotor"
        Me.LblMotor.Size = New System.Drawing.Size(54, 21)
        Me.LblMotor.TabIndex = 19
        Me.LblMotor.Text = "Motor"
        '
        'PbMTempAceite
        '
        Me.PbMTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbMTempAceite.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMTempAceite.ImageRotate = 0!
        Me.PbMTempAceite.Location = New System.Drawing.Point(28, 42)
        Me.PbMTempAceite.Name = "PbMTempAceite"
        Me.PbMTempAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbMTempAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMTempAceite.TabIndex = 4
        Me.PbMTempAceite.TabStop = False
        Me.PbMTempAceite.UseTransparentBackground = True
        '
        'Guna2Panel3
        '
        Me.Guna2Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel3.BorderRadius = 6
        Me.Guna2Panel3.BorderThickness = 1
        Me.Guna2Panel3.Controls.Add(Me.ErrorProgress3)
        Me.Guna2Panel3.Controls.Add(Me.LblTPresAceite)
        Me.Guna2Panel3.Controls.Add(Me.LblTTempAceite)
        Me.Guna2Panel3.Controls.Add(Me.PbTPresAceite)
        Me.Guna2Panel3.Controls.Add(Me.Guna2CustomGradientPanel3)
        Me.Guna2Panel3.Controls.Add(Me.PbTTempAceite)
        Me.Guna2Panel3.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel3.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel3.Location = New System.Drawing.Point(604, 44)
        Me.Guna2Panel3.Name = "Guna2Panel3"
        Me.Guna2Panel3.Size = New System.Drawing.Size(290, 240)
        Me.Guna2Panel3.TabIndex = 72
        '
        'ErrorProgress3
        '
        Me.ErrorProgress3.AnimationSpeed = 50
        Me.ErrorProgress3.AutoStart = True
        Me.ErrorProgress3.Location = New System.Drawing.Point(100, 75)
        Me.ErrorProgress3.Name = "ErrorProgress3"
        Me.ErrorProgress3.Size = New System.Drawing.Size(90, 90)
        Me.ErrorProgress3.TabIndex = 112
        Me.ErrorProgress3.UseTransparentBackground = True
        '
        'LblTPresAceite
        '
        Me.LblTPresAceite.AutoSize = True
        Me.LblTPresAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblTPresAceite.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTPresAceite.ForeColor = System.Drawing.Color.White
        Me.LblTPresAceite.Location = New System.Drawing.Point(67, 150)
        Me.LblTPresAceite.Name = "LblTPresAceite"
        Me.LblTPresAceite.Size = New System.Drawing.Size(140, 21)
        Me.LblTPresAceite.TabIndex = 71
        Me.LblTPresAceite.Text = "Presión de Aceite"
        '
        'LblTTempAceite
        '
        Me.LblTTempAceite.AutoSize = True
        Me.LblTTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblTTempAceite.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblTTempAceite.Location = New System.Drawing.Point(67, 79)
        Me.LblTTempAceite.Name = "LblTTempAceite"
        Me.LblTTempAceite.Size = New System.Drawing.Size(180, 21)
        Me.LblTTempAceite.TabIndex = 71
        Me.LblTTempAceite.Text = "Temperatura de Aceite"
        '
        'PbTPresAceite
        '
        Me.PbTPresAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbTPresAceite.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbTPresAceite.ImageRotate = 0!
        Me.PbTPresAceite.Location = New System.Drawing.Point(18, 150)
        Me.PbTPresAceite.Name = "PbTPresAceite"
        Me.PbTPresAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbTPresAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbTPresAceite.TabIndex = 70
        Me.PbTPresAceite.TabStop = False
        Me.PbTPresAceite.UseTransparentBackground = True
        '
        'Guna2CustomGradientPanel3
        '
        Me.Guna2CustomGradientPanel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.BorderRadius = 6
        Me.Guna2CustomGradientPanel3.Controls.Add(Me.LblTransmision)
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel3.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel3.Name = "Guna2CustomGradientPanel3"
        Me.Guna2CustomGradientPanel3.Size = New System.Drawing.Size(290, 26)
        Me.Guna2CustomGradientPanel3.TabIndex = 19
        '
        'LblTransmision
        '
        Me.LblTransmision.AutoSize = True
        Me.LblTransmision.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTransmision.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTransmision.Location = New System.Drawing.Point(95, 3)
        Me.LblTransmision.Name = "LblTransmision"
        Me.LblTransmision.Size = New System.Drawing.Size(100, 21)
        Me.LblTransmision.TabIndex = 19
        Me.LblTransmision.Text = "Transmisión"
        '
        'PbTTempAceite
        '
        Me.PbTTempAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbTTempAceite.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbTTempAceite.ImageRotate = 0!
        Me.PbTTempAceite.Location = New System.Drawing.Point(18, 79)
        Me.PbTTempAceite.Name = "PbTTempAceite"
        Me.PbTTempAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbTTempAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbTTempAceite.TabIndex = 70
        Me.PbTTempAceite.TabStop = False
        Me.PbTTempAceite.UseTransparentBackground = True
        '
        'TaskPanel
        '
        Me.TaskPanel.BackColor = System.Drawing.Color.Transparent
        Me.TaskPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.TaskPanel.BorderRadius = 6
        Me.TaskPanel.BorderThickness = 1
        Me.TaskPanel.Controls.Add(Me.ErrorProgress4)
        Me.TaskPanel.Controls.Add(Me.PbFiltrosBlowBy)
        Me.TaskPanel.Controls.Add(Me.PbMangueraSilicona)
        Me.TaskPanel.Controls.Add(Me.PbCambioCorrea)
        Me.TaskPanel.Controls.Add(Me.PbLimpiarDucto)
        Me.TaskPanel.Controls.Add(Me.PbFiltroAire)
        Me.TaskPanel.Controls.Add(Me.PbLuzValvulas)
        Me.TaskPanel.Controls.Add(Me.PbFiltroCombustible)
        Me.TaskPanel.Controls.Add(Me.PbAceiteTransmision)
        Me.TaskPanel.Controls.Add(Me.PbAceiteMotor)
        Me.TaskPanel.Controls.Add(Me.PbRefrigerante)
        Me.TaskPanel.Controls.Add(Me.PbMuestraAceite)
        Me.TaskPanel.Controls.Add(Me.LblLuzValvulas)
        Me.TaskPanel.Controls.Add(Me.LblFiltroCombustible)
        Me.TaskPanel.Controls.Add(Me.LblAceiteTransmision)
        Me.TaskPanel.Controls.Add(Me.LblAceiteMotor)
        Me.TaskPanel.Controls.Add(Me.LblRefrigerante)
        Me.TaskPanel.Controls.Add(Me.LblMuestraAceite)
        Me.TaskPanel.Controls.Add(Me.Label23)
        Me.TaskPanel.Controls.Add(Me.Label24)
        Me.TaskPanel.Controls.Add(Me.Label21)
        Me.TaskPanel.Controls.Add(Me.Label22)
        Me.TaskPanel.Controls.Add(Me.Label20)
        Me.TaskPanel.Controls.Add(Me.Label19)
        Me.TaskPanel.Controls.Add(Me.LblFiltroBlowBy)
        Me.TaskPanel.Controls.Add(Me.LblMangueraSilicona)
        Me.TaskPanel.Controls.Add(Me.LblCambioCorrea)
        Me.TaskPanel.Controls.Add(Me.LblLimpiarDucto)
        Me.TaskPanel.Controls.Add(Me.LblFiltroAire)
        Me.TaskPanel.Controls.Add(Me.Label38)
        Me.TaskPanel.Controls.Add(Me.Label39)
        Me.TaskPanel.Controls.Add(Me.Label40)
        Me.TaskPanel.Controls.Add(Me.Label41)
        Me.TaskPanel.Controls.Add(Me.Label42)
        Me.TaskPanel.Controls.Add(Me.LblTareas11)
        Me.TaskPanel.Controls.Add(Me.LblTareas10)
        Me.TaskPanel.Controls.Add(Me.LblTareas9)
        Me.TaskPanel.Controls.Add(Me.LblTareas8)
        Me.TaskPanel.Controls.Add(Me.LblTareas7)
        Me.TaskPanel.Controls.Add(Me.LblTareas6)
        Me.TaskPanel.Controls.Add(Me.VSep)
        Me.TaskPanel.Controls.Add(Me.LblTareas5)
        Me.TaskPanel.Controls.Add(Me.LblTareas4)
        Me.TaskPanel.Controls.Add(Me.LblTareas3)
        Me.TaskPanel.Controls.Add(Me.LblTareas2)
        Me.TaskPanel.Controls.Add(Me.LblTareas1)
        Me.TaskPanel.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.TaskPanel.Cursor = System.Windows.Forms.Cursors.Default
        Me.TaskPanel.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.TaskPanel.FillColor = System.Drawing.Color.Transparent
        Me.TaskPanel.Location = New System.Drawing.Point(12, 290)
        Me.TaskPanel.Name = "TaskPanel"
        Me.TaskPanel.Size = New System.Drawing.Size(882, 267)
        Me.TaskPanel.TabIndex = 73
        '
        'ErrorProgress4
        '
        Me.ErrorProgress4.AnimationSpeed = 50
        Me.ErrorProgress4.AutoStart = True
        Me.ErrorProgress4.Location = New System.Drawing.Point(392, 102)
        Me.ErrorProgress4.Name = "ErrorProgress4"
        Me.ErrorProgress4.Size = New System.Drawing.Size(90, 90)
        Me.ErrorProgress4.TabIndex = 111
        Me.ErrorProgress4.UseTransparentBackground = True
        '
        'PbFiltrosBlowBy
        '
        Me.PbFiltrosBlowBy.BackColor = System.Drawing.Color.Transparent
        Me.PbFiltrosBlowBy.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbFiltrosBlowBy.Image = CType(resources.GetObject("PbFiltrosBlowBy.Image"), System.Drawing.Image)
        Me.PbFiltrosBlowBy.ImageRotate = 0!
        Me.PbFiltrosBlowBy.Location = New System.Drawing.Point(766, 224)
        Me.PbFiltrosBlowBy.Name = "PbFiltrosBlowBy"
        Me.PbFiltrosBlowBy.Size = New System.Drawing.Size(25, 25)
        Me.PbFiltrosBlowBy.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbFiltrosBlowBy.TabIndex = 110
        Me.PbFiltrosBlowBy.TabStop = False
        Me.PbFiltrosBlowBy.UseTransparentBackground = True
        '
        'PbMangueraSilicona
        '
        Me.PbMangueraSilicona.BackColor = System.Drawing.Color.Transparent
        Me.PbMangueraSilicona.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbMangueraSilicona.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbMangueraSilicona.ImageRotate = 0!
        Me.PbMangueraSilicona.Location = New System.Drawing.Point(766, 179)
        Me.PbMangueraSilicona.Name = "PbMangueraSilicona"
        Me.PbMangueraSilicona.Size = New System.Drawing.Size(25, 25)
        Me.PbMangueraSilicona.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMangueraSilicona.TabIndex = 109
        Me.PbMangueraSilicona.TabStop = False
        Me.PbMangueraSilicona.UseTransparentBackground = True
        '
        'PbCambioCorrea
        '
        Me.PbCambioCorrea.BackColor = System.Drawing.Color.Transparent
        Me.PbCambioCorrea.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbCambioCorrea.Image = CType(resources.GetObject("PbCambioCorrea.Image"), System.Drawing.Image)
        Me.PbCambioCorrea.ImageRotate = 0!
        Me.PbCambioCorrea.Location = New System.Drawing.Point(766, 133)
        Me.PbCambioCorrea.Name = "PbCambioCorrea"
        Me.PbCambioCorrea.Size = New System.Drawing.Size(25, 25)
        Me.PbCambioCorrea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbCambioCorrea.TabIndex = 108
        Me.PbCambioCorrea.TabStop = False
        Me.PbCambioCorrea.UseTransparentBackground = True
        '
        'PbLimpiarDucto
        '
        Me.PbLimpiarDucto.BackColor = System.Drawing.Color.Transparent
        Me.PbLimpiarDucto.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbLimpiarDucto.Image = CType(resources.GetObject("PbLimpiarDucto.Image"), System.Drawing.Image)
        Me.PbLimpiarDucto.ImageRotate = 0!
        Me.PbLimpiarDucto.Location = New System.Drawing.Point(766, 87)
        Me.PbLimpiarDucto.Name = "PbLimpiarDucto"
        Me.PbLimpiarDucto.Size = New System.Drawing.Size(25, 25)
        Me.PbLimpiarDucto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbLimpiarDucto.TabIndex = 107
        Me.PbLimpiarDucto.TabStop = False
        Me.PbLimpiarDucto.UseTransparentBackground = True
        '
        'PbFiltroAire
        '
        Me.PbFiltroAire.BackColor = System.Drawing.Color.Transparent
        Me.PbFiltroAire.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbFiltroAire.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbFiltroAire.ImageRotate = 0!
        Me.PbFiltroAire.Location = New System.Drawing.Point(766, 41)
        Me.PbFiltroAire.Name = "PbFiltroAire"
        Me.PbFiltroAire.Size = New System.Drawing.Size(25, 25)
        Me.PbFiltroAire.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbFiltroAire.TabIndex = 106
        Me.PbFiltroAire.TabStop = False
        Me.PbFiltroAire.UseTransparentBackground = True
        '
        'PbLuzValvulas
        '
        Me.PbLuzValvulas.BackColor = System.Drawing.Color.Transparent
        Me.PbLuzValvulas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbLuzValvulas.Image = CType(resources.GetObject("PbLuzValvulas.Image"), System.Drawing.Image)
        Me.PbLuzValvulas.ImageRotate = 0!
        Me.PbLuzValvulas.Location = New System.Drawing.Point(316, 231)
        Me.PbLuzValvulas.Name = "PbLuzValvulas"
        Me.PbLuzValvulas.Size = New System.Drawing.Size(25, 25)
        Me.PbLuzValvulas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbLuzValvulas.TabIndex = 105
        Me.PbLuzValvulas.TabStop = False
        Me.PbLuzValvulas.UseTransparentBackground = True
        '
        'PbFiltroCombustible
        '
        Me.PbFiltroCombustible.BackColor = System.Drawing.Color.Transparent
        Me.PbFiltroCombustible.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbFiltroCombustible.Image = Global.sFRAC.My.Resources.Resources.check
        Me.PbFiltroCombustible.ImageRotate = 0!
        Me.PbFiltroCombustible.Location = New System.Drawing.Point(316, 193)
        Me.PbFiltroCombustible.Name = "PbFiltroCombustible"
        Me.PbFiltroCombustible.Size = New System.Drawing.Size(25, 25)
        Me.PbFiltroCombustible.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbFiltroCombustible.TabIndex = 104
        Me.PbFiltroCombustible.TabStop = False
        Me.PbFiltroCombustible.UseTransparentBackground = True
        '
        'PbAceiteTransmision
        '
        Me.PbAceiteTransmision.BackColor = System.Drawing.Color.Transparent
        Me.PbAceiteTransmision.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAceiteTransmision.Image = CType(resources.GetObject("PbAceiteTransmision.Image"), System.Drawing.Image)
        Me.PbAceiteTransmision.ImageRotate = 0!
        Me.PbAceiteTransmision.Location = New System.Drawing.Point(316, 155)
        Me.PbAceiteTransmision.Name = "PbAceiteTransmision"
        Me.PbAceiteTransmision.Size = New System.Drawing.Size(25, 25)
        Me.PbAceiteTransmision.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAceiteTransmision.TabIndex = 103
        Me.PbAceiteTransmision.TabStop = False
        Me.PbAceiteTransmision.UseTransparentBackground = True
        '
        'PbAceiteMotor
        '
        Me.PbAceiteMotor.BackColor = System.Drawing.Color.Transparent
        Me.PbAceiteMotor.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAceiteMotor.Image = CType(resources.GetObject("PbAceiteMotor.Image"), System.Drawing.Image)
        Me.PbAceiteMotor.ImageRotate = 0!
        Me.PbAceiteMotor.Location = New System.Drawing.Point(316, 117)
        Me.PbAceiteMotor.Name = "PbAceiteMotor"
        Me.PbAceiteMotor.Size = New System.Drawing.Size(25, 25)
        Me.PbAceiteMotor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAceiteMotor.TabIndex = 102
        Me.PbAceiteMotor.TabStop = False
        Me.PbAceiteMotor.UseTransparentBackground = True
        '
        'PbRefrigerante
        '
        Me.PbRefrigerante.BackColor = System.Drawing.Color.Transparent
        Me.PbRefrigerante.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbRefrigerante.Image = CType(resources.GetObject("PbRefrigerante.Image"), System.Drawing.Image)
        Me.PbRefrigerante.ImageRotate = 0!
        Me.PbRefrigerante.Location = New System.Drawing.Point(316, 79)
        Me.PbRefrigerante.Name = "PbRefrigerante"
        Me.PbRefrigerante.Size = New System.Drawing.Size(25, 25)
        Me.PbRefrigerante.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbRefrigerante.TabIndex = 101
        Me.PbRefrigerante.TabStop = False
        Me.PbRefrigerante.UseTransparentBackground = True
        '
        'PbMuestraAceite
        '
        Me.PbMuestraAceite.BackColor = System.Drawing.Color.Transparent
        Me.PbMuestraAceite.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbMuestraAceite.Image = Global.sFRAC.My.Resources.Resources.warning
        Me.PbMuestraAceite.ImageRotate = 0!
        Me.PbMuestraAceite.Location = New System.Drawing.Point(316, 41)
        Me.PbMuestraAceite.Name = "PbMuestraAceite"
        Me.PbMuestraAceite.Size = New System.Drawing.Size(25, 25)
        Me.PbMuestraAceite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbMuestraAceite.TabIndex = 70
        Me.PbMuestraAceite.TabStop = False
        Me.PbMuestraAceite.UseTransparentBackground = True
        '
        'LblLuzValvulas
        '
        Me.LblLuzValvulas.AutoSize = True
        Me.LblLuzValvulas.BackColor = System.Drawing.Color.Transparent
        Me.LblLuzValvulas.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblLuzValvulas.ForeColor = System.Drawing.Color.White
        Me.LblLuzValvulas.Location = New System.Drawing.Point(347, 233)
        Me.LblLuzValvulas.Name = "LblLuzValvulas"
        Me.LblLuzValvulas.Size = New System.Drawing.Size(35, 21)
        Me.LblLuzValvulas.TabIndex = 83
        Me.LblLuzValvulas.Text = "100"
        '
        'LblFiltroCombustible
        '
        Me.LblFiltroCombustible.AutoSize = True
        Me.LblFiltroCombustible.BackColor = System.Drawing.Color.Transparent
        Me.LblFiltroCombustible.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblFiltroCombustible.ForeColor = System.Drawing.Color.White
        Me.LblFiltroCombustible.Location = New System.Drawing.Point(347, 195)
        Me.LblFiltroCombustible.Name = "LblFiltroCombustible"
        Me.LblFiltroCombustible.Size = New System.Drawing.Size(35, 21)
        Me.LblFiltroCombustible.TabIndex = 82
        Me.LblFiltroCombustible.Text = "100"
        '
        'LblAceiteTransmision
        '
        Me.LblAceiteTransmision.AutoSize = True
        Me.LblAceiteTransmision.BackColor = System.Drawing.Color.Transparent
        Me.LblAceiteTransmision.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblAceiteTransmision.ForeColor = System.Drawing.Color.White
        Me.LblAceiteTransmision.Location = New System.Drawing.Point(347, 157)
        Me.LblAceiteTransmision.Name = "LblAceiteTransmision"
        Me.LblAceiteTransmision.Size = New System.Drawing.Size(35, 21)
        Me.LblAceiteTransmision.TabIndex = 81
        Me.LblAceiteTransmision.Text = "100"
        '
        'LblAceiteMotor
        '
        Me.LblAceiteMotor.AutoSize = True
        Me.LblAceiteMotor.BackColor = System.Drawing.Color.Transparent
        Me.LblAceiteMotor.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblAceiteMotor.ForeColor = System.Drawing.Color.White
        Me.LblAceiteMotor.Location = New System.Drawing.Point(347, 119)
        Me.LblAceiteMotor.Name = "LblAceiteMotor"
        Me.LblAceiteMotor.Size = New System.Drawing.Size(35, 21)
        Me.LblAceiteMotor.TabIndex = 80
        Me.LblAceiteMotor.Text = "100"
        '
        'LblRefrigerante
        '
        Me.LblRefrigerante.AutoSize = True
        Me.LblRefrigerante.BackColor = System.Drawing.Color.Transparent
        Me.LblRefrigerante.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblRefrigerante.ForeColor = System.Drawing.Color.White
        Me.LblRefrigerante.Location = New System.Drawing.Point(347, 81)
        Me.LblRefrigerante.Name = "LblRefrigerante"
        Me.LblRefrigerante.Size = New System.Drawing.Size(35, 21)
        Me.LblRefrigerante.TabIndex = 79
        Me.LblRefrigerante.Text = "100"
        '
        'LblMuestraAceite
        '
        Me.LblMuestraAceite.AutoSize = True
        Me.LblMuestraAceite.BackColor = System.Drawing.Color.Transparent
        Me.LblMuestraAceite.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMuestraAceite.ForeColor = System.Drawing.Color.White
        Me.LblMuestraAceite.Location = New System.Drawing.Point(347, 43)
        Me.LblMuestraAceite.Name = "LblMuestraAceite"
        Me.LblMuestraAceite.Size = New System.Drawing.Size(35, 21)
        Me.LblMuestraAceite.TabIndex = 78
        Me.LblMuestraAceite.Text = "100"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(390, 235)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(30, 16)
        Me.Label23.TabIndex = 77
        Me.Label23.Text = "[Hr]"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(390, 197)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(30, 16)
        Me.Label24.TabIndex = 76
        Me.Label24.Text = "[Hr]"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(390, 159)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(30, 16)
        Me.Label21.TabIndex = 75
        Me.Label21.Text = "[Hr]"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(390, 121)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(30, 16)
        Me.Label22.TabIndex = 74
        Me.Label22.Text = "[Hr]"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(390, 83)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(30, 16)
        Me.Label20.TabIndex = 73
        Me.Label20.Text = "[Hr]"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(390, 45)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(30, 16)
        Me.Label19.TabIndex = 72
        Me.Label19.Text = "[Hr]"
        '
        'LblFiltroBlowBy
        '
        Me.LblFiltroBlowBy.AutoSize = True
        Me.LblFiltroBlowBy.BackColor = System.Drawing.Color.Transparent
        Me.LblFiltroBlowBy.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblFiltroBlowBy.ForeColor = System.Drawing.Color.White
        Me.LblFiltroBlowBy.Location = New System.Drawing.Point(797, 226)
        Me.LblFiltroBlowBy.Name = "LblFiltroBlowBy"
        Me.LblFiltroBlowBy.Size = New System.Drawing.Size(35, 21)
        Me.LblFiltroBlowBy.TabIndex = 100
        Me.LblFiltroBlowBy.Text = "100"
        '
        'LblMangueraSilicona
        '
        Me.LblMangueraSilicona.AutoSize = True
        Me.LblMangueraSilicona.BackColor = System.Drawing.Color.Transparent
        Me.LblMangueraSilicona.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMangueraSilicona.ForeColor = System.Drawing.Color.White
        Me.LblMangueraSilicona.Location = New System.Drawing.Point(797, 180)
        Me.LblMangueraSilicona.Name = "LblMangueraSilicona"
        Me.LblMangueraSilicona.Size = New System.Drawing.Size(35, 21)
        Me.LblMangueraSilicona.TabIndex = 99
        Me.LblMangueraSilicona.Text = "100"
        '
        'LblCambioCorrea
        '
        Me.LblCambioCorrea.AutoSize = True
        Me.LblCambioCorrea.BackColor = System.Drawing.Color.Transparent
        Me.LblCambioCorrea.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblCambioCorrea.ForeColor = System.Drawing.Color.White
        Me.LblCambioCorrea.Location = New System.Drawing.Point(797, 135)
        Me.LblCambioCorrea.Name = "LblCambioCorrea"
        Me.LblCambioCorrea.Size = New System.Drawing.Size(35, 21)
        Me.LblCambioCorrea.TabIndex = 98
        Me.LblCambioCorrea.Text = "100"
        '
        'LblLimpiarDucto
        '
        Me.LblLimpiarDucto.AutoSize = True
        Me.LblLimpiarDucto.BackColor = System.Drawing.Color.Transparent
        Me.LblLimpiarDucto.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblLimpiarDucto.ForeColor = System.Drawing.Color.White
        Me.LblLimpiarDucto.Location = New System.Drawing.Point(797, 89)
        Me.LblLimpiarDucto.Name = "LblLimpiarDucto"
        Me.LblLimpiarDucto.Size = New System.Drawing.Size(35, 21)
        Me.LblLimpiarDucto.TabIndex = 97
        Me.LblLimpiarDucto.Text = "100"
        '
        'LblFiltroAire
        '
        Me.LblFiltroAire.AutoSize = True
        Me.LblFiltroAire.BackColor = System.Drawing.Color.Transparent
        Me.LblFiltroAire.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblFiltroAire.ForeColor = System.Drawing.Color.White
        Me.LblFiltroAire.Location = New System.Drawing.Point(797, 43)
        Me.LblFiltroAire.Name = "LblFiltroAire"
        Me.LblFiltroAire.Size = New System.Drawing.Size(35, 21)
        Me.LblFiltroAire.TabIndex = 96
        Me.LblFiltroAire.Text = "100"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.BackColor = System.Drawing.Color.Transparent
        Me.Label38.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(840, 228)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(30, 16)
        Me.Label38.TabIndex = 94
        Me.Label38.Text = "[Hr]"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.BackColor = System.Drawing.Color.Transparent
        Me.Label39.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label39.ForeColor = System.Drawing.Color.White
        Me.Label39.Location = New System.Drawing.Point(840, 182)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(30, 16)
        Me.Label39.TabIndex = 93
        Me.Label39.Text = "[Hr]"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(840, 137)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(30, 16)
        Me.Label40.TabIndex = 92
        Me.Label40.Text = "[Hr]"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.BackColor = System.Drawing.Color.Transparent
        Me.Label41.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(840, 91)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(30, 16)
        Me.Label41.TabIndex = 91
        Me.Label41.Text = "[Hr]"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        Me.Label42.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(840, 45)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(30, 16)
        Me.Label42.TabIndex = 90
        Me.Label42.Text = "[Hr]"
        '
        'LblTareas11
        '
        Me.LblTareas11.AutoSize = True
        Me.LblTareas11.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas11.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas11.ForeColor = System.Drawing.Color.White
        Me.LblTareas11.Location = New System.Drawing.Point(450, 226)
        Me.LblTareas11.Name = "LblTareas11"
        Me.LblTareas11.Size = New System.Drawing.Size(226, 21)
        Me.LblTareas11.TabIndex = 88
        Me.LblTareas11.Text = "Reemplazo de Filtro Blow-By"
        '
        'LblTareas10
        '
        Me.LblTareas10.AutoSize = True
        Me.LblTareas10.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas10.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas10.ForeColor = System.Drawing.Color.White
        Me.LblTareas10.Location = New System.Drawing.Point(450, 181)
        Me.LblTareas10.Name = "LblTareas10"
        Me.LblTareas10.Size = New System.Drawing.Size(312, 21)
        Me.LblTareas10.TabIndex = 87
        Me.LblTareas10.Text = "Mangueras de silicona en circuito de aire"
        '
        'LblTareas9
        '
        Me.LblTareas9.AutoSize = True
        Me.LblTareas9.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas9.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas9.ForeColor = System.Drawing.Color.White
        Me.LblTareas9.Location = New System.Drawing.Point(450, 135)
        Me.LblTareas9.Name = "LblTareas9"
        Me.LblTareas9.Size = New System.Drawing.Size(290, 21)
        Me.LblTareas9.TabIndex = 86
        Me.LblTareas9.Text = "Reemplazo de Correas del Alternador"
        '
        'LblTareas8
        '
        Me.LblTareas8.AutoSize = True
        Me.LblTareas8.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas8.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas8.ForeColor = System.Drawing.Color.White
        Me.LblTareas8.Location = New System.Drawing.Point(450, 89)
        Me.LblTareas8.Name = "LblTareas8"
        Me.LblTareas8.Size = New System.Drawing.Size(244, 21)
        Me.LblTareas8.TabIndex = 85
        Me.LblTareas8.Text = "Limpiar ducto de Filtros de Aire"
        '
        'LblTareas7
        '
        Me.LblTareas7.AutoSize = True
        Me.LblTareas7.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas7.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas7.ForeColor = System.Drawing.Color.White
        Me.LblTareas7.Location = New System.Drawing.Point(450, 43)
        Me.LblTareas7.Name = "LblTareas7"
        Me.LblTareas7.Size = New System.Drawing.Size(207, 21)
        Me.LblTareas7.TabIndex = 84
        Me.LblTareas7.Text = "Reemplazar Filtros de Aire"
        '
        'LblTareas6
        '
        Me.LblTareas6.AutoSize = True
        Me.LblTareas6.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas6.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas6.ForeColor = System.Drawing.Color.White
        Me.LblTareas6.Location = New System.Drawing.Point(9, 233)
        Me.LblTareas6.Name = "LblTareas6"
        Me.LblTareas6.Size = New System.Drawing.Size(219, 21)
        Me.LblTareas6.TabIndex = 71
        Me.LblTareas6.Text = "Chequeo de Luz de Valvulas"
        '
        'VSep
        '
        Me.VSep.Location = New System.Drawing.Point(436, 30)
        Me.VSep.Name = "VSep"
        Me.VSep.Size = New System.Drawing.Size(10, 232)
        Me.VSep.TabIndex = 70
        '
        'LblTareas5
        '
        Me.LblTareas5.AutoSize = True
        Me.LblTareas5.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas5.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas5.ForeColor = System.Drawing.Color.White
        Me.LblTareas5.Location = New System.Drawing.Point(9, 195)
        Me.LblTareas5.Name = "LblTareas5"
        Me.LblTareas5.Size = New System.Drawing.Size(283, 21)
        Me.LblTareas5.TabIndex = 69
        Me.LblTareas5.Text = "Reemplazo de Filtro de Combustible"
        '
        'LblTareas4
        '
        Me.LblTareas4.AutoSize = True
        Me.LblTareas4.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas4.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas4.ForeColor = System.Drawing.Color.White
        Me.LblTareas4.Location = New System.Drawing.Point(9, 157)
        Me.LblTareas4.Name = "LblTareas4"
        Me.LblTareas4.Size = New System.Drawing.Size(286, 21)
        Me.LblTareas4.TabIndex = 67
        Me.LblTareas4.Text = "Reemplazo de Aceite de Transmisión"
        '
        'LblTareas3
        '
        Me.LblTareas3.AutoSize = True
        Me.LblTareas3.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas3.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas3.ForeColor = System.Drawing.Color.White
        Me.LblTareas3.Location = New System.Drawing.Point(9, 119)
        Me.LblTareas3.Name = "LblTareas3"
        Me.LblTareas3.Size = New System.Drawing.Size(240, 21)
        Me.LblTareas3.TabIndex = 65
        Me.LblTareas3.Text = "Reemplazo de Aceite de Motor"
        '
        'LblTareas2
        '
        Me.LblTareas2.AutoSize = True
        Me.LblTareas2.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas2.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas2.ForeColor = System.Drawing.Color.White
        Me.LblTareas2.Location = New System.Drawing.Point(9, 81)
        Me.LblTareas2.Name = "LblTareas2"
        Me.LblTareas2.Size = New System.Drawing.Size(189, 21)
        Me.LblTareas2.TabIndex = 63
        Me.LblTareas2.Text = "Cambio de Refrigerante"
        '
        'LblTareas1
        '
        Me.LblTareas1.AutoSize = True
        Me.LblTareas1.BackColor = System.Drawing.Color.Transparent
        Me.LblTareas1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTareas1.ForeColor = System.Drawing.Color.White
        Me.LblTareas1.Location = New System.Drawing.Point(9, 43)
        Me.LblTareas1.Name = "LblTareas1"
        Me.LblTareas1.Size = New System.Drawing.Size(201, 21)
        Me.LblTareas1.TabIndex = 61
        Me.LblTareas1.Text = "Extraer Muestra de Aceite"
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.LblTarea)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(882, 26)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'LblTarea
        '
        Me.LblTarea.AutoSize = True
        Me.LblTarea.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTarea.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTarea.Location = New System.Drawing.Point(412, 3)
        Me.LblTarea.Name = "LblTarea"
        Me.LblTarea.Size = New System.Drawing.Size(59, 21)
        Me.LblTarea.TabIndex = 19
        Me.LblTarea.Text = "Tareas"
        '
        'LblUnidadD
        '
        Me.LblUnidadD.AutoSize = True
        Me.LblUnidadD.BackColor = System.Drawing.Color.Transparent
        Me.LblUnidadD.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblUnidadD.ForeColor = System.Drawing.Color.White
        Me.LblUnidadD.Location = New System.Drawing.Point(25, 579)
        Me.LblUnidadD.Name = "LblUnidadD"
        Me.LblUnidadD.Size = New System.Drawing.Size(68, 21)
        Me.LblUnidadD.TabIndex = 68
        Me.LblUnidadD.Text = "Unidad:"
        '
        'LblMantD
        '
        Me.LblMantD.AutoSize = True
        Me.LblMantD.BackColor = System.Drawing.Color.Transparent
        Me.LblMantD.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMantD.ForeColor = System.Drawing.Color.White
        Me.LblMantD.Location = New System.Drawing.Point(479, 579)
        Me.LblMantD.Name = "LblMantD"
        Me.LblMantD.Size = New System.Drawing.Size(152, 21)
        Me.LblMantD.TabIndex = 74
        Me.LblMantD.Text = "Mantenimiento en:"
        '
        'LblUnidad
        '
        Me.LblUnidad.AutoSize = True
        Me.LblUnidad.BackColor = System.Drawing.Color.Transparent
        Me.LblUnidad.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblUnidad.ForeColor = System.Drawing.Color.White
        Me.LblUnidad.Location = New System.Drawing.Point(99, 579)
        Me.LblUnidad.Name = "LblUnidad"
        Me.LblUnidad.Size = New System.Drawing.Size(63, 21)
        Me.LblUnidad.TabIndex = 75
        Me.LblUnidad.Text = "unidad"
        '
        'LblMant
        '
        Me.LblMant.AutoSize = True
        Me.LblMant.BackColor = System.Drawing.Color.Transparent
        Me.LblMant.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblMant.ForeColor = System.Drawing.Color.White
        Me.LblMant.Location = New System.Drawing.Point(637, 579)
        Me.LblMant.Name = "LblMant"
        Me.LblMant.Size = New System.Drawing.Size(51, 21)
        Me.LblMant.TabIndex = 76
        Me.LblMant.Text = "mant"
        '
        'TmrUpdate
        '
        Me.TmrUpdate.Interval = 1000
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(694, 579)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 21)
        Me.Label1.TabIndex = 111
        Me.Label1.Text = "[Hr]"
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'TmrLoad
        '
        Me.TmrLoad.Interval = 2000
        '
        'FracDiagnostico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(900, 620)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LblMant)
        Me.Controls.Add(Me.LblUnidad)
        Me.Controls.Add(Me.LblMantD)
        Me.Controls.Add(Me.LblUnidadD)
        Me.Controls.Add(Me.TaskPanel)
        Me.Controls.Add(Me.Guna2Panel3)
        Me.Controls.Add(Me.Guna2Panel2)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FracDiagnostico"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FracDiagnostico"
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        CType(Me.PbSPresLUB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbSTempLub, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbSPresSuccion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        CType(Me.PbSPresDescarga, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.Guna2Panel2.ResumeLayout(False)
        Me.Guna2Panel2.PerformLayout()
        CType(Me.PbMHorasMotor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbMCombustible, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbMPresAceite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbMTempAgua, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        CType(Me.PbMTempAceite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2Panel3.ResumeLayout(False)
        Me.Guna2Panel3.PerformLayout()
        CType(Me.PbTPresAceite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2CustomGradientPanel3.ResumeLayout(False)
        Me.Guna2CustomGradientPanel3.PerformLayout()
        CType(Me.PbTTempAceite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TaskPanel.ResumeLayout(False)
        Me.TaskPanel.PerformLayout()
        CType(Me.PbFiltrosBlowBy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbMangueraSilicona, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbCambioCorrea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbLimpiarDucto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbFiltroAire, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbLuzValvulas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbFiltroCombustible, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbAceiteTransmision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbAceiteMotor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbRefrigerante, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbMuestraAceite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PbSPresDescarga As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents PbSPresLUB As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbSTempLub As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbSPresSuccion As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents Guna2Panel2 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PbMHorasMotor As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMCombustible As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMPresAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMTempAgua As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents PbMTempAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents LblMant As Label
    Friend WithEvents LblUnidad As Label
    Friend WithEvents TaskPanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents LblFiltroBlowBy As Label
    Friend WithEvents LblMangueraSilicona As Label
    Friend WithEvents LblCambioCorrea As Label
    Friend WithEvents LblLimpiarDucto As Label
    Friend WithEvents LblFiltroAire As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents LblLuzValvulas As Label
    Friend WithEvents LblFiltroCombustible As Label
    Friend WithEvents LblAceiteTransmision As Label
    Friend WithEvents LblAceiteMotor As Label
    Friend WithEvents LblRefrigerante As Label
    Friend WithEvents LblMuestraAceite As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents VSep As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents Guna2Panel3 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents PbTPresAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents Guna2CustomGradientPanel3 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents PbTTempAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbFiltrosBlowBy As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMangueraSilicona As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbCambioCorrea As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbLimpiarDucto As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbFiltroAire As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbLuzValvulas As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbFiltroCombustible As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbAceiteTransmision As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbAceiteMotor As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbRefrigerante As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents PbMuestraAceite As Guna.UI2.WinForms.Guna2PictureBox
    Friend WithEvents TmrUpdate As Timer
    Friend WithEvents Label1 As Label
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents LblSPresDescarga As Label
    Public WithEvents LblSensores As Label
    Public WithEvents Title As Label
    Public WithEvents LblSPresLub As Label
    Public WithEvents LblSTempLub As Label
    Public WithEvents LblSPresSuccion As Label
    Public WithEvents LblMHoras As Label
    Public WithEvents LblMCombustible As Label
    Public WithEvents LblMPresAceite As Label
    Public WithEvents LblMTempAgua As Label
    Public WithEvents LblMTempAceite As Label
    Public WithEvents LblMotor As Label
    Public WithEvents LblMantD As Label
    Public WithEvents LblUnidadD As Label
    Public WithEvents LblTareas11 As Label
    Public WithEvents LblTareas10 As Label
    Public WithEvents LblTareas9 As Label
    Public WithEvents LblTareas8 As Label
    Public WithEvents LblTareas7 As Label
    Public WithEvents LblTareas6 As Label
    Public WithEvents LblTareas5 As Label
    Public WithEvents LblTareas4 As Label
    Public WithEvents LblTareas3 As Label
    Public WithEvents LblTareas2 As Label
    Public WithEvents LblTareas1 As Label
    Public WithEvents LblTarea As Label
    Public WithEvents LblTPresAceite As Label
    Public WithEvents LblTTempAceite As Label
    Public WithEvents LblTransmision As Label
    Friend WithEvents ErrorProgress4 As Guna.UI2.WinForms.Guna2WinProgressIndicator
    Friend WithEvents TmrLoad As Timer
    Friend WithEvents ErrorProgress1 As Guna.UI2.WinForms.Guna2WinProgressIndicator
    Friend WithEvents ErrorProgress3 As Guna.UI2.WinForms.Guna2WinProgressIndicator
    Friend WithEvents ErrorProgress2 As Guna.UI2.WinForms.Guna2WinProgressIndicator
End Class
