﻿Public Class SetFC
    Private Sub SetFC_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        TbFC.Select()
        TbFC.Text = 1 ''METERLO DE MY.SETTINGS
        CenterEverything()
    End Sub

    Public Sub CenterEverything()
        CenterObject(Title)
        LeftAlign(LblTB, LblTB.Width + 6)
        LeftAlign(TbFC, 12 + LblTB.Width + TbFC.Width)
        CenterObject(BtnAceptar)
        FCPanel.Width = 12 + LblTB.Width + 6 + TbFC.Width + 6
        CenterObject(FCPanel)
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If Not String.IsNullOrEmpty(TbFC.Text) Then
            If IsDecimal(TbFC.Text) Then
                FC(currentFrac) = GetDouble(TbFC.Text)
                Me.DialogResult = DialogResult.OK
            End If
        End If
    End Sub
End Class