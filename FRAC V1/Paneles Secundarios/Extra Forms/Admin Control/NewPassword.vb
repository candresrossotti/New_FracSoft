﻿Public Class NewPassword
    ''Guardamos la nueva contraseña y DialogResult.OK
    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If Not String.IsNullOrEmpty(TbContraseña.Text) Then
            My.Settings.Item("adminPassword") = TbContraseña.Text
            adminPassword = TbContraseña.Text
            AdminControl.TbContraseña.Text = Nothing
            Me.DialogResult = DialogResult.OK
        End If
    End Sub

    Private Sub NewPassword_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Centramos todo
        CenterEverything()
    End Sub

    ''Sub para centrar todo
    Public Sub CenterEverything()
        CenterObject(Title)
        LeftAlign(LblTB, LblTB.Width + 6)
        LeftAlign(TbContraseña, 12 + LblTB.Width + TbContraseña.Width)
        CenterObject(BtnAceptar)
        NPPanel.Width = 12 + LblTB.Width + 6 + TbContraseña.Width + 6
        CenterObject(NPPanel)
    End Sub

    ''Si escribio algo dejamos que acepte
    Private Sub TbContraseña_TextChanged(sender As Object, e As EventArgs) Handles TbContraseña.TextChanged
        If Not String.IsNullOrEmpty(TbContraseña.Text) Then
            BtnAceptar.Enabled = True
        Else
            BtnAceptar.Enabled = False
        End If
    End Sub
End Class