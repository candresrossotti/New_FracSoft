﻿Public Class AdminControl
    ''Si la contraseña es correcta -> DialogResult.OK
    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If Not String.IsNullOrEmpty(TbContraseña.Text) Then
            If TbContraseña.Text = adminPassword Then
                Me.DialogResult = DialogResult.OK
            End If
        End If
    End Sub

    Private Sub AdminControl_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        ''Centramos todo
        CenterEverything()
    End Sub

    ''Sub para centrar todo
    Public Sub CenterEverything()
        CenterObject(Title)
        LeftAlign(LblTB, LblTB.Width + 6)
        LeftAlign(TbContraseña, 12 + LblTB.Width + TbContraseña.Width)
        RightAlign(BtnChangePassword, TbContraseña)
        CenterObject(BtnAceptar)
        AdmPanel.Width = 12 + LblTB.Width + 6 + TbContraseña.Width + 6 + BtnChangePassword.Width + 6
        CenterObject(AdmPanel)
    End Sub

    ''Si la contraseña ingresada es la correcta, y clickea para cambiar la contraseña, mostramos el otro form
    Private Sub BtnChangePassword_Click(sender As Object, e As EventArgs) Handles BtnChangePassword.Click
        If Not String.IsNullOrEmpty(TbContraseña.Text) Then
            If TbContraseña.Text = adminPassword Then
                NewPassword.ShowDialog(Me)
            End If
        End If
    End Sub
End Class