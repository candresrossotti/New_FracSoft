﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminControl
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.AdmPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnChangePassword = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.TbContraseña = New Guna.UI2.WinForms.Guna2TextBox()
        Me.LblTB = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.AdmPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(450, 38)
        Me.TitlePanel.TabIndex = 62
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(95, 3)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(261, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Control Administrador"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(402, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'AdmPanel
        '
        Me.AdmPanel.Controls.Add(Me.BtnChangePassword)
        Me.AdmPanel.Controls.Add(Me.BtnAceptar)
        Me.AdmPanel.Controls.Add(Me.TbContraseña)
        Me.AdmPanel.Controls.Add(Me.LblTB)
        Me.AdmPanel.Location = New System.Drawing.Point(12, 44)
        Me.AdmPanel.Name = "AdmPanel"
        Me.AdmPanel.Size = New System.Drawing.Size(426, 119)
        Me.AdmPanel.TabIndex = 69
        '
        'BtnChangePassword
        '
        Me.BtnChangePassword.Animated = True
        Me.BtnChangePassword.BorderColor = System.Drawing.Color.White
        Me.BtnChangePassword.BorderRadius = 5
        Me.BtnChangePassword.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnChangePassword.FillColor = System.Drawing.Color.Transparent
        Me.BtnChangePassword.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.BtnChangePassword.ForeColor = System.Drawing.Color.White
        Me.BtnChangePassword.Image = Global.sFRAC.My.Resources.Resources.padlock
        Me.BtnChangePassword.ImageSize = New System.Drawing.Size(30, 30)
        Me.BtnChangePassword.Location = New System.Drawing.Point(374, 13)
        Me.BtnChangePassword.Name = "BtnChangePassword"
        Me.BtnChangePassword.Size = New System.Drawing.Size(40, 35)
        Me.BtnChangePassword.TabIndex = 72
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Empty
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(137, 66)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(138, 40)
        Me.BtnAceptar.TabIndex = 71
        Me.BtnAceptar.Text = "Aceptar"
        '
        'TbContraseña
        '
        Me.TbContraseña.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbContraseña.DefaultText = ""
        Me.TbContraseña.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbContraseña.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbContraseña.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbContraseña.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbContraseña.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbContraseña.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbContraseña.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbContraseña.Location = New System.Drawing.Point(168, 13)
        Me.TbContraseña.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbContraseña.Name = "TbContraseña"
        Me.TbContraseña.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TbContraseña.PlaceholderText = ""
        Me.TbContraseña.SelectedText = ""
        Me.TbContraseña.Size = New System.Drawing.Size(200, 35)
        Me.TbContraseña.TabIndex = 70
        Me.TbContraseña.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TbContraseña.UseSystemPasswordChar = True
        '
        'LblTB
        '
        Me.LblTB.AutoSize = True
        Me.LblTB.BackColor = System.Drawing.Color.Transparent
        Me.LblTB.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblTB.ForeColor = System.Drawing.Color.White
        Me.LblTB.Location = New System.Drawing.Point(6, 20)
        Me.LblTB.Name = "LblTB"
        Me.LblTB.Size = New System.Drawing.Size(161, 21)
        Me.LblTB.TabIndex = 69
        Me.LblTB.Text = "Ingresar contraseña:"
        '
        'AdminControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(450, 175)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.AdmPanel)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "AdminControl"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AdminControl"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.AdmPanel.ResumeLayout(False)
        Me.AdmPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Title As Label
    Friend WithEvents AdmPanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents BtnChangePassword As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents TbContraseña As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents LblTB As Label
End Class
