﻿Imports System.Threading
Imports ChartDirector
Public Class TesteoLinea


    ReadOnly stopWatch As New Stopwatch()

    Dim PresionDeseada As Decimal
    Dim MarchaDeseada As Integer

    Private nextDataTime As DateTime
    Public sampleSize As Integer = 7200

    Private ReadOnly ColorBg As Integer = ColorFrom0BGR(Color.FromArgb(40, 40, 40).ToArgb())
    Private ReadOnly ColorBg1 As Integer = ColorFrom0BGR(Color.FromArgb(60, 60, 60).ToArgb())
    Private ReadOnly ColorBg2 As Integer = ColorFrom0BGR(Color.FromArgb(192, 192, 192).ToArgb())

    ''->Titulo del Grafico
    Public Titulo As String = "FRACTURADOR"

    ''->Variables para el Chart
    Private ReadOnly timeStamps(sampleSize - 1) As Date
    Private currentIndex As Integer = 0
    Private hasFinishedInitialization As Boolean
    Private initialFullRange As Double
    Private ReadOnly zoomInLimit As Integer = 10
    Private ReadOnly dataSeriesA(sampleSize - 1) As Double

    Private TimeAnteriorTLinea As Decimal

    Public Frac As Integer

    Private Sub TesteoLinea_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)


        Frac = currentFrac

        ''Nombre del Frac
        LblUnidad.Text = ISystem.GetFracName(Frac)

        CbMarchaDeseada.SelectedIndex = 2
        CbMinutos.Text = 3

        chartUpdateTimer.Enabled = False
        dataRateTimer.Enabled = False
        TmrInit.Start()

        LSelect.UpdateForm(Me)
    End Sub

    ''Sub para centrar todo
    Public Sub CenterEverything()
        CenterObject(Title)

        CenterObject(LblAjustes)
        CenterObject(LblGrafico)

        LeftAlign(LblTL1, LblTL1.Width + 6)
        LeftAlign(LblTL2, LblTL1.Width + 6)
        LeftAlign(LblTL3, LblTL1.Width + 6)

        LeftAlign(LblTL4, LblTL4.Width + 6)
        LeftAlign(LblTL5, LblTL5.Width + 6)
        LeftAlign(LblTL6, LblTL5.Width + 6)

        RightAlign(TbPresion, LblTL1)
        RightAlign(CbMarchaDeseada, LblTL2)
        RightAlign(LblU1, TbPresion)


        RightAlign(CbMinutos, LblTL4)
        RightAlign(LblPresion, LblTL5)
        RightAlign(LblU2, LblPresion)
        RightAlign(LblTiempo, LblTL5)
        RightAlign(LblU3, LblTiempo)

        RightAlign(LblUnidad, LblTL7)

        Panel1.Width = 6 + LblTL1.Width + 6 + TbPresion.Width + 6 + 40
        Panel2.Width = 6 + LblTL4.Width + 6 + CbMinutos.Width + 6
        Panel3.Width = 6 + LblTL5.Width + 6 + TbPresion.Width + 6
        LblDetalle.Width = Panel1.Width - LblTL3.Width - LblTL3.Left

        RightAlign(LblDetalle, LblTL3)
        CenterObject(BtnComenzar)

        CenterObject(Panel1)
        CenterObject(Panel2)
    End Sub

    ''En cierre hacemos dispose y IDLE al frac, lo sacamos del modo prueba
    Private Sub FormClose(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        chartUpdateTimer.Stop()
        dataRateTimer.Stop()
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), FracGeneralValues("RPMIdle"), Frac)
        Thread.Sleep(10)
        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), FracGeneralValues("Parking"), Frac)
        Thread.Sleep(10)
        ISystem.MoxaSend(FracMoxaSendStartAddress("PruebaDeLinea_StartAddress"), 0, Frac)
        Me.Dispose()
    End Sub

    ''Comenzamos el testeo de linea
    Private Sub BtnComenzar_Click(sender As Object, e As EventArgs) Handles BtnComenzar.Click
        If IsDecimal(PresionDeseada) And PresionDeseada <> 0 And FracData.DblVFrac("EstadoAcelerador_" + Frac.ToString()) = 1 Then

            ISystem.MoxaSend(FracMoxaSendStartAddress("TRIP_StartAddress"), PresionDeseada, Frac)
            Thread.Sleep(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("PruebaDeLinea_StartAddress"), 1, Frac)
            Thread.Sleep(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), MarchaDeseada, Frac)

            CbMinutos.Enabled = False
            CbMarchaDeseada.Enabled = False
            TbPresion.Enabled = False
            BtnComenzar.Enabled = False

            ''Inicializamos el buffer de tiempo
            For i = 0 To UBound(timeStamps)
                timeStamps(i) = DateTime.MinValue
            Next

            nextDataTime = New DateTime((Now.Ticks \ 10000000) * 10000000)

            chartUpdateTimer.Enabled = True
            dataRateTimer.Enabled = True

            chartUpdateTimer.Start()
            dataRateTimer.Start()

            hasFinishedInitialization = True
            WinChartViewer1.MouseWheelZoomRatio = 1.1


            TmrStart.Start()
            stopWatch.Start()
            WinChartViewer1.updateDisplay()
        End If
    End Sub

    ''Timer de datos del graficador
    Private Sub DataRateTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles dataRateTimer.Tick

        Do While nextDataTime < DateTime.Now
            '
            ' In this demo, we use some formulas to generate new values. In real applications,
            ' it may be replaced by some data acquisition code.
            '
            Dim p As Double = nextDataTime.Ticks / 10000000.0 * 4
            Dim dataA As Double = FracData.DblVFrac("Viatran_" + Frac.ToString())


            ' We provide some visual feedback to the numbers generated, so you can see the
            ' values being generated.
            LblPresion.Text = dataA.ToString(".##")


            ' After obtaining the new values, we need to update the data arrays.
            If currentIndex < timeStamps.Length Then
                ' Store the new values in the current index position, and increment the index.
                dataSeriesA(currentIndex) = dataA
                timeStamps(currentIndex) = nextDataTime
                currentIndex += 1
            Else
                ' The data arrays are full. Shift the arrays and store the values at the end.
                ShiftData(dataSeriesA, dataA)
                ShiftData(timeStamps, nextDataTime)
            End If

            ' Update nextDataTime. This is needed by our data generator. In real applications,
            ' you may not need this variable or the associated do/while loop.
            nextDataTime = nextDataTime.AddMilliseconds(dataRateTimer.Interval)
        Loop

    End Sub

    ''Timer para chart update
    Private Sub ChartUpdateTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles chartUpdateTimer.Tick

        Dim viewer As WinChartViewer = WinChartViewer1

        If currentIndex > 0 Then

            Dim startDate As DateTime = timeStamps(0)
            Dim endDate As DateTime = timeStamps(currentIndex - 1)


            ''Si nos encontramos dentro de la ventana inicial, nos movemos en ella
            Dim duration As Double = endDate.Subtract(startDate).TotalSeconds
            If duration < initialFullRange Then
                endDate = startDate.AddSeconds(initialFullRange)
            End If

            ''Si se supero la ventana nos movemos con el maximo, excepto que estemos viendo informacion vieja
            Dim updateType As Integer = Chart.ScrollWithMax
            If viewer.ViewPortLeft + viewer.ViewPortWidth < 0.999 Then
                updateType = Chart.KeepVisibleRange
            End If
            Dim axisScaleHasChanged As Boolean = viewer.updateFullRangeH("x", startDate, endDate, updateType)

            viewer.ZoomInWidthLimit = zoomInLimit / (viewer.getValueAtViewPort("x", 1) -
                viewer.getValueAtViewPort("x", 0))

            ''Actualizamos el chart
            If axisScaleHasChanged Or duration < initialFullRange Then
                viewer.updateViewPort(True, False)
            End If

        End If
    End Sub

    Private Sub ShiftData(ByVal data As Object, ByVal newValue As Object)

        Dim i As Integer
        For i = 1 To UBound(data)
            data(i - 1) = data(i)
        Next
        data(UBound(data)) = newValue

    End Sub

    Private Sub WinChartViewer1_ViewPortChanged(ByVal sender As Object, ByVal e As WinViewPortEventArgs) Handles WinChartViewer1.ViewPortChanged
        ''Actualizamos los controles
        UpdateControls(WinChartViewer1)

        ''Actualizamos el grafico si es necesario
        If e.NeedUpdateChart Then
            DrawChart(WinChartViewer1)
        End If
    End Sub

    ''Actualizar scrollbar
    Private Sub UpdateControls(ByVal viewer As WinChartViewer)
        ''Actualizar scrollbar          
        hScrollBar1.Enabled = WinChartViewer1.ViewPortWidth < 1
        hScrollBar1.LargeChange = Math.Ceiling(WinChartViewer1.ViewPortWidth *
            (hScrollBar1.Maximum - hScrollBar1.Minimum))
        hScrollBar1.SmallChange = Math.Ceiling(hScrollBar1.LargeChange * 0.1)
        hScrollBar1.Value = Math.Round(WinChartViewer1.ViewPortLeft *
            (hScrollBar1.Maximum - hScrollBar1.Minimum)) + hScrollBar1.Minimum
    End Sub

    Private Sub DrawChart(ByVal viewer As WinChartViewer)

        Dim viewPortStartDate As DateTime = Chart.NTime(viewer.getValueAtViewPort("x", viewer.ViewPortLeft))
        Dim viewPortEndDate As DateTime = Chart.NTime(viewer.getValueAtViewPort("x", viewer.ViewPortLeft +
            viewer.ViewPortWidth))


        '================================================================================
        ' Configuracion del estilo del chart.
        '================================================================================

        ''Nuevo chart con esas dimensiones, color de fondo y borde.

        ' Create an XYChart object 639 x 550 pixels in size
        Dim c As New XYChart(639, 550, ColorBg, &H0, 1)
        c.setRoundedFrame(ColorBg1)

        ' Set the plotarea at (55, 25) and of size 555 x 475 pixels.
        ' Set clipping mode to clip the data lines to the plot area.
        c.setPlotArea(55, 25, 555, 475, -1, -1, ColorBg2, ColorBg1, ColorBg1)
        c.setClipping()

        ''Seteamos estilo al eje X
        c.xAxis().setLabelStyle("Montserrat", 10)
        c.xAxis().setTickDensity(75, 15)
        c.xAxis().setWidth(1)
        c.xAxis().setColors(ColorBg2, ColorBg2, ColorBg2, ColorBg2)


        c.yAxis().setTitle(LanguageSelect.DynamicLanguageRM.GetString("Presion") + " [PSI]", "Montserrat", 10)
        c.yAxis().setWidth(1)
        c.yAxis().setColors(ColorBg2, ColorBg2, ColorBg2, ColorBg2)
        '================================================================================
        ' Agregamos la informacion al chart
        '================================================================================

        c.yAxis().addZone(PresionDeseada, PresionDeseada + 0.5, ColorFrom0BGR(Color.Red.ToArgb))

        Dim iLayer As LineLayer = c.addLineLayer()
        iLayer.setXData(timeStamps)
        iLayer.addDataSet(dataSeriesA, ColorFrom0BGR(Color.Blue.ToArgb), LanguageSelect.DynamicLanguageRM.GetString("Presion") + " " + LblUnidad.Text)

        '================================================================================
        ' Configuracion de la escala de X y las leyendas
        '================================================================================

        If currentIndex > 0 Then
            c.xAxis().setDateScale(viewPortStartDate, viewPortEndDate)
        End If


        c.xAxis().setTickDensity(75)
        c.yAxis().setTickDensity(30)

        c.xAxis().setFormatCondition("align", 360 * 86400)
        c.xAxis().setLabelFormat("{value|yyyy}")

        c.xAxis().setFormatCondition("align", 30 * 86400)
        c.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
            Chart.AllPassFilter(), "{value|mmm}")

        c.xAxis().setFormatCondition("align", 86400)
        c.xAxis().setMultiFormat(Chart.StartOfYearFilter(),
            "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}", Chart.StartOfMonthFilter(),
            "<*font=bold*>{value|mmm dd}")
        c.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}")

        c.xAxis().setFormatCondition("align", 3600)
        c.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
            Chart.AllPassFilter(), "{value|hh:nn}")

        c.xAxis().setFormatCondition("align", 60)
        c.xAxis().setLabelFormat("{value|hh:nn}")

        c.xAxis().setFormatCondition("else")
        c.xAxis().setLabelFormat("{value|hh:nn:ss}")

        c.xAxis().setMinTickInc(1)

        '================================================================================
        ' Dibujamos el grafico
        '================================================================================

        ''Si el mouse esta dentro del chart, dibujamos las leyendas segun se haya elegido
        If Not viewer.IsInMouseMoveEvent Then
            TrackLineAxis(c, IIf(IsNothing(viewer.Chart), c.getPlotArea().getRightX(),
                  viewer.PlotAreaMouseX))
        End If

        ''Se agrega el grafico
        viewer.Chart = c

    End Sub

    ''Si mueve el mouse en el chart, actualizamos
    Private Sub WinChartViewer1_MouseMovePlotArea(ByVal sender As Object,
        ByVal e As System.Windows.Forms.MouseEventArgs) Handles WinChartViewer1.MouseMovePlotArea

        Dim viewer As WinChartViewer = sender
        TrackLineAxis(viewer.Chart, viewer.PlotAreaMouseX)
        viewer.updateDisplay()

    End Sub

    ''Mostramos datos en eje y linea vertical con el mouse
    Private Sub TrackLineAxis(c As XYChart, mouseX As Integer)

        ' Clear the current dynamic layer and get the DrawArea object to draw on it.
        Dim d As DrawArea = c.initDynamicLayer()

        ' The plot area object
        Dim plotArea As PlotArea = c.getPlotArea()

        ' Get the data x-value that is nearest to the mouse, and find its pixel coordinate.
        Dim xValue As Double = c.getNearestXValue(mouseX)
        Dim xCoor As Integer = c.getXCoor(xValue)

        ' The vertical track line is drawn up to the highest data point (the point with smallest
        ' y-coordinate). We need to iterate all datasets in all layers to determine where it is.
        Dim minY As Integer = plotArea.getBottomY()

        ' Iterate through all layers to find the highest data point
        For i As Integer = 0 To c.getLayerCount() - 1
            Dim layer As Layer = c.getLayerByZ(i)

            ' The data array index of the x-value
            Dim xIndex As Integer = layer.getXIndexOf(xValue)

            ' Iterate through all the data sets in the layer
            For j As Integer = 0 To layer.getDataSetCount() - 1
                Dim dataSet As ChartDirector.DataSet = layer.getDataSetByZ(j)

                Dim dataPoint As Double = dataSet.getPosition(xIndex)
                If (dataPoint <> Chart.NoValue) And (dataSet.getDataColor() <> Chart.Transparent) Then
                    minY = Math.Min(minY, c.getYCoor(dataPoint, dataSet.getUseYAxis()))
                End If
            Next
        Next

        ' Draw a vertical track line at the x-position up to the highest data point.
        d.vline(Math.Max(minY, plotArea.getTopY()), plotArea.getBottomY() + 6, xCoor, d.dashLineColor(
            ColorBg2, &H101))

        ' Draw a label on the x-axis to show the track line position
        d.text("<*font,bgColor=5555555*> " & c.xAxis().getFormattedLabel(xValue,
            "hh:nn:ss.ff") & " <*/font*>", "Montserrat", 10).draw(xCoor, plotArea.getBottomY() + 6, &HFFFFFF, Chart.Top)

        ' Iterate through all layers to build the legend array
        For i As Integer = 0 To c.getLayerCount() - 1
            Dim layer As Layer = c.getLayerByZ(i)

            ' The data array index of the x-value
            Dim xIndex As Integer = layer.getXIndexOf(xValue)

            ' Iterate through all the data sets in the layer
            For j As Integer = 0 To layer.getDataSetCount() - 1
                Dim dataSet As ChartDirector.DataSet = layer.getDataSetByZ(j)

                ' The positional value, axis binding, pixel coordinate and color of the data point.
                Dim dataPoint As Double = dataSet.getPosition(xIndex)
                Dim yAxis As Axis = dataSet.getUseYAxis()
                Dim yCoor As Integer = c.getYCoor(dataPoint, yAxis)
                Dim color As Integer = dataSet.getDataColor()

                ' Draw the axis label only for visible data points of named data sets
                If (dataPoint <> Chart.NoValue) And (color <> Chart.Transparent) And (yCoor >=
                    plotArea.getTopY()) And (yCoor <= plotArea.getBottomY()) Then
                    ' The axis label consists of 3 parts - a track dot for the data point, an axis label, and
                    ' a line joining the track dot to the axis label.

                    ' Draw the line first. The end point of the line at the axis label side depends on whether
                    ' the label is at the left or right side of the axis (that is, on whether the axis is on
                    ' the left or right side of the plot area).
                    Dim xPos As Integer = yAxis.getX() + IIf(yAxis.getAlignment() = Chart.Left, -4, 4)
                    d.hline(xCoor, xPos, yCoor, d.dashLineColor(color, &H101))

                    ' Draw the track dot
                    d.circle(xCoor, yCoor, 4, 4, color, color)

                    ' Draw the axis label. If the axis is on the left side of the plot area, the labels should
                    ' right aligned to the axis, and vice versa.
                    d.text("<*font,bgColor=000000" & Hex(color) & "*> " & c.formatValue(dataPoint, "{value|P4}") &
                        " <*/font*>", "Montserrat", 10).draw(xPos, yCoor, &HFFFFFF, IIf(yAxis.getAlignment() _
                         = Chart.Left, Chart.Right, Chart.Left))
                End If
            Next
        Next
        Dim legendEntries As New ArrayList()

        ' Iterate through all layers to build the legend array
        For i As Integer = 0 To c.getLayerCount() - 1
            Dim layer As Layer = c.getLayerByZ(i)

            ' The data array index of the x-value
            Dim xIndex As Integer = layer.getXIndexOf(xValue)

            ' Iterate through all the data sets in the layer
            For j As Integer = 0 To layer.getDataSetCount() - 1
                Dim dataSet As ChartDirector.DataSet = layer.getDataSetByZ(j)

                ' We are only interested in visible data sets with names
                Dim dataName As String = dataSet.getDataName()
                Dim color As Integer = dataSet.getDataColor()
                If (Not String.IsNullOrEmpty(dataName)) And (color <> Chart.Transparent) Then
                    ' Build the legend entry, consist of the legend icon, name and data value.
                    Dim dataValue As Double = dataSet.getValue(xIndex)
                    legendEntries.Add("<*block*>" & dataSet.getLegendIcon() & " " & dataName & ": " & IIf(
                        dataValue = Chart.NoValue, "N/A", c.formatValue(dataValue, "{value|P4}")) & "<*/*>")

                    ' Draw a track dot for data points within the plot area
                    Dim yCoor As Integer = c.getYCoor(dataSet.getPosition(xIndex), dataSet.getUseYAxis())
                    If (yCoor >= plotArea.getTopY()) And (yCoor <= plotArea.getBottomY()) Then
                        d.circle(xCoor, yCoor, 4, 4, color, color)
                    End If
                End If
            Next
        Next

        ' Create the legend by joining the legend entries
        legendEntries.Reverse()
        Dim legendText As String = "<*block,maxWidth=" & plotArea.getWidth() &
            "*><*block*><*font=Montserrat,Color=" + Hex(ColorBg2) + "*>[" + LanguageSelect.DynamicLanguageRM.GetString("Hora") + ": " & c.xAxis().getFormattedLabel(xValue, "hh:nn:ss") &
            "]<*/*>        " & Join(CType(legendEntries.ToArray(GetType(String)), String()), "        ") &
            "<*/*>"

        ' Display the legend on the top of the plot area
        Dim y As TTFText = d.text(legendText, "Montserrat", 10)
        y.draw(plotArea.getLeftX() + 5, plotArea.getTopY() - 3, (ColorBg2), Chart.BottomLeft)

    End Sub

    ''Presion maxima y minima 14500 y 0 PSI
    Private Sub TbPresion_TextChanged(sender As Object, e As EventArgs) Handles TbPresion.TextChanged
        If Not String.IsNullOrEmpty(TbPresion.Text) Then
            If IsDecimal(TbPresion.Text) Then
                PresionDeseada = GetDecimal(TbPresion.Text)
                If PresionDeseada > 14500 Then
                    PresionDeseada = 14500
                    TbPresion.Text = 14500.ToString()
                    avisoStr = LanguageSelect.DynamicLanguageRM.GetString("PresionMaxima") + " 14500 PSI"
                    avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                    AvisoGeneral.ShowDialog(Me)
                ElseIf PresionDeseada < 0 Then
                    PresionDeseada = 0
                    TbPresion.Text = 0.ToString()
                    avisoStr = LanguageSelect.DynamicLanguageRM.GetString("PresionMinima") + " 0 PSI"
                    avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                    AvisoGeneral.ShowDialog(Me)
                End If
            End If
        End If
    End Sub

    ''Cambio de marcha, mostramos maxima presion
    Private Sub CbMarchaDeseada_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbMarchaDeseada.SelectedIndexChanged
        MarchaDeseada = FracGeneralValues("Marcha" + CbMarchaDeseada.Text)
        Dim MaxPres As Integer
        Select Case MarchaDeseada
            Case 127
                MaxPres = 13100
            Case 128
                MaxPres = 11600
            Case 129
                MaxPres = 10200
            Case 130
                MaxPres = 8700
            Case 131
                MaxPres = 7300
            Case 132
                MaxPres = 5800
        End Select
        Select Case LanguageSelect.CurrentLanguage
            Case "Español"
                LblDetalle.Text = "La presión maxima en la marcha " + CbMarchaDeseada.Text + " es de " + MaxPres.ToString() + " PSI"
            Case "Ingles"
                LblDetalle.Text = "The maximum pressure in gear number " + CbMarchaDeseada.Text + " is " + MaxPres.ToString() + " PSI"
        End Select

    End Sub

    ''Btn IDLE, paramos modo prueba
    Private Sub BtnIdle_Click(sender As Object, e As EventArgs) Handles BtnIdle.Click
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), FracGeneralValues("RPMIdle"), Frac)
        Thread.Sleep(10)
        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), FracGeneralValues("Parking"), Frac)
        Thread.Sleep(10)
        ISystem.MoxaSend(FracMoxaSendStartAddress("PruebaDeLinea_StartAddress"), 0, Frac)

        stopWatch.Reset()
        CbMinutos.Enabled = True
        CbMarchaDeseada.Enabled = True
        TbPresion.Enabled = True
        BtnComenzar.Enabled = True
        dataRateTimer.Stop()
        chartUpdateTimer.Stop()
        TmrStart.Stop()
        currentIndex = 0
    End Sub

    ''Cambiamos el tamaño del chart inicial segun los minutos seleccionados
    Private Sub CbMinutos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbMinutos.SelectedIndexChanged
        initialFullRange = CInt(CbMinutos.Text) * 60
    End Sub

    ''TmrStart, seguimos la operacion
    Private Sub TmrStart_Tick(sender As Object, e As EventArgs) Handles TmrStart.Tick

        Dim ElapsedTicks As Decimal
        Dim Frequency As Decimal
        Dim NewTime As Decimal

        ElapsedTicks = stopWatch.ElapsedTicks
        Frequency = Stopwatch.Frequency
        NewTime = ElapsedTicks / Frequency

        If NewTime > initialFullRange Then      ''ESTO VA?
            dataRateTimer.Stop()
            chartUpdateTimer.Stop()
            TmrStart.Stop()
            stopWatch.Reset()

            ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), FracGeneralValues("RPMIdle"), Frac)
            Thread.Sleep(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), FracGeneralValues("Parking"), Frac)
            Thread.Sleep(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("PruebaDeLinea_StartAddress"), 0, Frac)
        End If

        LblTiempo.Text = (NewTime).ToString("F0")

        TimeAnteriorTLinea = NewTime
    End Sub

    ''TmrInit -> DrawChart
    Private Sub TmrInit_Tick(sender As Object, e As EventArgs) Handles TmrInit.Tick
        TmrInit.Stop()
        DrawChart(WinChartViewer1)
    End Sub
End Class