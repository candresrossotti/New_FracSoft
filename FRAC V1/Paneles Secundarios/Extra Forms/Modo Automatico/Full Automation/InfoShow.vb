﻿Public Class InfoShow

    Private Sub InfoShow_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        'Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        For ThisFrac = 0 To FullAutomation.NFrac - 1

            Dim OFI As New OneFracInfo
            OFI.LblUnidad1.Text = ISystem.GetFracName(FracList(ThisFrac))
            OFI.LblCaudal1.Text = FracAutomation.FracAutomationSettings.Item(FracList(ThisFrac)).Item("Caudal").ToString("F2")
            OFI.LblMarcha1.Text = FracAutomation.FracAutomationSettings.Item(FracList(ThisFrac)).Item("TargetGear")
            OFI.LblRPM1.Text = FracAutomation.FracAutomationSettings.Item(FracList(ThisFrac)).Item("TargetRPM")
            OFI.LblHP1.Text = FracAutomation.FracAutomationSettings.Item(FracList(ThisFrac)).Item("UsedHP")
            If FracAutomation.FracAutomationSettings.Item(FracList(ThisFrac)).Item("UsedHP") > 0.7 * FracData.SelectedEngines("SelectedEngine_" + FracList(ThisFrac).ToString())("MaxHP") Then
                OFI.PbWarning1.Show()
                OFI.ToolTip.SetToolTip(OFI.PbWarning1, "Utilizacion mayor al 70%")
            End If
            If ThisFrac > 0 Then
                Dim LastControl As OneFracInfo = PanelInfo.Controls.Find("Info" + (ThisFrac - 1).ToString, True)(0)
                OFI.Top = LastControl.Top + OFI.Height
                PanelInfo.Height += OFI.Height
                Panel1.Height += OFI.Height
                Height += OFI.Height
                BtnAceptar.Top += OFI.Height
            End If

            OFI.Name = "Info" + ThisFrac.ToString()
            PanelInfo.Controls.Add(OFI)

        Next

        Guna2VSeparator2.Height = PanelInfo.Height

        CenterObject(Title)
        CenterObject(BtnAceptar)
        CenterToScreen()

        LblDuracion.Text = "Duración: 00:00:00"

    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        DialogResult = DialogResult.OK
    End Sub

End Class