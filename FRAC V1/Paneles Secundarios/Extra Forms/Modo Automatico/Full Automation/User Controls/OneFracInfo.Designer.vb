﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OneFracInfo
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.LblHP1 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.LblRPM1 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.LblMarcha1 = New System.Windows.Forms.Label()
        Me.LblUnidad1 = New System.Windows.Forms.Label()
        Me.LblCaudal1 = New System.Windows.Forms.Label()
        Me.PbWarning1 = New System.Windows.Forms.PictureBox()
        Me.ToolTip = New Guna.UI2.WinForms.Guna2HtmlToolTip()
        CType(Me.PbWarning1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.BackColor = System.Drawing.Color.Transparent
        Me.Label55.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(736, 8)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(30, 20)
        Me.Label55.TabIndex = 137
        Me.Label55.Text = "HP"
        '
        'LblHP1
        '
        Me.LblHP1.AutoSize = True
        Me.LblHP1.BackColor = System.Drawing.Color.Transparent
        Me.LblHP1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblHP1.ForeColor = System.Drawing.Color.White
        Me.LblHP1.Location = New System.Drawing.Point(689, 8)
        Me.LblHP1.Name = "LblHP1"
        Me.LblHP1.Size = New System.Drawing.Size(44, 20)
        Me.LblHP1.TabIndex = 136
        Me.LblHP1.Text = "2000"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.BackColor = System.Drawing.Color.Transparent
        Me.Label57.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label57.ForeColor = System.Drawing.Color.White
        Me.Label57.Location = New System.Drawing.Point(652, 8)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(33, 20)
        Me.Label57.TabIndex = 135
        Me.Label57.Text = "HP:"
        '
        'LblRPM1
        '
        Me.LblRPM1.AutoSize = True
        Me.LblRPM1.BackColor = System.Drawing.Color.Transparent
        Me.LblRPM1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM1.ForeColor = System.Drawing.Color.White
        Me.LblRPM1.Location = New System.Drawing.Point(543, 8)
        Me.LblRPM1.Name = "LblRPM1"
        Me.LblRPM1.Size = New System.Drawing.Size(39, 20)
        Me.LblRPM1.TabIndex = 131
        Me.LblRPM1.Text = "rpm"
        Me.LblRPM1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.BackColor = System.Drawing.Color.Transparent
        Me.Label62.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label62.ForeColor = System.Drawing.Color.White
        Me.Label62.Location = New System.Drawing.Point(495, 8)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(45, 20)
        Me.Label62.TabIndex = 127
        Me.Label62.Text = "RPM:"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.BackColor = System.Drawing.Color.Transparent
        Me.Label63.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label63.ForeColor = System.Drawing.Color.White
        Me.Label63.Location = New System.Drawing.Point(585, 8)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(50, 20)
        Me.Label63.TabIndex = 134
        Me.Label63.Text = "[RPM]"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.BackColor = System.Drawing.Color.Transparent
        Me.Label64.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(2, 8)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(62, 20)
        Me.Label64.TabIndex = 125
        Me.Label64.Text = "Unidad:"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.BackColor = System.Drawing.Color.Transparent
        Me.Label66.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label66.ForeColor = System.Drawing.Color.White
        Me.Label66.Location = New System.Drawing.Point(356, 8)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(64, 20)
        Me.Label66.TabIndex = 126
        Me.Label66.Text = "Marcha:"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.BackColor = System.Drawing.Color.Transparent
        Me.Label67.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label67.ForeColor = System.Drawing.Color.White
        Me.Label67.Location = New System.Drawing.Point(260, 8)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(80, 20)
        Me.Label67.TabIndex = 133
        Me.Label67.Text = "[BBL/MIN]"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.BackColor = System.Drawing.Color.Transparent
        Me.Label68.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label68.ForeColor = System.Drawing.Color.White
        Me.Label68.Location = New System.Drawing.Point(137, 8)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(60, 20)
        Me.Label68.TabIndex = 128
        Me.Label68.Text = "Caudal:"
        '
        'LblMarcha1
        '
        Me.LblMarcha1.BackColor = System.Drawing.Color.Transparent
        Me.LblMarcha1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha1.ForeColor = System.Drawing.Color.White
        Me.LblMarcha1.Location = New System.Drawing.Point(423, 8)
        Me.LblMarcha1.Name = "LblMarcha1"
        Me.LblMarcha1.Size = New System.Drawing.Size(58, 20)
        Me.LblMarcha1.TabIndex = 132
        Me.LblMarcha1.Text = "marcha"
        Me.LblMarcha1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblUnidad1
        '
        Me.LblUnidad1.AutoSize = True
        Me.LblUnidad1.BackColor = System.Drawing.Color.Transparent
        Me.LblUnidad1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidad1.ForeColor = System.Drawing.Color.White
        Me.LblUnidad1.Location = New System.Drawing.Point(68, 8)
        Me.LblUnidad1.Name = "LblUnidad1"
        Me.LblUnidad1.Size = New System.Drawing.Size(57, 20)
        Me.LblUnidad1.TabIndex = 129
        Me.LblUnidad1.Text = "unidad"
        Me.LblUnidad1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblCaudal1
        '
        Me.LblCaudal1.AutoSize = True
        Me.LblCaudal1.BackColor = System.Drawing.Color.Transparent
        Me.LblCaudal1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCaudal1.ForeColor = System.Drawing.Color.White
        Me.LblCaudal1.Location = New System.Drawing.Point(201, 8)
        Me.LblCaudal1.Name = "LblCaudal1"
        Me.LblCaudal1.Size = New System.Drawing.Size(55, 20)
        Me.LblCaudal1.TabIndex = 130
        Me.LblCaudal1.Text = "caudal"
        Me.LblCaudal1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PbWarning1
        '
        Me.PbWarning1.Image = Global.sFRAC.My.Resources.Resources.warning
        Me.PbWarning1.Location = New System.Drawing.Point(764, 3)
        Me.PbWarning1.Name = "PbWarning1"
        Me.PbWarning1.Size = New System.Drawing.Size(30, 30)
        Me.PbWarning1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbWarning1.TabIndex = 138
        Me.PbWarning1.TabStop = False
        Me.PbWarning1.Visible = False
        '
        'ToolTip
        '
        Me.ToolTip.AllowLinksHandling = True
        Me.ToolTip.MaximumSize = New System.Drawing.Size(0, 0)
        Me.ToolTip.TitleFont = New System.Drawing.Font("Montserrat", 10.0!)
        '
        'OneFracInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.Controls.Add(Me.PbWarning1)
        Me.Controls.Add(Me.Label55)
        Me.Controls.Add(Me.LblHP1)
        Me.Controls.Add(Me.Label57)
        Me.Controls.Add(Me.LblRPM1)
        Me.Controls.Add(Me.Label62)
        Me.Controls.Add(Me.Label63)
        Me.Controls.Add(Me.Label64)
        Me.Controls.Add(Me.Label66)
        Me.Controls.Add(Me.Label67)
        Me.Controls.Add(Me.Label68)
        Me.Controls.Add(Me.LblMarcha1)
        Me.Controls.Add(Me.LblUnidad1)
        Me.Controls.Add(Me.LblCaudal1)
        Me.Name = "OneFracInfo"
        Me.Size = New System.Drawing.Size(795, 38)
        CType(Me.PbWarning1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PbWarning1 As PictureBox
    Friend WithEvents Label55 As Label
    Friend WithEvents LblHP1 As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents LblRPM1 As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents Label68 As Label
    Friend WithEvents LblMarcha1 As Label
    Friend WithEvents LblUnidad1 As Label
    Friend WithEvents LblCaudal1 As Label
    Friend WithEvents ToolTip As Guna.UI2.WinForms.Guna2HtmlToolTip
End Class
