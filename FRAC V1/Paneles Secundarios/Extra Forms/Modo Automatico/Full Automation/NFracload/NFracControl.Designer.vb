﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NFracControl
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CbFrac = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblFrac = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CbFrac
        '
        Me.CbFrac.BackColor = System.Drawing.Color.Transparent
        Me.CbFrac.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbFrac.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbFrac.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbFrac.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbFrac.FocusedState.Parent = Me.CbFrac
        Me.CbFrac.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbFrac.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbFrac.HoverState.Parent = Me.CbFrac
        Me.CbFrac.ItemHeight = 30
        Me.CbFrac.ItemsAppearance.Parent = Me.CbFrac
        Me.CbFrac.Location = New System.Drawing.Point(214, 16)
        Me.CbFrac.Name = "CbFrac"
        Me.CbFrac.ShadowDecoration.Parent = Me.CbFrac
        Me.CbFrac.Size = New System.Drawing.Size(200, 36)
        Me.CbFrac.TabIndex = 3
        Me.CbFrac.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblFrac
        '
        Me.LblFrac.AutoSize = True
        Me.LblFrac.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblFrac.ForeColor = System.Drawing.Color.White
        Me.LblFrac.Location = New System.Drawing.Point(-1, 24)
        Me.LblFrac.Name = "LblFrac"
        Me.LblFrac.Size = New System.Drawing.Size(196, 21)
        Me.LblFrac.TabIndex = 2
        Me.LblFrac.Text = "Seleccionar fracturador 1:"
        '
        'NFracControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.CbFrac)
        Me.Controls.Add(Me.LblFrac)
        Me.Name = "NFracControl"
        Me.Size = New System.Drawing.Size(421, 68)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CbFrac As Guna.UI2.WinForms.Guna2ComboBox
    Friend WithEvents LblFrac As Label
End Class
