﻿Public Class NFracLoad
    Public ActiveFracList As New List(Of String)
    Private CurrentControl As Integer = 1
    Private Sub NFracLoad_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CenterObject(BtnAceptar)
        CenterObject(Title)

        For i = 0 To ISystem.Devices - 1
            Dim ActiveFrac As Boolean = FracData.BoolVFrac("ActiveFrac_" + ISystem.GetActiveFrac(i).ToString())
            If ActiveFrac = True Then
                ActiveFracList.Add(ISystem.GetFracName(ISystem.GetActiveFrac(i)))
            End If
        Next

        NFracBackUp.CbFrac.Items.Add("Ninguno")
        NFracBackUp.CbFrac.Items.AddRange(ActiveFracList.ToArray())
        FracC1.CbFrac.Items.AddRange(ActiveFracList.ToArray())

        NFracBackUp.LblFrac.Text = "Seleccionar Back-Up: "
        CenterObject(FracC1)
        CenterObject(NFracBackUp)
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        FracList.Clear()
        If Not String.IsNullOrEmpty(NFracBackUp.CbFrac.Text) Then
            If NFracBackUp.CbFrac.Text <> "Ninguno" Then BackUpFrac = ISystem.GetFracID(NFracBackUp.CbFrac.Text)
            For i = 1 To CurrentControl
                Dim ActualControl As NFracControl = Controls.Find("FracC" + i.ToString(), True)(0)
                Dim ActualFrac As String = ActualControl.CbFrac.Text
                If Not String.IsNullOrEmpty(ActualFrac) Then
                    If FracList.Contains(ISystem.GetFracID(ActualFrac)) Or ISystem.GetFracID(ActualFrac) = BackUpFrac Then
                        avisoStr = LanguageSelect.DynamicLanguageRM.GetString("FracRepetidos")
                        avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                        AvisoGeneral.ShowDialog(Me)
                        Exit Sub
                    Else
                        FracList.Add(ISystem.GetFracID(ActualFrac))
                    End If
                Else
                    avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Valores")
                    avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                    AvisoGeneral.ShowDialog(Me)
                    Exit Sub
                End If
            Next
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Valores")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
            Exit Sub
        End If
        FracAutomate.Update(ISystem, FracData)
        DialogResult = DialogResult.OK
    End Sub

    Private Sub BtnAdd_Click(sender As Object, e As EventArgs) Handles BtnAdd.Click
        If CurrentControl < MaxFracs Then
            If CurrentControl < 10 Then
                Panel1.Height += 74
                Separator.Top += 74
                NFracBackUp.Top += 74
                BtnAceptar.Top += 74
                Height += 74
            End If
            Dim NewControl As New NFracControl
            Dim LastControl As Control = Panel1.Controls.Find("FracC" + CurrentControl.ToString(), True)(0)
            CurrentControl += 1
            NewControl.Top = LastControl.Top + LastControl.Height + 6
            NewControl.CbFrac.Items.AddRange(ActiveFracList.ToArray())
            NewControl.Name = "FracC" + (CurrentControl).ToString()
            NewControl.LblFrac.Text = "Seleccionar fracturador " + CurrentControl.ToString() + ":"
            Panel1.Controls.Add(NewControl)
            CenterObject(NewControl)
            Panel1.ScrollControlIntoView(NewControl)
        Else
            avisoStr = "La cantidad maxima de fracturadores es de: " + MaxFracs.ToString() + " fracturador(es)."
            avisoTitulo = "Aviso"
            AvisoGeneral.ShowDialog(Me)
        End If
    End Sub

    Private Sub BtnDel_Click(sender As Object, e As EventArgs) Handles BtnDel.Click
        If CurrentControl > 1 Then
            If CurrentControl < 11 Then
                Panel1.Height -= 74
                Separator.Top -= 74
                NFracBackUp.Top -= 74
                BtnAceptar.Top -= 74
                Height -= 74
            End If
            Dim LastControl As Control = Panel1.Controls.Find("FracC" + CurrentControl.ToString, True)(0)
            CurrentControl -= 1
            Panel1.Controls.Remove(LastControl)
        End If
    End Sub
End Class