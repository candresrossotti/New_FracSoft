﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NFracLoad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.FracC1 = New sFRAC.NFracControl()
        Me.BtnDel = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAdd = New Guna.UI2.WinForms.Guna2Button()
        Me.PnBackUp = New System.Windows.Forms.Panel()
        Me.NFracBackUp = New sFRAC.NFracControl()
        Me.Separator = New Guna.UI2.WinForms.Guna2Separator()
        Me.TitlePanel.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.PnBackUp.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.ShadowDecoration.Parent = Me.TitlePanel
        Me.TitlePanel.Size = New System.Drawing.Size(500, 38)
        Me.TitlePanel.TabIndex = 68
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Segoe UI", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(150, 3)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(274, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Selección de Fracturadores"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.HoverState.Parent = Me.Guna2ControlBox1
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(452, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.ShadowDecoration.Parent = Me.Guna2ControlBox1
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.CheckedState.Parent = Me.BtnAceptar
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.CustomImages.Parent = Me.BtnAceptar
        Me.BtnAceptar.DisabledState.Parent = Me.BtnAceptar
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.HoverState.Parent = Me.BtnAceptar
        Me.BtnAceptar.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.BtnAceptar.Location = New System.Drawing.Point(161, 262)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.ShadowDecoration.Parent = Me.BtnAceptar
        Me.BtnAceptar.Size = New System.Drawing.Size(164, 40)
        Me.BtnAceptar.TabIndex = 90
        Me.BtnAceptar.Text = "Aceptar"
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.Controls.Add(Me.FracC1)
        Me.Panel1.Location = New System.Drawing.Point(12, 87)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(476, 70)
        Me.Panel1.TabIndex = 97
        '
        'FracC1
        '
        Me.FracC1.BackColor = System.Drawing.Color.Transparent
        Me.FracC1.Location = New System.Drawing.Point(19, 1)
        Me.FracC1.Name = "FracC1"
        Me.FracC1.Size = New System.Drawing.Size(421, 68)
        Me.FracC1.TabIndex = 0
        '
        'BtnDel
        '
        Me.BtnDel.AutoRoundedCorners = True
        Me.BtnDel.BorderRadius = 19
        Me.BtnDel.CheckedState.Parent = Me.BtnDel
        Me.BtnDel.CustomImages.Parent = Me.BtnDel
        Me.BtnDel.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnDel.DisabledState.Parent = Me.BtnDel
        Me.BtnDel.FillColor = System.Drawing.Color.Transparent
        Me.BtnDel.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnDel.ForeColor = System.Drawing.Color.White
        Me.BtnDel.HoverState.Parent = Me.BtnDel
        Me.BtnDel.Image = Global.sFRAC.My.Resources.Resources.minus
        Me.BtnDel.ImageSize = New System.Drawing.Size(40, 40)
        Me.BtnDel.Location = New System.Drawing.Point(260, 41)
        Me.BtnDel.Name = "BtnDel"
        Me.BtnDel.ShadowDecoration.Parent = Me.BtnDel
        Me.BtnDel.Size = New System.Drawing.Size(40, 40)
        Me.BtnDel.TabIndex = 96
        '
        'BtnAdd
        '
        Me.BtnAdd.AutoRoundedCorners = True
        Me.BtnAdd.BorderRadius = 19
        Me.BtnAdd.CheckedState.Parent = Me.BtnAdd
        Me.BtnAdd.CustomImages.Parent = Me.BtnAdd
        Me.BtnAdd.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd.DisabledState.Parent = Me.BtnAdd
        Me.BtnAdd.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd.ForeColor = System.Drawing.Color.White
        Me.BtnAdd.HoverState.Parent = Me.BtnAdd
        Me.BtnAdd.Image = Global.sFRAC.My.Resources.Resources.add
        Me.BtnAdd.ImageSize = New System.Drawing.Size(40, 40)
        Me.BtnAdd.Location = New System.Drawing.Point(214, 41)
        Me.BtnAdd.Name = "BtnAdd"
        Me.BtnAdd.ShadowDecoration.Parent = Me.BtnAdd
        Me.BtnAdd.Size = New System.Drawing.Size(40, 40)
        Me.BtnAdd.TabIndex = 95
        '
        'PnBackUp
        '
        Me.PnBackUp.AutoScroll = True
        Me.PnBackUp.Controls.Add(Me.NFracBackUp)
        Me.PnBackUp.Controls.Add(Me.Separator)
        Me.PnBackUp.Location = New System.Drawing.Point(12, 163)
        Me.PnBackUp.Name = "PnBackUp"
        Me.PnBackUp.Size = New System.Drawing.Size(476, 93)
        Me.PnBackUp.TabIndex = 99
        '
        'NFracBackUp
        '
        Me.NFracBackUp.BackColor = System.Drawing.Color.Transparent
        Me.NFracBackUp.Location = New System.Drawing.Point(19, 17)
        Me.NFracBackUp.Name = "NFracBackUp"
        Me.NFracBackUp.Size = New System.Drawing.Size(421, 68)
        Me.NFracBackUp.TabIndex = 100
        '
        'Separator
        '
        Me.Separator.Location = New System.Drawing.Point(1, 1)
        Me.Separator.Name = "Separator"
        Me.Separator.Size = New System.Drawing.Size(474, 10)
        Me.Separator.TabIndex = 99
        '
        'NFracLoad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(500, 312)
        Me.Controls.Add(Me.PnBackUp)
        Me.Controls.Add(Me.BtnDel)
        Me.Controls.Add(Me.BtnAdd)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "NFracLoad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NFracLoad"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.PnBackUp.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents BtnAdd As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Panel1 As Panel
    Public WithEvents BtnDel As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents FracC1 As NFracControl
    Friend WithEvents PnBackUp As Panel
    Friend WithEvents NFracBackUp As NFracControl
    Friend WithEvents Separator As Guna.UI2.WinForms.Guna2Separator
End Class
