﻿Public Class NFracControl
    Private Sub TbFrac_TextChanged(sender As Object, e As EventArgs)
        If Not String.IsNullOrEmpty(sender.Text) Then
            If IsInteger(sender.Text) = False Then
                sender.Text = Mid(sender.Text, 1, Math.Max(sender.Text.Length() - 1, 0))
                sender.SelectionStart = sender.Text.Length()
            Else
                If GetInteger(sender.Text) > MaxFracs Then
                    Select Case LanguageSelect.CurrentLanguage
                        Case "Español"
                            avisoStr = "Solo se encuentra(n) " + MaxFracs.ToString() + " fracturadores conectado(s)"
                        Case "Ingles"
                            avisoStr = "There is only " + MaxFracs.ToString() + " frac pump unit(s) available"
                    End Select
                    avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Aviso")
                    AvisoGeneral.ShowDialog(Me)
                    sender.Text = MaxFracs
                    sender.SelectionStart = sender.Text.Length()
                End If
            End If
        End If
    End Sub

    Private Sub CbFrac_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbFrac.SelectedIndexChanged
        'NFracLoad.FracList.Remove(CbFrac.Text)
        'Dim ThisControl As Integer = GetInteger(Name)

        'Dim NextControl As NFracControl = NFracLoad.Controls.Find("Frac" + (ThisControl + 1).ToString(), True)(0)
        'If NextControl IsNot Nothing Then

        'End If
    End Sub
End Class
