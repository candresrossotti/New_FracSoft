﻿Public Class FullAutomation

    Private ReadOnly TRIPMaximo As Integer = 16000
    Private ReadOnly CaudalMaximo As Integer = 20
    Private ReadOnly FracAutomation As New FracAutomation

    Public TRIP As Integer
    Public NFrac As Integer
    Public CaudalTotal As Double
    Public CaudalIndividual As Double
    Public Barriles(0) As Double
    Public Etapa As Integer = 1

    Private Sub FullAutomation_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CenterEverything()

        NFrac = FracList.Count

        'FracAutomation.Update() ''Podriamos modificar para que el update mande los datos de la bomba,motor,transmision
    End Sub

    Public Sub CenterEverything()
        CenterObject(Title)

        LeftAlign(LblFA2, TbPresion.Left - 6)
        LeftAlign(LblFA3, TbCaudal.Left - 6)
        LeftAlign(LblFA4, TbBarriles.Left - 6)

    End Sub


    Private Sub UpdateTb()

        TbPresion.Text = TRIP
        TbCaudal.Text = CaudalTotal
        TbBarriles.Text = Barriles(Etapa - 1)
    End Sub

    Private Sub CheckTb()
        If Not String.IsNullOrEmpty(TbCaudal.Text) And Not String.IsNullOrEmpty(TbPresion.Text) And
           Not String.IsNullOrEmpty(TbBarriles.Text) Then
            BtnStart.Enabled = True
        Else
            BtnStart.Enabled = False
        End If
    End Sub

    Private Function CheckValues()

        If GetInteger(TbPresion.Text) > TRIPMaximo Then
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TRIPAlto")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
            Return False
        End If

        If GetDouble(TbCaudal.Text) > CaudalMaximo * NFrac Then
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("CaudalAlto")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
            Return False
        End If

        If GetInteger(TbBarriles.Text) = 0 Then
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("CeroBarriles")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
            Return False
        End If

        Return True
    End Function

    Private Sub TbTextChanged(sender As Object, e As EventArgs) Handles TbPresion.TextChanged, TbCaudal.TextChanged, TbBarriles.TextChanged
        If Not String.IsNullOrEmpty(sender.Text) Then
            If IsDecimal(sender.Text) = False Then
                sender.Text = Mid(sender.Text, 1, sender.Text.Length() - 1)
                sender.SelectionStart = sender.Text.Length()
            End If
            If sender.Name = "TbNFrac" Then
                If Not String.IsNullOrEmpty(sender.Text) Then
                    If sender.Text > MaxFracs Then
                        Select Case LanguageSelect.CurrentLanguage
                            Case "Español"
                                avisoStr = "Solo se encuentra(n) " + MaxFracs.ToString() + " fracturadores conectado(s)"
                            Case "Ingles"
                                avisoStr = "There is only " + MaxFracs.ToString() + " frac pump unit(s) available"
                        End Select
                        avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                        AvisoGeneral.ShowDialog(Me)
                        sender.Text = Math.Min(MaxFracs, GetInteger(sender.Text))
                    End If
                End If
            End If
        End If
        CheckTb()
    End Sub

    Private Sub BtnStart_Click(sender As Object, e As EventArgs) Handles BtnStart.Click
        If CheckValues() = True Then
            TRIP = GetInteger(TbPresion.Text)
            CaudalTotal = GetDouble(TbCaudal.Text)
            Barriles(Etapa - 1) = GetDouble(TbBarriles.Text)

            'If FracAutomate.SetParameters(NFrac, TRIP, CaudalTotal, Barriles, FracList) = True Then


            '    If InfoShow.ShowDialog(Me) = DialogResult.OK Then

            '        FracAutomate.StartOp()

            '        DialogResult = DialogResult.OK
            '    Else
            '        InfoShow.Dispose()
            '        NFracLoad.Dispose()
            '    End If

            'End If
        End If
    End Sub


    Private Sub OpenFile(sender As Object, e As EventArgs) Handles BtnOpen.Click
        Dim FC As New FractureCreate
        If FC.FracRead() = True Then
            TRIP = FractureCreate.PresionDeseada
            CaudalTotal = FractureCreate.CaudalDeseado
            Barriles = FractureCreate.BarrilesTotales
            InitOp_FromFile()
        End If
    End Sub

    Private Sub InitOp_FromFile()


        'If FracAutomate.SetParameters(NFrac, TRIP, CaudalTotal, Barriles, FracList) = True Then
        '    CaudalIndividual = CaudalTotal / NFrac


        '    If InfoShow.ShowDialog(Me) = DialogResult.OK Then
        '        FracAutomate.StartOp()

        '        Me.DialogResult = DialogResult.OK
        '    Else
        '        InfoShow.Dispose()
        '        NFracLoad.Dispose()
        '    End If

        'End If
    End Sub



End Class