﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FullAutomation
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.LblFA2 = New System.Windows.Forms.Label()
        Me.LblFA3 = New System.Windows.Forms.Label()
        Me.BtnStart = New Guna.UI2.WinForms.Guna2Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TbPresion = New Guna.UI2.WinForms.Guna2TextBox()
        Me.TbCaudal = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.LblFA4 = New System.Windows.Forms.Label()
        Me.TbBarriles = New Guna.UI2.WinForms.Guna2TextBox()
        Me.BtnOpen = New Guna.UI2.WinForms.Guna2Button()
        Me.TitlePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.ShadowDecoration.Parent = Me.TitlePanel
        Me.TitlePanel.Size = New System.Drawing.Size(569, 38)
        Me.TitlePanel.TabIndex = 67
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Segoe UI", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(150, 3)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(230, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Automatización Global"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.HoverState.Parent = Me.Guna2ControlBox1
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(521, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.ShadowDecoration.Parent = Me.Guna2ControlBox1
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'LblFA2
        '
        Me.LblFA2.AutoSize = True
        Me.LblFA2.BackColor = System.Drawing.Color.Transparent
        Me.LblFA2.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblFA2.ForeColor = System.Drawing.Color.White
        Me.LblFA2.Location = New System.Drawing.Point(36, 61)
        Me.LblFA2.Name = "LblFA2"
        Me.LblFA2.Size = New System.Drawing.Size(178, 20)
        Me.LblFA2.TabIndex = 68
        Me.LblFA2.Text = "Ingrese el TRIP deseado:"
        '
        'LblFA3
        '
        Me.LblFA3.AutoSize = True
        Me.LblFA3.BackColor = System.Drawing.Color.Transparent
        Me.LblFA3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblFA3.ForeColor = System.Drawing.Color.White
        Me.LblFA3.Location = New System.Drawing.Point(40, 116)
        Me.LblFA3.Name = "LblFA3"
        Me.LblFA3.Size = New System.Drawing.Size(192, 20)
        Me.LblFA3.TabIndex = 69
        Me.LblFA3.Text = "Ingrese el caudal deseado:"
        '
        'BtnStart
        '
        Me.BtnStart.Animated = True
        Me.BtnStart.BorderColor = System.Drawing.Color.White
        Me.BtnStart.BorderRadius = 5
        Me.BtnStart.BorderThickness = 1
        Me.BtnStart.CheckedState.Parent = Me.BtnStart
        Me.BtnStart.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStart.CustomImages.Parent = Me.BtnStart
        Me.BtnStart.DisabledState.Parent = Me.BtnStart
        Me.BtnStart.FillColor = System.Drawing.Color.Transparent
        Me.BtnStart.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnStart.ForeColor = System.Drawing.Color.White
        Me.BtnStart.HoverState.Parent = Me.BtnStart
        Me.BtnStart.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.BtnStart.Location = New System.Drawing.Point(194, 221)
        Me.BtnStart.Name = "BtnStart"
        Me.BtnStart.ShadowDecoration.Parent = Me.BtnStart
        Me.BtnStart.Size = New System.Drawing.Size(164, 40)
        Me.BtnStart.TabIndex = 6
        Me.BtnStart.Text = "Continuar"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(449, 61)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 20)
        Me.Label5.TabIndex = 92
        Me.Label5.Text = "[PSI]"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(449, 116)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 20)
        Me.Label6.TabIndex = 93
        Me.Label6.Text = "[BBL/MIN]"
        '
        'TbPresion
        '
        Me.TbPresion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbPresion.DefaultText = ""
        Me.TbPresion.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbPresion.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbPresion.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPresion.DisabledState.Parent = Me.TbPresion
        Me.TbPresion.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPresion.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPresion.FocusedState.Parent = Me.TbPresion
        Me.TbPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbPresion.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPresion.HoverState.Parent = Me.TbPresion
        Me.TbPresion.Location = New System.Drawing.Point(243, 51)
        Me.TbPresion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbPresion.Name = "TbPresion"
        Me.TbPresion.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbPresion.PlaceholderText = ""
        Me.TbPresion.SelectedText = ""
        Me.TbPresion.ShadowDecoration.Parent = Me.TbPresion
        Me.TbPresion.Size = New System.Drawing.Size(200, 40)
        Me.TbPresion.TabIndex = 1
        Me.TbPresion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TbCaudal
        '
        Me.TbCaudal.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbCaudal.DefaultText = ""
        Me.TbCaudal.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbCaudal.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbCaudal.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCaudal.DisabledState.Parent = Me.TbCaudal
        Me.TbCaudal.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCaudal.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCaudal.FocusedState.Parent = Me.TbCaudal
        Me.TbCaudal.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbCaudal.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCaudal.HoverState.Parent = Me.TbCaudal
        Me.TbCaudal.Location = New System.Drawing.Point(243, 106)
        Me.TbCaudal.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbCaudal.Name = "TbCaudal"
        Me.TbCaudal.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbCaudal.PlaceholderText = ""
        Me.TbCaudal.SelectedText = ""
        Me.TbCaudal.ShadowDecoration.Parent = Me.TbCaudal
        Me.TbCaudal.Size = New System.Drawing.Size(200, 40)
        Me.TbCaudal.TabIndex = 2
        Me.TbCaudal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(449, 171)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 20)
        Me.Label7.TabIndex = 99
        Me.Label7.Text = "[BBL]"
        '
        'LblFA4
        '
        Me.LblFA4.AutoSize = True
        Me.LblFA4.BackColor = System.Drawing.Color.Transparent
        Me.LblFA4.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblFA4.ForeColor = System.Drawing.Color.White
        Me.LblFA4.Location = New System.Drawing.Point(13, 171)
        Me.LblFA4.Name = "LblFA4"
        Me.LblFA4.Size = New System.Drawing.Size(222, 20)
        Me.LblFA4.TabIndex = 98
        Me.LblFA4.Text = "Ingrese la cantidad de barriles:"
        '
        'TbBarriles
        '
        Me.TbBarriles.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbBarriles.DefaultText = ""
        Me.TbBarriles.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbBarriles.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbBarriles.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbBarriles.DisabledState.Parent = Me.TbBarriles
        Me.TbBarriles.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbBarriles.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbBarriles.FocusedState.Parent = Me.TbBarriles
        Me.TbBarriles.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbBarriles.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbBarriles.HoverState.Parent = Me.TbBarriles
        Me.TbBarriles.Location = New System.Drawing.Point(243, 161)
        Me.TbBarriles.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbBarriles.Name = "TbBarriles"
        Me.TbBarriles.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbBarriles.PlaceholderText = ""
        Me.TbBarriles.SelectedText = ""
        Me.TbBarriles.ShadowDecoration.Parent = Me.TbBarriles
        Me.TbBarriles.Size = New System.Drawing.Size(200, 40)
        Me.TbBarriles.TabIndex = 3
        Me.TbBarriles.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BtnOpen
        '
        Me.BtnOpen.CheckedState.Parent = Me.BtnOpen
        Me.BtnOpen.CustomImages.Parent = Me.BtnOpen
        Me.BtnOpen.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.BtnOpen.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.BtnOpen.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.BtnOpen.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.BtnOpen.DisabledState.Parent = Me.BtnOpen
        Me.BtnOpen.FillColor = System.Drawing.Color.Transparent
        Me.BtnOpen.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnOpen.ForeColor = System.Drawing.Color.White
        Me.BtnOpen.HoverState.Parent = Me.BtnOpen
        Me.BtnOpen.Image = Global.sFRAC.My.Resources.Resources.open_blanco
        Me.BtnOpen.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnOpen.ImageSize = New System.Drawing.Size(39, 39)
        Me.BtnOpen.Location = New System.Drawing.Point(522, 219)
        Me.BtnOpen.Name = "BtnOpen"
        Me.BtnOpen.ShadowDecoration.Parent = Me.BtnOpen
        Me.BtnOpen.Size = New System.Drawing.Size(40, 40)
        Me.BtnOpen.TabIndex = 8
        '
        'FullAutomation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(569, 273)
        Me.Controls.Add(Me.BtnOpen)
        Me.Controls.Add(Me.TbBarriles)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.LblFA4)
        Me.Controls.Add(Me.TbCaudal)
        Me.Controls.Add(Me.TbPresion)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.BtnStart)
        Me.Controls.Add(Me.LblFA3)
        Me.Controls.Add(Me.LblFA2)
        Me.Controls.Add(Me.TitlePanel)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FullAutomation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FullAutomation"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Title As Label
    Public WithEvents LblFA2 As Label
    Public WithEvents LblFA3 As Label
    Public WithEvents BtnStart As Guna.UI2.WinForms.Guna2Button
    Public WithEvents TbCaudal As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents TbPresion As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents Label6 As Label
    Public WithEvents Label5 As Label
    Public WithEvents Label7 As Label
    Public WithEvents LblFA4 As Label
    Public WithEvents TbBarriles As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents BtnOpen As Guna.UI2.WinForms.Guna2Button
End Class
