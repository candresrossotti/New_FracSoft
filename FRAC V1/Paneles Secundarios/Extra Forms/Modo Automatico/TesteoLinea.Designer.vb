﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TesteoLinea
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2Panel3 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LblU1 = New System.Windows.Forms.Label()
        Me.LblDetalle = New System.Windows.Forms.Label()
        Me.LblTL3 = New System.Windows.Forms.Label()
        Me.CbMarchaDeseada = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblTL2 = New System.Windows.Forms.Label()
        Me.BtnComenzar = New Guna.UI2.WinForms.Guna2Button()
        Me.TbPresion = New Guna.UI2.WinForms.Guna2TextBox()
        Me.LblTL1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblAjustes = New System.Windows.Forms.Label()
        Me.LblTL7 = New System.Windows.Forms.Label()
        Me.LblUnidad = New System.Windows.Forms.Label()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnIdle = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblGrafico = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.CbMinutos = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblTL4 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.LblU3 = New System.Windows.Forms.Label()
        Me.LblTiempo = New System.Windows.Forms.Label()
        Me.LblTL6 = New System.Windows.Forms.Label()
        Me.LblU2 = New System.Windows.Forms.Label()
        Me.LblPresion = New System.Windows.Forms.Label()
        Me.LblTL5 = New System.Windows.Forms.Label()
        Me.dataRateTimer = New System.Windows.Forms.Timer(Me.components)
        Me.chartUpdateTimer = New System.Windows.Forms.Timer(Me.components)
        Me.hScrollBar1 = New Guna.UI2.WinForms.Guna2HScrollBar()
        Me.TmrStart = New System.Windows.Forms.Timer(Me.components)
        Me.WinChartViewer1 = New ChartDirector.WinChartViewer()
        Me.TmrInit = New System.Windows.Forms.Timer(Me.components)
        Me.TitlePanel.SuspendLayout()
        Me.Guna2Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.WinChartViewer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(1125, 38)
        Me.TitlePanel.TabIndex = 65
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(470, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(185, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Testeo de Linea"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(1077, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.TitlePanel
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.Title
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'Guna2Panel3
        '
        Me.Guna2Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel3.BorderRadius = 6
        Me.Guna2Panel3.BorderThickness = 1
        Me.Guna2Panel3.Controls.Add(Me.Panel1)
        Me.Guna2Panel3.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.Guna2Panel3.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel3.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel3.Location = New System.Drawing.Point(12, 44)
        Me.Guna2Panel3.Name = "Guna2Panel3"
        Me.Guna2Panel3.Size = New System.Drawing.Size(456, 265)
        Me.Guna2Panel3.TabIndex = 77
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.LblU1)
        Me.Panel1.Controls.Add(Me.LblDetalle)
        Me.Panel1.Controls.Add(Me.LblTL3)
        Me.Panel1.Controls.Add(Me.CbMarchaDeseada)
        Me.Panel1.Controls.Add(Me.LblTL2)
        Me.Panel1.Controls.Add(Me.BtnComenzar)
        Me.Panel1.Controls.Add(Me.TbPresion)
        Me.Panel1.Controls.Add(Me.LblTL1)
        Me.Panel1.Location = New System.Drawing.Point(4, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(449, 230)
        Me.Panel1.TabIndex = 20
        '
        'LblU1
        '
        Me.LblU1.AutoSize = True
        Me.LblU1.BackColor = System.Drawing.Color.Transparent
        Me.LblU1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblU1.ForeColor = System.Drawing.Color.White
        Me.LblU1.Location = New System.Drawing.Point(411, 12)
        Me.LblU1.Name = "LblU1"
        Me.LblU1.Size = New System.Drawing.Size(40, 20)
        Me.LblU1.TabIndex = 97
        Me.LblU1.Text = "[PSI]"
        '
        'LblDetalle
        '
        Me.LblDetalle.BackColor = System.Drawing.Color.Transparent
        Me.LblDetalle.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDetalle.ForeColor = System.Drawing.Color.White
        Me.LblDetalle.Location = New System.Drawing.Point(90, 125)
        Me.LblDetalle.Name = "LblDetalle"
        Me.LblDetalle.Size = New System.Drawing.Size(356, 57)
        Me.LblDetalle.TabIndex = 96
        Me.LblDetalle.Text = "-"
        Me.LblDetalle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LblTL3
        '
        Me.LblTL3.AutoSize = True
        Me.LblTL3.BackColor = System.Drawing.Color.Transparent
        Me.LblTL3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTL3.ForeColor = System.Drawing.Color.White
        Me.LblTL3.Location = New System.Drawing.Point(2, 124)
        Me.LblTL3.Name = "LblTL3"
        Me.LblTL3.Size = New System.Drawing.Size(85, 20)
        Me.LblTL3.TabIndex = 95
        Me.LblTL3.Text = "Aclaración:"
        '
        'CbMarchaDeseada
        '
        Me.CbMarchaDeseada.BackColor = System.Drawing.Color.Transparent
        Me.CbMarchaDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbMarchaDeseada.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbMarchaDeseada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbMarchaDeseada.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbMarchaDeseada.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbMarchaDeseada.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbMarchaDeseada.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbMarchaDeseada.ItemHeight = 30
        Me.CbMarchaDeseada.Items.AddRange(New Object() {"2", "3", "4", "5", "6", "7"})
        Me.CbMarchaDeseada.Location = New System.Drawing.Point(205, 60)
        Me.CbMarchaDeseada.Name = "CbMarchaDeseada"
        Me.CbMarchaDeseada.Size = New System.Drawing.Size(200, 36)
        Me.CbMarchaDeseada.TabIndex = 94
        Me.CbMarchaDeseada.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblTL2
        '
        Me.LblTL2.AutoSize = True
        Me.LblTL2.BackColor = System.Drawing.Color.Transparent
        Me.LblTL2.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTL2.ForeColor = System.Drawing.Color.White
        Me.LblTL2.Location = New System.Drawing.Point(2, 68)
        Me.LblTL2.Name = "LblTL2"
        Me.LblTL2.Size = New System.Drawing.Size(199, 20)
        Me.LblTL2.TabIndex = 93
        Me.LblTL2.Text = "Ingrese la marcha deseada:"
        '
        'BtnComenzar
        '
        Me.BtnComenzar.Animated = True
        Me.BtnComenzar.BorderColor = System.Drawing.Color.White
        Me.BtnComenzar.BorderRadius = 5
        Me.BtnComenzar.BorderThickness = 1
        Me.BtnComenzar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnComenzar.FillColor = System.Drawing.Color.Transparent
        Me.BtnComenzar.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnComenzar.ForeColor = System.Drawing.Color.White
        Me.BtnComenzar.ImageAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.BtnComenzar.Location = New System.Drawing.Point(122, 185)
        Me.BtnComenzar.Name = "BtnComenzar"
        Me.BtnComenzar.Size = New System.Drawing.Size(200, 40)
        Me.BtnComenzar.TabIndex = 92
        Me.BtnComenzar.Text = "Comenzar"
        '
        'TbPresion
        '
        Me.TbPresion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbPresion.DefaultText = ""
        Me.TbPresion.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbPresion.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbPresion.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPresion.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPresion.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPresion.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.TbPresion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TbPresion.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPresion.Location = New System.Drawing.Point(205, 5)
        Me.TbPresion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbPresion.Name = "TbPresion"
        Me.TbPresion.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbPresion.PlaceholderText = ""
        Me.TbPresion.SelectedText = ""
        Me.TbPresion.Size = New System.Drawing.Size(200, 35)
        Me.TbPresion.TabIndex = 91
        Me.TbPresion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblTL1
        '
        Me.LblTL1.AutoSize = True
        Me.LblTL1.BackColor = System.Drawing.Color.Transparent
        Me.LblTL1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTL1.ForeColor = System.Drawing.Color.White
        Me.LblTL1.Location = New System.Drawing.Point(2, 12)
        Me.LblTL1.Name = "LblTL1"
        Me.LblTL1.Size = New System.Drawing.Size(197, 20)
        Me.LblTL1.TabIndex = 90
        Me.LblTL1.Text = "Ingrese la presión deseada:"
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblAjustes)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(456, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblAjustes
        '
        Me.LblAjustes.AutoSize = True
        Me.LblAjustes.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblAjustes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblAjustes.Location = New System.Drawing.Point(196, 3)
        Me.LblAjustes.Name = "LblAjustes"
        Me.LblAjustes.Size = New System.Drawing.Size(64, 21)
        Me.LblAjustes.TabIndex = 19
        Me.LblAjustes.Text = "Ajustes"
        '
        'LblTL7
        '
        Me.LblTL7.AutoSize = True
        Me.LblTL7.BackColor = System.Drawing.Color.Transparent
        Me.LblTL7.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTL7.ForeColor = System.Drawing.Color.White
        Me.LblTL7.Location = New System.Drawing.Point(12, 577)
        Me.LblTL7.Name = "LblTL7"
        Me.LblTL7.Size = New System.Drawing.Size(62, 20)
        Me.LblTL7.TabIndex = 79
        Me.LblTL7.Text = "Unidad:"
        '
        'LblUnidad
        '
        Me.LblUnidad.AutoSize = True
        Me.LblUnidad.BackColor = System.Drawing.Color.Transparent
        Me.LblUnidad.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblUnidad.ForeColor = System.Drawing.Color.White
        Me.LblUnidad.Location = New System.Drawing.Point(78, 577)
        Me.LblUnidad.Name = "LblUnidad"
        Me.LblUnidad.Size = New System.Drawing.Size(57, 20)
        Me.LblUnidad.TabIndex = 80
        Me.LblUnidad.Text = "unidad"
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.BtnIdle)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.Controls.Add(Me.Panel2)
        Me.Guna2Panel1.Controls.Add(Me.Panel3)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(12, 315)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(456, 259)
        Me.Guna2Panel1.TabIndex = 79
        '
        'BtnIdle
        '
        Me.BtnIdle.Animated = True
        Me.BtnIdle.BorderColor = System.Drawing.Color.White
        Me.BtnIdle.BorderRadius = 5
        Me.BtnIdle.BorderThickness = 1
        Me.BtnIdle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdle.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdle.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.BtnIdle.ForeColor = System.Drawing.Color.White
        Me.BtnIdle.Location = New System.Drawing.Point(319, 124)
        Me.BtnIdle.Name = "BtnIdle"
        Me.BtnIdle.Size = New System.Drawing.Size(125, 125)
        Me.BtnIdle.TabIndex = 85
        Me.BtnIdle.Text = "IDLE"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblGrafico)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(456, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblGrafico
        '
        Me.LblGrafico.AutoSize = True
        Me.LblGrafico.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblGrafico.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblGrafico.Location = New System.Drawing.Point(197, 3)
        Me.LblGrafico.Name = "LblGrafico"
        Me.LblGrafico.Size = New System.Drawing.Size(62, 21)
        Me.LblGrafico.TabIndex = 19
        Me.LblGrafico.Text = "Gráfico"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.CbMinutos)
        Me.Panel2.Controls.Add(Me.LblTL4)
        Me.Panel2.Location = New System.Drawing.Point(10, 37)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(434, 36)
        Me.Panel2.TabIndex = 91
        '
        'CbMinutos
        '
        Me.CbMinutos.BackColor = System.Drawing.Color.Transparent
        Me.CbMinutos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbMinutos.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbMinutos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbMinutos.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbMinutos.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbMinutos.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbMinutos.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbMinutos.ItemHeight = 30
        Me.CbMinutos.Items.AddRange(New Object() {"3", "5", "10"})
        Me.CbMinutos.Location = New System.Drawing.Point(235, 0)
        Me.CbMinutos.Name = "CbMinutos"
        Me.CbMinutos.Size = New System.Drawing.Size(200, 36)
        Me.CbMinutos.TabIndex = 92
        Me.CbMinutos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblTL4
        '
        Me.LblTL4.AutoSize = True
        Me.LblTL4.BackColor = System.Drawing.Color.Transparent
        Me.LblTL4.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTL4.ForeColor = System.Drawing.Color.White
        Me.LblTL4.Location = New System.Drawing.Point(0, 8)
        Me.LblTL4.Name = "LblTL4"
        Me.LblTL4.Size = New System.Drawing.Size(229, 20)
        Me.LblTL4.TabIndex = 91
        Me.LblTL4.Text = "Ingrese la cantidad de minutos:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.LblU3)
        Me.Panel3.Controls.Add(Me.LblTiempo)
        Me.Panel3.Controls.Add(Me.LblTL6)
        Me.Panel3.Controls.Add(Me.LblU2)
        Me.Panel3.Controls.Add(Me.LblPresion)
        Me.Panel3.Controls.Add(Me.LblTL5)
        Me.Panel3.Location = New System.Drawing.Point(10, 111)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(229, 112)
        Me.Panel3.TabIndex = 92
        '
        'LblU3
        '
        Me.LblU3.AutoSize = True
        Me.LblU3.BackColor = System.Drawing.Color.Transparent
        Me.LblU3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblU3.ForeColor = System.Drawing.Color.White
        Me.LblU3.Location = New System.Drawing.Point(172, 86)
        Me.LblU3.Name = "LblU3"
        Me.LblU3.Size = New System.Drawing.Size(46, 20)
        Me.LblU3.TabIndex = 90
        Me.LblU3.Text = "[SEG]"
        '
        'LblTiempo
        '
        Me.LblTiempo.AutoSize = True
        Me.LblTiempo.BackColor = System.Drawing.Color.Transparent
        Me.LblTiempo.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTiempo.ForeColor = System.Drawing.Color.White
        Me.LblTiempo.Location = New System.Drawing.Point(121, 86)
        Me.LblTiempo.Name = "LblTiempo"
        Me.LblTiempo.Size = New System.Drawing.Size(54, 20)
        Me.LblTiempo.TabIndex = 89
        Me.LblTiempo.Text = "         0"
        '
        'LblTL6
        '
        Me.LblTL6.AutoSize = True
        Me.LblTL6.BackColor = System.Drawing.Color.Transparent
        Me.LblTL6.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTL6.ForeColor = System.Drawing.Color.White
        Me.LblTL6.Location = New System.Drawing.Point(50, 86)
        Me.LblTL6.Name = "LblTL6"
        Me.LblTL6.Size = New System.Drawing.Size(65, 20)
        Me.LblTL6.TabIndex = 88
        Me.LblTL6.Text = "Tiempo:"
        '
        'LblU2
        '
        Me.LblU2.AutoSize = True
        Me.LblU2.BackColor = System.Drawing.Color.Transparent
        Me.LblU2.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblU2.ForeColor = System.Drawing.Color.White
        Me.LblU2.Location = New System.Drawing.Point(172, 7)
        Me.LblU2.Name = "LblU2"
        Me.LblU2.Size = New System.Drawing.Size(40, 20)
        Me.LblU2.TabIndex = 87
        Me.LblU2.Text = "[PSI]"
        '
        'LblPresion
        '
        Me.LblPresion.AutoSize = True
        Me.LblPresion.BackColor = System.Drawing.Color.Transparent
        Me.LblPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPresion.ForeColor = System.Drawing.Color.White
        Me.LblPresion.Location = New System.Drawing.Point(121, 7)
        Me.LblPresion.Name = "LblPresion"
        Me.LblPresion.Size = New System.Drawing.Size(54, 20)
        Me.LblPresion.TabIndex = 86
        Me.LblPresion.Text = "         0"
        '
        'LblTL5
        '
        Me.LblTL5.AutoSize = True
        Me.LblTL5.BackColor = System.Drawing.Color.Transparent
        Me.LblTL5.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTL5.ForeColor = System.Drawing.Color.White
        Me.LblTL5.Location = New System.Drawing.Point(3, 7)
        Me.LblTL5.Name = "LblTL5"
        Me.LblTL5.Size = New System.Drawing.Size(112, 20)
        Me.LblTL5.TabIndex = 85
        Me.LblTL5.Text = "Presión actual:"
        '
        'dataRateTimer
        '
        Me.dataRateTimer.Interval = 1000
        '
        'chartUpdateTimer
        '
        '
        'hScrollBar1
        '
        Me.hScrollBar1.InUpdate = False
        Me.hScrollBar1.LargeChange = 10
        Me.hScrollBar1.Location = New System.Drawing.Point(474, 584)
        Me.hScrollBar1.Name = "hScrollBar1"
        Me.hScrollBar1.ScrollbarSize = 10
        Me.hScrollBar1.Size = New System.Drawing.Size(639, 10)
        Me.hScrollBar1.TabIndex = 81
        '
        'TmrStart
        '
        Me.TmrStart.Interval = 1000
        '
        'WinChartViewer1
        '
        Me.WinChartViewer1.Location = New System.Drawing.Point(474, 44)
        Me.WinChartViewer1.Name = "WinChartViewer1"
        Me.WinChartViewer1.Size = New System.Drawing.Size(639, 550)
        Me.WinChartViewer1.TabIndex = 68
        Me.WinChartViewer1.TabStop = False
        '
        'TmrInit
        '
        Me.TmrInit.Interval = 1
        '
        'TesteoLinea
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1125, 606)
        Me.Controls.Add(Me.hScrollBar1)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.LblUnidad)
        Me.Controls.Add(Me.LblTL7)
        Me.Controls.Add(Me.Guna2Panel3)
        Me.Controls.Add(Me.WinChartViewer1)
        Me.Controls.Add(Me.TitlePanel)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "TesteoLinea"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TesteoLinea"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.Guna2Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.WinChartViewer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents WinChartViewer1 As ChartDirector.WinChartViewer
    Friend WithEvents Guna2Panel3 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents LblUnidad As Label
    Friend WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents dataRateTimer As Timer
    Friend WithEvents chartUpdateTimer As Timer
    Friend WithEvents hScrollBar1 As Guna.UI2.WinForms.Guna2HScrollBar
    Friend WithEvents TmrStart As Timer
    Friend WithEvents TmrInit As Timer
    Public WithEvents Title As Label
    Public WithEvents LblAjustes As Label
    Public WithEvents LblTL7 As Label
    Public WithEvents LblGrafico As Label
    Public WithEvents BtnIdle As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Panel1 As Panel
    Public WithEvents LblU1 As Label
    Public WithEvents LblDetalle As Label
    Public WithEvents LblTL3 As Label
    Public WithEvents CbMarchaDeseada As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblTL2 As Label
    Public WithEvents BtnComenzar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents TbPresion As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents LblTL1 As Label
    Friend WithEvents Panel2 As Panel
    Public WithEvents CbMinutos As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblTL4 As Label
    Friend WithEvents Panel3 As Panel
    Public WithEvents LblU3 As Label
    Public WithEvents LblTiempo As Label
    Public WithEvents LblTL6 As Label
    Public WithEvents LblU2 As Label
    Public WithEvents LblPresion As Label
    Public WithEvents LblTL5 As Label
End Class
