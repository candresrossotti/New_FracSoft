﻿Public Class SetRPM

    Private MyEngine As Dictionary(Of String, Object)
    Private Sub SetRPM_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        MyEngine = FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString())

        TbRPM.Select()
        TbRPM.Text = FracData.DblVFrac("RPMDeseada_" + currentFrac.ToString())
        CenterEverything()
    End Sub

    Public Sub CenterEverything()
        CenterObject(Title)
        LeftAlign(LblTB, LblTB.Width + 6)
        LeftAlign(TbRPM, 12 + LblTB.Width + TbRPM.Width)
        CenterObject(BtnAceptar)
        RPMPanel.Width = 12 + LblTB.Width + 6 + TbRPM.Width + 6
        CenterObject(RPMPanel)
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If Not String.IsNullOrEmpty(TbRPM.Text) Then
            If IsInteger(TbRPM.Text) Then
                Dim RPM As Integer
                If FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString())("LimitRPM") = True Then
                    RPM = Clamp(MyEngine("RPM_LMin"), MyEngine("RPM_LMax"), GetInteger(TbRPM.Text))
                Else
                    RPM = Clamp(MyEngine("MinRPM"), MyEngine("MaxRPM"), GetInteger(TbRPM.Text))
                End If
                ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), RPM, currentFrac)
                Me.DialogResult = DialogResult.OK
            End If
        End If
        Me.Close()
    End Sub
End Class