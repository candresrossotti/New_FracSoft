﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FracSelect
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnManualReload = New Guna.UI2.WinForms.Guna2Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtnReload = New Guna.UI2.WinForms.Guna2Button()
        Me.Title = New System.Windows.Forms.Label()
        Me.CloseFracSelect = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.MainPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2Separator21 = New Guna.UI2.WinForms.Guna2Separator()
        Me.PanelFrac23 = New System.Windows.Forms.Panel()
        Me.BtnFrac23 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac23Pump = New System.Windows.Forms.Label()
        Me.LblFrac23Trans = New System.Windows.Forms.Label()
        Me.LblFrac23Engine = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.PanelFrac21 = New System.Windows.Forms.Panel()
        Me.BtnFrac21 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac21Pump = New System.Windows.Forms.Label()
        Me.LblFrac21Trans = New System.Windows.Forms.Label()
        Me.LblFrac21Engine = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.Guna2Separator22 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator19 = New Guna.UI2.WinForms.Guna2Separator()
        Me.PanelFrac24 = New System.Windows.Forms.Panel()
        Me.BtnFrac24 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac24Pump = New System.Windows.Forms.Label()
        Me.LblFrac24Trans = New System.Windows.Forms.Label()
        Me.LblFrac24Engine = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.PanelFrac22 = New System.Windows.Forms.Panel()
        Me.BtnFrac22 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac22Pump = New System.Windows.Forms.Label()
        Me.LblFrac22Trans = New System.Windows.Forms.Label()
        Me.LblFrac22Engine = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Guna2Separator20 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator14 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator15 = New Guna.UI2.WinForms.Guna2Separator()
        Me.PanelFrac19 = New System.Windows.Forms.Panel()
        Me.BtnFrac19 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac19Pump = New System.Windows.Forms.Label()
        Me.LblFrac19Trans = New System.Windows.Forms.Label()
        Me.LblFrac19Engine = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.PanelFrac17 = New System.Windows.Forms.Panel()
        Me.BtnFrac17 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac17Pump = New System.Windows.Forms.Label()
        Me.LblFrac17Trans = New System.Windows.Forms.Label()
        Me.LblFrac17Engine = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.PanelFrac13 = New System.Windows.Forms.Panel()
        Me.BtnFrac13 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac13Pump = New System.Windows.Forms.Label()
        Me.LblFrac13Trans = New System.Windows.Forms.Label()
        Me.LblFrac13Engine = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.PanelFrac15 = New System.Windows.Forms.Panel()
        Me.BtnFrac15 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac15Pump = New System.Windows.Forms.Label()
        Me.LblFrac15Trans = New System.Windows.Forms.Label()
        Me.LblFrac15Engine = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.PanelFrac11 = New System.Windows.Forms.Panel()
        Me.BtnFrac11 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac11Pump = New System.Windows.Forms.Label()
        Me.LblFrac11Trans = New System.Windows.Forms.Label()
        Me.LblFrac11Engine = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Guna2Separator16 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator17 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator18 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator13 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator9 = New Guna.UI2.WinForms.Guna2Separator()
        Me.PanelFrac20 = New System.Windows.Forms.Panel()
        Me.BtnFrac20 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac20Pump = New System.Windows.Forms.Label()
        Me.LblFrac20Trans = New System.Windows.Forms.Label()
        Me.LblFrac20Engine = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.PanelFrac18 = New System.Windows.Forms.Panel()
        Me.BtnFrac18 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac18Pump = New System.Windows.Forms.Label()
        Me.LblFrac18Trans = New System.Windows.Forms.Label()
        Me.LblFrac18Engine = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.PanelFrac14 = New System.Windows.Forms.Panel()
        Me.BtnFrac14 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac14Pump = New System.Windows.Forms.Label()
        Me.LblFrac14Trans = New System.Windows.Forms.Label()
        Me.LblFrac14Engine = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.PanelFrac16 = New System.Windows.Forms.Panel()
        Me.BtnFrac16 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac16Pump = New System.Windows.Forms.Label()
        Me.LblFrac16Trans = New System.Windows.Forms.Label()
        Me.LblFrac16Engine = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.PanelFrac12 = New System.Windows.Forms.Panel()
        Me.BtnFrac12 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac12Pump = New System.Windows.Forms.Label()
        Me.LblFrac12Trans = New System.Windows.Forms.Label()
        Me.LblFrac12Engine = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Guna2Separator10 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator11 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator12 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator8 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator7 = New Guna.UI2.WinForms.Guna2Separator()
        Me.PanelFrac10 = New System.Windows.Forms.Panel()
        Me.BtnFrac10 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac10Pump = New System.Windows.Forms.Label()
        Me.LblFrac10Trans = New System.Windows.Forms.Label()
        Me.LblFrac10Engine = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PanelFrac9 = New System.Windows.Forms.Panel()
        Me.BtnFrac9 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac9Pump = New System.Windows.Forms.Label()
        Me.LblFrac9Trans = New System.Windows.Forms.Label()
        Me.LblFrac9Engine = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.PanelFrac5 = New System.Windows.Forms.Panel()
        Me.BtnFrac5 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac5Pump = New System.Windows.Forms.Label()
        Me.LblFrac5Trans = New System.Windows.Forms.Label()
        Me.LblFrac5Engine = New System.Windows.Forms.Label()
        Me.LblFSPump5 = New System.Windows.Forms.Label()
        Me.LblFSTrans5 = New System.Windows.Forms.Label()
        Me.LblFSEngine5 = New System.Windows.Forms.Label()
        Me.PanelFrac8 = New System.Windows.Forms.Panel()
        Me.BtnFrac8 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac8Pump = New System.Windows.Forms.Label()
        Me.LblFrac8Trans = New System.Windows.Forms.Label()
        Me.LblFrac8Engine = New System.Windows.Forms.Label()
        Me.LblFSPump8 = New System.Windows.Forms.Label()
        Me.LblFSTrans8 = New System.Windows.Forms.Label()
        Me.LblFSEngine8 = New System.Windows.Forms.Label()
        Me.PanelFrac7 = New System.Windows.Forms.Panel()
        Me.BtnFrac7 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac7Pump = New System.Windows.Forms.Label()
        Me.LblFrac7Trans = New System.Windows.Forms.Label()
        Me.LblFrac7Engine = New System.Windows.Forms.Label()
        Me.LblFSPump7 = New System.Windows.Forms.Label()
        Me.LblFSTrans7 = New System.Windows.Forms.Label()
        Me.LblFSEngine7 = New System.Windows.Forms.Label()
        Me.PanelFrac4 = New System.Windows.Forms.Panel()
        Me.BtnFrac4 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac4Pump = New System.Windows.Forms.Label()
        Me.LblFrac4Trans = New System.Windows.Forms.Label()
        Me.LblFrac4Engine = New System.Windows.Forms.Label()
        Me.LblFSPump4 = New System.Windows.Forms.Label()
        Me.LblFSTrans4 = New System.Windows.Forms.Label()
        Me.LblFSEngine4 = New System.Windows.Forms.Label()
        Me.PanelFrac6 = New System.Windows.Forms.Panel()
        Me.BtnFrac6 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac6Pump = New System.Windows.Forms.Label()
        Me.LblFrac6Trans = New System.Windows.Forms.Label()
        Me.LblFrac6Engine = New System.Windows.Forms.Label()
        Me.LblFSPump6 = New System.Windows.Forms.Label()
        Me.LblFSTrans6 = New System.Windows.Forms.Label()
        Me.LblFSEngine6 = New System.Windows.Forms.Label()
        Me.PanelFrac3 = New System.Windows.Forms.Panel()
        Me.BtnFrac3 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac3Pump = New System.Windows.Forms.Label()
        Me.LblFrac3Trans = New System.Windows.Forms.Label()
        Me.LblFrac3Engine = New System.Windows.Forms.Label()
        Me.LblFSPump3 = New System.Windows.Forms.Label()
        Me.LblFSTrans3 = New System.Windows.Forms.Label()
        Me.LblFSEngine3 = New System.Windows.Forms.Label()
        Me.PanelFrac2 = New System.Windows.Forms.Panel()
        Me.BtnFrac2 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac2Pump = New System.Windows.Forms.Label()
        Me.LblFrac2Trans = New System.Windows.Forms.Label()
        Me.LblFrac2Engine = New System.Windows.Forms.Label()
        Me.LblFSPump2 = New System.Windows.Forms.Label()
        Me.LblFSTrans2 = New System.Windows.Forms.Label()
        Me.LblFSEngine2 = New System.Windows.Forms.Label()
        Me.PanelFrac1 = New System.Windows.Forms.Panel()
        Me.BtnFrac1 = New Guna.UI2.WinForms.Guna2Button()
        Me.LblFrac1Pump = New System.Windows.Forms.Label()
        Me.LblFrac1Trans = New System.Windows.Forms.Label()
        Me.LblFrac1Engine = New System.Windows.Forms.Label()
        Me.LblFSPump1 = New System.Windows.Forms.Label()
        Me.LblFSTrans1 = New System.Windows.Forms.Label()
        Me.LblFSEngine1 = New System.Windows.Forms.Label()
        Me.Guna2Separator5 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator6 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator4 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator3 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2Separator2 = New Guna.UI2.WinForms.Guna2Separator()
        Me.Guna2VSeparator1 = New Guna.UI2.WinForms.Guna2VSeparator()
        Me.Guna2Separator1 = New Guna.UI2.WinForms.Guna2Separator()
        Me.PbInicio = New System.Windows.Forms.ProgressBar()
        Me.TitlePanel.SuspendLayout()
        Me.MainPanel.SuspendLayout()
        Me.PanelFrac23.SuspendLayout()
        Me.PanelFrac21.SuspendLayout()
        Me.PanelFrac24.SuspendLayout()
        Me.PanelFrac22.SuspendLayout()
        Me.PanelFrac19.SuspendLayout()
        Me.PanelFrac17.SuspendLayout()
        Me.PanelFrac13.SuspendLayout()
        Me.PanelFrac15.SuspendLayout()
        Me.PanelFrac11.SuspendLayout()
        Me.PanelFrac20.SuspendLayout()
        Me.PanelFrac18.SuspendLayout()
        Me.PanelFrac14.SuspendLayout()
        Me.PanelFrac16.SuspendLayout()
        Me.PanelFrac12.SuspendLayout()
        Me.PanelFrac10.SuspendLayout()
        Me.PanelFrac9.SuspendLayout()
        Me.PanelFrac5.SuspendLayout()
        Me.PanelFrac8.SuspendLayout()
        Me.PanelFrac7.SuspendLayout()
        Me.PanelFrac4.SuspendLayout()
        Me.PanelFrac6.SuspendLayout()
        Me.PanelFrac3.SuspendLayout()
        Me.PanelFrac2.SuspendLayout()
        Me.PanelFrac1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.BtnManualReload)
        Me.TitlePanel.Controls.Add(Me.Label2)
        Me.TitlePanel.Controls.Add(Me.BtnReload)
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.CloseFracSelect)
        Me.TitlePanel.Controls.Add(Me.Label1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(800, 38)
        Me.TitlePanel.TabIndex = 1
        '
        'BtnManualReload
        '
        Me.BtnManualReload.BackColor = System.Drawing.Color.Transparent
        Me.BtnManualReload.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.BtnManualReload.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.BtnManualReload.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.BtnManualReload.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.BtnManualReload.FillColor = System.Drawing.Color.Transparent
        Me.BtnManualReload.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnManualReload.ForeColor = System.Drawing.Color.White
        Me.BtnManualReload.Image = Global.sFRAC.My.Resources.Resources.reload
        Me.BtnManualReload.ImageOffset = New System.Drawing.Point(0, 1)
        Me.BtnManualReload.ImageSize = New System.Drawing.Size(36, 36)
        Me.BtnManualReload.Location = New System.Drawing.Point(47, 0)
        Me.BtnManualReload.Name = "BtnManualReload"
        Me.BtnManualReload.Size = New System.Drawing.Size(38, 38)
        Me.BtnManualReload.TabIndex = 34
        Me.BtnManualReload.UseTransparentBackground = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(54, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 22)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "M"
        '
        'BtnReload
        '
        Me.BtnReload.BackColor = System.Drawing.Color.Transparent
        Me.BtnReload.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.BtnReload.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.BtnReload.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.BtnReload.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.BtnReload.FillColor = System.Drawing.Color.Transparent
        Me.BtnReload.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnReload.ForeColor = System.Drawing.Color.White
        Me.BtnReload.Image = Global.sFRAC.My.Resources.Resources.reload
        Me.BtnReload.ImageOffset = New System.Drawing.Point(0, 1)
        Me.BtnReload.ImageSize = New System.Drawing.Size(36, 36)
        Me.BtnReload.Location = New System.Drawing.Point(3, 0)
        Me.BtnReload.Name = "BtnReload"
        Me.BtnReload.Size = New System.Drawing.Size(38, 38)
        Me.BtnReload.TabIndex = 30
        Me.BtnReload.UseTransparentBackground = True
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(256, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(288, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Selección de Fracturador"
        '
        'CloseFracSelect
        '
        Me.CloseFracSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseFracSelect.BackColor = System.Drawing.Color.Transparent
        Me.CloseFracSelect.Cursor = System.Windows.Forms.Cursors.Default
        Me.CloseFracSelect.FillColor = System.Drawing.Color.Transparent
        Me.CloseFracSelect.IconColor = System.Drawing.Color.White
        Me.CloseFracSelect.Location = New System.Drawing.Point(752, 3)
        Me.CloseFracSelect.Name = "CloseFracSelect"
        Me.CloseFracSelect.Size = New System.Drawing.Size(45, 29)
        Me.CloseFracSelect.TabIndex = 0
        Me.CloseFracSelect.UseTransparentBackground = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(12, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 22)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "A"
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.TitlePanel
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.Title
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'MainPanel
        '
        Me.MainPanel.Controls.Add(Me.Guna2Separator21)
        Me.MainPanel.Controls.Add(Me.PanelFrac23)
        Me.MainPanel.Controls.Add(Me.PanelFrac21)
        Me.MainPanel.Controls.Add(Me.Guna2Separator22)
        Me.MainPanel.Controls.Add(Me.Guna2Separator19)
        Me.MainPanel.Controls.Add(Me.PanelFrac24)
        Me.MainPanel.Controls.Add(Me.PanelFrac22)
        Me.MainPanel.Controls.Add(Me.Guna2Separator20)
        Me.MainPanel.Controls.Add(Me.Guna2Separator14)
        Me.MainPanel.Controls.Add(Me.Guna2Separator15)
        Me.MainPanel.Controls.Add(Me.PanelFrac19)
        Me.MainPanel.Controls.Add(Me.PanelFrac17)
        Me.MainPanel.Controls.Add(Me.PanelFrac13)
        Me.MainPanel.Controls.Add(Me.PanelFrac15)
        Me.MainPanel.Controls.Add(Me.PanelFrac11)
        Me.MainPanel.Controls.Add(Me.Guna2Separator16)
        Me.MainPanel.Controls.Add(Me.Guna2Separator17)
        Me.MainPanel.Controls.Add(Me.Guna2Separator18)
        Me.MainPanel.Controls.Add(Me.Guna2Separator13)
        Me.MainPanel.Controls.Add(Me.Guna2Separator9)
        Me.MainPanel.Controls.Add(Me.PanelFrac20)
        Me.MainPanel.Controls.Add(Me.PanelFrac18)
        Me.MainPanel.Controls.Add(Me.PanelFrac14)
        Me.MainPanel.Controls.Add(Me.PanelFrac16)
        Me.MainPanel.Controls.Add(Me.PanelFrac12)
        Me.MainPanel.Controls.Add(Me.Guna2Separator10)
        Me.MainPanel.Controls.Add(Me.Guna2Separator11)
        Me.MainPanel.Controls.Add(Me.Guna2Separator12)
        Me.MainPanel.Controls.Add(Me.Guna2Separator8)
        Me.MainPanel.Controls.Add(Me.Guna2Separator7)
        Me.MainPanel.Controls.Add(Me.PanelFrac10)
        Me.MainPanel.Controls.Add(Me.PanelFrac9)
        Me.MainPanel.Controls.Add(Me.PanelFrac5)
        Me.MainPanel.Controls.Add(Me.PanelFrac8)
        Me.MainPanel.Controls.Add(Me.PanelFrac7)
        Me.MainPanel.Controls.Add(Me.PanelFrac4)
        Me.MainPanel.Controls.Add(Me.PanelFrac6)
        Me.MainPanel.Controls.Add(Me.PanelFrac3)
        Me.MainPanel.Controls.Add(Me.PanelFrac2)
        Me.MainPanel.Controls.Add(Me.PanelFrac1)
        Me.MainPanel.Controls.Add(Me.Guna2Separator5)
        Me.MainPanel.Controls.Add(Me.Guna2Separator6)
        Me.MainPanel.Controls.Add(Me.Guna2Separator4)
        Me.MainPanel.Controls.Add(Me.Guna2Separator3)
        Me.MainPanel.Controls.Add(Me.Guna2Separator2)
        Me.MainPanel.Controls.Add(Me.Guna2VSeparator1)
        Me.MainPanel.Controls.Add(Me.Guna2Separator1)
        Me.MainPanel.Location = New System.Drawing.Point(12, 44)
        Me.MainPanel.Name = "MainPanel"
        Me.MainPanel.Size = New System.Drawing.Size(778, 497)
        Me.MainPanel.TabIndex = 100
        '
        'Guna2Separator21
        '
        Me.Guna2Separator21.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator21.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator21.Location = New System.Drawing.Point(4, 1101)
        Me.Guna2Separator21.Name = "Guna2Separator21"
        Me.Guna2Separator21.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator21.TabIndex = 146
        Me.Guna2Separator21.UseTransparentBackground = True
        '
        'PanelFrac23
        '
        Me.PanelFrac23.Controls.Add(Me.BtnFrac23)
        Me.PanelFrac23.Controls.Add(Me.LblFrac23Pump)
        Me.PanelFrac23.Controls.Add(Me.LblFrac23Trans)
        Me.PanelFrac23.Controls.Add(Me.LblFrac23Engine)
        Me.PanelFrac23.Controls.Add(Me.Label88)
        Me.PanelFrac23.Controls.Add(Me.Label89)
        Me.PanelFrac23.Controls.Add(Me.Label90)
        Me.PanelFrac23.Location = New System.Drawing.Point(4, 1110)
        Me.PanelFrac23.Name = "PanelFrac23"
        Me.PanelFrac23.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac23.TabIndex = 145
        Me.PanelFrac23.Tag = "22"
        Me.PanelFrac23.Visible = False
        '
        'BtnFrac23
        '
        Me.BtnFrac23.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac23.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac23.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac23.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac23.ForeColor = System.Drawing.Color.White
        Me.BtnFrac23.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac23.Name = "BtnFrac23"
        Me.BtnFrac23.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac23.TabIndex = 18
        Me.BtnFrac23.Tag = "22"
        Me.BtnFrac23.Text = "Fracturador23"
        Me.BtnFrac23.UseTransparentBackground = True
        '
        'LblFrac23Pump
        '
        Me.LblFrac23Pump.AutoSize = True
        Me.LblFrac23Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac23Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac23Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac23Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac23Pump.Name = "LblFrac23Pump"
        Me.LblFrac23Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac23Pump.TabIndex = 29
        Me.LblFrac23Pump.Text = "QWS2500"
        '
        'LblFrac23Trans
        '
        Me.LblFrac23Trans.AutoSize = True
        Me.LblFrac23Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac23Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac23Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac23Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac23Trans.Name = "LblFrac23Trans"
        Me.LblFrac23Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac23Trans.TabIndex = 28
        Me.LblFrac23Trans.Text = "ZF-8 TX"
        '
        'LblFrac23Engine
        '
        Me.LblFrac23Engine.AutoSize = True
        Me.LblFrac23Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac23Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac23Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac23Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac23Engine.Name = "LblFrac23Engine"
        Me.LblFrac23Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac23Engine.TabIndex = 27
        Me.LblFrac23Engine.Text = "D9620"
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.BackColor = System.Drawing.Color.Transparent
        Me.Label88.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label88.ForeColor = System.Drawing.Color.White
        Me.Label88.Location = New System.Drawing.Point(254, 63)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(55, 16)
        Me.Label88.TabIndex = 26
        Me.Label88.Text = "Bomba:"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.BackColor = System.Drawing.Color.Transparent
        Me.Label89.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label89.ForeColor = System.Drawing.Color.White
        Me.Label89.Location = New System.Drawing.Point(115, 63)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(75, 16)
        Me.Label89.TabIndex = 25
        Me.Label89.Text = "Trasmisión:"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.BackColor = System.Drawing.Color.Transparent
        Me.Label90.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label90.ForeColor = System.Drawing.Color.White
        Me.Label90.Location = New System.Drawing.Point(-3, 63)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(47, 16)
        Me.Label90.TabIndex = 24
        Me.Label90.Text = "Motor:"
        '
        'PanelFrac21
        '
        Me.PanelFrac21.Controls.Add(Me.BtnFrac21)
        Me.PanelFrac21.Controls.Add(Me.LblFrac21Pump)
        Me.PanelFrac21.Controls.Add(Me.LblFrac21Trans)
        Me.PanelFrac21.Controls.Add(Me.LblFrac21Engine)
        Me.PanelFrac21.Controls.Add(Me.Label94)
        Me.PanelFrac21.Controls.Add(Me.Label95)
        Me.PanelFrac21.Controls.Add(Me.Label96)
        Me.PanelFrac21.Location = New System.Drawing.Point(4, 1014)
        Me.PanelFrac21.Name = "PanelFrac21"
        Me.PanelFrac21.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac21.TabIndex = 144
        Me.PanelFrac21.Tag = "20"
        Me.PanelFrac21.Visible = False
        '
        'BtnFrac21
        '
        Me.BtnFrac21.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac21.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac21.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac21.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac21.ForeColor = System.Drawing.Color.White
        Me.BtnFrac21.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac21.Name = "BtnFrac21"
        Me.BtnFrac21.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac21.TabIndex = 18
        Me.BtnFrac21.Tag = "20"
        Me.BtnFrac21.Text = "Fracturador21"
        Me.BtnFrac21.UseTransparentBackground = True
        '
        'LblFrac21Pump
        '
        Me.LblFrac21Pump.AutoSize = True
        Me.LblFrac21Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac21Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac21Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac21Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac21Pump.Name = "LblFrac21Pump"
        Me.LblFrac21Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac21Pump.TabIndex = 29
        Me.LblFrac21Pump.Text = "QWS2500"
        '
        'LblFrac21Trans
        '
        Me.LblFrac21Trans.AutoSize = True
        Me.LblFrac21Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac21Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac21Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac21Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac21Trans.Name = "LblFrac21Trans"
        Me.LblFrac21Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac21Trans.TabIndex = 28
        Me.LblFrac21Trans.Text = "ZF-8 TX"
        '
        'LblFrac21Engine
        '
        Me.LblFrac21Engine.AutoSize = True
        Me.LblFrac21Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac21Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac21Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac21Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac21Engine.Name = "LblFrac21Engine"
        Me.LblFrac21Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac21Engine.TabIndex = 27
        Me.LblFrac21Engine.Text = "D9620"
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.BackColor = System.Drawing.Color.Transparent
        Me.Label94.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label94.ForeColor = System.Drawing.Color.White
        Me.Label94.Location = New System.Drawing.Point(254, 63)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(55, 16)
        Me.Label94.TabIndex = 26
        Me.Label94.Text = "Bomba:"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.BackColor = System.Drawing.Color.Transparent
        Me.Label95.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label95.ForeColor = System.Drawing.Color.White
        Me.Label95.Location = New System.Drawing.Point(115, 63)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(75, 16)
        Me.Label95.TabIndex = 25
        Me.Label95.Text = "Trasmisión:"
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.BackColor = System.Drawing.Color.Transparent
        Me.Label96.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label96.ForeColor = System.Drawing.Color.White
        Me.Label96.Location = New System.Drawing.Point(-3, 63)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(47, 16)
        Me.Label96.TabIndex = 24
        Me.Label96.Text = "Motor:"
        '
        'Guna2Separator22
        '
        Me.Guna2Separator22.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator22.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator22.Location = New System.Drawing.Point(4, 998)
        Me.Guna2Separator22.Name = "Guna2Separator22"
        Me.Guna2Separator22.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator22.TabIndex = 143
        Me.Guna2Separator22.UseTransparentBackground = True
        '
        'Guna2Separator19
        '
        Me.Guna2Separator19.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator19.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator19.Location = New System.Drawing.Point(398, 1101)
        Me.Guna2Separator19.Name = "Guna2Separator19"
        Me.Guna2Separator19.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator19.TabIndex = 142
        Me.Guna2Separator19.UseTransparentBackground = True
        '
        'PanelFrac24
        '
        Me.PanelFrac24.Controls.Add(Me.BtnFrac24)
        Me.PanelFrac24.Controls.Add(Me.LblFrac24Pump)
        Me.PanelFrac24.Controls.Add(Me.LblFrac24Trans)
        Me.PanelFrac24.Controls.Add(Me.LblFrac24Engine)
        Me.PanelFrac24.Controls.Add(Me.Label76)
        Me.PanelFrac24.Controls.Add(Me.Label77)
        Me.PanelFrac24.Controls.Add(Me.Label78)
        Me.PanelFrac24.Location = New System.Drawing.Point(398, 1110)
        Me.PanelFrac24.Name = "PanelFrac24"
        Me.PanelFrac24.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac24.TabIndex = 141
        Me.PanelFrac24.Tag = "23"
        Me.PanelFrac24.Visible = False
        '
        'BtnFrac24
        '
        Me.BtnFrac24.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac24.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac24.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac24.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac24.ForeColor = System.Drawing.Color.White
        Me.BtnFrac24.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac24.Name = "BtnFrac24"
        Me.BtnFrac24.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac24.TabIndex = 18
        Me.BtnFrac24.Tag = "23"
        Me.BtnFrac24.Text = "Fracturador24"
        Me.BtnFrac24.UseTransparentBackground = True
        '
        'LblFrac24Pump
        '
        Me.LblFrac24Pump.AutoSize = True
        Me.LblFrac24Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac24Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac24Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac24Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac24Pump.Name = "LblFrac24Pump"
        Me.LblFrac24Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac24Pump.TabIndex = 29
        Me.LblFrac24Pump.Text = "QWS2500"
        '
        'LblFrac24Trans
        '
        Me.LblFrac24Trans.AutoSize = True
        Me.LblFrac24Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac24Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac24Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac24Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac24Trans.Name = "LblFrac24Trans"
        Me.LblFrac24Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac24Trans.TabIndex = 28
        Me.LblFrac24Trans.Text = "ZF-8 TX"
        '
        'LblFrac24Engine
        '
        Me.LblFrac24Engine.AutoSize = True
        Me.LblFrac24Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac24Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac24Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac24Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac24Engine.Name = "LblFrac24Engine"
        Me.LblFrac24Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac24Engine.TabIndex = 27
        Me.LblFrac24Engine.Text = "D9620"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.BackColor = System.Drawing.Color.Transparent
        Me.Label76.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label76.ForeColor = System.Drawing.Color.White
        Me.Label76.Location = New System.Drawing.Point(254, 63)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(55, 16)
        Me.Label76.TabIndex = 26
        Me.Label76.Text = "Bomba:"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.BackColor = System.Drawing.Color.Transparent
        Me.Label77.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label77.ForeColor = System.Drawing.Color.White
        Me.Label77.Location = New System.Drawing.Point(115, 63)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(75, 16)
        Me.Label77.TabIndex = 25
        Me.Label77.Text = "Trasmisión:"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.BackColor = System.Drawing.Color.Transparent
        Me.Label78.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label78.ForeColor = System.Drawing.Color.White
        Me.Label78.Location = New System.Drawing.Point(-3, 63)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(47, 16)
        Me.Label78.TabIndex = 24
        Me.Label78.Text = "Motor:"
        '
        'PanelFrac22
        '
        Me.PanelFrac22.Controls.Add(Me.BtnFrac22)
        Me.PanelFrac22.Controls.Add(Me.LblFrac22Pump)
        Me.PanelFrac22.Controls.Add(Me.LblFrac22Trans)
        Me.PanelFrac22.Controls.Add(Me.LblFrac22Engine)
        Me.PanelFrac22.Controls.Add(Me.Label82)
        Me.PanelFrac22.Controls.Add(Me.Label83)
        Me.PanelFrac22.Controls.Add(Me.Label84)
        Me.PanelFrac22.Location = New System.Drawing.Point(398, 1014)
        Me.PanelFrac22.Name = "PanelFrac22"
        Me.PanelFrac22.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac22.TabIndex = 140
        Me.PanelFrac22.Tag = "21"
        Me.PanelFrac22.Visible = False
        '
        'BtnFrac22
        '
        Me.BtnFrac22.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac22.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac22.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac22.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac22.ForeColor = System.Drawing.Color.White
        Me.BtnFrac22.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac22.Name = "BtnFrac22"
        Me.BtnFrac22.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac22.TabIndex = 18
        Me.BtnFrac22.Tag = "21"
        Me.BtnFrac22.Text = "Fracturador22"
        Me.BtnFrac22.UseTransparentBackground = True
        '
        'LblFrac22Pump
        '
        Me.LblFrac22Pump.AutoSize = True
        Me.LblFrac22Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac22Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac22Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac22Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac22Pump.Name = "LblFrac22Pump"
        Me.LblFrac22Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac22Pump.TabIndex = 29
        Me.LblFrac22Pump.Text = "QWS2500"
        '
        'LblFrac22Trans
        '
        Me.LblFrac22Trans.AutoSize = True
        Me.LblFrac22Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac22Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac22Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac22Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac22Trans.Name = "LblFrac22Trans"
        Me.LblFrac22Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac22Trans.TabIndex = 28
        Me.LblFrac22Trans.Text = "ZF-8 TX"
        '
        'LblFrac22Engine
        '
        Me.LblFrac22Engine.AutoSize = True
        Me.LblFrac22Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac22Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac22Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac22Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac22Engine.Name = "LblFrac22Engine"
        Me.LblFrac22Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac22Engine.TabIndex = 27
        Me.LblFrac22Engine.Text = "D9620"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.BackColor = System.Drawing.Color.Transparent
        Me.Label82.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label82.ForeColor = System.Drawing.Color.White
        Me.Label82.Location = New System.Drawing.Point(254, 63)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(55, 16)
        Me.Label82.TabIndex = 26
        Me.Label82.Text = "Bomba:"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.BackColor = System.Drawing.Color.Transparent
        Me.Label83.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label83.ForeColor = System.Drawing.Color.White
        Me.Label83.Location = New System.Drawing.Point(115, 63)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(75, 16)
        Me.Label83.TabIndex = 25
        Me.Label83.Text = "Trasmisión:"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.BackColor = System.Drawing.Color.Transparent
        Me.Label84.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label84.ForeColor = System.Drawing.Color.White
        Me.Label84.Location = New System.Drawing.Point(-3, 63)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(47, 16)
        Me.Label84.TabIndex = 24
        Me.Label84.Text = "Motor:"
        '
        'Guna2Separator20
        '
        Me.Guna2Separator20.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator20.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator20.Location = New System.Drawing.Point(398, 998)
        Me.Guna2Separator20.Name = "Guna2Separator20"
        Me.Guna2Separator20.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator20.TabIndex = 139
        Me.Guna2Separator20.UseTransparentBackground = True
        '
        'Guna2Separator14
        '
        Me.Guna2Separator14.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator14.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator14.Location = New System.Drawing.Point(4, 497)
        Me.Guna2Separator14.Name = "Guna2Separator14"
        Me.Guna2Separator14.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator14.TabIndex = 138
        Me.Guna2Separator14.UseTransparentBackground = True
        '
        'Guna2Separator15
        '
        Me.Guna2Separator15.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator15.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator15.Location = New System.Drawing.Point(4, 902)
        Me.Guna2Separator15.Name = "Guna2Separator15"
        Me.Guna2Separator15.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator15.TabIndex = 137
        Me.Guna2Separator15.UseTransparentBackground = True
        '
        'PanelFrac19
        '
        Me.PanelFrac19.Controls.Add(Me.BtnFrac19)
        Me.PanelFrac19.Controls.Add(Me.LblFrac19Pump)
        Me.PanelFrac19.Controls.Add(Me.LblFrac19Trans)
        Me.PanelFrac19.Controls.Add(Me.LblFrac19Engine)
        Me.PanelFrac19.Controls.Add(Me.Label46)
        Me.PanelFrac19.Controls.Add(Me.Label47)
        Me.PanelFrac19.Controls.Add(Me.Label48)
        Me.PanelFrac19.Location = New System.Drawing.Point(4, 911)
        Me.PanelFrac19.Name = "PanelFrac19"
        Me.PanelFrac19.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac19.TabIndex = 136
        Me.PanelFrac19.Tag = "18"
        Me.PanelFrac19.Visible = False
        '
        'BtnFrac19
        '
        Me.BtnFrac19.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac19.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac19.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac19.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac19.ForeColor = System.Drawing.Color.White
        Me.BtnFrac19.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac19.Name = "BtnFrac19"
        Me.BtnFrac19.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac19.TabIndex = 18
        Me.BtnFrac19.Tag = "18"
        Me.BtnFrac19.Text = "Fracturador19"
        Me.BtnFrac19.UseTransparentBackground = True
        '
        'LblFrac19Pump
        '
        Me.LblFrac19Pump.AutoSize = True
        Me.LblFrac19Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac19Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac19Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac19Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac19Pump.Name = "LblFrac19Pump"
        Me.LblFrac19Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac19Pump.TabIndex = 29
        Me.LblFrac19Pump.Text = "QWS2500"
        '
        'LblFrac19Trans
        '
        Me.LblFrac19Trans.AutoSize = True
        Me.LblFrac19Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac19Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac19Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac19Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac19Trans.Name = "LblFrac19Trans"
        Me.LblFrac19Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac19Trans.TabIndex = 28
        Me.LblFrac19Trans.Text = "ZF-8 TX"
        '
        'LblFrac19Engine
        '
        Me.LblFrac19Engine.AutoSize = True
        Me.LblFrac19Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac19Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac19Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac19Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac19Engine.Name = "LblFrac19Engine"
        Me.LblFrac19Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac19Engine.TabIndex = 27
        Me.LblFrac19Engine.Text = "D9620"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.BackColor = System.Drawing.Color.Transparent
        Me.Label46.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label46.ForeColor = System.Drawing.Color.White
        Me.Label46.Location = New System.Drawing.Point(254, 63)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(55, 16)
        Me.Label46.TabIndex = 26
        Me.Label46.Text = "Bomba:"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.Transparent
        Me.Label47.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label47.ForeColor = System.Drawing.Color.White
        Me.Label47.Location = New System.Drawing.Point(115, 63)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(75, 16)
        Me.Label47.TabIndex = 25
        Me.Label47.Text = "Trasmisión:"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        Me.Label48.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label48.ForeColor = System.Drawing.Color.White
        Me.Label48.Location = New System.Drawing.Point(-3, 63)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(47, 16)
        Me.Label48.TabIndex = 24
        Me.Label48.Text = "Motor:"
        '
        'PanelFrac17
        '
        Me.PanelFrac17.Controls.Add(Me.BtnFrac17)
        Me.PanelFrac17.Controls.Add(Me.LblFrac17Pump)
        Me.PanelFrac17.Controls.Add(Me.LblFrac17Trans)
        Me.PanelFrac17.Controls.Add(Me.LblFrac17Engine)
        Me.PanelFrac17.Controls.Add(Me.Label52)
        Me.PanelFrac17.Controls.Add(Me.Label53)
        Me.PanelFrac17.Controls.Add(Me.Label54)
        Me.PanelFrac17.Location = New System.Drawing.Point(4, 815)
        Me.PanelFrac17.Name = "PanelFrac17"
        Me.PanelFrac17.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac17.TabIndex = 135
        Me.PanelFrac17.Tag = "16"
        Me.PanelFrac17.Visible = False
        '
        'BtnFrac17
        '
        Me.BtnFrac17.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac17.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac17.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac17.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac17.ForeColor = System.Drawing.Color.White
        Me.BtnFrac17.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac17.Name = "BtnFrac17"
        Me.BtnFrac17.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac17.TabIndex = 18
        Me.BtnFrac17.Tag = "16"
        Me.BtnFrac17.Text = "Fracturador17"
        Me.BtnFrac17.UseTransparentBackground = True
        '
        'LblFrac17Pump
        '
        Me.LblFrac17Pump.AutoSize = True
        Me.LblFrac17Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac17Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac17Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac17Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac17Pump.Name = "LblFrac17Pump"
        Me.LblFrac17Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac17Pump.TabIndex = 29
        Me.LblFrac17Pump.Text = "QWS2500"
        '
        'LblFrac17Trans
        '
        Me.LblFrac17Trans.AutoSize = True
        Me.LblFrac17Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac17Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac17Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac17Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac17Trans.Name = "LblFrac17Trans"
        Me.LblFrac17Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac17Trans.TabIndex = 28
        Me.LblFrac17Trans.Text = "ZF-8 TX"
        '
        'LblFrac17Engine
        '
        Me.LblFrac17Engine.AutoSize = True
        Me.LblFrac17Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac17Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac17Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac17Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac17Engine.Name = "LblFrac17Engine"
        Me.LblFrac17Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac17Engine.TabIndex = 27
        Me.LblFrac17Engine.Text = "D9620"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.BackColor = System.Drawing.Color.Transparent
        Me.Label52.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label52.ForeColor = System.Drawing.Color.White
        Me.Label52.Location = New System.Drawing.Point(254, 63)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(55, 16)
        Me.Label52.TabIndex = 26
        Me.Label52.Text = "Bomba:"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.Color.Transparent
        Me.Label53.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label53.ForeColor = System.Drawing.Color.White
        Me.Label53.Location = New System.Drawing.Point(115, 63)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(75, 16)
        Me.Label53.TabIndex = 25
        Me.Label53.Text = "Trasmisión:"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.BackColor = System.Drawing.Color.Transparent
        Me.Label54.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(-3, 63)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(47, 16)
        Me.Label54.TabIndex = 24
        Me.Label54.Text = "Motor:"
        '
        'PanelFrac13
        '
        Me.PanelFrac13.Controls.Add(Me.BtnFrac13)
        Me.PanelFrac13.Controls.Add(Me.LblFrac13Pump)
        Me.PanelFrac13.Controls.Add(Me.LblFrac13Trans)
        Me.PanelFrac13.Controls.Add(Me.LblFrac13Engine)
        Me.PanelFrac13.Controls.Add(Me.Label58)
        Me.PanelFrac13.Controls.Add(Me.Label59)
        Me.PanelFrac13.Controls.Add(Me.Label60)
        Me.PanelFrac13.Location = New System.Drawing.Point(4, 609)
        Me.PanelFrac13.Name = "PanelFrac13"
        Me.PanelFrac13.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac13.TabIndex = 134
        Me.PanelFrac13.Tag = "12"
        Me.PanelFrac13.Visible = False
        '
        'BtnFrac13
        '
        Me.BtnFrac13.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac13.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac13.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac13.ForeColor = System.Drawing.Color.White
        Me.BtnFrac13.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac13.Name = "BtnFrac13"
        Me.BtnFrac13.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac13.TabIndex = 18
        Me.BtnFrac13.Tag = "12"
        Me.BtnFrac13.Text = "Fracturador13"
        Me.BtnFrac13.UseTransparentBackground = True
        '
        'LblFrac13Pump
        '
        Me.LblFrac13Pump.AutoSize = True
        Me.LblFrac13Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac13Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac13Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac13Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac13Pump.Name = "LblFrac13Pump"
        Me.LblFrac13Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac13Pump.TabIndex = 29
        Me.LblFrac13Pump.Text = "QWS2500"
        '
        'LblFrac13Trans
        '
        Me.LblFrac13Trans.AutoSize = True
        Me.LblFrac13Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac13Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac13Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac13Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac13Trans.Name = "LblFrac13Trans"
        Me.LblFrac13Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac13Trans.TabIndex = 28
        Me.LblFrac13Trans.Text = "ZF-8 TX"
        '
        'LblFrac13Engine
        '
        Me.LblFrac13Engine.AutoSize = True
        Me.LblFrac13Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac13Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac13Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac13Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac13Engine.Name = "LblFrac13Engine"
        Me.LblFrac13Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac13Engine.TabIndex = 27
        Me.LblFrac13Engine.Text = "D9620"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.BackColor = System.Drawing.Color.Transparent
        Me.Label58.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label58.ForeColor = System.Drawing.Color.White
        Me.Label58.Location = New System.Drawing.Point(254, 63)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(55, 16)
        Me.Label58.TabIndex = 26
        Me.Label58.Text = "Bomba:"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.BackColor = System.Drawing.Color.Transparent
        Me.Label59.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label59.ForeColor = System.Drawing.Color.White
        Me.Label59.Location = New System.Drawing.Point(115, 63)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(75, 16)
        Me.Label59.TabIndex = 25
        Me.Label59.Text = "Trasmisión:"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.BackColor = System.Drawing.Color.Transparent
        Me.Label60.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label60.ForeColor = System.Drawing.Color.White
        Me.Label60.Location = New System.Drawing.Point(-3, 63)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(47, 16)
        Me.Label60.TabIndex = 24
        Me.Label60.Text = "Motor:"
        '
        'PanelFrac15
        '
        Me.PanelFrac15.Controls.Add(Me.BtnFrac15)
        Me.PanelFrac15.Controls.Add(Me.LblFrac15Pump)
        Me.PanelFrac15.Controls.Add(Me.LblFrac15Trans)
        Me.PanelFrac15.Controls.Add(Me.LblFrac15Engine)
        Me.PanelFrac15.Controls.Add(Me.Label64)
        Me.PanelFrac15.Controls.Add(Me.Label65)
        Me.PanelFrac15.Controls.Add(Me.Label66)
        Me.PanelFrac15.Location = New System.Drawing.Point(4, 712)
        Me.PanelFrac15.Name = "PanelFrac15"
        Me.PanelFrac15.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac15.TabIndex = 133
        Me.PanelFrac15.Tag = "14"
        Me.PanelFrac15.Visible = False
        '
        'BtnFrac15
        '
        Me.BtnFrac15.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac15.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac15.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac15.ForeColor = System.Drawing.Color.White
        Me.BtnFrac15.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac15.Name = "BtnFrac15"
        Me.BtnFrac15.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac15.TabIndex = 18
        Me.BtnFrac15.Tag = "14"
        Me.BtnFrac15.Text = "Fracturador15"
        Me.BtnFrac15.UseTransparentBackground = True
        '
        'LblFrac15Pump
        '
        Me.LblFrac15Pump.AutoSize = True
        Me.LblFrac15Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac15Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac15Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac15Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac15Pump.Name = "LblFrac15Pump"
        Me.LblFrac15Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac15Pump.TabIndex = 29
        Me.LblFrac15Pump.Text = "QWS2500"
        '
        'LblFrac15Trans
        '
        Me.LblFrac15Trans.AutoSize = True
        Me.LblFrac15Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac15Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac15Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac15Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac15Trans.Name = "LblFrac15Trans"
        Me.LblFrac15Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac15Trans.TabIndex = 28
        Me.LblFrac15Trans.Text = "ZF-8 TX"
        '
        'LblFrac15Engine
        '
        Me.LblFrac15Engine.AutoSize = True
        Me.LblFrac15Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac15Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac15Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac15Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac15Engine.Name = "LblFrac15Engine"
        Me.LblFrac15Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac15Engine.TabIndex = 27
        Me.LblFrac15Engine.Text = "D9620"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.BackColor = System.Drawing.Color.Transparent
        Me.Label64.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(254, 63)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(55, 16)
        Me.Label64.TabIndex = 26
        Me.Label64.Text = "Bomba:"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.BackColor = System.Drawing.Color.Transparent
        Me.Label65.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(115, 63)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(75, 16)
        Me.Label65.TabIndex = 25
        Me.Label65.Text = "Trasmisión:"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.BackColor = System.Drawing.Color.Transparent
        Me.Label66.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label66.ForeColor = System.Drawing.Color.White
        Me.Label66.Location = New System.Drawing.Point(-3, 63)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(47, 16)
        Me.Label66.TabIndex = 24
        Me.Label66.Text = "Motor:"
        '
        'PanelFrac11
        '
        Me.PanelFrac11.Controls.Add(Me.BtnFrac11)
        Me.PanelFrac11.Controls.Add(Me.LblFrac11Pump)
        Me.PanelFrac11.Controls.Add(Me.LblFrac11Trans)
        Me.PanelFrac11.Controls.Add(Me.LblFrac11Engine)
        Me.PanelFrac11.Controls.Add(Me.Label70)
        Me.PanelFrac11.Controls.Add(Me.Label71)
        Me.PanelFrac11.Controls.Add(Me.Label72)
        Me.PanelFrac11.Location = New System.Drawing.Point(4, 506)
        Me.PanelFrac11.Name = "PanelFrac11"
        Me.PanelFrac11.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac11.TabIndex = 132
        Me.PanelFrac11.Tag = "10"
        Me.PanelFrac11.Visible = False
        '
        'BtnFrac11
        '
        Me.BtnFrac11.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac11.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac11.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac11.ForeColor = System.Drawing.Color.White
        Me.BtnFrac11.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac11.Name = "BtnFrac11"
        Me.BtnFrac11.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac11.TabIndex = 18
        Me.BtnFrac11.Tag = "10"
        Me.BtnFrac11.Text = "Fracturador11"
        Me.BtnFrac11.UseTransparentBackground = True
        '
        'LblFrac11Pump
        '
        Me.LblFrac11Pump.AutoSize = True
        Me.LblFrac11Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac11Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac11Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac11Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac11Pump.Name = "LblFrac11Pump"
        Me.LblFrac11Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac11Pump.TabIndex = 29
        Me.LblFrac11Pump.Text = "QWS2500"
        '
        'LblFrac11Trans
        '
        Me.LblFrac11Trans.AutoSize = True
        Me.LblFrac11Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac11Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac11Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac11Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac11Trans.Name = "LblFrac11Trans"
        Me.LblFrac11Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac11Trans.TabIndex = 28
        Me.LblFrac11Trans.Text = "ZF-8 TX"
        '
        'LblFrac11Engine
        '
        Me.LblFrac11Engine.AutoSize = True
        Me.LblFrac11Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac11Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac11Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac11Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac11Engine.Name = "LblFrac11Engine"
        Me.LblFrac11Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac11Engine.TabIndex = 27
        Me.LblFrac11Engine.Text = "D9620"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.BackColor = System.Drawing.Color.Transparent
        Me.Label70.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label70.ForeColor = System.Drawing.Color.White
        Me.Label70.Location = New System.Drawing.Point(254, 63)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(55, 16)
        Me.Label70.TabIndex = 26
        Me.Label70.Text = "Bomba:"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.BackColor = System.Drawing.Color.Transparent
        Me.Label71.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label71.ForeColor = System.Drawing.Color.White
        Me.Label71.Location = New System.Drawing.Point(115, 63)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(75, 16)
        Me.Label71.TabIndex = 25
        Me.Label71.Text = "Trasmisión:"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.BackColor = System.Drawing.Color.Transparent
        Me.Label72.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label72.ForeColor = System.Drawing.Color.White
        Me.Label72.Location = New System.Drawing.Point(-3, 63)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(47, 16)
        Me.Label72.TabIndex = 24
        Me.Label72.Text = "Motor:"
        '
        'Guna2Separator16
        '
        Me.Guna2Separator16.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator16.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator16.Location = New System.Drawing.Point(4, 593)
        Me.Guna2Separator16.Name = "Guna2Separator16"
        Me.Guna2Separator16.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator16.TabIndex = 131
        Me.Guna2Separator16.UseTransparentBackground = True
        '
        'Guna2Separator17
        '
        Me.Guna2Separator17.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator17.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator17.Location = New System.Drawing.Point(4, 799)
        Me.Guna2Separator17.Name = "Guna2Separator17"
        Me.Guna2Separator17.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator17.TabIndex = 130
        Me.Guna2Separator17.UseTransparentBackground = True
        '
        'Guna2Separator18
        '
        Me.Guna2Separator18.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator18.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator18.Location = New System.Drawing.Point(4, 696)
        Me.Guna2Separator18.Name = "Guna2Separator18"
        Me.Guna2Separator18.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator18.TabIndex = 129
        Me.Guna2Separator18.UseTransparentBackground = True
        '
        'Guna2Separator13
        '
        Me.Guna2Separator13.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator13.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator13.Location = New System.Drawing.Point(400, 497)
        Me.Guna2Separator13.Name = "Guna2Separator13"
        Me.Guna2Separator13.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator13.TabIndex = 128
        Me.Guna2Separator13.UseTransparentBackground = True
        '
        'Guna2Separator9
        '
        Me.Guna2Separator9.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator9.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator9.Location = New System.Drawing.Point(400, 902)
        Me.Guna2Separator9.Name = "Guna2Separator9"
        Me.Guna2Separator9.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator9.TabIndex = 127
        Me.Guna2Separator9.UseTransparentBackground = True
        '
        'PanelFrac20
        '
        Me.PanelFrac20.Controls.Add(Me.BtnFrac20)
        Me.PanelFrac20.Controls.Add(Me.LblFrac20Pump)
        Me.PanelFrac20.Controls.Add(Me.LblFrac20Trans)
        Me.PanelFrac20.Controls.Add(Me.LblFrac20Engine)
        Me.PanelFrac20.Controls.Add(Me.Label16)
        Me.PanelFrac20.Controls.Add(Me.Label17)
        Me.PanelFrac20.Controls.Add(Me.Label18)
        Me.PanelFrac20.Location = New System.Drawing.Point(400, 911)
        Me.PanelFrac20.Name = "PanelFrac20"
        Me.PanelFrac20.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac20.TabIndex = 126
        Me.PanelFrac20.Tag = "19"
        Me.PanelFrac20.Visible = False
        '
        'BtnFrac20
        '
        Me.BtnFrac20.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac20.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac20.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac20.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac20.ForeColor = System.Drawing.Color.White
        Me.BtnFrac20.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac20.Name = "BtnFrac20"
        Me.BtnFrac20.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac20.TabIndex = 18
        Me.BtnFrac20.Tag = "19"
        Me.BtnFrac20.Text = "Fracturador20"
        Me.BtnFrac20.UseTransparentBackground = True
        '
        'LblFrac20Pump
        '
        Me.LblFrac20Pump.AutoSize = True
        Me.LblFrac20Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac20Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac20Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac20Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac20Pump.Name = "LblFrac20Pump"
        Me.LblFrac20Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac20Pump.TabIndex = 29
        Me.LblFrac20Pump.Text = "QWS2500"
        '
        'LblFrac20Trans
        '
        Me.LblFrac20Trans.AutoSize = True
        Me.LblFrac20Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac20Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac20Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac20Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac20Trans.Name = "LblFrac20Trans"
        Me.LblFrac20Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac20Trans.TabIndex = 28
        Me.LblFrac20Trans.Text = "ZF-8 TX"
        '
        'LblFrac20Engine
        '
        Me.LblFrac20Engine.AutoSize = True
        Me.LblFrac20Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac20Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac20Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac20Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac20Engine.Name = "LblFrac20Engine"
        Me.LblFrac20Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac20Engine.TabIndex = 27
        Me.LblFrac20Engine.Text = "D9620"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(254, 63)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(55, 16)
        Me.Label16.TabIndex = 26
        Me.Label16.Text = "Bomba:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(115, 63)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(75, 16)
        Me.Label17.TabIndex = 25
        Me.Label17.Text = "Trasmisión:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(-3, 63)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(47, 16)
        Me.Label18.TabIndex = 24
        Me.Label18.Text = "Motor:"
        '
        'PanelFrac18
        '
        Me.PanelFrac18.Controls.Add(Me.BtnFrac18)
        Me.PanelFrac18.Controls.Add(Me.LblFrac18Pump)
        Me.PanelFrac18.Controls.Add(Me.LblFrac18Trans)
        Me.PanelFrac18.Controls.Add(Me.LblFrac18Engine)
        Me.PanelFrac18.Controls.Add(Me.Label22)
        Me.PanelFrac18.Controls.Add(Me.Label23)
        Me.PanelFrac18.Controls.Add(Me.Label24)
        Me.PanelFrac18.Location = New System.Drawing.Point(400, 815)
        Me.PanelFrac18.Name = "PanelFrac18"
        Me.PanelFrac18.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac18.TabIndex = 125
        Me.PanelFrac18.Tag = "17"
        Me.PanelFrac18.Visible = False
        '
        'BtnFrac18
        '
        Me.BtnFrac18.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac18.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac18.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac18.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac18.ForeColor = System.Drawing.Color.White
        Me.BtnFrac18.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac18.Name = "BtnFrac18"
        Me.BtnFrac18.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac18.TabIndex = 18
        Me.BtnFrac18.Tag = "17"
        Me.BtnFrac18.Text = "Fracturador18"
        Me.BtnFrac18.UseTransparentBackground = True
        '
        'LblFrac18Pump
        '
        Me.LblFrac18Pump.AutoSize = True
        Me.LblFrac18Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac18Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac18Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac18Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac18Pump.Name = "LblFrac18Pump"
        Me.LblFrac18Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac18Pump.TabIndex = 29
        Me.LblFrac18Pump.Text = "QWS2500"
        '
        'LblFrac18Trans
        '
        Me.LblFrac18Trans.AutoSize = True
        Me.LblFrac18Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac18Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac18Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac18Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac18Trans.Name = "LblFrac18Trans"
        Me.LblFrac18Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac18Trans.TabIndex = 28
        Me.LblFrac18Trans.Text = "ZF-8 TX"
        '
        'LblFrac18Engine
        '
        Me.LblFrac18Engine.AutoSize = True
        Me.LblFrac18Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac18Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac18Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac18Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac18Engine.Name = "LblFrac18Engine"
        Me.LblFrac18Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac18Engine.TabIndex = 27
        Me.LblFrac18Engine.Text = "D9620"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(254, 63)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(55, 16)
        Me.Label22.TabIndex = 26
        Me.Label22.Text = "Bomba:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(115, 63)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(75, 16)
        Me.Label23.TabIndex = 25
        Me.Label23.Text = "Trasmisión:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(-3, 63)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(47, 16)
        Me.Label24.TabIndex = 24
        Me.Label24.Text = "Motor:"
        '
        'PanelFrac14
        '
        Me.PanelFrac14.Controls.Add(Me.BtnFrac14)
        Me.PanelFrac14.Controls.Add(Me.LblFrac14Pump)
        Me.PanelFrac14.Controls.Add(Me.LblFrac14Trans)
        Me.PanelFrac14.Controls.Add(Me.LblFrac14Engine)
        Me.PanelFrac14.Controls.Add(Me.Label28)
        Me.PanelFrac14.Controls.Add(Me.Label29)
        Me.PanelFrac14.Controls.Add(Me.Label30)
        Me.PanelFrac14.Location = New System.Drawing.Point(400, 609)
        Me.PanelFrac14.Name = "PanelFrac14"
        Me.PanelFrac14.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac14.TabIndex = 124
        Me.PanelFrac14.Tag = "13"
        Me.PanelFrac14.Visible = False
        '
        'BtnFrac14
        '
        Me.BtnFrac14.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac14.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac14.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac14.ForeColor = System.Drawing.Color.White
        Me.BtnFrac14.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac14.Name = "BtnFrac14"
        Me.BtnFrac14.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac14.TabIndex = 18
        Me.BtnFrac14.Tag = "13"
        Me.BtnFrac14.Text = "Fracturador14"
        Me.BtnFrac14.UseTransparentBackground = True
        '
        'LblFrac14Pump
        '
        Me.LblFrac14Pump.AutoSize = True
        Me.LblFrac14Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac14Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac14Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac14Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac14Pump.Name = "LblFrac14Pump"
        Me.LblFrac14Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac14Pump.TabIndex = 29
        Me.LblFrac14Pump.Text = "QWS2500"
        '
        'LblFrac14Trans
        '
        Me.LblFrac14Trans.AutoSize = True
        Me.LblFrac14Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac14Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac14Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac14Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac14Trans.Name = "LblFrac14Trans"
        Me.LblFrac14Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac14Trans.TabIndex = 28
        Me.LblFrac14Trans.Text = "ZF-8 TX"
        '
        'LblFrac14Engine
        '
        Me.LblFrac14Engine.AutoSize = True
        Me.LblFrac14Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac14Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac14Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac14Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac14Engine.Name = "LblFrac14Engine"
        Me.LblFrac14Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac14Engine.TabIndex = 27
        Me.LblFrac14Engine.Text = "D9620"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(254, 63)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(55, 16)
        Me.Label28.TabIndex = 26
        Me.Label28.Text = "Bomba:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label29.ForeColor = System.Drawing.Color.White
        Me.Label29.Location = New System.Drawing.Point(115, 63)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(75, 16)
        Me.Label29.TabIndex = 25
        Me.Label29.Text = "Trasmisión:"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.Transparent
        Me.Label30.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(-3, 63)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(47, 16)
        Me.Label30.TabIndex = 24
        Me.Label30.Text = "Motor:"
        '
        'PanelFrac16
        '
        Me.PanelFrac16.Controls.Add(Me.BtnFrac16)
        Me.PanelFrac16.Controls.Add(Me.LblFrac16Pump)
        Me.PanelFrac16.Controls.Add(Me.LblFrac16Trans)
        Me.PanelFrac16.Controls.Add(Me.LblFrac16Engine)
        Me.PanelFrac16.Controls.Add(Me.Label34)
        Me.PanelFrac16.Controls.Add(Me.Label35)
        Me.PanelFrac16.Controls.Add(Me.Label36)
        Me.PanelFrac16.Location = New System.Drawing.Point(400, 712)
        Me.PanelFrac16.Name = "PanelFrac16"
        Me.PanelFrac16.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac16.TabIndex = 123
        Me.PanelFrac16.Tag = "15"
        Me.PanelFrac16.Visible = False
        '
        'BtnFrac16
        '
        Me.BtnFrac16.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac16.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac16.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac16.ForeColor = System.Drawing.Color.White
        Me.BtnFrac16.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac16.Name = "BtnFrac16"
        Me.BtnFrac16.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac16.TabIndex = 18
        Me.BtnFrac16.Tag = "15"
        Me.BtnFrac16.Text = "Fracturador16"
        Me.BtnFrac16.UseTransparentBackground = True
        '
        'LblFrac16Pump
        '
        Me.LblFrac16Pump.AutoSize = True
        Me.LblFrac16Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac16Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac16Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac16Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac16Pump.Name = "LblFrac16Pump"
        Me.LblFrac16Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac16Pump.TabIndex = 29
        Me.LblFrac16Pump.Text = "QWS2500"
        '
        'LblFrac16Trans
        '
        Me.LblFrac16Trans.AutoSize = True
        Me.LblFrac16Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac16Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac16Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac16Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac16Trans.Name = "LblFrac16Trans"
        Me.LblFrac16Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac16Trans.TabIndex = 28
        Me.LblFrac16Trans.Text = "ZF-8 TX"
        '
        'LblFrac16Engine
        '
        Me.LblFrac16Engine.AutoSize = True
        Me.LblFrac16Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac16Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac16Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac16Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac16Engine.Name = "LblFrac16Engine"
        Me.LblFrac16Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac16Engine.TabIndex = 27
        Me.LblFrac16Engine.Text = "D9620"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.Transparent
        Me.Label34.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label34.ForeColor = System.Drawing.Color.White
        Me.Label34.Location = New System.Drawing.Point(254, 63)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(55, 16)
        Me.Label34.TabIndex = 26
        Me.Label34.Text = "Bomba:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(115, 63)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(75, 16)
        Me.Label35.TabIndex = 25
        Me.Label35.Text = "Trasmisión:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(-3, 63)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(47, 16)
        Me.Label36.TabIndex = 24
        Me.Label36.Text = "Motor:"
        '
        'PanelFrac12
        '
        Me.PanelFrac12.Controls.Add(Me.BtnFrac12)
        Me.PanelFrac12.Controls.Add(Me.LblFrac12Pump)
        Me.PanelFrac12.Controls.Add(Me.LblFrac12Trans)
        Me.PanelFrac12.Controls.Add(Me.LblFrac12Engine)
        Me.PanelFrac12.Controls.Add(Me.Label40)
        Me.PanelFrac12.Controls.Add(Me.Label41)
        Me.PanelFrac12.Controls.Add(Me.Label42)
        Me.PanelFrac12.Location = New System.Drawing.Point(400, 506)
        Me.PanelFrac12.Name = "PanelFrac12"
        Me.PanelFrac12.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac12.TabIndex = 122
        Me.PanelFrac12.Tag = "11"
        Me.PanelFrac12.Visible = False
        '
        'BtnFrac12
        '
        Me.BtnFrac12.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac12.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac12.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac12.ForeColor = System.Drawing.Color.White
        Me.BtnFrac12.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac12.Name = "BtnFrac12"
        Me.BtnFrac12.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac12.TabIndex = 18
        Me.BtnFrac12.Tag = "11"
        Me.BtnFrac12.Text = "Fracturador12"
        Me.BtnFrac12.UseTransparentBackground = True
        '
        'LblFrac12Pump
        '
        Me.LblFrac12Pump.AutoSize = True
        Me.LblFrac12Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac12Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac12Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac12Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac12Pump.Name = "LblFrac12Pump"
        Me.LblFrac12Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac12Pump.TabIndex = 29
        Me.LblFrac12Pump.Text = "QWS2500"
        '
        'LblFrac12Trans
        '
        Me.LblFrac12Trans.AutoSize = True
        Me.LblFrac12Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac12Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac12Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac12Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac12Trans.Name = "LblFrac12Trans"
        Me.LblFrac12Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac12Trans.TabIndex = 28
        Me.LblFrac12Trans.Text = "ZF-8 TX"
        '
        'LblFrac12Engine
        '
        Me.LblFrac12Engine.AutoSize = True
        Me.LblFrac12Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac12Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac12Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac12Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac12Engine.Name = "LblFrac12Engine"
        Me.LblFrac12Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac12Engine.TabIndex = 27
        Me.LblFrac12Engine.Text = "D9620"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(254, 63)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(55, 16)
        Me.Label40.TabIndex = 26
        Me.Label40.Text = "Bomba:"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.BackColor = System.Drawing.Color.Transparent
        Me.Label41.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(115, 63)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(75, 16)
        Me.Label41.TabIndex = 25
        Me.Label41.Text = "Trasmisión:"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        Me.Label42.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(-3, 63)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(47, 16)
        Me.Label42.TabIndex = 24
        Me.Label42.Text = "Motor:"
        '
        'Guna2Separator10
        '
        Me.Guna2Separator10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator10.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator10.Location = New System.Drawing.Point(400, 593)
        Me.Guna2Separator10.Name = "Guna2Separator10"
        Me.Guna2Separator10.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator10.TabIndex = 121
        Me.Guna2Separator10.UseTransparentBackground = True
        '
        'Guna2Separator11
        '
        Me.Guna2Separator11.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator11.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator11.Location = New System.Drawing.Point(400, 799)
        Me.Guna2Separator11.Name = "Guna2Separator11"
        Me.Guna2Separator11.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator11.TabIndex = 120
        Me.Guna2Separator11.UseTransparentBackground = True
        '
        'Guna2Separator12
        '
        Me.Guna2Separator12.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator12.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator12.Location = New System.Drawing.Point(400, 696)
        Me.Guna2Separator12.Name = "Guna2Separator12"
        Me.Guna2Separator12.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator12.TabIndex = 119
        Me.Guna2Separator12.UseTransparentBackground = True
        '
        'Guna2Separator8
        '
        Me.Guna2Separator8.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator8.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator8.Location = New System.Drawing.Point(1, 401)
        Me.Guna2Separator8.Name = "Guna2Separator8"
        Me.Guna2Separator8.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator8.TabIndex = 118
        Me.Guna2Separator8.UseTransparentBackground = True
        '
        'Guna2Separator7
        '
        Me.Guna2Separator7.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator7.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator7.Location = New System.Drawing.Point(400, 401)
        Me.Guna2Separator7.Name = "Guna2Separator7"
        Me.Guna2Separator7.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator7.TabIndex = 117
        Me.Guna2Separator7.UseTransparentBackground = True
        '
        'PanelFrac10
        '
        Me.PanelFrac10.Controls.Add(Me.BtnFrac10)
        Me.PanelFrac10.Controls.Add(Me.LblFrac10Pump)
        Me.PanelFrac10.Controls.Add(Me.LblFrac10Trans)
        Me.PanelFrac10.Controls.Add(Me.LblFrac10Engine)
        Me.PanelFrac10.Controls.Add(Me.Label4)
        Me.PanelFrac10.Controls.Add(Me.Label5)
        Me.PanelFrac10.Controls.Add(Me.Label6)
        Me.PanelFrac10.Location = New System.Drawing.Point(400, 410)
        Me.PanelFrac10.Name = "PanelFrac10"
        Me.PanelFrac10.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac10.TabIndex = 116
        Me.PanelFrac10.Tag = "9"
        Me.PanelFrac10.Visible = False
        '
        'BtnFrac10
        '
        Me.BtnFrac10.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac10.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac10.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac10.ForeColor = System.Drawing.Color.White
        Me.BtnFrac10.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac10.Name = "BtnFrac10"
        Me.BtnFrac10.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac10.TabIndex = 18
        Me.BtnFrac10.Tag = "9"
        Me.BtnFrac10.Text = "Fracturador10"
        '
        'LblFrac10Pump
        '
        Me.LblFrac10Pump.AutoSize = True
        Me.LblFrac10Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac10Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac10Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac10Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac10Pump.Name = "LblFrac10Pump"
        Me.LblFrac10Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac10Pump.TabIndex = 29
        Me.LblFrac10Pump.Text = "QWS2500"
        '
        'LblFrac10Trans
        '
        Me.LblFrac10Trans.AutoSize = True
        Me.LblFrac10Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac10Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac10Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac10Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac10Trans.Name = "LblFrac10Trans"
        Me.LblFrac10Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac10Trans.TabIndex = 28
        Me.LblFrac10Trans.Text = "ZF-8 TX"
        '
        'LblFrac10Engine
        '
        Me.LblFrac10Engine.AutoSize = True
        Me.LblFrac10Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac10Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac10Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac10Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac10Engine.Name = "LblFrac10Engine"
        Me.LblFrac10Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac10Engine.TabIndex = 27
        Me.LblFrac10Engine.Text = "D9620"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(254, 63)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 16)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Bomba:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(115, 63)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(75, 16)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Trasmisión:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(-3, 63)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 16)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Motor:"
        '
        'PanelFrac9
        '
        Me.PanelFrac9.Controls.Add(Me.BtnFrac9)
        Me.PanelFrac9.Controls.Add(Me.LblFrac9Pump)
        Me.PanelFrac9.Controls.Add(Me.LblFrac9Trans)
        Me.PanelFrac9.Controls.Add(Me.LblFrac9Engine)
        Me.PanelFrac9.Controls.Add(Me.Label10)
        Me.PanelFrac9.Controls.Add(Me.Label11)
        Me.PanelFrac9.Controls.Add(Me.Label12)
        Me.PanelFrac9.Location = New System.Drawing.Point(4, 410)
        Me.PanelFrac9.Name = "PanelFrac9"
        Me.PanelFrac9.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac9.TabIndex = 115
        Me.PanelFrac9.Tag = "8"
        Me.PanelFrac9.Visible = False
        '
        'BtnFrac9
        '
        Me.BtnFrac9.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac9.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac9.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac9.ForeColor = System.Drawing.Color.White
        Me.BtnFrac9.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac9.Name = "BtnFrac9"
        Me.BtnFrac9.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac9.TabIndex = 18
        Me.BtnFrac9.Tag = "8"
        Me.BtnFrac9.Text = "Fracturador9"
        '
        'LblFrac9Pump
        '
        Me.LblFrac9Pump.AutoSize = True
        Me.LblFrac9Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac9Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac9Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac9Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac9Pump.Name = "LblFrac9Pump"
        Me.LblFrac9Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac9Pump.TabIndex = 29
        Me.LblFrac9Pump.Text = "QWS2500"
        '
        'LblFrac9Trans
        '
        Me.LblFrac9Trans.AutoSize = True
        Me.LblFrac9Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac9Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac9Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac9Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac9Trans.Name = "LblFrac9Trans"
        Me.LblFrac9Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac9Trans.TabIndex = 28
        Me.LblFrac9Trans.Text = "ZF-8 TX"
        '
        'LblFrac9Engine
        '
        Me.LblFrac9Engine.AutoSize = True
        Me.LblFrac9Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac9Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac9Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac9Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac9Engine.Name = "LblFrac9Engine"
        Me.LblFrac9Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac9Engine.TabIndex = 27
        Me.LblFrac9Engine.Text = "D9620"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(254, 63)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(55, 16)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "Bomba:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(115, 63)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(75, 16)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "Trasmisión:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(-3, 63)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 16)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Motor:"
        '
        'PanelFrac5
        '
        Me.PanelFrac5.Controls.Add(Me.BtnFrac5)
        Me.PanelFrac5.Controls.Add(Me.LblFrac5Pump)
        Me.PanelFrac5.Controls.Add(Me.LblFrac5Trans)
        Me.PanelFrac5.Controls.Add(Me.LblFrac5Engine)
        Me.PanelFrac5.Controls.Add(Me.LblFSPump5)
        Me.PanelFrac5.Controls.Add(Me.LblFSTrans5)
        Me.PanelFrac5.Controls.Add(Me.LblFSEngine5)
        Me.PanelFrac5.Location = New System.Drawing.Point(4, 211)
        Me.PanelFrac5.Name = "PanelFrac5"
        Me.PanelFrac5.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac5.TabIndex = 114
        Me.PanelFrac5.Tag = "4"
        Me.PanelFrac5.Visible = False
        '
        'BtnFrac5
        '
        Me.BtnFrac5.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac5.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac5.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac5.ForeColor = System.Drawing.Color.White
        Me.BtnFrac5.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac5.Name = "BtnFrac5"
        Me.BtnFrac5.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac5.TabIndex = 18
        Me.BtnFrac5.Tag = "4"
        Me.BtnFrac5.Text = "Fracturador5"
        '
        'LblFrac5Pump
        '
        Me.LblFrac5Pump.AutoSize = True
        Me.LblFrac5Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac5Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac5Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac5Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac5Pump.Name = "LblFrac5Pump"
        Me.LblFrac5Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac5Pump.TabIndex = 29
        Me.LblFrac5Pump.Text = "QWS2500"
        '
        'LblFrac5Trans
        '
        Me.LblFrac5Trans.AutoSize = True
        Me.LblFrac5Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac5Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac5Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac5Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac5Trans.Name = "LblFrac5Trans"
        Me.LblFrac5Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac5Trans.TabIndex = 28
        Me.LblFrac5Trans.Text = "ZF-8 TX"
        '
        'LblFrac5Engine
        '
        Me.LblFrac5Engine.AutoSize = True
        Me.LblFrac5Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac5Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac5Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac5Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac5Engine.Name = "LblFrac5Engine"
        Me.LblFrac5Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac5Engine.TabIndex = 27
        Me.LblFrac5Engine.Text = "D9620"
        '
        'LblFSPump5
        '
        Me.LblFSPump5.AutoSize = True
        Me.LblFSPump5.BackColor = System.Drawing.Color.Transparent
        Me.LblFSPump5.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSPump5.ForeColor = System.Drawing.Color.White
        Me.LblFSPump5.Location = New System.Drawing.Point(254, 63)
        Me.LblFSPump5.Name = "LblFSPump5"
        Me.LblFSPump5.Size = New System.Drawing.Size(55, 16)
        Me.LblFSPump5.TabIndex = 26
        Me.LblFSPump5.Text = "Bomba:"
        '
        'LblFSTrans5
        '
        Me.LblFSTrans5.AutoSize = True
        Me.LblFSTrans5.BackColor = System.Drawing.Color.Transparent
        Me.LblFSTrans5.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSTrans5.ForeColor = System.Drawing.Color.White
        Me.LblFSTrans5.Location = New System.Drawing.Point(115, 63)
        Me.LblFSTrans5.Name = "LblFSTrans5"
        Me.LblFSTrans5.Size = New System.Drawing.Size(75, 16)
        Me.LblFSTrans5.TabIndex = 25
        Me.LblFSTrans5.Text = "Trasmisión:"
        '
        'LblFSEngine5
        '
        Me.LblFSEngine5.AutoSize = True
        Me.LblFSEngine5.BackColor = System.Drawing.Color.Transparent
        Me.LblFSEngine5.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSEngine5.ForeColor = System.Drawing.Color.White
        Me.LblFSEngine5.Location = New System.Drawing.Point(-3, 63)
        Me.LblFSEngine5.Name = "LblFSEngine5"
        Me.LblFSEngine5.Size = New System.Drawing.Size(47, 16)
        Me.LblFSEngine5.TabIndex = 24
        Me.LblFSEngine5.Text = "Motor:"
        '
        'PanelFrac8
        '
        Me.PanelFrac8.Controls.Add(Me.BtnFrac8)
        Me.PanelFrac8.Controls.Add(Me.LblFrac8Pump)
        Me.PanelFrac8.Controls.Add(Me.LblFrac8Trans)
        Me.PanelFrac8.Controls.Add(Me.LblFrac8Engine)
        Me.PanelFrac8.Controls.Add(Me.LblFSPump8)
        Me.PanelFrac8.Controls.Add(Me.LblFSTrans8)
        Me.PanelFrac8.Controls.Add(Me.LblFSEngine8)
        Me.PanelFrac8.Location = New System.Drawing.Point(400, 314)
        Me.PanelFrac8.Name = "PanelFrac8"
        Me.PanelFrac8.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac8.TabIndex = 113
        Me.PanelFrac8.Tag = "7"
        Me.PanelFrac8.Visible = False
        '
        'BtnFrac8
        '
        Me.BtnFrac8.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac8.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac8.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac8.ForeColor = System.Drawing.Color.White
        Me.BtnFrac8.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac8.Name = "BtnFrac8"
        Me.BtnFrac8.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac8.TabIndex = 18
        Me.BtnFrac8.Tag = "7"
        Me.BtnFrac8.Text = "Fracturador8"
        '
        'LblFrac8Pump
        '
        Me.LblFrac8Pump.AutoSize = True
        Me.LblFrac8Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac8Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac8Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac8Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac8Pump.Name = "LblFrac8Pump"
        Me.LblFrac8Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac8Pump.TabIndex = 29
        Me.LblFrac8Pump.Text = "QWS2500"
        '
        'LblFrac8Trans
        '
        Me.LblFrac8Trans.AutoSize = True
        Me.LblFrac8Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac8Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac8Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac8Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac8Trans.Name = "LblFrac8Trans"
        Me.LblFrac8Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac8Trans.TabIndex = 28
        Me.LblFrac8Trans.Text = "ZF-8 TX"
        '
        'LblFrac8Engine
        '
        Me.LblFrac8Engine.AutoSize = True
        Me.LblFrac8Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac8Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac8Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac8Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac8Engine.Name = "LblFrac8Engine"
        Me.LblFrac8Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac8Engine.TabIndex = 27
        Me.LblFrac8Engine.Text = "D9620"
        '
        'LblFSPump8
        '
        Me.LblFSPump8.AutoSize = True
        Me.LblFSPump8.BackColor = System.Drawing.Color.Transparent
        Me.LblFSPump8.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSPump8.ForeColor = System.Drawing.Color.White
        Me.LblFSPump8.Location = New System.Drawing.Point(254, 63)
        Me.LblFSPump8.Name = "LblFSPump8"
        Me.LblFSPump8.Size = New System.Drawing.Size(55, 16)
        Me.LblFSPump8.TabIndex = 26
        Me.LblFSPump8.Text = "Bomba:"
        '
        'LblFSTrans8
        '
        Me.LblFSTrans8.AutoSize = True
        Me.LblFSTrans8.BackColor = System.Drawing.Color.Transparent
        Me.LblFSTrans8.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSTrans8.ForeColor = System.Drawing.Color.White
        Me.LblFSTrans8.Location = New System.Drawing.Point(115, 63)
        Me.LblFSTrans8.Name = "LblFSTrans8"
        Me.LblFSTrans8.Size = New System.Drawing.Size(75, 16)
        Me.LblFSTrans8.TabIndex = 25
        Me.LblFSTrans8.Text = "Trasmisión:"
        '
        'LblFSEngine8
        '
        Me.LblFSEngine8.AutoSize = True
        Me.LblFSEngine8.BackColor = System.Drawing.Color.Transparent
        Me.LblFSEngine8.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSEngine8.ForeColor = System.Drawing.Color.White
        Me.LblFSEngine8.Location = New System.Drawing.Point(-3, 63)
        Me.LblFSEngine8.Name = "LblFSEngine8"
        Me.LblFSEngine8.Size = New System.Drawing.Size(47, 16)
        Me.LblFSEngine8.TabIndex = 24
        Me.LblFSEngine8.Text = "Motor:"
        '
        'PanelFrac7
        '
        Me.PanelFrac7.Controls.Add(Me.BtnFrac7)
        Me.PanelFrac7.Controls.Add(Me.LblFrac7Pump)
        Me.PanelFrac7.Controls.Add(Me.LblFrac7Trans)
        Me.PanelFrac7.Controls.Add(Me.LblFrac7Engine)
        Me.PanelFrac7.Controls.Add(Me.LblFSPump7)
        Me.PanelFrac7.Controls.Add(Me.LblFSTrans7)
        Me.PanelFrac7.Controls.Add(Me.LblFSEngine7)
        Me.PanelFrac7.Location = New System.Drawing.Point(4, 314)
        Me.PanelFrac7.Name = "PanelFrac7"
        Me.PanelFrac7.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac7.TabIndex = 112
        Me.PanelFrac7.Tag = "6"
        Me.PanelFrac7.Visible = False
        '
        'BtnFrac7
        '
        Me.BtnFrac7.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac7.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac7.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac7.ForeColor = System.Drawing.Color.White
        Me.BtnFrac7.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac7.Name = "BtnFrac7"
        Me.BtnFrac7.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac7.TabIndex = 18
        Me.BtnFrac7.Tag = "6"
        Me.BtnFrac7.Text = "Fracturador7"
        '
        'LblFrac7Pump
        '
        Me.LblFrac7Pump.AutoSize = True
        Me.LblFrac7Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac7Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac7Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac7Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac7Pump.Name = "LblFrac7Pump"
        Me.LblFrac7Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac7Pump.TabIndex = 29
        Me.LblFrac7Pump.Text = "QWS2500"
        '
        'LblFrac7Trans
        '
        Me.LblFrac7Trans.AutoSize = True
        Me.LblFrac7Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac7Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac7Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac7Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac7Trans.Name = "LblFrac7Trans"
        Me.LblFrac7Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac7Trans.TabIndex = 28
        Me.LblFrac7Trans.Text = "ZF-8 TX"
        '
        'LblFrac7Engine
        '
        Me.LblFrac7Engine.AutoSize = True
        Me.LblFrac7Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac7Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac7Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac7Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac7Engine.Name = "LblFrac7Engine"
        Me.LblFrac7Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac7Engine.TabIndex = 27
        Me.LblFrac7Engine.Text = "D9620"
        '
        'LblFSPump7
        '
        Me.LblFSPump7.AutoSize = True
        Me.LblFSPump7.BackColor = System.Drawing.Color.Transparent
        Me.LblFSPump7.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSPump7.ForeColor = System.Drawing.Color.White
        Me.LblFSPump7.Location = New System.Drawing.Point(254, 63)
        Me.LblFSPump7.Name = "LblFSPump7"
        Me.LblFSPump7.Size = New System.Drawing.Size(55, 16)
        Me.LblFSPump7.TabIndex = 26
        Me.LblFSPump7.Text = "Bomba:"
        '
        'LblFSTrans7
        '
        Me.LblFSTrans7.AutoSize = True
        Me.LblFSTrans7.BackColor = System.Drawing.Color.Transparent
        Me.LblFSTrans7.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSTrans7.ForeColor = System.Drawing.Color.White
        Me.LblFSTrans7.Location = New System.Drawing.Point(115, 63)
        Me.LblFSTrans7.Name = "LblFSTrans7"
        Me.LblFSTrans7.Size = New System.Drawing.Size(75, 16)
        Me.LblFSTrans7.TabIndex = 25
        Me.LblFSTrans7.Text = "Trasmisión:"
        '
        'LblFSEngine7
        '
        Me.LblFSEngine7.AutoSize = True
        Me.LblFSEngine7.BackColor = System.Drawing.Color.Transparent
        Me.LblFSEngine7.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSEngine7.ForeColor = System.Drawing.Color.White
        Me.LblFSEngine7.Location = New System.Drawing.Point(-3, 63)
        Me.LblFSEngine7.Name = "LblFSEngine7"
        Me.LblFSEngine7.Size = New System.Drawing.Size(47, 16)
        Me.LblFSEngine7.TabIndex = 24
        Me.LblFSEngine7.Text = "Motor:"
        '
        'PanelFrac4
        '
        Me.PanelFrac4.Controls.Add(Me.BtnFrac4)
        Me.PanelFrac4.Controls.Add(Me.LblFrac4Pump)
        Me.PanelFrac4.Controls.Add(Me.LblFrac4Trans)
        Me.PanelFrac4.Controls.Add(Me.LblFrac4Engine)
        Me.PanelFrac4.Controls.Add(Me.LblFSPump4)
        Me.PanelFrac4.Controls.Add(Me.LblFSTrans4)
        Me.PanelFrac4.Controls.Add(Me.LblFSEngine4)
        Me.PanelFrac4.Location = New System.Drawing.Point(400, 108)
        Me.PanelFrac4.Name = "PanelFrac4"
        Me.PanelFrac4.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac4.TabIndex = 111
        Me.PanelFrac4.Tag = "3"
        Me.PanelFrac4.Visible = False
        '
        'BtnFrac4
        '
        Me.BtnFrac4.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac4.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac4.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac4.ForeColor = System.Drawing.Color.White
        Me.BtnFrac4.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac4.Name = "BtnFrac4"
        Me.BtnFrac4.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac4.TabIndex = 18
        Me.BtnFrac4.Tag = "3"
        Me.BtnFrac4.Text = "Fracturador4"
        '
        'LblFrac4Pump
        '
        Me.LblFrac4Pump.AutoSize = True
        Me.LblFrac4Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac4Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac4Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac4Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac4Pump.Name = "LblFrac4Pump"
        Me.LblFrac4Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac4Pump.TabIndex = 29
        Me.LblFrac4Pump.Text = "QWS2500"
        '
        'LblFrac4Trans
        '
        Me.LblFrac4Trans.AutoSize = True
        Me.LblFrac4Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac4Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac4Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac4Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac4Trans.Name = "LblFrac4Trans"
        Me.LblFrac4Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac4Trans.TabIndex = 28
        Me.LblFrac4Trans.Text = "ZF-8 TX"
        '
        'LblFrac4Engine
        '
        Me.LblFrac4Engine.AutoSize = True
        Me.LblFrac4Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac4Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac4Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac4Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac4Engine.Name = "LblFrac4Engine"
        Me.LblFrac4Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac4Engine.TabIndex = 27
        Me.LblFrac4Engine.Text = "D9620"
        '
        'LblFSPump4
        '
        Me.LblFSPump4.AutoSize = True
        Me.LblFSPump4.BackColor = System.Drawing.Color.Transparent
        Me.LblFSPump4.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSPump4.ForeColor = System.Drawing.Color.White
        Me.LblFSPump4.Location = New System.Drawing.Point(254, 63)
        Me.LblFSPump4.Name = "LblFSPump4"
        Me.LblFSPump4.Size = New System.Drawing.Size(55, 16)
        Me.LblFSPump4.TabIndex = 26
        Me.LblFSPump4.Text = "Bomba:"
        '
        'LblFSTrans4
        '
        Me.LblFSTrans4.AutoSize = True
        Me.LblFSTrans4.BackColor = System.Drawing.Color.Transparent
        Me.LblFSTrans4.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSTrans4.ForeColor = System.Drawing.Color.White
        Me.LblFSTrans4.Location = New System.Drawing.Point(115, 63)
        Me.LblFSTrans4.Name = "LblFSTrans4"
        Me.LblFSTrans4.Size = New System.Drawing.Size(75, 16)
        Me.LblFSTrans4.TabIndex = 25
        Me.LblFSTrans4.Text = "Trasmisión:"
        '
        'LblFSEngine4
        '
        Me.LblFSEngine4.AutoSize = True
        Me.LblFSEngine4.BackColor = System.Drawing.Color.Transparent
        Me.LblFSEngine4.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSEngine4.ForeColor = System.Drawing.Color.White
        Me.LblFSEngine4.Location = New System.Drawing.Point(-3, 63)
        Me.LblFSEngine4.Name = "LblFSEngine4"
        Me.LblFSEngine4.Size = New System.Drawing.Size(47, 16)
        Me.LblFSEngine4.TabIndex = 24
        Me.LblFSEngine4.Text = "Motor:"
        '
        'PanelFrac6
        '
        Me.PanelFrac6.Controls.Add(Me.BtnFrac6)
        Me.PanelFrac6.Controls.Add(Me.LblFrac6Pump)
        Me.PanelFrac6.Controls.Add(Me.LblFrac6Trans)
        Me.PanelFrac6.Controls.Add(Me.LblFrac6Engine)
        Me.PanelFrac6.Controls.Add(Me.LblFSPump6)
        Me.PanelFrac6.Controls.Add(Me.LblFSTrans6)
        Me.PanelFrac6.Controls.Add(Me.LblFSEngine6)
        Me.PanelFrac6.Location = New System.Drawing.Point(400, 211)
        Me.PanelFrac6.Name = "PanelFrac6"
        Me.PanelFrac6.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac6.TabIndex = 110
        Me.PanelFrac6.Tag = "5"
        Me.PanelFrac6.Visible = False
        '
        'BtnFrac6
        '
        Me.BtnFrac6.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac6.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac6.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac6.ForeColor = System.Drawing.Color.White
        Me.BtnFrac6.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac6.Name = "BtnFrac6"
        Me.BtnFrac6.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac6.TabIndex = 18
        Me.BtnFrac6.Tag = "5"
        Me.BtnFrac6.Text = "Fracturador6"
        '
        'LblFrac6Pump
        '
        Me.LblFrac6Pump.AutoSize = True
        Me.LblFrac6Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac6Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac6Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac6Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac6Pump.Name = "LblFrac6Pump"
        Me.LblFrac6Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac6Pump.TabIndex = 29
        Me.LblFrac6Pump.Text = "QWS2500"
        '
        'LblFrac6Trans
        '
        Me.LblFrac6Trans.AutoSize = True
        Me.LblFrac6Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac6Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac6Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac6Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac6Trans.Name = "LblFrac6Trans"
        Me.LblFrac6Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac6Trans.TabIndex = 28
        Me.LblFrac6Trans.Text = "ZF-8 TX"
        '
        'LblFrac6Engine
        '
        Me.LblFrac6Engine.AutoSize = True
        Me.LblFrac6Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac6Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac6Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac6Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac6Engine.Name = "LblFrac6Engine"
        Me.LblFrac6Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac6Engine.TabIndex = 27
        Me.LblFrac6Engine.Text = "D9620"
        '
        'LblFSPump6
        '
        Me.LblFSPump6.AutoSize = True
        Me.LblFSPump6.BackColor = System.Drawing.Color.Transparent
        Me.LblFSPump6.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSPump6.ForeColor = System.Drawing.Color.White
        Me.LblFSPump6.Location = New System.Drawing.Point(254, 63)
        Me.LblFSPump6.Name = "LblFSPump6"
        Me.LblFSPump6.Size = New System.Drawing.Size(55, 16)
        Me.LblFSPump6.TabIndex = 26
        Me.LblFSPump6.Text = "Bomba:"
        '
        'LblFSTrans6
        '
        Me.LblFSTrans6.AutoSize = True
        Me.LblFSTrans6.BackColor = System.Drawing.Color.Transparent
        Me.LblFSTrans6.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSTrans6.ForeColor = System.Drawing.Color.White
        Me.LblFSTrans6.Location = New System.Drawing.Point(115, 63)
        Me.LblFSTrans6.Name = "LblFSTrans6"
        Me.LblFSTrans6.Size = New System.Drawing.Size(75, 16)
        Me.LblFSTrans6.TabIndex = 25
        Me.LblFSTrans6.Text = "Trasmisión:"
        '
        'LblFSEngine6
        '
        Me.LblFSEngine6.AutoSize = True
        Me.LblFSEngine6.BackColor = System.Drawing.Color.Transparent
        Me.LblFSEngine6.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSEngine6.ForeColor = System.Drawing.Color.White
        Me.LblFSEngine6.Location = New System.Drawing.Point(-3, 63)
        Me.LblFSEngine6.Name = "LblFSEngine6"
        Me.LblFSEngine6.Size = New System.Drawing.Size(47, 16)
        Me.LblFSEngine6.TabIndex = 24
        Me.LblFSEngine6.Text = "Motor:"
        '
        'PanelFrac3
        '
        Me.PanelFrac3.Controls.Add(Me.BtnFrac3)
        Me.PanelFrac3.Controls.Add(Me.LblFrac3Pump)
        Me.PanelFrac3.Controls.Add(Me.LblFrac3Trans)
        Me.PanelFrac3.Controls.Add(Me.LblFrac3Engine)
        Me.PanelFrac3.Controls.Add(Me.LblFSPump3)
        Me.PanelFrac3.Controls.Add(Me.LblFSTrans3)
        Me.PanelFrac3.Controls.Add(Me.LblFSEngine3)
        Me.PanelFrac3.Location = New System.Drawing.Point(4, 108)
        Me.PanelFrac3.Name = "PanelFrac3"
        Me.PanelFrac3.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac3.TabIndex = 109
        Me.PanelFrac3.Tag = "2"
        Me.PanelFrac3.Visible = False
        '
        'BtnFrac3
        '
        Me.BtnFrac3.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac3.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac3.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac3.ForeColor = System.Drawing.Color.White
        Me.BtnFrac3.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac3.Name = "BtnFrac3"
        Me.BtnFrac3.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac3.TabIndex = 18
        Me.BtnFrac3.Tag = "2"
        Me.BtnFrac3.Text = "Fracturador3"
        '
        'LblFrac3Pump
        '
        Me.LblFrac3Pump.AutoSize = True
        Me.LblFrac3Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac3Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac3Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac3Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac3Pump.Name = "LblFrac3Pump"
        Me.LblFrac3Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac3Pump.TabIndex = 29
        Me.LblFrac3Pump.Text = "QWS2500"
        '
        'LblFrac3Trans
        '
        Me.LblFrac3Trans.AutoSize = True
        Me.LblFrac3Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac3Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac3Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac3Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac3Trans.Name = "LblFrac3Trans"
        Me.LblFrac3Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac3Trans.TabIndex = 28
        Me.LblFrac3Trans.Text = "ZF-8 TX"
        '
        'LblFrac3Engine
        '
        Me.LblFrac3Engine.AutoSize = True
        Me.LblFrac3Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac3Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac3Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac3Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac3Engine.Name = "LblFrac3Engine"
        Me.LblFrac3Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac3Engine.TabIndex = 27
        Me.LblFrac3Engine.Text = "D9620"
        '
        'LblFSPump3
        '
        Me.LblFSPump3.AutoSize = True
        Me.LblFSPump3.BackColor = System.Drawing.Color.Transparent
        Me.LblFSPump3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSPump3.ForeColor = System.Drawing.Color.White
        Me.LblFSPump3.Location = New System.Drawing.Point(254, 63)
        Me.LblFSPump3.Name = "LblFSPump3"
        Me.LblFSPump3.Size = New System.Drawing.Size(55, 16)
        Me.LblFSPump3.TabIndex = 26
        Me.LblFSPump3.Text = "Bomba:"
        '
        'LblFSTrans3
        '
        Me.LblFSTrans3.AutoSize = True
        Me.LblFSTrans3.BackColor = System.Drawing.Color.Transparent
        Me.LblFSTrans3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSTrans3.ForeColor = System.Drawing.Color.White
        Me.LblFSTrans3.Location = New System.Drawing.Point(115, 63)
        Me.LblFSTrans3.Name = "LblFSTrans3"
        Me.LblFSTrans3.Size = New System.Drawing.Size(75, 16)
        Me.LblFSTrans3.TabIndex = 25
        Me.LblFSTrans3.Text = "Trasmisión:"
        '
        'LblFSEngine3
        '
        Me.LblFSEngine3.AutoSize = True
        Me.LblFSEngine3.BackColor = System.Drawing.Color.Transparent
        Me.LblFSEngine3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSEngine3.ForeColor = System.Drawing.Color.White
        Me.LblFSEngine3.Location = New System.Drawing.Point(-3, 63)
        Me.LblFSEngine3.Name = "LblFSEngine3"
        Me.LblFSEngine3.Size = New System.Drawing.Size(47, 16)
        Me.LblFSEngine3.TabIndex = 24
        Me.LblFSEngine3.Text = "Motor:"
        '
        'PanelFrac2
        '
        Me.PanelFrac2.Controls.Add(Me.BtnFrac2)
        Me.PanelFrac2.Controls.Add(Me.LblFrac2Pump)
        Me.PanelFrac2.Controls.Add(Me.LblFrac2Trans)
        Me.PanelFrac2.Controls.Add(Me.LblFrac2Engine)
        Me.PanelFrac2.Controls.Add(Me.LblFSPump2)
        Me.PanelFrac2.Controls.Add(Me.LblFSTrans2)
        Me.PanelFrac2.Controls.Add(Me.LblFSEngine2)
        Me.PanelFrac2.Location = New System.Drawing.Point(400, 5)
        Me.PanelFrac2.Name = "PanelFrac2"
        Me.PanelFrac2.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac2.TabIndex = 108
        Me.PanelFrac2.Tag = "1"
        Me.PanelFrac2.Visible = False
        '
        'BtnFrac2
        '
        Me.BtnFrac2.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac2.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac2.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac2.ForeColor = System.Drawing.Color.White
        Me.BtnFrac2.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac2.Name = "BtnFrac2"
        Me.BtnFrac2.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac2.TabIndex = 18
        Me.BtnFrac2.Tag = "1"
        Me.BtnFrac2.Text = "Fracturador2"
        '
        'LblFrac2Pump
        '
        Me.LblFrac2Pump.AutoSize = True
        Me.LblFrac2Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac2Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac2Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac2Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac2Pump.Name = "LblFrac2Pump"
        Me.LblFrac2Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac2Pump.TabIndex = 29
        Me.LblFrac2Pump.Text = "QWS2500"
        '
        'LblFrac2Trans
        '
        Me.LblFrac2Trans.AutoSize = True
        Me.LblFrac2Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac2Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac2Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac2Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac2Trans.Name = "LblFrac2Trans"
        Me.LblFrac2Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac2Trans.TabIndex = 28
        Me.LblFrac2Trans.Text = "ZF-8 TX"
        '
        'LblFrac2Engine
        '
        Me.LblFrac2Engine.AutoSize = True
        Me.LblFrac2Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac2Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac2Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac2Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac2Engine.Name = "LblFrac2Engine"
        Me.LblFrac2Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac2Engine.TabIndex = 27
        Me.LblFrac2Engine.Text = "D9620"
        '
        'LblFSPump2
        '
        Me.LblFSPump2.AutoSize = True
        Me.LblFSPump2.BackColor = System.Drawing.Color.Transparent
        Me.LblFSPump2.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSPump2.ForeColor = System.Drawing.Color.White
        Me.LblFSPump2.Location = New System.Drawing.Point(254, 63)
        Me.LblFSPump2.Name = "LblFSPump2"
        Me.LblFSPump2.Size = New System.Drawing.Size(55, 16)
        Me.LblFSPump2.TabIndex = 26
        Me.LblFSPump2.Text = "Bomba:"
        '
        'LblFSTrans2
        '
        Me.LblFSTrans2.AutoSize = True
        Me.LblFSTrans2.BackColor = System.Drawing.Color.Transparent
        Me.LblFSTrans2.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSTrans2.ForeColor = System.Drawing.Color.White
        Me.LblFSTrans2.Location = New System.Drawing.Point(115, 63)
        Me.LblFSTrans2.Name = "LblFSTrans2"
        Me.LblFSTrans2.Size = New System.Drawing.Size(75, 16)
        Me.LblFSTrans2.TabIndex = 25
        Me.LblFSTrans2.Text = "Trasmisión:"
        '
        'LblFSEngine2
        '
        Me.LblFSEngine2.AutoSize = True
        Me.LblFSEngine2.BackColor = System.Drawing.Color.Transparent
        Me.LblFSEngine2.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSEngine2.ForeColor = System.Drawing.Color.White
        Me.LblFSEngine2.Location = New System.Drawing.Point(-3, 63)
        Me.LblFSEngine2.Name = "LblFSEngine2"
        Me.LblFSEngine2.Size = New System.Drawing.Size(47, 16)
        Me.LblFSEngine2.TabIndex = 24
        Me.LblFSEngine2.Text = "Motor:"
        '
        'PanelFrac1
        '
        Me.PanelFrac1.Controls.Add(Me.BtnFrac1)
        Me.PanelFrac1.Controls.Add(Me.LblFrac1Pump)
        Me.PanelFrac1.Controls.Add(Me.LblFrac1Trans)
        Me.PanelFrac1.Controls.Add(Me.LblFrac1Engine)
        Me.PanelFrac1.Controls.Add(Me.LblFSPump1)
        Me.PanelFrac1.Controls.Add(Me.LblFSTrans1)
        Me.PanelFrac1.Controls.Add(Me.LblFSEngine1)
        Me.PanelFrac1.Location = New System.Drawing.Point(4, 5)
        Me.PanelFrac1.Name = "PanelFrac1"
        Me.PanelFrac1.Size = New System.Drawing.Size(377, 81)
        Me.PanelFrac1.TabIndex = 107
        Me.PanelFrac1.Tag = "0"
        Me.PanelFrac1.Visible = False
        '
        'BtnFrac1
        '
        Me.BtnFrac1.BackColor = System.Drawing.Color.Transparent
        Me.BtnFrac1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFrac1.FillColor = System.Drawing.Color.Transparent
        Me.BtnFrac1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnFrac1.ForeColor = System.Drawing.Color.White
        Me.BtnFrac1.Location = New System.Drawing.Point(0, 0)
        Me.BtnFrac1.Name = "BtnFrac1"
        Me.BtnFrac1.Size = New System.Drawing.Size(377, 81)
        Me.BtnFrac1.TabIndex = 18
        Me.BtnFrac1.Tag = "0"
        Me.BtnFrac1.Text = "Fracturador1"
        '
        'LblFrac1Pump
        '
        Me.LblFrac1Pump.AutoSize = True
        Me.LblFrac1Pump.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac1Pump.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac1Pump.ForeColor = System.Drawing.Color.White
        Me.LblFrac1Pump.Location = New System.Drawing.Point(309, 63)
        Me.LblFrac1Pump.Name = "LblFrac1Pump"
        Me.LblFrac1Pump.Size = New System.Drawing.Size(67, 16)
        Me.LblFrac1Pump.TabIndex = 29
        Me.LblFrac1Pump.Text = "QWS2500"
        '
        'LblFrac1Trans
        '
        Me.LblFrac1Trans.AutoSize = True
        Me.LblFrac1Trans.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac1Trans.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac1Trans.ForeColor = System.Drawing.Color.White
        Me.LblFrac1Trans.Location = New System.Drawing.Point(189, 63)
        Me.LblFrac1Trans.Name = "LblFrac1Trans"
        Me.LblFrac1Trans.Size = New System.Drawing.Size(54, 16)
        Me.LblFrac1Trans.TabIndex = 28
        Me.LblFrac1Trans.Text = "ZF-8 TX"
        '
        'LblFrac1Engine
        '
        Me.LblFrac1Engine.AutoSize = True
        Me.LblFrac1Engine.BackColor = System.Drawing.Color.Transparent
        Me.LblFrac1Engine.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFrac1Engine.ForeColor = System.Drawing.Color.White
        Me.LblFrac1Engine.Location = New System.Drawing.Point(42, 63)
        Me.LblFrac1Engine.Name = "LblFrac1Engine"
        Me.LblFrac1Engine.Size = New System.Drawing.Size(46, 16)
        Me.LblFrac1Engine.TabIndex = 27
        Me.LblFrac1Engine.Text = "D9620"
        '
        'LblFSPump1
        '
        Me.LblFSPump1.AutoSize = True
        Me.LblFSPump1.BackColor = System.Drawing.Color.Transparent
        Me.LblFSPump1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSPump1.ForeColor = System.Drawing.Color.White
        Me.LblFSPump1.Location = New System.Drawing.Point(254, 63)
        Me.LblFSPump1.Name = "LblFSPump1"
        Me.LblFSPump1.Size = New System.Drawing.Size(55, 16)
        Me.LblFSPump1.TabIndex = 26
        Me.LblFSPump1.Text = "Bomba:"
        '
        'LblFSTrans1
        '
        Me.LblFSTrans1.AutoSize = True
        Me.LblFSTrans1.BackColor = System.Drawing.Color.Transparent
        Me.LblFSTrans1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSTrans1.ForeColor = System.Drawing.Color.White
        Me.LblFSTrans1.Location = New System.Drawing.Point(115, 63)
        Me.LblFSTrans1.Name = "LblFSTrans1"
        Me.LblFSTrans1.Size = New System.Drawing.Size(75, 16)
        Me.LblFSTrans1.TabIndex = 25
        Me.LblFSTrans1.Text = "Trasmisión:"
        '
        'LblFSEngine1
        '
        Me.LblFSEngine1.AutoSize = True
        Me.LblFSEngine1.BackColor = System.Drawing.Color.Transparent
        Me.LblFSEngine1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblFSEngine1.ForeColor = System.Drawing.Color.White
        Me.LblFSEngine1.Location = New System.Drawing.Point(-3, 63)
        Me.LblFSEngine1.Name = "LblFSEngine1"
        Me.LblFSEngine1.Size = New System.Drawing.Size(47, 16)
        Me.LblFSEngine1.TabIndex = 24
        Me.LblFSEngine1.Text = "Motor:"
        '
        'Guna2Separator5
        '
        Me.Guna2Separator5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator5.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator5.Location = New System.Drawing.Point(1, 298)
        Me.Guna2Separator5.Name = "Guna2Separator5"
        Me.Guna2Separator5.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator5.TabIndex = 106
        Me.Guna2Separator5.UseTransparentBackground = True
        '
        'Guna2Separator6
        '
        Me.Guna2Separator6.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator6.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator6.Location = New System.Drawing.Point(1, 195)
        Me.Guna2Separator6.Name = "Guna2Separator6"
        Me.Guna2Separator6.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator6.TabIndex = 105
        Me.Guna2Separator6.UseTransparentBackground = True
        '
        'Guna2Separator4
        '
        Me.Guna2Separator4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator4.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator4.Location = New System.Drawing.Point(400, 92)
        Me.Guna2Separator4.Name = "Guna2Separator4"
        Me.Guna2Separator4.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator4.TabIndex = 104
        Me.Guna2Separator4.UseTransparentBackground = True
        '
        'Guna2Separator3
        '
        Me.Guna2Separator3.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator3.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator3.Location = New System.Drawing.Point(400, 298)
        Me.Guna2Separator3.Name = "Guna2Separator3"
        Me.Guna2Separator3.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator3.TabIndex = 103
        Me.Guna2Separator3.UseTransparentBackground = True
        '
        'Guna2Separator2
        '
        Me.Guna2Separator2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator2.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator2.Location = New System.Drawing.Point(400, 195)
        Me.Guna2Separator2.Name = "Guna2Separator2"
        Me.Guna2Separator2.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator2.TabIndex = 102
        Me.Guna2Separator2.UseTransparentBackground = True
        '
        'Guna2VSeparator1
        '
        Me.Guna2VSeparator1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2VSeparator1.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2VSeparator1.Location = New System.Drawing.Point(384, 3)
        Me.Guna2VSeparator1.Name = "Guna2VSeparator1"
        Me.Guna2VSeparator1.Size = New System.Drawing.Size(10, 1196)
        Me.Guna2VSeparator1.TabIndex = 101
        Me.Guna2VSeparator1.UseTransparentBackground = True
        '
        'Guna2Separator1
        '
        Me.Guna2Separator1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Separator1.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Separator1.Location = New System.Drawing.Point(1, 92)
        Me.Guna2Separator1.Name = "Guna2Separator1"
        Me.Guna2Separator1.Size = New System.Drawing.Size(377, 10)
        Me.Guna2Separator1.TabIndex = 100
        Me.Guna2Separator1.UseTransparentBackground = True
        '
        'PbInicio
        '
        Me.PbInicio.Location = New System.Drawing.Point(0, 550)
        Me.PbInicio.Maximum = 24
        Me.PbInicio.Name = "PbInicio"
        Me.PbInicio.Size = New System.Drawing.Size(800, 4)
        Me.PbInicio.Step = 1
        Me.PbInicio.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.PbInicio.TabIndex = 30
        Me.PbInicio.Visible = False
        '
        'FracSelect
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(800, 553)
        Me.Controls.Add(Me.PbInicio)
        Me.Controls.Add(Me.MainPanel)
        Me.Controls.Add(Me.TitlePanel)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FracSelect"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de fracturadores"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.MainPanel.ResumeLayout(False)
        Me.PanelFrac23.ResumeLayout(False)
        Me.PanelFrac23.PerformLayout()
        Me.PanelFrac21.ResumeLayout(False)
        Me.PanelFrac21.PerformLayout()
        Me.PanelFrac24.ResumeLayout(False)
        Me.PanelFrac24.PerformLayout()
        Me.PanelFrac22.ResumeLayout(False)
        Me.PanelFrac22.PerformLayout()
        Me.PanelFrac19.ResumeLayout(False)
        Me.PanelFrac19.PerformLayout()
        Me.PanelFrac17.ResumeLayout(False)
        Me.PanelFrac17.PerformLayout()
        Me.PanelFrac13.ResumeLayout(False)
        Me.PanelFrac13.PerformLayout()
        Me.PanelFrac15.ResumeLayout(False)
        Me.PanelFrac15.PerformLayout()
        Me.PanelFrac11.ResumeLayout(False)
        Me.PanelFrac11.PerformLayout()
        Me.PanelFrac20.ResumeLayout(False)
        Me.PanelFrac20.PerformLayout()
        Me.PanelFrac18.ResumeLayout(False)
        Me.PanelFrac18.PerformLayout()
        Me.PanelFrac14.ResumeLayout(False)
        Me.PanelFrac14.PerformLayout()
        Me.PanelFrac16.ResumeLayout(False)
        Me.PanelFrac16.PerformLayout()
        Me.PanelFrac12.ResumeLayout(False)
        Me.PanelFrac12.PerformLayout()
        Me.PanelFrac10.ResumeLayout(False)
        Me.PanelFrac10.PerformLayout()
        Me.PanelFrac9.ResumeLayout(False)
        Me.PanelFrac9.PerformLayout()
        Me.PanelFrac5.ResumeLayout(False)
        Me.PanelFrac5.PerformLayout()
        Me.PanelFrac8.ResumeLayout(False)
        Me.PanelFrac8.PerformLayout()
        Me.PanelFrac7.ResumeLayout(False)
        Me.PanelFrac7.PerformLayout()
        Me.PanelFrac4.ResumeLayout(False)
        Me.PanelFrac4.PerformLayout()
        Me.PanelFrac6.ResumeLayout(False)
        Me.PanelFrac6.PerformLayout()
        Me.PanelFrac3.ResumeLayout(False)
        Me.PanelFrac3.PerformLayout()
        Me.PanelFrac2.ResumeLayout(False)
        Me.PanelFrac2.PerformLayout()
        Me.PanelFrac1.ResumeLayout(False)
        Me.PanelFrac1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents CloseFracSelect As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Title As Label
    Friend WithEvents MainPanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2Separator14 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator15 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents PanelFrac19 As Panel
    Friend WithEvents BtnFrac19 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac19Pump As Label
    Friend WithEvents LblFrac19Trans As Label
    Friend WithEvents LblFrac19Engine As Label
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents PanelFrac17 As Panel
    Friend WithEvents BtnFrac17 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac17Pump As Label
    Friend WithEvents LblFrac17Trans As Label
    Friend WithEvents LblFrac17Engine As Label
    Friend WithEvents Label52 As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents PanelFrac13 As Panel
    Friend WithEvents BtnFrac13 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac13Pump As Label
    Friend WithEvents LblFrac13Trans As Label
    Friend WithEvents LblFrac13Engine As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents PanelFrac15 As Panel
    Friend WithEvents BtnFrac15 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac15Pump As Label
    Friend WithEvents LblFrac15Trans As Label
    Friend WithEvents LblFrac15Engine As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents PanelFrac11 As Panel
    Friend WithEvents BtnFrac11 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac11Pump As Label
    Friend WithEvents LblFrac11Trans As Label
    Friend WithEvents LblFrac11Engine As Label
    Friend WithEvents Label70 As Label
    Friend WithEvents Label71 As Label
    Friend WithEvents Label72 As Label
    Friend WithEvents Guna2Separator16 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator17 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator18 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator13 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator9 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents PanelFrac20 As Panel
    Friend WithEvents BtnFrac20 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac20Pump As Label
    Friend WithEvents LblFrac20Trans As Label
    Friend WithEvents LblFrac20Engine As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents PanelFrac18 As Panel
    Friend WithEvents BtnFrac18 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac18Pump As Label
    Friend WithEvents LblFrac18Trans As Label
    Friend WithEvents LblFrac18Engine As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents PanelFrac14 As Panel
    Friend WithEvents BtnFrac14 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac14Pump As Label
    Friend WithEvents LblFrac14Trans As Label
    Friend WithEvents LblFrac14Engine As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents PanelFrac16 As Panel
    Friend WithEvents BtnFrac16 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac16Pump As Label
    Friend WithEvents LblFrac16Trans As Label
    Friend WithEvents LblFrac16Engine As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents PanelFrac12 As Panel
    Friend WithEvents BtnFrac12 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac12Pump As Label
    Friend WithEvents LblFrac12Trans As Label
    Friend WithEvents LblFrac12Engine As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Guna2Separator10 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator11 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator12 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator8 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator7 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents PanelFrac10 As Panel
    Friend WithEvents BtnFrac10 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac10Pump As Label
    Friend WithEvents LblFrac10Trans As Label
    Friend WithEvents LblFrac10Engine As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents PanelFrac9 As Panel
    Friend WithEvents BtnFrac9 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac9Pump As Label
    Friend WithEvents LblFrac9Trans As Label
    Friend WithEvents LblFrac9Engine As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents PanelFrac5 As Panel
    Friend WithEvents BtnFrac5 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac5Pump As Label
    Friend WithEvents LblFrac5Trans As Label
    Friend WithEvents LblFrac5Engine As Label
    Friend WithEvents LblFSPump5 As Label
    Friend WithEvents LblFSTrans5 As Label
    Friend WithEvents LblFSEngine5 As Label
    Friend WithEvents PanelFrac8 As Panel
    Friend WithEvents BtnFrac8 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac8Pump As Label
    Friend WithEvents LblFrac8Trans As Label
    Friend WithEvents LblFrac8Engine As Label
    Friend WithEvents LblFSPump8 As Label
    Friend WithEvents LblFSTrans8 As Label
    Friend WithEvents LblFSEngine8 As Label
    Friend WithEvents PanelFrac7 As Panel
    Friend WithEvents BtnFrac7 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac7Pump As Label
    Friend WithEvents LblFrac7Trans As Label
    Friend WithEvents LblFrac7Engine As Label
    Friend WithEvents LblFSPump7 As Label
    Friend WithEvents LblFSTrans7 As Label
    Friend WithEvents LblFSEngine7 As Label
    Friend WithEvents PanelFrac4 As Panel
    Friend WithEvents BtnFrac4 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac4Pump As Label
    Friend WithEvents LblFrac4Trans As Label
    Friend WithEvents LblFrac4Engine As Label
    Friend WithEvents LblFSPump4 As Label
    Friend WithEvents LblFSTrans4 As Label
    Friend WithEvents LblFSEngine4 As Label
    Friend WithEvents PanelFrac6 As Panel
    Friend WithEvents BtnFrac6 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac6Pump As Label
    Friend WithEvents LblFrac6Trans As Label
    Friend WithEvents LblFrac6Engine As Label
    Friend WithEvents LblFSPump6 As Label
    Friend WithEvents LblFSTrans6 As Label
    Friend WithEvents LblFSEngine6 As Label
    Friend WithEvents PanelFrac3 As Panel
    Friend WithEvents BtnFrac3 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac3Pump As Label
    Friend WithEvents LblFrac3Trans As Label
    Friend WithEvents LblFrac3Engine As Label
    Friend WithEvents LblFSPump3 As Label
    Friend WithEvents LblFSTrans3 As Label
    Friend WithEvents LblFSEngine3 As Label
    Friend WithEvents PanelFrac2 As Panel
    Friend WithEvents BtnFrac2 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac2Pump As Label
    Friend WithEvents LblFrac2Trans As Label
    Friend WithEvents LblFrac2Engine As Label
    Friend WithEvents LblFSPump2 As Label
    Friend WithEvents LblFSTrans2 As Label
    Friend WithEvents LblFSEngine2 As Label
    Friend WithEvents PanelFrac1 As Panel
    Friend WithEvents BtnFrac1 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac1Pump As Label
    Friend WithEvents LblFrac1Trans As Label
    Friend WithEvents LblFrac1Engine As Label
    Friend WithEvents LblFSPump1 As Label
    Friend WithEvents LblFSTrans1 As Label
    Friend WithEvents LblFSEngine1 As Label
    Friend WithEvents Guna2Separator5 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator6 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator4 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator3 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator2 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2VSeparator1 As Guna.UI2.WinForms.Guna2VSeparator
    Friend WithEvents Guna2Separator1 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator21 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents PanelFrac23 As Panel
    Friend WithEvents BtnFrac23 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac23Pump As Label
    Friend WithEvents LblFrac23Trans As Label
    Friend WithEvents LblFrac23Engine As Label
    Friend WithEvents Label88 As Label
    Friend WithEvents Label89 As Label
    Friend WithEvents Label90 As Label
    Friend WithEvents PanelFrac21 As Panel
    Friend WithEvents BtnFrac21 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac21Pump As Label
    Friend WithEvents LblFrac21Trans As Label
    Friend WithEvents LblFrac21Engine As Label
    Friend WithEvents Label94 As Label
    Friend WithEvents Label95 As Label
    Friend WithEvents Label96 As Label
    Friend WithEvents Guna2Separator22 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents Guna2Separator19 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents PanelFrac24 As Panel
    Friend WithEvents BtnFrac24 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac24Pump As Label
    Friend WithEvents LblFrac24Trans As Label
    Friend WithEvents LblFrac24Engine As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents Label77 As Label
    Friend WithEvents Label78 As Label
    Friend WithEvents PanelFrac22 As Panel
    Friend WithEvents BtnFrac22 As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents LblFrac22Pump As Label
    Friend WithEvents LblFrac22Trans As Label
    Friend WithEvents LblFrac22Engine As Label
    Friend WithEvents Label82 As Label
    Friend WithEvents Label83 As Label
    Friend WithEvents Label84 As Label
    Friend WithEvents Guna2Separator20 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents BtnReload As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PbInicio As ProgressBar
    Friend WithEvents BtnManualReload As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
End Class
