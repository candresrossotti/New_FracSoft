﻿
Public Class FracSelect
    Private Sub FracSelect_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CenterObject(Title)
        If OneConnected = True Then
            For i = 0 To ISystem.Devices - 1
                Dim ActiveFrac As Boolean = FracData.BoolVFrac("ActiveFrac_" + ISystem.GetActiveFrac(i).ToString())
                Dim SenderButton As Guna.UI2.WinForms.Guna2Button = TryCast(Controls.Find("BtnFrac" + (i + 1).ToString(), True)(0), Guna.UI2.WinForms.Guna2Button)
                If ActiveFrac = True Then
                    SenderButton.FillColor = Color.LimeGreen
                Else
                    SenderButton.FillColor = Color.Transparent
                End If
            Next
        End If

        'For ThisButton = 1 To 8
        '    Dim SenderButton As Guna.UI2.WinForms.Guna2Button = TryCast(Controls.Find("BtnFrac" + (ThisButton).ToString(), True)(0), Guna.UI2.WinForms.Guna2Button)
        '    SenderButton.UseTransparentBackground = True
        'Next

        AddHandler IFracSystem.ScanStep, AddressOf PbIncrement
        AddHandler IFracSystem.InitializationFinished, AddressOf PbFinished
    End Sub



    ''Sub para incrementar el progressbar
    Private Sub PbIncrement()
        PbInicio.Increment(1)
    End Sub

    Private Sub PbFinished()
        PbInicio.Visible = False
        PbInicio.Value = 0
    End Sub

    Private Sub BtnClick(sender As Object, e As EventArgs) Handles BtnFrac1.Click, BtnFrac2.Click, BtnFrac3.Click, BtnFrac4.Click, BtnFrac5.Click, BtnFrac6.Click, BtnFrac7.Click,
                                                            BtnFrac8.Click, BtnFrac9.Click, BtnFrac10.Click, BtnFrac11.Click, BtnFrac12.Click, BtnFrac13.Click, BtnFrac14.Click,
                                                            BtnFrac15.Click, BtnFrac16.Click, BtnFrac17.Click, BtnFrac18.Click, BtnFrac19.Click, BtnFrac20.Click, BtnFrac21.Click,
                                                            BtnFrac22.Click, BtnFrac23.Click, BtnFrac24.Click

        CurrentFrac = ISystem.GetFracID(sender.Text)
        CurrentFracName = ISystem.GetFracName(currentFrac)
        If AllowMultipleConnections = True Then
            Connect()
        ElseIf AllowMultipleConnections = False And FracData.BoolVFrac("ActiveFrac_" + currentFrac.ToString()) = False Then
            Connect()
        End If

    End Sub

    Private Sub CloseWindow(sender As Object, e As EventArgs) Handles CloseFracSelect.Click
        If OneConnected = False Then
            Principal.Close()
        End If
    End Sub

    Private Sub Connect()
        Try
            ''Nos conectamos al moxa
            Dim Req As Boolean = ISystem.Connect(CurrentFrac)
            If Req = True Then
                FConfig.UpdateISystem(ISystem)      ''Actualizamos la clase FConfig
                ''Iniciamos el fracturador y guardamos los datos del equipo en memoria

                ISystem.InitializeSystem(currentFrac)                           ''ESTO NO ESTA REPETIDO?=?

                If FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString()) Is Nothing Then FracData.SelectedEngines("SelectedEngine_" + currentFrac.ToString()) = EngineSelect.SelectedEngine
                If FracData.SelectedPumps("SelectedPump_" + currentFrac.ToString()) Is Nothing Then FracData.SelectedPumps("SelectedPump_" + currentFrac.ToString()) = PumpSelect.SelectedPump
                If FracData.SelectedTransmissions("SelectedTransmission_" + currentFrac.ToString()) Is Nothing Then FracData.SelectedTransmissions("SelectedTransmission_" + currentFrac.ToString()) = TransSelect.SelectedTransmisison
                If Not ConnectedFracsID.Contains(CurrentFrac) Then
                    MaxFracs += 1
                    ConnectedFracsID.Add(CurrentFrac)
                End If


                DialogResult = DialogResult.OK
            End If

            FracData.BoolVFrac("ActiveFrac_" + currentFrac.ToString()) = Req

            ''Si es la primera conexion abrimos todo, sino salteamos esto
            If OneConnected = False Then
                ''Habilitamos todas las ventanas
                Principal.Enabled = True
                Secundaria.Enabled = True
                Tercera.Enabled = True
                Dim currentScreen As Integer = My.Settings.Item("currentScreens")
                ''Agregamos el primer control
                AddFirstControl(currentScreen)
                Inicio.LastTime = Now
                Inicio.TmrTotal.Start()
                OneConnected = Req
            End If
        Catch ex As Exception
            LogError(ex)
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Reconnect")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub

    Private Sub MainPanel_MouseEnter(sender As Object, e As EventArgs) Handles MainPanel.MouseWheel, MainPanel.Scroll
        If MainPanel.Height + Math.Abs(MainPanel.AutoScrollPosition.Y) > MainPanel.VerticalScroll.Maximum Then
            MainPanel.AutoScrollPosition = New Point(0, MainPanel.VerticalScroll.Maximum - MainPanel.Height)
            Refresh()
        End If
    End Sub

    Private Sub BtnReload_Click(sender As Object, e As EventArgs) Handles BtnReload.Click
        PbInicio.Visible = True
        Dim status As Boolean = ISystem.AutoScan(True)                              ''Realizamos el auto escaneo
        PbInicio.Value = 0
        PbInicio.Visible = False
    End Sub

    Private Sub Guna2Button1_Click(sender As Object, e As EventArgs)
        If ManualConnection.ShowDialog() = DialogResult.OK Then
            ''Si es la primera conexion abrimos todo, sino salteamos esto
            If OneConnected = False Then
                ''Habilitamos todas las ventanas
                Principal.Enabled = True
                Secundaria.Enabled = True
                Tercera.Enabled = True
                Dim currentScreen As Integer = My.Settings.Item("currentScreens")
                ''Agregamos el primer control
                AddFirstControl(currentScreen)
                Inicio.LastTime = Now
                Inicio.TmrTotal.Start()
                OneConnected = True
                DialogResult = DialogResult.OK
            End If
        End If
    End Sub

    Private Sub BtnManualReload_Click(sender As Object, e As EventArgs) Handles BtnManualReload.Click
        PbInicio.Visible = True
        Dim status As Boolean = ISystem.AutoScan(False)                              ''Realizamos el auto escaneo
        PbInicio.Value = 0
        PbInicio.Visible = False
    End Sub

End Class