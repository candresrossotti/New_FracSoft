﻿Public Class ManualConnection
    Private ID As Integer
    Private TestIP As Net.IPAddress
    Private Engine As Dictionary(Of String, Object)
    Private Transmission As Dictionary(Of String, Object)
    Private Pump As Dictionary(Of String, Object)
    Private Sub BtnConectar_Click(sender As Object, e As EventArgs) Handles BtnConectar.Click
        'If Net.IPAddress.TryParse(TbIP.Text, TestIP) And Not String.IsNullOrEmpty(TbID.Text) Then
        '    ID = TbID.Text
        '    If ISystem.ManualConnect(TbIP.Text, ID) Then

        '        FConfig.UpdateISystem(ISystem)      ''Actualizamos la clase FConfig

        '        If FracData.SelectedEngines("SelectedEngine_" + ID.ToString()) Is Nothing Then FracData.SelectedEngines("SelectedEngine_" + ID.ToString()) = Engine
        '        If FracData.SelectedPumps("SelectedPump_" + ID.ToString()) Is Nothing Then FracData.SelectedPumps("SelectedPump_" + ID.ToString()) = Pump
        '        If FracData.SelectedTransmissions("SelectedTransmission_" + ID.ToString()) Is Nothing Then FracData.SelectedTransmissions("SelectedTransmission_" + ID.ToString()) = Transmission
        '        If Not ConnectedFracsID.Contains(ID) Then MaxFracs += 1
        '        ConnectedFracsID.Add(ID)

        '        FracData.BoolVFrac("ActiveFrac_" + ID.ToString()) = True

        '        CurrentFrac = ID
        '        CurrentFracName = TbNombre.Text

        '        DialogResult = DialogResult.OK
        '    End If
        'End If
    End Sub

    Private Sub ManualConnection_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

    End Sub

    Dim Regex As New Text.RegularExpressions.Regex("[^0-9]+")
    Private Sub TbID_TextChanged(sender As Object, e As EventArgs)
        If Regex.Match(sender.Text).Success Then
            Dim Number As Integer
            Integer.TryParse(System.Text.RegularExpressions.Regex.Replace(sender.Text, "[^\d]", ""), Number)
            sender.Text = Number.ToString
            sender.SelectionStart = sender.Text.Length()
        End If
    End Sub

End Class