﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ManualConnection
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.CloseFracSelect = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.BtnConectar = New Guna.UI2.WinForms.Guna2Button()
        Me.TbNombre = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LblTransmision = New System.Windows.Forms.Label()
        Me.LblBomba = New System.Windows.Forms.Label()
        Me.LblMotor = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.CloseFracSelect)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(390, 38)
        Me.TitlePanel.TabIndex = 2
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(91, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(208, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Conexión manual"
        '
        'CloseFracSelect
        '
        Me.CloseFracSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseFracSelect.BackColor = System.Drawing.Color.Transparent
        Me.CloseFracSelect.Cursor = System.Windows.Forms.Cursors.Default
        Me.CloseFracSelect.FillColor = System.Drawing.Color.Transparent
        Me.CloseFracSelect.IconColor = System.Drawing.Color.White
        Me.CloseFracSelect.Location = New System.Drawing.Point(342, 5)
        Me.CloseFracSelect.Name = "CloseFracSelect"
        Me.CloseFracSelect.Size = New System.Drawing.Size(45, 29)
        Me.CloseFracSelect.TabIndex = 0
        Me.CloseFracSelect.UseTransparentBackground = True
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.TitlePanel
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.Title
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'BtnConectar
        '
        Me.BtnConectar.BackColor = System.Drawing.Color.Transparent
        Me.BtnConectar.BorderColor = System.Drawing.Color.White
        Me.BtnConectar.BorderRadius = 5
        Me.BtnConectar.BorderThickness = 1
        Me.BtnConectar.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.BtnConectar.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.BtnConectar.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.BtnConectar.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.BtnConectar.FillColor = System.Drawing.Color.Transparent
        Me.BtnConectar.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnConectar.ForeColor = System.Drawing.Color.White
        Me.BtnConectar.Image = Global.sFRAC.My.Resources.Resources.right_arrow1
        Me.BtnConectar.ImageAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.BtnConectar.ImageSize = New System.Drawing.Size(36, 36)
        Me.BtnConectar.Location = New System.Drawing.Point(122, 263)
        Me.BtnConectar.Name = "BtnConectar"
        Me.BtnConectar.Size = New System.Drawing.Size(147, 38)
        Me.BtnConectar.TabIndex = 32
        Me.BtnConectar.Text = "Conectar"
        Me.BtnConectar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'TbNombre
        '
        Me.TbNombre.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbNombre.DefaultText = ""
        Me.TbNombre.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbNombre.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbNombre.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbNombre.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbNombre.Enabled = False
        Me.TbNombre.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbNombre.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.TbNombre.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbNombre.Location = New System.Drawing.Point(126, 47)
        Me.TbNombre.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TbNombre.Name = "TbNombre"
        Me.TbNombre.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbNombre.PlaceholderText = ""
        Me.TbNombre.SelectedText = ""
        Me.TbNombre.Size = New System.Drawing.Size(242, 36)
        Me.TbNombre.TabIndex = 74
        Me.TbNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(40, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 22)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "Nombre:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(91, 97)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 22)
        Me.Label2.TabIndex = 75
        Me.Label2.Text = "IP:"
        '
        'LblTransmision
        '
        Me.LblTransmision.AutoSize = True
        Me.LblTransmision.BackColor = System.Drawing.Color.Transparent
        Me.LblTransmision.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTransmision.ForeColor = System.Drawing.Color.White
        Me.LblTransmision.Location = New System.Drawing.Point(11, 183)
        Me.LblTransmision.Name = "LblTransmision"
        Me.LblTransmision.Size = New System.Drawing.Size(109, 22)
        Me.LblTransmision.TabIndex = 89
        Me.LblTransmision.Text = "Transmisión:"
        '
        'LblBomba
        '
        Me.LblBomba.AutoSize = True
        Me.LblBomba.BackColor = System.Drawing.Color.Transparent
        Me.LblBomba.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblBomba.ForeColor = System.Drawing.Color.White
        Me.LblBomba.Location = New System.Drawing.Point(48, 226)
        Me.LblBomba.Name = "LblBomba"
        Me.LblBomba.Size = New System.Drawing.Size(72, 22)
        Me.LblBomba.TabIndex = 90
        Me.LblBomba.Text = "Bomba:"
        '
        'LblMotor
        '
        Me.LblMotor.AutoSize = True
        Me.LblMotor.BackColor = System.Drawing.Color.Transparent
        Me.LblMotor.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblMotor.ForeColor = System.Drawing.Color.White
        Me.LblMotor.Location = New System.Drawing.Point(60, 140)
        Me.LblMotor.Name = "LblMotor"
        Me.LblMotor.Size = New System.Drawing.Size(60, 22)
        Me.LblMotor.TabIndex = 88
        Me.LblMotor.Text = "Motor:"
        '
        'ManualConnection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(390, 313)
        Me.Controls.Add(Me.LblTransmision)
        Me.Controls.Add(Me.LblBomba)
        Me.Controls.Add(Me.LblMotor)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TbNombre)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnConectar)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "ManualConnection"
        Me.Text = "ManualConnection"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents CloseFracSelect As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents BtnConectar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Label2 As Label
    Public WithEvents TbNombre As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents Label1 As Label
    Public WithEvents LblTransmision As Label
    Public WithEvents LblBomba As Label
    Public WithEvents LblMotor As Label
End Class
