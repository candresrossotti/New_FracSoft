﻿Public Class ConfigWITS

    Private WITSStartTime As Date
    Private WITSOperationSpan As TimeSpan
    Private ActiveWITS As Boolean = False
    ''Guardamos todos los datos en memoria
    Private Sub BtnGuardar_Click(sender As Object, e As EventArgs) Handles BtnGuardar.Click
        Try

            For Each Cb As Guna.UI2.WinForms.Guna2ComboBox In DataPanel.Controls.OfType(Of Guna.UI2.WinForms.Guna2ComboBox)
                If String.IsNullOrEmpty(Cb.Text) Then
                    avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Valores")
                    avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                    AvisoGeneral.ShowDialog(Me)
                    Exit Try
                End If
            Next


            ''Paridad
            Select Case CbParity.Text
                Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]")
                    My.Settings.ParityWITS = 0
                Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[2]")      ''SI ES IMPAR, GUARDAMOS UN UNO
                    My.Settings.ParityWITS = 1
                Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[1]")      ''SI ES PAR, GUARDAMOS UN DOS
                    My.Settings.ParityWITS = 2
                Case Else
                    My.Settings.ParityWITS = 0
            End Select

            ''StopBits
            Select Case CbStopBits.Text
                Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[0]")
                    My.Settings.StopBitsWITS = 0
                Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]")
                    My.Settings.StopBitsWITS = 1
                Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[2]")
                    My.Settings.StopBitsWITS = 2
                Case Else
                    My.Settings.StopBitsWITS = 1
            End Select

            My.Settings.BaudRateWITS = Convert.ToInt32(CbBaudRate.Text)
            My.Settings.DataBitsWITS = Convert.ToInt32(CbDataBits.Text)

            My.Settings.WITSDataCollect.Clear()
            WITSSend.Clear()
            WITSIDSend.Clear()
            For i = 1 To WITSNData
                Dim Cb As Control() = Me.Controls.Find("CbDato" + i.ToString(), True)
                WITSSend.Add(DataNombre.Item(DataList.IndexOf(Cb(0).Text)))
                My.Settings.WITSDataCollect.Add(DataList.IndexOf(Cb(0).Text))
                Dim TbID As Guna.UI2.WinForms.Guna2TextBox = TryCast(Me.Controls.Find("TbID" + i.ToString(), True)(0), Guna.UI2.WinForms.Guna2TextBox)
                WITSIDSend.Add(TbID.Text)
            Next

            My.Settings.WITSIDCollect.Clear()
            My.Settings.WITSIDCollect.Add(TBTimeStampID.Text)
            For i = 1 To WITSNData
                Dim TbID As Guna.UI2.WinForms.Guna2TextBox = TryCast(Controls.Find("TBID" + (i).ToString, True)(0), Guna.UI2.WinForms.Guna2TextBox)
                My.Settings.WITSIDCollect.Add(TbID.Text)
            Next

            My.Settings.WITSNData = WITSNData

            My.Settings.Save()

            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("AjustesOK")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Aviso")
            AvisoGeneral.ShowDialog(Me)
            Me.Hide()

        Catch ex As Exception
            LogError(ex)
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("AjustesProblema")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Aviso")
            AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub

    ''Scaneamos los puertos
    Private Sub TmrScan_Tick(sender As Object, e As EventArgs) Handles TmrScan.Tick
        CbComPorts.Items.Clear()
        For Each SPort As String In My.Computer.Ports.SerialPortNames
            CbComPorts.Items.Add(SPort)
        Next
        CbComPorts.Text = My.Settings.PortName
        TmrScan.Stop()
        Refresh()
    End Sub

    Private Sub WITS_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CbBaudRate.Text = My.Settings.BaudRateWITS
        CbDataBits.Text = My.Settings.DataBitsWITS
        CbComPorts.Text = My.Settings.PortNameWITS

        CbParity.Items.Clear()
        CbParity.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]"))
        CbParity.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbParity[1]"))
        CbParity.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbParity[2]"))

        CbStopBits.Items.Clear()
        CbStopBits.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[0]"))
        CbStopBits.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]"))
        CbStopBits.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[2]"))

        Select Case My.Settings.Parity
            Case 0
                CbParity.Text = LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]")   ''NINGUNO
            Case 1
                CbParity.Text = LanguageSelect.DynamicLanguageRM.GetString("CbParity[2]")   ''SI ES UN UNO, ES IMPAR
            Case 2
                CbParity.Text = LanguageSelect.DynamicLanguageRM.GetString("CbParity[1]")   ''SI ES UN DOS, ES PAR
            Case Else
                CbParity.Text = LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]")
        End Select

        Select Case My.Settings.StopBits
            Case 0
                CbStopBits.Text = LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[0]")   ''NINGUNO
            Case 1
                CbStopBits.Text = LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]")   ''UNO
            Case 2
                CbStopBits.Text = LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[2]")   ''DOS   
            Case Else
                CbStopBits.Text = LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]")
        End Select


        CenterEverything()

        If ActiveWITS = False Then
            CbDato1.Items.AddRange(DataList.ToArray)
            ''Agregamos todos los combobox que quedaron guardados
            CbDato1.Text = DataList.Item(My.Settings.WITSDataCollect.Item(0))
            TBTimeStampID.Text = My.Settings.WITSIDCollect.Item(0)
            WITSSend.Add(DataNombre.Item(My.Settings.WITSDataCollect.Item(0)))
            WITSIDSend.Add(My.Settings.WITSIDCollect.Item(0))
            Dim ActualData = WITSNData
            WITSNData = 1
            For j = 1 To ActualData - 1
                BtnAgregarDato.PerformClick()
                Dim Cb As Guna.UI2.WinForms.Guna2ComboBox = TryCast(Controls.Find("CbDato" + (j + 1).ToString(), True)(0), Guna.UI2.WinForms.Guna2ComboBox)
                Cb.Text = DataList.Item(My.Settings.WITSDataCollect.Item(j))
                WITSSend.Add(DataNombre.Item(My.Settings.WITSDataCollect.Item(j)))
                WITSIDSend.Add(My.Settings.WITSIDCollect.Item(j))
            Next
            For i = 1 To ActualData
                Dim TbID As Guna.UI2.WinForms.Guna2TextBox = TryCast(Controls.Find("TBID" + (i).ToString, True)(0), Guna.UI2.WinForms.Guna2TextBox)
                TbID.Text = My.Settings.WITSIDCollect.Item(i)
            Next
        End If

        TmrScan.Start()

        AddHandler TbID1.TextChanged, AddressOf TextChange
        AddHandler CbDato1.TextChanged, AddressOf TextChange
        ActiveWITS = True
    End Sub

    ''Sub para centrar y alinear todo
    Public Sub CenterEverything()
        CenterObject(Title)
        CenterObject(BtnGuardar)
        CenterObject(LblPuertoSerie)
        CenterObject(LblDatos)

        LeftAlign(LblWITS1, CbComPorts.Left - 6)
        LeftAlign(LblWITS2, CbBaudRate.Left - 6)
        LeftAlign(LblWITS3, CbParity.Left - 6)
        LeftAlign(LblWITS4, CbDataBits.Left - 6)
        LeftAlign(LblWITS5, CbStopBits.Left - 6)
        LeftAlign(LblWITS6, TBTimeStampID.Left - 6)
        LeftAlign(LblDato1, CbDato1.Left - 6)
    End Sub

    ''No cerramos el form, lo escondemos
    Private Sub FormClose(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
        Hide()
    End Sub

    ''Agregamos un dato
    Private Sub BtnAgregarDato_Click(sender As Object, e As EventArgs) Handles BtnAgregarDato.Click

        BtnAgregarDato.Top += 60
        BtnDelDato.Top += 60

        Dim LblAnterior As Control() = Me.Controls.Find("LblDato" + (WITSNData).ToString(), True)
        Dim CbAnterior As Control() = Me.Controls.Find("CbDato" + (WITSNData).ToString(), True)

        Dim LblAnterior1 As Control() = Me.Controls.Find("LblID" + (WITSNData).ToString(), True)
        Dim TbAnterior1 As Control() = Me.Controls.Find("TbID" + (WITSNData).ToString(), True)

        WITSNData += 1

        Dim Lb As New Label With {
            .Name = "LblDato" + WITSNData.ToString(),
            .Text = LanguageSelect.DynamicLanguageRM.GetString("Dato").PadLeft(1, " ") + WITSNData.ToString() + ":",
            .ForeColor = Color.White,
            .Font = New Font("Montserrat", 11),
            .AutoSize = True
        }

        Dim Cb As New Guna.UI2.WinForms.Guna2ComboBox With {
            .Name = "CbDato" + WITSNData.ToString(),
            .Size = New Size(200, 36),
            .Location = New Point(CbAnterior(0).Left, CbAnterior(0).Top + 60),
            .BorderStyle = Drawing2D.DashStyle.Solid,
            .TextAlign = HorizontalAlignment.Center,
            .BorderThickness = 1,
            .Cursor = Cursors.Hand
        }
        Cb.FocusedState.BorderColor = Color.FromArgb(94, 148, 255)
        Cb.HoverState.BorderColor = Color.FromArgb(94, 148, 255)

        Dim Lb1 As New Label With {
            .Name = "LblID" + WITSNData.ToString(),
            .Text = "ID:",
            .ForeColor = Color.White,
            .Font = New Font("Montserrat", 11),
            .AutoSize = True
        }

        Dim Tb1 As New Guna.UI2.WinForms.Guna2TextBox With {
            .Name = "TbID" + WITSNData.ToString(),
            .Size = New Size(140, 36),
            .Location = New Point(TbAnterior1(0).Left, TbAnterior1(0).Top + 60),
            .BorderStyle = Drawing2D.DashStyle.Solid,
            .TextAlign = HorizontalAlignment.Center,
            .BorderThickness = 1,
            .Cursor = Cursors.IBeam
        }
        Tb1.FocusedState.BorderColor = Color.FromArgb(94, 148, 255)
        Tb1.HoverState.BorderColor = Color.FromArgb(94, 148, 255)

        DataPanel.Controls.Add(Cb)
        DataPanel.Controls.Add(Lb)
        Lb.Location = New Point(Cb.Left - 6 - Lb.Width, LblAnterior(0).Top + 60)
        DataPanel.Controls.Add(Tb1)
        DataPanel.Controls.Add(Lb1)
        Lb1.Location = New Point(Tb1.Left - 6 - Lb1.Width, LblAnterior1(0).Top + 60)

        Cb.Items.AddRange(DataList.ToArray)


        AddHandler Tb1.TextChanged, AddressOf TextChange

        DataPanel.ScrollControlIntoView(BtnAgregarDato)
    End Sub

    Private Sub BtnDelDato_Click(sender As Object, e As EventArgs) Handles BtnDelDato.Click
        If WITSNData > 1 Then
            BtnAgregarDato.Top -= 60
            BtnDelDato.Top -= 60


            Dim Cb As Control() = Me.Controls.Find("CbDato" + (WITSNData).ToString(), True)
            DataPanel.Controls.Remove(Cb(0))
            Dim Lb As Control() = Me.Controls.Find("LblDato" + (WITSNData).ToString(), True)
            DataPanel.Controls.Remove(Lb(0))
            Dim Cb1 As Control() = Me.Controls.Find("TbID" + (WITSNData).ToString(), True)
            DataPanel.Controls.Remove(Cb1(0))
            Dim Lb1 As Control() = Me.Controls.Find("LblID" + (WITSNData).ToString(), True)
            DataPanel.Controls.Remove(Lb1(0))
            WITSNData -= 1

        End If
    End Sub

    Private Sub TextChange(sender As Object, e As EventArgs) Handles TBTimeStampID.TextChanged, TbID1.TextChanged
        If Not String.IsNullOrEmpty(sender.Text) Then
            If Not IsDecimal(sender.Text) Then
                sender.Text = Mid(sender.Text, 1, sender.Text.Length() - 1)
                sender.SelectionStart = sender.Text.Length()
            End If
        End If
    End Sub

    Private Sub TmrSend_Tick(sender As Object, e As EventArgs) Handles TmrSend.Tick
        Dim WITSTask As New Task(AddressOf SendWITS)
        WITSTask.Start()
    End Sub

    Private Sub SendWITS()
        Try
            SerialPort.WriteLine("&&")
            WITSOperationSpan = Now.Subtract(WITSStartTime)
            SerialPort.Write(TBTimeStampID.Text)
            SerialPort.WriteLine(GetDecimal(Math.Round(WITSOperationSpan.TotalSeconds, 1)))

            For j = 0 To WITSDevices - 1
                For i = 0 To WITSNData - 1
                    SerialPort.Write(WITSIDSend(i))
                    If WITSSend(i) = "MarchaActual_" Then
                        If IsInteger(FracData.StrVFrac(WITSSend(i) + WITSFracs(j).ToString())) = False Then
                            SerialPort.Write("0")
                        Else
                            SerialPort.Write(FracData.StrVFrac(WITSSend(i) + WITSFracs(j).ToString()))
                        End If
                    Else
                        SerialPort.Write(FracData.DblVFrac(WITSSend(i) + WITSFracs(j).ToString()))
                    End If
                    SerialPort.WriteLine("")
                Next
            Next
            SerialPort.WriteLine("!!")
        Catch ex As Exception
            LogError(ex)
            TmrSend.Stop()
            TSWITS.Checked = False
            Ajustes.BtnWITS.FillColor = Color.Transparent
            'avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Reconnect")
            'avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Aviso")
            'AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub

    ''Activamos el envio de WITS
    Private Sub TSSP_CheckedChanged(sender As Object, e As EventArgs) Handles TSWITS.CheckedChanged
        Try
            If sender.Checked = True Then
                For Each Cb As Guna.UI2.WinForms.Guna2ComboBox In DataPanel.Controls.OfType(Of Guna.UI2.WinForms.Guna2ComboBox)
                    If String.IsNullOrEmpty(Cb.Text) Then
                        avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Valores")
                        avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                        AvisoGeneral.ShowDialog(Me)
                        sender.Checked = False
                        Exit Sub
                    End If
                Next

                For Each Cb As Guna.UI2.WinForms.Guna2ComboBox In PortPanel.Controls.OfType(Of Guna.UI2.WinForms.Guna2ComboBox)
                    If String.IsNullOrEmpty(Cb.Text) Then
                        avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Valores")
                        avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                        AvisoGeneral.ShowDialog(Me)
                        sender.Checked = False
                        Exit Sub
                    End If
                Next

                WITSStartTime = DateTime.Now()                          ''Tiempo de inicio
                TmrSend.Interval = Convert.ToInt16(My.Settings.Periodo)
                SerialPort.PortName = CbComPorts.Text
                SerialPort.BaudRate = CbBaudRate.Text
                Select Case CbParity.Text
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]")
                        SerialPort.Parity = 0
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[2]")      ''SI ES IMPAR, GUARDAMOS UN UNO
                        SerialPort.Parity = 1
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[1]")      ''SI ES PAR, GUARDAMOS UN DOS
                        SerialPort.Parity = 2
                    Case Else
                        SerialPort.Parity = 0
                End Select

                Select Case CbStopBits.Text
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[0]")
                        SerialPort.StopBits = 0
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]")
                        SerialPort.StopBits = 1
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[2]")
                        SerialPort.StopBits = 2
                    Case Else
                        SerialPort.StopBits = 1
                End Select
                SerialPort.DataBits = CbDataBits.Text
                SerialPort.Open()
                TmrSend.Start()
                Ajustes.BtnWITS.FillColor = Color.LimeGreen
            Else
                TmrSend.Stop()
                SerialPort.Close()

                Ajustes.BtnWITS.FillColor = Color.Transparent
            End If
        Catch ex As Exception
            LogError(ex)
            sender.Checked = False
            avisoStr = "No se pudo abrir el puerto serie."
            avisoTitulo = "Error"
            AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub

    ''Sub para poder buscar en FracData los valores a enviar
    Private Function GetDictionaryName(ByVal Sender As String) As String
        Select Case Sender
            Case "RPM"
                Return "RPMActual_"
            Case "Marcha"
                Return "MarchaActual_"
            Case "Presion Actual"
                Return "PresionActual_"
            Case "TRIP"
                Return "TRIPDeseado_"
            Case "Presion Succion"
                Return "PresSuc_"
            Case "Temperatura Transmision"
                Return "TemperaturaAceiteTransmision_"
            Case "RPM Transmision"
                Return "RPMOutTrans_"
            Case "Voltaje"
                Return "Voltaje_"
            Case "Presion Motor"
                Return "PresionAceiteMotor_"
            Case "Caudal"
                Return "CaudalActual_"
            Case "Combustible [L/Hr]"
                Return "FuelRate_"
            Case "Temperatura LUB"
                Return "TempLUB_"
            Case "% Carga Motor"
                Return "CargaMotor_"
            Case "HHP"
                Return "HHPMotor_"
            Case "Presion LUB"
                Return "PresLUB_"
            Case "Horas Motor"
                Return "HorasMotor_"
            Case "Presion Transmision"
                Return "PresTrans_"
            Case "Temp. Agua Motor"
                Return "TempAguaMotor_"
            Case "% Combustible"
                Return "CombustibleTanque_"
            Case "Temp. Aceite Motor"
                Return "TemperaturaAceiteMotor_"
            Case "Totalizador"
                Return "0_"
            Case "Combustible Total"
                Return "0_"
            Case "Caudal Total"
                Return "0_"
        End Select
        Return "0"
    End Function
End Class





