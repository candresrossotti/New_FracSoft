﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SerialPortConfig
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.PortPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.CbStopBits = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.CbDataBits = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblSP5 = New System.Windows.Forms.Label()
        Me.LblSP4 = New System.Windows.Forms.Label()
        Me.CbParity = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblSP3 = New System.Windows.Forms.Label()
        Me.CbBaudRate = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.CbComPorts = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblSP2 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel16 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblPuertoSerie = New System.Windows.Forms.Label()
        Me.LblSP1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblDatos = New System.Windows.Forms.Label()
        Me.TmrScan = New System.Windows.Forms.Timer(Me.components)
        Me.BtnGuardar = New Guna.UI2.WinForms.Guna2Button()
        Me.DataPanel = New System.Windows.Forms.Panel()
        Me.BtnDelDato = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAgregarDato = New Guna.UI2.WinForms.Guna2Button()
        Me.CbDato1 = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblDato1 = New System.Windows.Forms.Label()
        Me.CbPeriodo = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.CbSeparador = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblSP7 = New System.Windows.Forms.Label()
        Me.LblSP6 = New System.Windows.Forms.Label()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.TmrSend = New System.Windows.Forms.Timer(Me.components)
        Me.SerialPort = New System.IO.Ports.SerialPort(Me.components)
        Me.TSSP = New Guna.UI2.WinForms.Guna2ToggleSwitch()
        Me.LblSP8 = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.PortPanel.SuspendLayout()
        Me.Guna2CustomGradientPanel16.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.DataPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(700, 38)
        Me.TitlePanel.TabIndex = 113
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(195, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(311, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Configuracion Puerto Serie"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(652, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.ContainerControl = Me
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.ContainerControl = Me
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'PortPanel
        '
        Me.PortPanel.BackColor = System.Drawing.Color.Transparent
        Me.PortPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PortPanel.BorderRadius = 6
        Me.PortPanel.BorderThickness = 1
        Me.PortPanel.Controls.Add(Me.CbStopBits)
        Me.PortPanel.Controls.Add(Me.CbDataBits)
        Me.PortPanel.Controls.Add(Me.LblSP5)
        Me.PortPanel.Controls.Add(Me.LblSP4)
        Me.PortPanel.Controls.Add(Me.CbParity)
        Me.PortPanel.Controls.Add(Me.LblSP3)
        Me.PortPanel.Controls.Add(Me.CbBaudRate)
        Me.PortPanel.Controls.Add(Me.CbComPorts)
        Me.PortPanel.Controls.Add(Me.LblSP2)
        Me.PortPanel.Controls.Add(Me.Guna2CustomGradientPanel16)
        Me.PortPanel.Controls.Add(Me.LblSP1)
        Me.PortPanel.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PortPanel.CustomizableEdges.BottomLeft = False
        Me.PortPanel.CustomizableEdges.BottomRight = False
        Me.PortPanel.FillColor = System.Drawing.Color.Transparent
        Me.PortPanel.Location = New System.Drawing.Point(12, 44)
        Me.PortPanel.Name = "PortPanel"
        Me.PortPanel.Size = New System.Drawing.Size(285, 326)
        Me.PortPanel.TabIndex = 130
        '
        'CbStopBits
        '
        Me.CbStopBits.BackColor = System.Drawing.Color.Transparent
        Me.CbStopBits.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbStopBits.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbStopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbStopBits.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbStopBits.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbStopBits.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbStopBits.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbStopBits.ItemHeight = 30
        Me.CbStopBits.Location = New System.Drawing.Point(130, 272)
        Me.CbStopBits.Name = "CbStopBits"
        Me.CbStopBits.Size = New System.Drawing.Size(140, 36)
        Me.CbStopBits.TabIndex = 138
        Me.CbStopBits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CbDataBits
        '
        Me.CbDataBits.BackColor = System.Drawing.Color.Transparent
        Me.CbDataBits.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbDataBits.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbDataBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbDataBits.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDataBits.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDataBits.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbDataBits.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbDataBits.ItemHeight = 30
        Me.CbDataBits.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8"})
        Me.CbDataBits.Location = New System.Drawing.Point(130, 212)
        Me.CbDataBits.Name = "CbDataBits"
        Me.CbDataBits.Size = New System.Drawing.Size(140, 36)
        Me.CbDataBits.TabIndex = 137
        Me.CbDataBits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblSP5
        '
        Me.LblSP5.AutoSize = True
        Me.LblSP5.BackColor = System.Drawing.Color.Transparent
        Me.LblSP5.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSP5.ForeColor = System.Drawing.Color.White
        Me.LblSP5.Location = New System.Drawing.Point(19, 279)
        Me.LblSP5.Name = "LblSP5"
        Me.LblSP5.Size = New System.Drawing.Size(102, 21)
        Me.LblSP5.TabIndex = 136
        Me.LblSP5.Text = "Bits de Stop:"
        '
        'LblSP4
        '
        Me.LblSP4.AutoSize = True
        Me.LblSP4.BackColor = System.Drawing.Color.Transparent
        Me.LblSP4.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSP4.ForeColor = System.Drawing.Color.White
        Me.LblSP4.Location = New System.Drawing.Point(11, 219)
        Me.LblSP4.Name = "LblSP4"
        Me.LblSP4.Size = New System.Drawing.Size(111, 21)
        Me.LblSP4.TabIndex = 135
        Me.LblSP4.Text = "Bits de Datos:"
        '
        'CbParity
        '
        Me.CbParity.BackColor = System.Drawing.Color.Transparent
        Me.CbParity.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbParity.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbParity.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbParity.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbParity.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbParity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbParity.ItemHeight = 30
        Me.CbParity.Location = New System.Drawing.Point(130, 152)
        Me.CbParity.Name = "CbParity"
        Me.CbParity.Size = New System.Drawing.Size(140, 36)
        Me.CbParity.TabIndex = 134
        Me.CbParity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblSP3
        '
        Me.LblSP3.AutoSize = True
        Me.LblSP3.BackColor = System.Drawing.Color.Transparent
        Me.LblSP3.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSP3.ForeColor = System.Drawing.Color.White
        Me.LblSP3.Location = New System.Drawing.Point(6, 159)
        Me.LblSP3.Name = "LblSP3"
        Me.LblSP3.Size = New System.Drawing.Size(120, 21)
        Me.LblSP3.TabIndex = 133
        Me.LblSP3.Text = "Bit de Paridad:"
        '
        'CbBaudRate
        '
        Me.CbBaudRate.BackColor = System.Drawing.Color.Transparent
        Me.CbBaudRate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbBaudRate.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbBaudRate.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbBaudRate.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbBaudRate.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbBaudRate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbBaudRate.ItemHeight = 30
        Me.CbBaudRate.Items.AddRange(New Object() {"1200", "2400", "4800", "9600", "14400", "19200", "28800", "38400", "56000", "57600", "115200"})
        Me.CbBaudRate.Location = New System.Drawing.Point(130, 92)
        Me.CbBaudRate.Name = "CbBaudRate"
        Me.CbBaudRate.Size = New System.Drawing.Size(140, 36)
        Me.CbBaudRate.TabIndex = 132
        Me.CbBaudRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CbComPorts
        '
        Me.CbComPorts.BackColor = System.Drawing.Color.Transparent
        Me.CbComPorts.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbComPorts.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbComPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbComPorts.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbComPorts.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbComPorts.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbComPorts.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbComPorts.ItemHeight = 30
        Me.CbComPorts.Items.AddRange(New Object() {"LAS 1", "LAS 2", "LAS 3", "LAS 4"})
        Me.CbComPorts.Location = New System.Drawing.Point(130, 32)
        Me.CbComPorts.Name = "CbComPorts"
        Me.CbComPorts.Size = New System.Drawing.Size(140, 36)
        Me.CbComPorts.TabIndex = 131
        Me.CbComPorts.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblSP2
        '
        Me.LblSP2.AutoSize = True
        Me.LblSP2.BackColor = System.Drawing.Color.Transparent
        Me.LblSP2.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSP2.ForeColor = System.Drawing.Color.White
        Me.LblSP2.Location = New System.Drawing.Point(31, 99)
        Me.LblSP2.Name = "LblSP2"
        Me.LblSP2.Size = New System.Drawing.Size(92, 21)
        Me.LblSP2.TabIndex = 131
        Me.LblSP2.Text = "Baud Rate:"
        '
        'Guna2CustomGradientPanel16
        '
        Me.Guna2CustomGradientPanel16.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.BorderRadius = 6
        Me.Guna2CustomGradientPanel16.Controls.Add(Me.LblPuertoSerie)
        Me.Guna2CustomGradientPanel16.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel16.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel16.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel16.Name = "Guna2CustomGradientPanel16"
        Me.Guna2CustomGradientPanel16.Size = New System.Drawing.Size(285, 26)
        Me.Guna2CustomGradientPanel16.TabIndex = 19
        '
        'LblPuertoSerie
        '
        Me.LblPuertoSerie.AutoSize = True
        Me.LblPuertoSerie.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPuertoSerie.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblPuertoSerie.Location = New System.Drawing.Point(74, 4)
        Me.LblPuertoSerie.Name = "LblPuertoSerie"
        Me.LblPuertoSerie.Size = New System.Drawing.Size(165, 20)
        Me.LblPuertoSerie.TabIndex = 19
        Me.LblPuertoSerie.Text = "Ajuste de Puerto Serie"
        '
        'LblSP1
        '
        Me.LblSP1.AutoSize = True
        Me.LblSP1.BackColor = System.Drawing.Color.Transparent
        Me.LblSP1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSP1.ForeColor = System.Drawing.Color.White
        Me.LblSP1.Location = New System.Drawing.Point(19, 39)
        Me.LblSP1.Name = "LblSP1"
        Me.LblSP1.Size = New System.Drawing.Size(105, 21)
        Me.LblSP1.TabIndex = 130
        Me.LblSP1.Text = "Puerto Serie:"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblDatos)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(303, 44)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(382, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblDatos
        '
        Me.LblDatos.AutoSize = True
        Me.LblDatos.BackColor = System.Drawing.Color.Transparent
        Me.LblDatos.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblDatos.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblDatos.Location = New System.Drawing.Point(134, 3)
        Me.LblDatos.Name = "LblDatos"
        Me.LblDatos.Size = New System.Drawing.Size(120, 20)
        Me.LblDatos.TabIndex = 19
        Me.LblDatos.Text = "Ajuste de Datos"
        '
        'TmrScan
        '
        Me.TmrScan.Interval = 500
        '
        'BtnGuardar
        '
        Me.BtnGuardar.Animated = True
        Me.BtnGuardar.BorderColor = System.Drawing.Color.White
        Me.BtnGuardar.BorderRadius = 5
        Me.BtnGuardar.BorderThickness = 1
        Me.BtnGuardar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnGuardar.FillColor = System.Drawing.Color.Transparent
        Me.BtnGuardar.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnGuardar.ForeColor = System.Drawing.Color.White
        Me.BtnGuardar.Location = New System.Drawing.Point(300, 386)
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.Size = New System.Drawing.Size(100, 40)
        Me.BtnGuardar.TabIndex = 140
        Me.BtnGuardar.Text = "Guardar"
        '
        'DataPanel
        '
        Me.DataPanel.AutoScroll = True
        Me.DataPanel.AutoScrollMargin = New System.Drawing.Size(0, 50)
        Me.DataPanel.Controls.Add(Me.BtnDelDato)
        Me.DataPanel.Controls.Add(Me.BtnAgregarDato)
        Me.DataPanel.Controls.Add(Me.CbDato1)
        Me.DataPanel.Controls.Add(Me.LblDato1)
        Me.DataPanel.Controls.Add(Me.CbPeriodo)
        Me.DataPanel.Controls.Add(Me.CbSeparador)
        Me.DataPanel.Controls.Add(Me.LblSP7)
        Me.DataPanel.Controls.Add(Me.LblSP6)
        Me.DataPanel.Location = New System.Drawing.Point(304, 70)
        Me.DataPanel.Name = "DataPanel"
        Me.DataPanel.Size = New System.Drawing.Size(380, 299)
        Me.DataPanel.TabIndex = 141
        '
        'BtnDelDato
        '
        Me.BtnDelDato.Animated = True
        Me.BtnDelDato.BorderColor = System.Drawing.Color.White
        Me.BtnDelDato.BorderRadius = 5
        Me.BtnDelDato.BorderThickness = 1
        Me.BtnDelDato.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDelDato.FillColor = System.Drawing.Color.Transparent
        Me.BtnDelDato.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnDelDato.ForeColor = System.Drawing.Color.White
        Me.BtnDelDato.Location = New System.Drawing.Point(199, 202)
        Me.BtnDelDato.Name = "BtnDelDato"
        Me.BtnDelDato.Size = New System.Drawing.Size(110, 47)
        Me.BtnDelDato.TabIndex = 152
        Me.BtnDelDato.Text = "Eliminar Dato"
        '
        'BtnAgregarDato
        '
        Me.BtnAgregarDato.Animated = True
        Me.BtnAgregarDato.BorderColor = System.Drawing.Color.White
        Me.BtnAgregarDato.BorderRadius = 5
        Me.BtnAgregarDato.BorderThickness = 1
        Me.BtnAgregarDato.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAgregarDato.FillColor = System.Drawing.Color.Transparent
        Me.BtnAgregarDato.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnAgregarDato.ForeColor = System.Drawing.Color.White
        Me.BtnAgregarDato.Location = New System.Drawing.Point(74, 202)
        Me.BtnAgregarDato.Name = "BtnAgregarDato"
        Me.BtnAgregarDato.Size = New System.Drawing.Size(110, 47)
        Me.BtnAgregarDato.TabIndex = 147
        Me.BtnAgregarDato.Text = "Agregar Dato"
        '
        'CbDato1
        '
        Me.CbDato1.BackColor = System.Drawing.Color.Transparent
        Me.CbDato1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbDato1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbDato1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbDato1.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDato1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDato1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbDato1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbDato1.ItemHeight = 30
        Me.CbDato1.Location = New System.Drawing.Point(116, 133)
        Me.CbDato1.Name = "CbDato1"
        Me.CbDato1.Size = New System.Drawing.Size(231, 36)
        Me.CbDato1.TabIndex = 151
        Me.CbDato1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblDato1
        '
        Me.LblDato1.AutoSize = True
        Me.LblDato1.BackColor = System.Drawing.Color.Transparent
        Me.LblDato1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblDato1.ForeColor = System.Drawing.Color.White
        Me.LblDato1.Location = New System.Drawing.Point(44, 141)
        Me.LblDato1.Name = "LblDato1"
        Me.LblDato1.Size = New System.Drawing.Size(58, 21)
        Me.LblDato1.TabIndex = 150
        Me.LblDato1.Text = "Dato 1:"
        '
        'CbPeriodo
        '
        Me.CbPeriodo.BackColor = System.Drawing.Color.Transparent
        Me.CbPeriodo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbPeriodo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbPeriodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbPeriodo.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPeriodo.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPeriodo.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbPeriodo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbPeriodo.ItemHeight = 30
        Me.CbPeriodo.Items.AddRange(New Object() {"1", "2", "5", "30"})
        Me.CbPeriodo.Location = New System.Drawing.Point(116, 73)
        Me.CbPeriodo.Name = "CbPeriodo"
        Me.CbPeriodo.Size = New System.Drawing.Size(231, 36)
        Me.CbPeriodo.TabIndex = 149
        Me.CbPeriodo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CbSeparador
        '
        Me.CbSeparador.BackColor = System.Drawing.Color.Transparent
        Me.CbSeparador.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbSeparador.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbSeparador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbSeparador.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbSeparador.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbSeparador.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbSeparador.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbSeparador.ItemHeight = 30
        Me.CbSeparador.Items.AddRange(New Object() {";", ",", "/", "*"})
        Me.CbSeparador.Location = New System.Drawing.Point(116, 13)
        Me.CbSeparador.Name = "CbSeparador"
        Me.CbSeparador.Size = New System.Drawing.Size(231, 36)
        Me.CbSeparador.TabIndex = 146
        Me.CbSeparador.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblSP7
        '
        Me.LblSP7.AutoSize = True
        Me.LblSP7.BackColor = System.Drawing.Color.Transparent
        Me.LblSP7.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSP7.ForeColor = System.Drawing.Color.White
        Me.LblSP7.Location = New System.Drawing.Point(-1, 81)
        Me.LblSP7.Name = "LblSP7"
        Me.LblSP7.Size = New System.Drawing.Size(113, 21)
        Me.LblSP7.TabIndex = 148
        Me.LblSP7.Text = "Periodo [Seg]:"
        '
        'LblSP6
        '
        Me.LblSP6.AutoSize = True
        Me.LblSP6.BackColor = System.Drawing.Color.Transparent
        Me.LblSP6.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSP6.ForeColor = System.Drawing.Color.White
        Me.LblSP6.Location = New System.Drawing.Point(20, 21)
        Me.LblSP6.Name = "LblSP6"
        Me.LblSP6.Size = New System.Drawing.Size(89, 21)
        Me.LblSP6.TabIndex = 145
        Me.LblSP6.Text = "Separador:"
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.CustomizableEdges.TopLeft = False
        Me.Guna2Panel1.CustomizableEdges.TopRight = False
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(303, 70)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(382, 300)
        Me.Guna2Panel1.TabIndex = 142
        '
        'TmrSend
        '
        Me.TmrSend.Interval = 1000
        '
        'TSSP
        '
        Me.TSSP.Animated = True
        Me.TSSP.CheckedState.FillColor = System.Drawing.Color.LimeGreen
        Me.TSSP.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TSSP.Location = New System.Drawing.Point(630, 406)
        Me.TSSP.Name = "TSSP"
        Me.TSSP.Size = New System.Drawing.Size(58, 20)
        Me.TSSP.TabIndex = 160
        Me.TSSP.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.TSSP.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.TSSP.UncheckedState.InnerBorderColor = System.Drawing.Color.White
        Me.TSSP.UncheckedState.InnerColor = System.Drawing.Color.White
        '
        'LblSP8
        '
        Me.LblSP8.AutoSize = True
        Me.LblSP8.BackColor = System.Drawing.Color.Transparent
        Me.LblSP8.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblSP8.ForeColor = System.Drawing.Color.White
        Me.LblSP8.Location = New System.Drawing.Point(628, 382)
        Me.LblSP8.Name = "LblSP8"
        Me.LblSP8.Size = New System.Drawing.Size(62, 21)
        Me.LblSP8.TabIndex = 161
        Me.LblSP8.Text = "Activar"
        '
        'SerialPortConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(700, 438)
        Me.Controls.Add(Me.LblSP8)
        Me.Controls.Add(Me.TSSP)
        Me.Controls.Add(Me.DataPanel)
        Me.Controls.Add(Me.BtnGuardar)
        Me.Controls.Add(Me.PortPanel)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "SerialPortConfig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SerialPortConfig"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.PortPanel.ResumeLayout(False)
        Me.PortPanel.PerformLayout()
        Me.Guna2CustomGradientPanel16.ResumeLayout(False)
        Me.Guna2CustomGradientPanel16.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.DataPanel.ResumeLayout(False)
        Me.DataPanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Public WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Public WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblDatos As Label
    Public WithEvents PortPanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents CbStopBits As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents CbDataBits As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblSP5 As Label
    Public WithEvents LblSP4 As Label
    Public WithEvents CbParity As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblSP3 As Label
    Public WithEvents CbBaudRate As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents CbComPorts As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblSP2 As Label
    Public WithEvents Guna2CustomGradientPanel16 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblPuertoSerie As Label
    Public WithEvents LblSP1 As Label
    Public WithEvents TmrScan As Timer
    Public WithEvents BtnGuardar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents DataPanel As Panel
    Public WithEvents BtnAgregarDato As Guna.UI2.WinForms.Guna2Button
    Public WithEvents CbDato1 As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblDato1 As Label
    Public WithEvents CbPeriodo As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents CbSeparador As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblSP7 As Label
    Public WithEvents LblSP6 As Label
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnDelDato As Guna.UI2.WinForms.Guna2Button
    Public WithEvents TmrSend As Timer
    Public WithEvents SerialPort As IO.Ports.SerialPort
    Public WithEvents LblSP8 As Label
    Public WithEvents TSSP As Guna.UI2.WinForms.Guna2ToggleSwitch
End Class
