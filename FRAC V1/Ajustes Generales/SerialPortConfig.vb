﻿Public Class SerialPortConfig

    Private SPStartTime As Date
    Private SPOperationSpan As TimeSpan
    Private ActiveSP As Boolean = False
    Private Sub SerialPortConfig_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)


        ''Mostramos toda la configuracion de SP
        CbPeriodo.Text = My.Settings.Periodo / 1000
        CbSeparador.Text = My.Settings.Separador
        CbBaudRate.Text = My.Settings.BaudRate
        CbDataBits.Text = My.Settings.DataBits
        CbComPorts.Text = My.Settings.PortName

        CbParity.Items.Clear()
        CbParity.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]"))
        CbParity.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbParity[1]"))
        CbParity.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbParity[2]"))

        CbStopBits.Items.Clear()
        CbStopBits.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[0]"))
        CbStopBits.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]"))
        CbStopBits.Items.Add(LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[2]"))

        Select Case My.Settings.Parity
            Case 0
                CbParity.Text = LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]")   ''NINGUNO
            Case 1
                CbParity.Text = LanguageSelect.DynamicLanguageRM.GetString("CbParity[2]")   ''SI ES UN UNO, ES IMPAR
            Case 2
                CbParity.Text = LanguageSelect.DynamicLanguageRM.GetString("CbParity[1]")   ''SI ES UN DOS, ES PAR
            Case Else
                CbParity.Text = LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]")
        End Select

        Select Case My.Settings.StopBits
            Case 0
                CbStopBits.Text = LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[0]")   ''NINGUNO
            Case 1
                CbStopBits.Text = LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]")   ''UNO
            Case 2
                CbStopBits.Text = LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[2]")   ''DOS   
            Case Else
                CbStopBits.Text = LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]")
        End Select

        ''Centramos todo
        CenterEverything()

        ''Periodo del envio de datos
        TmrSend.Interval = My.Settings.Periodo

        If ActiveSP = False Then
            ''Agregamos todos los datos que se pueden al primer ComboBox
            CbDato1.Items.AddRange(DataList.ToArray())

            ''Agregamos todos los combobox que quedaron guardados
            CbDato1.Text = DataList.Item(My.Settings.SerialPortDataCollect.Item(0))
            SPSend.Add(DataNombre.Item(My.Settings.SerialPortDataCollect.Item(0)))
            Dim ActualData = SPNData
            SPNData = 1
            For j = 1 To ActualData - 1
                BtnAgregarDato.PerformClick()
                Dim Cb As Guna.UI2.WinForms.Guna2ComboBox = TryCast(Controls.Find("CbDato" + (j + 1).ToString(), True)(0), Guna.UI2.WinForms.Guna2ComboBox)
                Cb.Text = DataList.Item(My.Settings.SerialPortDataCollect.Item(j))     ''ACA TIRO ERROR, PUEDE SER POR AGREGAR COMBOBOX SIN NOMBERS, NO DEJARLO
                SPSend.Add(DataNombre.Item(My.Settings.SerialPortDataCollect.Item(j)))
            Next
        End If

        ''Buscamos puertos serie presentes
        TmrScan.Start()
        ActiveSP = True

    End Sub

    ''Sub para centrar y alinear todo
    Public Sub CenterEverything()
        CenterObject(Title)
        CenterObject(BtnGuardar)
        CenterObject(LblPuertoSerie)
        CenterObject(LblDatos)

        LeftAlign(LblSP1, CbComPorts.Left - 6)
        LeftAlign(LblSP2, CbBaudRate.Left - 6)
        LeftAlign(LblSP3, CbParity.Left - 6)
        LeftAlign(LblSP4, CbDataBits.Left - 6)
        LeftAlign(LblSP5, CbStopBits.Left - 6)
        LeftAlign(LblSP6, CbSeparador.Left - 6)
        LeftAlign(LblSP7, CbPeriodo.Left - 6)
        LeftAlign(LblDato1, CbDato1.Left - 6)
    End Sub

    ''En formclose hacemos hide para que no se deje de enviar datos
    Private Sub FormClose(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
        Hide()
    End Sub

    ''Timer de scan para puertos serie
    Private Sub TmrScan_Tick(sender As Object, e As EventArgs) Handles TmrScan.Tick
        CbComPorts.Items.Clear()
        For Each SPort As String In My.Computer.Ports.SerialPortNames
            CbComPorts.Items.Add(SPort)
        Next
        CbComPorts.Text = My.Settings.PortName
        TmrScan.Stop()
        Refresh()
    End Sub

    ''Guardamos todos los datos introducidos y la configuracion
    Private Sub BtnGuardar_Click(sender As Object, e As EventArgs) Handles BtnGuardar.Click
        Try

            For Each Cb As Guna.UI2.WinForms.Guna2ComboBox In DataPanel.Controls.OfType(Of Guna.UI2.WinForms.Guna2ComboBox)
                If String.IsNullOrEmpty(Cb.Text) Then
                    avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Valores")
                    avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                    AvisoGeneral.ShowDialog(Me)
                    Exit Try
                End If
            Next


            Select Case CbParity.Text
                Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]")
                    My.Settings.Parity = 0
                Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[2]")      ''SI ES IMPAR, GUARDAMOS UN UNO
                    My.Settings.Parity = 1
                Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[1]")      ''SI ES PAR, GUARDAMOS UN DOS
                    My.Settings.Parity = 2
                Case Else
                    My.Settings.Parity = 0
            End Select

            Select Case CbStopBits.Text
                Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[0]")
                    My.Settings.StopBits = 0
                Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]")
                    My.Settings.StopBits = 1
                Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[2]")
                    My.Settings.StopBits = 2
                Case Else
                    My.Settings.StopBits = 1
            End Select

            My.Settings.BaudRate = Convert.ToInt32(CbBaudRate.Text)
            My.Settings.DataBits = Convert.ToInt32(CbDataBits.Text)
            My.Settings.PortName = CbComPorts.Text

            My.Settings.SerialPortDataCollect.Clear()
            SPSend.Clear()
            For i = 1 To SPNData
                Dim Cb As Control() = Me.Controls.Find("CbDato" + i.ToString(), True)
                SPSend.Add(DataNombre.Item(DataList.IndexOf(Cb(0).Text)))
                My.Settings.SerialPortDataCollect.Add(DataList.IndexOf(Cb(0).Text))
            Next
            My.Settings.SPNData = SPNData
            My.Settings.Separador = CbSeparador.Text
            My.Settings.Periodo = CbPeriodo.Text * 1000

            My.Settings.Save()

            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("AjustesOK")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Aviso")
            AvisoGeneral.ShowDialog(Me)
            Hide()

        Catch ex As Exception
            LogError(ex)
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("AjustesProblema")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Aviso")
            AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub

    ''Sub para agregar un dato
    Private Sub BtnAgregarDato_Click(sender As Object, e As EventArgs) Handles BtnAgregarDato.Click

        BtnAgregarDato.Top += 60
        BtnDelDato.Top += 60

        SPNData += 1

        Dim LblAnterior As Control() = Me.Controls.Find("LblDato" + (SPNData - 1).ToString(), True)
        Dim CbAnterior As Control() = Me.Controls.Find("CbDato" + (SPNData - 1).ToString(), True)

        Dim Cb As New Guna.UI2.WinForms.Guna2ComboBox With {
            .Name = "CbDato" + SPNData.ToString(),
            .Size = New Size(231, 36),
            .Location = New Point(CbAnterior(0).Left, CbAnterior(0).Top + 60),
            .Font = New Font("Montserrat", 10),
            .TextAlign = HorizontalAlignment.Center,
            .BorderThickness = 1,
            .BorderStyle = Drawing2D.DashStyle.Solid,
            .Cursor = Cursors.Hand
        }
        Cb.FocusedState.BorderColor = Color.FromArgb(94, 148, 255)
        Cb.HoverState.BorderColor = Color.FromArgb(94, 148, 255)

        Dim Lb As New Label With {
            .Name = "LblDato" + SPNData.ToString(),
            .Text = LanguageSelect.DynamicLanguageRM.GetString("Dato") + SPNData.ToString() + ":",
            .ForeColor = Color.White,
            .Font = New Font("Montserrat", 11),
            .AutoSize = True
        }

        DataPanel.Controls.Add(Lb)
        Lb.Location = New Point(Cb.Left - Lb.Width - 6, LblAnterior(0).Top + 60)
        DataPanel.Controls.Add(Cb)

        DataPanel.ScrollControlIntoView(BtnAgregarDato)

        Cb.Items.AddRange(DataList.ToArray)
    End Sub

    ''Sub para eliniar el ultimo dato
    Private Sub BtnDelDato_Click(sender As Object, e As EventArgs) Handles BtnDelDato.Click
        If SPNData > 1 Then

            BtnAgregarDato.Top -= 60
            BtnDelDato.Top -= 60


            Dim Cb As Control() = Me.Controls.Find("CbDato" + (SPNData).ToString(), True)
            DataPanel.Controls.Remove(Cb(0))
            Dim Lb As Control() = Me.Controls.Find("LblDato" + (SPNData).ToString(), True)
            DataPanel.Controls.Remove(Lb(0))
            SPNData -= 1
        End If
    End Sub


    ''Timer de envio de datos
    Private Sub TmrSend_Tick(sender As Object, e As EventArgs) Handles TmrSend.Tick
        Dim SendTask As New Task(AddressOf SendSP)
        SendTask.Start()
    End Sub

    Private Sub SendSP()

        ''  SPOperationSpan = Now.Subtract(SPStartTime)

        ''  SerialPort.WriteLine(GetDecimal(Math.Round(SPOperationSpan.TotalSeconds, 1)))

        Try
            SerialPort.Write(Totalizador.ToString() + My.Settings.Separador)
            SerialPort.Write(CombustibleTotal.ToString() + My.Settings.Separador)
            SerialPort.Write(Caudal.ToString() + My.Settings.Separador)
            For j = 0 To SPDevices - 1
                For i = 0 To SPNData - 1
                    If SPSend(i) = "MarchaActual_" Then
                        If IsInteger(FracData.StrVFrac(SPSend(i) + SPFracs(j).ToString())) = False Then
                            SerialPort.Write("0")
                        Else
                            SerialPort.Write(FracData.StrVFrac(SPSend(i) + SPFracs(j).ToString()))
                        End If
                    Else
                        SerialPort.Write(FracData.DblVFrac(SPSend(i) + SPFracs(j).ToString()))
                    End If

                    If i = SPNData - 1 Then
                        SerialPort.WriteLine("")
                    Else
                        SerialPort.Write(My.Settings.Separador)
                    End If
                Next
            Next

        Catch ex As Exception
            LogError(ex)
            TmrSend.Stop()
            TSSP.Checked = False
            Ajustes.BtnComSerie.FillColor = Color.Transparent
            'avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Reconnect")
            'avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Aviso")
            'AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub

    ''Activamos el envio de datos, para saber sobre que frac enviamos tenemos q seleccionarlo en los ajustes de fracturador
    Private Sub TSSP_CheckedChanged(sender As Object, e As EventArgs) Handles TSSP.CheckedChanged
        Try
            If sender.Checked = True Then
                For Each Cb As Guna.UI2.WinForms.Guna2ComboBox In DataPanel.Controls.OfType(Of Guna.UI2.WinForms.Guna2ComboBox)
                    If String.IsNullOrEmpty(Cb.Text) Then
                        avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Valores")
                        avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                        AvisoGeneral.ShowDialog(Me)
                        sender.Checked = False
                        Exit Sub
                    End If
                Next

                For Each Cb As Guna.UI2.WinForms.Guna2ComboBox In PortPanel.Controls.OfType(Of Guna.UI2.WinForms.Guna2ComboBox)
                    If String.IsNullOrEmpty(Cb.Text) Then
                        avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Valores")
                        avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
                        AvisoGeneral.ShowDialog(Me)
                        sender.Checked = False
                        Exit Sub
                    End If
                Next

                SPStartTime = DateTime.Now()                          ''Tiempo de inicio
                TmrSend.Interval = Convert.ToInt16(My.Settings.Periodo)
                SerialPort.PortName = CbComPorts.Text
                SerialPort.BaudRate = CbBaudRate.Text
                Select Case CbParity.Text
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[0]")
                        SerialPort.Parity = 0
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[2]")      ''SI ES IMPAR, GUARDAMOS UN UNO
                        SerialPort.Parity = 1
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbParity[1]")      ''SI ES PAR, GUARDAMOS UN DOS
                        SerialPort.Parity = 2
                    Case Else
                        SerialPort.Parity = 0
                End Select

                Select Case CbStopBits.Text
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[0]")
                        SerialPort.StopBits = 0
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[1]")
                        SerialPort.StopBits = 1
                    Case LanguageSelect.DynamicLanguageRM.GetString("CbStopBits[2]")
                        SerialPort.StopBits = 2
                    Case Else
                        SerialPort.StopBits = 1
                End Select
                SerialPort.DataBits = CbDataBits.Text
                SerialPort.Open()
                TmrSend.Start()
                Ajustes.BtnComSerie.FillColor = Color.LimeGreen
            Else
                TmrSend.Stop()
                SerialPort.Close()
                Ajustes.BtnComSerie.FillColor = Color.Transparent
            End If
        Catch ex As Exception
            LogError(ex)
            sender.Checked = False
            avisoStr = "No se pudo abrir el puerto serie."
            avisoTitulo = "Error"
            AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub
End Class