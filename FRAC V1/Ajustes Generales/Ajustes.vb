﻿
Public Class Ajustes

    Private ReadOnly ES As EngineSelect = EngineSelect.GetInstancia
    Private ReadOnly PS As PumpSelect = PumpSelect.GetInstancia
    Private ReadOnly TS As TransSelect = TransSelect.GetInstancia

    Private Sub Ajustes_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)


        ''Cargamos los ComboBox con los valores actuales para la pantalla, idioma y resolucion
        CbIdioma.Text = My.Settings.Idioma
        CbResolucion.Text = My.Settings.Resolucion
        CbTotalScreens.Text = My.Settings.TotalScreens

        ''Actualizamos las clases engine, pump y transmission con ISystem para que puedan comunicarse al fracturador
        ES.Update(ISystem)
        PS.Update(ISystem)                          ''es necesario esto?
        TS.Update(ISystem)

        ''Centramos todos los controles
        CenterEverything()

        ''Agregamos todos los fracturadores conectados al combobox de los frac
        For i = 0 To ISystem.Devices - 1
            CbFrac.Items.Add(ISystem.GetFracName(ISystem.GetActiveFrac(i)))
        Next

        ''Seleccionamos el primer frac encontrado
        CbFrac.Text = CbFrac.Items(0)

        CbDato1.Items.AddRange(DataList.ToArray)
        'Agregamos todos los combobox que quedaron guardados
        CbDato1.Text = DataList.Item(My.Settings.GraphDataCollect.Item(0))
        GraphSend.Clear()
        GraphSend.Add(DataNombre.Item(My.Settings.GraphDataCollect.Item(0)))
        Dim ActualData = NGraphData
        NGraphData = 1
        For j = 1 To ActualData - 1
            BtnAddData.PerformClick()
            Dim Cb As Guna.UI2.WinForms.Guna2ComboBox = TryCast(Controls.Find("CbDato" + (j + 1).ToString(), True)(0), Guna.UI2.WinForms.Guna2ComboBox)
            Cb.Text = DataList.Item(My.Settings.GraphDataCollect.Item(j))     ''ACA TIRO ERROR, PUEDE SER POR AGREGAR COMBOBOX SIN NOMBERS, NO DEJARLO
            GraphSend.Add(DataNombre.Item(My.Settings.GraphDataCollect.Item(j)))
        Next

        TmrRefresh.Start()
    End Sub

    ''Sub para centrar todos los controles
    Public Sub CenterEverything()
        CenterObject(Title)
        CenterObject(BtnAceptar)
        CenterObject(LblGeneral)
        CenterObject(LblFracturadores)
        CenterObject(BtnComSerie)
        CenterObject(BtnWITS)

        LeftAlign(LblResolucion, CbResolucion.Left - 6)
        LeftAlign(LblIdioma, CbIdioma.Left - 6)

        LeftAlign(LblMotor, LblEngine.Left - 6)
        LeftAlign(LblBomba, LblPump.Left - 6)
        LeftAlign(LblTransmision, LblTransmission.Left - 6)

        LeftAlign(LblFracturador, CbFrac.Left - 6)
    End Sub

    ''Boton aceptar, guardamos en memoria lo necesario. Si hay que cambiar de pantalla cambiamos, etc
    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click


        ''Guardamos en memoria las pantallas e idioma a usar
        If CbTotalScreens.Text <> My.Settings.TotalScreens Then
            avisoStr = "Reinicio pendiente"
            avisoTitulo = "Aviso"
            AvisoGeneral.ShowDialog()
        End If

        My.Settings.Idioma = CbIdioma.Text
        My.Settings.TotalScreens = CbTotalScreens.Text
        My.Settings.Resolucion = CbResolucion.Text

        My.Settings.GraphDataCollect.Clear()
        GraphSend.Clear()
        For i = 1 To NGraphData
            Dim Cb As Control() = Me.Controls.Find("CbDato" + i.ToString(), True)
            GraphSend.Add(DataNombre.Item(DataList.IndexOf(Cb(0).Text)))
            My.Settings.GraphDataCollect.Add(DataList.IndexOf(Cb(0).Text))
        Next
        My.Settings.NGraphData = NGraphData
        My.Settings.Save()



        ''Cerramos ajustes y volvemos a la pantalla principal
        Me.Close()
    End Sub

    ''Cambiamos el idioma segun se seleccione
    Private Sub CbIdioma_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbIdioma.SelectedIndexChanged

        Select Case sender.Text
            Case "Español"
                LSelect.SetLanguage("Español")
                DataList = DataListES
            Case "Ingles"
                LSelect.SetLanguage("Ingles")
                DataList = DataListEN
        End Select
        LSelect.UpdateFracInterface()

        ''Aplicamos el idioma a todos los controles
        ''Para la primera pantalla
        Select Case My.Settings.currentScreens
            Case 1
                Dim CurrentPanel As Control() = Container1_1.Controls.Find("Panel1", True)
                If CurrentPanel(0).Controls.Find("OneFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                    Dim CurrentFrac As Control() = CurrentPanel(0).Controls.Find("OneFrac_" + My.Settings.Resolucion, True)
                    Dim C1 = CurrentFrac(0)
                    LSelect.OneFracLanguageSet(C1)
                End If
            Case 2
                For CurrentOne = 1 To 2
                    Dim CurrentPanel As Control() = Container2_1.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("TwoFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim TwoFrac = CurrentPanel(0).Controls(1)
                        If TwoFrac IsNot Nothing Then
                            LSelect.TwoFracLanguageSet(TwoFrac)
                        End If
                    End If
                Next
            Case 4
                For CurrentOne = 1 To 4
                    Dim CurrentPanel As Control() = Container4_1.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("FourFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim FourFrac = CurrentPanel(0).Controls(1)
                        If FourFrac IsNot Nothing Then
                            LSelect.FourFracLanguageSet(FourFrac)
                        End If
                    End If
                Next
            Case 8
                For CurrentOne = 1 To 8
                    Dim CurrentPanel As Control() = Container8_1.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("EightFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim EightFrac = CurrentPanel(0).Controls(1)
                        If EightFrac IsNot Nothing Then
                            LSelect.EightFracLanguageSet(EightFrac)
                        End If
                    End If
                Next
        End Select

        ''Para la segunda pantalla
        Select Case My.Settings.currentScreens_2
            Case 1
                Dim CurrentPanel As Control() = Container1_2.Controls.Find("Panel1", True)
                If CurrentPanel(0).Controls.Find("OneFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                    Dim CurrentFrac As Control() = CurrentPanel(0).Controls.Find("OneFrac_" + My.Settings.Resolucion, True)
                    Dim C1 = CurrentFrac(0)
                    LSelect.OneFracLanguageSet(C1)
                End If
            Case 2
                For CurrentOne = 1 To 2
                    Dim CurrentPanel As Control() = Container2_2.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("TwoFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim TwoFrac = CurrentPanel(0).Controls(1)
                        If TwoFrac IsNot Nothing Then
                            LSelect.TwoFracLanguageSet(TwoFrac)
                        End If
                    End If
                Next
            Case 4
                For CurrentOne = 1 To 4
                    Dim CurrentPanel As Control() = Container4_2.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("FourFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim FourFrac = CurrentPanel(0).Controls(1)
                        If FourFrac IsNot Nothing Then
                            LSelect.FourFracLanguageSet(FourFrac)
                        End If
                    End If
                Next
            Case 8
                For CurrentOne = 1 To 8
                    Dim CurrentPanel As Control() = Container8_2.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("EightFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim EightFrac = CurrentPanel(0).Controls(1)
                        If EightFrac IsNot Nothing Then
                            LSelect.EightFracLanguageSet(EightFrac)
                        End If
                    End If
                Next
        End Select

        ''Para la tercera pantalla
        Select Case My.Settings.currentScreens_3
            Case 1
                Dim CurrentPanel As Control() = Container1_3.Controls.Find("Panel1", True)
                If CurrentPanel(0).Controls.Find("OneFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                    Dim CurrentFrac As Control() = CurrentPanel(0).Controls.Find("OneFrac_" + My.Settings.Resolucion, True)
                    Dim C1 = CurrentFrac(0)
                    LSelect.OneFracLanguageSet(C1)
                End If
            Case 2
                For CurrentOne = 1 To 2
                    Dim CurrentPanel As Control() = Container2_3.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("TwoFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim TwoFrac = CurrentPanel(0).Controls(1)
                        If TwoFrac IsNot Nothing Then
                            LSelect.TwoFracLanguageSet(TwoFrac)
                        End If
                    End If
                Next
            Case 4
                For CurrentOne = 1 To 4
                    Dim CurrentPanel As Control() = Container4_3.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("FourFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim FourFrac = CurrentPanel(0).Controls(1)
                        If FourFrac IsNot Nothing Then
                            LSelect.FourFracLanguageSet(FourFrac)
                        End If
                    End If
                Next
            Case 8
                For CurrentOne = 1 To 8
                    Dim CurrentPanel As Control() = Container8_3.Controls.Find("Panel" + CurrentOne.ToString, True)
                    If CurrentPanel(0).Controls.Find("EightFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                        Dim EightFrac = CurrentPanel(0).Controls(1)
                        If EightFrac IsNot Nothing Then
                            LSelect.EightFracLanguageSet(EightFrac)
                        End If
                    End If
                Next
        End Select
    End Sub

    ''Actualizamos el motor, transmision y bomba segun el fracturador
    Private Sub CbFrac_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbFrac.SelectedIndexChanged
        LblEngine.Text = FracEdit.GetFracEngine(ISystem.GetFracID(sender.Text))
        LblTransmission.Text = FracEdit.GetFracTransmission(ISystem.GetFracID(sender.Text))
        LblPump.Text = FracEdit.GetFracPump(ISystem.GetFracID(sender.Text))
    End Sub

    ''Configuracion de la comunicacion serie
    Private Sub BtnComSerie_Click(sender As Object, e As EventArgs) Handles BtnComSerie.Click
        SerialPortConfig.ShowDialog(Me)
    End Sub


    ''Configuracion de los fracturadores en base de datos
    Private Sub BtnConfigFrac_Click(sender As Object, e As EventArgs)
        ConfigFrac.Show(Me)
    End Sub

    ''Escondemos el form cuando se lo quiera cerrar
    Private Sub FormClose(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
        Hide()
    End Sub

    ''Configuracion de WITS
    Private Sub BtnWITS_Click(sender As Object, e As EventArgs) Handles BtnWITS.Click
        ConfigWITS.Show(Me)
    End Sub

    Private Sub BtnAvanzado_Click(sender As Object, e As EventArgs) Handles BtnAvanzado.Click
        ConfigAvanzado.Show(Me)
    End Sub

    Private Sub BtnStartGraph_Click(sender As Object, e As EventArgs) Handles BtnStartGraph.Click


        My.Settings.GraphDataCollect.Clear()
        GraphSend.Clear()
        For i = 1 To NGraphData
            Dim Cb As Control() = Me.Controls.Find("CbDato" + i.ToString(), True)
            GraphSend.Add(DataNombre.Item(DataList.IndexOf(Cb(0).Text)))
            My.Settings.GraphDataCollect.Add(DataList.IndexOf(Cb(0).Text))
        Next
        My.Settings.NGraphData = NGraphData
        My.Settings.Save()

        If (IsConnected And SGraphConnect.Protocol = "TCP") Or (SGraphConnect.Protocol = "PC") Then

            BtnStartGraph.Checked = Not BtnStartGraph.Checked
            If BtnStartGraph.Checked Then
                SGraphConnect.DataNombre = GetAllGraphNames(GraphSend.ToArray)
                Inicio.TmrGraph.Start()
                GraphConnect.StartWrite()
            Else
                Inicio.TmrGraph.Stop()
                GraphConnect.StopWrite()
            End If
        End If
    End Sub


    Private Sub BtnAddData_Click(sender As Object, e As EventArgs) Handles BtnAddData.Click

        NGraphData += 1

        Dim LblAnterior As Control() = Me.Controls.Find("LblDato" + (NGraphData - 1).ToString(), True)
        Dim CbAnterior As Control() = Me.Controls.Find("CbDato" + (NGraphData - 1).ToString(), True)

        Dim Cb As New Guna.UI2.WinForms.Guna2ComboBox With {
            .Name = "CbDato" + NGraphData.ToString(),
            .Size = New Size(237, 36),
            .Location = New Point(CbAnterior(0).Left, CbAnterior(0).Top + 60),
            .Font = New Font("Montserrat", 10),
            .TextAlign = HorizontalAlignment.Center,
            .BorderThickness = 1,
            .BorderStyle = Drawing2D.DashStyle.Solid,
            .Cursor = Cursors.Hand
        }

        Dim Lb As New Label With {
            .Name = "LblDato" + NGraphData.ToString(),
            .Text = LanguageSelect.DynamicLanguageRM.GetString("Dato") + NGraphData.ToString() + ":",
            .ForeColor = Color.White,
            .Font = New Font("Montserrat", 11),
            .AutoSize = True
        }

        DataPanel.Controls.Add(Lb)
        Lb.Location = New Point(Cb.Left - Lb.Width - 6, LblAnterior(0).Top + 60)
        DataPanel.Controls.Add(Cb)

        DataPanel.ScrollControlIntoView(Cb)

        Cb.Items.AddRange(DataList.ToArray)
    End Sub

    Private Sub BtnDelData_Click(sender As Object, e As EventArgs) Handles BtnDelData.Click
        If NGraphData > 1 Then
            Dim Cb As Control() = Me.Controls.Find("CbDato" + (NGraphData).ToString(), True)
            DataPanel.Controls.Remove(Cb(0))
            Dim Lb As Control() = Me.Controls.Find("LblDato" + (NGraphData).ToString(), True)
            DataPanel.Controls.Remove(Lb(0))
            NGraphData -= 1
        End If
    End Sub

    Private Sub TmrRefresh_Tick(sender As Object, e As EventArgs) Handles TmrRefresh.Tick
        TmrRefresh.Stop()
        Refresh()
    End Sub

    Private Function GetAllGraphNames(ByVal Names As String()) As String()
        Dim ReturnString(Names.Length * ConnectedFracsID.Count - 1) As String
        For ThisFrac = 0 To ConnectedFracsID.Count - 1
            For CurrentDataName = 0 To Names.Length - 1
                ReturnString(CurrentDataName + (ThisFrac * (Names.Length))) = Names(CurrentDataName) + ConnectedFracsID(ThisFrac).ToString()
            Next
        Next
        Return ReturnString
    End Function

End Class