﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ExcelGraphs
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.CbRPM = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.CbPresCaud = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CbPresion = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CbCaudal = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CbEngTrans = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.CbPump = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.Title
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(115, 3)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(101, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Gráficos"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(282, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(330, 38)
        Me.TitlePanel.TabIndex = 165
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.TitlePanel
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'CbRPM
        '
        Me.CbRPM.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbRPM.CheckedState.BorderRadius = 2
        Me.CbRPM.CheckedState.BorderThickness = 0
        Me.CbRPM.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbRPM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbRPM.Location = New System.Drawing.Point(17, 51)
        Me.CbRPM.Name = "CbRPM"
        Me.CbRPM.Size = New System.Drawing.Size(20, 20)
        Me.CbRPM.TabIndex = 183
        Me.CbRPM.Text = "Guna2CustomCheckBox2"
        Me.CbRPM.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbRPM.UncheckedState.BorderRadius = 2
        Me.CbRPM.UncheckedState.BorderThickness = 0
        Me.CbRPM.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(43, 51)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(187, 21)
        Me.Label15.TabIndex = 182
        Me.Label15.Text = "Generar grafico de RPM"
        '
        'CbPresCaud
        '
        Me.CbPresCaud.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPresCaud.CheckedState.BorderRadius = 2
        Me.CbPresCaud.CheckedState.BorderThickness = 0
        Me.CbPresCaud.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPresCaud.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbPresCaud.Location = New System.Drawing.Point(17, 147)
        Me.CbPresCaud.Name = "CbPresCaud"
        Me.CbPresCaud.Size = New System.Drawing.Size(20, 20)
        Me.CbPresCaud.TabIndex = 185
        Me.CbPresCaud.Text = "Guna2CustomCheckBox2"
        Me.CbPresCaud.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbPresCaud.UncheckedState.BorderRadius = 2
        Me.CbPresCaud.UncheckedState.BorderThickness = 0
        Me.CbPresCaud.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(43, 147)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(265, 21)
        Me.Label1.TabIndex = 184
        Me.Label1.Text = "Generar grafico de Caudal/Presion"
        '
        'CbPresion
        '
        Me.CbPresion.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPresion.CheckedState.BorderRadius = 2
        Me.CbPresion.CheckedState.BorderThickness = 0
        Me.CbPresion.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPresion.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbPresion.Location = New System.Drawing.Point(17, 83)
        Me.CbPresion.Name = "CbPresion"
        Me.CbPresion.Size = New System.Drawing.Size(20, 20)
        Me.CbPresion.TabIndex = 187
        Me.CbPresion.Text = "Guna2CustomCheckBox2"
        Me.CbPresion.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbPresion.UncheckedState.BorderRadius = 2
        Me.CbPresion.UncheckedState.BorderThickness = 0
        Me.CbPresion.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(43, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(206, 21)
        Me.Label2.TabIndex = 186
        Me.Label2.Text = "Generar grafico de presión"
        '
        'CbCaudal
        '
        Me.CbCaudal.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbCaudal.CheckedState.BorderRadius = 2
        Me.CbCaudal.CheckedState.BorderThickness = 0
        Me.CbCaudal.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbCaudal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbCaudal.Location = New System.Drawing.Point(17, 115)
        Me.CbCaudal.Name = "CbCaudal"
        Me.CbCaudal.Size = New System.Drawing.Size(20, 20)
        Me.CbCaudal.TabIndex = 189
        Me.CbCaudal.Text = "Guna2CustomCheckBox3"
        Me.CbCaudal.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbCaudal.UncheckedState.BorderRadius = 2
        Me.CbCaudal.UncheckedState.BorderThickness = 0
        Me.CbCaudal.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(43, 115)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(201, 21)
        Me.Label3.TabIndex = 188
        Me.Label3.Text = "Generar grafico de caudal"
        '
        'CbEngTrans
        '
        Me.CbEngTrans.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbEngTrans.CheckedState.BorderRadius = 2
        Me.CbEngTrans.CheckedState.BorderThickness = 0
        Me.CbEngTrans.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbEngTrans.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbEngTrans.Location = New System.Drawing.Point(17, 179)
        Me.CbEngTrans.Name = "CbEngTrans"
        Me.CbEngTrans.Size = New System.Drawing.Size(20, 20)
        Me.CbEngTrans.TabIndex = 191
        Me.CbEngTrans.Text = "Guna2CustomCheckBox2"
        Me.CbEngTrans.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbEngTrans.UncheckedState.BorderRadius = 2
        Me.CbEngTrans.UncheckedState.BorderThickness = 0
        Me.CbEngTrans.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(43, 179)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(289, 21)
        Me.Label4.TabIndex = 190
        Me.Label4.Text = "Generar grafico de Motor/Transmision"
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(111, 245)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(109, 40)
        Me.BtnAceptar.TabIndex = 192
        Me.BtnAceptar.Text = "Aceptar"
        '
        'CbPump
        '
        Me.CbPump.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPump.CheckedState.BorderRadius = 2
        Me.CbPump.CheckedState.BorderThickness = 0
        Me.CbPump.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPump.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbPump.Location = New System.Drawing.Point(17, 211)
        Me.CbPump.Name = "CbPump"
        Me.CbPump.Size = New System.Drawing.Size(20, 20)
        Me.CbPump.TabIndex = 194
        Me.CbPump.Text = "Guna2CustomCheckBox2"
        Me.CbPump.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbPump.UncheckedState.BorderRadius = 2
        Me.CbPump.UncheckedState.BorderThickness = 0
        Me.CbPump.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(43, 211)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(205, 21)
        Me.Label5.TabIndex = 193
        Me.Label5.Text = "Generar grafico de bomba"
        '
        'ExcelGraphs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(330, 297)
        Me.Controls.Add(Me.CbPump)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.CbEngTrans)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CbCaudal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CbPresion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CbPresCaud)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CbRPM)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "ExcelGraphs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ExcelGraphs"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Title As Label
    Public WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents CbCaudal As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label3 As Label
    Friend WithEvents CbPresion As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label2 As Label
    Friend WithEvents CbPresCaud As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label1 As Label
    Friend WithEvents CbRPM As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label15 As Label
    Friend WithEvents CbEngTrans As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label4 As Label
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents CbPump As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label5 As Label
End Class
