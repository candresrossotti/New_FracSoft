﻿Public Class FractureInfo
    Private Sub FractureInfo_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        TbYacimiento.Text = Yacimiento
        TbEtapa.Text = Etapa
        TbLocacion.Text = Locacion
        TbTipoPozo.Text = TipoPozo
        TbDistrito.Text = Distrito
        TbProvincia.Text = Provincia
        TbCliente.Text = Cliente

        CenterEverything()
    End Sub

    Private Sub CenterEverything()
        CenterObject(BtnAceptar)
        CenterObject(Title)

        CenterToScreen()
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click

        Yacimiento = TbYacimiento.Text
        Etapa = GetInteger(TbEtapa.Text)
        Locacion = TbLocacion.Text
        TipoPozo = TbTipoPozo.Text
        Distrito = TbDistrito.Text
        Provincia = TbProvincia.Text
        Cliente = TbCliente.Text

        My.Settings.Yacimiento = Yacimiento
        My.Settings.Etapa = Etapa
        My.Settings.Locacion = Locacion
        My.Settings.TipoPozo = TipoPozo
        My.Settings.Distrito = Distrito
        My.Settings.Provincia = Provincia
        My.Settings.Cliente = Cliente

        My.Settings.Save()

        Close()
    End Sub
End Class