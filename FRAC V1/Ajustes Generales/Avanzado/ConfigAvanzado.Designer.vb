﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ConfigAvanzado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CbMultipleConnections = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnDB = New Guna.UI2.WinForms.Guna2Button()
        Me.FBD = New System.Windows.Forms.FolderBrowserDialog()
        Me.Guna2Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.CbExcel = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.CbPDF = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CbCSVBackup = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtnExcelGraphs = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel3 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblReportes = New System.Windows.Forms.Label()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.TbPDFOP = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TbCSVOP = New Guna.UI2.WinForms.Guna2TextBox()
        Me.TbCSVBU = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblDirectorios = New System.Windows.Forms.Label()
        Me.Guna2Panel3 = New Guna.UI2.WinForms.Guna2Panel()
        Me.CbConnect = New Guna.UI2.WinForms.Guna2CheckBox()
        Me.PortCB = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.TbID = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.PingCB = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.TbPORT = New Guna.UI2.WinForms.Guna2TextBox()
        Me.TbIP = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.CbTCP = New Guna.UI2.WinForms.Guna2CheckBox()
        Me.CbProtocolo = New Guna.UI2.WinForms.Guna2CheckBox()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblGraficador = New System.Windows.Forms.Label()
        Me.Guna2Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.CbBackupFrac = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Guna2CustomCheckBox1 = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Guna2TextBox3 = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblAuto = New System.Windows.Forms.Label()
        Me.Guna2Panel5 = New Guna.UI2.WinForms.Guna2Panel()
        Me.TbCliente = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TbProvincia = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TbDistrito = New Guna.UI2.WinForms.Guna2TextBox()
        Me.TbTipoPozo = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TbLocacion = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TbEtapa = New Guna.UI2.WinForms.Guna2TextBox()
        Me.TbYacimiento = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel5 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Guna2Panel6 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2CustomGradientPanel6 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.Guna2Panel2.SuspendLayout()
        Me.Guna2CustomGradientPanel3.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.Guna2Panel3.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        Me.Guna2Panel4.SuspendLayout()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.Guna2Panel5.SuspendLayout()
        Me.Guna2CustomGradientPanel5.SuspendLayout()
        Me.Guna2Panel6.SuspendLayout()
        Me.Guna2CustomGradientPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(478, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(94, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Ajustes"
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(1050, 38)
        Me.TitlePanel.TabIndex = 78
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(1002, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(38, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(233, 21)
        Me.Label1.TabIndex = 79
        Me.Label1.Text = "Permitir multiples conexiones"
        '
        'CbMultipleConnections
        '
        Me.CbMultipleConnections.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbMultipleConnections.CheckedState.BorderRadius = 2
        Me.CbMultipleConnections.CheckedState.BorderThickness = 0
        Me.CbMultipleConnections.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbMultipleConnections.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbMultipleConnections.Location = New System.Drawing.Point(12, 33)
        Me.CbMultipleConnections.Name = "CbMultipleConnections"
        Me.CbMultipleConnections.Size = New System.Drawing.Size(20, 20)
        Me.CbMultipleConnections.TabIndex = 81
        Me.CbMultipleConnections.Text = "Guna2CustomCheckBox1"
        Me.CbMultipleConnections.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbMultipleConnections.UncheckedState.BorderRadius = 2
        Me.CbMultipleConnections.UncheckedState.BorderThickness = 0
        Me.CbMultipleConnections.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(734, 398)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(149, 65)
        Me.BtnAceptar.TabIndex = 92
        Me.BtnAceptar.Text = "Aceptar"
        '
        'BtnDB
        '
        Me.BtnDB.Animated = True
        Me.BtnDB.BorderColor = System.Drawing.Color.White
        Me.BtnDB.BorderRadius = 5
        Me.BtnDB.BorderThickness = 1
        Me.BtnDB.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDB.FillColor = System.Drawing.Color.Transparent
        Me.BtnDB.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnDB.ForeColor = System.Drawing.Color.White
        Me.BtnDB.Location = New System.Drawing.Point(889, 398)
        Me.BtnDB.Name = "BtnDB"
        Me.BtnDB.Size = New System.Drawing.Size(150, 65)
        Me.BtnDB.TabIndex = 185
        Me.BtnDB.Text = "Base de datos"
        '
        'Guna2Panel2
        '
        Me.Guna2Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.BorderRadius = 6
        Me.Guna2Panel2.BorderThickness = 1
        Me.Guna2Panel2.Controls.Add(Me.CbExcel)
        Me.Guna2Panel2.Controls.Add(Me.Label9)
        Me.Guna2Panel2.Controls.Add(Me.CbPDF)
        Me.Guna2Panel2.Controls.Add(Me.Label10)
        Me.Guna2Panel2.Controls.Add(Me.CbCSVBackup)
        Me.Guna2Panel2.Controls.Add(Me.Label2)
        Me.Guna2Panel2.Controls.Add(Me.BtnExcelGraphs)
        Me.Guna2Panel2.Controls.Add(Me.Guna2CustomGradientPanel3)
        Me.Guna2Panel2.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.Location = New System.Drawing.Point(12, 44)
        Me.Guna2Panel2.Name = "Guna2Panel2"
        Me.Guna2Panel2.Size = New System.Drawing.Size(279, 171)
        Me.Guna2Panel2.TabIndex = 188
        '
        'CbExcel
        '
        Me.CbExcel.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbExcel.CheckedState.BorderRadius = 2
        Me.CbExcel.CheckedState.BorderThickness = 0
        Me.CbExcel.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbExcel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbExcel.Location = New System.Drawing.Point(12, 129)
        Me.CbExcel.Name = "CbExcel"
        Me.CbExcel.Size = New System.Drawing.Size(20, 20)
        Me.CbExcel.TabIndex = 197
        Me.CbExcel.Text = "Guna2CustomCheckBox2"
        Me.CbExcel.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbExcel.UncheckedState.BorderRadius = 2
        Me.CbExcel.UncheckedState.BorderThickness = 0
        Me.CbExcel.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(38, 129)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(194, 21)
        Me.Label9.TabIndex = 196
        Me.Label9.Text = "Generar reporte en excel"
        '
        'CbPDF
        '
        Me.CbPDF.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPDF.CheckedState.BorderRadius = 2
        Me.CbPDF.CheckedState.BorderThickness = 0
        Me.CbPDF.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPDF.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbPDF.Location = New System.Drawing.Point(12, 85)
        Me.CbPDF.Name = "CbPDF"
        Me.CbPDF.Size = New System.Drawing.Size(20, 20)
        Me.CbPDF.TabIndex = 195
        Me.CbPDF.Text = "Guna2CustomCheckBox2"
        Me.CbPDF.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbPDF.UncheckedState.BorderRadius = 2
        Me.CbPDF.UncheckedState.BorderThickness = 0
        Me.CbPDF.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(38, 85)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(188, 21)
        Me.Label10.TabIndex = 194
        Me.Label10.Text = "Generar reporte en PDF"
        '
        'CbCSVBackup
        '
        Me.CbCSVBackup.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbCSVBackup.CheckedState.BorderRadius = 2
        Me.CbCSVBackup.CheckedState.BorderThickness = 0
        Me.CbCSVBackup.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbCSVBackup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbCSVBackup.Location = New System.Drawing.Point(12, 41)
        Me.CbCSVBackup.Name = "CbCSVBackup"
        Me.CbCSVBackup.Size = New System.Drawing.Size(20, 20)
        Me.CbCSVBackup.TabIndex = 193
        Me.CbCSVBackup.Text = "Guna2CustomCheckBox2"
        Me.CbCSVBackup.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbCSVBackup.UncheckedState.BorderRadius = 2
        Me.CbCSVBackup.UncheckedState.BorderThickness = 0
        Me.CbCSVBackup.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(38, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(187, 21)
        Me.Label2.TabIndex = 192
        Me.Label2.Text = "Generar CSV de backup"
        '
        'BtnExcelGraphs
        '
        Me.BtnExcelGraphs.Animated = True
        Me.BtnExcelGraphs.BorderColor = System.Drawing.Color.White
        Me.BtnExcelGraphs.BorderRadius = 5
        Me.BtnExcelGraphs.BorderThickness = 1
        Me.BtnExcelGraphs.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnExcelGraphs.FillColor = System.Drawing.Color.Transparent
        Me.BtnExcelGraphs.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnExcelGraphs.ForeColor = System.Drawing.Color.White
        Me.BtnExcelGraphs.Location = New System.Drawing.Point(233, 122)
        Me.BtnExcelGraphs.Name = "BtnExcelGraphs"
        Me.BtnExcelGraphs.Size = New System.Drawing.Size(35, 30)
        Me.BtnExcelGraphs.TabIndex = 185
        Me.BtnExcelGraphs.Text = "+"
        '
        'Guna2CustomGradientPanel3
        '
        Me.Guna2CustomGradientPanel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.BorderRadius = 6
        Me.Guna2CustomGradientPanel3.Controls.Add(Me.LblReportes)
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel3.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel3.Name = "Guna2CustomGradientPanel3"
        Me.Guna2CustomGradientPanel3.Size = New System.Drawing.Size(279, 26)
        Me.Guna2CustomGradientPanel3.TabIndex = 19
        '
        'LblReportes
        '
        Me.LblReportes.AutoSize = True
        Me.LblReportes.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblReportes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblReportes.Location = New System.Drawing.Point(101, 3)
        Me.LblReportes.Name = "LblReportes"
        Me.LblReportes.Size = New System.Drawing.Size(77, 21)
        Me.LblReportes.TabIndex = 19
        Me.LblReportes.Text = "Reportes"
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.TbPDFOP)
        Me.Guna2Panel1.Controls.Add(Me.Label7)
        Me.Guna2Panel1.Controls.Add(Me.TbCSVOP)
        Me.Guna2Panel1.Controls.Add(Me.TbCSVBU)
        Me.Guna2Panel1.Controls.Add(Me.Label4)
        Me.Guna2Panel1.Controls.Add(Me.Label3)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(297, 44)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(431, 171)
        Me.Guna2Panel1.TabIndex = 198
        '
        'TbPDFOP
        '
        Me.TbPDFOP.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbPDFOP.DefaultText = ""
        Me.TbPDFOP.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbPDFOP.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbPDFOP.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPDFOP.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPDFOP.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPDFOP.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbPDFOP.ForeColor = System.Drawing.Color.Black
        Me.TbPDFOP.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPDFOP.Location = New System.Drawing.Point(244, 121)
        Me.TbPDFOP.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbPDFOP.Name = "TbPDFOP"
        Me.TbPDFOP.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbPDFOP.PlaceholderText = ""
        Me.TbPDFOP.SelectedText = ""
        Me.TbPDFOP.Size = New System.Drawing.Size(175, 36)
        Me.TbPDFOP.TabIndex = 193
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(37, 129)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(201, 21)
        Me.Label7.TabIndex = 192
        Me.Label7.Text = "Direccion de reporte PDF:"
        '
        'TbCSVOP
        '
        Me.TbCSVOP.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbCSVOP.DefaultText = ""
        Me.TbCSVOP.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbCSVOP.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbCSVOP.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCSVOP.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCSVOP.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCSVOP.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbCSVOP.ForeColor = System.Drawing.Color.Black
        Me.TbCSVOP.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCSVOP.Location = New System.Drawing.Point(244, 77)
        Me.TbCSVOP.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbCSVOP.Name = "TbCSVOP"
        Me.TbCSVOP.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbCSVOP.PlaceholderText = ""
        Me.TbCSVOP.SelectedText = ""
        Me.TbCSVOP.Size = New System.Drawing.Size(175, 36)
        Me.TbCSVOP.TabIndex = 191
        '
        'TbCSVBU
        '
        Me.TbCSVBU.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbCSVBU.DefaultText = ""
        Me.TbCSVBU.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbCSVBU.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbCSVBU.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCSVBU.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCSVBU.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCSVBU.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbCSVBU.ForeColor = System.Drawing.Color.Black
        Me.TbCSVBU.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCSVBU.Location = New System.Drawing.Point(244, 33)
        Me.TbCSVBU.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbCSVBU.Name = "TbCSVBU"
        Me.TbCSVBU.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbCSVBU.PlaceholderText = ""
        Me.TbCSVBU.SelectedText = ""
        Me.TbCSVBU.Size = New System.Drawing.Size(175, 36)
        Me.TbCSVBU.TabIndex = 190
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(0, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(240, 21)
        Me.Label4.TabIndex = 189
        Me.Label4.Text = "Direccion de CSV de operacion:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(69, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(169, 21)
        Me.Label3.TabIndex = 188
        Me.Label3.Text = "Direccion de BackUp:"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblDirectorios)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(431, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblDirectorios
        '
        Me.LblDirectorios.AutoSize = True
        Me.LblDirectorios.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblDirectorios.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblDirectorios.Location = New System.Drawing.Point(170, 3)
        Me.LblDirectorios.Name = "LblDirectorios"
        Me.LblDirectorios.Size = New System.Drawing.Size(90, 21)
        Me.LblDirectorios.TabIndex = 19
        Me.LblDirectorios.Text = "Directorios"
        '
        'Guna2Panel3
        '
        Me.Guna2Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel3.BorderRadius = 6
        Me.Guna2Panel3.BorderThickness = 1
        Me.Guna2Panel3.Controls.Add(Me.CbConnect)
        Me.Guna2Panel3.Controls.Add(Me.PortCB)
        Me.Guna2Panel3.Controls.Add(Me.TbID)
        Me.Guna2Panel3.Controls.Add(Me.Label22)
        Me.Guna2Panel3.Controls.Add(Me.PingCB)
        Me.Guna2Panel3.Controls.Add(Me.TbPORT)
        Me.Guna2Panel3.Controls.Add(Me.TbIP)
        Me.Guna2Panel3.Controls.Add(Me.Label23)
        Me.Guna2Panel3.Controls.Add(Me.Label24)
        Me.Guna2Panel3.Controls.Add(Me.CbTCP)
        Me.Guna2Panel3.Controls.Add(Me.CbProtocolo)
        Me.Guna2Panel3.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.Guna2Panel3.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel3.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel3.Location = New System.Drawing.Point(297, 221)
        Me.Guna2Panel3.Name = "Guna2Panel3"
        Me.Guna2Panel3.Size = New System.Drawing.Size(431, 242)
        Me.Guna2Panel3.TabIndex = 199
        '
        'CbConnect
        '
        Me.CbConnect.AutoSize = True
        Me.CbConnect.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbConnect.CheckedState.BorderRadius = 0
        Me.CbConnect.CheckedState.BorderThickness = 0
        Me.CbConnect.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbConnect.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbConnect.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbConnect.ForeColor = System.Drawing.Color.White
        Me.CbConnect.Location = New System.Drawing.Point(135, 206)
        Me.CbConnect.Name = "CbConnect"
        Me.CbConnect.Size = New System.Drawing.Size(152, 25)
        Me.CbConnect.TabIndex = 199
        Me.CbConnect.Text = "Activar conexión"
        Me.CbConnect.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbConnect.UncheckedState.BorderRadius = 0
        Me.CbConnect.UncheckedState.BorderThickness = 0
        Me.CbConnect.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'PortCB
        '
        Me.PortCB.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.PortCB.CheckedState.BorderRadius = 2
        Me.PortCB.CheckedState.BorderThickness = 0
        Me.PortCB.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.PortCB.Enabled = False
        Me.PortCB.Location = New System.Drawing.Point(356, 121)
        Me.PortCB.Name = "PortCB"
        Me.PortCB.Size = New System.Drawing.Size(20, 20)
        Me.PortCB.TabIndex = 203
        Me.PortCB.Text = "Guna2CustomCheckBox3"
        Me.PortCB.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.PortCB.UncheckedState.BorderRadius = 2
        Me.PortCB.UncheckedState.BorderThickness = 0
        Me.PortCB.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'TbID
        '
        Me.TbID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbID.DefaultText = ""
        Me.TbID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbID.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbID.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbID.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbID.ForeColor = System.Drawing.Color.Black
        Me.TbID.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbID.Location = New System.Drawing.Point(175, 159)
        Me.TbID.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbID.Name = "TbID"
        Me.TbID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbID.PlaceholderText = ""
        Me.TbID.SelectedText = ""
        Me.TbID.Size = New System.Drawing.Size(175, 36)
        Me.TbID.TabIndex = 2
        Me.TbID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(35, 167)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(135, 21)
        Me.Label22.TabIndex = 198
        Me.Label22.Text = "ID de dispositivo:"
        '
        'PingCB
        '
        Me.PingCB.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.PingCB.CheckedState.BorderRadius = 2
        Me.PingCB.CheckedState.BorderThickness = 0
        Me.PingCB.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.PingCB.Enabled = False
        Me.PingCB.Location = New System.Drawing.Point(356, 75)
        Me.PingCB.Name = "PingCB"
        Me.PingCB.Size = New System.Drawing.Size(20, 20)
        Me.PingCB.TabIndex = 202
        Me.PingCB.Text = "Guna2CustomCheckBox4"
        Me.PingCB.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.PingCB.UncheckedState.BorderRadius = 2
        Me.PingCB.UncheckedState.BorderThickness = 0
        Me.PingCB.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'TbPORT
        '
        Me.TbPORT.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbPORT.DefaultText = ""
        Me.TbPORT.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbPORT.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbPORT.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPORT.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbPORT.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPORT.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbPORT.ForeColor = System.Drawing.Color.Black
        Me.TbPORT.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbPORT.Location = New System.Drawing.Point(175, 113)
        Me.TbPORT.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbPORT.Name = "TbPORT"
        Me.TbPORT.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbPORT.PlaceholderText = ""
        Me.TbPORT.SelectedText = ""
        Me.TbPORT.Size = New System.Drawing.Size(175, 36)
        Me.TbPORT.TabIndex = 199
        Me.TbPORT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TbIP
        '
        Me.TbIP.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbIP.DefaultText = ""
        Me.TbIP.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbIP.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbIP.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbIP.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbIP.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbIP.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbIP.ForeColor = System.Drawing.Color.Black
        Me.TbIP.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbIP.Location = New System.Drawing.Point(175, 67)
        Me.TbIP.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbIP.Name = "TbIP"
        Me.TbIP.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbIP.PlaceholderText = ""
        Me.TbIP.SelectedText = ""
        Me.TbIP.Size = New System.Drawing.Size(175, 36)
        Me.TbIP.TabIndex = 198
        Me.TbIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(25, 121)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(145, 21)
        Me.Label23.TabIndex = 201
        Me.Label23.Text = "Puerto Graficador:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(62, 75)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(107, 21)
        Me.Label24.TabIndex = 200
        Me.Label24.Text = "IP Graficador"
        '
        'CbTCP
        '
        Me.CbTCP.AutoSize = True
        Me.CbTCP.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbTCP.CheckedState.BorderRadius = 0
        Me.CbTCP.CheckedState.BorderThickness = 0
        Me.CbTCP.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbTCP.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbTCP.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbTCP.ForeColor = System.Drawing.Color.White
        Me.CbTCP.Location = New System.Drawing.Point(289, 34)
        Me.CbTCP.Name = "CbTCP"
        Me.CbTCP.Size = New System.Drawing.Size(80, 25)
        Me.CbTCP.TabIndex = 197
        Me.CbTCP.Text = "TCP/IP"
        Me.CbTCP.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbTCP.UncheckedState.BorderRadius = 0
        Me.CbTCP.UncheckedState.BorderThickness = 0
        Me.CbTCP.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'CbProtocolo
        '
        Me.CbProtocolo.AutoSize = True
        Me.CbProtocolo.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbProtocolo.CheckedState.BorderRadius = 0
        Me.CbProtocolo.CheckedState.BorderThickness = 0
        Me.CbProtocolo.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbProtocolo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbProtocolo.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbProtocolo.ForeColor = System.Drawing.Color.White
        Me.CbProtocolo.Location = New System.Drawing.Point(47, 34)
        Me.CbProtocolo.Name = "CbProtocolo"
        Me.CbProtocolo.Size = New System.Drawing.Size(158, 25)
        Me.CbProtocolo.TabIndex = 196
        Me.CbProtocolo.Text = "Protocolo interno"
        Me.CbProtocolo.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbProtocolo.UncheckedState.BorderRadius = 0
        Me.CbProtocolo.UncheckedState.BorderThickness = 0
        Me.CbProtocolo.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblGraficador)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(431, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblGraficador
        '
        Me.LblGraficador.AutoSize = True
        Me.LblGraficador.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblGraficador.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblGraficador.Location = New System.Drawing.Point(172, 3)
        Me.LblGraficador.Name = "LblGraficador"
        Me.LblGraficador.Size = New System.Drawing.Size(87, 21)
        Me.LblGraficador.TabIndex = 19
        Me.LblGraficador.Text = "Graficador"
        '
        'Guna2Panel4
        '
        Me.Guna2Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.BorderRadius = 6
        Me.Guna2Panel4.BorderThickness = 1
        Me.Guna2Panel4.Controls.Add(Me.CbBackupFrac)
        Me.Guna2Panel4.Controls.Add(Me.Label20)
        Me.Guna2Panel4.Controls.Add(Me.Guna2CustomCheckBox1)
        Me.Guna2Panel4.Controls.Add(Me.Label12)
        Me.Guna2Panel4.Controls.Add(Me.Guna2TextBox3)
        Me.Guna2Panel4.Controls.Add(Me.Label11)
        Me.Guna2Panel4.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.Guna2Panel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.Location = New System.Drawing.Point(12, 221)
        Me.Guna2Panel4.Name = "Guna2Panel4"
        Me.Guna2Panel4.Size = New System.Drawing.Size(279, 171)
        Me.Guna2Panel4.TabIndex = 200
        '
        'CbBackupFrac
        '
        Me.CbBackupFrac.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbBackupFrac.CheckedState.BorderRadius = 2
        Me.CbBackupFrac.CheckedState.BorderThickness = 0
        Me.CbBackupFrac.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbBackupFrac.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbBackupFrac.Location = New System.Drawing.Point(12, 40)
        Me.CbBackupFrac.Name = "CbBackupFrac"
        Me.CbBackupFrac.Size = New System.Drawing.Size(20, 20)
        Me.CbBackupFrac.TabIndex = 112
        Me.CbBackupFrac.Text = "Guna2CustomCheckBox2"
        Me.CbBackupFrac.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbBackupFrac.UncheckedState.BorderRadius = 2
        Me.CbBackupFrac.UncheckedState.BorderThickness = 0
        Me.CbBackupFrac.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(38, 40)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(218, 21)
        Me.Label20.TabIndex = 111
        Me.Label20.Text = "Permitir fracturador backup"
        '
        'Guna2CustomCheckBox1
        '
        Me.Guna2CustomCheckBox1.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Guna2CustomCheckBox1.CheckedState.BorderRadius = 2
        Me.Guna2CustomCheckBox1.CheckedState.BorderThickness = 0
        Me.Guna2CustomCheckBox1.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Guna2CustomCheckBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2CustomCheckBox1.Location = New System.Drawing.Point(12, 84)
        Me.Guna2CustomCheckBox1.Name = "Guna2CustomCheckBox1"
        Me.Guna2CustomCheckBox1.Size = New System.Drawing.Size(20, 20)
        Me.Guna2CustomCheckBox1.TabIndex = 110
        Me.Guna2CustomCheckBox1.Text = "Guna2CustomCheckBox2"
        Me.Guna2CustomCheckBox1.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.Guna2CustomCheckBox1.UncheckedState.BorderRadius = 2
        Me.Guna2CustomCheckBox1.UncheckedState.BorderThickness = 0
        Me.Guna2CustomCheckBox1.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(38, 84)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(95, 21)
        Me.Label12.TabIndex = 109
        Me.Label12.Text = "Activar SFC"
        '
        'Guna2TextBox3
        '
        Me.Guna2TextBox3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Guna2TextBox3.DefaultText = ""
        Me.Guna2TextBox3.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.Guna2TextBox3.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.Guna2TextBox3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.Guna2TextBox3.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.Guna2TextBox3.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Guna2TextBox3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Guna2TextBox3.ForeColor = System.Drawing.Color.Black
        Me.Guna2TextBox3.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Guna2TextBox3.Location = New System.Drawing.Point(101, 120)
        Me.Guna2TextBox3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Guna2TextBox3.Name = "Guna2TextBox3"
        Me.Guna2TextBox3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Guna2TextBox3.PlaceholderText = ""
        Me.Guna2TextBox3.SelectedText = ""
        Me.Guna2TextBox3.Size = New System.Drawing.Size(145, 36)
        Me.Guna2TextBox3.TabIndex = 108
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(8, 128)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(86, 20)
        Me.Label11.TabIndex = 107
        Me.Label11.Text = "Indice SFR:"
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.LblAuto)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(279, 26)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'LblAuto
        '
        Me.LblAuto.AutoSize = True
        Me.LblAuto.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblAuto.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblAuto.Location = New System.Drawing.Point(75, 3)
        Me.LblAuto.Name = "LblAuto"
        Me.LblAuto.Size = New System.Drawing.Size(129, 21)
        Me.LblAuto.TabIndex = 19
        Me.LblAuto.Text = "Automatizacion"
        '
        'Guna2Panel5
        '
        Me.Guna2Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.BorderRadius = 6
        Me.Guna2Panel5.BorderThickness = 1
        Me.Guna2Panel5.Controls.Add(Me.TbCliente)
        Me.Guna2Panel5.Controls.Add(Me.Label19)
        Me.Guna2Panel5.Controls.Add(Me.TbProvincia)
        Me.Guna2Panel5.Controls.Add(Me.Label16)
        Me.Guna2Panel5.Controls.Add(Me.TbDistrito)
        Me.Guna2Panel5.Controls.Add(Me.TbTipoPozo)
        Me.Guna2Panel5.Controls.Add(Me.Label17)
        Me.Guna2Panel5.Controls.Add(Me.Label18)
        Me.Guna2Panel5.Controls.Add(Me.TbLocacion)
        Me.Guna2Panel5.Controls.Add(Me.Label8)
        Me.Guna2Panel5.Controls.Add(Me.TbEtapa)
        Me.Guna2Panel5.Controls.Add(Me.TbYacimiento)
        Me.Guna2Panel5.Controls.Add(Me.Label13)
        Me.Guna2Panel5.Controls.Add(Me.Label14)
        Me.Guna2Panel5.Controls.Add(Me.Guna2CustomGradientPanel5)
        Me.Guna2Panel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.Location = New System.Drawing.Point(734, 44)
        Me.Guna2Panel5.Name = "Guna2Panel5"
        Me.Guna2Panel5.Size = New System.Drawing.Size(305, 348)
        Me.Guna2Panel5.TabIndex = 201
        '
        'TbCliente
        '
        Me.TbCliente.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbCliente.DefaultText = ""
        Me.TbCliente.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbCliente.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbCliente.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCliente.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCliente.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCliente.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbCliente.ForeColor = System.Drawing.Color.Black
        Me.TbCliente.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCliente.Location = New System.Drawing.Point(114, 297)
        Me.TbCliente.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbCliente.Name = "TbCliente"
        Me.TbCliente.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbCliente.PlaceholderText = ""
        Me.TbCliente.SelectedText = ""
        Me.TbCliente.Size = New System.Drawing.Size(175, 36)
        Me.TbCliente.TabIndex = 201
        Me.TbCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(41, 305)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(66, 21)
        Me.Label19.TabIndex = 200
        Me.Label19.Text = "Cliente:"
        '
        'TbProvincia
        '
        Me.TbProvincia.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbProvincia.DefaultText = ""
        Me.TbProvincia.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbProvincia.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbProvincia.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbProvincia.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbProvincia.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbProvincia.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbProvincia.ForeColor = System.Drawing.Color.Black
        Me.TbProvincia.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbProvincia.Location = New System.Drawing.Point(114, 253)
        Me.TbProvincia.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbProvincia.Name = "TbProvincia"
        Me.TbProvincia.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbProvincia.PlaceholderText = ""
        Me.TbProvincia.SelectedText = ""
        Me.TbProvincia.Size = New System.Drawing.Size(175, 36)
        Me.TbProvincia.TabIndex = 199
        Me.TbProvincia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(24, 261)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(82, 21)
        Me.Label16.TabIndex = 198
        Me.Label16.Text = "Provincia:"
        '
        'TbDistrito
        '
        Me.TbDistrito.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbDistrito.DefaultText = ""
        Me.TbDistrito.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbDistrito.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbDistrito.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbDistrito.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbDistrito.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbDistrito.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbDistrito.ForeColor = System.Drawing.Color.Black
        Me.TbDistrito.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbDistrito.Location = New System.Drawing.Point(114, 209)
        Me.TbDistrito.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbDistrito.Name = "TbDistrito"
        Me.TbDistrito.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbDistrito.PlaceholderText = ""
        Me.TbDistrito.SelectedText = ""
        Me.TbDistrito.Size = New System.Drawing.Size(175, 36)
        Me.TbDistrito.TabIndex = 197
        Me.TbDistrito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TbTipoPozo
        '
        Me.TbTipoPozo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbTipoPozo.DefaultText = ""
        Me.TbTipoPozo.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbTipoPozo.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbTipoPozo.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbTipoPozo.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbTipoPozo.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbTipoPozo.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbTipoPozo.ForeColor = System.Drawing.Color.Black
        Me.TbTipoPozo.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbTipoPozo.Location = New System.Drawing.Point(114, 165)
        Me.TbTipoPozo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbTipoPozo.Name = "TbTipoPozo"
        Me.TbTipoPozo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbTipoPozo.PlaceholderText = ""
        Me.TbTipoPozo.SelectedText = ""
        Me.TbTipoPozo.Size = New System.Drawing.Size(175, 36)
        Me.TbTipoPozo.TabIndex = 196
        Me.TbTipoPozo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(36, 217)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 21)
        Me.Label17.TabIndex = 195
        Me.Label17.Text = "Distrito:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(1, 173)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(108, 21)
        Me.Label18.TabIndex = 194
        Me.Label18.Text = "Tipo de pozo:"
        '
        'TbLocacion
        '
        Me.TbLocacion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbLocacion.DefaultText = ""
        Me.TbLocacion.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbLocacion.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbLocacion.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbLocacion.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbLocacion.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbLocacion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbLocacion.ForeColor = System.Drawing.Color.Black
        Me.TbLocacion.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbLocacion.Location = New System.Drawing.Point(114, 121)
        Me.TbLocacion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbLocacion.Name = "TbLocacion"
        Me.TbLocacion.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbLocacion.PlaceholderText = ""
        Me.TbLocacion.SelectedText = ""
        Me.TbLocacion.Size = New System.Drawing.Size(175, 36)
        Me.TbLocacion.TabIndex = 193
        Me.TbLocacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(27, 129)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 21)
        Me.Label8.TabIndex = 192
        Me.Label8.Text = "Locacion:"
        '
        'TbEtapa
        '
        Me.TbEtapa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbEtapa.DefaultText = ""
        Me.TbEtapa.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbEtapa.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbEtapa.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbEtapa.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbEtapa.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbEtapa.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbEtapa.ForeColor = System.Drawing.Color.Black
        Me.TbEtapa.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbEtapa.Location = New System.Drawing.Point(114, 77)
        Me.TbEtapa.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbEtapa.Name = "TbEtapa"
        Me.TbEtapa.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbEtapa.PlaceholderText = ""
        Me.TbEtapa.SelectedText = ""
        Me.TbEtapa.Size = New System.Drawing.Size(175, 36)
        Me.TbEtapa.TabIndex = 191
        Me.TbEtapa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TbYacimiento
        '
        Me.TbYacimiento.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbYacimiento.DefaultText = ""
        Me.TbYacimiento.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbYacimiento.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbYacimiento.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbYacimiento.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbYacimiento.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbYacimiento.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbYacimiento.ForeColor = System.Drawing.Color.Black
        Me.TbYacimiento.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbYacimiento.Location = New System.Drawing.Point(114, 33)
        Me.TbYacimiento.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbYacimiento.Name = "TbYacimiento"
        Me.TbYacimiento.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbYacimiento.PlaceholderText = ""
        Me.TbYacimiento.SelectedText = ""
        Me.TbYacimiento.Size = New System.Drawing.Size(175, 36)
        Me.TbYacimiento.TabIndex = 190
        Me.TbYacimiento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(50, 85)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(57, 21)
        Me.Label13.TabIndex = 189
        Me.Label13.Text = "Etapa:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(11, 41)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(97, 21)
        Me.Label14.TabIndex = 188
        Me.Label14.Text = "Yacimiento:"
        '
        'Guna2CustomGradientPanel5
        '
        Me.Guna2CustomGradientPanel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.BorderRadius = 6
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.Label15)
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel5.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel5.Name = "Guna2CustomGradientPanel5"
        Me.Guna2CustomGradientPanel5.Size = New System.Drawing.Size(305, 26)
        Me.Guna2CustomGradientPanel5.TabIndex = 19
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(86, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(137, 21)
        Me.Label15.TabIndex = 19
        Me.Label15.Text = "Datos de fractura"
        '
        'Guna2Panel6
        '
        Me.Guna2Panel6.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel6.BorderRadius = 6
        Me.Guna2Panel6.BorderThickness = 1
        Me.Guna2Panel6.Controls.Add(Me.Guna2CustomGradientPanel6)
        Me.Guna2Panel6.Controls.Add(Me.CbMultipleConnections)
        Me.Guna2Panel6.Controls.Add(Me.Label1)
        Me.Guna2Panel6.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel6.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel6.Location = New System.Drawing.Point(12, 398)
        Me.Guna2Panel6.Name = "Guna2Panel6"
        Me.Guna2Panel6.Size = New System.Drawing.Size(279, 65)
        Me.Guna2Panel6.TabIndex = 202
        '
        'Guna2CustomGradientPanel6
        '
        Me.Guna2CustomGradientPanel6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.BorderRadius = 6
        Me.Guna2CustomGradientPanel6.Controls.Add(Me.Label25)
        Me.Guna2CustomGradientPanel6.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel6.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel6.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel6.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel6.Name = "Guna2CustomGradientPanel6"
        Me.Guna2CustomGradientPanel6.Size = New System.Drawing.Size(279, 26)
        Me.Guna2CustomGradientPanel6.TabIndex = 19
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label25.Location = New System.Drawing.Point(114, 3)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(50, 21)
        Me.Label25.TabIndex = 19
        Me.Label25.Text = "Moxa"
        '
        'ConfigAvanzado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1050, 476)
        Me.Controls.Add(Me.Guna2Panel6)
        Me.Controls.Add(Me.Guna2Panel5)
        Me.Controls.Add(Me.Guna2Panel4)
        Me.Controls.Add(Me.Guna2Panel3)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.Guna2Panel2)
        Me.Controls.Add(Me.BtnDB)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "ConfigAvanzado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ConfigAvanzado"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.Guna2Panel2.ResumeLayout(False)
        Me.Guna2Panel2.PerformLayout()
        Me.Guna2CustomGradientPanel3.ResumeLayout(False)
        Me.Guna2CustomGradientPanel3.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.Guna2Panel3.ResumeLayout(False)
        Me.Guna2Panel3.PerformLayout()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        Me.Guna2Panel4.ResumeLayout(False)
        Me.Guna2Panel4.PerformLayout()
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.Guna2Panel5.ResumeLayout(False)
        Me.Guna2Panel5.PerformLayout()
        Me.Guna2CustomGradientPanel5.ResumeLayout(False)
        Me.Guna2CustomGradientPanel5.PerformLayout()
        Me.Guna2Panel6.ResumeLayout(False)
        Me.Guna2Panel6.PerformLayout()
        Me.Guna2CustomGradientPanel6.ResumeLayout(False)
        Me.Guna2CustomGradientPanel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Public WithEvents Title As Label
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents CbMultipleConnections As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label1 As Label
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnDB As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents FBD As FolderBrowserDialog
    Friend WithEvents Guna2Panel2 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel3 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblReportes As Label
    Public WithEvents BtnExcelGraphs As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents TbPDFOP As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TbCSVOP As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents TbCSVBU As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblDirectorios As Label
    Friend WithEvents CbExcel As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label9 As Label
    Friend WithEvents CbPDF As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label10 As Label
    Friend WithEvents CbCSVBackup As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Guna2Panel3 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblGraficador As Label
    Friend WithEvents Guna2Panel4 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomCheckBox1 As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Guna2TextBox3 As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblAuto As Label
    Friend WithEvents Guna2Panel5 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents TbCliente As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents TbProvincia As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents TbDistrito As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents TbTipoPozo As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents TbLocacion As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents TbEtapa As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents TbYacimiento As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Guna2CustomGradientPanel5 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Label15 As Label
    Friend WithEvents CbBackupFrac As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Label20 As Label
    Friend WithEvents CbConnect As Guna.UI2.WinForms.Guna2CheckBox
    Friend WithEvents TbID As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents Guna2Panel6 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel6 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Label25 As Label
    Friend WithEvents PortCB As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents PingCB As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents TbPORT As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents TbIP As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents CbTCP As Guna.UI2.WinForms.Guna2CheckBox
    Friend WithEvents CbProtocolo As Guna.UI2.WinForms.Guna2CheckBox
End Class
