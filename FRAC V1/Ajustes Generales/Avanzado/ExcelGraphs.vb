﻿Public Class ExcelGraphs

    Private Sub ExcelGraphs_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CbPresCaud.Checked = GeneratePresCaudGraph
        CbPresion.Checked = GeneratePresionGraph
        CbCaudal.Checked = GenerateCaudalGraph
        CbRPM.Checked = GenerateRPMGraph
        CbEngTrans.Checked = GenerateEngTransGraph
        CbPump.Checked = GeneratePumpGraph
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        GeneratePresCaudGraph = CbPresCaud.Checked
        GeneratePresionGraph = CbPresion.Checked
        GenerateCaudalGraph = CbCaudal.Checked
        GenerateRPMGraph = CbRPM.Checked
        GenerateEngTransGraph = CbEngTrans.Checked
        GeneratePumpGraph = CbPump.Checked

        My.Settings.GenerateRPMGraph = GenerateRPMGraph
        My.Settings.GenerateCaudalGraph = GenerateCaudalGraph
        My.Settings.GeneratePresionGraph = GeneratePresionGraph
        My.Settings.GeneratePresCaudGraph = GeneratePresCaudGraph
        My.Settings.GenerateEngTransGraph = GenerateEngTransGraph
        My.Settings.GeneratePumpGraph = GeneratePumpGraph
        My.Settings.Save()
        Close()
    End Sub

End Class