﻿Public Class ConfigAvanzado

    Private CbEnable As Boolean = True

    Private Sub ConfigAvanzado_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim TestTask As New Task(AddressOf TestIP)
        TestTask.Start()
        Dim TestTask1 As New Task(AddressOf TestPort)
        TestTask1.Start()

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CbMultipleConnections.Checked = AllowMultipleConnections
        CbCSVBackup.Checked = GenerateCSVBackUp
        CbExcel.Checked = GenerateExcel
        CbPDF.Checked = GeneratePDF

        TbCSVBU.Text = CSVBackUpPath
        TbCSVOP.Text = CSVOperationPath
        TbPDFOP.Text = PDFPath

        CbBackupFrac.Checked = UseBackupFrac

        TbYacimiento.Text = Yacimiento
        TbEtapa.Text = Etapa
        TbLocacion.Text = Locacion
        TbTipoPozo.Text = TipoPozo
        TbDistrito.Text = Distrito
        TbProvincia.Text = Provincia
        TbCliente.Text = Cliente

        Select Case My.Settings.Protocol
            Case "PC"
                CbProtocolo.Checked = True
            Case "TCP"
                CbTCP.Checked = True
        End Select

        TbIP.Text = My.Settings.Host
        TbPORT.Text = My.Settings.Port
        TbID.Text = My.Settings.ID

        CbConnect.Checked = IsConnected

        CenterEverything()

    End Sub

    Private Sub CenterEverything()
        CenterObject(Title)
        CenterObject(LblDirectorios)
        CenterObject(LblReportes)
        CenterObject(LblGraficador)
        CenterObject(LblAuto)

        CenterToScreen()
    End Sub

    Private Sub PortCB_Click(sender As Object, e As EventArgs) Handles CbConnect.CheckedChanged
        If Not PortCB.Checked And Not IsConnected Then
            sender.CheckState = CheckState.Unchecked
        End If
    End Sub

    Private Sub TbIP_Leave(sender As Object, e As EventArgs) Handles TbIP.Leave
        Dim TestTask As New Task(AddressOf TestIP)
        TestTask.Start()
    End Sub

    Private Sub TestIP()
        Try
            PingCB.Checked = My.Computer.Network.Ping(TbIP.Text)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub TestPort()
        Try
            PortCB.Checked = TryPort(TbIP.Text, TbPORT.Text)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TbPORT_TextChanged(sender As Object, e As EventArgs) Handles TbPORT.Leave
        Dim TestTask As New Task(AddressOf TestPort)
        TestTask.Start()
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click

        Yacimiento = TbYacimiento.Text
        Etapa = GetInteger(TbEtapa.Text)
        Locacion = TbLocacion.Text
        TipoPozo = TbTipoPozo.Text
        Distrito = TbDistrito.Text
        Provincia = TbProvincia.Text
        Cliente = TbCliente.Text

        AllowMultipleConnections = CbMultipleConnections.Checked
        GenerateCSVBackUp = CbCSVBackup.Checked
        GenerateExcel = CbExcel.Checked
        GeneratePDF = CbPDF.Checked

        UseBackupFrac = CbBackupFrac.Checked

        My.Settings.AllowMultipleConnections = AllowMultipleConnections
        My.Settings.GenerateCSVBackup = GenerateCSVBackUp
        My.Settings.GenerateExcel = GenerateExcel
        My.Settings.GeneratePDF = GeneratePDF

        My.Settings.Yacimiento = Yacimiento
        My.Settings.Etapa = Etapa
        My.Settings.Locacion = Locacion
        My.Settings.TipoPozo = TipoPozo
        My.Settings.Distrito = Distrito
        My.Settings.Provincia = Provincia
        My.Settings.Cliente = Cliente

        My.Settings.UseBackupFrac = UseBackupFrac

        My.Settings.Host = TbIP.Text
        My.Settings.Port = GetInteger(TbPORT.Text)
        My.Settings.ID = GetInteger(TbID.Text)
        If CbProtocolo.Checked Then
            My.Settings.Protocol = "PC"
        ElseIf CbTCP.Checked Then
            My.Settings.Protocol = "TCP"
        Else
            My.Settings.Protocol = "None"
            SGraphConnect.Protocol = "None"
        End If

        If CbConnect.Checked Then
            Dim TestTask As New Task(AddressOf TryConnect)
            TestTask.Start()
        Else
            GraphConnect.TCPDisconnect()
            IsConnected = False
        End If

        My.Settings.Save()

        My.Settings.Save()

        Close()
    End Sub

    Private Sub TryConnect()
        If PortCB.Checked And Not IsConnected Then
            SGraphConnect.Protocol = "TCP"
            SGraphConnect.ID = My.Settings.ID

            If GraphConnect.TCPConnect(My.Settings.Host, My.Settings.Port) Then

                IsConnected = True
            Else
                IsConnected = False
                avisoStr = "Eror en la conexión."
                avisoTitulo = "Error"
                AvisoGeneral.ShowDialog(Me)
            End If
        End If
    End Sub


    Private Sub BtnExcelGraphs_Click(sender As Object, e As EventArgs) Handles BtnExcelGraphs.Click
        ExcelGraphs.ShowDialog(Me)
    End Sub

    Private Sub BtnDBClick(sender As Object, e As EventArgs) Handles BtnDB.Click
        ConfigFrac.Show(Me)
    End Sub

    Private Sub TbCSVBU_MouseClick(sender As Object, e As EventArgs) Handles TbCSVBU.MouseClick
        FBD.SelectedPath = CSVBackUpPath
        If FBD.ShowDialog(Me) = DialogResult.OK Then
            My.Settings.CSVBackpPath = FBD.SelectedPath
            CSVBackUpPath = FBD.SelectedPath
            sender.Text = FBD.SelectedPath
        End If
    End Sub

    Private Sub TbCSVOP_MouseClick(sender As Object, e As MouseEventArgs) Handles TbCSVOP.MouseClick
        FBD.SelectedPath = CSVOperationPath
        If FBD.ShowDialog(Me) = DialogResult.OK Then
            My.Settings.CSVOperationPath = FBD.SelectedPath
            CSVOperationPath = FBD.SelectedPath
            sender.Text = FBD.SelectedPath
        End If
    End Sub

    Private Sub TbPDFOP_MouseClick(sender As Object, e As MouseEventArgs) Handles TbPDFOP.MouseClick
        FBD.SelectedPath = PDFPath
        If FBD.ShowDialog(Me) = DialogResult.OK Then
            My.Settings.PDFPath = FBD.SelectedPath
            PDFPath = FBD.SelectedPath
            sender.Text = FBD.SelectedPath
        End If
    End Sub

    Private Sub CbProtocolo_CheckedChanged(sender As Object, e As EventArgs) Handles CbProtocolo.CheckedChanged
        If CbEnable Then
            CbEnable = False
            CbTCP.CheckState = CheckState.Unchecked
            SGraphConnect.Protocol = "PC"
            CbEnable = True
        End If
    End Sub

    Private Sub CbTCP_CheckedChanged(sender As Object, e As EventArgs) Handles CbTCP.CheckedChanged
        If CbEnable Then
            CbEnable = False
            CbProtocolo.CheckState = CheckState.Unchecked
            SGraphConnect.Protocol = "TCP"
            CbEnable = True
        End If

        If Not sender.Checked Then
            GraphConnect.TCPDisconnect()
            IsConnected = False
        End If
    End Sub
End Class