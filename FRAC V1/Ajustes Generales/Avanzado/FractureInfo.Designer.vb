﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FractureInfo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.CbMultipleConnections = New Guna.UI2.WinForms.Guna2CustomCheckBox()
        Me.TbCliente = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TbProvincia = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TbDistrito = New Guna.UI2.WinForms.Guna2TextBox()
        Me.TbTipoPozo = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TbLocacion = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TbEtapa = New Guna.UI2.WinForms.Guna2TextBox()
        Me.TbYacimiento = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TitlePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(101, 358)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(109, 40)
        Me.BtnAceptar.TabIndex = 205
        Me.BtnAceptar.Text = "Aceptar"
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(262, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(310, 38)
        Me.TitlePanel.TabIndex = 202
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(81, 3)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(148, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Información"
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'CbMultipleConnections
        '
        Me.CbMultipleConnections.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbMultipleConnections.CheckedState.BorderRadius = 2
        Me.CbMultipleConnections.CheckedState.BorderThickness = 0
        Me.CbMultipleConnections.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbMultipleConnections.Location = New System.Drawing.Point(-113, 419)
        Me.CbMultipleConnections.Name = "CbMultipleConnections"
        Me.CbMultipleConnections.Size = New System.Drawing.Size(20, 20)
        Me.CbMultipleConnections.TabIndex = 204
        Me.CbMultipleConnections.Text = "Guna2CustomCheckBox1"
        Me.CbMultipleConnections.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbMultipleConnections.UncheckedState.BorderRadius = 2
        Me.CbMultipleConnections.UncheckedState.BorderThickness = 0
        Me.CbMultipleConnections.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'TbCliente
        '
        Me.TbCliente.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbCliente.DefaultText = ""
        Me.TbCliente.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbCliente.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbCliente.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCliente.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbCliente.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCliente.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbCliente.ForeColor = System.Drawing.Color.Black
        Me.TbCliente.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbCliente.Location = New System.Drawing.Point(119, 309)
        Me.TbCliente.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbCliente.Name = "TbCliente"
        Me.TbCliente.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbCliente.PlaceholderText = ""
        Me.TbCliente.SelectedText = ""
        Me.TbCliente.Size = New System.Drawing.Size(175, 36)
        Me.TbCliente.TabIndex = 225
        Me.TbCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(49, 317)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(66, 21)
        Me.Label19.TabIndex = 224
        Me.Label19.Text = "Cliente:"
        '
        'TbProvincia
        '
        Me.TbProvincia.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbProvincia.DefaultText = ""
        Me.TbProvincia.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbProvincia.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbProvincia.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbProvincia.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbProvincia.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbProvincia.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbProvincia.ForeColor = System.Drawing.Color.Black
        Me.TbProvincia.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbProvincia.Location = New System.Drawing.Point(119, 265)
        Me.TbProvincia.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbProvincia.Name = "TbProvincia"
        Me.TbProvincia.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbProvincia.PlaceholderText = ""
        Me.TbProvincia.SelectedText = ""
        Me.TbProvincia.Size = New System.Drawing.Size(175, 36)
        Me.TbProvincia.TabIndex = 223
        Me.TbProvincia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(32, 273)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(82, 21)
        Me.Label16.TabIndex = 222
        Me.Label16.Text = "Provincia:"
        '
        'TbDistrito
        '
        Me.TbDistrito.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbDistrito.DefaultText = ""
        Me.TbDistrito.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbDistrito.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbDistrito.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbDistrito.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbDistrito.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbDistrito.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbDistrito.ForeColor = System.Drawing.Color.Black
        Me.TbDistrito.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbDistrito.Location = New System.Drawing.Point(119, 221)
        Me.TbDistrito.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbDistrito.Name = "TbDistrito"
        Me.TbDistrito.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbDistrito.PlaceholderText = ""
        Me.TbDistrito.SelectedText = ""
        Me.TbDistrito.Size = New System.Drawing.Size(175, 36)
        Me.TbDistrito.TabIndex = 221
        Me.TbDistrito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TbTipoPozo
        '
        Me.TbTipoPozo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbTipoPozo.DefaultText = ""
        Me.TbTipoPozo.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbTipoPozo.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbTipoPozo.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbTipoPozo.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbTipoPozo.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbTipoPozo.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbTipoPozo.ForeColor = System.Drawing.Color.Black
        Me.TbTipoPozo.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbTipoPozo.Location = New System.Drawing.Point(119, 177)
        Me.TbTipoPozo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbTipoPozo.Name = "TbTipoPozo"
        Me.TbTipoPozo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbTipoPozo.PlaceholderText = ""
        Me.TbTipoPozo.SelectedText = ""
        Me.TbTipoPozo.Size = New System.Drawing.Size(175, 36)
        Me.TbTipoPozo.TabIndex = 220
        Me.TbTipoPozo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(44, 229)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 21)
        Me.Label17.TabIndex = 219
        Me.Label17.Text = "Distrito:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(9, 185)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(108, 21)
        Me.Label18.TabIndex = 218
        Me.Label18.Text = "Tipo de pozo:"
        '
        'TbLocacion
        '
        Me.TbLocacion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbLocacion.DefaultText = ""
        Me.TbLocacion.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbLocacion.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbLocacion.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbLocacion.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbLocacion.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbLocacion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbLocacion.ForeColor = System.Drawing.Color.Black
        Me.TbLocacion.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbLocacion.Location = New System.Drawing.Point(119, 133)
        Me.TbLocacion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbLocacion.Name = "TbLocacion"
        Me.TbLocacion.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbLocacion.PlaceholderText = ""
        Me.TbLocacion.SelectedText = ""
        Me.TbLocacion.Size = New System.Drawing.Size(175, 36)
        Me.TbLocacion.TabIndex = 217
        Me.TbLocacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(35, 141)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 21)
        Me.Label8.TabIndex = 216
        Me.Label8.Text = "Locacion:"
        '
        'TbEtapa
        '
        Me.TbEtapa.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbEtapa.DefaultText = ""
        Me.TbEtapa.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbEtapa.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbEtapa.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbEtapa.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbEtapa.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbEtapa.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbEtapa.ForeColor = System.Drawing.Color.Black
        Me.TbEtapa.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbEtapa.Location = New System.Drawing.Point(119, 89)
        Me.TbEtapa.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbEtapa.Name = "TbEtapa"
        Me.TbEtapa.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbEtapa.PlaceholderText = ""
        Me.TbEtapa.SelectedText = ""
        Me.TbEtapa.Size = New System.Drawing.Size(175, 36)
        Me.TbEtapa.TabIndex = 215
        Me.TbEtapa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TbYacimiento
        '
        Me.TbYacimiento.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbYacimiento.DefaultText = ""
        Me.TbYacimiento.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbYacimiento.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbYacimiento.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbYacimiento.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbYacimiento.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbYacimiento.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.TbYacimiento.ForeColor = System.Drawing.Color.Black
        Me.TbYacimiento.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbYacimiento.Location = New System.Drawing.Point(119, 45)
        Me.TbYacimiento.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.TbYacimiento.Name = "TbYacimiento"
        Me.TbYacimiento.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbYacimiento.PlaceholderText = ""
        Me.TbYacimiento.SelectedText = ""
        Me.TbYacimiento.Size = New System.Drawing.Size(175, 36)
        Me.TbYacimiento.TabIndex = 214
        Me.TbYacimiento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(58, 97)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(57, 21)
        Me.Label13.TabIndex = 213
        Me.Label13.Text = "Etapa:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(19, 53)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(97, 21)
        Me.Label14.TabIndex = 212
        Me.Label14.Text = "Yacimiento:"
        '
        'FractureInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(310, 411)
        Me.Controls.Add(Me.TbCliente)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.TbProvincia)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.TbDistrito)
        Me.Controls.Add(Me.TbTipoPozo)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.TbLocacion)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.TbEtapa)
        Me.Controls.Add(Me.TbYacimiento)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.CbMultipleConnections)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FractureInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FractureInfo"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents CbMultipleConnections As Guna.UI2.WinForms.Guna2CustomCheckBox
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents TbCliente As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents TbProvincia As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents TbDistrito As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents TbTipoPozo As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents TbLocacion As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents TbEtapa As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents TbYacimiento As Guna.UI2.WinForms.Guna2TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
End Class
