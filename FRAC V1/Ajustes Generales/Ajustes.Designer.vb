﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Ajustes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.LblFracturador = New System.Windows.Forms.Label()
        Me.CbFrac = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblMotor = New System.Windows.Forms.Label()
        Me.LblTransmision = New System.Windows.Forms.Label()
        Me.LblBomba = New System.Windows.Forms.Label()
        Me.LblIdioma = New System.Windows.Forms.Label()
        Me.CbIdioma = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblResolucion = New System.Windows.Forms.Label()
        Me.CbResolucion = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblPump = New System.Windows.Forms.Label()
        Me.LblTransmission = New System.Windows.Forms.Label()
        Me.LblEngine = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblFracturadores = New System.Windows.Forms.Label()
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.CbTotalScreens = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblPantallaSimple = New System.Windows.Forms.Label()
        Me.BtnWITS = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnComSerie = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblGeneral = New System.Windows.Forms.Label()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAvanzado = New Guna.UI2.WinForms.Guna2Button()
        Me.Panel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2Separator1 = New Guna.UI2.WinForms.Guna2Separator()
        Me.BtnDelData = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAddData = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnStartGraph = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel3 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DataPanel = New System.Windows.Forms.Panel()
        Me.CbDato1 = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblDato1 = New System.Windows.Forms.Label()
        Me.TmrRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.TitlePanel.SuspendLayout()
        Me.Guna2Panel2.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        Me.Panel.SuspendLayout()
        Me.Guna2CustomGradientPanel3.SuspendLayout()
        Me.DataPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'LblFracturador
        '
        Me.LblFracturador.AutoSize = True
        Me.LblFracturador.BackColor = System.Drawing.Color.Transparent
        Me.LblFracturador.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblFracturador.ForeColor = System.Drawing.Color.White
        Me.LblFracturador.Location = New System.Drawing.Point(16, 48)
        Me.LblFracturador.Name = "LblFracturador"
        Me.LblFracturador.Size = New System.Drawing.Size(104, 22)
        Me.LblFracturador.TabIndex = 56
        Me.LblFracturador.Text = "Fracturador:"
        '
        'CbFrac
        '
        Me.CbFrac.BackColor = System.Drawing.Color.Transparent
        Me.CbFrac.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbFrac.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbFrac.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbFrac.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbFrac.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbFrac.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbFrac.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbFrac.ItemHeight = 30
        Me.CbFrac.Location = New System.Drawing.Point(129, 41)
        Me.CbFrac.Name = "CbFrac"
        Me.CbFrac.Size = New System.Drawing.Size(175, 36)
        Me.CbFrac.TabIndex = 55
        Me.CbFrac.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblMotor
        '
        Me.LblMotor.AutoSize = True
        Me.LblMotor.BackColor = System.Drawing.Color.Transparent
        Me.LblMotor.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblMotor.ForeColor = System.Drawing.Color.White
        Me.LblMotor.Location = New System.Drawing.Point(76, 137)
        Me.LblMotor.Name = "LblMotor"
        Me.LblMotor.Size = New System.Drawing.Size(60, 22)
        Me.LblMotor.TabIndex = 58
        Me.LblMotor.Text = "Motor:"
        '
        'LblTransmision
        '
        Me.LblTransmision.AutoSize = True
        Me.LblTransmision.BackColor = System.Drawing.Color.Transparent
        Me.LblTransmision.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTransmision.ForeColor = System.Drawing.Color.White
        Me.LblTransmision.Location = New System.Drawing.Point(27, 226)
        Me.LblTransmision.Name = "LblTransmision"
        Me.LblTransmision.Size = New System.Drawing.Size(109, 22)
        Me.LblTransmision.TabIndex = 60
        Me.LblTransmision.Text = "Transmisión:"
        '
        'LblBomba
        '
        Me.LblBomba.AutoSize = True
        Me.LblBomba.BackColor = System.Drawing.Color.Transparent
        Me.LblBomba.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblBomba.ForeColor = System.Drawing.Color.White
        Me.LblBomba.Location = New System.Drawing.Point(65, 315)
        Me.LblBomba.Name = "LblBomba"
        Me.LblBomba.Size = New System.Drawing.Size(72, 22)
        Me.LblBomba.TabIndex = 62
        Me.LblBomba.Text = "Bomba:"
        '
        'LblIdioma
        '
        Me.LblIdioma.AutoSize = True
        Me.LblIdioma.BackColor = System.Drawing.Color.Transparent
        Me.LblIdioma.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblIdioma.ForeColor = System.Drawing.Color.White
        Me.LblIdioma.Location = New System.Drawing.Point(54, 220)
        Me.LblIdioma.Name = "LblIdioma"
        Me.LblIdioma.Size = New System.Drawing.Size(69, 22)
        Me.LblIdioma.TabIndex = 65
        Me.LblIdioma.Text = "Idioma:"
        '
        'CbIdioma
        '
        Me.CbIdioma.BackColor = System.Drawing.Color.Transparent
        Me.CbIdioma.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbIdioma.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbIdioma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbIdioma.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbIdioma.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbIdioma.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbIdioma.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbIdioma.ItemHeight = 30
        Me.CbIdioma.Items.AddRange(New Object() {"Español", "Ingles"})
        Me.CbIdioma.Location = New System.Drawing.Point(129, 213)
        Me.CbIdioma.Name = "CbIdioma"
        Me.CbIdioma.Size = New System.Drawing.Size(175, 36)
        Me.CbIdioma.TabIndex = 64
        Me.CbIdioma.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblResolucion
        '
        Me.LblResolucion.AutoSize = True
        Me.LblResolucion.BackColor = System.Drawing.Color.Transparent
        Me.LblResolucion.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblResolucion.ForeColor = System.Drawing.Color.White
        Me.LblResolucion.Location = New System.Drawing.Point(21, 134)
        Me.LblResolucion.Name = "LblResolucion"
        Me.LblResolucion.Size = New System.Drawing.Size(102, 22)
        Me.LblResolucion.TabIndex = 67
        Me.LblResolucion.Text = "Resolución:"
        '
        'CbResolucion
        '
        Me.CbResolucion.BackColor = System.Drawing.Color.Transparent
        Me.CbResolucion.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbResolucion.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbResolucion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbResolucion.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbResolucion.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbResolucion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbResolucion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbResolucion.ItemHeight = 30
        Me.CbResolucion.Items.AddRange(New Object() {"1280x840", "1920x1080"})
        Me.CbResolucion.Location = New System.Drawing.Point(129, 127)
        Me.CbResolucion.Name = "CbResolucion"
        Me.CbResolucion.Size = New System.Drawing.Size(175, 36)
        Me.CbResolucion.TabIndex = 66
        Me.CbResolucion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.LblPump)
        Me.Guna2Panel1.Controls.Add(Me.LblTransmission)
        Me.Guna2Panel1.Controls.Add(Me.LblEngine)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.Controls.Add(Me.CbFrac)
        Me.Guna2Panel1.Controls.Add(Me.LblFracturador)
        Me.Guna2Panel1.Controls.Add(Me.LblMotor)
        Me.Guna2Panel1.Controls.Add(Me.LblBomba)
        Me.Guna2Panel1.Controls.Add(Me.LblTransmision)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.CustomBorderThickness = New System.Windows.Forms.Padding(1, 0, 1, 1)
        Me.Guna2Panel1.CustomizableEdges.BottomLeft = False
        Me.Guna2Panel1.CustomizableEdges.BottomRight = False
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(362, 44)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(344, 380)
        Me.Guna2Panel1.TabIndex = 69
        '
        'LblPump
        '
        Me.LblPump.AutoSize = True
        Me.LblPump.BackColor = System.Drawing.Color.Transparent
        Me.LblPump.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblPump.ForeColor = System.Drawing.Color.White
        Me.LblPump.Location = New System.Drawing.Point(143, 315)
        Me.LblPump.Name = "LblPump"
        Me.LblPump.Size = New System.Drawing.Size(16, 22)
        Me.LblPump.TabIndex = 71
        Me.LblPump.Text = "-"
        '
        'LblTransmission
        '
        Me.LblTransmission.AutoSize = True
        Me.LblTransmission.BackColor = System.Drawing.Color.Transparent
        Me.LblTransmission.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTransmission.ForeColor = System.Drawing.Color.White
        Me.LblTransmission.Location = New System.Drawing.Point(143, 226)
        Me.LblTransmission.Name = "LblTransmission"
        Me.LblTransmission.Size = New System.Drawing.Size(16, 22)
        Me.LblTransmission.TabIndex = 70
        Me.LblTransmission.Text = "-"
        '
        'LblEngine
        '
        Me.LblEngine.AutoSize = True
        Me.LblEngine.BackColor = System.Drawing.Color.Transparent
        Me.LblEngine.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblEngine.ForeColor = System.Drawing.Color.White
        Me.LblEngine.Location = New System.Drawing.Point(143, 137)
        Me.LblEngine.Name = "LblEngine"
        Me.LblEngine.Size = New System.Drawing.Size(16, 22)
        Me.LblEngine.TabIndex = 69
        Me.LblEngine.Text = "-"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblFracturadores)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(344, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblFracturadores
        '
        Me.LblFracturadores.AutoSize = True
        Me.LblFracturadores.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblFracturadores.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblFracturadores.Location = New System.Drawing.Point(116, 3)
        Me.LblFracturadores.Name = "LblFracturadores"
        Me.LblFracturadores.Size = New System.Drawing.Size(112, 21)
        Me.LblFracturadores.TabIndex = 19
        Me.LblFracturadores.Text = "Fracturadores"
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(1069, 38)
        Me.TitlePanel.TabIndex = 70
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(487, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(94, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Ajustes"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(1021, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2Panel2
        '
        Me.Guna2Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.BorderRadius = 6
        Me.Guna2Panel2.BorderThickness = 1
        Me.Guna2Panel2.Controls.Add(Me.CbTotalScreens)
        Me.Guna2Panel2.Controls.Add(Me.LblPantallaSimple)
        Me.Guna2Panel2.Controls.Add(Me.BtnWITS)
        Me.Guna2Panel2.Controls.Add(Me.BtnComSerie)
        Me.Guna2Panel2.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.Guna2Panel2.Controls.Add(Me.LblResolucion)
        Me.Guna2Panel2.Controls.Add(Me.CbResolucion)
        Me.Guna2Panel2.Controls.Add(Me.CbIdioma)
        Me.Guna2Panel2.Controls.Add(Me.LblIdioma)
        Me.Guna2Panel2.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.CustomBorderThickness = New System.Windows.Forms.Padding(1, 0, 1, 1)
        Me.Guna2Panel2.CustomizableEdges.BottomLeft = False
        Me.Guna2Panel2.CustomizableEdges.BottomRight = False
        Me.Guna2Panel2.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.Location = New System.Drawing.Point(12, 44)
        Me.Guna2Panel2.Name = "Guna2Panel2"
        Me.Guna2Panel2.Size = New System.Drawing.Size(344, 380)
        Me.Guna2Panel2.TabIndex = 70
        '
        'CbTotalScreens
        '
        Me.CbTotalScreens.BackColor = System.Drawing.Color.Transparent
        Me.CbTotalScreens.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbTotalScreens.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbTotalScreens.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbTotalScreens.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbTotalScreens.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbTotalScreens.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbTotalScreens.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbTotalScreens.ItemHeight = 30
        Me.CbTotalScreens.Items.AddRange(New Object() {"1", "2", "3"})
        Me.CbTotalScreens.Location = New System.Drawing.Point(129, 41)
        Me.CbTotalScreens.Name = "CbTotalScreens"
        Me.CbTotalScreens.Size = New System.Drawing.Size(175, 36)
        Me.CbTotalScreens.TabIndex = 162
        Me.CbTotalScreens.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblPantallaSimple
        '
        Me.LblPantallaSimple.AutoSize = True
        Me.LblPantallaSimple.BackColor = System.Drawing.Color.Transparent
        Me.LblPantallaSimple.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblPantallaSimple.ForeColor = System.Drawing.Color.White
        Me.LblPantallaSimple.Location = New System.Drawing.Point(38, 48)
        Me.LblPantallaSimple.Name = "LblPantallaSimple"
        Me.LblPantallaSimple.Size = New System.Drawing.Size(84, 22)
        Me.LblPantallaSimple.TabIndex = 161
        Me.LblPantallaSimple.Text = "Pantallas:"
        '
        'BtnWITS
        '
        Me.BtnWITS.Animated = True
        Me.BtnWITS.BorderColor = System.Drawing.Color.White
        Me.BtnWITS.BorderRadius = 5
        Me.BtnWITS.BorderThickness = 1
        Me.BtnWITS.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnWITS.FillColor = System.Drawing.Color.Transparent
        Me.BtnWITS.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnWITS.ForeColor = System.Drawing.Color.White
        Me.BtnWITS.Location = New System.Drawing.Point(55, 326)
        Me.BtnWITS.Name = "BtnWITS"
        Me.BtnWITS.Size = New System.Drawing.Size(238, 40)
        Me.BtnWITS.TabIndex = 69
        Me.BtnWITS.Text = "WITS"
        '
        'BtnComSerie
        '
        Me.BtnComSerie.Animated = True
        Me.BtnComSerie.BorderColor = System.Drawing.Color.White
        Me.BtnComSerie.BorderRadius = 5
        Me.BtnComSerie.BorderThickness = 1
        Me.BtnComSerie.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnComSerie.FillColor = System.Drawing.Color.Transparent
        Me.BtnComSerie.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnComSerie.ForeColor = System.Drawing.Color.White
        Me.BtnComSerie.Location = New System.Drawing.Point(55, 280)
        Me.BtnComSerie.Name = "BtnComSerie"
        Me.BtnComSerie.Size = New System.Drawing.Size(238, 40)
        Me.BtnComSerie.TabIndex = 68
        Me.BtnComSerie.Text = "Comunicación Serie"
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblGeneral)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(344, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblGeneral
        '
        Me.LblGeneral.AutoSize = True
        Me.LblGeneral.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblGeneral.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblGeneral.Location = New System.Drawing.Point(84, 3)
        Me.LblGeneral.Name = "LblGeneral"
        Me.LblGeneral.Size = New System.Drawing.Size(176, 21)
        Me.LblGeneral.TabIndex = 19
        Me.LblGeneral.Text = "Configuración General"
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 14.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(447, 431)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(175, 40)
        Me.BtnAceptar.TabIndex = 70
        Me.BtnAceptar.Text = "Aceptar"
        '
        'BtnAvanzado
        '
        Me.BtnAvanzado.Animated = True
        Me.BtnAvanzado.BorderColor = System.Drawing.Color.White
        Me.BtnAvanzado.BorderRadius = 5
        Me.BtnAvanzado.BorderThickness = 1
        Me.BtnAvanzado.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAvanzado.FillColor = System.Drawing.Color.Transparent
        Me.BtnAvanzado.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnAvanzado.ForeColor = System.Drawing.Color.White
        Me.BtnAvanzado.Location = New System.Drawing.Point(947, 431)
        Me.BtnAvanzado.Name = "BtnAvanzado"
        Me.BtnAvanzado.Size = New System.Drawing.Size(109, 40)
        Me.BtnAvanzado.TabIndex = 72
        Me.BtnAvanzado.Text = "Avanzado"
        '
        'Panel
        '
        Me.Panel.BackColor = System.Drawing.Color.Transparent
        Me.Panel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Panel.BorderRadius = 6
        Me.Panel.BorderThickness = 1
        Me.Panel.Controls.Add(Me.Guna2Separator1)
        Me.Panel.Controls.Add(Me.BtnDelData)
        Me.Panel.Controls.Add(Me.BtnAddData)
        Me.Panel.Controls.Add(Me.BtnStartGraph)
        Me.Panel.Controls.Add(Me.Guna2CustomGradientPanel3)
        Me.Panel.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Panel.CustomBorderThickness = New System.Windows.Forms.Padding(1, 0, 1, 1)
        Me.Panel.CustomizableEdges.BottomLeft = False
        Me.Panel.CustomizableEdges.BottomRight = False
        Me.Panel.FillColor = System.Drawing.Color.Transparent
        Me.Panel.Location = New System.Drawing.Point(712, 44)
        Me.Panel.Name = "Panel"
        Me.Panel.Size = New System.Drawing.Size(344, 380)
        Me.Panel.TabIndex = 73
        '
        'Guna2Separator1
        '
        Me.Guna2Separator1.Location = New System.Drawing.Point(7, 87)
        Me.Guna2Separator1.Name = "Guna2Separator1"
        Me.Guna2Separator1.Size = New System.Drawing.Size(330, 1)
        Me.Guna2Separator1.TabIndex = 86
        '
        'BtnDelData
        '
        Me.BtnDelData.Animated = True
        Me.BtnDelData.BorderColor = System.Drawing.Color.White
        Me.BtnDelData.BorderRadius = 5
        Me.BtnDelData.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDelData.FillColor = System.Drawing.Color.Transparent
        Me.BtnDelData.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnDelData.ForeColor = System.Drawing.Color.White
        Me.BtnDelData.Image = Global.sFRAC.My.Resources.Resources.minus
        Me.BtnDelData.ImageSize = New System.Drawing.Size(45, 45)
        Me.BtnDelData.Location = New System.Drawing.Point(147, 32)
        Me.BtnDelData.Name = "BtnDelData"
        Me.BtnDelData.Size = New System.Drawing.Size(50, 50)
        Me.BtnDelData.TabIndex = 83
        '
        'BtnAddData
        '
        Me.BtnAddData.Animated = True
        Me.BtnAddData.BorderColor = System.Drawing.Color.White
        Me.BtnAddData.BorderRadius = 5
        Me.BtnAddData.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAddData.FillColor = System.Drawing.Color.Transparent
        Me.BtnAddData.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnAddData.ForeColor = System.Drawing.Color.White
        Me.BtnAddData.Image = Global.sFRAC.My.Resources.Resources.add1
        Me.BtnAddData.ImageSize = New System.Drawing.Size(45, 45)
        Me.BtnAddData.Location = New System.Drawing.Point(91, 32)
        Me.BtnAddData.Name = "BtnAddData"
        Me.BtnAddData.Size = New System.Drawing.Size(50, 50)
        Me.BtnAddData.TabIndex = 82
        '
        'BtnStartGraph
        '
        Me.BtnStartGraph.Animated = True
        Me.BtnStartGraph.BorderColor = System.Drawing.Color.White
        Me.BtnStartGraph.BorderRadius = 5
        Me.BtnStartGraph.CheckedState.Image = Global.sFRAC.My.Resources.Resources.pause
        Me.BtnStartGraph.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStartGraph.FillColor = System.Drawing.Color.Transparent
        Me.BtnStartGraph.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnStartGraph.ForeColor = System.Drawing.Color.White
        Me.BtnStartGraph.Image = Global.sFRAC.My.Resources.Resources.play
        Me.BtnStartGraph.ImageSize = New System.Drawing.Size(45, 45)
        Me.BtnStartGraph.Location = New System.Drawing.Point(203, 32)
        Me.BtnStartGraph.Name = "BtnStartGraph"
        Me.BtnStartGraph.Size = New System.Drawing.Size(50, 50)
        Me.BtnStartGraph.TabIndex = 81
        '
        'Guna2CustomGradientPanel3
        '
        Me.Guna2CustomGradientPanel3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.BorderRadius = 6
        Me.Guna2CustomGradientPanel3.Controls.Add(Me.Label4)
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel3.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel3.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel3.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel3.Name = "Guna2CustomGradientPanel3"
        Me.Guna2CustomGradientPanel3.Size = New System.Drawing.Size(344, 26)
        Me.Guna2CustomGradientPanel3.TabIndex = 19
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(129, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 21)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Graficador"
        '
        'DataPanel
        '
        Me.DataPanel.AutoScroll = True
        Me.DataPanel.AutoScrollMargin = New System.Drawing.Size(0, 20)
        Me.DataPanel.Controls.Add(Me.CbDato1)
        Me.DataPanel.Controls.Add(Me.LblDato1)
        Me.DataPanel.Location = New System.Drawing.Point(713, 132)
        Me.DataPanel.Name = "DataPanel"
        Me.DataPanel.Size = New System.Drawing.Size(342, 289)
        Me.DataPanel.TabIndex = 79
        '
        'CbDato1
        '
        Me.CbDato1.BackColor = System.Drawing.Color.Transparent
        Me.CbDato1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbDato1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbDato1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbDato1.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDato1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDato1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.CbDato1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbDato1.ItemHeight = 30
        Me.CbDato1.Location = New System.Drawing.Point(76, 14)
        Me.CbDato1.Name = "CbDato1"
        Me.CbDato1.Size = New System.Drawing.Size(237, 36)
        Me.CbDato1.TabIndex = 82
        Me.CbDato1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblDato1
        '
        Me.LblDato1.AutoSize = True
        Me.LblDato1.BackColor = System.Drawing.Color.Transparent
        Me.LblDato1.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblDato1.ForeColor = System.Drawing.Color.White
        Me.LblDato1.Location = New System.Drawing.Point(8, 21)
        Me.LblDato1.Name = "LblDato1"
        Me.LblDato1.Size = New System.Drawing.Size(61, 22)
        Me.LblDato1.TabIndex = 81
        Me.LblDato1.Text = "Dato 1:"
        '
        'TmrRefresh
        '
        Me.TmrRefresh.Interval = 2
        '
        'Ajustes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1069, 483)
        Me.Controls.Add(Me.BtnAvanzado)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.Guna2Panel2)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.DataPanel)
        Me.Controls.Add(Me.Panel)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Ajustes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ajustes"
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.Guna2Panel2.ResumeLayout(False)
        Me.Guna2Panel2.PerformLayout()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        Me.Panel.ResumeLayout(False)
        Me.Guna2CustomGradientPanel3.ResumeLayout(False)
        Me.Guna2CustomGradientPanel3.PerformLayout()
        Me.DataPanel.ResumeLayout(False)
        Me.DataPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents Guna2Panel2 As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents LblPump As Label
    Friend WithEvents LblTransmission As Label
    Friend WithEvents LblEngine As Label
    Public WithEvents LblGeneral As Label
    Public WithEvents LblBomba As Label
    Public WithEvents LblTransmision As Label
    Public WithEvents LblMotor As Label
    Public WithEvents LblFracturador As Label
    Public WithEvents CbFrac As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblIdioma As Label
    Public WithEvents CbIdioma As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblResolucion As Label
    Public WithEvents CbResolucion As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblFracturadores As Label
    Public WithEvents BtnComSerie As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnWITS As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblPantallaSimple As Label
    Public WithEvents BtnAvanzado As Guna.UI2.WinForms.Guna2Button
    Public WithEvents CbTotalScreens As Guna.UI2.WinForms.Guna2ComboBox
    Friend WithEvents Panel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2CustomGradientPanel3 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Label4 As Label
    Friend WithEvents DataPanel As Panel
    Public WithEvents CbDato1 As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblDato1 As Label
    Public WithEvents BtnDelData As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAddData As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnStartGraph As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents Guna2Separator1 As Guna.UI2.WinForms.Guna2Separator
    Friend WithEvents TmrRefresh As Timer
End Class
