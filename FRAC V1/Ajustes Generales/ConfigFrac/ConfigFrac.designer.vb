﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ConfigFrac
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGV1 = New Guna.UI2.WinForms.Guna2DataGridView()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.BtnAgregar = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnEliminar = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnEditar = New Guna.UI2.WinForms.Guna2Button()
        Me.FracUnitDS = New sFRAC.FracUnitDS()
        Me.FracUnitDSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FracDBTableAdapter = New sFRAC.FracUnitDSTableAdapters.FracDBTableAdapter()
        Me.FracIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FracNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FracIPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FracEngineDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FracTransmissionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FracPumpDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FracBakerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TitlePanel.SuspendLayout()
        CType(Me.FracUnitDS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FracUnitDSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGV1
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(189, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.DGV1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV1.AutoGenerateColumns = False
        Me.DGV1.BackgroundColor = System.Drawing.Color.Gray
        Me.DGV1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(242, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DGV1.ColumnHeadersHeight = 30
        Me.DGV1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FracIDDataGridViewTextBoxColumn, Me.FracNameDataGridViewTextBoxColumn, Me.FracIPDataGridViewTextBoxColumn, Me.FracEngineDataGridViewTextBoxColumn, Me.FracTransmissionDataGridViewTextBoxColumn, Me.FracPumpDataGridViewTextBoxColumn, Me.FracBakerDataGridViewTextBoxColumn})
        Me.DGV1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DGV1.DataMember = "FracDB"
        Me.DGV1.DataSource = Me.FracUnitDSBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(246, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV1.DefaultCellStyle = DataGridViewCellStyle3
        Me.DGV1.GridColor = System.Drawing.Color.Silver
        Me.DGV1.Location = New System.Drawing.Point(12, 44)
        Me.DGV1.MultiSelect = False
        Me.DGV1.Name = "DGV1"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Montserrat", 8.25!)
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV1.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGV1.RowHeadersVisible = False
        Me.DGV1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DGV1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV1.Size = New System.Drawing.Size(906, 302)
        Me.DGV1.TabIndex = 0
        Me.DGV1.Theme = Guna.UI2.WinForms.Enums.DataGridViewPresetThemes.Blue
        Me.DGV1.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(189, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.DGV1.ThemeStyle.AlternatingRowsStyle.Font = Nothing
        Me.DGV1.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty
        Me.DGV1.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty
        Me.DGV1.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty
        Me.DGV1.ThemeStyle.BackColor = System.Drawing.Color.Gray
        Me.DGV1.ThemeStyle.GridColor = System.Drawing.Color.Silver
        Me.DGV1.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DGV1.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.DGV1.ThemeStyle.HeaderStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.5!)
        Me.DGV1.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White
        Me.DGV1.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DGV1.ThemeStyle.HeaderStyle.Height = 30
        Me.DGV1.ThemeStyle.ReadOnly = False
        Me.DGV1.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DGV1.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.DGV1.ThemeStyle.RowsStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!)
        Me.DGV1.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.Black
        Me.DGV1.ThemeStyle.RowsStyle.Height = 22
        Me.DGV1.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(107, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.DGV1.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.Black
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(930, 38)
        Me.TitlePanel.TabIndex = 71
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(418, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(94, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Ajustes"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(882, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'BtnAgregar
        '
        Me.BtnAgregar.Animated = True
        Me.BtnAgregar.BorderColor = System.Drawing.Color.White
        Me.BtnAgregar.BorderRadius = 5
        Me.BtnAgregar.BorderThickness = 1
        Me.BtnAgregar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAgregar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAgregar.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnAgregar.ForeColor = System.Drawing.Color.White
        Me.BtnAgregar.Location = New System.Drawing.Point(12, 352)
        Me.BtnAgregar.Name = "BtnAgregar"
        Me.BtnAgregar.Size = New System.Drawing.Size(175, 40)
        Me.BtnAgregar.TabIndex = 72
        Me.BtnAgregar.Text = "Agregar"
        '
        'BtnEliminar
        '
        Me.BtnEliminar.Animated = True
        Me.BtnEliminar.BorderColor = System.Drawing.Color.White
        Me.BtnEliminar.BorderRadius = 5
        Me.BtnEliminar.BorderThickness = 1
        Me.BtnEliminar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEliminar.FillColor = System.Drawing.Color.Transparent
        Me.BtnEliminar.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnEliminar.ForeColor = System.Drawing.Color.White
        Me.BtnEliminar.Location = New System.Drawing.Point(742, 352)
        Me.BtnEliminar.Name = "BtnEliminar"
        Me.BtnEliminar.Size = New System.Drawing.Size(175, 40)
        Me.BtnEliminar.TabIndex = 73
        Me.BtnEliminar.Text = "Eliminar"
        '
        'BtnEditar
        '
        Me.BtnEditar.Animated = True
        Me.BtnEditar.BorderColor = System.Drawing.Color.White
        Me.BtnEditar.BorderRadius = 5
        Me.BtnEditar.BorderThickness = 1
        Me.BtnEditar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEditar.FillColor = System.Drawing.Color.Transparent
        Me.BtnEditar.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnEditar.ForeColor = System.Drawing.Color.White
        Me.BtnEditar.Location = New System.Drawing.Point(378, 352)
        Me.BtnEditar.Name = "BtnEditar"
        Me.BtnEditar.Size = New System.Drawing.Size(175, 40)
        Me.BtnEditar.TabIndex = 74
        Me.BtnEditar.Text = "Editar"
        '
        'FracUnitDS
        '
        Me.FracUnitDS.DataSetName = "FracUnitDS"
        Me.FracUnitDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FracUnitDSBindingSource
        '
        Me.FracUnitDSBindingSource.DataSource = Me.FracUnitDS
        Me.FracUnitDSBindingSource.Position = 0
        '
        'FracDBTableAdapter
        '
        Me.FracDBTableAdapter.ClearBeforeFill = True
        '
        'FracIDDataGridViewTextBoxColumn
        '
        Me.FracIDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.FracIDDataGridViewTextBoxColumn.DataPropertyName = "FracID"
        Me.FracIDDataGridViewTextBoxColumn.HeaderText = "FracID"
        Me.FracIDDataGridViewTextBoxColumn.Name = "FracIDDataGridViewTextBoxColumn"
        Me.FracIDDataGridViewTextBoxColumn.Width = 73
        '
        'FracNameDataGridViewTextBoxColumn
        '
        Me.FracNameDataGridViewTextBoxColumn.DataPropertyName = "FracName"
        Me.FracNameDataGridViewTextBoxColumn.HeaderText = "FracName"
        Me.FracNameDataGridViewTextBoxColumn.Name = "FracNameDataGridViewTextBoxColumn"
        '
        'FracIPDataGridViewTextBoxColumn
        '
        Me.FracIPDataGridViewTextBoxColumn.DataPropertyName = "FracIP"
        Me.FracIPDataGridViewTextBoxColumn.HeaderText = "FracIP"
        Me.FracIPDataGridViewTextBoxColumn.Name = "FracIPDataGridViewTextBoxColumn"
        '
        'FracEngineDataGridViewTextBoxColumn
        '
        Me.FracEngineDataGridViewTextBoxColumn.DataPropertyName = "FracEngine"
        Me.FracEngineDataGridViewTextBoxColumn.HeaderText = "FracEngine"
        Me.FracEngineDataGridViewTextBoxColumn.Name = "FracEngineDataGridViewTextBoxColumn"
        '
        'FracTransmissionDataGridViewTextBoxColumn
        '
        Me.FracTransmissionDataGridViewTextBoxColumn.DataPropertyName = "FracTransmission"
        Me.FracTransmissionDataGridViewTextBoxColumn.HeaderText = "FracTransmission"
        Me.FracTransmissionDataGridViewTextBoxColumn.Name = "FracTransmissionDataGridViewTextBoxColumn"
        '
        'FracPumpDataGridViewTextBoxColumn
        '
        Me.FracPumpDataGridViewTextBoxColumn.DataPropertyName = "FracPump"
        Me.FracPumpDataGridViewTextBoxColumn.HeaderText = "FracPump"
        Me.FracPumpDataGridViewTextBoxColumn.Name = "FracPumpDataGridViewTextBoxColumn"
        '
        'FracBakerDataGridViewTextBoxColumn
        '
        Me.FracBakerDataGridViewTextBoxColumn.DataPropertyName = "FracBaker"
        Me.FracBakerDataGridViewTextBoxColumn.HeaderText = "FracBaker"
        Me.FracBakerDataGridViewTextBoxColumn.Name = "FracBakerDataGridViewTextBoxColumn"
        '
        'ConfigFrac
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(930, 410)
        Me.Controls.Add(Me.BtnEditar)
        Me.Controls.Add(Me.BtnEliminar)
        Me.Controls.Add(Me.BtnAgregar)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.DGV1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "ConfigFrac"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AddFrac"
        CType(Me.DGV1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        CType(Me.FracUnitDS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FracUnitDSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents Title As Label
    Public WithEvents DGV1 As Guna.UI2.WinForms.Guna2DataGridView
    Public WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Public WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents BtnEditar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnEliminar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAgregar As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents FracUnitDSBindingSource As BindingSource
    Friend WithEvents FracUnitDS As FracUnitDS
    Friend WithEvents FracDBTableAdapter As FracUnitDSTableAdapters.FracDBTableAdapter
    Friend WithEvents FracIDDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FracNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FracIPDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FracEngineDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FracTransmissionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FracPumpDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FracBakerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class
