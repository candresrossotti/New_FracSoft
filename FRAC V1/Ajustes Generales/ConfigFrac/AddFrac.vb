﻿Public Class AddFrac

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        Try
            ''Agregamos todo a la bas de datos
            ConfigFrac.FracDBTableAdapter.Insert(TbID.Text, TbName.Text, TbIP.Text, CbEngine.Text, CbTransmission.Text, CbPump.Text, CbCentrifuga.Checked, ISystem.GetFSEngineByEngine(CbEngine.Text),
                                                ISystem.GetFSTransByTrans(CbTransmission.Text), ISystem.GetFSPumpByPump(CbPump.Text))
            ConfigFrac.FracDBTableAdapter.Fill(ConfigFrac.FracUnitDS.FracDB)
            Me.Close()
        Catch ex As Exception
            LogError(ex)
            avisoTitulo = "Error"
            avisoStr = ex.Message
            AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub

    Private Sub AddFrac_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CbEngine.Items.Clear()
        CbEngine.Items.AddRange(ISystem.GetAllEngines().ToArray())

        CbTransmission.Items.Clear()
        CbTransmission.Items.AddRange(ISystem.GetAllTrans().ToArray())

        CbPump.Items.Clear()
        CbPump.Items.AddRange(ISystem.GetAllPumps().ToArray())

        CenterObject(BtnAceptar)
        CenterObject(CbCentrifuga)
    End Sub

    ''No aceptamos letras en el ID
    Private Sub TbID_TextChanged(sender As Object, e As EventArgs) Handles TbID.TextChanged
        If Not String.IsNullOrEmpty(sender.Text) Then
            If IsInteger(sender.Text) = False Then
                sender.Text = Mid(sender.Text, 1, sender.Text.Length() - 1)
                sender.SelectionStart = sender.Text.Length()
            End If
        End If
    End Sub

    ''Sub para centrar todo
    Public Sub CenterEverything()
        CenterObject(Title)

        LeftAlign(LblNombre, TbName.Left - 6)
        LeftAlign(LblMotor, CbEngine.Left - 6)
        LeftAlign(LblTransmision, CbTransmission.Left - 6)
        LeftAlign(LblBomba, CbPump.Left - 6)

    End Sub
End Class