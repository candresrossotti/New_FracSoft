﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EditFrac
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TbIP = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TbID = New Guna.UI2.WinForms.Guna2TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TbName = New Guna.UI2.WinForms.Guna2TextBox()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.CbPump = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblNombre = New System.Windows.Forms.Label()
        Me.LblTransmision = New System.Windows.Forms.Label()
        Me.CbEngine = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblBomba = New System.Windows.Forms.Label()
        Me.LblMotor = New System.Windows.Forms.Label()
        Me.CbTransmission = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.CbCentrifuga = New Guna.UI2.WinForms.Guna2CheckBox()
        Me.FracUnitDS = New sFRAC.FracUnitDS()
        Me.FracDBBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TitlePanel.SuspendLayout()
        CType(Me.FracUnitDS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FracDBBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TbIP
        '
        Me.TbIP.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbIP.DefaultText = ""
        Me.TbIP.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbIP.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbIP.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbIP.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbIP.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbIP.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.TbIP.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TbIP.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbIP.Location = New System.Drawing.Point(127, 140)
        Me.TbIP.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TbIP.Name = "TbIP"
        Me.TbIP.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbIP.PlaceholderText = ""
        Me.TbIP.SelectedText = ""
        Me.TbIP.Size = New System.Drawing.Size(242, 36)
        Me.TbIP.TabIndex = 2
        Me.TbIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(92, 147)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 22)
        Me.Label6.TabIndex = 90
        Me.Label6.Text = "IP:"
        '
        'TbID
        '
        Me.TbID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbID.DefaultText = ""
        Me.TbID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbID.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbID.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbID.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.TbID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TbID.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbID.Location = New System.Drawing.Point(127, 56)
        Me.TbID.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TbID.Name = "TbID"
        Me.TbID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbID.PlaceholderText = ""
        Me.TbID.SelectedText = ""
        Me.TbID.Size = New System.Drawing.Size(242, 36)
        Me.TbID.TabIndex = 0
        Me.TbID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(90, 63)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 22)
        Me.Label5.TabIndex = 88
        Me.Label5.Text = "ID:"
        '
        'TbName
        '
        Me.TbName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbName.DefaultText = ""
        Me.TbName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbName.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbName.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.TbName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TbName.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbName.Location = New System.Drawing.Point(127, 98)
        Me.TbName.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TbName.Name = "TbName"
        Me.TbName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbName.PlaceholderText = ""
        Me.TbName.SelectedText = ""
        Me.TbName.Size = New System.Drawing.Size(242, 36)
        Me.TbName.TabIndex = 1
        Me.TbName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 14.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(81, 377)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(238, 40)
        Me.BtnAceptar.TabIndex = 85
        Me.BtnAceptar.Text = "Aceptar"
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(400, 38)
        Me.TitlePanel.TabIndex = 86
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(92, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(216, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Editar Fracturador"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(352, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'CbPump
        '
        Me.CbPump.BackColor = System.Drawing.Color.Transparent
        Me.CbPump.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbPump.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbPump.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbPump.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPump.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbPump.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbPump.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CbPump.ItemHeight = 30
        Me.CbPump.Items.AddRange(New Object() {"Bomba 1", "NA"})
        Me.CbPump.Location = New System.Drawing.Point(127, 266)
        Me.CbPump.Name = "CbPump"
        Me.CbPump.Size = New System.Drawing.Size(242, 36)
        Me.CbPump.TabIndex = 5
        Me.CbPump.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblNombre
        '
        Me.LblNombre.AutoSize = True
        Me.LblNombre.BackColor = System.Drawing.Color.Transparent
        Me.LblNombre.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblNombre.ForeColor = System.Drawing.Color.White
        Me.LblNombre.Location = New System.Drawing.Point(41, 105)
        Me.LblNombre.Name = "LblNombre"
        Me.LblNombre.Size = New System.Drawing.Size(80, 22)
        Me.LblNombre.TabIndex = 78
        Me.LblNombre.Text = "Nombre:"
        '
        'LblTransmision
        '
        Me.LblTransmision.AutoSize = True
        Me.LblTransmision.BackColor = System.Drawing.Color.Transparent
        Me.LblTransmision.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblTransmision.ForeColor = System.Drawing.Color.White
        Me.LblTransmision.Location = New System.Drawing.Point(11, 231)
        Me.LblTransmision.Name = "LblTransmision"
        Me.LblTransmision.Size = New System.Drawing.Size(109, 22)
        Me.LblTransmision.TabIndex = 82
        Me.LblTransmision.Text = "Transmisión:"
        '
        'CbEngine
        '
        Me.CbEngine.BackColor = System.Drawing.Color.Transparent
        Me.CbEngine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbEngine.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbEngine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbEngine.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbEngine.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbEngine.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbEngine.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CbEngine.ItemHeight = 30
        Me.CbEngine.Items.AddRange(New Object() {"Liebherr D9620A7", "NA"})
        Me.CbEngine.Location = New System.Drawing.Point(127, 182)
        Me.CbEngine.Name = "CbEngine"
        Me.CbEngine.Size = New System.Drawing.Size(242, 36)
        Me.CbEngine.TabIndex = 3
        Me.CbEngine.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblBomba
        '
        Me.LblBomba.AutoSize = True
        Me.LblBomba.BackColor = System.Drawing.Color.Transparent
        Me.LblBomba.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblBomba.ForeColor = System.Drawing.Color.White
        Me.LblBomba.Location = New System.Drawing.Point(49, 273)
        Me.LblBomba.Name = "LblBomba"
        Me.LblBomba.Size = New System.Drawing.Size(72, 22)
        Me.LblBomba.TabIndex = 84
        Me.LblBomba.Text = "Bomba:"
        '
        'LblMotor
        '
        Me.LblMotor.AutoSize = True
        Me.LblMotor.BackColor = System.Drawing.Color.Transparent
        Me.LblMotor.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblMotor.ForeColor = System.Drawing.Color.White
        Me.LblMotor.Location = New System.Drawing.Point(60, 189)
        Me.LblMotor.Name = "LblMotor"
        Me.LblMotor.Size = New System.Drawing.Size(60, 22)
        Me.LblMotor.TabIndex = 80
        Me.LblMotor.Text = "Motor:"
        '
        'CbTransmission
        '
        Me.CbTransmission.BackColor = System.Drawing.Color.Transparent
        Me.CbTransmission.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbTransmission.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbTransmission.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbTransmission.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbTransmission.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbTransmission.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.CbTransmission.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CbTransmission.ItemHeight = 30
        Me.CbTransmission.Items.AddRange(New Object() {"Transmision 1", "NA"})
        Me.CbTransmission.Location = New System.Drawing.Point(127, 224)
        Me.CbTransmission.Name = "CbTransmission"
        Me.CbTransmission.Size = New System.Drawing.Size(242, 36)
        Me.CbTransmission.TabIndex = 4
        Me.CbTransmission.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'CbCentrifuga
        '
        Me.CbCentrifuga.AutoSize = True
        Me.CbCentrifuga.CheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbCentrifuga.CheckedState.BorderRadius = 0
        Me.CbCentrifuga.CheckedState.BorderThickness = 0
        Me.CbCentrifuga.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbCentrifuga.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbCentrifuga.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.CbCentrifuga.ForeColor = System.Drawing.Color.White
        Me.CbCentrifuga.Location = New System.Drawing.Point(143, 323)
        Me.CbCentrifuga.Name = "CbCentrifuga"
        Me.CbCentrifuga.Size = New System.Drawing.Size(114, 26)
        Me.CbCentrifuga.TabIndex = 92
        Me.CbCentrifuga.Text = "Centrifuga"
        Me.CbCentrifuga.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.CbCentrifuga.UncheckedState.BorderRadius = 0
        Me.CbCentrifuga.UncheckedState.BorderThickness = 0
        Me.CbCentrifuga.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        '
        'FracUnitDS
        '
        Me.FracUnitDS.DataSetName = "FracUnitDBDataSet"
        Me.FracUnitDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FracDBBindingSource
        '
        Me.FracDBBindingSource.DataMember = "FracDB"
        Me.FracDBBindingSource.DataSource = Me.FracUnitDS
        '
        'EditFrac
        '
        Me.AcceptButton = Me.BtnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(400, 429)
        Me.Controls.Add(Me.CbCentrifuga)
        Me.Controls.Add(Me.TbIP)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TbID)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TbName)
        Me.Controls.Add(Me.BtnAceptar)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.CbPump)
        Me.Controls.Add(Me.LblNombre)
        Me.Controls.Add(Me.LblTransmision)
        Me.Controls.Add(Me.CbEngine)
        Me.Controls.Add(Me.LblBomba)
        Me.Controls.Add(Me.LblMotor)
        Me.Controls.Add(Me.CbTransmission)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "EditFrac"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EditFrac"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        CType(Me.FracUnitDS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FracDBBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents Title As Label
    Public WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Public WithEvents TbIP As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents Label6 As Label
    Public WithEvents TbID As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents Label5 As Label
    Public WithEvents TbName As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents CbPump As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblNombre As Label
    Public WithEvents LblTransmision As Label
    Public WithEvents CbEngine As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblBomba As Label
    Public WithEvents LblMotor As Label
    Public WithEvents CbTransmission As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents CbCentrifuga As Guna.UI2.WinForms.Guna2CheckBox
    Public WithEvents FracUnitDS As FracUnitDS
    Public WithEvents FracDBBindingSource As BindingSource
End Class
