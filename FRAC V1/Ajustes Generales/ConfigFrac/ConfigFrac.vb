﻿Public Class ConfigFrac
    Private Sub AddFrac_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        'TODO: esta línea de código carga datos en la tabla 'FracUnitDBDataSet.FracDB' Puede moverla o quitarla según sea necesario.
        Me.FracDBTableAdapter.Fill(Me.FracUnitDS.FracDB)

        CenterToScreen()
    End Sub

    ''Eliminamos la linea seleccionada de la base de datos
    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles BtnEliminar.Click
        Try
            Dim SelectedRow As Integer = DGV1.SelectedRows(0).Index
            DGV1.Rows.RemoveAt(SelectedRow)
            FracDBTableAdapter.Update(FracUnitDS.FracDB)
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Abrimos el form para editar un fracturador
    Private Sub BtnEditar_Click(sender As Object, e As EventArgs) Handles BtnEditar.Click
        EditFrac.ShowDialog(Me)
    End Sub

    ''Abrimos el form para agregar un fracturador
    Private Sub BtnAgregar_Click(sender As Object, e As EventArgs) Handles BtnAgregar.Click
        AddFrac.ShowDialog(Me)
    End Sub

    ''Cuando cerramos el form guardamos todo en la base de datos
    Private Sub FormClose(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        FracDBTableAdapter.Update(FracUnitDS.FracDB)
    End Sub
End Class