﻿Public Class EditFrac

    Dim ID As String
    Dim FName As String
    Dim IP As String
    Dim Engine As String
    Dim Transmission As String
    Dim Pump As String
    Dim Centrifuge As Boolean

    Private Sub EditFrac_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CbEngine.Items.Clear()
        CbEngine.Items.AddRange(ISystem.GetAllEngines().ToArray())

        CbTransmission.Items.Clear()
        CbTransmission.Items.AddRange(ISystem.GetAllTrans().toarray())

        CbPump.Items.Clear()
        CbPump.Items.AddRange(ISystem.GetAllPumps().ToArray())

        ''Asignamos todos los valores en los TextBox del frac seleccionado
        ID = ConfigFrac.DGV1.SelectedRows.Item(0).Cells.Item(0).Value.ToString()
        If String.IsNullOrEmpty(ID) Then TbID.Text = "" Else TbID.Text = ID
        FName = ConfigFrac.DGV1.SelectedRows.Item(0).Cells.Item(1).Value.ToString()
        If String.IsNullOrEmpty(FName) Then TbName.Text = "" Else TbName.Text = FName
        IP = ConfigFrac.DGV1.SelectedRows.Item(0).Cells.Item(2).Value.ToString()
        If String.IsNullOrEmpty(IP) Then TbID.Text = "" Else TbIP.Text = IP
        Engine = ConfigFrac.DGV1.SelectedRows.Item(0).Cells.Item(3).Value.ToString()
        If String.IsNullOrEmpty(Engine) Then CbEngine.Text = "" Else CbEngine.Text = Engine
        Transmission = ConfigFrac.DGV1.SelectedRows.Item(0).Cells.Item(4).Value.ToString()
        If String.IsNullOrEmpty(Transmission) Then CbTransmission.Text = "" Else CbTransmission.Text = Transmission
        Pump = ConfigFrac.DGV1.SelectedRows.Item(0).Cells.Item(5).Value.ToString()
        If String.IsNullOrEmpty(Pump) Then CbPump.Text = "" Else CbPump.Text = Pump
        Centrifuge = ConfigFrac.DGV1.SelectedRows.Item(0).Cells.Item(6).Value
        CbCentrifuga.Checked = Centrifuge

        CenterEverything()
    End Sub

    ''Guardamos todos los datos editados en la base de datos
    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        Try

            ConfigFrac.FracDBTableAdapter.Update(TbID.Text, TbName.Text, TbIP.Text, CbEngine.Text, CbTransmission.Text, CbPump.Text, CbCentrifuga.Checked,
                                                 ISystem.GetFSEngineByEngine(CbEngine.Text), ISystem.GetFSTransByTrans(CbTransmission.Text), ISystem.GetFSPumpByPump(CbPump.Text),
                                                 ID, FName, IP, Engine, Transmission, Pump, Centrifuge, ISystem.GetFSEngineByEngine(Engine), ISystem.GetFSTransByTrans(Transmission),
                                                 ISystem.GetFSPumpByPump(Pump))
            ConfigFrac.FracDBTableAdapter.Fill(ConfigFrac.FracUnitDS.FracDB)
            Me.Close()
        Catch ex As Exception
            LogError(ex)
            avisoTitulo = "Error"
            avisoStr = ex.Message
            AvisoGeneral.ShowDialog(Me)
        End Try
    End Sub

    ''No aceptamos letras en el ID
    Private Sub TbID_TextChanged(sender As Object, e As EventArgs) Handles TbID.TextChanged
        If Not String.IsNullOrEmpty(sender.Text) Then
            If IsInteger(sender.Text) = False Then
                sender.Text = Mid(sender.Text, 1, sender.Text.Length() - 1)
                sender.SelectionStart = sender.Text.Length()
            End If
        End If
    End Sub

    ''Sub para centrar todo
    Public Sub CenterEverything()
        CenterObject(Title)

        LeftAlign(LblNombre, TbName.Left - 6)
        LeftAlign(LblMotor, CbEngine.Left - 6)
        LeftAlign(LblTransmision, CbTransmission.Left - 6)
        LeftAlign(LblBomba, CbPump.Left - 6)

        CenterObject(BtnAceptar)
        CenterObject(CbCentrifuga)
    End Sub
End Class