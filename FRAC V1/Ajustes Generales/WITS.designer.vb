﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfigWITS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ConfigWITS))
        Me.SerialPort = New System.IO.Ports.SerialPort(Me.components)
        Me.TmrSend = New System.Windows.Forms.Timer(Me.components)
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.TmrScan = New System.Windows.Forms.Timer(Me.components)
        Me.BtnGuardar = New Guna.UI2.WinForms.Guna2Button()
        Me.PortPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.CbStopBits = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.CbDataBits = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblWITS5 = New System.Windows.Forms.Label()
        Me.LblWITS4 = New System.Windows.Forms.Label()
        Me.CbParity = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblWITS3 = New System.Windows.Forms.Label()
        Me.CbBaudRate = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.CbComPorts = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblWITS2 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel16 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblPuertoSerie = New System.Windows.Forms.Label()
        Me.LblWITS1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblDatos = New System.Windows.Forms.Label()
        Me.DPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.DataPanel = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.LblWITS8 = New System.Windows.Forms.Label()
        Me.TBTimeStampID = New Guna.UI2.WinForms.Guna2TextBox()
        Me.LblWITS6 = New System.Windows.Forms.Label()
        Me.BtnDelDato = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAgregarDato = New Guna.UI2.WinForms.Guna2Button()
        Me.CbDato1 = New Guna.UI2.WinForms.Guna2ComboBox()
        Me.LblDato1 = New System.Windows.Forms.Label()
        Me.TbID1 = New Guna.UI2.WinForms.Guna2TextBox()
        Me.LblID1 = New System.Windows.Forms.Label()
        Me.LblWITS7 = New System.Windows.Forms.Label()
        Me.TSWITS = New Guna.UI2.WinForms.Guna2ToggleSwitch()
        Me.TitlePanel.SuspendLayout()
        Me.PortPanel.SuspendLayout()
        Me.Guna2CustomGradientPanel16.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.DPanel.SuspendLayout()
        Me.DataPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'TmrSend
        '
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.TitlePanel
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(815, 38)
        Me.TitlePanel.TabIndex = 144
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(291, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(232, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Configuración WITS"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(767, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.Title
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'TmrScan
        '
        Me.TmrScan.Interval = 500
        '
        'BtnGuardar
        '
        Me.BtnGuardar.Animated = True
        Me.BtnGuardar.BorderColor = System.Drawing.Color.White
        Me.BtnGuardar.BorderRadius = 5
        Me.BtnGuardar.BorderThickness = 1
        Me.BtnGuardar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnGuardar.FillColor = System.Drawing.Color.Transparent
        Me.BtnGuardar.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnGuardar.ForeColor = System.Drawing.Color.White
        Me.BtnGuardar.Location = New System.Drawing.Point(357, 391)
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.Size = New System.Drawing.Size(100, 40)
        Me.BtnGuardar.TabIndex = 146
        Me.BtnGuardar.Text = "Guardar"
        '
        'PortPanel
        '
        Me.PortPanel.BackColor = System.Drawing.Color.Transparent
        Me.PortPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PortPanel.BorderRadius = 6
        Me.PortPanel.BorderThickness = 1
        Me.PortPanel.Controls.Add(Me.CbStopBits)
        Me.PortPanel.Controls.Add(Me.CbDataBits)
        Me.PortPanel.Controls.Add(Me.LblWITS5)
        Me.PortPanel.Controls.Add(Me.LblWITS4)
        Me.PortPanel.Controls.Add(Me.CbParity)
        Me.PortPanel.Controls.Add(Me.LblWITS3)
        Me.PortPanel.Controls.Add(Me.CbBaudRate)
        Me.PortPanel.Controls.Add(Me.CbComPorts)
        Me.PortPanel.Controls.Add(Me.LblWITS2)
        Me.PortPanel.Controls.Add(Me.Guna2CustomGradientPanel16)
        Me.PortPanel.Controls.Add(Me.LblWITS1)
        Me.PortPanel.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PortPanel.CustomizableEdges.BottomLeft = False
        Me.PortPanel.CustomizableEdges.BottomRight = False
        Me.PortPanel.FillColor = System.Drawing.Color.Transparent
        Me.PortPanel.Location = New System.Drawing.Point(12, 44)
        Me.PortPanel.Name = "PortPanel"
        Me.PortPanel.Size = New System.Drawing.Size(285, 326)
        Me.PortPanel.TabIndex = 145
        '
        'CbStopBits
        '
        Me.CbStopBits.BackColor = System.Drawing.Color.Transparent
        Me.CbStopBits.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbStopBits.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbStopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbStopBits.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbStopBits.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbStopBits.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.CbStopBits.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbStopBits.ItemHeight = 30
        Me.CbStopBits.Items.AddRange(New Object() {"Ninguno", "Uno", "Dos"})
        Me.CbStopBits.Location = New System.Drawing.Point(130, 272)
        Me.CbStopBits.Name = "CbStopBits"
        Me.CbStopBits.Size = New System.Drawing.Size(140, 36)
        Me.CbStopBits.TabIndex = 138
        Me.CbStopBits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CbDataBits
        '
        Me.CbDataBits.BackColor = System.Drawing.Color.Transparent
        Me.CbDataBits.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbDataBits.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbDataBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbDataBits.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDataBits.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDataBits.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.CbDataBits.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbDataBits.ItemHeight = 30
        Me.CbDataBits.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8"})
        Me.CbDataBits.Location = New System.Drawing.Point(130, 212)
        Me.CbDataBits.Name = "CbDataBits"
        Me.CbDataBits.Size = New System.Drawing.Size(140, 36)
        Me.CbDataBits.TabIndex = 137
        Me.CbDataBits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblWITS5
        '
        Me.LblWITS5.AutoSize = True
        Me.LblWITS5.BackColor = System.Drawing.Color.Transparent
        Me.LblWITS5.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblWITS5.ForeColor = System.Drawing.Color.White
        Me.LblWITS5.Location = New System.Drawing.Point(18, 280)
        Me.LblWITS5.Name = "LblWITS5"
        Me.LblWITS5.Size = New System.Drawing.Size(102, 21)
        Me.LblWITS5.TabIndex = 136
        Me.LblWITS5.Text = "Bits de Stop:"
        '
        'LblWITS4
        '
        Me.LblWITS4.AutoSize = True
        Me.LblWITS4.BackColor = System.Drawing.Color.Transparent
        Me.LblWITS4.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblWITS4.ForeColor = System.Drawing.Color.White
        Me.LblWITS4.Location = New System.Drawing.Point(10, 220)
        Me.LblWITS4.Name = "LblWITS4"
        Me.LblWITS4.Size = New System.Drawing.Size(111, 21)
        Me.LblWITS4.TabIndex = 135
        Me.LblWITS4.Text = "Bits de Datos:"
        '
        'CbParity
        '
        Me.CbParity.BackColor = System.Drawing.Color.Transparent
        Me.CbParity.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbParity.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbParity.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbParity.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbParity.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.CbParity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbParity.ItemHeight = 30
        Me.CbParity.Items.AddRange(New Object() {"Ninguno", "Par", "Impar"})
        Me.CbParity.Location = New System.Drawing.Point(130, 152)
        Me.CbParity.Name = "CbParity"
        Me.CbParity.Size = New System.Drawing.Size(140, 36)
        Me.CbParity.TabIndex = 134
        Me.CbParity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblWITS3
        '
        Me.LblWITS3.AutoSize = True
        Me.LblWITS3.BackColor = System.Drawing.Color.Transparent
        Me.LblWITS3.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblWITS3.ForeColor = System.Drawing.Color.White
        Me.LblWITS3.Location = New System.Drawing.Point(5, 160)
        Me.LblWITS3.Name = "LblWITS3"
        Me.LblWITS3.Size = New System.Drawing.Size(120, 21)
        Me.LblWITS3.TabIndex = 133
        Me.LblWITS3.Text = "Bit de Paridad:"
        '
        'CbBaudRate
        '
        Me.CbBaudRate.BackColor = System.Drawing.Color.Transparent
        Me.CbBaudRate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbBaudRate.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbBaudRate.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbBaudRate.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbBaudRate.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.CbBaudRate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbBaudRate.ItemHeight = 30
        Me.CbBaudRate.Items.AddRange(New Object() {"1200", "2400", "4800", "9600", "14400", "19200", "28800", "38400", "56000", "57600", "115200"})
        Me.CbBaudRate.Location = New System.Drawing.Point(130, 92)
        Me.CbBaudRate.Name = "CbBaudRate"
        Me.CbBaudRate.Size = New System.Drawing.Size(140, 36)
        Me.CbBaudRate.TabIndex = 132
        Me.CbBaudRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CbComPorts
        '
        Me.CbComPorts.BackColor = System.Drawing.Color.Transparent
        Me.CbComPorts.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbComPorts.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbComPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbComPorts.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbComPorts.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbComPorts.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.CbComPorts.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbComPorts.ItemHeight = 30
        Me.CbComPorts.Items.AddRange(New Object() {"LAS 1", "LAS 2", "LAS 3", "LAS 4"})
        Me.CbComPorts.Location = New System.Drawing.Point(130, 32)
        Me.CbComPorts.Name = "CbComPorts"
        Me.CbComPorts.Size = New System.Drawing.Size(140, 36)
        Me.CbComPorts.TabIndex = 131
        Me.CbComPorts.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblWITS2
        '
        Me.LblWITS2.AutoSize = True
        Me.LblWITS2.BackColor = System.Drawing.Color.Transparent
        Me.LblWITS2.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblWITS2.ForeColor = System.Drawing.Color.White
        Me.LblWITS2.Location = New System.Drawing.Point(30, 100)
        Me.LblWITS2.Name = "LblWITS2"
        Me.LblWITS2.Size = New System.Drawing.Size(92, 21)
        Me.LblWITS2.TabIndex = 131
        Me.LblWITS2.Text = "Baud Rate:"
        '
        'Guna2CustomGradientPanel16
        '
        Me.Guna2CustomGradientPanel16.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.BorderRadius = 6
        Me.Guna2CustomGradientPanel16.Controls.Add(Me.LblPuertoSerie)
        Me.Guna2CustomGradientPanel16.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel16.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel16.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel16.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel16.Name = "Guna2CustomGradientPanel16"
        Me.Guna2CustomGradientPanel16.Size = New System.Drawing.Size(285, 26)
        Me.Guna2CustomGradientPanel16.TabIndex = 19
        '
        'LblPuertoSerie
        '
        Me.LblPuertoSerie.AutoSize = True
        Me.LblPuertoSerie.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblPuertoSerie.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblPuertoSerie.Location = New System.Drawing.Point(74, 4)
        Me.LblPuertoSerie.Name = "LblPuertoSerie"
        Me.LblPuertoSerie.Size = New System.Drawing.Size(176, 21)
        Me.LblPuertoSerie.TabIndex = 19
        Me.LblPuertoSerie.Text = "Ajuste de Puerto Serie"
        '
        'LblWITS1
        '
        Me.LblWITS1.AutoSize = True
        Me.LblWITS1.BackColor = System.Drawing.Color.Transparent
        Me.LblWITS1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblWITS1.ForeColor = System.Drawing.Color.White
        Me.LblWITS1.Location = New System.Drawing.Point(18, 40)
        Me.LblWITS1.Name = "LblWITS1"
        Me.LblWITS1.Size = New System.Drawing.Size(105, 21)
        Me.LblWITS1.TabIndex = 130
        Me.LblWITS1.Text = "Puerto Serie:"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblDatos)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(303, 44)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(500, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 143
        '
        'LblDatos
        '
        Me.LblDatos.AutoSize = True
        Me.LblDatos.BackColor = System.Drawing.Color.Transparent
        Me.LblDatos.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblDatos.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblDatos.Location = New System.Drawing.Point(163, 4)
        Me.LblDatos.Name = "LblDatos"
        Me.LblDatos.Size = New System.Drawing.Size(127, 21)
        Me.LblDatos.TabIndex = 19
        Me.LblDatos.Text = "Ajuste de Datos"
        '
        'DPanel
        '
        Me.DPanel.AutoScroll = True
        Me.DPanel.BackColor = System.Drawing.Color.Transparent
        Me.DPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.DPanel.Controls.Add(Me.DataPanel)
        Me.DPanel.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.DPanel.CustomBorderThickness = New System.Windows.Forms.Padding(1, 0, 1, 1)
        Me.DPanel.CustomizableEdges.TopLeft = False
        Me.DPanel.CustomizableEdges.TopRight = False
        Me.DPanel.FillColor = System.Drawing.Color.Transparent
        Me.DPanel.Location = New System.Drawing.Point(303, 70)
        Me.DPanel.Name = "DPanel"
        Me.DPanel.Size = New System.Drawing.Size(500, 300)
        Me.DPanel.TabIndex = 148
        '
        'DataPanel
        '
        Me.DataPanel.AutoScroll = True
        Me.DataPanel.Controls.Add(Me.Label8)
        Me.DataPanel.Controls.Add(Me.LblWITS8)
        Me.DataPanel.Controls.Add(Me.TBTimeStampID)
        Me.DataPanel.Controls.Add(Me.LblWITS6)
        Me.DataPanel.Controls.Add(Me.BtnDelDato)
        Me.DataPanel.Controls.Add(Me.BtnAgregarDato)
        Me.DataPanel.Controls.Add(Me.CbDato1)
        Me.DataPanel.Controls.Add(Me.LblDato1)
        Me.DataPanel.Controls.Add(Me.TbID1)
        Me.DataPanel.Controls.Add(Me.LblID1)
        Me.DataPanel.Location = New System.Drawing.Point(0, 0)
        Me.DataPanel.Name = "DataPanel"
        Me.DataPanel.Size = New System.Drawing.Size(500, 299)
        Me.DataPanel.TabIndex = 149
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(278, 14)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(98, 21)
        Me.Label8.TabIndex = 158
        Me.Label8.Text = "TimeStamp"
        '
        'LblWITS8
        '
        Me.LblWITS8.AutoSize = True
        Me.LblWITS8.BackColor = System.Drawing.Color.Transparent
        Me.LblWITS8.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblWITS8.ForeColor = System.Drawing.Color.White
        Me.LblWITS8.Location = New System.Drawing.Point(227, 14)
        Me.LblWITS8.Name = "LblWITS8"
        Me.LblWITS8.Size = New System.Drawing.Size(49, 21)
        Me.LblWITS8.TabIndex = 157
        Me.LblWITS8.Text = "Dato:"
        '
        'TBTimeStampID
        '
        Me.TBTimeStampID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TBTimeStampID.DefaultText = ""
        Me.TBTimeStampID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TBTimeStampID.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TBTimeStampID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TBTimeStampID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TBTimeStampID.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TBTimeStampID.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.TBTimeStampID.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TBTimeStampID.Location = New System.Drawing.Point(52, 6)
        Me.TBTimeStampID.Name = "TBTimeStampID"
        Me.TBTimeStampID.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TBTimeStampID.PlaceholderText = ""
        Me.TBTimeStampID.SelectedText = ""
        Me.TBTimeStampID.Size = New System.Drawing.Size(140, 36)
        Me.TBTimeStampID.TabIndex = 156
        Me.TBTimeStampID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblWITS6
        '
        Me.LblWITS6.AutoSize = True
        Me.LblWITS6.BackColor = System.Drawing.Color.Transparent
        Me.LblWITS6.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblWITS6.ForeColor = System.Drawing.Color.White
        Me.LblWITS6.Location = New System.Drawing.Point(19, 14)
        Me.LblWITS6.Name = "LblWITS6"
        Me.LblWITS6.Size = New System.Drawing.Size(30, 21)
        Me.LblWITS6.TabIndex = 155
        Me.LblWITS6.Text = "ID:"
        '
        'BtnDelDato
        '
        Me.BtnDelDato.Animated = True
        Me.BtnDelDato.BorderColor = System.Drawing.Color.White
        Me.BtnDelDato.BorderRadius = 5
        Me.BtnDelDato.BorderThickness = 1
        Me.BtnDelDato.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDelDato.FillColor = System.Drawing.Color.Transparent
        Me.BtnDelDato.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnDelDato.ForeColor = System.Drawing.Color.White
        Me.BtnDelDato.Location = New System.Drawing.Point(269, 131)
        Me.BtnDelDato.Name = "BtnDelDato"
        Me.BtnDelDato.Size = New System.Drawing.Size(142, 40)
        Me.BtnDelDato.TabIndex = 154
        Me.BtnDelDato.Text = "Eliminar Dato"
        '
        'BtnAgregarDato
        '
        Me.BtnAgregarDato.Animated = True
        Me.BtnAgregarDato.BorderColor = System.Drawing.Color.White
        Me.BtnAgregarDato.BorderRadius = 5
        Me.BtnAgregarDato.BorderThickness = 1
        Me.BtnAgregarDato.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAgregarDato.FillColor = System.Drawing.Color.Transparent
        Me.BtnAgregarDato.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnAgregarDato.ForeColor = System.Drawing.Color.White
        Me.BtnAgregarDato.Location = New System.Drawing.Point(93, 131)
        Me.BtnAgregarDato.Name = "BtnAgregarDato"
        Me.BtnAgregarDato.Size = New System.Drawing.Size(142, 40)
        Me.BtnAgregarDato.TabIndex = 153
        Me.BtnAgregarDato.Text = "Agregar Dato"
        '
        'CbDato1
        '
        Me.CbDato1.BackColor = System.Drawing.Color.Transparent
        Me.CbDato1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CbDato1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CbDato1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbDato1.FocusedColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDato1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CbDato1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.CbDato1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(112, Byte), Integer))
        Me.CbDato1.ItemHeight = 30
        Me.CbDato1.Location = New System.Drawing.Point(278, 66)
        Me.CbDato1.Name = "CbDato1"
        Me.CbDato1.Size = New System.Drawing.Size(200, 36)
        Me.CbDato1.TabIndex = 152
        Me.CbDato1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblDato1
        '
        Me.LblDato1.AutoSize = True
        Me.LblDato1.BackColor = System.Drawing.Color.Transparent
        Me.LblDato1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblDato1.ForeColor = System.Drawing.Color.White
        Me.LblDato1.Location = New System.Drawing.Point(223, 74)
        Me.LblDato1.Name = "LblDato1"
        Me.LblDato1.Size = New System.Drawing.Size(49, 21)
        Me.LblDato1.TabIndex = 151
        Me.LblDato1.Text = "Dato:"
        '
        'TbID1
        '
        Me.TbID1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TbID1.DefaultText = ""
        Me.TbID1.DisabledState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.TbID1.DisabledState.FillColor = System.Drawing.Color.FromArgb(CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(226, Byte), Integer))
        Me.TbID1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbID1.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.TbID1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbID1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.TbID1.HoverState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TbID1.Location = New System.Drawing.Point(52, 66)
        Me.TbID1.Name = "TbID1"
        Me.TbID1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TbID1.PlaceholderText = ""
        Me.TbID1.SelectedText = ""
        Me.TbID1.Size = New System.Drawing.Size(140, 36)
        Me.TbID1.TabIndex = 150
        Me.TbID1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LblID1
        '
        Me.LblID1.AutoSize = True
        Me.LblID1.BackColor = System.Drawing.Color.Transparent
        Me.LblID1.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblID1.ForeColor = System.Drawing.Color.White
        Me.LblID1.Location = New System.Drawing.Point(19, 74)
        Me.LblID1.Name = "LblID1"
        Me.LblID1.Size = New System.Drawing.Size(30, 21)
        Me.LblID1.TabIndex = 149
        Me.LblID1.Text = "ID:"
        '
        'LblWITS7
        '
        Me.LblWITS7.AutoSize = True
        Me.LblWITS7.BackColor = System.Drawing.Color.Transparent
        Me.LblWITS7.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.LblWITS7.ForeColor = System.Drawing.Color.White
        Me.LblWITS7.Location = New System.Drawing.Point(743, 387)
        Me.LblWITS7.Name = "LblWITS7"
        Me.LblWITS7.Size = New System.Drawing.Size(62, 21)
        Me.LblWITS7.TabIndex = 163
        Me.LblWITS7.Text = "Activar"
        '
        'TSWITS
        '
        Me.TSWITS.Animated = True
        Me.TSWITS.CheckedState.FillColor = System.Drawing.Color.LimeGreen
        Me.TSWITS.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TSWITS.Location = New System.Drawing.Point(745, 411)
        Me.TSWITS.Name = "TSWITS"
        Me.TSWITS.Size = New System.Drawing.Size(58, 20)
        Me.TSWITS.TabIndex = 162
        Me.TSWITS.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.TSWITS.UncheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.TSWITS.UncheckedState.InnerBorderColor = System.Drawing.Color.White
        Me.TSWITS.UncheckedState.InnerColor = System.Drawing.Color.White
        '
        'ConfigWITS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(815, 443)
        Me.Controls.Add(Me.LblWITS7)
        Me.Controls.Add(Me.TSWITS)
        Me.Controls.Add(Me.BtnGuardar)
        Me.Controls.Add(Me.PortPanel)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Controls.Add(Me.DPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ConfigWITS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "WITS"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.PortPanel.ResumeLayout(False)
        Me.PortPanel.PerformLayout()
        Me.Guna2CustomGradientPanel16.ResumeLayout(False)
        Me.Guna2CustomGradientPanel16.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.DPanel.ResumeLayout(False)
        Me.DataPanel.ResumeLayout(False)
        Me.DataPanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Public WithEvents SerialPort As IO.Ports.SerialPort
    Public WithEvents TmrSend As Timer
    Public WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Public WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents TmrScan As Timer
    Public WithEvents BtnGuardar As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PortPanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents CbStopBits As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents CbDataBits As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblWITS5 As Label
    Public WithEvents LblWITS4 As Label
    Public WithEvents CbParity As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblWITS3 As Label
    Public WithEvents CbBaudRate As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents CbComPorts As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblWITS2 As Label
    Public WithEvents Guna2CustomGradientPanel16 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblPuertoSerie As Label
    Public WithEvents LblWITS1 As Label
    Public WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Public WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblDatos As Label
    Public WithEvents DPanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents DataPanel As Panel
    Public WithEvents BtnDelDato As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAgregarDato As Guna.UI2.WinForms.Guna2Button
    Public WithEvents CbDato1 As Guna.UI2.WinForms.Guna2ComboBox
    Public WithEvents LblDato1 As Label
    Public WithEvents TbID1 As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents LblID1 As Label
    Public WithEvents Label8 As Label
    Public WithEvents LblWITS8 As Label
    Public WithEvents TBTimeStampID As Guna.UI2.WinForms.Guna2TextBox
    Public WithEvents LblWITS6 As Label
    Public WithEvents LblWITS7 As Label
    Public WithEvents TSWITS As Guna.UI2.WinForms.Guna2ToggleSwitch
End Class
