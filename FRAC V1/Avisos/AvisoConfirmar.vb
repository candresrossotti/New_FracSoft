﻿Public Class AvisoConfirmar
    Private Sub AvisoConfirmar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        Title.Text = avisoTitulo
        LblText.Text = avisoStr
        CenterObject(LblText)
        CenterObject(Title)
        CenterObject(PanelBtn)
        TopMost = True
    End Sub

    ''Aceptar -> DialogResult.OK
    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        DialogResult = DialogResult.OK
    End Sub

    Private Sub BtnDenegar_Click(sender As Object, e As EventArgs) Handles BtnDenegar.Click
        Close()
    End Sub
End Class