﻿Public Class Reinicio
    ''Si no quiere reiniciar ahora, posponemos
    Private Sub BtnPosponer_Click(sender As Object, e As EventArgs) Handles BtnPosponer.Click
        Close()
    End Sub

    Private Sub Reinicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        CenterObject(Title)
        CenterObject(LblText)
        CenterObject(Panel1)
    End Sub

    ''Si da restart, cerramos todo y reiniciamos
    Private Sub BtnRestart_Click(sender As Object, e As EventArgs) Handles BtnRestart.Click
        IdleALL()
        Application.Restart()       ''No funciona esto por alguna razon
    End Sub

End Class