﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Reinicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.LblText = New System.Windows.Forms.Label()
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BtnRestart = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPosponer = New Guna.UI2.WinForms.Guna2Button()
        Me.TitlePanel.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'LblText
        '
        Me.LblText.AutoSize = True
        Me.LblText.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblText.ForeColor = System.Drawing.Color.White
        Me.LblText.Location = New System.Drawing.Point(170, 59)
        Me.LblText.Name = "LblText"
        Me.LblText.Size = New System.Drawing.Size(162, 22)
        Me.LblText.TabIndex = 55
        Me.LblText.Text = "Reinicio pendiente"
        Me.LblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(500, 38)
        Me.TitlePanel.TabIndex = 54
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Segoe UI", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(232, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(65, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Aviso"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(452, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.BtnRestart)
        Me.Panel1.Controls.Add(Me.BtnPosponer)
        Me.Panel1.Location = New System.Drawing.Point(82, 108)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(338, 42)
        Me.Panel1.TabIndex = 58
        '
        'BtnRestart
        '
        Me.BtnRestart.Animated = True
        Me.BtnRestart.BorderColor = System.Drawing.Color.White
        Me.BtnRestart.BorderRadius = 5
        Me.BtnRestart.BorderThickness = 1
        Me.BtnRestart.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRestart.FillColor = System.Drawing.Color.Transparent
        Me.BtnRestart.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnRestart.ForeColor = System.Drawing.Color.White
        Me.BtnRestart.Location = New System.Drawing.Point(172, 1)
        Me.BtnRestart.Name = "BtnRestart"
        Me.BtnRestart.Size = New System.Drawing.Size(165, 40)
        Me.BtnRestart.TabIndex = 59
        Me.BtnRestart.Text = "Reiniciar ahora"
        '
        'BtnPosponer
        '
        Me.BtnPosponer.Animated = True
        Me.BtnPosponer.BorderColor = System.Drawing.Color.White
        Me.BtnPosponer.BorderRadius = 5
        Me.BtnPosponer.BorderThickness = 1
        Me.BtnPosponer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPosponer.FillColor = System.Drawing.Color.Transparent
        Me.BtnPosponer.Font = New System.Drawing.Font("Montserrat", 11.0!)
        Me.BtnPosponer.ForeColor = System.Drawing.Color.White
        Me.BtnPosponer.Location = New System.Drawing.Point(1, 1)
        Me.BtnPosponer.Name = "BtnPosponer"
        Me.BtnPosponer.Size = New System.Drawing.Size(165, 40)
        Me.BtnPosponer.TabIndex = 58
        Me.BtnPosponer.Text = "Posponer reinicio"
        '
        'Reinicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(500, 164)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.LblText)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Reinicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reinicio"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents LblText As Label
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents BtnRestart As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents BtnPosponer As Guna.UI2.WinForms.Guna2Button
End Class
