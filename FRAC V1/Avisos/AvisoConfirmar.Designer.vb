﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AvisoConfirmar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.LblText = New System.Windows.Forms.Label()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.PanelBtn = New System.Windows.Forms.Panel()
        Me.BtnDenegar = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnAceptar = New Guna.UI2.WinForms.Guna2Button()
        Me.TitlePanel.SuspendLayout()
        Me.PanelBtn.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(500, 38)
        Me.TitlePanel.TabIndex = 54
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(212, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(76, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Titulo"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(452, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'LblText
        '
        Me.LblText.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.LblText.ForeColor = System.Drawing.Color.White
        Me.LblText.Location = New System.Drawing.Point(12, 60)
        Me.LblText.Name = "LblText"
        Me.LblText.Size = New System.Drawing.Size(476, 79)
        Me.LblText.TabIndex = 55
        Me.LblText.Text = "Texto"
        Me.LblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.Title
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'PanelBtn
        '
        Me.PanelBtn.Controls.Add(Me.BtnDenegar)
        Me.PanelBtn.Controls.Add(Me.BtnAceptar)
        Me.PanelBtn.Location = New System.Drawing.Point(132, 154)
        Me.PanelBtn.Name = "PanelBtn"
        Me.PanelBtn.Size = New System.Drawing.Size(228, 40)
        Me.PanelBtn.TabIndex = 57
        '
        'BtnDenegar
        '
        Me.BtnDenegar.Animated = True
        Me.BtnDenegar.BorderColor = System.Drawing.Color.White
        Me.BtnDenegar.BorderRadius = 5
        Me.BtnDenegar.BorderThickness = 1
        Me.BtnDenegar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDenegar.FillColor = System.Drawing.Color.Transparent
        Me.BtnDenegar.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnDenegar.ForeColor = System.Drawing.Color.White
        Me.BtnDenegar.Location = New System.Drawing.Point(117, 0)
        Me.BtnDenegar.Name = "BtnDenegar"
        Me.BtnDenegar.Size = New System.Drawing.Size(111, 40)
        Me.BtnDenegar.TabIndex = 58
        Me.BtnDenegar.Text = "No"
        '
        'BtnAceptar
        '
        Me.BtnAceptar.Animated = True
        Me.BtnAceptar.BorderColor = System.Drawing.Color.White
        Me.BtnAceptar.BorderRadius = 5
        Me.BtnAceptar.BorderThickness = 1
        Me.BtnAceptar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAceptar.FillColor = System.Drawing.Color.Transparent
        Me.BtnAceptar.Font = New System.Drawing.Font("Montserrat", 12.0!)
        Me.BtnAceptar.ForeColor = System.Drawing.Color.White
        Me.BtnAceptar.Location = New System.Drawing.Point(0, 0)
        Me.BtnAceptar.Name = "BtnAceptar"
        Me.BtnAceptar.Size = New System.Drawing.Size(111, 40)
        Me.BtnAceptar.TabIndex = 57
        Me.BtnAceptar.Text = "Si"
        '
        'AvisoConfirmar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(95, Byte), Integer), CType(CType(117, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(500, 206)
        Me.Controls.Add(Me.PanelBtn)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.LblText)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "AvisoConfirmar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AvisoConfirmar"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.PanelBtn.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents LblText As Label
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents PanelBtn As Panel
    Friend WithEvents BtnDenegar As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents BtnAceptar As Guna.UI2.WinForms.Guna2Button
End Class
