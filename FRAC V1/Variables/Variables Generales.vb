﻿Module Variables_Generales

    ''Variables sobre clases para el manejo del fracturador
    Public ISystem As IFracSystem = IFracSystem.GetInstancia(Inicio, Ajustes, AjustesFracturador, AdminControl, NewPassword, FracDiagnostico, FracMantenimiento, FracErrores, FullAutomation,
                                    InfoShow, TesteoLinea, SetFC, SetTRIP, SetZERO, FracSelect, Container1_1, SetGEAR, SetRPM, Container2_1, Container4_1, Container8_1, Principal,
                                    Secundaria, Tercera, ConfigWITS, SerialPortConfig, ConfigFrac, AddFrac, EditFrac)

    Public LSelect As New LanguageSelect(ISystem)
    Public FConfig As New FracConfig
    Public FracData As New FData
    Public FracEdit As New FracDB
    Public FracAutomate As New FracAutomation
    Public GraphConnect As New SGraphConnect

    ''VARIABLES FRAC-SELECT
    Public CurrentFracName As String

    ''Variables para totalizadores
    Public Caudal As Double = 0
    Public PresionMaxima As Integer
    Public Totalizador As Double
    Public CombustibleTotal As Double

    ''Variables para aviso general
    Public avisoStr As String
    Public avisoTitulo As String

    ''Contraseña para administrador
    Public adminPassword As String = My.Settings.Item("adminPassword")
    Public sPassMem As Boolean = False

    ''Variables para graficador por TCP
    Public IsConnected As Boolean = False

    ''Maximos fracturadores para distintas operaciones
    Public MaxFracs As Integer
    Public TotalMoxaData As Integer = 75
    Public OneConnected As Boolean = False

    ''Variables para fracturadores
    Public ConnectedFracsID As New List(Of Integer)

    ''Variables para serial port/wits
    Public SPFracs As New List(Of Integer)
    Public WITSFracs As New List(Of Integer)
    Public SPSend As New List(Of String)
    Public WITSSend As New List(Of String)
    Public WITSIDSend As New List(Of Integer)
    Public SPNData As Integer = My.Settings.SPNData
    Public WITSNData As Integer = My.Settings.WITSNData
    Public SPDevices As Integer = 0
    Public WITSDevices As Integer = 0

    ''Extra
    Public MarchaActualInt As Integer
    Public CurrentFrac As Integer = 1
    Public FC As New List(Of Double) From {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}                             ''FACTOR CAUDAL
    Public PZero(23) As Integer
    Public MaxTRIP As Integer = 20000

    ''Variable para el envio de datos al graficador/SP/WITS(EN-ES)
    Public ReadOnly DataNombre As New List(Of String) From {
        "RPMMotor_",
        "MarchaActual_",
        "Viatran_",
        "TRIP_",
        "PresSuc_",
        "TemperaturaAceiteTransmision_",
        "RPMSalidaTransmision_",
        "Voltaje_",
        "PresionAceiteMotor_",
        "Caudal_",
        "FuelRate_",
         "TemperaturaLUB_LP_",
         "PorcentajeCarga_",
         "HHPMotor_",
         "PresionLUB_LP_",
         "HorasMotor_",
         "PresTrans_",
         "TemperaturaAguaMotor_",
         "CombustibleTanque_",
         "TemperaturaAceiteMotor_"
    }

    Public ReadOnly DataListES As New List(Of String) From {
        "RPM",
        "Marcha",
        "Presion Actual",
        "TRIP",
        "Presion Succion",
        "Temperatura Transmision",
        "RPM Transmision",
        "Voltaje",
        "Presion Motor",
        "Caudal",
        "Combustible [L/Hr]",
        "Temperatura LUB",
        "% Carga Motor",
        "HHP",
        "Presion LUB",
        "Horas Motor",
        "Presion Transmision",
        "Temp. Agua Motor",
        "% Combustible",
        "Temp. Aceite Motor"
    }
    Public ReadOnly DataListEN As New List(Of String) From {
        "RPM",
        "Gear",
        "Pressure",
        "TRIP",
        "Suction Pressure",
        "Transmission Temperature",
        "Transmission RPM",
        "Voltage",
        "Engine Pressure",
        "Flow rate",
        "Fuel rate [L/Hr]",
        "LUB Temp",
        "% Eng Load",
        "HHP",
        "LUB Pressure",
        "Engine Hours",
        "Transmission Pressure",
        "Engine Coolant Temp",
        "% Fuel",
        "Engine Oil Temp."
    }
    Public DataList As New List(Of String)

    ''Variables para cambiar de resolucion
    Public Principal As New Object
    Public Secundaria As New Object
    Public Tercera As New Object
    Public Container1_1 As New Object
    Public Container2_1 As New Object
    Public Container4_1 As New Object
    Public Container8_1 As New Object
    Public Container1_2 As New Object
    Public Container2_2 As New Object
    Public Container4_2 As New Object
    Public Container8_2 As New Object
    Public Container1_3 As New Object
    Public Container2_3 As New Object
    Public Container4_3 As New Object
    Public Container8_3 As New Object

#Region "Variables para el manejo de clases"
    Public Class OneFracDo
        Public Function GetClass()
            Select Case My.Settings.Resolucion
                Case "1920x1080"
                    Return New OneFrac_1920x1080
                Case "1280x840"
                    Return New OneFrac_1280x840
                Case Else
                    Return Nothing
            End Select
        End Function
    End Class
    Public OneFrac As New OneFracDo

    Public Class TwoFracDo
        Public Function GetClass()
            Select Case My.Settings.Resolucion
                Case "1920x1080"
                    Return New TwoFrac_1920x1080
                Case "1280x840"
                    Return New TwoFrac_1280x840
                Case Else
                    Return Nothing
            End Select
        End Function
    End Class
    Public TwoFrac As New TwoFracDo

    Public Class FourFracDo
        Public Function GetClass()
            Select Case My.Settings.Resolucion
                Case "1920x1080"
                    Return New FourFrac_1920x1080
                Case "1280x840"
                    Return New FourFrac_1280x840
                Case Else
                    Return Nothing
            End Select
        End Function
    End Class
    Public FourFrac As New FourFracDo

    Public Class EightFracDo
        Public Function GetClass()
            Select Case My.Settings.Resolucion
                Case "1920x1080"
                    Return New EightFrac_1920x1080
                Case "1280x840"
                    Return New EightFrac_1280x840
                Case Else
                    Return Nothing
            End Select
        End Function
    End Class
    Public EightFrac As New EightFracDo
#End Region

    ''Variables para la automatizacion
    Public FracList As New List(Of Integer)
    Public BackUpFrac As Integer

    ''Tiempo de operacion 
    Public TiempoOperacion As String
    Public OperationStartTime As Date

    ''Variables para checklist en ajustes
    Public FracturaGeneralName As String = "Fractura General.pdf"
    Public GenerateCSVBackUp As Boolean = My.Settings.GenerateCSVBackup
    Public CSVBackUpPath As String = My.Settings.CSVBackpPath
    Public AllowMultipleConnections As Boolean = My.Settings.AllowMultipleConnections
    Public CSVOperationPath As String = My.Settings.CSVOperationPath
    Public GenerateExcel As Boolean = My.Settings.GenerateExcel
    Public PDFPath As String = My.Settings.PDFPath
    Public GeneratePDF As Boolean = My.Settings.GeneratePDF
    Public GenerateRPMGraph As Boolean = My.Settings.GenerateRPMGraph
    Public GenerateCaudalGraph As Boolean = My.Settings.GenerateCaudalGraph
    Public GeneratePresionGraph As Boolean = My.Settings.GeneratePresionGraph
    Public GeneratePresCaudGraph As Boolean = My.Settings.GeneratePresCaudGraph
    Public GenerateEngTransGraph As Boolean = My.Settings.GenerateEngTransGraph
    Public GeneratePumpGraph As Boolean = My.Settings.GeneratePumpGraph

    ''Variables para CSV
    Public ActiveCSV As New List(Of Integer)
    Public CSVFlushInterval As Integer = 60000

    ''Variables para automatizacion
    Public UseBackupFrac As Boolean = My.Settings.UseBackupFrac

    ''Variables para datos de fractura
    Public Yacimiento As String = My.Settings.Yacimiento
    Public Etapa As Integer = My.Settings.Etapa
    Public Locacion As String = My.Settings.Locacion
    Public TipoPozo As String = My.Settings.TipoPozo
    Public Distrito As String = My.Settings.Distrito
    Public Provincia As String = My.Settings.Provincia
    Public Cliente As String = My.Settings.Cliente

    ''Variables para el graficador
    Public NGraphData As Integer = My.Settings.NGraphData
    Public GraphSend As New List(Of String)

End Module
