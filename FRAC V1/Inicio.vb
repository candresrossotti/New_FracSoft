﻿Public Class Inicio

    Public LastTime As Date

    Private Sub Inicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Seteamos el icono general
        Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)

        'CurrentFrac seteado para evitar errores
        currentFrac = 1
        CenterToScreen()
        TmrDo.Start()

        ''Seteamos idioma, resolucion y demas cosas para que funcione todo
        Select Case My.Settings.Idioma
            Case "Español"
                DataList = DataListES
            Case "Ingles"
                DataList = DataListEN
        End Select

        ''Ajustamos la resolucion deseada y generamos todos los contenedores
        Select Case My.Settings.Resolucion
            Case "1920x1080"
                Principal = Principal_1920x1080
                Secundaria = Secundaria_1920x1080
                Tercera = Tercera_1920x1080

                Container1_1 = New Container1_1920x1080
                Container2_1 = New Container2_1920x1080
                Container4_1 = New Container4_1920x1080
                Container8_1 = New Container8_1920x1080

                Container1_2 = New Container1_1920x1080
                Container2_2 = New Container2_1920x1080
                Container4_2 = New Container4_1920x1080
                Container8_2 = New Container8_1920x1080

                Container1_3 = New Container1_1920x1080
                Container2_3 = New Container2_1920x1080
                Container4_3 = New Container4_1920x1080
                Container8_3 = New Container8_1920x1080

            Case "1280x840"
                Principal = Principal_1280x840
                Secundaria = Secundaria_1280x840
                Tercera = Tercera_1280x840

                Container1_1 = New Container1_1280x840
                Container2_1 = New Container2_1280x840
                Container4_1 = New Container4_1280x840
                Container8_1 = New Container8_1280x840

                Container1_2 = New Container1_1280x840
                Container2_2 = New Container2_1280x840
                Container4_2 = New Container4_1280x840
                Container8_2 = New Container8_1280x840

                Container1_3 = New Container1_1280x840
                Container2_3 = New Container2_1280x840
                Container4_3 = New Container4_1280x840
                Container8_3 = New Container8_1280x840
        End Select

        ''Sub para actualizar ISystem con todos las ventanas
        UpdateISystem()

        ''Para que podamos poner fondo transparente
        SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        Me.BackColor = Color.Transparent

        ''Actualizamos la clase LSelect con ISystem
        LSelect.Update(ISystem)
        ''Seteamos el lenguaje elegido
        LSelect.SetLanguage(My.Settings.Idioma)
        ''Actualizamos la interfaz
        LSelect.UpdateFracInterface()

        ''Handler para cuando inicie una operacion
        AddHandler IFracSystem.OperationStart, AddressOf OpStart
        AddHandler IFracSystem.ScanStep, AddressOf PbIncrement
    End Sub

    ''Sub para incrementar el progressbar
    Private Sub PbIncrement()
        PbInicio.Increment(1)
    End Sub

    ''Sub para actualizar ISystem con las ventanas
    Private Sub UpdateISystem()

        ISystem.UpdateISystem(Principal)
        ISystem.UpdateISystem(Secundaria)
        ISystem.UpdateISystem(Tercera)
        ISystem.UpdateISystem(Container1_1)
        ISystem.UpdateISystem(Container2_1)
        ISystem.UpdateISystem(Container4_1)
        ISystem.UpdateISystem(Container8_1)
        ISystem.UpdateISystem(Container1_2, 1)
        ISystem.UpdateISystem(Container2_2, 1)
        ISystem.UpdateISystem(Container4_2, 1)
        ISystem.UpdateISystem(Container8_2, 1)
        ISystem.UpdateISystem(Container1_3, 2)
        ISystem.UpdateISystem(Container2_3, 2)
        ISystem.UpdateISystem(Container4_3, 2)
        ISystem.UpdateISystem(Container8_3, 2)

    End Sub

    ''Tmr para inicio
    Private Sub TmrStart(sender As Object, e As EventArgs) Handles TmrDo.Tick
        TmrDo.Stop()                                                    ''Detenemos el tick del timer
        Dim status As Boolean = ISystem.AutoScan(True)                  ''Buscamos fracturadores en la red con el sistema de base de datos
        InitWindows()
        If status = False Then
            avisoStr = "No se encontraron fracturadores"
            avisoTitulo = "Aviso"
            AvisoGeneral.ShowDialog(Me)
            'Principal.Close()
            'Close()
        End If
        FracSelect.ShowDialog(Me)
        Hide()
    End Sub

#Region "TmrOP"
    Private Sub OpStart()
        If IFracSystem.ActiveOP = True Then
            ResetGValues()                  ''Reseteamos todos los valores generales
            TmrOp.Start()
        Else
            TmrOp.Stop()
        End If
    End Sub
    ''Timer para obtener tiempo de operacion
    Public Sub TmrOp_Tick(sender As Object, e As EventArgs) Handles TmrOp.Tick
        Dim GetTimeOpThread As New Task(AddressOf TmrOpDo)
        GetTimeOpThread.Start()
    End Sub

    Private Sub TmrOpDo()
        TiempoOperacion = Now.Subtract(OperationStartTime).ToString("hh\:mm")
    End Sub

#End Region

#Region "TmrTotal"
    Private Sub TmrTotal_Tick(sender As Object, e As EventArgs) Handles TmrTotal.Tick
        Dim TotalTask As New Task(Sub() GetTotal(Now.Subtract(LastTime).TotalSeconds))
        TotalTask.Start()
    End Sub

    Private Sub GetTotal(ByVal OperationSpan As Integer)
        If ConnectedFracsID.Count > 0 Then
            Dim ConsumoMotor As Double = 0
            Dim PresionTemp As Integer()

            ReDim PresionTemp(ConnectedFracsID.Count)


            Caudal = 0
            Totalizador = 0

            For Fracs = 1 To ConnectedFracsID.Count
                Caudal += GetDouble(FracData.DblVFrac("Caudal_" + ConnectedFracsID(Fracs - 1).ToString()))
                PresionTemp(Fracs - 1) = FracData.DblVFrac("Viatran_" + ConnectedFracsID(Fracs - 1).ToString())
                ConsumoMotor += FracData.DblVFrac("Combustible_" + ConnectedFracsID(Fracs - 1).ToString() + "_Acum")
            Next

            PresionMaxima = GetMax(PresionTemp)
            CombustibleTotal = ConsumoMotor

            For i = 0 To ConnectedFracsID.Count - 1
                Totalizador += FracData.DblVFrac("Caudal_" + ConnectedFracsID(i).ToString() + "_Acum")
            Next

            LastTime = Now
        End If
    End Sub
#End Region

#Region "Actualizacion de valores a graficar"
    Private Sub TmrGraph_Tick(sender As Object, e As EventArgs) Handles TmrGraph.Tick
        Dim UpdateGraphTask As New Task(AddressOf UGData)
        UpdateGraphTask.Start()
    End Sub

    Private Sub UGData()
        Try
            Dim Data(NGraphData * ConnectedFracsID.Count - 1) As String
            For j = 0 To ConnectedFracsID.Count - 1
                For i = 0 To NGraphData - 1
                    If GraphSend(i) = "MarchaActual_" Then
                        If IsInteger(FracData.StrVFrac(GraphSend(i) + ConnectedFracsID(j).ToString())) = False Then
                            Data(i + j * NGraphData) = ("0")
                        Else
                            Data(i + j * NGraphData) = FracData.StrVFrac(GraphSend(i) + ConnectedFracsID(j).ToString())
                        End If
                    Else
                        Data(i + j * NGraphData) = FracData.DblVFrac(GraphSend(i) + ConnectedFracsID(j).ToString())
                    End If
                Next
            Next

            SGraphConnect.Values = Data

        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
#End Region


End Class
