﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OneFrac_1280x840
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OneFrac_1280x840))
        Me.BtnErrores = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnFC = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnDiagnostico = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel11 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnEmpezarTrabajo = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnSemiAuto = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnEncenderLuces = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel11 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblOpciones = New System.Windows.Forms.Label()
        Me.Guna2Panel10 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.LblCaudalActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel10 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCaudal = New System.Windows.Forms.Label()
        Me.Guna2Panel7 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.LblBombaHHP = New System.Windows.Forms.Label()
        Me.LblHHP = New System.Windows.Forms.Label()
        Me.LblBombaCargaMotor = New System.Windows.Forms.Label()
        Me.LblBombaPresSuccion = New System.Windows.Forms.Label()
        Me.LblCargaMotor = New System.Windows.Forms.Label()
        Me.LblPresSuc = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.LblBombaHorasMotor = New System.Windows.Forms.Label()
        Me.LblHM = New System.Windows.Forms.Label()
        Me.LblBombaPresLUB = New System.Windows.Forms.Label()
        Me.LblBombaTempLUB = New System.Windows.Forms.Label()
        Me.LblPresLub = New System.Windows.Forms.Label()
        Me.LblTempLub = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel7 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblBomba = New System.Windows.Forms.Label()
        Me.Guna2Panel5 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.LblTransRpmOut = New System.Windows.Forms.Label()
        Me.LblTrans5 = New System.Windows.Forms.Label()
        Me.LblTransPresion = New System.Windows.Forms.Label()
        Me.LblTransTemp = New System.Windows.Forms.Label()
        Me.LblTrans3 = New System.Windows.Forms.Label()
        Me.LblTrans1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel5 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTransmision = New System.Windows.Forms.Label()
        Me.Guna2Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.LblMotorCombustibleP100 = New System.Windows.Forms.Label()
        Me.LblCombustible1 = New System.Windows.Forms.Label()
        Me.LblMotorCombustible = New System.Windows.Forms.Label()
        Me.LblMotorTempAceite = New System.Windows.Forms.Label()
        Me.LblCombustible = New System.Windows.Forms.Label()
        Me.LblTempAceite = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.LblMotorVoltaje = New System.Windows.Forms.Label()
        Me.LblVoltaje = New System.Windows.Forms.Label()
        Me.LblMotorPresion = New System.Windows.Forms.Label()
        Me.LblMotorTempAgua = New System.Windows.Forms.Label()
        Me.LblPresion = New System.Windows.Forms.Label()
        Me.LblTempAgua = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMotor = New System.Windows.Forms.Label()
        Me.TmrReconnect = New System.Windows.Forms.Timer(Me.components)
        Me.TmrErrorHandle = New System.Windows.Forms.Timer(Me.components)
        Me.BtnIdle = New Guna.UI2.WinForms.Guna2Button()
        Me.LblRPMDeseada = New System.Windows.Forms.Label()
        Me.LblRPMActual = New System.Windows.Forms.Label()
        Me.LblRPM1 = New System.Windows.Forms.Label()
        Me.LblRPM3 = New System.Windows.Forms.Label()
        Me.RgRPM = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMarchaActual = New System.Windows.Forms.Label()
        Me.LblMarchaDeseada = New System.Windows.Forms.Label()
        Me.LblMarcha3 = New System.Windows.Forms.Label()
        Me.LblMarcha1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMarcha = New System.Windows.Forms.Label()
        Me.BtnTripZero = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnTrip = New Guna.UI2.WinForms.Guna2Button()
        Me.LblPCorteActual = New System.Windows.Forms.Label()
        Me.LblPCorte3 = New System.Windows.Forms.Label()
        Me.LblPCorteDeseada = New System.Windows.Forms.Label()
        Me.LblPCorte1 = New System.Windows.Forms.Label()
        Me.RgPresion = New Guna.UI2.WinForms.Guna2RadialGauge()
        Me.Guna2Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnRpmMinus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmMinus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel3 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnMinusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPlusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.PbAutomatic = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.BtnAjustes = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnStartEngine = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnResetRPM = New Guna.UI2.WinForms.Guna2Button()
        Me.TmrOp = New System.Windows.Forms.Timer(Me.components)
        Me.TmrCheck = New System.Windows.Forms.Timer(Me.components)
        Me.TmrRead = New System.Windows.Forms.Timer(Me.components)
        Me.TmrBackUp = New System.Windows.Forms.Timer(Me.components)
        Me.BtnEKill = New Guna.UI2.WinForms.Guna2Button()
        Me.LblErrorAdmisible = New System.Windows.Forms.Label()
        Me.LblErrorGrave = New System.Windows.Forms.Label()
        Me.Guna2Panel11.SuspendLayout()
        Me.Guna2CustomGradientPanel11.SuspendLayout()
        Me.Guna2Panel10.SuspendLayout()
        Me.Guna2CustomGradientPanel10.SuspendLayout()
        Me.Guna2Panel7.SuspendLayout()
        Me.Guna2CustomGradientPanel7.SuspendLayout()
        Me.Guna2Panel5.SuspendLayout()
        Me.Guna2CustomGradientPanel5.SuspendLayout()
        Me.Guna2Panel4.SuspendLayout()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.Guna2Panel2.SuspendLayout()
        Me.Guna2Panel3.SuspendLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BtnErrores
        '
        Me.BtnErrores.Animated = True
        Me.BtnErrores.BorderColor = System.Drawing.Color.White
        Me.BtnErrores.BorderRadius = 5
        Me.BtnErrores.BorderThickness = 1
        Me.BtnErrores.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnErrores.FillColor = System.Drawing.Color.Transparent
        Me.BtnErrores.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnErrores.ForeColor = System.Drawing.Color.White
        Me.BtnErrores.Location = New System.Drawing.Point(556, 104)
        Me.BtnErrores.Name = "BtnErrores"
        Me.BtnErrores.Size = New System.Drawing.Size(97, 23)
        Me.BtnErrores.TabIndex = 78
        Me.BtnErrores.Text = "Errores"
        '
        'BtnFC
        '
        Me.BtnFC.Animated = True
        Me.BtnFC.BorderColor = System.Drawing.Color.White
        Me.BtnFC.BorderRadius = 5
        Me.BtnFC.BorderThickness = 1
        Me.BtnFC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFC.FillColor = System.Drawing.Color.Transparent
        Me.BtnFC.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnFC.ForeColor = System.Drawing.Color.White
        Me.BtnFC.Location = New System.Drawing.Point(1139, 383)
        Me.BtnFC.Name = "BtnFC"
        Me.BtnFC.Size = New System.Drawing.Size(45, 45)
        Me.BtnFC.TabIndex = 77
        Me.BtnFC.Text = "FC"
        '
        'BtnDiagnostico
        '
        Me.BtnDiagnostico.Animated = True
        Me.BtnDiagnostico.BorderColor = System.Drawing.Color.White
        Me.BtnDiagnostico.BorderRadius = 5
        Me.BtnDiagnostico.BorderThickness = 1
        Me.BtnDiagnostico.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDiagnostico.FillColor = System.Drawing.Color.Transparent
        Me.BtnDiagnostico.Font = New System.Drawing.Font("Montserrat", 13.0!)
        Me.BtnDiagnostico.ForeColor = System.Drawing.Color.White
        Me.BtnDiagnostico.Location = New System.Drawing.Point(520, 141)
        Me.BtnDiagnostico.Name = "BtnDiagnostico"
        Me.BtnDiagnostico.Size = New System.Drawing.Size(168, 45)
        Me.BtnDiagnostico.TabIndex = 76
        Me.BtnDiagnostico.Text = "Diagnóstico"
        '
        'Guna2Panel11
        '
        Me.Guna2Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.BorderRadius = 6
        Me.Guna2Panel11.BorderThickness = 1
        Me.Guna2Panel11.Controls.Add(Me.BtnEmpezarTrabajo)
        Me.Guna2Panel11.Controls.Add(Me.BtnSemiAuto)
        Me.Guna2Panel11.Controls.Add(Me.BtnEncenderLuces)
        Me.Guna2Panel11.Controls.Add(Me.Guna2CustomGradientPanel11)
        Me.Guna2Panel11.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.Location = New System.Drawing.Point(12, 359)
        Me.Guna2Panel11.Name = "Guna2Panel11"
        Me.Guna2Panel11.Size = New System.Drawing.Size(330, 69)
        Me.Guna2Panel11.TabIndex = 79
        '
        'BtnEmpezarTrabajo
        '
        Me.BtnEmpezarTrabajo.Animated = True
        Me.BtnEmpezarTrabajo.BorderColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.BorderRadius = 5
        Me.BtnEmpezarTrabajo.BorderThickness = 1
        Me.BtnEmpezarTrabajo.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEmpezarTrabajo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEmpezarTrabajo.FillColor = System.Drawing.Color.Transparent
        Me.BtnEmpezarTrabajo.Font = New System.Drawing.Font("Montserrat", 8.0!)
        Me.BtnEmpezarTrabajo.ForeColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.Location = New System.Drawing.Point(9, 30)
        Me.BtnEmpezarTrabajo.Name = "BtnEmpezarTrabajo"
        Me.BtnEmpezarTrabajo.Size = New System.Drawing.Size(90, 35)
        Me.BtnEmpezarTrabajo.TabIndex = 54
        Me.BtnEmpezarTrabajo.Text = "Empezar Trabajo"
        '
        'BtnSemiAuto
        '
        Me.BtnSemiAuto.Animated = True
        Me.BtnSemiAuto.BorderColor = System.Drawing.Color.White
        Me.BtnSemiAuto.BorderRadius = 5
        Me.BtnSemiAuto.BorderThickness = 1
        Me.BtnSemiAuto.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSemiAuto.FillColor = System.Drawing.Color.Transparent
        Me.BtnSemiAuto.Font = New System.Drawing.Font("Montserrat", 8.0!)
        Me.BtnSemiAuto.ForeColor = System.Drawing.Color.White
        Me.BtnSemiAuto.Location = New System.Drawing.Point(203, 30)
        Me.BtnSemiAuto.Name = "BtnSemiAuto"
        Me.BtnSemiAuto.Size = New System.Drawing.Size(124, 35)
        Me.BtnSemiAuto.TabIndex = 53
        Me.BtnSemiAuto.Text = "Modo Semi-Automatico"
        '
        'BtnEncenderLuces
        '
        Me.BtnEncenderLuces.Animated = True
        Me.BtnEncenderLuces.BorderColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.BorderRadius = 5
        Me.BtnEncenderLuces.BorderThickness = 1
        Me.BtnEncenderLuces.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEncenderLuces.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEncenderLuces.FillColor = System.Drawing.Color.Transparent
        Me.BtnEncenderLuces.Font = New System.Drawing.Font("Montserrat", 8.0!)
        Me.BtnEncenderLuces.ForeColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.Location = New System.Drawing.Point(105, 30)
        Me.BtnEncenderLuces.Name = "BtnEncenderLuces"
        Me.BtnEncenderLuces.Size = New System.Drawing.Size(92, 35)
        Me.BtnEncenderLuces.TabIndex = 52
        Me.BtnEncenderLuces.Text = "Encender Luces"
        '
        'Guna2CustomGradientPanel11
        '
        Me.Guna2CustomGradientPanel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.BorderRadius = 6
        Me.Guna2CustomGradientPanel11.Controls.Add(Me.LblOpciones)
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel11.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel11.Name = "Guna2CustomGradientPanel11"
        Me.Guna2CustomGradientPanel11.Size = New System.Drawing.Size(330, 26)
        Me.Guna2CustomGradientPanel11.TabIndex = 19
        '
        'LblOpciones
        '
        Me.LblOpciones.AutoSize = True
        Me.LblOpciones.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblOpciones.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblOpciones.Location = New System.Drawing.Point(122, 3)
        Me.LblOpciones.Name = "LblOpciones"
        Me.LblOpciones.Size = New System.Drawing.Size(81, 20)
        Me.LblOpciones.TabIndex = 19
        Me.LblOpciones.Text = "Operación"
        '
        'Guna2Panel10
        '
        Me.Guna2Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.BorderRadius = 6
        Me.Guna2Panel10.BorderThickness = 1
        Me.Guna2Panel10.Controls.Add(Me.Label81)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudalActual)
        Me.Guna2Panel10.Controls.Add(Me.Guna2CustomGradientPanel10)
        Me.Guna2Panel10.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.Location = New System.Drawing.Point(951, 359)
        Me.Guna2Panel10.Name = "Guna2Panel10"
        Me.Guna2Panel10.Size = New System.Drawing.Size(182, 69)
        Me.Guna2Panel10.TabIndex = 74
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label81.ForeColor = System.Drawing.Color.White
        Me.Label81.Location = New System.Drawing.Point(97, 37)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(70, 20)
        Me.Label81.TabIndex = 23
        Me.Label81.Text = "BBL/Min"
        '
        'LblCaudalActual
        '
        Me.LblCaudalActual.AutoSize = True
        Me.LblCaudalActual.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCaudalActual.ForeColor = System.Drawing.Color.White
        Me.LblCaudalActual.Location = New System.Drawing.Point(40, 37)
        Me.LblCaudalActual.Name = "LblCaudalActual"
        Me.LblCaudalActual.Size = New System.Drawing.Size(14, 20)
        Me.LblCaudalActual.TabIndex = 22
        Me.LblCaudalActual.Text = "-"
        '
        'Guna2CustomGradientPanel10
        '
        Me.Guna2CustomGradientPanel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.BorderRadius = 6
        Me.Guna2CustomGradientPanel10.Controls.Add(Me.LblCaudal)
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel10.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel10.Name = "Guna2CustomGradientPanel10"
        Me.Guna2CustomGradientPanel10.Size = New System.Drawing.Size(182, 26)
        Me.Guna2CustomGradientPanel10.TabIndex = 19
        '
        'LblCaudal
        '
        Me.LblCaudal.AutoSize = True
        Me.LblCaudal.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCaudal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCaudal.Location = New System.Drawing.Point(68, 4)
        Me.LblCaudal.Name = "LblCaudal"
        Me.LblCaudal.Size = New System.Drawing.Size(57, 20)
        Me.LblCaudal.TabIndex = 19
        Me.LblCaudal.Text = "Caudal"
        '
        'Guna2Panel7
        '
        Me.Guna2Panel7.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel7.BorderRadius = 6
        Me.Guna2Panel7.BorderThickness = 1
        Me.Guna2Panel7.Controls.Add(Me.Label54)
        Me.Guna2Panel7.Controls.Add(Me.Label55)
        Me.Guna2Panel7.Controls.Add(Me.Label56)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaHHP)
        Me.Guna2Panel7.Controls.Add(Me.LblHHP)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaCargaMotor)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaPresSuccion)
        Me.Guna2Panel7.Controls.Add(Me.LblCargaMotor)
        Me.Guna2Panel7.Controls.Add(Me.LblPresSuc)
        Me.Guna2Panel7.Controls.Add(Me.Label63)
        Me.Guna2Panel7.Controls.Add(Me.Label64)
        Me.Guna2Panel7.Controls.Add(Me.Label65)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaHorasMotor)
        Me.Guna2Panel7.Controls.Add(Me.LblHM)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaPresLUB)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaTempLUB)
        Me.Guna2Panel7.Controls.Add(Me.LblPresLub)
        Me.Guna2Panel7.Controls.Add(Me.LblTempLub)
        Me.Guna2Panel7.Controls.Add(Me.Guna2CustomGradientPanel7)
        Me.Guna2Panel7.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel7.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel7.Location = New System.Drawing.Point(867, 434)
        Me.Guna2Panel7.Name = "Guna2Panel7"
        Me.Guna2Panel7.Size = New System.Drawing.Size(330, 199)
        Me.Guna2Panel7.TabIndex = 67
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(247, 176)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(41, 20)
        Me.Label54.TabIndex = 37
        Me.Label54.Text = "HHP"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(247, 118)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(21, 20)
        Me.Label55.TabIndex = 36
        Me.Label55.Text = "%"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label56.ForeColor = System.Drawing.Color.White
        Me.Label56.Location = New System.Drawing.Point(247, 59)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(32, 20)
        Me.Label56.TabIndex = 35
        Me.Label56.Text = "PSI"
        '
        'LblBombaHHP
        '
        Me.LblBombaHHP.AutoSize = True
        Me.LblBombaHHP.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaHHP.ForeColor = System.Drawing.Color.White
        Me.LblBombaHHP.Location = New System.Drawing.Point(198, 176)
        Me.LblBombaHHP.Name = "LblBombaHHP"
        Me.LblBombaHHP.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaHHP.TabIndex = 34
        Me.LblBombaHHP.Text = "-"
        '
        'LblHHP
        '
        Me.LblHHP.AutoSize = True
        Me.LblHHP.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblHHP.ForeColor = System.Drawing.Color.White
        Me.LblHHP.Location = New System.Drawing.Point(198, 147)
        Me.LblHHP.Name = "LblHHP"
        Me.LblHHP.Size = New System.Drawing.Size(41, 20)
        Me.LblHHP.TabIndex = 33
        Me.LblHHP.Text = "HHP"
        '
        'LblBombaCargaMotor
        '
        Me.LblBombaCargaMotor.AutoSize = True
        Me.LblBombaCargaMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaCargaMotor.ForeColor = System.Drawing.Color.White
        Me.LblBombaCargaMotor.Location = New System.Drawing.Point(198, 118)
        Me.LblBombaCargaMotor.Name = "LblBombaCargaMotor"
        Me.LblBombaCargaMotor.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaCargaMotor.TabIndex = 32
        Me.LblBombaCargaMotor.Text = "-"
        '
        'LblBombaPresSuccion
        '
        Me.LblBombaPresSuccion.AutoSize = True
        Me.LblBombaPresSuccion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresSuccion.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresSuccion.Location = New System.Drawing.Point(198, 59)
        Me.LblBombaPresSuccion.Name = "LblBombaPresSuccion"
        Me.LblBombaPresSuccion.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaPresSuccion.TabIndex = 31
        Me.LblBombaPresSuccion.Text = "-"
        '
        'LblCargaMotor
        '
        Me.LblCargaMotor.AutoSize = True
        Me.LblCargaMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCargaMotor.ForeColor = System.Drawing.Color.White
        Me.LblCargaMotor.Location = New System.Drawing.Point(198, 87)
        Me.LblCargaMotor.Name = "LblCargaMotor"
        Me.LblCargaMotor.Size = New System.Drawing.Size(114, 20)
        Me.LblCargaMotor.TabIndex = 30
        Me.LblCargaMotor.Text = "% Carga Motor"
        '
        'LblPresSuc
        '
        Me.LblPresSuc.AutoSize = True
        Me.LblPresSuc.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPresSuc.ForeColor = System.Drawing.Color.White
        Me.LblPresSuc.Location = New System.Drawing.Point(198, 30)
        Me.LblPresSuc.Name = "LblPresSuc"
        Me.LblPresSuc.Size = New System.Drawing.Size(122, 20)
        Me.LblPresSuc.TabIndex = 29
        Me.LblPresSuc.Text = "Presión Succión"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label63.ForeColor = System.Drawing.Color.White
        Me.Label63.Location = New System.Drawing.Point(72, 176)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(26, 20)
        Me.Label63.TabIndex = 28
        Me.Label63.Text = "Hr"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(72, 118)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(32, 20)
        Me.Label64.TabIndex = 27
        Me.Label64.Text = "PSI"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(72, 59)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(25, 20)
        Me.Label65.TabIndex = 26
        Me.Label65.Text = "ºC"
        '
        'LblBombaHorasMotor
        '
        Me.LblBombaHorasMotor.AutoSize = True
        Me.LblBombaHorasMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaHorasMotor.ForeColor = System.Drawing.Color.White
        Me.LblBombaHorasMotor.Location = New System.Drawing.Point(13, 176)
        Me.LblBombaHorasMotor.Name = "LblBombaHorasMotor"
        Me.LblBombaHorasMotor.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaHorasMotor.TabIndex = 25
        Me.LblBombaHorasMotor.Text = "-"
        '
        'LblHM
        '
        Me.LblHM.AutoSize = True
        Me.LblHM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblHM.ForeColor = System.Drawing.Color.White
        Me.LblHM.Location = New System.Drawing.Point(13, 147)
        Me.LblHM.Name = "LblHM"
        Me.LblHM.Size = New System.Drawing.Size(117, 20)
        Me.LblHM.TabIndex = 24
        Me.LblHM.Text = "Horas de Motor"
        '
        'LblBombaPresLUB
        '
        Me.LblBombaPresLUB.AutoSize = True
        Me.LblBombaPresLUB.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaPresLUB.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresLUB.Location = New System.Drawing.Point(13, 118)
        Me.LblBombaPresLUB.Name = "LblBombaPresLUB"
        Me.LblBombaPresLUB.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaPresLUB.TabIndex = 23
        Me.LblBombaPresLUB.Text = "-"
        '
        'LblBombaTempLUB
        '
        Me.LblBombaTempLUB.AutoSize = True
        Me.LblBombaTempLUB.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBombaTempLUB.ForeColor = System.Drawing.Color.White
        Me.LblBombaTempLUB.Location = New System.Drawing.Point(13, 59)
        Me.LblBombaTempLUB.Name = "LblBombaTempLUB"
        Me.LblBombaTempLUB.Size = New System.Drawing.Size(14, 20)
        Me.LblBombaTempLUB.TabIndex = 22
        Me.LblBombaTempLUB.Text = "-"
        '
        'LblPresLub
        '
        Me.LblPresLub.AutoSize = True
        Me.LblPresLub.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPresLub.ForeColor = System.Drawing.Color.White
        Me.LblPresLub.Location = New System.Drawing.Point(13, 87)
        Me.LblPresLub.Name = "LblPresLub"
        Me.LblPresLub.Size = New System.Drawing.Size(96, 20)
        Me.LblPresLub.TabIndex = 21
        Me.LblPresLub.Text = "Presión LUB"
        '
        'LblTempLub
        '
        Me.LblTempLub.AutoSize = True
        Me.LblTempLub.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTempLub.ForeColor = System.Drawing.Color.White
        Me.LblTempLub.Location = New System.Drawing.Point(13, 30)
        Me.LblTempLub.Name = "LblTempLub"
        Me.LblTempLub.Size = New System.Drawing.Size(131, 20)
        Me.LblTempLub.TabIndex = 20
        Me.LblTempLub.Text = "Temperatura LUB"
        '
        'Guna2CustomGradientPanel7
        '
        Me.Guna2CustomGradientPanel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.BorderRadius = 6
        Me.Guna2CustomGradientPanel7.Controls.Add(Me.LblBomba)
        Me.Guna2CustomGradientPanel7.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel7.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel7.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel7.Name = "Guna2CustomGradientPanel7"
        Me.Guna2CustomGradientPanel7.Size = New System.Drawing.Size(330, 26)
        Me.Guna2CustomGradientPanel7.TabIndex = 19
        '
        'LblBomba
        '
        Me.LblBomba.AutoSize = True
        Me.LblBomba.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblBomba.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblBomba.Location = New System.Drawing.Point(123, 4)
        Me.LblBomba.Name = "LblBomba"
        Me.LblBomba.Size = New System.Drawing.Size(61, 20)
        Me.LblBomba.TabIndex = 19
        Me.LblBomba.Text = "Bomba"
        '
        'Guna2Panel5
        '
        Me.Guna2Panel5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.BorderRadius = 6
        Me.Guna2Panel5.BorderThickness = 1
        Me.Guna2Panel5.Controls.Add(Me.Label41)
        Me.Guna2Panel5.Controls.Add(Me.Label42)
        Me.Guna2Panel5.Controls.Add(Me.Label43)
        Me.Guna2Panel5.Controls.Add(Me.LblTransRpmOut)
        Me.Guna2Panel5.Controls.Add(Me.LblTrans5)
        Me.Guna2Panel5.Controls.Add(Me.LblTransPresion)
        Me.Guna2Panel5.Controls.Add(Me.LblTransTemp)
        Me.Guna2Panel5.Controls.Add(Me.LblTrans3)
        Me.Guna2Panel5.Controls.Add(Me.LblTrans1)
        Me.Guna2Panel5.Controls.Add(Me.Guna2CustomGradientPanel5)
        Me.Guna2Panel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.Location = New System.Drawing.Point(445, 434)
        Me.Guna2Panel5.Name = "Guna2Panel5"
        Me.Guna2Panel5.Size = New System.Drawing.Size(330, 199)
        Me.Guna2Panel5.TabIndex = 65
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(204, 175)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(42, 20)
        Me.Label41.TabIndex = 28
        Me.Label41.Text = "RPM"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(204, 117)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(32, 20)
        Me.Label42.TabIndex = 27
        Me.Label42.Text = "PSI"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label43.ForeColor = System.Drawing.Color.White
        Me.Label43.Location = New System.Drawing.Point(204, 58)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(25, 20)
        Me.Label43.TabIndex = 26
        Me.Label43.Text = "ºC"
        '
        'LblTransRpmOut
        '
        Me.LblTransRpmOut.AutoSize = True
        Me.LblTransRpmOut.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransRpmOut.ForeColor = System.Drawing.Color.White
        Me.LblTransRpmOut.Location = New System.Drawing.Point(116, 175)
        Me.LblTransRpmOut.Name = "LblTransRpmOut"
        Me.LblTransRpmOut.Size = New System.Drawing.Size(14, 20)
        Me.LblTransRpmOut.TabIndex = 25
        Me.LblTransRpmOut.Text = "-"
        '
        'LblTrans5
        '
        Me.LblTrans5.AutoSize = True
        Me.LblTrans5.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTrans5.ForeColor = System.Drawing.Color.White
        Me.LblTrans5.Location = New System.Drawing.Point(116, 146)
        Me.LblTrans5.Name = "LblTrans5"
        Me.LblTrans5.Size = New System.Drawing.Size(73, 20)
        Me.LblTrans5.TabIndex = 24
        Me.LblTrans5.Text = "RPM Out"
        '
        'LblTransPresion
        '
        Me.LblTransPresion.AutoSize = True
        Me.LblTransPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransPresion.ForeColor = System.Drawing.Color.White
        Me.LblTransPresion.Location = New System.Drawing.Point(116, 117)
        Me.LblTransPresion.Name = "LblTransPresion"
        Me.LblTransPresion.Size = New System.Drawing.Size(14, 20)
        Me.LblTransPresion.TabIndex = 23
        Me.LblTransPresion.Text = "-"
        '
        'LblTransTemp
        '
        Me.LblTransTemp.AutoSize = True
        Me.LblTransTemp.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransTemp.ForeColor = System.Drawing.Color.White
        Me.LblTransTemp.Location = New System.Drawing.Point(116, 58)
        Me.LblTransTemp.Name = "LblTransTemp"
        Me.LblTransTemp.Size = New System.Drawing.Size(14, 20)
        Me.LblTransTemp.TabIndex = 22
        Me.LblTransTemp.Text = "-"
        '
        'LblTrans3
        '
        Me.LblTrans3.AutoSize = True
        Me.LblTrans3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTrans3.ForeColor = System.Drawing.Color.White
        Me.LblTrans3.Location = New System.Drawing.Point(116, 86)
        Me.LblTrans3.Name = "LblTrans3"
        Me.LblTrans3.Size = New System.Drawing.Size(62, 20)
        Me.LblTrans3.TabIndex = 21
        Me.LblTrans3.Text = "Presión"
        '
        'LblTrans1
        '
        Me.LblTrans1.AutoSize = True
        Me.LblTrans1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTrans1.ForeColor = System.Drawing.Color.White
        Me.LblTrans1.Location = New System.Drawing.Point(116, 29)
        Me.LblTrans1.Name = "LblTrans1"
        Me.LblTrans1.Size = New System.Drawing.Size(97, 20)
        Me.LblTrans1.TabIndex = 20
        Me.LblTrans1.Text = "Temperatura"
        '
        'Guna2CustomGradientPanel5
        '
        Me.Guna2CustomGradientPanel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.BorderRadius = 6
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.LblTransmision)
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel5.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel5.Name = "Guna2CustomGradientPanel5"
        Me.Guna2CustomGradientPanel5.Size = New System.Drawing.Size(330, 26)
        Me.Guna2CustomGradientPanel5.TabIndex = 19
        '
        'LblTransmision
        '
        Me.LblTransmision.AutoSize = True
        Me.LblTransmision.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTransmision.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTransmision.Location = New System.Drawing.Point(123, 4)
        Me.LblTransmision.Name = "LblTransmision"
        Me.LblTransmision.Size = New System.Drawing.Size(94, 20)
        Me.LblTransmision.TabIndex = 19
        Me.LblTransmision.Text = "Transmisión"
        '
        'Guna2Panel4
        '
        Me.Guna2Panel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.BorderRadius = 6
        Me.Guna2Panel4.BorderThickness = 1
        Me.Guna2Panel4.Controls.Add(Me.Label23)
        Me.Guna2Panel4.Controls.Add(Me.Label24)
        Me.Guna2Panel4.Controls.Add(Me.Label25)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorCombustibleP100)
        Me.Guna2Panel4.Controls.Add(Me.LblCombustible1)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorCombustible)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorTempAceite)
        Me.Guna2Panel4.Controls.Add(Me.LblCombustible)
        Me.Guna2Panel4.Controls.Add(Me.LblTempAceite)
        Me.Guna2Panel4.Controls.Add(Me.Label22)
        Me.Guna2Panel4.Controls.Add(Me.Label21)
        Me.Guna2Panel4.Controls.Add(Me.Label20)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorVoltaje)
        Me.Guna2Panel4.Controls.Add(Me.LblVoltaje)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorPresion)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorTempAgua)
        Me.Guna2Panel4.Controls.Add(Me.LblPresion)
        Me.Guna2Panel4.Controls.Add(Me.LblTempAgua)
        Me.Guna2Panel4.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.Guna2Panel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.Location = New System.Drawing.Point(12, 434)
        Me.Guna2Panel4.Name = "Guna2Panel4"
        Me.Guna2Panel4.Size = New System.Drawing.Size(330, 199)
        Me.Guna2Panel4.TabIndex = 63
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(266, 174)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(21, 20)
        Me.Label23.TabIndex = 37
        Me.Label23.Text = "%"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(266, 116)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(39, 20)
        Me.Label24.TabIndex = 36
        Me.Label24.Text = "L/Hr"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(266, 57)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(25, 20)
        Me.Label25.TabIndex = 35
        Me.Label25.Text = "ºC"
        '
        'LblMotorCombustibleP100
        '
        Me.LblMotorCombustibleP100.AutoSize = True
        Me.LblMotorCombustibleP100.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCombustibleP100.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustibleP100.Location = New System.Drawing.Point(200, 174)
        Me.LblMotorCombustibleP100.Name = "LblMotorCombustibleP100"
        Me.LblMotorCombustibleP100.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorCombustibleP100.TabIndex = 34
        Me.LblMotorCombustibleP100.Text = "-"
        '
        'LblCombustible1
        '
        Me.LblCombustible1.AutoSize = True
        Me.LblCombustible1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCombustible1.ForeColor = System.Drawing.Color.White
        Me.LblCombustible1.Location = New System.Drawing.Point(200, 145)
        Me.LblCombustible1.Name = "LblCombustible1"
        Me.LblCombustible1.Size = New System.Drawing.Size(99, 20)
        Me.LblCombustible1.TabIndex = 33
        Me.LblCombustible1.Text = "Combustible"
        '
        'LblMotorCombustible
        '
        Me.LblMotorCombustible.AutoSize = True
        Me.LblMotorCombustible.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorCombustible.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustible.Location = New System.Drawing.Point(200, 116)
        Me.LblMotorCombustible.Name = "LblMotorCombustible"
        Me.LblMotorCombustible.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorCombustible.TabIndex = 32
        Me.LblMotorCombustible.Text = "-"
        '
        'LblMotorTempAceite
        '
        Me.LblMotorTempAceite.AutoSize = True
        Me.LblMotorTempAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAceite.Location = New System.Drawing.Point(200, 57)
        Me.LblMotorTempAceite.Name = "LblMotorTempAceite"
        Me.LblMotorTempAceite.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorTempAceite.TabIndex = 31
        Me.LblMotorTempAceite.Text = "-"
        '
        'LblCombustible
        '
        Me.LblCombustible.AutoSize = True
        Me.LblCombustible.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCombustible.ForeColor = System.Drawing.Color.White
        Me.LblCombustible.Location = New System.Drawing.Point(200, 85)
        Me.LblCombustible.Name = "LblCombustible"
        Me.LblCombustible.Size = New System.Drawing.Size(99, 20)
        Me.LblCombustible.TabIndex = 30
        Me.LblCombustible.Text = "Combustible"
        '
        'LblTempAceite
        '
        Me.LblTempAceite.AutoSize = True
        Me.LblTempAceite.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblTempAceite.Location = New System.Drawing.Point(200, 28)
        Me.LblTempAceite.Name = "LblTempAceite"
        Me.LblTempAceite.Size = New System.Drawing.Size(96, 20)
        Me.LblTempAceite.TabIndex = 29
        Me.LblTempAceite.Text = "Temp Aceite"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(78, 175)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(19, 20)
        Me.Label22.TabIndex = 28
        Me.Label22.Text = "V"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(78, 117)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(32, 20)
        Me.Label21.TabIndex = 27
        Me.Label21.Text = "PSI"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(78, 58)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(25, 20)
        Me.Label20.TabIndex = 26
        Me.Label20.Text = "ºC"
        '
        'LblMotorVoltaje
        '
        Me.LblMotorVoltaje.AutoSize = True
        Me.LblMotorVoltaje.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorVoltaje.ForeColor = System.Drawing.Color.White
        Me.LblMotorVoltaje.Location = New System.Drawing.Point(18, 175)
        Me.LblMotorVoltaje.Name = "LblMotorVoltaje"
        Me.LblMotorVoltaje.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorVoltaje.TabIndex = 25
        Me.LblMotorVoltaje.Text = "-"
        '
        'LblVoltaje
        '
        Me.LblVoltaje.AutoSize = True
        Me.LblVoltaje.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblVoltaje.ForeColor = System.Drawing.Color.White
        Me.LblVoltaje.Location = New System.Drawing.Point(18, 146)
        Me.LblVoltaje.Name = "LblVoltaje"
        Me.LblVoltaje.Size = New System.Drawing.Size(57, 20)
        Me.LblVoltaje.TabIndex = 24
        Me.LblVoltaje.Text = "Voltaje"
        '
        'LblMotorPresion
        '
        Me.LblMotorPresion.AutoSize = True
        Me.LblMotorPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorPresion.ForeColor = System.Drawing.Color.White
        Me.LblMotorPresion.Location = New System.Drawing.Point(18, 117)
        Me.LblMotorPresion.Name = "LblMotorPresion"
        Me.LblMotorPresion.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorPresion.TabIndex = 23
        Me.LblMotorPresion.Text = "-"
        '
        'LblMotorTempAgua
        '
        Me.LblMotorTempAgua.AutoSize = True
        Me.LblMotorTempAgua.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotorTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAgua.Location = New System.Drawing.Point(18, 59)
        Me.LblMotorTempAgua.Name = "LblMotorTempAgua"
        Me.LblMotorTempAgua.Size = New System.Drawing.Size(14, 20)
        Me.LblMotorTempAgua.TabIndex = 22
        Me.LblMotorTempAgua.Text = "-"
        '
        'LblPresion
        '
        Me.LblPresion.AutoSize = True
        Me.LblPresion.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPresion.ForeColor = System.Drawing.Color.White
        Me.LblPresion.Location = New System.Drawing.Point(18, 86)
        Me.LblPresion.Name = "LblPresion"
        Me.LblPresion.Size = New System.Drawing.Size(62, 20)
        Me.LblPresion.TabIndex = 21
        Me.LblPresion.Text = "Presión"
        '
        'LblTempAgua
        '
        Me.LblTempAgua.AutoSize = True
        Me.LblTempAgua.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblTempAgua.Location = New System.Drawing.Point(18, 29)
        Me.LblTempAgua.Name = "LblTempAgua"
        Me.LblTempAgua.Size = New System.Drawing.Size(89, 20)
        Me.LblTempAgua.TabIndex = 20
        Me.LblTempAgua.Text = "Temp Agua"
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.LblMotor)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(330, 26)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'LblMotor
        '
        Me.LblMotor.AutoSize = True
        Me.LblMotor.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMotor.Location = New System.Drawing.Point(123, 4)
        Me.LblMotor.Name = "LblMotor"
        Me.LblMotor.Size = New System.Drawing.Size(52, 20)
        Me.LblMotor.TabIndex = 19
        Me.LblMotor.Text = "Motor"
        '
        'TmrReconnect
        '
        Me.TmrReconnect.Interval = 1000
        '
        'TmrErrorHandle
        '
        Me.TmrErrorHandle.Interval = 600
        '
        'BtnIdle
        '
        Me.BtnIdle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnIdle.Animated = True
        Me.BtnIdle.BorderColor = System.Drawing.Color.White
        Me.BtnIdle.BorderRadius = 5
        Me.BtnIdle.BorderThickness = 1
        Me.BtnIdle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdle.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdle.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnIdle.ForeColor = System.Drawing.Color.White
        Me.BtnIdle.Location = New System.Drawing.Point(111, 267)
        Me.BtnIdle.Name = "BtnIdle"
        Me.BtnIdle.Size = New System.Drawing.Size(138, 40)
        Me.BtnIdle.TabIndex = 146
        Me.BtnIdle.Text = "IDLE"
        '
        'LblRPMDeseada
        '
        Me.LblRPMDeseada.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPMDeseada.AutoSize = True
        Me.LblRPMDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblRPMDeseada.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblRPMDeseada.ForeColor = System.Drawing.Color.White
        Me.LblRPMDeseada.Location = New System.Drawing.Point(81, 200)
        Me.LblRPMDeseada.Name = "LblRPMDeseada"
        Me.LblRPMDeseada.Size = New System.Drawing.Size(27, 37)
        Me.LblRPMDeseada.TabIndex = 144
        Me.LblRPMDeseada.Text = "-"
        '
        'LblRPMActual
        '
        Me.LblRPMActual.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPMActual.AutoSize = True
        Me.LblRPMActual.Font = New System.Drawing.Font("Montserrat", 30.0!)
        Me.LblRPMActual.ForeColor = System.Drawing.Color.White
        Me.LblRPMActual.Location = New System.Drawing.Point(159, 97)
        Me.LblRPMActual.Name = "LblRPMActual"
        Me.LblRPMActual.Size = New System.Drawing.Size(39, 55)
        Me.LblRPMActual.TabIndex = 145
        Me.LblRPMActual.Text = "-"
        '
        'LblRPM1
        '
        Me.LblRPM1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPM1.AutoSize = True
        Me.LblRPM1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM1.ForeColor = System.Drawing.Color.White
        Me.LblRPM1.Location = New System.Drawing.Point(55, 173)
        Me.LblRPM1.Name = "LblRPM1"
        Me.LblRPM1.Size = New System.Drawing.Size(113, 20)
        Me.LblRPM1.TabIndex = 142
        Me.LblRPM1.Text = "RPM Deseadas"
        '
        'LblRPM3
        '
        Me.LblRPM3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LblRPM3.AutoSize = True
        Me.LblRPM3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblRPM3.ForeColor = System.Drawing.Color.White
        Me.LblRPM3.Location = New System.Drawing.Point(127, 67)
        Me.LblRPM3.Name = "LblRPM3"
        Me.LblRPM3.Size = New System.Drawing.Size(106, 20)
        Me.LblRPM3.TabIndex = 143
        Me.LblRPM3.Text = "RPM Actuales"
        '
        'RgRPM
        '
        Me.RgRPM.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgRPM.ArrowColor = System.Drawing.Color.Silver
        Me.RgRPM.ArrowThickness = 0
        Me.RgRPM.ArrowVisible = False
        Me.RgRPM.BackColor = System.Drawing.Color.Transparent
        Me.RgRPM.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgRPM.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgRPM.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgRPM.Location = New System.Drawing.Point(1, 1)
        Me.RgRPM.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgRPM.Name = "RgRPM"
        Me.RgRPM.ProgressColor = System.Drawing.Color.DeepSkyBlue
        Me.RgRPM.ProgressColor2 = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.RgRPM.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPM.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgRPM.ProgressThickness = 12
        Me.RgRPM.ShowPercentage = False
        Me.RgRPM.Size = New System.Drawing.Size(358, 358)
        Me.RgRPM.TabIndex = 151
        Me.RgRPM.UseTransparentBackground = True
        Me.RgRPM.Value = 100
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaActual)
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaDeseada)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha3)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(520, 193)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(168, 156)
        Me.Guna2Panel1.TabIndex = 152
        '
        'LblMarchaActual
        '
        Me.LblMarchaActual.AutoSize = True
        Me.LblMarchaActual.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarchaActual.ForeColor = System.Drawing.Color.White
        Me.LblMarchaActual.Location = New System.Drawing.Point(76, 131)
        Me.LblMarchaActual.Name = "LblMarchaActual"
        Me.LblMarchaActual.Size = New System.Drawing.Size(20, 20)
        Me.LblMarchaActual.TabIndex = 23
        Me.LblMarchaActual.Text = "N"
        '
        'LblMarchaDeseada
        '
        Me.LblMarchaDeseada.AutoSize = True
        Me.LblMarchaDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblMarchaDeseada.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.LblMarchaDeseada.ForeColor = System.Drawing.Color.White
        Me.LblMarchaDeseada.Location = New System.Drawing.Point(76, 65)
        Me.LblMarchaDeseada.Name = "LblMarchaDeseada"
        Me.LblMarchaDeseada.Size = New System.Drawing.Size(18, 17)
        Me.LblMarchaDeseada.TabIndex = 22
        Me.LblMarchaDeseada.Text = "N"
        '
        'LblMarcha3
        '
        Me.LblMarcha3.AutoSize = True
        Me.LblMarcha3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha3.ForeColor = System.Drawing.Color.White
        Me.LblMarcha3.Location = New System.Drawing.Point(38, 98)
        Me.LblMarcha3.Name = "LblMarcha3"
        Me.LblMarcha3.Size = New System.Drawing.Size(110, 20)
        Me.LblMarcha3.TabIndex = 21
        Me.LblMarcha3.Text = "Marcha Actual"
        '
        'LblMarcha1
        '
        Me.LblMarcha1.AutoSize = True
        Me.LblMarcha1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha1.ForeColor = System.Drawing.Color.White
        Me.LblMarcha1.Location = New System.Drawing.Point(34, 32)
        Me.LblMarcha1.Name = "LblMarcha1"
        Me.LblMarcha1.Size = New System.Drawing.Size(125, 20)
        Me.LblMarcha1.TabIndex = 20
        Me.LblMarcha1.Text = "Marcha Deseada"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblMarcha)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(168, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblMarcha
        '
        Me.LblMarcha.AutoSize = True
        Me.LblMarcha.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblMarcha.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMarcha.Location = New System.Drawing.Point(56, 4)
        Me.LblMarcha.Name = "LblMarcha"
        Me.LblMarcha.Size = New System.Drawing.Size(61, 20)
        Me.LblMarcha.TabIndex = 19
        Me.LblMarcha.Text = "Marcha"
        '
        'BtnTripZero
        '
        Me.BtnTripZero.Animated = True
        Me.BtnTripZero.BorderColor = System.Drawing.Color.White
        Me.BtnTripZero.BorderRadius = 5
        Me.BtnTripZero.BorderThickness = 1
        Me.BtnTripZero.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTripZero.FillColor = System.Drawing.Color.Transparent
        Me.BtnTripZero.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnTripZero.ForeColor = System.Drawing.Color.White
        Me.BtnTripZero.Location = New System.Drawing.Point(125, 147)
        Me.BtnTripZero.Name = "BtnTripZero"
        Me.BtnTripZero.Size = New System.Drawing.Size(111, 40)
        Me.BtnTripZero.TabIndex = 161
        Me.BtnTripZero.Text = "ZERO"
        '
        'BtnTrip
        '
        Me.BtnTrip.Animated = True
        Me.BtnTrip.BorderColor = System.Drawing.Color.White
        Me.BtnTrip.BorderRadius = 5
        Me.BtnTrip.BorderThickness = 1
        Me.BtnTrip.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTrip.FillColor = System.Drawing.Color.Transparent
        Me.BtnTrip.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnTrip.ForeColor = System.Drawing.Color.White
        Me.BtnTrip.Location = New System.Drawing.Point(125, 267)
        Me.BtnTrip.Name = "BtnTrip"
        Me.BtnTrip.Size = New System.Drawing.Size(111, 40)
        Me.BtnTrip.TabIndex = 160
        Me.BtnTrip.Text = "TRIP"
        '
        'LblPCorteActual
        '
        Me.LblPCorteActual.AutoSize = True
        Me.LblPCorteActual.Font = New System.Drawing.Font("Montserrat", 30.0!)
        Me.LblPCorteActual.ForeColor = System.Drawing.Color.White
        Me.LblPCorteActual.Location = New System.Drawing.Point(159, 89)
        Me.LblPCorteActual.Name = "LblPCorteActual"
        Me.LblPCorteActual.Size = New System.Drawing.Size(39, 55)
        Me.LblPCorteActual.TabIndex = 159
        Me.LblPCorteActual.Text = "-"
        '
        'LblPCorte3
        '
        Me.LblPCorte3.AutoSize = True
        Me.LblPCorte3.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPCorte3.ForeColor = System.Drawing.Color.White
        Me.LblPCorte3.Location = New System.Drawing.Point(125, 59)
        Me.LblPCorte3.Name = "LblPCorte3"
        Me.LblPCorte3.Size = New System.Drawing.Size(111, 20)
        Me.LblPCorte3.TabIndex = 157
        Me.LblPCorte3.Text = "Presión Actual"
        '
        'LblPCorteDeseada
        '
        Me.LblPCorteDeseada.AutoSize = True
        Me.LblPCorteDeseada.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblPCorteDeseada.ForeColor = System.Drawing.Color.White
        Me.LblPCorteDeseada.Location = New System.Drawing.Point(166, 225)
        Me.LblPCorteDeseada.Name = "LblPCorteDeseada"
        Me.LblPCorteDeseada.Size = New System.Drawing.Size(27, 37)
        Me.LblPCorteDeseada.TabIndex = 158
        Me.LblPCorteDeseada.Text = "-"
        '
        'LblPCorte1
        '
        Me.LblPCorte1.AutoSize = True
        Me.LblPCorte1.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblPCorte1.ForeColor = System.Drawing.Color.White
        Me.LblPCorte1.Location = New System.Drawing.Point(117, 205)
        Me.LblPCorte1.Name = "LblPCorte1"
        Me.LblPCorte1.Size = New System.Drawing.Size(126, 20)
        Me.LblPCorte1.TabIndex = 156
        Me.LblPCorte1.Text = "Presión de Corte"
        '
        'RgPresion
        '
        Me.RgPresion.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.RgPresion.ArrowColor = System.Drawing.Color.Silver
        Me.RgPresion.ArrowThickness = 0
        Me.RgPresion.ArrowVisible = False
        Me.RgPresion.BackColor = System.Drawing.Color.Transparent
        Me.RgPresion.FillColor = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(137, Byte), Integer), CType(CType(149, Byte), Integer))
        Me.RgPresion.Font = New System.Drawing.Font("Verdana", 8.2!)
        Me.RgPresion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(166, Byte), Integer))
        Me.RgPresion.Location = New System.Drawing.Point(1, 1)
        Me.RgPresion.MinimumSize = New System.Drawing.Size(30, 30)
        Me.RgPresion.Name = "RgPresion"
        Me.RgPresion.ProgressColor = System.Drawing.Color.Lime
        Me.RgPresion.ProgressColor2 = System.Drawing.Color.DarkGreen
        Me.RgPresion.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresion.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round
        Me.RgPresion.ProgressThickness = 12
        Me.RgPresion.ShowPercentage = False
        Me.RgPresion.Size = New System.Drawing.Size(358, 358)
        Me.RgPresion.TabIndex = 162
        Me.RgPresion.UseTransparentBackground = True
        Me.RgPresion.Value = 100
        '
        'Guna2Panel2
        '
        Me.Guna2Panel2.Controls.Add(Me.LblRPMActual)
        Me.Guna2Panel2.Controls.Add(Me.BtnRpmMinus50)
        Me.Guna2Panel2.Controls.Add(Me.LblRPMDeseada)
        Me.Guna2Panel2.Controls.Add(Me.BtnRpmPlus25)
        Me.Guna2Panel2.Controls.Add(Me.BtnIdle)
        Me.Guna2Panel2.Controls.Add(Me.LblRPM3)
        Me.Guna2Panel2.Controls.Add(Me.LblRPM1)
        Me.Guna2Panel2.Controls.Add(Me.BtnRpmPlus50)
        Me.Guna2Panel2.Controls.Add(Me.BtnRpmMinus25)
        Me.Guna2Panel2.Controls.Add(Me.RgRPM)
        Me.Guna2Panel2.Location = New System.Drawing.Point(25, 0)
        Me.Guna2Panel2.Name = "Guna2Panel2"
        Me.Guna2Panel2.Size = New System.Drawing.Size(360, 360)
        Me.Guna2Panel2.TabIndex = 163
        '
        'BtnRpmMinus50
        '
        Me.BtnRpmMinus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmMinus50.Animated = True
        Me.BtnRpmMinus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.BorderRadius = 5
        Me.BtnRpmMinus50.BorderThickness = 1
        Me.BtnRpmMinus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.Image = CType(resources.GetObject("BtnRpmMinus50.Image"), System.Drawing.Image)
        Me.BtnRpmMinus50.Location = New System.Drawing.Point(221, 209)
        Me.BtnRpmMinus50.Name = "BtnRpmMinus50"
        Me.BtnRpmMinus50.Size = New System.Drawing.Size(40, 40)
        Me.BtnRpmMinus50.TabIndex = 150
        '
        'BtnRpmPlus25
        '
        Me.BtnRpmPlus25.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmPlus25.Animated = True
        Me.BtnRpmPlus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.BorderRadius = 5
        Me.BtnRpmPlus25.BorderThickness = 1
        Me.BtnRpmPlus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.Image = CType(resources.GetObject("BtnRpmPlus25.Image"), System.Drawing.Image)
        Me.BtnRpmPlus25.Location = New System.Drawing.Point(267, 173)
        Me.BtnRpmPlus25.Name = "BtnRpmPlus25"
        Me.BtnRpmPlus25.Size = New System.Drawing.Size(30, 30)
        Me.BtnRpmPlus25.TabIndex = 147
        '
        'BtnRpmPlus50
        '
        Me.BtnRpmPlus50.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmPlus50.Animated = True
        Me.BtnRpmPlus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.BorderRadius = 5
        Me.BtnRpmPlus50.BorderThickness = 1
        Me.BtnRpmPlus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.Image = CType(resources.GetObject("BtnRpmPlus50.Image"), System.Drawing.Image)
        Me.BtnRpmPlus50.Location = New System.Drawing.Point(221, 163)
        Me.BtnRpmPlus50.Name = "BtnRpmPlus50"
        Me.BtnRpmPlus50.Size = New System.Drawing.Size(40, 40)
        Me.BtnRpmPlus50.TabIndex = 149
        '
        'BtnRpmMinus25
        '
        Me.BtnRpmMinus25.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRpmMinus25.Animated = True
        Me.BtnRpmMinus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.BorderRadius = 5
        Me.BtnRpmMinus25.BorderThickness = 1
        Me.BtnRpmMinus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.Image = CType(resources.GetObject("BtnRpmMinus25.Image"), System.Drawing.Image)
        Me.BtnRpmMinus25.Location = New System.Drawing.Point(267, 209)
        Me.BtnRpmMinus25.Name = "BtnRpmMinus25"
        Me.BtnRpmMinus25.Size = New System.Drawing.Size(30, 30)
        Me.BtnRpmMinus25.TabIndex = 148
        '
        'Guna2Panel3
        '
        Me.Guna2Panel3.Controls.Add(Me.BtnTripZero)
        Me.Guna2Panel3.Controls.Add(Me.LblPCorte1)
        Me.Guna2Panel3.Controls.Add(Me.BtnTrip)
        Me.Guna2Panel3.Controls.Add(Me.LblPCorteDeseada)
        Me.Guna2Panel3.Controls.Add(Me.LblPCorte3)
        Me.Guna2Panel3.Controls.Add(Me.LblPCorteActual)
        Me.Guna2Panel3.Controls.Add(Me.RgPresion)
        Me.Guna2Panel3.Location = New System.Drawing.Point(833, 0)
        Me.Guna2Panel3.Name = "Guna2Panel3"
        Me.Guna2Panel3.Size = New System.Drawing.Size(360, 360)
        Me.Guna2Panel3.TabIndex = 164
        '
        'BtnMinusGear
        '
        Me.BtnMinusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnMinusGear.Animated = True
        Me.BtnMinusGear.BorderColor = System.Drawing.Color.White
        Me.BtnMinusGear.BorderRadius = 5
        Me.BtnMinusGear.BorderThickness = 1
        Me.BtnMinusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMinusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnMinusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnMinusGear.ForeColor = System.Drawing.Color.White
        Me.BtnMinusGear.Image = CType(resources.GetObject("BtnMinusGear.Image"), System.Drawing.Image)
        Me.BtnMinusGear.Location = New System.Drawing.Point(694, 309)
        Me.BtnMinusGear.Name = "BtnMinusGear"
        Me.BtnMinusGear.Size = New System.Drawing.Size(40, 40)
        Me.BtnMinusGear.TabIndex = 154
        '
        'BtnPlusGear
        '
        Me.BtnPlusGear.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnPlusGear.Animated = True
        Me.BtnPlusGear.BackColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.BorderColor = System.Drawing.Color.White
        Me.BtnPlusGear.BorderRadius = 5
        Me.BtnPlusGear.BorderThickness = 1
        Me.BtnPlusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPlusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnPlusGear.ForeColor = System.Drawing.Color.White
        Me.BtnPlusGear.Image = CType(resources.GetObject("BtnPlusGear.Image"), System.Drawing.Image)
        Me.BtnPlusGear.Location = New System.Drawing.Point(694, 263)
        Me.BtnPlusGear.Name = "BtnPlusGear"
        Me.BtnPlusGear.Size = New System.Drawing.Size(40, 40)
        Me.BtnPlusGear.TabIndex = 153
        '
        'PbAutomatic
        '
        Me.PbAutomatic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAutomatic.Image = Global.sFRAC.My.Resources.Resources.automatic1
        Me.PbAutomatic.ImageRotate = 0!
        Me.PbAutomatic.Location = New System.Drawing.Point(715, 19)
        Me.PbAutomatic.Name = "PbAutomatic"
        Me.PbAutomatic.Size = New System.Drawing.Size(60, 60)
        Me.PbAutomatic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAutomatic.TabIndex = 84
        Me.PbAutomatic.TabStop = False
        Me.PbAutomatic.Visible = False
        '
        'BtnAjustes
        '
        Me.BtnAjustes.Animated = True
        Me.BtnAjustes.BorderColor = System.Drawing.Color.White
        Me.BtnAjustes.BorderRadius = 5
        Me.BtnAjustes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAjustes.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAjustes.ForeColor = System.Drawing.Color.White
        Me.BtnAjustes.Image = Global.sFRAC.My.Resources.Resources.settings_1
        Me.BtnAjustes.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnAjustes.ImageSize = New System.Drawing.Size(59, 59)
        Me.BtnAjustes.Location = New System.Drawing.Point(574, 19)
        Me.BtnAjustes.Name = "BtnAjustes"
        Me.BtnAjustes.Size = New System.Drawing.Size(60, 60)
        Me.BtnAjustes.TabIndex = 81
        '
        'BtnStartEngine
        '
        Me.BtnStartEngine.Animated = True
        Me.BtnStartEngine.BorderColor = System.Drawing.Color.White
        Me.BtnStartEngine.BorderRadius = 5
        Me.BtnStartEngine.CheckedState.Image = Global.sFRAC.My.Resources.Resources.Engine_STOP
        Me.BtnStartEngine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStartEngine.FillColor = System.Drawing.Color.Transparent
        Me.BtnStartEngine.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnStartEngine.ForeColor = System.Drawing.Color.White
        Me.BtnStartEngine.Image = Global.sFRAC.My.Resources.Resources.Engine_START
        Me.BtnStartEngine.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnStartEngine.ImageSize = New System.Drawing.Size(59, 59)
        Me.BtnStartEngine.Location = New System.Drawing.Point(1149, 0)
        Me.BtnStartEngine.Name = "BtnStartEngine"
        Me.BtnStartEngine.Size = New System.Drawing.Size(60, 60)
        Me.BtnStartEngine.TabIndex = 80
        '
        'BtnResetRPM
        '
        Me.BtnResetRPM.Animated = True
        Me.BtnResetRPM.BorderColor = System.Drawing.Color.White
        Me.BtnResetRPM.BorderRadius = 5
        Me.BtnResetRPM.BorderThickness = 1
        Me.BtnResetRPM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnResetRPM.FillColor = System.Drawing.Color.Transparent
        Me.BtnResetRPM.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.BtnResetRPM.ForeColor = System.Drawing.Color.White
        Me.BtnResetRPM.Location = New System.Drawing.Point(416, 19)
        Me.BtnResetRPM.Name = "BtnResetRPM"
        Me.BtnResetRPM.Size = New System.Drawing.Size(77, 60)
        Me.BtnResetRPM.TabIndex = 165
        Me.BtnResetRPM.Text = "Reset RPM"
        '
        'TmrOp
        '
        Me.TmrOp.Interval = 1000
        '
        'TmrCheck
        '
        Me.TmrCheck.Interval = 1500
        '
        'TmrRead
        '
        Me.TmrRead.Interval = 1000
        '
        'TmrBackUp
        '
        Me.TmrBackUp.Interval = 1000
        '
        'BtnEKill
        '
        Me.BtnEKill.Animated = True
        Me.BtnEKill.BorderColor = System.Drawing.Color.White
        Me.BtnEKill.BorderRadius = 5
        Me.BtnEKill.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BtnEKill.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEKill.FillColor = System.Drawing.Color.Transparent
        Me.BtnEKill.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnEKill.ForeColor = System.Drawing.Color.White
        Me.BtnEKill.Image = Global.sFRAC.My.Resources.Resources.E_STOP
        Me.BtnEKill.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnEKill.ImageSize = New System.Drawing.Size(59, 59)
        Me.BtnEKill.Location = New System.Drawing.Point(0, 0)
        Me.BtnEKill.Name = "BtnEKill"
        Me.BtnEKill.Size = New System.Drawing.Size(60, 60)
        Me.BtnEKill.TabIndex = 188
        '
        'LblErrorAdmisible
        '
        Me.LblErrorAdmisible.AutoSize = True
        Me.LblErrorAdmisible.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblErrorAdmisible.ForeColor = System.Drawing.Color.Orange
        Me.LblErrorAdmisible.Location = New System.Drawing.Point(449, 107)
        Me.LblErrorAdmisible.Name = "LblErrorAdmisible"
        Me.LblErrorAdmisible.Size = New System.Drawing.Size(101, 16)
        Me.LblErrorAdmisible.TabIndex = 190
        Me.LblErrorAdmisible.Text = "Error Admisible"
        Me.LblErrorAdmisible.Visible = False
        '
        'LblErrorGrave
        '
        Me.LblErrorGrave.AutoSize = True
        Me.LblErrorGrave.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblErrorGrave.ForeColor = System.Drawing.Color.Red
        Me.LblErrorGrave.Location = New System.Drawing.Point(659, 107)
        Me.LblErrorGrave.Name = "LblErrorGrave"
        Me.LblErrorGrave.Size = New System.Drawing.Size(75, 16)
        Me.LblErrorGrave.TabIndex = 189
        Me.LblErrorGrave.Text = "Error Grave"
        Me.LblErrorGrave.Visible = False
        '
        'OneFrac_1280x840
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Controls.Add(Me.LblErrorAdmisible)
        Me.Controls.Add(Me.LblErrorGrave)
        Me.Controls.Add(Me.BtnEKill)
        Me.Controls.Add(Me.BtnResetRPM)
        Me.Controls.Add(Me.BtnMinusGear)
        Me.Controls.Add(Me.BtnPlusGear)
        Me.Controls.Add(Me.BtnFC)
        Me.Controls.Add(Me.Guna2Panel11)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.PbAutomatic)
        Me.Controls.Add(Me.BtnErrores)
        Me.Controls.Add(Me.BtnDiagnostico)
        Me.Controls.Add(Me.Guna2Panel10)
        Me.Controls.Add(Me.BtnAjustes)
        Me.Controls.Add(Me.BtnStartEngine)
        Me.Controls.Add(Me.Guna2Panel7)
        Me.Controls.Add(Me.Guna2Panel5)
        Me.Controls.Add(Me.Guna2Panel4)
        Me.Controls.Add(Me.Guna2Panel2)
        Me.Controls.Add(Me.Guna2Panel3)
        Me.DoubleBuffered = True
        Me.Name = "OneFrac_1280x840"
        Me.Size = New System.Drawing.Size(1209, 638)
        Me.Guna2Panel11.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.PerformLayout()
        Me.Guna2Panel10.ResumeLayout(False)
        Me.Guna2Panel10.PerformLayout()
        Me.Guna2CustomGradientPanel10.ResumeLayout(False)
        Me.Guna2CustomGradientPanel10.PerformLayout()
        Me.Guna2Panel7.ResumeLayout(False)
        Me.Guna2Panel7.PerformLayout()
        Me.Guna2CustomGradientPanel7.ResumeLayout(False)
        Me.Guna2CustomGradientPanel7.PerformLayout()
        Me.Guna2Panel5.ResumeLayout(False)
        Me.Guna2Panel5.PerformLayout()
        Me.Guna2CustomGradientPanel5.ResumeLayout(False)
        Me.Guna2CustomGradientPanel5.PerformLayout()
        Me.Guna2Panel4.ResumeLayout(False)
        Me.Guna2Panel4.PerformLayout()
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.Guna2Panel2.ResumeLayout(False)
        Me.Guna2Panel2.PerformLayout()
        Me.Guna2Panel3.ResumeLayout(False)
        Me.Guna2Panel3.PerformLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents BtnErrores As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PbAutomatic As Guna.UI2.WinForms.Guna2PictureBox
    Public WithEvents BtnFC As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnDiagnostico As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAjustes As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnStartEngine As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel11 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnEmpezarTrabajo As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnSemiAuto As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnEncenderLuces As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblOpciones As Label
    Public WithEvents Guna2Panel10 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblCaudalActual As Label
    Public WithEvents LblCaudal As Label
    Public WithEvents Guna2Panel7 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label54 As Label
    Public WithEvents Label55 As Label
    Public WithEvents Label56 As Label
    Public WithEvents LblBombaHHP As Label
    Public WithEvents LblHHP As Label
    Public WithEvents LblBombaCargaMotor As Label
    Public WithEvents LblBombaPresSuccion As Label
    Public WithEvents LblCargaMotor As Label
    Public WithEvents LblPresSuc As Label
    Public WithEvents Label63 As Label
    Public WithEvents Label64 As Label
    Public WithEvents Label65 As Label
    Public WithEvents LblBombaHorasMotor As Label
    Public WithEvents LblHM As Label
    Public WithEvents LblBombaPresLUB As Label
    Public WithEvents LblBombaTempLUB As Label
    Public WithEvents LblPresLub As Label
    Public WithEvents LblTempLub As Label
    Public WithEvents LblBomba As Label
    Public WithEvents Guna2Panel5 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label41 As Label
    Public WithEvents Label42 As Label
    Public WithEvents Label43 As Label
    Public WithEvents LblTransRpmOut As Label
    Public WithEvents LblTrans5 As Label
    Public WithEvents LblTransPresion As Label
    Public WithEvents LblTransTemp As Label
    Public WithEvents LblTrans3 As Label
    Public WithEvents LblTrans1 As Label
    Public WithEvents LblTransmision As Label
    Public WithEvents Guna2Panel4 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label23 As Label
    Public WithEvents Label24 As Label
    Public WithEvents Label25 As Label
    Public WithEvents LblMotorCombustibleP100 As Label
    Public WithEvents LblCombustible1 As Label
    Public WithEvents LblMotorCombustible As Label
    Public WithEvents LblMotorTempAceite As Label
    Public WithEvents LblCombustible As Label
    Public WithEvents LblTempAceite As Label
    Public WithEvents Label22 As Label
    Public WithEvents Label21 As Label
    Public WithEvents Label20 As Label
    Public WithEvents LblMotorVoltaje As Label
    Public WithEvents LblVoltaje As Label
    Public WithEvents LblMotorPresion As Label
    Public WithEvents LblMotorTempAgua As Label
    Public WithEvents LblPresion As Label
    Public WithEvents LblTempAgua As Label
    Public WithEvents LblMotor As Label
    Public WithEvents BtnMinusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnPlusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2Panel2 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2Panel3 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnIdle As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPMDeseada As Label
    Public WithEvents LblRPMActual As Label
    Public WithEvents BtnRpmMinus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPM1 As Label
    Public WithEvents BtnRpmMinus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPM3 As Label
    Public WithEvents LblMarchaActual As Label
    Public WithEvents LblMarchaDeseada As Label
    Public WithEvents LblMarcha3 As Label
    Public WithEvents LblMarcha1 As Label
    Public WithEvents BtnTripZero As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnTrip As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblPCorteActual As Label
    Public WithEvents LblPCorte3 As Label
    Public WithEvents LblPCorteDeseada As Label
    Public WithEvents LblPCorte1 As Label
    Public WithEvents LblMarcha As Label
    Public WithEvents BtnResetRPM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel11 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Label81 As Label
    Public WithEvents Guna2CustomGradientPanel10 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel7 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel5 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents TmrReconnect As Timer
    Public WithEvents TmrErrorHandle As Timer
    Public WithEvents RgRPM As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents RgPresion As Guna.UI2.WinForms.Guna2RadialGauge
    Public WithEvents TmrOp As Timer
    Public WithEvents TmrCheck As Timer
    Public WithEvents TmrRead As Timer
    Friend WithEvents TmrBackUp As Timer
    Public WithEvents BtnEKill As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblErrorAdmisible As Label
    Public WithEvents LblErrorGrave As Label
End Class
