﻿Public Class OneFrac_1280x840


    Public RPM As Integer
    Public Frac As Integer
    Public totalError As Integer
    Private MyEngine As Dictionary(Of String, Object)
    Private MyTransmission As Dictionary(Of String, Object)

    Private CSV_BACKUP As CSV
    Private CSV_OPERATION As CSV

    Private Sub OneFrac1_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        ''Se guarda en Frac (variable local para el UC) el fracturador de esta pantalla, el motor y backup de csv
        Frac = currentFrac
        MyEngine = FracData.SelectedEngines("SelectedEngine_" + Frac.ToString())
        MyTransmission = FracData.SelectedTransmissions("SelectedTransmission_" + Frac.ToString())
        If GenerateCSVBackUp Then
            CSV_BACKUP_INIT()
        End If

        ''Si estamos en una fractura automatica mostramos la pestaña para saberlo
        If FracAutomation.AutoStart = True Then
            PbHandle()
        End If

        ''Iniciamos lectura de datos y control de errores/diagnostico
        TmrRead.Start()
        TmrCheck.Start()

        ''Leemos la configuracion actual del fracturador para precargarla
        Dim UpdateConfigTh As New Task(AddressOf UpdateConfig)
        UpdateConfigTh.Start()

        ''Agregamos handler por si se agrega un fracturador a la pantalla despues de iniciar una operacion/grafico
        AddHandler FracAutomation.AutomationStart, AddressOf PbHandleInvoke
        AddHandler IFracSystem.OperationStart, AddressOf OperationStart

        ''Chequeamos por diagnostico del fracturador
        FracDiagnostico.CheckFrac()
    End Sub


    ''Sub ejecutado desde load, precargamos la configuracion actual del Fracturador
    Private Sub UpdateConfig()
        FConfig.UpdateSettings(Frac)
    End Sub

    ''Sub para centrar/alinear todos los objetos
    Public Sub CenterEverything()

        CenterObject(LblCaudal)

        ''Panel RPM
        CenterObject(LblRPM3)
        CenterObject(LblRPMActual)
        CenterObject(BtnIdle)

        ''Panel Marcha
        CenterObject(LblMarcha)
        CenterObject(LblMarcha1)
        CenterObject(LblMarchaDeseada)
        CenterObject(LblMarcha3)
        CenterObject(LblMarchaActual)

        ''Panel Presion de Corte
        CenterObject(LblPCorte1)
        CenterObject(LblPCorteDeseada)
        CenterObject(LblPCorte3)
        CenterObject(LblPCorteActual)
        CenterObject(BtnTrip)
        CenterObject(BtnTripZero)

        ''Panel transmision
        CenterObject(LblTransmision)
        CenterObject(LblTrans1)
        CenterObject(LblTransTemp)
        CenterObject(LblTrans3)
        CenterObject(LblTransPresion)
        CenterObject(LblTrans5)
        CenterObject(LblTransRpmOut)

        ''Panel Opciones
        CenterObject(LblOpciones)

        ''Panel Bomba
        LeftAlign(LblBombaTempLUB, LblTempLub)
        LeftAlign(LblBombaPresSuccion, LblPresSuc)
        LeftAlign(LblBombaPresLUB, LblPresLub)
        LeftAlign(LblBombaCargaMotor, LblCargaMotor)
        LeftAlign(LblBombaHorasMotor, LblHM)
        LeftAlign(LblBombaHHP, LblHHP)

        ''Panel Motor
        LeftAlign(LblMotorTempAgua, LblTempAgua)
        LeftAlign(LblMotorTempAceite, LblTempAceite)
        LeftAlign(LblMotorPresion, LblPresion)
        LeftAlign(LblMotorCombustible, LblCombustible)
        LeftAlign(LblMotorVoltaje, LblVoltaje)
        LeftAlign(LblMotorCombustibleP100, LblCombustible1)

    End Sub


    ''Region dedicada a la lectura y presentacion de los datos
#Region "READ - DISPLAY"

    Delegate Sub ReadThreadDelegate()
    Private Sub TmrRead_Tick(sender As Object, e As EventArgs) Handles TmrRead.Tick
        Dim ReadThread As New Task(AddressOf ReadThreadDo)
        ReadThread.Start()
    End Sub

    Private Sub ReadThreadDo()
        Dim readResult As Integer() = ISystem.MoxaRead(0, TotalMoxaData, Frac)
        If readResult.Length() = TotalMoxaData Then
            FracDataAssign(readResult, Frac)
            GetMaintenance(readResult, Frac)
        End If
        Invoke(New ReadThreadDelegate(AddressOf DisplayData))
    End Sub

    Private Sub DisplayData()

        ''Leer errores
        totalError = FracData.DblVFrac("TotalErr_" + Frac.ToString())

        ''Display RPM
        LblRPMActual.Text = FracData.DblVFrac("RPMMotor_" + Frac.ToString())
        LblRPMDeseada.Text = FracData.DblVFrac("RPMDeseada_" + Frac.ToString())
        CenterObject(LblRPMActual)
        CenterAlign(LblRPMDeseada, LblRPM1)


        ''Display Marcha
        LblMarchaDeseada.Text = FracData.StrVFrac("MarchaDeseada_" + Frac.ToString())
        LblMarchaActual.Text = FracData.StrVFrac("MarchaActual_" + Frac.ToString())
        CenterObject(LblMarchaDeseada)
        CenterObject(LblMarchaActual)

        ''Display Caudal
        LblCaudalActual.Text = FracData.DblVFrac("Caudal_" + Frac.ToString())

        ''Display Presion de Corte
        LblPCorteDeseada.Text = FracData.DblVFrac("TRIP_" + Frac.ToString())
        LblPCorteActual.Text = FracData.DblVFrac("Viatran_" + Frac.ToString())
        CenterObject(LblPCorteActual)
        CenterObject(LblPCorteDeseada)

        ''Display Motor
        LblMotorTempAgua.Text = FracData.DblVFrac("TemperaturaAguaMotor_" + Frac.ToString())
        LblMotorTempAceite.Text = FracData.DblVFrac("TemperaturaAceiteMotor_" + Frac.ToString())
        LblMotorPresion.Text = FracData.DblVFrac("PresionAceiteMotor_" + Frac.ToString())
        LblMotorCombustible.Text = FracData.DblVFrac("FuelRate_" + Frac.ToString())
        LblMotorVoltaje.Text = FracData.DblVFrac("Voltaje_" + Frac.ToString())
        LblMotorCombustibleP100.Text = FracData.DblVFrac("CombustibleTanque_" + Frac.ToString())
        LeftAlign(LblMotorTempAgua, LblTempAgua)
        LeftAlign(LblMotorTempAceite, LblTempAceite)
        LeftAlign(LblMotorPresion, LblPresion)
        LeftAlign(LblMotorCombustible, LblCombustible)
        LeftAlign(LblMotorVoltaje, LblVoltaje)
        LeftAlign(LblMotorCombustibleP100, LblCombustible1)

        ''Display Transmision
        LblTransTemp.Text = FracData.DblVFrac("TemperaturaAceiteTransmision_" + Frac.ToString())
        LblTransPresion.Text = FracData.DblVFrac("PresTrans_" + Frac.ToString())
        LblTransRpmOut.Text = FracData.DblVFrac("RPMSalidaTransmision_" + Frac.ToString())
        CenterObject(LblTransTemp)
        CenterObject(LblTransPresion)
        CenterObject(LblTransRpmOut)

        ''Display Bomba
        LblBombaTempLUB.Text = FracData.DblVFrac("TemperaturaLUB_LP_" + Frac.ToString())
        LblBombaPresSuccion.Text = FracData.DblVFrac("PresSuc_" + Frac.ToString())
        LblBombaPresLUB.Text = FracData.DblVFrac("PresionLUB_LP_" + Frac.ToString())
        LblBombaCargaMotor.Text = FracData.DblVFrac("PorcentajeCarga_" + Frac.ToString())
        LblBombaHorasMotor.Text = FracData.DblVFrac("HorasMotor_" + Frac.ToString())
        LblBombaHHP.Text = FracData.DblVFrac("HHPMotor_" + Frac.ToString())
        LeftAlign(LblBombaTempLUB, LblTempLub)
        LeftAlign(LblBombaPresSuccion, LblPresSuc)
        LeftAlign(LblBombaPresLUB, LblPresLub)
        LeftAlign(LblBombaCargaMotor, LblCargaMotor)
        LeftAlign(LblBombaHorasMotor, LblHM)
        LeftAlign(LblBombaHHP, LblHHP)

        ''Display Errores
        LblErrorGrave.Visible = FracData.DblVFrac("LamparaRoja_" + Frac.ToString())
        LblErrorAdmisible.Visible = FracData.DblVFrac("LamparaAmbar_" + Frac.ToString())

        Try
            RgRPM.Value = (FracData.DblVFrac("RPMMotor_" + Frac.ToString()) / 1950) * 100
            If FracData.DblVFrac("TRIP_" + Frac.ToString()) <> 0 Then RgPresion.Value = (FracData.DblVFrac("Viatran_" + Frac.ToString()) / FracData.DblVFrac("TRIP_" + Frac.ToString())) * 100
        Catch Ex As Exception
            LogError(Ex)
        End Try

    End Sub

#End Region

    ''Region dedicada al control de RPM
#Region "RPM Control"
    Private Sub RpmPlus(sender As Object, e As EventArgs) Handles BtnRpmPlus50.Click, BtnRpmPlus25.Click
        Dim RpmPlusTh As New Task(Sub() RpmPlusDo(sender))
        RpmPlusTh.Start()
    End Sub

    Private Sub RpmPlusDo(ByVal sender As Object)
        Dim RpmPlus As Integer = GetInteger(sender.name)
        If MyEngine("LimitRPM") = True Then
            RPM = Clamp(MyEngine("RPM_LMin"), MyEngine("RPM_LMax"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) + RpmPlus)
        Else
            RPM = Clamp(MyEngine("MinRPM"), MyEngine("MaxRPM"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) + RpmPlus)
        End If
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), RPM, Frac)
    End Sub

    Private Sub RpmMinus(sender As Object, e As EventArgs) Handles BtnRpmMinus25.Click, BtnRpmMinus50.Click
        Dim RpmMinusTh As New Task(Sub() RpmMinusDo(sender))
        RpmMinusTh.Start()
    End Sub

    Private Sub RpmMinusDo(sender As Object)
        Dim RpmMinus As Integer = GetInteger(sender.name)
        If MyEngine("LimitRPM") = True Then
            RPM = Clamp(MyEngine("RPM_LMin"), MyEngine("RPM_LMax"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) - RpmMinus)
        Else
            RPM = Clamp(MyEngine("MinRPM"), MyEngine("MaxRPM"), FracData.DblVFrac("RPMMotor_" + Frac.ToString()) - RpmMinus)
        End If
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), RPM, Frac)
    End Sub

    Private Sub BtnIdle_Click(sender As Object, e As EventArgs) Handles BtnIdle.Click
        Dim RpmIdleTh As New Task(AddressOf RpmIdle)
        RpmIdleTh.Start()
    End Sub

    Private Async Sub RpmIdle()
        ''Motor en IDLE
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), MyEngine("MinRPM"), Frac)
        Await Task.Delay(100)
        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), FracGeneralValues("Marcha0"), Frac)
    End Sub

    Private Sub LblRPMDeseada_Click(sender As Object, e As EventArgs) Handles LblRPMDeseada.Click
        currentFrac = Frac
        SetRPM.ShowDialog(Me)
    End Sub
#End Region

    ''Region dedicada al control de Gear
#Region "Gear Select"
    Private Sub BtnPlusGear_Click(sender As Object, e As EventArgs) Handles BtnPlusGear.Click
        Dim GearPlusTh As New Task(AddressOf GearPlus)
        GearPlusTh.Start()
    End Sub

    Private Sub GearPlus()
        Dim marcha As Integer
        Select Case FracData.StrVFrac("MarchaActual_" + Frac.ToString())
            Case "N"
                marcha = FracGeneralValues("Marcha1")
            Case "P"
                Exit Sub
            Case Else
                Dim MarchaReal As Integer = GetInteger(FracData.StrVFrac("MarchaActual_" + Frac.ToString()))
                marcha = FracGeneralValues("Marcha" + Math.Min(MarchaReal + 1, MyTransmission.Item("Gears")).ToString())
        End Select

        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, Frac)
    End Sub

    Private Sub BtnMinusGear_Click(sender As Object, e As EventArgs) Handles BtnMinusGear.Click
        Dim GearMinusTh As New Task(AddressOf GearMinus)
        GearMinusTh.Start()
    End Sub

    Private Sub GearMinus()
        Dim marcha As Integer

        Select Case FracData.StrVFrac("MarchaActual_" + Frac.ToString())
            Case "N"
                marcha = FracGeneralValues("Marcha0")
            Case "P"
                Exit Sub
            Case Else
                Dim MarchaReal As Integer = GetInteger(FracData.StrVFrac("MarchaActual_" + Frac.ToString()))
                marcha = FracGeneralValues("Marcha" + (MarchaReal - 1).ToString())
        End Select

        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, Frac)
    End Sub


    Private Sub LblMarchaDeseada_Click(sender As Object, e As EventArgs) Handles LblMarchaDeseada.Click
        currentFrac = Frac
        SetGEAR.ShowDialog(Me)
    End Sub
#End Region

    ''Region para encender motor
#Region "EngineStart"
    Private Sub BtnStartEngine_Click(sender As Object, e As EventArgs) Handles BtnStartEngine.Click
        Dim EngineStartTh As New Task(AddressOf EngineStart)
        EngineStartTh.Start()
    End Sub

    Private Async Sub EngineStart()
        If BtnStartEngine.Checked = False Then
            ISystem.MoxaSend(FracMoxaSendStartAddress("Arranque_StartAddress"), FracGeneralValues("ArranqueON"), Frac)
            Await Task.Delay(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("Arranque_StartAddress"), FracGeneralValues("ArranqueOFF"), Frac)
        Else
            ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerON"), Frac)
            Await Task.Delay(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerOFF"), Frac)
        End If
    End Sub
#End Region

    ''Sub con Handler al control de TRIP
    Private Sub BtnTrip_Click(sender As Object, e As EventArgs) Handles BtnTrip.Click
        currentFrac = Frac
        SetTRIP.ShowDialog(Me)
    End Sub

    ''Sub para controlar el factor caudal
    Private Sub BtnFC_Click(sender As Object, e As EventArgs) Handles BtnFC.Click
        currentFrac = Frac
        SetFC.ShowDialog(Me)
    End Sub

    ''Sub para el control del offset de TRIP
    Private Sub BtnTripZero_Click(sender As Object, e As EventArgs) Handles BtnTripZero.Click
        currentFrac = Frac
        SetZERO.ShowDialog(Me)
    End Sub

    ''Encender luces del Fracturador
    Private Sub BtnEncenderLuces_Click(sender As Object, e As EventArgs) Handles BtnEncenderLuces.Click
        sender.Checked = Not sender.Checked
        HandleLuces(sender, Frac)
    End Sub

    ''Region para control de errores/diagnostico (TmrCheck)
#Region "Control de errores/diagnostico"
    Delegate Sub BtnDelegate()
    Private Sub TmrCheck_Tick(sender As Object, e As EventArgs) Handles TmrCheck.Tick
        Dim CheckHandler As New Task(AddressOf BgCheck)
        CheckHandler.Start()
    End Sub

    Private Sub BgCheck()
        Invoke(New BtnDelegate(AddressOf CheckUpdate))
    End Sub

    Dim ResetRPM As Boolean = True
    Private Sub CheckUpdate()

        If FracData.DblVFrac("RPMMotor_" + Frac.ToString) > 100 Then
            BtnStartEngine.Checked = True
        Else
            BtnStartEngine.Checked = False
        End If

        If totalError <> 0 Then
            BtnErrores.Visible = True
            TmrErrorHandle.Start()
        Else
            BtnErrores.Visible = False
            TmrErrorHandle.Stop()
        End If

        If FracDiagnostico.sError = True Then
            BtnDiagnostico.FillColor = Color.FromArgb(255, 128, 128)
        ElseIf FracDiagnostico.sWarning = True Then
            BtnDiagnostico.FillColor = Color.FromArgb(255, 147, 74)
        Else
            BtnDiagnostico.FillColor = Color.Transparent
        End If

        If FracData.DblVFrac("HHPMotor_" + Frac.ToString()) >= 2240 Then
            BtnPlusGear.FillColor = Color.FromArgb(255, 64, 64)
        Else
            BtnPlusGear.FillColor = Color.Transparent
        End If

        If FracData.DblVFrac("EstadoAcelerador_" + Frac.ToString()) = 0 And ResetRPM = False Then
            RgRPM.ProgressColor = Color.Maroon
            RgRPM.ProgressColor2 = Color.Maroon
            RgPresion.ProgressColor = Color.Maroon
            RgPresion.ProgressColor2 = Color.Maroon
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Advertencia")
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("PresionExcedida")
            BtnResetRPM.Visible = True
            If Application.OpenForms().OfType(Of AvisoGeneral).Any = False Then
                My.Computer.Audio.Play(My.Application.Info.DirectoryPath + "\alarm1.wav", AudioPlayMode.BackgroundLoop)
                If AvisoGeneral.ShowDialog(Me) = DialogResult.OK Then
                    BtnIdle.PerformClick()
                    My.Computer.Audio.Stop()
                    ResetRPM = True
                    BtnResetRPM.FillColor = Color.FromArgb(255, 128, 128)
                End If
            End If
        ElseIf FracData.DblVFrac("EstadoAcelerador_" + Frac.ToString()) = 0 Then
            RgRPM.ProgressColor = Color.Maroon
            RgRPM.ProgressColor2 = Color.Maroon
            RgPresion.ProgressColor = Color.Maroon
            RgPresion.ProgressColor2 = Color.Maroon
        Else
            RgRPM.ProgressColor = Color.DeepSkyBlue
            RgRPM.ProgressColor2 = Color.FromArgb(10, 10, 248)
            RgPresion.ProgressColor = Color.Lime
            RgPresion.ProgressColor2 = Color.DarkGreen
            BtnResetRPM.Visible = False
            ResetRPM = False
        End If

    End Sub

#End Region

    ''Sub para abrir ajustes de Fracturador
    Private Sub BtnAjustes_Click(sender As Object, e As EventArgs) Handles BtnAjustes.Click
        currentFrac = Frac
        AjustesFracturador.ShowDialog(Me)
    End Sub

    ''Sub para iniciar graficador
    Private Sub BtnEmpezarTrabajo_Click(sender As Object, e As EventArgs) Handles BtnEmpezarTrabajo.Click
        If BtnEmpezarTrabajo.Checked = False Then
            OperationStartTime = DateTime.Now()                          ''Tiempo de inicio
            IFracSystem.ActiveOP = True
        Else
            IFracSystem.ActiveOP = False
        End If
    End Sub

    ''Sub para abrir pantalla de diagnostico
    Private Sub BtnDiagnostico_Click(sender As Object, e As EventArgs) Handles BtnDiagnostico.Click
        currentFrac = Frac
        FracDiagnostico.ShowDialog(Me)
    End Sub

    ''Sub para abrir pantalla de control semiautomatico
    Private Sub BtnSemiAuto_Click(sender As Object, e As EventArgs) Handles BtnSemiAuto.Click
        currentFrac = Frac
        TesteoLinea.ShowDialog(Me)
    End Sub

    ''Sub para abrir pantalla de errores
    Private Sub BtnErrores_Click(sender As Object, e As EventArgs) Handles BtnErrores.Click
        currentFrac = Frac
        FracErrores.ShowDialog(Me)
    End Sub

    ''Sub para el control/informacion de fractura automatica
    Private Sub PbAutomatic_Click(sender As Object, e As EventArgs) Handles PbAutomatic.Click
        InfoShow.ShowDialog(Me)
    End Sub

    ''Sub para reconexion (NO ESTA USADO ACTUALMENTE)
    Private Sub TmrReconnect_Tick(sender As Object, e As EventArgs) Handles TmrReconnect.Tick
        ''Revisar si esto se va a usar en algun momento
        Try
            Dim req As Boolean = ISystem.Connect(Frac)
            If req = False Then

            End If
        Catch ex As Exception
            LogError(ex)
            TmrReconnect.Stop()
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("avisoStr.Reconnect")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
            'Principal.Close()
        End Try
    End Sub


    ''Seccion para activar el control/informacion de fractura automatica
    Delegate Sub PbHandleDelegate()
    Private Sub PbHandleInvoke()
        PbAutomatic.Invoke(New PbHandleDelegate(AddressOf PbHandle))
    End Sub
    Private Sub PbHandle()
        If FracAutomation.FracList.Contains(Frac) Then
            BtnEmpezarTrabajo.PerformClick()
            PbAutomatic.Visible = FracAutomation.AutoStart
        End If
    End Sub

    ''Seccion para avisar sobre errores presentes en el fracturador
    Private Sub TmrErrorHandle_Tick(sender As Object, e As EventArgs) Handles TmrErrorHandle.Tick
        Dim ErrorHandler As New Task(AddressOf ErrorHandle)
        ErrorHandler.Start()
    End Sub
    Dim ErrorI As Integer = 0
    Private Sub ErrorHandle()
        If ErrorI Mod 2 <> 0 Then
            BtnErrores.FillColor = Color.Maroon
        Else
            BtnErrores.FillColor = Color.FromArgb(49, 66, 82)
        End If
        ErrorI += 1
    End Sub

    ''Control del tiempo de operacion
    Private Async Sub TmrOp_Tick(sender As Object, e As EventArgs) Handles TmrOp.Tick
        Await Task.Run(Sub() WriteCSVRecord(CSV_OPERATION, Frac))
    End Sub

    ''Si se inicia grafico en otro fracturador con este prendido, se grafica tambien
    Private Sub OperationStart()
        If IFracSystem.ActiveOP = False Then
            TmrOp.Stop()
            If CSV_OPERATION IsNot Nothing Then
                CSV_OPERATION.CloseCSV()
                ActiveCSV.Remove(Frac)
            End If
            BtnEmpezarTrabajo.Checked = False
            BtnEmpezarTrabajo.Text = LanguageSelect.DynamicLanguageRM.GetString("EmpezarTrabajo")
        Else
            If Not ActiveCSV.Contains(Frac) Then
                If CSV_OPERATION_INIT(CSV_OPERATION, Frac) Then
                    TmrOp.Start()
                    BtnEmpezarTrabajo.Checked = True
                    BtnEmpezarTrabajo.Text = LanguageSelect.DynamicLanguageRM.GetString("FinalizarTrabajo")
                End If
            End If
        End If
    End Sub

    ''Seccion para hacer ResetRPM al Fracturador
    Private Sub BtnResetRPM_Click(sender As Object, e As EventArgs) Handles BtnResetRPM.Click
        Dim ResetRPMTh As New Task(AddressOf ResetRPMDo)
        ResetRPMTh.Start()
    End Sub
    Private Async Sub ResetRPMDo()
        ISystem.MoxaSend(FracMoxaSendStartAddress("ResetRPM_StartAddress"), 1, Frac)
        Await Task.Delay(100)
        ISystem.MoxaSend(FracMoxaSendStartAddress("ResetRPM_StartAddress"), 0, Frac)
    End Sub

    ''Sub para apagar todos los timers del fracturador
    Public Sub StopTimers()
        TmrCheck.Stop()
        TmrRead.Stop()
        TmrReconnect.Stop()
        TmrOp.Stop()
        TmrErrorHandle.Stop()
        TmrBackUp.Stop()

        If CSV_BACKUP IsNot Nothing Then
            CSV_BACKUP.CloseCSV()
        End If

        If CSV_OPERATION IsNot Nothing Then
            CSV_OPERATION.CloseCSV()
        End If

        RemoveHandler FracAutomation.AutomationStart, AddressOf PbHandleInvoke
        RemoveHandler IFracSystem.OperationStart, AddressOf OperationStart
    End Sub


#Region "BackUpCSV"
    Private Sub TmrBackUp_Tick(sender As Object, e As EventArgs) Handles TmrBackUp.Tick
        Dim BUThread As New Task(Sub() WriteCSVRecord(CSV_BACKUP, Frac))
        BUThread.Start()
    End Sub

    Private Sub CSV_BACKUP_INIT()
        If Not GetDuplicateFracs(Frac) Then
            CSV_BACKUP = New CSV(ISystem.GetFracName(Frac), CSVBackUpPath, GenerateExcel, CSVFlushInterval)
            Dim Fields As ICollection(Of String) = {"Hora", "RPM Motor", "RPM Trans.", "Presion", "Caudal", "Temp. Agua Motor", "Temp. Aceite Motor",
                                                    "Presion Motor", "Temp. Trans.", "Presion Trans.", "Temp. LUB", "Presion Succ.", "Presion LUB", "Carga Motor"}
            CSV_BACKUP.WriteFields(Fields)
            TmrBackUp.Start()
        End If
    End Sub
#End Region

    ''EKill Handle
    Private Sub BtnEKill_Click(sender As Object, e As EventArgs) Handles BtnEKill.Click
        Dim EKillTask As New Task(AddressOf EKill)
    End Sub

    Private Async Sub EKill()
        ISystem.MoxaSend(FracMoxaSendStartAddress("E-KillStartAddress"), 1, Frac)
        Await Task.Delay(100)
        ISystem.MoxaSend(FracMoxaSendStartAddress("E-KillStartAddress"), 0, Frac)
    End Sub
End Class
