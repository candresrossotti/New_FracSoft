﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Secundaria_1280x840
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Secundaria_1280x840))
        Me.MainPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.TOPPanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblTOp1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel8 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTOp = New System.Windows.Forms.Label()
        Me.BtnStopAll = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnIdleALL = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.SidePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnSwitch = New Guna.UI2.WinForms.Guna2Button()
        Me.QMLogo = New System.Windows.Forms.PictureBox()
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Guna2ControlBox2 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.TmrOp = New System.Windows.Forms.Timer(Me.components)
        Me.MainPanel.SuspendLayout()
        Me.TOPPanel.SuspendLayout()
        Me.Guna2CustomGradientPanel8.SuspendLayout()
        Me.SidePanel.SuspendLayout()
        CType(Me.QMLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TitlePanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainPanel
        '
        Me.MainPanel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MainPanel.BackColor = System.Drawing.Color.Transparent
        Me.MainPanel.Controls.Add(Me.TOPPanel)
        Me.MainPanel.Controls.Add(Me.BtnStopAll)
        Me.MainPanel.Controls.Add(Me.BtnIdleALL)
        Me.MainPanel.Location = New System.Drawing.Point(45, 38)
        Me.MainPanel.Name = "MainPanel"
        Me.MainPanel.Size = New System.Drawing.Size(1235, 762)
        Me.MainPanel.TabIndex = 4
        '
        'TOPPanel
        '
        Me.TOPPanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TOPPanel.BackColor = System.Drawing.Color.Transparent
        Me.TOPPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.TOPPanel.BorderRadius = 6
        Me.TOPPanel.BorderThickness = 1
        Me.TOPPanel.Controls.Add(Me.LblTOp1)
        Me.TOPPanel.Controls.Add(Me.Guna2CustomGradientPanel8)
        Me.TOPPanel.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.TOPPanel.FillColor = System.Drawing.Color.Transparent
        Me.TOPPanel.Location = New System.Drawing.Point(441, 680)
        Me.TOPPanel.Name = "TOPPanel"
        Me.TOPPanel.Size = New System.Drawing.Size(346, 70)
        Me.TOPPanel.TabIndex = 88
        '
        'LblTOp1
        '
        Me.LblTOp1.AutoSize = True
        Me.LblTOp1.Font = New System.Drawing.Font("Montserrat", 20.0!)
        Me.LblTOp1.ForeColor = System.Drawing.Color.White
        Me.LblTOp1.Location = New System.Drawing.Point(126, 29)
        Me.LblTOp1.Name = "LblTOp1"
        Me.LblTOp1.Size = New System.Drawing.Size(95, 37)
        Me.LblTOp1.TabIndex = 22
        Me.LblTOp1.Text = "00:00"
        '
        'Guna2CustomGradientPanel8
        '
        Me.Guna2CustomGradientPanel8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.BorderRadius = 6
        Me.Guna2CustomGradientPanel8.Controls.Add(Me.LblTOp)
        Me.Guna2CustomGradientPanel8.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel8.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel8.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel8.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel8.Name = "Guna2CustomGradientPanel8"
        Me.Guna2CustomGradientPanel8.Size = New System.Drawing.Size(346, 26)
        Me.Guna2CustomGradientPanel8.TabIndex = 19
        '
        'LblTOp
        '
        Me.LblTOp.AutoSize = True
        Me.LblTOp.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblTOp.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTOp.Location = New System.Drawing.Point(95, 4)
        Me.LblTOp.Name = "LblTOp"
        Me.LblTOp.Size = New System.Drawing.Size(156, 20)
        Me.LblTOp.TabIndex = 19
        Me.LblTOp.Text = "Tiempo de operación"
        '
        'BtnStopAll
        '
        Me.BtnStopAll.BorderColor = System.Drawing.Color.White
        Me.BtnStopAll.BorderRadius = 5
        Me.BtnStopAll.BorderThickness = 1
        Me.BtnStopAll.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BtnStopAll.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStopAll.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnStopAll.FillColor = System.Drawing.Color.Transparent
        Me.BtnStopAll.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.BtnStopAll.ForeColor = System.Drawing.Color.White
        Me.BtnStopAll.Location = New System.Drawing.Point(6, 680)
        Me.BtnStopAll.Name = "BtnStopAll"
        Me.BtnStopAll.Size = New System.Drawing.Size(346, 70)
        Me.BtnStopAll.TabIndex = 87
        Me.BtnStopAll.Text = "STOP ALL"
        '
        'BtnIdleALL
        '
        Me.BtnIdleALL.Animated = True
        Me.BtnIdleALL.BorderColor = System.Drawing.Color.White
        Me.BtnIdleALL.BorderRadius = 5
        Me.BtnIdleALL.BorderThickness = 1
        Me.BtnIdleALL.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdleALL.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdleALL.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdleALL.Font = New System.Drawing.Font("Montserrat", 25.0!)
        Me.BtnIdleALL.ForeColor = System.Drawing.Color.White
        Me.BtnIdleALL.Location = New System.Drawing.Point(876, 680)
        Me.BtnIdleALL.Name = "BtnIdleALL"
        Me.BtnIdleALL.Size = New System.Drawing.Size(346, 70)
        Me.BtnIdleALL.TabIndex = 86
        Me.BtnIdleALL.Text = "IDLE"
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.ShadowColor = System.Drawing.Color.White
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'SidePanel
        '
        Me.SidePanel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SidePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(40, Byte), Integer))
        Me.SidePanel.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanel.BorderThickness = 1
        Me.SidePanel.Controls.Add(Me.BtnSwitch)
        Me.SidePanel.Controls.Add(Me.QMLogo)
        Me.SidePanel.Location = New System.Drawing.Point(0, 38)
        Me.SidePanel.Name = "SidePanel"
        Me.SidePanel.Size = New System.Drawing.Size(45, 762)
        Me.SidePanel.TabIndex = 2
        '
        'BtnSwitch
        '
        Me.BtnSwitch.BackColor = System.Drawing.Color.Transparent
        Me.BtnSwitch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSwitch.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnSwitch.FillColor = System.Drawing.Color.Transparent
        Me.BtnSwitch.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnSwitch.ForeColor = System.Drawing.Color.White
        Me.BtnSwitch.Image = Global.sFRAC.My.Resources.Resources.swap
        Me.BtnSwitch.ImageSize = New System.Drawing.Size(39, 39)
        Me.BtnSwitch.Location = New System.Drawing.Point(0, 43)
        Me.BtnSwitch.Name = "BtnSwitch"
        Me.BtnSwitch.Size = New System.Drawing.Size(45, 45)
        Me.BtnSwitch.TabIndex = 2
        Me.BtnSwitch.UseTransparentBackground = True
        '
        'QMLogo
        '
        Me.QMLogo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.QMLogo.Image = CType(resources.GetObject("QMLogo.Image"), System.Drawing.Image)
        Me.QMLogo.Location = New System.Drawing.Point(-29, 689)
        Me.QMLogo.Name = "QMLogo"
        Me.QMLogo.Size = New System.Drawing.Size(102, 73)
        Me.QMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.QMLogo.TabIndex = 0
        Me.QMLogo.TabStop = False
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox2)
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.Size = New System.Drawing.Size(1280, 38)
        Me.TitlePanel.TabIndex = 3
        '
        'Guna2ControlBox2
        '
        Me.Guna2ControlBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox
        Me.Guna2ControlBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox2.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox2.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox2.Location = New System.Drawing.Point(1181, 5)
        Me.Guna2ControlBox2.Name = "Guna2ControlBox2"
        Me.Guna2ControlBox2.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox2.TabIndex = 4
        Me.Guna2ControlBox2.UseTransparentBackground = True
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Montserrat", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(557, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(167, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Fracturadores"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(1232, 5)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl1.TargetControl = Me.TitlePanel
        Me.Guna2DragControl1.UseTransparentDrag = True
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2DragControl2.TargetControl = Me.Title
        Me.Guna2DragControl2.UseTransparentDrag = True
        '
        'TmrOp
        '
        Me.TmrOp.Interval = 1000
        '
        'Secundaria_1280x840
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1280, 800)
        Me.Controls.Add(Me.MainPanel)
        Me.Controls.Add(Me.SidePanel)
        Me.Controls.Add(Me.TitlePanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Secundaria_1280x840"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Secundaria_1280x840_"
        Me.MainPanel.ResumeLayout(False)
        Me.TOPPanel.ResumeLayout(False)
        Me.TOPPanel.PerformLayout()
        Me.Guna2CustomGradientPanel8.ResumeLayout(False)
        Me.Guna2CustomGradientPanel8.PerformLayout()
        Me.SidePanel.ResumeLayout(False)
        CType(Me.QMLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents MainPanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents SidePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents QMLogo As PictureBox
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Title As Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Public WithEvents BtnSwitch As Guna.UI2.WinForms.Guna2Button
    Public WithEvents TOPPanel As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblTOp1 As Label
    Public WithEvents Guna2CustomGradientPanel8 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents LblTOp As Label
    Public WithEvents BtnStopAll As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnIdleALL As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents TmrOp As Timer
    Friend WithEvents Guna2ControlBox2 As Guna.UI2.WinForms.Guna2ControlBox
End Class
