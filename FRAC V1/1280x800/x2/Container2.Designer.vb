﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Container2_1280x840
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Guna2Panel11 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd2 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel11 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize2 = New Guna.UI2.WinForms.Guna2Button()
        Me.M2 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Title2 = New System.Windows.Forms.Label()
        Me.CloseButton2 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd1 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize1 = New Guna.UI2.WinForms.Guna2Button()
        Me.Title1 = New System.Windows.Forms.Label()
        Me.CloseButton1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.M1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel11.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Guna2CustomGradientPanel11.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.HasFormShadow = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Guna2Panel11
        '
        Me.Guna2Panel11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel11.BorderRadius = 6
        Me.Guna2Panel11.BorderThickness = 1
        Me.Guna2Panel11.Controls.Add(Me.Panel2)
        Me.Guna2Panel11.Controls.Add(Me.Guna2CustomGradientPanel11)
        Me.Guna2Panel11.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.Location = New System.Drawing.Point(620, 12)
        Me.Guna2Panel11.Name = "Guna2Panel11"
        Me.Guna2Panel11.Size = New System.Drawing.Size(602, 663)
        Me.Guna2Panel11.TabIndex = 67
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.Controls.Add(Me.BtnAdd2)
        Me.Panel2.Location = New System.Drawing.Point(1, 23)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(600, 638)
        Me.Panel2.TabIndex = 57
        '
        'BtnAdd2
        '
        Me.BtnAdd2.AutoRoundedCorners = True
        Me.BtnAdd2.BorderRadius = 49
        Me.BtnAdd2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd2.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd2.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd2.ForeColor = System.Drawing.Color.White
        Me.BtnAdd2.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd2.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd2.Location = New System.Drawing.Point(251, 292)
        Me.BtnAdd2.Name = "BtnAdd2"
        Me.BtnAdd2.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd2.TabIndex = 1
        '
        'Guna2CustomGradientPanel11
        '
        Me.Guna2CustomGradientPanel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.BorderRadius = 6
        Me.Guna2CustomGradientPanel11.Controls.Add(Me.Maximize2)
        Me.Guna2CustomGradientPanel11.Controls.Add(Me.M2)
        Me.Guna2CustomGradientPanel11.Controls.Add(Me.Title2)
        Me.Guna2CustomGradientPanel11.Controls.Add(Me.CloseButton2)
        Me.Guna2CustomGradientPanel11.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel11.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel11.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel11.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel11.Name = "Guna2CustomGradientPanel11"
        Me.Guna2CustomGradientPanel11.Size = New System.Drawing.Size(602, 23)
        Me.Guna2CustomGradientPanel11.TabIndex = 19
        '
        'Maximize2
        '
        Me.Maximize2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize2.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize2.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize2.Enabled = False
        Me.Maximize2.FillColor = System.Drawing.Color.Transparent
        Me.Maximize2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize2.ForeColor = System.Drawing.Color.White
        Me.Maximize2.Location = New System.Drawing.Point(553, 1)
        Me.Maximize2.Name = "Maximize2"
        Me.Maximize2.Size = New System.Drawing.Size(20, 20)
        Me.Maximize2.TabIndex = 21
        Me.Maximize2.UseTransparentBackground = True
        '
        'M2
        '
        Me.M2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M2.Enabled = False
        Me.M2.FillColor = System.Drawing.Color.Transparent
        Me.M2.IconColor = System.Drawing.Color.White
        Me.M2.Location = New System.Drawing.Point(553, 1)
        Me.M2.Name = "M2"
        Me.M2.Size = New System.Drawing.Size(20, 20)
        Me.M2.TabIndex = 20
        '
        'Title2
        '
        Me.Title2.AutoSize = True
        Me.Title2.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Title2.ForeColor = System.Drawing.Color.White
        Me.Title2.Location = New System.Drawing.Point(277, 3)
        Me.Title2.Name = "Title2"
        Me.Title2.Size = New System.Drawing.Size(48, 16)
        Me.Title2.TabIndex = 19
        Me.Title2.Text = "Titulo2"
        '
        'CloseButton2
        '
        Me.CloseButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton2.Enabled = False
        Me.CloseButton2.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton2.IconColor = System.Drawing.Color.White
        Me.CloseButton2.Location = New System.Drawing.Point(579, 1)
        Me.CloseButton2.Name = "CloseButton2"
        Me.CloseButton2.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton2.TabIndex = 4
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 5
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.Panel1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(602, 663)
        Me.Guna2Panel1.TabIndex = 68
        '
        'Panel1
        '
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel1.Controls.Add(Me.BtnAdd1)
        Me.Panel1.Location = New System.Drawing.Point(1, 23)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(600, 638)
        Me.Panel1.TabIndex = 57
        '
        'BtnAdd1
        '
        Me.BtnAdd1.AutoRoundedCorners = True
        Me.BtnAdd1.BorderRadius = 49
        Me.BtnAdd1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd1.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd1.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd1.ForeColor = System.Drawing.Color.White
        Me.BtnAdd1.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd1.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd1.Location = New System.Drawing.Point(251, 292)
        Me.BtnAdd1.Name = "BtnAdd1"
        Me.BtnAdd1.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd1.TabIndex = 0
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.Maximize1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.Title1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.CloseButton1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.M1)
        Me.Guna2CustomGradientPanel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel1.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(602, 23)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'Maximize1
        '
        Me.Maximize1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize1.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize1.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize1.Enabled = False
        Me.Maximize1.FillColor = System.Drawing.Color.Transparent
        Me.Maximize1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize1.ForeColor = System.Drawing.Color.White
        Me.Maximize1.Location = New System.Drawing.Point(553, 1)
        Me.Maximize1.Name = "Maximize1"
        Me.Maximize1.Size = New System.Drawing.Size(20, 20)
        Me.Maximize1.TabIndex = 1
        Me.Maximize1.UseTransparentBackground = True
        '
        'Title1
        '
        Me.Title1.AutoSize = True
        Me.Title1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Title1.ForeColor = System.Drawing.Color.White
        Me.Title1.Location = New System.Drawing.Point(279, 3)
        Me.Title1.Name = "Title1"
        Me.Title1.Size = New System.Drawing.Size(45, 16)
        Me.Title1.TabIndex = 19
        Me.Title1.Text = "Titulo1"
        '
        'CloseButton1
        '
        Me.CloseButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton1.Enabled = False
        Me.CloseButton1.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton1.IconColor = System.Drawing.Color.White
        Me.CloseButton1.Location = New System.Drawing.Point(579, 1)
        Me.CloseButton1.Name = "CloseButton1"
        Me.CloseButton1.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton1.TabIndex = 70
        '
        'M1
        '
        Me.M1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M1.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M1.Enabled = False
        Me.M1.FillColor = System.Drawing.Color.Transparent
        Me.M1.IconColor = System.Drawing.Color.White
        Me.M1.Location = New System.Drawing.Point(553, 1)
        Me.M1.Name = "M1"
        Me.M1.Size = New System.Drawing.Size(20, 20)
        Me.M1.TabIndex = 71
        '
        'Container2_1280x840
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1235, 685)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.Guna2Panel11)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Container2_1280x840"
        Me.Text = "Container2"
        Me.Guna2Panel11.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents Title2 As Label
    Public WithEvents Title1 As Label
    Public WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Public WithEvents Guna2Panel11 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel2 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2CustomGradientPanel11 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents BtnAdd1 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAdd2 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents CloseButton1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents CloseButton2 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M2 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Maximize1 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Maximize2 As Guna.UI2.WinForms.Guna2Button
End Class
