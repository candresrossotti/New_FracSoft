﻿Public Class Container2_1280x840
    Private Sub Container2_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ''Centramos el boton de add
        CenterObject(BtnAdd1)
        CenterObject(BtnAdd2)

        ''Centramos el resto de los controles
        CenterEverything()

        ''Centramos verticalmente el control de add
        BtnAdd1.Top = (BtnAdd1.Parent.Height \ 2) - (BtnAdd1.Height \ 2)
        BtnAdd2.Top = (BtnAdd2.Parent.Height \ 2) - (BtnAdd2.Height \ 2)

    End Sub

    ''Sub para centrar/alinear todos los objetos
    Public Sub CenterEverything()
        CenterObject(Title1)
        CenterObject(Title2)
    End Sub

    ''Sub para agregar fracturador a la pantalla
    Private Sub BtnAdd(sender As Object, e As EventArgs) Handles BtnAdd1.Click, BtnAdd2.Click
        If IFracSystem.ActiveOP = False Then
            If FracSelect.ShowDialog(Me) = DialogResult.OK Then
                Dim CurrentScreen As Integer = GetInteger(sender.Name)

                Dim MaximizeButton As Control() = Me.Controls.Find("Maximize" + CurrentScreen.ToString(), True)
                MaximizeButton(0).Enabled = True

                Dim CloseButton As Control() = Me.Controls.Find("CloseButton" + CurrentScreen.ToString, True)
                CloseButton(0).Enabled = True

                Dim AnotherFrac = TwoFrac.GetClass()
                Dim CurrentPanel As Control() = Me.Controls.Find("Panel" + CurrentScreen.ToString(), True)
                CurrentPanel(0).Controls.Add(AnotherFrac)
                AnotherFrac.Location = New Point(1, 0)
                LSelect.TwoFracLanguageSet(AnotherFrac)
                AnotherFrac.Show()

                sender.Visible = False

                Dim Title As Control() = Me.Controls.Find("Title" + CurrentScreen.ToString(), True)
                Title(0).Text = CurrentFracName
                CenterObject(Title(0))
            End If
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TrabajoActivo")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
        End If
    End Sub

    ''Sub para cerrar y desconectar un fracturador
    Private Sub CloseButton(sender As Object, e As EventArgs) Handles CloseButton1.Click, CloseButton2.Click
        If IFracSystem.ActiveOP = False Then
            Dim CurrentScreen As Integer = GetInteger(sender.Name)
            Dim CurrentPanel As Control() = Me.Controls.Find("Panel" + CurrentScreen.ToString(), True)
            If CurrentPanel(0).Controls.Find("TwoFrac_" + My.Settings.Resolucion, True).Length() <> 0 Then
                Dim CurrentFrac As Control() = CurrentPanel(0).Controls.Find("TwoFrac_" + My.Settings.Resolucion, True)
                Dim C1 As Object = CurrentFrac(0)

                ISystem.Disconnect(C1.Frac)
                C1.StopTimers()

                Dim MaximizeButton As Control() = Me.Controls.Find("Maximize" + CurrentScreen.ToString(), True)
                MaximizeButton(0).Enabled = False
                sender.Enabled = False

                CurrentPanel(0).Controls.RemoveAt(1)

                Dim BtnAdd As Control() = Me.Controls.Find("BtnAdd" + CurrentScreen.ToString(), True)
                BtnAdd(0).Visible = True

                Dim Title As Control() = Me.Controls.Find("Title" + CurrentScreen.ToString(), True)
                Title(0).Text = LanguageSelect.LanguageRM.GetString("Container.Title")
                CenterObject(Title(0))

                If Not GetDuplicateFracs(C1.Frac) Then
                    MaxFracs -= 1
                    FracData.BoolVFrac("ActiveFrac_" + C1.Frac.ToString()) = False
                End If
                ConnectedFracsID.Remove(C1.Frac)

            End If
        Else
            avisoStr = LanguageSelect.DynamicLanguageRM.GetString("TrabajoActivo")
            avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            AvisoGeneral.ShowDialog(Me)
        End If

    End Sub

    ''FormClose sub, para no cerrar el programa con el boton de cerrar fracturador
    Private Sub Formclose(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
    End Sub


    ''Sub para ir a OneFracMaximize y ver mejor todo
    Private Sub Maximize1_Click(sender As Object, e As EventArgs) Handles Maximize1.Click, Maximize2.Click
        Dim CurrentOne As Integer = GetInteger(sender.Name)
        Dim CurrentPanel As Control() = Me.Controls.Find("Panel" + CurrentOne.ToString(), True)
        Dim TwoFrac As Object = CurrentPanel(0).Controls(1)
        currentFrac = TwoFrac.Frac
        CurrentFracName = ISystem.GetFracName(currentFrac)
        MaximizeForm.Show(Me)
    End Sub

    ''IDLE all frac
    Private Sub BtnIdleALL_Click(sender As Object, e As EventArgs)
        Dim IdleALLTh As New Task(AddressOf IdleALL)                                      ''IdleALL Funcion en FuncionesGenerales.vb
        IdleALLTh.Start()
    End Sub
End Class