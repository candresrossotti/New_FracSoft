﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TwoFrac_1280x840
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2Panel11 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnEmpezarTrabajo = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnSemiAuto = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnEncenderLuces = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel11 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblOpciones = New System.Windows.Forms.Label()
        Me.BtnDiagnostico = New Guna.UI2.WinForms.Guna2Button()
        Me.LblErrorAdmisible = New System.Windows.Forms.Label()
        Me.LblErrorGrave = New System.Windows.Forms.Label()
        Me.Guna2Panel9 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BtnTripZero = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnTrip = New Guna.UI2.WinForms.Guna2Button()
        Me.LblPCorteActual = New System.Windows.Forms.Label()
        Me.LblPCorteDeseada = New System.Windows.Forms.Label()
        Me.LblPCorte3 = New System.Windows.Forms.Label()
        Me.LblPCorte1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel9 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblPCorte = New System.Windows.Forms.Label()
        Me.Guna2Panel10 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.LblCaudalActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel10 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCaudal = New System.Windows.Forms.Label()
        Me.Guna2Panel7 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.LblBombaHHP = New System.Windows.Forms.Label()
        Me.LblHHP = New System.Windows.Forms.Label()
        Me.LblBombaCargaMotor = New System.Windows.Forms.Label()
        Me.LblBombaPresSuccion = New System.Windows.Forms.Label()
        Me.LblCargaMotor = New System.Windows.Forms.Label()
        Me.LblPresSuc = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.LblBombaHorasMotor = New System.Windows.Forms.Label()
        Me.LblHM = New System.Windows.Forms.Label()
        Me.LblBombaPresLUB = New System.Windows.Forms.Label()
        Me.LblBombaTempLUB = New System.Windows.Forms.Label()
        Me.LblPresLub = New System.Windows.Forms.Label()
        Me.LblTempLub = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel7 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblBomba = New System.Windows.Forms.Label()
        Me.Guna2Panel5 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.LblTransRpmOut = New System.Windows.Forms.Label()
        Me.LblTrans5 = New System.Windows.Forms.Label()
        Me.LblTransPresion = New System.Windows.Forms.Label()
        Me.LblTransTemp = New System.Windows.Forms.Label()
        Me.LblTrans3 = New System.Windows.Forms.Label()
        Me.LblTrans1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel5 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblTransmision = New System.Windows.Forms.Label()
        Me.Guna2Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.LblMotorCombustibleP100 = New System.Windows.Forms.Label()
        Me.LblCombustible1 = New System.Windows.Forms.Label()
        Me.LblMotorCombustible = New System.Windows.Forms.Label()
        Me.LblMotorTempAceite = New System.Windows.Forms.Label()
        Me.LblCombustible = New System.Windows.Forms.Label()
        Me.LblTempAceite = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.LblMotorVoltaje = New System.Windows.Forms.Label()
        Me.LblVoltaje = New System.Windows.Forms.Label()
        Me.LblMotorPresion = New System.Windows.Forms.Label()
        Me.LblMotorTempAgua = New System.Windows.Forms.Label()
        Me.LblPresion = New System.Windows.Forms.Label()
        Me.LblTempAgua = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMotor = New System.Windows.Forms.Label()
        Me.BtnMinusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPlusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMarchaActual = New System.Windows.Forms.Label()
        Me.LblMarchaDeseada = New System.Windows.Forms.Label()
        Me.LblMarcha3 = New System.Windows.Forms.Label()
        Me.LblMarcha1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMarcha = New System.Windows.Forms.Label()
        Me.BtnRpmMinus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmMinus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.PanelRPM = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnIdle = New Guna.UI2.WinForms.Guna2Button()
        Me.LblRPMActual = New System.Windows.Forms.Label()
        Me.LblRPMDeseada = New System.Windows.Forms.Label()
        Me.LblRPM3 = New System.Windows.Forms.Label()
        Me.LblRPM1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblRpm = New System.Windows.Forms.Label()
        Me.BtnErrores = New Guna.UI2.WinForms.Guna2Button()
        Me.TmrRead = New System.Windows.Forms.Timer(Me.components)
        Me.TmrCheck = New System.Windows.Forms.Timer(Me.components)
        Me.BtnAjustes = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnStartEngine = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnFC = New Guna.UI2.WinForms.Guna2Button()
        Me.PbAutomatic = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.TmrReconnect = New System.Windows.Forms.Timer(Me.components)
        Me.TmrErrorHandle = New System.Windows.Forms.Timer(Me.components)
        Me.BtnResetRPM = New Guna.UI2.WinForms.Guna2Button()
        Me.TmrOp = New System.Windows.Forms.Timer(Me.components)
        Me.TmrBackUp = New System.Windows.Forms.Timer(Me.components)
        Me.BtnEKill = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel11.SuspendLayout()
        Me.Guna2CustomGradientPanel11.SuspendLayout()
        Me.Guna2Panel9.SuspendLayout()
        Me.Guna2CustomGradientPanel9.SuspendLayout()
        Me.Guna2Panel10.SuspendLayout()
        Me.Guna2CustomGradientPanel10.SuspendLayout()
        Me.Guna2Panel7.SuspendLayout()
        Me.Guna2CustomGradientPanel7.SuspendLayout()
        Me.Guna2Panel5.SuspendLayout()
        Me.Guna2CustomGradientPanel5.SuspendLayout()
        Me.Guna2Panel4.SuspendLayout()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.PanelRPM.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Guna2Panel11
        '
        Me.Guna2Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.BorderRadius = 6
        Me.Guna2Panel11.BorderThickness = 1
        Me.Guna2Panel11.Controls.Add(Me.BtnEmpezarTrabajo)
        Me.Guna2Panel11.Controls.Add(Me.BtnSemiAuto)
        Me.Guna2Panel11.Controls.Add(Me.BtnEncenderLuces)
        Me.Guna2Panel11.Controls.Add(Me.Guna2CustomGradientPanel11)
        Me.Guna2Panel11.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.Location = New System.Drawing.Point(408, 104)
        Me.Guna2Panel11.Name = "Guna2Panel11"
        Me.Guna2Panel11.Size = New System.Drawing.Size(176, 241)
        Me.Guna2Panel11.TabIndex = 87
        '
        'BtnEmpezarTrabajo
        '
        Me.BtnEmpezarTrabajo.Animated = True
        Me.BtnEmpezarTrabajo.BorderColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.BorderRadius = 5
        Me.BtnEmpezarTrabajo.BorderThickness = 1
        Me.BtnEmpezarTrabajo.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEmpezarTrabajo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEmpezarTrabajo.FillColor = System.Drawing.Color.Transparent
        Me.BtnEmpezarTrabajo.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnEmpezarTrabajo.ForeColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.Location = New System.Drawing.Point(11, 45)
        Me.BtnEmpezarTrabajo.Name = "BtnEmpezarTrabajo"
        Me.BtnEmpezarTrabajo.Size = New System.Drawing.Size(156, 36)
        Me.BtnEmpezarTrabajo.TabIndex = 54
        Me.BtnEmpezarTrabajo.Text = "Empezar Trabajo"
        '
        'BtnSemiAuto
        '
        Me.BtnSemiAuto.Animated = True
        Me.BtnSemiAuto.BorderColor = System.Drawing.Color.White
        Me.BtnSemiAuto.BorderRadius = 5
        Me.BtnSemiAuto.BorderThickness = 1
        Me.BtnSemiAuto.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSemiAuto.FillColor = System.Drawing.Color.Transparent
        Me.BtnSemiAuto.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnSemiAuto.ForeColor = System.Drawing.Color.White
        Me.BtnSemiAuto.Location = New System.Drawing.Point(11, 192)
        Me.BtnSemiAuto.Name = "BtnSemiAuto"
        Me.BtnSemiAuto.Size = New System.Drawing.Size(156, 36)
        Me.BtnSemiAuto.TabIndex = 53
        Me.BtnSemiAuto.Text = "Modo Semi-Automatico"
        '
        'BtnEncenderLuces
        '
        Me.BtnEncenderLuces.Animated = True
        Me.BtnEncenderLuces.BorderColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.BorderRadius = 5
        Me.BtnEncenderLuces.BorderThickness = 1
        Me.BtnEncenderLuces.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEncenderLuces.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEncenderLuces.FillColor = System.Drawing.Color.Transparent
        Me.BtnEncenderLuces.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnEncenderLuces.ForeColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.Location = New System.Drawing.Point(11, 120)
        Me.BtnEncenderLuces.Name = "BtnEncenderLuces"
        Me.BtnEncenderLuces.Size = New System.Drawing.Size(156, 36)
        Me.BtnEncenderLuces.TabIndex = 52
        Me.BtnEncenderLuces.Text = "Encender Luces"
        '
        'Guna2CustomGradientPanel11
        '
        Me.Guna2CustomGradientPanel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.BorderRadius = 6
        Me.Guna2CustomGradientPanel11.Controls.Add(Me.LblOpciones)
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel11.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel11.Name = "Guna2CustomGradientPanel11"
        Me.Guna2CustomGradientPanel11.Size = New System.Drawing.Size(176, 26)
        Me.Guna2CustomGradientPanel11.TabIndex = 19
        '
        'LblOpciones
        '
        Me.LblOpciones.AutoSize = True
        Me.LblOpciones.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblOpciones.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblOpciones.Location = New System.Drawing.Point(64, 4)
        Me.LblOpciones.Name = "LblOpciones"
        Me.LblOpciones.Size = New System.Drawing.Size(64, 16)
        Me.LblOpciones.TabIndex = 19
        Me.LblOpciones.Text = "Opciones"
        '
        'BtnDiagnostico
        '
        Me.BtnDiagnostico.Animated = True
        Me.BtnDiagnostico.BorderColor = System.Drawing.Color.White
        Me.BtnDiagnostico.BorderRadius = 5
        Me.BtnDiagnostico.BorderThickness = 1
        Me.BtnDiagnostico.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDiagnostico.FillColor = System.Drawing.Color.Transparent
        Me.BtnDiagnostico.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnDiagnostico.ForeColor = System.Drawing.Color.White
        Me.BtnDiagnostico.Location = New System.Drawing.Point(426, 357)
        Me.BtnDiagnostico.Name = "BtnDiagnostico"
        Me.BtnDiagnostico.Size = New System.Drawing.Size(140, 30)
        Me.BtnDiagnostico.TabIndex = 85
        Me.BtnDiagnostico.Text = "Diagnóstico"
        '
        'LblErrorAdmisible
        '
        Me.LblErrorAdmisible.AutoSize = True
        Me.LblErrorAdmisible.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblErrorAdmisible.ForeColor = System.Drawing.Color.Orange
        Me.LblErrorAdmisible.Location = New System.Drawing.Point(249, 8)
        Me.LblErrorAdmisible.Name = "LblErrorAdmisible"
        Me.LblErrorAdmisible.Size = New System.Drawing.Size(101, 16)
        Me.LblErrorAdmisible.TabIndex = 83
        Me.LblErrorAdmisible.Text = "Error Admisible"
        Me.LblErrorAdmisible.Visible = False
        '
        'LblErrorGrave
        '
        Me.LblErrorGrave.AutoSize = True
        Me.LblErrorGrave.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblErrorGrave.ForeColor = System.Drawing.Color.Red
        Me.LblErrorGrave.Location = New System.Drawing.Point(166, 8)
        Me.LblErrorGrave.Name = "LblErrorGrave"
        Me.LblErrorGrave.Size = New System.Drawing.Size(75, 16)
        Me.LblErrorGrave.TabIndex = 80
        Me.LblErrorGrave.Text = "Error Grave"
        Me.LblErrorGrave.Visible = False
        '
        'Guna2Panel9
        '
        Me.Guna2Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.BorderRadius = 6
        Me.Guna2Panel9.BorderThickness = 1
        Me.Guna2Panel9.Controls.Add(Me.Label2)
        Me.Guna2Panel9.Controls.Add(Me.Label1)
        Me.Guna2Panel9.Controls.Add(Me.BtnTripZero)
        Me.Guna2Panel9.Controls.Add(Me.BtnTrip)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorteActual)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorteDeseada)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorte3)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorte1)
        Me.Guna2Panel9.Controls.Add(Me.Guna2CustomGradientPanel9)
        Me.Guna2Panel9.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.Location = New System.Drawing.Point(233, 104)
        Me.Guna2Panel9.Name = "Guna2Panel9"
        Me.Guna2Panel9.Size = New System.Drawing.Size(135, 241)
        Me.Guna2Panel9.TabIndex = 82
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(100, 169)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 16)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "PSI"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(100, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 16)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "PSI"
        '
        'BtnTripZero
        '
        Me.BtnTripZero.Animated = True
        Me.BtnTripZero.BorderColor = System.Drawing.Color.White
        Me.BtnTripZero.BorderRadius = 5
        Me.BtnTripZero.BorderThickness = 1
        Me.BtnTripZero.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTripZero.FillColor = System.Drawing.Color.Transparent
        Me.BtnTripZero.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnTripZero.ForeColor = System.Drawing.Color.White
        Me.BtnTripZero.Location = New System.Drawing.Point(20, 198)
        Me.BtnTripZero.Name = "BtnTripZero"
        Me.BtnTripZero.Size = New System.Drawing.Size(100, 30)
        Me.BtnTripZero.TabIndex = 53
        Me.BtnTripZero.Text = "ZERO"
        '
        'BtnTrip
        '
        Me.BtnTrip.Animated = True
        Me.BtnTrip.BorderColor = System.Drawing.Color.White
        Me.BtnTrip.BorderRadius = 5
        Me.BtnTrip.BorderThickness = 1
        Me.BtnTrip.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTrip.FillColor = System.Drawing.Color.Transparent
        Me.BtnTrip.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnTrip.ForeColor = System.Drawing.Color.White
        Me.BtnTrip.Location = New System.Drawing.Point(20, 90)
        Me.BtnTrip.Name = "BtnTrip"
        Me.BtnTrip.Size = New System.Drawing.Size(100, 30)
        Me.BtnTrip.TabIndex = 52
        Me.BtnTrip.Text = "TRIP"
        '
        'LblPCorteActual
        '
        Me.LblPCorteActual.AutoSize = True
        Me.LblPCorteActual.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblPCorteActual.ForeColor = System.Drawing.Color.White
        Me.LblPCorteActual.Location = New System.Drawing.Point(33, 158)
        Me.LblPCorteActual.Name = "LblPCorteActual"
        Me.LblPCorteActual.Size = New System.Drawing.Size(20, 27)
        Me.LblPCorteActual.TabIndex = 23
        Me.LblPCorteActual.Text = "-"
        '
        'LblPCorteDeseada
        '
        Me.LblPCorteDeseada.AutoSize = True
        Me.LblPCorteDeseada.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPCorteDeseada.ForeColor = System.Drawing.Color.White
        Me.LblPCorteDeseada.Location = New System.Drawing.Point(51, 60)
        Me.LblPCorteDeseada.Name = "LblPCorteDeseada"
        Me.LblPCorteDeseada.Size = New System.Drawing.Size(12, 16)
        Me.LblPCorteDeseada.TabIndex = 22
        Me.LblPCorteDeseada.Text = "-"
        '
        'LblPCorte3
        '
        Me.LblPCorte3.AutoSize = True
        Me.LblPCorte3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPCorte3.ForeColor = System.Drawing.Color.White
        Me.LblPCorte3.Location = New System.Drawing.Point(27, 140)
        Me.LblPCorte3.Name = "LblPCorte3"
        Me.LblPCorte3.Size = New System.Drawing.Size(95, 16)
        Me.LblPCorte3.TabIndex = 21
        Me.LblPCorte3.Text = "Presión Actual"
        '
        'LblPCorte1
        '
        Me.LblPCorte1.AutoSize = True
        Me.LblPCorte1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPCorte1.ForeColor = System.Drawing.Color.White
        Me.LblPCorte1.Location = New System.Drawing.Point(20, 32)
        Me.LblPCorte1.Name = "LblPCorte1"
        Me.LblPCorte1.Size = New System.Drawing.Size(108, 16)
        Me.LblPCorte1.TabIndex = 20
        Me.LblPCorte1.Text = "Presión de Corte"
        '
        'Guna2CustomGradientPanel9
        '
        Me.Guna2CustomGradientPanel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.BorderRadius = 6
        Me.Guna2CustomGradientPanel9.Controls.Add(Me.LblPCorte)
        Me.Guna2CustomGradientPanel9.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel9.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel9.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel9.Name = "Guna2CustomGradientPanel9"
        Me.Guna2CustomGradientPanel9.Size = New System.Drawing.Size(135, 26)
        Me.Guna2CustomGradientPanel9.TabIndex = 19
        '
        'LblPCorte
        '
        Me.LblPCorte.AutoSize = True
        Me.LblPCorte.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPCorte.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblPCorte.Location = New System.Drawing.Point(20, 5)
        Me.LblPCorte.Name = "LblPCorte"
        Me.LblPCorte.Size = New System.Drawing.Size(108, 16)
        Me.LblPCorte.TabIndex = 19
        Me.LblPCorte.Text = "Presión de Corte"
        '
        'Guna2Panel10
        '
        Me.Guna2Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.BorderRadius = 6
        Me.Guna2Panel10.BorderThickness = 1
        Me.Guna2Panel10.Controls.Add(Me.Label81)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudalActual)
        Me.Guna2Panel10.Controls.Add(Me.Guna2CustomGradientPanel10)
        Me.Guna2Panel10.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.Location = New System.Drawing.Point(233, 29)
        Me.Guna2Panel10.Name = "Guna2Panel10"
        Me.Guna2Panel10.Size = New System.Drawing.Size(135, 69)
        Me.Guna2Panel10.TabIndex = 81
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label81.ForeColor = System.Drawing.Color.White
        Me.Label81.Location = New System.Drawing.Point(71, 37)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(58, 16)
        Me.Label81.TabIndex = 23
        Me.Label81.Text = "BBL/Min"
        '
        'LblCaudalActual
        '
        Me.LblCaudalActual.AutoSize = True
        Me.LblCaudalActual.Font = New System.Drawing.Font("Montserrat", 10.0!)
        Me.LblCaudalActual.ForeColor = System.Drawing.Color.White
        Me.LblCaudalActual.Location = New System.Drawing.Point(34, 35)
        Me.LblCaudalActual.Name = "LblCaudalActual"
        Me.LblCaudalActual.Size = New System.Drawing.Size(14, 20)
        Me.LblCaudalActual.TabIndex = 22
        Me.LblCaudalActual.Text = "-"
        '
        'Guna2CustomGradientPanel10
        '
        Me.Guna2CustomGradientPanel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.BorderRadius = 6
        Me.Guna2CustomGradientPanel10.Controls.Add(Me.LblCaudal)
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel10.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel10.Name = "Guna2CustomGradientPanel10"
        Me.Guna2CustomGradientPanel10.Size = New System.Drawing.Size(135, 26)
        Me.Guna2CustomGradientPanel10.TabIndex = 19
        '
        'LblCaudal
        '
        Me.LblCaudal.AutoSize = True
        Me.LblCaudal.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblCaudal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCaudal.Location = New System.Drawing.Point(47, 4)
        Me.LblCaudal.Name = "LblCaudal"
        Me.LblCaudal.Size = New System.Drawing.Size(49, 16)
        Me.LblCaudal.TabIndex = 19
        Me.LblCaudal.Text = "Caudal"
        '
        'Guna2Panel7
        '
        Me.Guna2Panel7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel7.BorderRadius = 6
        Me.Guna2Panel7.BorderThickness = 1
        Me.Guna2Panel7.Controls.Add(Me.Label54)
        Me.Guna2Panel7.Controls.Add(Me.Label55)
        Me.Guna2Panel7.Controls.Add(Me.Label56)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaHHP)
        Me.Guna2Panel7.Controls.Add(Me.LblHHP)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaCargaMotor)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaPresSuccion)
        Me.Guna2Panel7.Controls.Add(Me.LblCargaMotor)
        Me.Guna2Panel7.Controls.Add(Me.LblPresSuc)
        Me.Guna2Panel7.Controls.Add(Me.Label63)
        Me.Guna2Panel7.Controls.Add(Me.Label64)
        Me.Guna2Panel7.Controls.Add(Me.Label65)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaHorasMotor)
        Me.Guna2Panel7.Controls.Add(Me.LblHM)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaPresLUB)
        Me.Guna2Panel7.Controls.Add(Me.LblBombaTempLUB)
        Me.Guna2Panel7.Controls.Add(Me.LblPresLub)
        Me.Guna2Panel7.Controls.Add(Me.LblTempLub)
        Me.Guna2Panel7.Controls.Add(Me.Guna2CustomGradientPanel7)
        Me.Guna2Panel7.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel7.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel7.Location = New System.Drawing.Point(404, 401)
        Me.Guna2Panel7.Name = "Guna2Panel7"
        Me.Guna2Panel7.Size = New System.Drawing.Size(185, 229)
        Me.Guna2Panel7.TabIndex = 70
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(132, 204)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(36, 16)
        Me.Label54.TabIndex = 37
        Me.Label54.Text = "HHP"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(132, 138)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(17, 16)
        Me.Label55.TabIndex = 36
        Me.Label55.Text = "%"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label56.ForeColor = System.Drawing.Color.White
        Me.Label56.Location = New System.Drawing.Point(132, 71)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(21, 16)
        Me.Label56.TabIndex = 35
        Me.Label56.Text = "ºC"
        '
        'LblBombaHHP
        '
        Me.LblBombaHHP.AutoSize = True
        Me.LblBombaHHP.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblBombaHHP.ForeColor = System.Drawing.Color.White
        Me.LblBombaHHP.Location = New System.Drawing.Point(87, 204)
        Me.LblBombaHHP.Name = "LblBombaHHP"
        Me.LblBombaHHP.Size = New System.Drawing.Size(12, 16)
        Me.LblBombaHHP.TabIndex = 34
        Me.LblBombaHHP.Text = "-"
        '
        'LblHHP
        '
        Me.LblHHP.AutoSize = True
        Me.LblHHP.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblHHP.ForeColor = System.Drawing.Color.White
        Me.LblHHP.Location = New System.Drawing.Point(87, 171)
        Me.LblHHP.Name = "LblHHP"
        Me.LblHHP.Size = New System.Drawing.Size(36, 16)
        Me.LblHHP.TabIndex = 33
        Me.LblHHP.Text = "HHP"
        '
        'LblBombaCargaMotor
        '
        Me.LblBombaCargaMotor.AutoSize = True
        Me.LblBombaCargaMotor.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblBombaCargaMotor.ForeColor = System.Drawing.Color.White
        Me.LblBombaCargaMotor.Location = New System.Drawing.Point(87, 138)
        Me.LblBombaCargaMotor.Name = "LblBombaCargaMotor"
        Me.LblBombaCargaMotor.Size = New System.Drawing.Size(12, 16)
        Me.LblBombaCargaMotor.TabIndex = 32
        Me.LblBombaCargaMotor.Text = "-"
        '
        'LblBombaPresSuccion
        '
        Me.LblBombaPresSuccion.AutoSize = True
        Me.LblBombaPresSuccion.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblBombaPresSuccion.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresSuccion.Location = New System.Drawing.Point(87, 71)
        Me.LblBombaPresSuccion.Name = "LblBombaPresSuccion"
        Me.LblBombaPresSuccion.Size = New System.Drawing.Size(12, 16)
        Me.LblBombaPresSuccion.TabIndex = 31
        Me.LblBombaPresSuccion.Text = "-"
        '
        'LblCargaMotor
        '
        Me.LblCargaMotor.AutoSize = True
        Me.LblCargaMotor.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblCargaMotor.ForeColor = System.Drawing.Color.White
        Me.LblCargaMotor.Location = New System.Drawing.Point(87, 103)
        Me.LblCargaMotor.Name = "LblCargaMotor"
        Me.LblCargaMotor.Size = New System.Drawing.Size(96, 16)
        Me.LblCargaMotor.TabIndex = 30
        Me.LblCargaMotor.Text = "% Carga Motor"
        '
        'LblPresSuc
        '
        Me.LblPresSuc.AutoSize = True
        Me.LblPresSuc.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPresSuc.ForeColor = System.Drawing.Color.White
        Me.LblPresSuc.Location = New System.Drawing.Point(87, 38)
        Me.LblPresSuc.Name = "LblPresSuc"
        Me.LblPresSuc.Size = New System.Drawing.Size(85, 16)
        Me.LblPresSuc.TabIndex = 29
        Me.LblPresSuc.Text = "Pres Succión"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label63.ForeColor = System.Drawing.Color.White
        Me.Label63.Location = New System.Drawing.Point(41, 204)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(22, 16)
        Me.Label63.TabIndex = 28
        Me.Label63.Text = "Hr"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(38, 138)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(27, 16)
        Me.Label64.TabIndex = 27
        Me.Label64.Text = "PSI"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(41, 71)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(21, 16)
        Me.Label65.TabIndex = 26
        Me.Label65.Text = "ºC"
        '
        'LblBombaHorasMotor
        '
        Me.LblBombaHorasMotor.AutoSize = True
        Me.LblBombaHorasMotor.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblBombaHorasMotor.ForeColor = System.Drawing.Color.White
        Me.LblBombaHorasMotor.Location = New System.Drawing.Point(8, 204)
        Me.LblBombaHorasMotor.Name = "LblBombaHorasMotor"
        Me.LblBombaHorasMotor.Size = New System.Drawing.Size(12, 16)
        Me.LblBombaHorasMotor.TabIndex = 25
        Me.LblBombaHorasMotor.Text = "-"
        '
        'LblHM
        '
        Me.LblHM.AutoSize = True
        Me.LblHM.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblHM.ForeColor = System.Drawing.Color.White
        Me.LblHM.Location = New System.Drawing.Point(8, 171)
        Me.LblHM.Name = "LblHM"
        Me.LblHM.Size = New System.Drawing.Size(66, 16)
        Me.LblHM.TabIndex = 24
        Me.LblHM.Text = "Hs. Motor"
        '
        'LblBombaPresLUB
        '
        Me.LblBombaPresLUB.AutoSize = True
        Me.LblBombaPresLUB.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblBombaPresLUB.ForeColor = System.Drawing.Color.White
        Me.LblBombaPresLUB.Location = New System.Drawing.Point(8, 138)
        Me.LblBombaPresLUB.Name = "LblBombaPresLUB"
        Me.LblBombaPresLUB.Size = New System.Drawing.Size(12, 16)
        Me.LblBombaPresLUB.TabIndex = 23
        Me.LblBombaPresLUB.Text = "-"
        '
        'LblBombaTempLUB
        '
        Me.LblBombaTempLUB.AutoSize = True
        Me.LblBombaTempLUB.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblBombaTempLUB.ForeColor = System.Drawing.Color.White
        Me.LblBombaTempLUB.Location = New System.Drawing.Point(8, 71)
        Me.LblBombaTempLUB.Name = "LblBombaTempLUB"
        Me.LblBombaTempLUB.Size = New System.Drawing.Size(12, 16)
        Me.LblBombaTempLUB.TabIndex = 22
        Me.LblBombaTempLUB.Text = "-"
        '
        'LblPresLub
        '
        Me.LblPresLub.AutoSize = True
        Me.LblPresLub.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPresLub.ForeColor = System.Drawing.Color.White
        Me.LblPresLub.Location = New System.Drawing.Point(8, 103)
        Me.LblPresLub.Name = "LblPresLub"
        Me.LblPresLub.Size = New System.Drawing.Size(62, 16)
        Me.LblPresLub.TabIndex = 21
        Me.LblPresLub.Text = "Pres LUB"
        '
        'LblTempLub
        '
        Me.LblTempLub.AutoSize = True
        Me.LblTempLub.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTempLub.ForeColor = System.Drawing.Color.White
        Me.LblTempLub.Location = New System.Drawing.Point(8, 38)
        Me.LblTempLub.Name = "LblTempLub"
        Me.LblTempLub.Size = New System.Drawing.Size(69, 16)
        Me.LblTempLub.TabIndex = 20
        Me.LblTempLub.Text = "Temp LUB"
        '
        'Guna2CustomGradientPanel7
        '
        Me.Guna2CustomGradientPanel7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.BorderRadius = 6
        Me.Guna2CustomGradientPanel7.Controls.Add(Me.LblBomba)
        Me.Guna2CustomGradientPanel7.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel7.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel7.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel7.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel7.Name = "Guna2CustomGradientPanel7"
        Me.Guna2CustomGradientPanel7.Size = New System.Drawing.Size(185, 26)
        Me.Guna2CustomGradientPanel7.TabIndex = 19
        '
        'LblBomba
        '
        Me.LblBomba.AutoSize = True
        Me.LblBomba.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblBomba.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblBomba.Location = New System.Drawing.Point(66, 4)
        Me.LblBomba.Name = "LblBomba"
        Me.LblBomba.Size = New System.Drawing.Size(52, 16)
        Me.LblBomba.TabIndex = 19
        Me.LblBomba.Text = "Bomba"
        '
        'Guna2Panel5
        '
        Me.Guna2Panel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.BorderRadius = 6
        Me.Guna2Panel5.BorderThickness = 1
        Me.Guna2Panel5.Controls.Add(Me.Label41)
        Me.Guna2Panel5.Controls.Add(Me.Label42)
        Me.Guna2Panel5.Controls.Add(Me.Label43)
        Me.Guna2Panel5.Controls.Add(Me.LblTransRpmOut)
        Me.Guna2Panel5.Controls.Add(Me.LblTrans5)
        Me.Guna2Panel5.Controls.Add(Me.LblTransPresion)
        Me.Guna2Panel5.Controls.Add(Me.LblTransTemp)
        Me.Guna2Panel5.Controls.Add(Me.LblTrans3)
        Me.Guna2Panel5.Controls.Add(Me.LblTrans1)
        Me.Guna2Panel5.Controls.Add(Me.Guna2CustomGradientPanel5)
        Me.Guna2Panel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.Location = New System.Drawing.Point(208, 401)
        Me.Guna2Panel5.Name = "Guna2Panel5"
        Me.Guna2Panel5.Size = New System.Drawing.Size(185, 229)
        Me.Guna2Panel5.TabIndex = 69
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(133, 202)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(36, 16)
        Me.Label41.TabIndex = 28
        Me.Label41.Text = "RPM"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(133, 138)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(27, 16)
        Me.Label42.TabIndex = 27
        Me.Label42.Text = "PSI"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label43.ForeColor = System.Drawing.Color.White
        Me.Label43.Location = New System.Drawing.Point(133, 71)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(21, 16)
        Me.Label43.TabIndex = 26
        Me.Label43.Text = "ºC"
        '
        'LblTransRpmOut
        '
        Me.LblTransRpmOut.AutoSize = True
        Me.LblTransRpmOut.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTransRpmOut.ForeColor = System.Drawing.Color.White
        Me.LblTransRpmOut.Location = New System.Drawing.Point(77, 204)
        Me.LblTransRpmOut.Name = "LblTransRpmOut"
        Me.LblTransRpmOut.Size = New System.Drawing.Size(12, 16)
        Me.LblTransRpmOut.TabIndex = 25
        Me.LblTransRpmOut.Text = "-"
        '
        'LblTrans5
        '
        Me.LblTrans5.AutoSize = True
        Me.LblTrans5.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTrans5.ForeColor = System.Drawing.Color.White
        Me.LblTrans5.Location = New System.Drawing.Point(51, 171)
        Me.LblTrans5.Name = "LblTrans5"
        Me.LblTrans5.Size = New System.Drawing.Size(62, 16)
        Me.LblTrans5.TabIndex = 24
        Me.LblTrans5.Text = "RPM Out"
        '
        'LblTransPresion
        '
        Me.LblTransPresion.AutoSize = True
        Me.LblTransPresion.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTransPresion.ForeColor = System.Drawing.Color.White
        Me.LblTransPresion.Location = New System.Drawing.Point(76, 138)
        Me.LblTransPresion.Name = "LblTransPresion"
        Me.LblTransPresion.Size = New System.Drawing.Size(12, 16)
        Me.LblTransPresion.TabIndex = 23
        Me.LblTransPresion.Text = "-"
        '
        'LblTransTemp
        '
        Me.LblTransTemp.AutoSize = True
        Me.LblTransTemp.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTransTemp.ForeColor = System.Drawing.Color.White
        Me.LblTransTemp.Location = New System.Drawing.Point(77, 71)
        Me.LblTransTemp.Name = "LblTransTemp"
        Me.LblTransTemp.Size = New System.Drawing.Size(12, 16)
        Me.LblTransTemp.TabIndex = 22
        Me.LblTransTemp.Text = "-"
        '
        'LblTrans3
        '
        Me.LblTrans3.AutoSize = True
        Me.LblTrans3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTrans3.ForeColor = System.Drawing.Color.White
        Me.LblTrans3.Location = New System.Drawing.Point(51, 103)
        Me.LblTrans3.Name = "LblTrans3"
        Me.LblTrans3.Size = New System.Drawing.Size(53, 16)
        Me.LblTrans3.TabIndex = 21
        Me.LblTrans3.Text = "Presión"
        '
        'LblTrans1
        '
        Me.LblTrans1.AutoSize = True
        Me.LblTrans1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTrans1.ForeColor = System.Drawing.Color.White
        Me.LblTrans1.Location = New System.Drawing.Point(51, 38)
        Me.LblTrans1.Name = "LblTrans1"
        Me.LblTrans1.Size = New System.Drawing.Size(83, 16)
        Me.LblTrans1.TabIndex = 20
        Me.LblTrans1.Text = "Temperatura"
        '
        'Guna2CustomGradientPanel5
        '
        Me.Guna2CustomGradientPanel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.BorderRadius = 6
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.LblTransmision)
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel5.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel5.Name = "Guna2CustomGradientPanel5"
        Me.Guna2CustomGradientPanel5.Size = New System.Drawing.Size(185, 26)
        Me.Guna2CustomGradientPanel5.TabIndex = 19
        '
        'LblTransmision
        '
        Me.LblTransmision.AutoSize = True
        Me.LblTransmision.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTransmision.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblTransmision.Location = New System.Drawing.Point(55, 4)
        Me.LblTransmision.Name = "LblTransmision"
        Me.LblTransmision.Size = New System.Drawing.Size(80, 16)
        Me.LblTransmision.TabIndex = 19
        Me.LblTransmision.Text = "Transmisión"
        '
        'Guna2Panel4
        '
        Me.Guna2Panel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Guna2Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.BorderRadius = 6
        Me.Guna2Panel4.BorderThickness = 1
        Me.Guna2Panel4.Controls.Add(Me.Label23)
        Me.Guna2Panel4.Controls.Add(Me.Label24)
        Me.Guna2Panel4.Controls.Add(Me.Label25)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorCombustibleP100)
        Me.Guna2Panel4.Controls.Add(Me.LblCombustible1)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorCombustible)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorTempAceite)
        Me.Guna2Panel4.Controls.Add(Me.LblCombustible)
        Me.Guna2Panel4.Controls.Add(Me.LblTempAceite)
        Me.Guna2Panel4.Controls.Add(Me.Label22)
        Me.Guna2Panel4.Controls.Add(Me.Label21)
        Me.Guna2Panel4.Controls.Add(Me.Label20)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorVoltaje)
        Me.Guna2Panel4.Controls.Add(Me.LblVoltaje)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorPresion)
        Me.Guna2Panel4.Controls.Add(Me.LblMotorTempAgua)
        Me.Guna2Panel4.Controls.Add(Me.LblPresion)
        Me.Guna2Panel4.Controls.Add(Me.LblTempAgua)
        Me.Guna2Panel4.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.Guna2Panel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel4.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel4.Location = New System.Drawing.Point(12, 401)
        Me.Guna2Panel4.Name = "Guna2Panel4"
        Me.Guna2Panel4.Size = New System.Drawing.Size(185, 229)
        Me.Guna2Panel4.TabIndex = 79
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(155, 202)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(17, 16)
        Me.Label23.TabIndex = 37
        Me.Label23.Text = "%"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(141, 136)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(33, 16)
        Me.Label24.TabIndex = 36
        Me.Label24.Text = "L/Hr"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(152, 69)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(21, 16)
        Me.Label25.TabIndex = 35
        Me.Label25.Text = "ºC"
        '
        'LblMotorCombustibleP100
        '
        Me.LblMotorCombustibleP100.AutoSize = True
        Me.LblMotorCombustibleP100.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMotorCombustibleP100.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustibleP100.Location = New System.Drawing.Point(97, 202)
        Me.LblMotorCombustibleP100.Name = "LblMotorCombustibleP100"
        Me.LblMotorCombustibleP100.Size = New System.Drawing.Size(12, 16)
        Me.LblMotorCombustibleP100.TabIndex = 34
        Me.LblMotorCombustibleP100.Text = "-"
        '
        'LblCombustible1
        '
        Me.LblCombustible1.AutoSize = True
        Me.LblCombustible1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblCombustible1.ForeColor = System.Drawing.Color.White
        Me.LblCombustible1.Location = New System.Drawing.Point(97, 169)
        Me.LblCombustible1.Name = "LblCombustible1"
        Me.LblCombustible1.Size = New System.Drawing.Size(85, 16)
        Me.LblCombustible1.TabIndex = 33
        Me.LblCombustible1.Text = "Combustible"
        '
        'LblMotorCombustible
        '
        Me.LblMotorCombustible.AutoSize = True
        Me.LblMotorCombustible.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMotorCombustible.ForeColor = System.Drawing.Color.White
        Me.LblMotorCombustible.Location = New System.Drawing.Point(97, 136)
        Me.LblMotorCombustible.Name = "LblMotorCombustible"
        Me.LblMotorCombustible.Size = New System.Drawing.Size(12, 16)
        Me.LblMotorCombustible.TabIndex = 32
        Me.LblMotorCombustible.Text = "-"
        '
        'LblMotorTempAceite
        '
        Me.LblMotorTempAceite.AutoSize = True
        Me.LblMotorTempAceite.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMotorTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAceite.Location = New System.Drawing.Point(97, 69)
        Me.LblMotorTempAceite.Name = "LblMotorTempAceite"
        Me.LblMotorTempAceite.Size = New System.Drawing.Size(12, 16)
        Me.LblMotorTempAceite.TabIndex = 31
        Me.LblMotorTempAceite.Text = "-"
        '
        'LblCombustible
        '
        Me.LblCombustible.AutoSize = True
        Me.LblCombustible.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblCombustible.ForeColor = System.Drawing.Color.White
        Me.LblCombustible.Location = New System.Drawing.Point(97, 101)
        Me.LblCombustible.Name = "LblCombustible"
        Me.LblCombustible.Size = New System.Drawing.Size(85, 16)
        Me.LblCombustible.TabIndex = 30
        Me.LblCombustible.Text = "Combustible"
        '
        'LblTempAceite
        '
        Me.LblTempAceite.AutoSize = True
        Me.LblTempAceite.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTempAceite.ForeColor = System.Drawing.Color.White
        Me.LblTempAceite.Location = New System.Drawing.Point(97, 36)
        Me.LblTempAceite.Name = "LblTempAceite"
        Me.LblTempAceite.Size = New System.Drawing.Size(82, 16)
        Me.LblTempAceite.TabIndex = 29
        Me.LblTempAceite.Text = "Temp Aceite"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(54, 202)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(15, 16)
        Me.Label22.TabIndex = 28
        Me.Label22.Text = "V"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(54, 136)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(27, 16)
        Me.Label21.TabIndex = 27
        Me.Label21.Text = "PSI"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(54, 69)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(21, 16)
        Me.Label20.TabIndex = 26
        Me.Label20.Text = "ºC"
        '
        'LblMotorVoltaje
        '
        Me.LblMotorVoltaje.AutoSize = True
        Me.LblMotorVoltaje.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMotorVoltaje.ForeColor = System.Drawing.Color.White
        Me.LblMotorVoltaje.Location = New System.Drawing.Point(10, 202)
        Me.LblMotorVoltaje.Name = "LblMotorVoltaje"
        Me.LblMotorVoltaje.Size = New System.Drawing.Size(12, 16)
        Me.LblMotorVoltaje.TabIndex = 25
        Me.LblMotorVoltaje.Text = "-"
        '
        'LblVoltaje
        '
        Me.LblVoltaje.AutoSize = True
        Me.LblVoltaje.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblVoltaje.ForeColor = System.Drawing.Color.White
        Me.LblVoltaje.Location = New System.Drawing.Point(10, 169)
        Me.LblVoltaje.Name = "LblVoltaje"
        Me.LblVoltaje.Size = New System.Drawing.Size(47, 16)
        Me.LblVoltaje.TabIndex = 24
        Me.LblVoltaje.Text = "Voltaje"
        '
        'LblMotorPresion
        '
        Me.LblMotorPresion.AutoSize = True
        Me.LblMotorPresion.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMotorPresion.ForeColor = System.Drawing.Color.White
        Me.LblMotorPresion.Location = New System.Drawing.Point(10, 136)
        Me.LblMotorPresion.Name = "LblMotorPresion"
        Me.LblMotorPresion.Size = New System.Drawing.Size(12, 16)
        Me.LblMotorPresion.TabIndex = 23
        Me.LblMotorPresion.Text = "-"
        '
        'LblMotorTempAgua
        '
        Me.LblMotorTempAgua.AutoSize = True
        Me.LblMotorTempAgua.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMotorTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblMotorTempAgua.Location = New System.Drawing.Point(10, 69)
        Me.LblMotorTempAgua.Name = "LblMotorTempAgua"
        Me.LblMotorTempAgua.Size = New System.Drawing.Size(12, 16)
        Me.LblMotorTempAgua.TabIndex = 22
        Me.LblMotorTempAgua.Text = "-"
        '
        'LblPresion
        '
        Me.LblPresion.AutoSize = True
        Me.LblPresion.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPresion.ForeColor = System.Drawing.Color.White
        Me.LblPresion.Location = New System.Drawing.Point(10, 101)
        Me.LblPresion.Name = "LblPresion"
        Me.LblPresion.Size = New System.Drawing.Size(53, 16)
        Me.LblPresion.TabIndex = 21
        Me.LblPresion.Text = "Presión"
        '
        'LblTempAgua
        '
        Me.LblTempAgua.AutoSize = True
        Me.LblTempAgua.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblTempAgua.ForeColor = System.Drawing.Color.White
        Me.LblTempAgua.Location = New System.Drawing.Point(10, 36)
        Me.LblTempAgua.Name = "LblTempAgua"
        Me.LblTempAgua.Size = New System.Drawing.Size(76, 16)
        Me.LblTempAgua.TabIndex = 20
        Me.LblTempAgua.Text = "Temp Agua"
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.LblMotor)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(185, 26)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'LblMotor
        '
        Me.LblMotor.AutoSize = True
        Me.LblMotor.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMotor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMotor.Location = New System.Drawing.Point(76, 4)
        Me.LblMotor.Name = "LblMotor"
        Me.LblMotor.Size = New System.Drawing.Size(44, 16)
        Me.LblMotor.TabIndex = 19
        Me.LblMotor.Text = "Motor"
        '
        'BtnMinusGear
        '
        Me.BtnMinusGear.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnMinusGear.Animated = True
        Me.BtnMinusGear.BorderColor = System.Drawing.Color.White
        Me.BtnMinusGear.BorderRadius = 5
        Me.BtnMinusGear.BorderThickness = 1
        Me.BtnMinusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMinusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnMinusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnMinusGear.ForeColor = System.Drawing.Color.White
        Me.BtnMinusGear.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnMinusGear.Location = New System.Drawing.Point(172, 314)
        Me.BtnMinusGear.Name = "BtnMinusGear"
        Me.BtnMinusGear.Size = New System.Drawing.Size(35, 35)
        Me.BtnMinusGear.TabIndex = 78
        '
        'BtnPlusGear
        '
        Me.BtnPlusGear.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnPlusGear.Animated = True
        Me.BtnPlusGear.BorderColor = System.Drawing.Color.White
        Me.BtnPlusGear.BorderRadius = 5
        Me.BtnPlusGear.BorderThickness = 1
        Me.BtnPlusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPlusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnPlusGear.ForeColor = System.Drawing.Color.White
        Me.BtnPlusGear.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnPlusGear.Location = New System.Drawing.Point(172, 268)
        Me.BtnPlusGear.Name = "BtnPlusGear"
        Me.BtnPlusGear.Size = New System.Drawing.Size(35, 35)
        Me.BtnPlusGear.TabIndex = 77
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaActual)
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaDeseada)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha3)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(39, 207)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(128, 154)
        Me.Guna2Panel1.TabIndex = 76
        '
        'LblMarchaActual
        '
        Me.LblMarchaActual.AutoSize = True
        Me.LblMarchaActual.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblMarchaActual.ForeColor = System.Drawing.Color.White
        Me.LblMarchaActual.Location = New System.Drawing.Point(56, 115)
        Me.LblMarchaActual.Name = "LblMarchaActual"
        Me.LblMarchaActual.Size = New System.Drawing.Size(20, 27)
        Me.LblMarchaActual.TabIndex = 23
        Me.LblMarchaActual.Text = "-"
        '
        'LblMarchaDeseada
        '
        Me.LblMarchaDeseada.AutoSize = True
        Me.LblMarchaDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblMarchaDeseada.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMarchaDeseada.ForeColor = System.Drawing.Color.White
        Me.LblMarchaDeseada.Location = New System.Drawing.Point(58, 66)
        Me.LblMarchaDeseada.Name = "LblMarchaDeseada"
        Me.LblMarchaDeseada.Size = New System.Drawing.Size(12, 16)
        Me.LblMarchaDeseada.TabIndex = 22
        Me.LblMarchaDeseada.Text = "-"
        '
        'LblMarcha3
        '
        Me.LblMarcha3.AutoSize = True
        Me.LblMarcha3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMarcha3.ForeColor = System.Drawing.Color.White
        Me.LblMarcha3.Location = New System.Drawing.Point(20, 95)
        Me.LblMarcha3.Name = "LblMarcha3"
        Me.LblMarcha3.Size = New System.Drawing.Size(94, 16)
        Me.LblMarcha3.TabIndex = 21
        Me.LblMarcha3.Text = "Marcha Actual"
        '
        'LblMarcha1
        '
        Me.LblMarcha1.AutoSize = True
        Me.LblMarcha1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMarcha1.ForeColor = System.Drawing.Color.White
        Me.LblMarcha1.Location = New System.Drawing.Point(16, 33)
        Me.LblMarcha1.Name = "LblMarcha1"
        Me.LblMarcha1.Size = New System.Drawing.Size(107, 16)
        Me.LblMarcha1.TabIndex = 20
        Me.LblMarcha1.Text = "Marcha Deseada"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblMarcha)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(128, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblMarcha
        '
        Me.LblMarcha.AutoSize = True
        Me.LblMarcha.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMarcha.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMarcha.Location = New System.Drawing.Point(44, 6)
        Me.LblMarcha.Name = "LblMarcha"
        Me.LblMarcha.Size = New System.Drawing.Size(52, 16)
        Me.LblMarcha.TabIndex = 19
        Me.LblMarcha.Text = "Marcha"
        '
        'BtnRpmMinus50
        '
        Me.BtnRpmMinus50.Animated = True
        Me.BtnRpmMinus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.BorderRadius = 5
        Me.BtnRpmMinus50.BorderThickness = 1
        Me.BtnRpmMinus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnRpmMinus50.Location = New System.Drawing.Point(172, 172)
        Me.BtnRpmMinus50.Name = "BtnRpmMinus50"
        Me.BtnRpmMinus50.Size = New System.Drawing.Size(35, 35)
        Me.BtnRpmMinus50.TabIndex = 75
        '
        'BtnRpmPlus50
        '
        Me.BtnRpmPlus50.Animated = True
        Me.BtnRpmPlus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.BorderRadius = 5
        Me.BtnRpmPlus50.BorderThickness = 1
        Me.BtnRpmPlus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnRpmPlus50.Location = New System.Drawing.Point(172, 53)
        Me.BtnRpmPlus50.Name = "BtnRpmPlus50"
        Me.BtnRpmPlus50.Size = New System.Drawing.Size(35, 35)
        Me.BtnRpmPlus50.TabIndex = 74
        '
        'BtnRpmMinus25
        '
        Me.BtnRpmMinus25.Animated = True
        Me.BtnRpmMinus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.BorderRadius = 5
        Me.BtnRpmMinus25.BorderThickness = 1
        Me.BtnRpmMinus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnRpmMinus25.Location = New System.Drawing.Point(177, 136)
        Me.BtnRpmMinus25.Name = "BtnRpmMinus25"
        Me.BtnRpmMinus25.Size = New System.Drawing.Size(25, 25)
        Me.BtnRpmMinus25.TabIndex = 73
        '
        'BtnRpmPlus25
        '
        Me.BtnRpmPlus25.Animated = True
        Me.BtnRpmPlus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.BorderRadius = 5
        Me.BtnRpmPlus25.BorderThickness = 1
        Me.BtnRpmPlus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnRpmPlus25.Location = New System.Drawing.Point(177, 100)
        Me.BtnRpmPlus25.Name = "BtnRpmPlus25"
        Me.BtnRpmPlus25.Size = New System.Drawing.Size(25, 25)
        Me.BtnRpmPlus25.TabIndex = 72
        '
        'PanelRPM
        '
        Me.PanelRPM.BackColor = System.Drawing.Color.Transparent
        Me.PanelRPM.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelRPM.BorderRadius = 6
        Me.PanelRPM.BorderThickness = 1
        Me.PanelRPM.Controls.Add(Me.BtnIdle)
        Me.PanelRPM.Controls.Add(Me.LblRPMActual)
        Me.PanelRPM.Controls.Add(Me.LblRPMDeseada)
        Me.PanelRPM.Controls.Add(Me.LblRPM3)
        Me.PanelRPM.Controls.Add(Me.LblRPM1)
        Me.PanelRPM.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.PanelRPM.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelRPM.FillColor = System.Drawing.Color.Transparent
        Me.PanelRPM.Location = New System.Drawing.Point(39, 29)
        Me.PanelRPM.Name = "PanelRPM"
        Me.PanelRPM.Size = New System.Drawing.Size(128, 174)
        Me.PanelRPM.TabIndex = 71
        '
        'BtnIdle
        '
        Me.BtnIdle.Animated = True
        Me.BtnIdle.BorderColor = System.Drawing.Color.White
        Me.BtnIdle.BorderRadius = 5
        Me.BtnIdle.BorderThickness = 1
        Me.BtnIdle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdle.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdle.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnIdle.ForeColor = System.Drawing.Color.White
        Me.BtnIdle.Location = New System.Drawing.Point(23, 138)
        Me.BtnIdle.Name = "BtnIdle"
        Me.BtnIdle.Size = New System.Drawing.Size(82, 30)
        Me.BtnIdle.TabIndex = 69
        Me.BtnIdle.Text = "IDLE"
        '
        'LblRPMActual
        '
        Me.LblRPMActual.AutoSize = True
        Me.LblRPMActual.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblRPMActual.ForeColor = System.Drawing.Color.White
        Me.LblRPMActual.Location = New System.Drawing.Point(54, 105)
        Me.LblRPMActual.Name = "LblRPMActual"
        Me.LblRPMActual.Size = New System.Drawing.Size(20, 27)
        Me.LblRPMActual.TabIndex = 23
        Me.LblRPMActual.Text = "-"
        '
        'LblRPMDeseada
        '
        Me.LblRPMDeseada.AutoSize = True
        Me.LblRPMDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblRPMDeseada.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblRPMDeseada.ForeColor = System.Drawing.Color.White
        Me.LblRPMDeseada.Location = New System.Drawing.Point(63, 60)
        Me.LblRPMDeseada.Name = "LblRPMDeseada"
        Me.LblRPMDeseada.Size = New System.Drawing.Size(12, 16)
        Me.LblRPMDeseada.TabIndex = 22
        Me.LblRPMDeseada.Text = "-"
        '
        'LblRPM3
        '
        Me.LblRPM3.AutoSize = True
        Me.LblRPM3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblRPM3.ForeColor = System.Drawing.Color.White
        Me.LblRPM3.Location = New System.Drawing.Point(25, 86)
        Me.LblRPM3.Name = "LblRPM3"
        Me.LblRPM3.Size = New System.Drawing.Size(91, 16)
        Me.LblRPM3.TabIndex = 21
        Me.LblRPM3.Text = "RPM Actuales"
        '
        'LblRPM1
        '
        Me.LblRPM1.AutoSize = True
        Me.LblRPM1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblRPM1.ForeColor = System.Drawing.Color.White
        Me.LblRPM1.Location = New System.Drawing.Point(25, 35)
        Me.LblRPM1.Name = "LblRPM1"
        Me.LblRPM1.Size = New System.Drawing.Size(97, 16)
        Me.LblRPM1.TabIndex = 20
        Me.LblRPM1.Text = "RPM Deseadas"
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblRpm)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(128, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblRpm
        '
        Me.LblRpm.AutoSize = True
        Me.LblRpm.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblRpm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblRpm.Location = New System.Drawing.Point(49, 4)
        Me.LblRpm.Name = "LblRpm"
        Me.LblRpm.Size = New System.Drawing.Size(36, 16)
        Me.LblRpm.TabIndex = 19
        Me.LblRpm.Text = "RPM"
        '
        'BtnErrores
        '
        Me.BtnErrores.Animated = True
        Me.BtnErrores.BorderColor = System.Drawing.Color.White
        Me.BtnErrores.BorderRadius = 5
        Me.BtnErrores.BorderThickness = 1
        Me.BtnErrores.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnErrores.FillColor = System.Drawing.Color.Transparent
        Me.BtnErrores.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnErrores.ForeColor = System.Drawing.Color.White
        Me.BtnErrores.Location = New System.Drawing.Point(355, 6)
        Me.BtnErrores.Name = "BtnErrores"
        Me.BtnErrores.Size = New System.Drawing.Size(73, 20)
        Me.BtnErrores.TabIndex = 91
        Me.BtnErrores.Text = "Errores"
        '
        'TmrRead
        '
        Me.TmrRead.Interval = 1000
        '
        'TmrCheck
        '
        Me.TmrCheck.Interval = 1000
        '
        'BtnAjustes
        '
        Me.BtnAjustes.Animated = True
        Me.BtnAjustes.BorderColor = System.Drawing.Color.White
        Me.BtnAjustes.BorderRadius = 5
        Me.BtnAjustes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAjustes.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAjustes.ForeColor = System.Drawing.Color.White
        Me.BtnAjustes.Image = Global.sFRAC.My.Resources.Resources.settings_1
        Me.BtnAjustes.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnAjustes.ImageSize = New System.Drawing.Size(49, 49)
        Me.BtnAjustes.Location = New System.Drawing.Point(490, 5)
        Me.BtnAjustes.Name = "BtnAjustes"
        Me.BtnAjustes.Size = New System.Drawing.Size(50, 50)
        Me.BtnAjustes.TabIndex = 93
        '
        'BtnStartEngine
        '
        Me.BtnStartEngine.Animated = True
        Me.BtnStartEngine.BorderColor = System.Drawing.Color.White
        Me.BtnStartEngine.BorderRadius = 5
        Me.BtnStartEngine.CheckedState.Image = Global.sFRAC.My.Resources.Resources.Engine_STOP
        Me.BtnStartEngine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStartEngine.FillColor = System.Drawing.Color.Transparent
        Me.BtnStartEngine.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnStartEngine.ForeColor = System.Drawing.Color.White
        Me.BtnStartEngine.Image = Global.sFRAC.My.Resources.Resources.Engine_START
        Me.BtnStartEngine.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnStartEngine.ImageSize = New System.Drawing.Size(49, 49)
        Me.BtnStartEngine.Location = New System.Drawing.Point(546, 5)
        Me.BtnStartEngine.Name = "BtnStartEngine"
        Me.BtnStartEngine.Size = New System.Drawing.Size(50, 50)
        Me.BtnStartEngine.TabIndex = 92
        '
        'BtnFC
        '
        Me.BtnFC.Animated = True
        Me.BtnFC.BorderColor = System.Drawing.Color.White
        Me.BtnFC.BorderRadius = 5
        Me.BtnFC.BorderThickness = 1
        Me.BtnFC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFC.FillColor = System.Drawing.Color.Transparent
        Me.BtnFC.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnFC.ForeColor = System.Drawing.Color.White
        Me.BtnFC.Location = New System.Drawing.Point(374, 54)
        Me.BtnFC.Name = "BtnFC"
        Me.BtnFC.Size = New System.Drawing.Size(45, 40)
        Me.BtnFC.TabIndex = 94
        Me.BtnFC.Text = "FC"
        '
        'PbAutomatic
        '
        Me.PbAutomatic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAutomatic.Image = Global.sFRAC.My.Resources.Resources.automatic1
        Me.PbAutomatic.ImageRotate = 0!
        Me.PbAutomatic.Location = New System.Drawing.Point(434, 5)
        Me.PbAutomatic.Name = "PbAutomatic"
        Me.PbAutomatic.Size = New System.Drawing.Size(50, 50)
        Me.PbAutomatic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAutomatic.TabIndex = 95
        Me.PbAutomatic.TabStop = False
        Me.PbAutomatic.Visible = False
        '
        'TmrReconnect
        '
        Me.TmrReconnect.Interval = 1000
        '
        'TmrErrorHandle
        '
        Me.TmrErrorHandle.Interval = 600
        '
        'BtnResetRPM
        '
        Me.BtnResetRPM.Animated = True
        Me.BtnResetRPM.BorderColor = System.Drawing.Color.White
        Me.BtnResetRPM.BorderRadius = 5
        Me.BtnResetRPM.BorderThickness = 1
        Me.BtnResetRPM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnResetRPM.FillColor = System.Drawing.Color.Transparent
        Me.BtnResetRPM.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnResetRPM.ForeColor = System.Drawing.Color.White
        Me.BtnResetRPM.Location = New System.Drawing.Point(434, 64)
        Me.BtnResetRPM.Name = "BtnResetRPM"
        Me.BtnResetRPM.Size = New System.Drawing.Size(150, 30)
        Me.BtnResetRPM.TabIndex = 96
        Me.BtnResetRPM.Text = "Reset RPM"
        Me.BtnResetRPM.Visible = False
        '
        'TmrOp
        '
        Me.TmrOp.Interval = 1000
        '
        'TmrBackUp
        '
        Me.TmrBackUp.Interval = 1000
        '
        'BtnEKill
        '
        Me.BtnEKill.Animated = True
        Me.BtnEKill.BorderColor = System.Drawing.Color.White
        Me.BtnEKill.BorderRadius = 5
        Me.BtnEKill.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BtnEKill.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEKill.FillColor = System.Drawing.Color.Transparent
        Me.BtnEKill.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnEKill.ForeColor = System.Drawing.Color.White
        Me.BtnEKill.Image = Global.sFRAC.My.Resources.Resources.E_STOP
        Me.BtnEKill.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnEKill.ImageSize = New System.Drawing.Size(49, 49)
        Me.BtnEKill.Location = New System.Drawing.Point(0, 0)
        Me.BtnEKill.Name = "BtnEKill"
        Me.BtnEKill.Size = New System.Drawing.Size(50, 50)
        Me.BtnEKill.TabIndex = 189
        '
        'TwoFrac_1280x840
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Controls.Add(Me.BtnEKill)
        Me.Controls.Add(Me.BtnResetRPM)
        Me.Controls.Add(Me.PbAutomatic)
        Me.Controls.Add(Me.BtnFC)
        Me.Controls.Add(Me.BtnAjustes)
        Me.Controls.Add(Me.BtnStartEngine)
        Me.Controls.Add(Me.BtnErrores)
        Me.Controls.Add(Me.Guna2Panel11)
        Me.Controls.Add(Me.BtnDiagnostico)
        Me.Controls.Add(Me.LblErrorAdmisible)
        Me.Controls.Add(Me.LblErrorGrave)
        Me.Controls.Add(Me.Guna2Panel9)
        Me.Controls.Add(Me.Guna2Panel10)
        Me.Controls.Add(Me.Guna2Panel7)
        Me.Controls.Add(Me.Guna2Panel5)
        Me.Controls.Add(Me.Guna2Panel4)
        Me.Controls.Add(Me.BtnMinusGear)
        Me.Controls.Add(Me.BtnPlusGear)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.BtnRpmMinus50)
        Me.Controls.Add(Me.BtnRpmPlus50)
        Me.Controls.Add(Me.BtnRpmMinus25)
        Me.Controls.Add(Me.BtnRpmPlus25)
        Me.Controls.Add(Me.PanelRPM)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.DoubleBuffered = True
        Me.Name = "TwoFrac_1280x840"
        Me.Size = New System.Drawing.Size(600, 637)
        Me.Guna2Panel11.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.PerformLayout()
        Me.Guna2Panel9.ResumeLayout(False)
        Me.Guna2Panel9.PerformLayout()
        Me.Guna2CustomGradientPanel9.ResumeLayout(False)
        Me.Guna2CustomGradientPanel9.PerformLayout()
        Me.Guna2Panel10.ResumeLayout(False)
        Me.Guna2Panel10.PerformLayout()
        Me.Guna2CustomGradientPanel10.ResumeLayout(False)
        Me.Guna2CustomGradientPanel10.PerformLayout()
        Me.Guna2Panel7.ResumeLayout(False)
        Me.Guna2Panel7.PerformLayout()
        Me.Guna2CustomGradientPanel7.ResumeLayout(False)
        Me.Guna2CustomGradientPanel7.PerformLayout()
        Me.Guna2Panel5.ResumeLayout(False)
        Me.Guna2Panel5.PerformLayout()
        Me.Guna2CustomGradientPanel5.ResumeLayout(False)
        Me.Guna2CustomGradientPanel5.PerformLayout()
        Me.Guna2Panel4.ResumeLayout(False)
        Me.Guna2Panel4.PerformLayout()
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.PanelRPM.ResumeLayout(False)
        Me.PanelRPM.PerformLayout()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents Guna2Panel11 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnEmpezarTrabajo As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnSemiAuto As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnEncenderLuces As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblOpciones As Label
    Public WithEvents BtnDiagnostico As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblErrorAdmisible As Label
    Public WithEvents LblErrorGrave As Label
    Public WithEvents Guna2Panel9 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label2 As Label
    Public WithEvents Label1 As Label
    Public WithEvents BtnTripZero As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnTrip As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblPCorteActual As Label
    Public WithEvents LblPCorteDeseada As Label
    Public WithEvents LblPCorte3 As Label
    Public WithEvents LblPCorte1 As Label
    Public WithEvents LblPCorte As Label
    Public WithEvents Guna2Panel10 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label81 As Label
    Public WithEvents LblCaudalActual As Label
    Public WithEvents LblCaudal As Label
    Public WithEvents Guna2Panel7 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label54 As Label
    Public WithEvents Label55 As Label
    Public WithEvents Label56 As Label
    Public WithEvents LblBombaHHP As Label
    Public WithEvents LblHHP As Label
    Public WithEvents LblBombaCargaMotor As Label
    Public WithEvents LblBombaPresSuccion As Label
    Public WithEvents LblCargaMotor As Label
    Public WithEvents LblPresSuc As Label
    Public WithEvents Label63 As Label
    Public WithEvents Label64 As Label
    Public WithEvents Label65 As Label
    Public WithEvents LblBombaHorasMotor As Label
    Public WithEvents LblHM As Label
    Public WithEvents LblBombaPresLUB As Label
    Public WithEvents LblBombaTempLUB As Label
    Public WithEvents LblPresLub As Label
    Public WithEvents LblTempLub As Label
    Public WithEvents LblBomba As Label
    Public WithEvents Guna2Panel5 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label41 As Label
    Public WithEvents Label42 As Label
    Public WithEvents Label43 As Label
    Public WithEvents LblTransRpmOut As Label
    Public WithEvents LblTrans5 As Label
    Public WithEvents LblTransPresion As Label
    Public WithEvents LblTransTemp As Label
    Public WithEvents LblTrans3 As Label
    Public WithEvents LblTrans1 As Label
    Public WithEvents LblTransmision As Label
    Public WithEvents Guna2Panel4 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label23 As Label
    Public WithEvents Label24 As Label
    Public WithEvents Label25 As Label
    Public WithEvents LblMotorCombustibleP100 As Label
    Public WithEvents LblCombustible1 As Label
    Public WithEvents LblMotorCombustible As Label
    Public WithEvents LblMotorTempAceite As Label
    Public WithEvents LblCombustible As Label
    Public WithEvents LblTempAceite As Label
    Public WithEvents Label22 As Label
    Public WithEvents Label21 As Label
    Public WithEvents Label20 As Label
    Public WithEvents LblMotorVoltaje As Label
    Public WithEvents LblVoltaje As Label
    Public WithEvents LblMotorPresion As Label
    Public WithEvents LblMotorTempAgua As Label
    Public WithEvents LblPresion As Label
    Public WithEvents LblTempAgua As Label
    Public WithEvents LblMotor As Label
    Public WithEvents BtnMinusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnPlusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblMarchaActual As Label
    Public WithEvents LblMarchaDeseada As Label
    Public WithEvents LblMarcha3 As Label
    Public WithEvents LblMarcha1 As Label
    Public WithEvents LblMarcha As Label
    Public WithEvents BtnRpmMinus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmMinus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PanelRPM As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnIdle As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPMActual As Label
    Public WithEvents LblRPMDeseada As Label
    Public WithEvents LblRPM3 As Label
    Public WithEvents LblRPM1 As Label
    Public WithEvents LblRpm As Label
    Public WithEvents BtnErrores As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnAjustes As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnStartEngine As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnFC As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PbAutomatic As Guna.UI2.WinForms.Guna2PictureBox
    Public WithEvents BtnResetRPM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel11 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel9 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel10 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel7 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel5 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents TmrRead As Timer
    Public WithEvents TmrCheck As Timer
    Public WithEvents TmrReconnect As Timer
    Public WithEvents TmrErrorHandle As Timer
    Public WithEvents TmrOp As Timer
    Friend WithEvents TmrBackUp As Timer
    Public WithEvents BtnEKill As Guna.UI2.WinForms.Guna2Button
End Class
