﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FourFrac_1280x840
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2Panel11 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnEmpezarTrabajo = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnSemiAuto = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnEncenderLuces = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel11 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblOpciones = New System.Windows.Forms.Label()
        Me.BtnAjustes = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnStartEngine = New Guna.UI2.WinForms.Guna2Button()
        Me.LblErrorAdmisible = New System.Windows.Forms.Label()
        Me.LblErrorGrave = New System.Windows.Forms.Label()
        Me.Guna2Panel9 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BtnTripZero = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnTrip = New Guna.UI2.WinForms.Guna2Button()
        Me.LblPCorteActual = New System.Windows.Forms.Label()
        Me.LblPCorteDeseada = New System.Windows.Forms.Label()
        Me.LblPCorte3 = New System.Windows.Forms.Label()
        Me.LblPCorte1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel9 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblPCorte = New System.Windows.Forms.Label()
        Me.Guna2Panel10 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnFC = New Guna.UI2.WinForms.Guna2Button()
        Me.LblCaudal2 = New System.Windows.Forms.Label()
        Me.LblCaudalActual = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel10 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblCaudal = New System.Windows.Forms.Label()
        Me.BtnMinusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnPlusGear = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.LblMarchaActual = New System.Windows.Forms.Label()
        Me.LblMarchaDeseada = New System.Windows.Forms.Label()
        Me.LblMarcha3 = New System.Windows.Forms.Label()
        Me.LblMarcha1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblMarcha = New System.Windows.Forms.Label()
        Me.BtnRpmMinus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus50 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmMinus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnRpmPlus25 = New Guna.UI2.WinForms.Guna2Button()
        Me.PanelRPM = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnResetRPM = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnIdle = New Guna.UI2.WinForms.Guna2Button()
        Me.LblRPMActual = New System.Windows.Forms.Label()
        Me.LblRPMDeseada = New System.Windows.Forms.Label()
        Me.LblRPM3 = New System.Windows.Forms.Label()
        Me.LblRPM1 = New System.Windows.Forms.Label()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.LblRpm = New System.Windows.Forms.Label()
        Me.BtnErrores = New Guna.UI2.WinForms.Guna2Button()
        Me.PbAutomatic = New Guna.UI2.WinForms.Guna2PictureBox()
        Me.TmrReconnect = New System.Windows.Forms.Timer(Me.components)
        Me.TmrErrorHandle = New System.Windows.Forms.Timer(Me.components)
        Me.TmrCheck = New System.Windows.Forms.Timer(Me.components)
        Me.TmrRead = New System.Windows.Forms.Timer(Me.components)
        Me.TmrBackUp = New System.Windows.Forms.Timer(Me.components)
        Me.TmrOp = New System.Windows.Forms.Timer(Me.components)
        Me.BtnEKill = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2Panel11.SuspendLayout()
        Me.Guna2CustomGradientPanel11.SuspendLayout()
        Me.Guna2Panel9.SuspendLayout()
        Me.Guna2CustomGradientPanel9.SuspendLayout()
        Me.Guna2Panel10.SuspendLayout()
        Me.Guna2CustomGradientPanel10.SuspendLayout()
        Me.Guna2Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.PanelRPM.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Guna2Panel11
        '
        Me.Guna2Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.BorderRadius = 6
        Me.Guna2Panel11.BorderThickness = 1
        Me.Guna2Panel11.Controls.Add(Me.BtnEmpezarTrabajo)
        Me.Guna2Panel11.Controls.Add(Me.BtnSemiAuto)
        Me.Guna2Panel11.Controls.Add(Me.BtnEncenderLuces)
        Me.Guna2Panel11.Controls.Add(Me.Guna2CustomGradientPanel11)
        Me.Guna2Panel11.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel11.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel11.Location = New System.Drawing.Point(144, 383)
        Me.Guna2Panel11.Name = "Guna2Panel11"
        Me.Guna2Panel11.Size = New System.Drawing.Size(143, 241)
        Me.Guna2Panel11.TabIndex = 87
        '
        'BtnEmpezarTrabajo
        '
        Me.BtnEmpezarTrabajo.Animated = True
        Me.BtnEmpezarTrabajo.BorderColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.BorderRadius = 5
        Me.BtnEmpezarTrabajo.BorderThickness = 1
        Me.BtnEmpezarTrabajo.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEmpezarTrabajo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEmpezarTrabajo.FillColor = System.Drawing.Color.Transparent
        Me.BtnEmpezarTrabajo.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnEmpezarTrabajo.ForeColor = System.Drawing.Color.White
        Me.BtnEmpezarTrabajo.Location = New System.Drawing.Point(3, 37)
        Me.BtnEmpezarTrabajo.Name = "BtnEmpezarTrabajo"
        Me.BtnEmpezarTrabajo.Size = New System.Drawing.Size(137, 39)
        Me.BtnEmpezarTrabajo.TabIndex = 54
        Me.BtnEmpezarTrabajo.Text = "Empezar Trabajo"
        '
        'BtnSemiAuto
        '
        Me.BtnSemiAuto.Animated = True
        Me.BtnSemiAuto.BorderColor = System.Drawing.Color.White
        Me.BtnSemiAuto.BorderRadius = 5
        Me.BtnSemiAuto.BorderThickness = 1
        Me.BtnSemiAuto.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSemiAuto.FillColor = System.Drawing.Color.Transparent
        Me.BtnSemiAuto.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnSemiAuto.ForeColor = System.Drawing.Color.White
        Me.BtnSemiAuto.Location = New System.Drawing.Point(3, 189)
        Me.BtnSemiAuto.Name = "BtnSemiAuto"
        Me.BtnSemiAuto.Size = New System.Drawing.Size(137, 39)
        Me.BtnSemiAuto.TabIndex = 53
        Me.BtnSemiAuto.Text = "Semi-Automatico"
        '
        'BtnEncenderLuces
        '
        Me.BtnEncenderLuces.Animated = True
        Me.BtnEncenderLuces.BorderColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.BorderRadius = 5
        Me.BtnEncenderLuces.BorderThickness = 1
        Me.BtnEncenderLuces.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.BtnEncenderLuces.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEncenderLuces.FillColor = System.Drawing.Color.Transparent
        Me.BtnEncenderLuces.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnEncenderLuces.ForeColor = System.Drawing.Color.White
        Me.BtnEncenderLuces.Location = New System.Drawing.Point(3, 113)
        Me.BtnEncenderLuces.Name = "BtnEncenderLuces"
        Me.BtnEncenderLuces.Size = New System.Drawing.Size(137, 39)
        Me.BtnEncenderLuces.TabIndex = 52
        Me.BtnEncenderLuces.Text = "Encender Luces"
        '
        'Guna2CustomGradientPanel11
        '
        Me.Guna2CustomGradientPanel11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.BorderRadius = 6
        Me.Guna2CustomGradientPanel11.Controls.Add(Me.LblOpciones)
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel11.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel11.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel11.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel11.Name = "Guna2CustomGradientPanel11"
        Me.Guna2CustomGradientPanel11.Size = New System.Drawing.Size(143, 26)
        Me.Guna2CustomGradientPanel11.TabIndex = 19
        '
        'LblOpciones
        '
        Me.LblOpciones.AutoSize = True
        Me.LblOpciones.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblOpciones.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblOpciones.Location = New System.Drawing.Point(39, 4)
        Me.LblOpciones.Name = "LblOpciones"
        Me.LblOpciones.Size = New System.Drawing.Size(64, 16)
        Me.LblOpciones.TabIndex = 19
        Me.LblOpciones.Text = "Opciones"
        '
        'BtnAjustes
        '
        Me.BtnAjustes.Animated = True
        Me.BtnAjustes.BorderColor = System.Drawing.Color.White
        Me.BtnAjustes.BorderRadius = 5
        Me.BtnAjustes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAjustes.FillColor = System.Drawing.Color.Transparent
        Me.BtnAjustes.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAjustes.ForeColor = System.Drawing.Color.White
        Me.BtnAjustes.Image = Global.sFRAC.My.Resources.Resources.settings_1
        Me.BtnAjustes.ImageSize = New System.Drawing.Size(40, 40)
        Me.BtnAjustes.Location = New System.Drawing.Point(211, 102)
        Me.BtnAjustes.Name = "BtnAjustes"
        Me.BtnAjustes.Size = New System.Drawing.Size(50, 50)
        Me.BtnAjustes.TabIndex = 86
        '
        'BtnStartEngine
        '
        Me.BtnStartEngine.Animated = True
        Me.BtnStartEngine.BorderColor = System.Drawing.Color.White
        Me.BtnStartEngine.BorderRadius = 5
        Me.BtnStartEngine.CheckedState.Image = Global.sFRAC.My.Resources.Resources.Engine_STOP
        Me.BtnStartEngine.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnStartEngine.FillColor = System.Drawing.Color.Transparent
        Me.BtnStartEngine.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnStartEngine.ForeColor = System.Drawing.Color.White
        Me.BtnStartEngine.Image = Global.sFRAC.My.Resources.Resources.Engine_START
        Me.BtnStartEngine.ImageSize = New System.Drawing.Size(40, 40)
        Me.BtnStartEngine.Location = New System.Drawing.Point(211, 43)
        Me.BtnStartEngine.Name = "BtnStartEngine"
        Me.BtnStartEngine.Size = New System.Drawing.Size(50, 50)
        Me.BtnStartEngine.TabIndex = 85
        '
        'LblErrorAdmisible
        '
        Me.LblErrorAdmisible.AutoSize = True
        Me.LblErrorAdmisible.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblErrorAdmisible.ForeColor = System.Drawing.Color.Orange
        Me.LblErrorAdmisible.Location = New System.Drawing.Point(101, 14)
        Me.LblErrorAdmisible.Name = "LblErrorAdmisible"
        Me.LblErrorAdmisible.Size = New System.Drawing.Size(101, 16)
        Me.LblErrorAdmisible.TabIndex = 83
        Me.LblErrorAdmisible.Text = "Error Admisible"
        Me.LblErrorAdmisible.Visible = False
        '
        'LblErrorGrave
        '
        Me.LblErrorGrave.AutoSize = True
        Me.LblErrorGrave.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblErrorGrave.ForeColor = System.Drawing.Color.Red
        Me.LblErrorGrave.Location = New System.Drawing.Point(18, 14)
        Me.LblErrorGrave.Name = "LblErrorGrave"
        Me.LblErrorGrave.Size = New System.Drawing.Size(75, 16)
        Me.LblErrorGrave.TabIndex = 82
        Me.LblErrorGrave.Text = "Error Grave"
        Me.LblErrorGrave.Visible = False
        '
        'Guna2Panel9
        '
        Me.Guna2Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.BorderRadius = 6
        Me.Guna2Panel9.BorderThickness = 1
        Me.Guna2Panel9.Controls.Add(Me.Label2)
        Me.Guna2Panel9.Controls.Add(Me.Label1)
        Me.Guna2Panel9.Controls.Add(Me.BtnTripZero)
        Me.Guna2Panel9.Controls.Add(Me.BtnTrip)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorteActual)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorteDeseada)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorte3)
        Me.Guna2Panel9.Controls.Add(Me.LblPCorte1)
        Me.Guna2Panel9.Controls.Add(Me.Guna2CustomGradientPanel9)
        Me.Guna2Panel9.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.Location = New System.Drawing.Point(11, 383)
        Me.Guna2Panel9.Name = "Guna2Panel9"
        Me.Guna2Panel9.Size = New System.Drawing.Size(128, 241)
        Me.Guna2Panel9.TabIndex = 81
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(94, 169)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 16)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "PSI"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(95, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 16)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "PSI"
        '
        'BtnTripZero
        '
        Me.BtnTripZero.Animated = True
        Me.BtnTripZero.BorderColor = System.Drawing.Color.White
        Me.BtnTripZero.BorderRadius = 5
        Me.BtnTripZero.BorderThickness = 1
        Me.BtnTripZero.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTripZero.FillColor = System.Drawing.Color.Transparent
        Me.BtnTripZero.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnTripZero.ForeColor = System.Drawing.Color.White
        Me.BtnTripZero.Location = New System.Drawing.Point(14, 198)
        Me.BtnTripZero.Name = "BtnTripZero"
        Me.BtnTripZero.Size = New System.Drawing.Size(100, 30)
        Me.BtnTripZero.TabIndex = 53
        Me.BtnTripZero.Text = "ZERO"
        '
        'BtnTrip
        '
        Me.BtnTrip.Animated = True
        Me.BtnTrip.BorderColor = System.Drawing.Color.White
        Me.BtnTrip.BorderRadius = 5
        Me.BtnTrip.BorderThickness = 1
        Me.BtnTrip.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnTrip.FillColor = System.Drawing.Color.Transparent
        Me.BtnTrip.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnTrip.ForeColor = System.Drawing.Color.White
        Me.BtnTrip.Location = New System.Drawing.Point(14, 90)
        Me.BtnTrip.Name = "BtnTrip"
        Me.BtnTrip.Size = New System.Drawing.Size(100, 30)
        Me.BtnTrip.TabIndex = 52
        Me.BtnTrip.Text = "TRIP"
        '
        'LblPCorteActual
        '
        Me.LblPCorteActual.AutoSize = True
        Me.LblPCorteActual.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblPCorteActual.ForeColor = System.Drawing.Color.White
        Me.LblPCorteActual.Location = New System.Drawing.Point(27, 158)
        Me.LblPCorteActual.Name = "LblPCorteActual"
        Me.LblPCorteActual.Size = New System.Drawing.Size(20, 27)
        Me.LblPCorteActual.TabIndex = 23
        Me.LblPCorteActual.Text = "-"
        '
        'LblPCorteDeseada
        '
        Me.LblPCorteDeseada.AutoSize = True
        Me.LblPCorteDeseada.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPCorteDeseada.ForeColor = System.Drawing.Color.White
        Me.LblPCorteDeseada.Location = New System.Drawing.Point(45, 60)
        Me.LblPCorteDeseada.Name = "LblPCorteDeseada"
        Me.LblPCorteDeseada.Size = New System.Drawing.Size(12, 16)
        Me.LblPCorteDeseada.TabIndex = 22
        Me.LblPCorteDeseada.Text = "-"
        '
        'LblPCorte3
        '
        Me.LblPCorte3.AutoSize = True
        Me.LblPCorte3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPCorte3.ForeColor = System.Drawing.Color.White
        Me.LblPCorte3.Location = New System.Drawing.Point(21, 140)
        Me.LblPCorte3.Name = "LblPCorte3"
        Me.LblPCorte3.Size = New System.Drawing.Size(95, 16)
        Me.LblPCorte3.TabIndex = 21
        Me.LblPCorte3.Text = "Presión Actual"
        '
        'LblPCorte1
        '
        Me.LblPCorte1.AutoSize = True
        Me.LblPCorte1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPCorte1.ForeColor = System.Drawing.Color.White
        Me.LblPCorte1.Location = New System.Drawing.Point(14, 32)
        Me.LblPCorte1.Name = "LblPCorte1"
        Me.LblPCorte1.Size = New System.Drawing.Size(108, 16)
        Me.LblPCorte1.TabIndex = 20
        Me.LblPCorte1.Text = "Presión de Corte"
        '
        'Guna2CustomGradientPanel9
        '
        Me.Guna2CustomGradientPanel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.BorderRadius = 6
        Me.Guna2CustomGradientPanel9.Controls.Add(Me.LblPCorte)
        Me.Guna2CustomGradientPanel9.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel9.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel9.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel9.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel9.Name = "Guna2CustomGradientPanel9"
        Me.Guna2CustomGradientPanel9.Size = New System.Drawing.Size(128, 26)
        Me.Guna2CustomGradientPanel9.TabIndex = 19
        '
        'LblPCorte
        '
        Me.LblPCorte.AutoSize = True
        Me.LblPCorte.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblPCorte.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblPCorte.Location = New System.Drawing.Point(20, 5)
        Me.LblPCorte.Name = "LblPCorte"
        Me.LblPCorte.Size = New System.Drawing.Size(108, 16)
        Me.LblPCorte.TabIndex = 19
        Me.LblPCorte.Text = "Presión de Corte"
        '
        'Guna2Panel10
        '
        Me.Guna2Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.BorderRadius = 6
        Me.Guna2Panel10.BorderThickness = 1
        Me.Guna2Panel10.Controls.Add(Me.BtnFC)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudal2)
        Me.Guna2Panel10.Controls.Add(Me.LblCaudalActual)
        Me.Guna2Panel10.Controls.Add(Me.Guna2CustomGradientPanel10)
        Me.Guna2Panel10.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel10.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel10.Location = New System.Drawing.Point(185, 223)
        Me.Guna2Panel10.Name = "Guna2Panel10"
        Me.Guna2Panel10.Size = New System.Drawing.Size(102, 154)
        Me.Guna2Panel10.TabIndex = 80
        '
        'BtnFC
        '
        Me.BtnFC.Animated = True
        Me.BtnFC.BorderColor = System.Drawing.Color.White
        Me.BtnFC.BorderRadius = 5
        Me.BtnFC.BorderThickness = 1
        Me.BtnFC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnFC.FillColor = System.Drawing.Color.Transparent
        Me.BtnFC.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnFC.ForeColor = System.Drawing.Color.White
        Me.BtnFC.Location = New System.Drawing.Point(29, 106)
        Me.BtnFC.Name = "BtnFC"
        Me.BtnFC.Size = New System.Drawing.Size(45, 40)
        Me.BtnFC.TabIndex = 95
        Me.BtnFC.Text = "FC"
        '
        'LblCaudal2
        '
        Me.LblCaudal2.AutoSize = True
        Me.LblCaudal2.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblCaudal2.ForeColor = System.Drawing.Color.White
        Me.LblCaudal2.Location = New System.Drawing.Point(21, 77)
        Me.LblCaudal2.Name = "LblCaudal2"
        Me.LblCaudal2.Size = New System.Drawing.Size(58, 16)
        Me.LblCaudal2.TabIndex = 23
        Me.LblCaudal2.Text = "BBL/Min"
        '
        'LblCaudalActual
        '
        Me.LblCaudalActual.AutoSize = True
        Me.LblCaudalActual.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblCaudalActual.ForeColor = System.Drawing.Color.White
        Me.LblCaudalActual.Location = New System.Drawing.Point(40, 35)
        Me.LblCaudalActual.Name = "LblCaudalActual"
        Me.LblCaudalActual.Size = New System.Drawing.Size(20, 27)
        Me.LblCaudalActual.TabIndex = 22
        Me.LblCaudalActual.Text = "-"
        '
        'Guna2CustomGradientPanel10
        '
        Me.Guna2CustomGradientPanel10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.BorderRadius = 6
        Me.Guna2CustomGradientPanel10.Controls.Add(Me.LblCaudal)
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel10.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel10.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel10.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel10.Name = "Guna2CustomGradientPanel10"
        Me.Guna2CustomGradientPanel10.Size = New System.Drawing.Size(102, 26)
        Me.Guna2CustomGradientPanel10.TabIndex = 19
        '
        'LblCaudal
        '
        Me.LblCaudal.AutoSize = True
        Me.LblCaudal.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblCaudal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblCaudal.Location = New System.Drawing.Point(32, 4)
        Me.LblCaudal.Name = "LblCaudal"
        Me.LblCaudal.Size = New System.Drawing.Size(49, 16)
        Me.LblCaudal.TabIndex = 19
        Me.LblCaudal.Text = "Caudal"
        '
        'BtnMinusGear
        '
        Me.BtnMinusGear.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnMinusGear.Animated = True
        Me.BtnMinusGear.BorderColor = System.Drawing.Color.White
        Me.BtnMinusGear.BorderRadius = 5
        Me.BtnMinusGear.BorderThickness = 1
        Me.BtnMinusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnMinusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnMinusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnMinusGear.ForeColor = System.Drawing.Color.White
        Me.BtnMinusGear.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnMinusGear.Location = New System.Drawing.Point(144, 337)
        Me.BtnMinusGear.Name = "BtnMinusGear"
        Me.BtnMinusGear.Size = New System.Drawing.Size(35, 35)
        Me.BtnMinusGear.TabIndex = 79
        '
        'BtnPlusGear
        '
        Me.BtnPlusGear.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnPlusGear.Animated = True
        Me.BtnPlusGear.BorderColor = System.Drawing.Color.White
        Me.BtnPlusGear.BorderRadius = 5
        Me.BtnPlusGear.BorderThickness = 1
        Me.BtnPlusGear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnPlusGear.FillColor = System.Drawing.Color.Transparent
        Me.BtnPlusGear.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnPlusGear.ForeColor = System.Drawing.Color.White
        Me.BtnPlusGear.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnPlusGear.Location = New System.Drawing.Point(144, 294)
        Me.BtnPlusGear.Name = "BtnPlusGear"
        Me.BtnPlusGear.Size = New System.Drawing.Size(35, 35)
        Me.BtnPlusGear.TabIndex = 78
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaActual)
        Me.Guna2Panel1.Controls.Add(Me.LblMarchaDeseada)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha3)
        Me.Guna2Panel1.Controls.Add(Me.LblMarcha1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(11, 223)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(128, 154)
        Me.Guna2Panel1.TabIndex = 77
        '
        'LblMarchaActual
        '
        Me.LblMarchaActual.AutoSize = True
        Me.LblMarchaActual.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblMarchaActual.ForeColor = System.Drawing.Color.White
        Me.LblMarchaActual.Location = New System.Drawing.Point(56, 122)
        Me.LblMarchaActual.Name = "LblMarchaActual"
        Me.LblMarchaActual.Size = New System.Drawing.Size(20, 27)
        Me.LblMarchaActual.TabIndex = 23
        Me.LblMarchaActual.Text = "-"
        '
        'LblMarchaDeseada
        '
        Me.LblMarchaDeseada.AutoSize = True
        Me.LblMarchaDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblMarchaDeseada.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMarchaDeseada.ForeColor = System.Drawing.Color.White
        Me.LblMarchaDeseada.Location = New System.Drawing.Point(54, 66)
        Me.LblMarchaDeseada.Name = "LblMarchaDeseada"
        Me.LblMarchaDeseada.Size = New System.Drawing.Size(12, 16)
        Me.LblMarchaDeseada.TabIndex = 22
        Me.LblMarchaDeseada.Text = "-"
        '
        'LblMarcha3
        '
        Me.LblMarcha3.AutoSize = True
        Me.LblMarcha3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMarcha3.ForeColor = System.Drawing.Color.White
        Me.LblMarcha3.Location = New System.Drawing.Point(16, 99)
        Me.LblMarcha3.Name = "LblMarcha3"
        Me.LblMarcha3.Size = New System.Drawing.Size(94, 16)
        Me.LblMarcha3.TabIndex = 21
        Me.LblMarcha3.Text = "Marcha Actual"
        '
        'LblMarcha1
        '
        Me.LblMarcha1.AutoSize = True
        Me.LblMarcha1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMarcha1.ForeColor = System.Drawing.Color.White
        Me.LblMarcha1.Location = New System.Drawing.Point(12, 33)
        Me.LblMarcha1.Name = "LblMarcha1"
        Me.LblMarcha1.Size = New System.Drawing.Size(107, 16)
        Me.LblMarcha1.TabIndex = 20
        Me.LblMarcha1.Text = "Marcha Deseada"
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.LblMarcha)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(128, 26)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'LblMarcha
        '
        Me.LblMarcha.AutoSize = True
        Me.LblMarcha.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblMarcha.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblMarcha.Location = New System.Drawing.Point(44, 6)
        Me.LblMarcha.Name = "LblMarcha"
        Me.LblMarcha.Size = New System.Drawing.Size(52, 16)
        Me.LblMarcha.TabIndex = 19
        Me.LblMarcha.Text = "Marcha"
        '
        'BtnRpmMinus50
        '
        Me.BtnRpmMinus50.Animated = True
        Me.BtnRpmMinus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.BorderRadius = 5
        Me.BtnRpmMinus50.BorderThickness = 1
        Me.BtnRpmMinus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus50.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnRpmMinus50.Location = New System.Drawing.Point(144, 180)
        Me.BtnRpmMinus50.Name = "BtnRpmMinus50"
        Me.BtnRpmMinus50.Size = New System.Drawing.Size(35, 35)
        Me.BtnRpmMinus50.TabIndex = 76
        '
        'BtnRpmPlus50
        '
        Me.BtnRpmPlus50.Animated = True
        Me.BtnRpmPlus50.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.BorderRadius = 5
        Me.BtnRpmPlus50.BorderThickness = 1
        Me.BtnRpmPlus50.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus50.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus50.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus50.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus50.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnRpmPlus50.Location = New System.Drawing.Point(144, 61)
        Me.BtnRpmPlus50.Name = "BtnRpmPlus50"
        Me.BtnRpmPlus50.Size = New System.Drawing.Size(35, 35)
        Me.BtnRpmPlus50.TabIndex = 75
        '
        'BtnRpmMinus25
        '
        Me.BtnRpmMinus25.Animated = True
        Me.BtnRpmMinus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.BorderRadius = 5
        Me.BtnRpmMinus25.BorderThickness = 1
        Me.BtnRpmMinus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmMinus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmMinus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmMinus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmMinus25.Image = Global.sFRAC.My.Resources.Resources.down_arrow
        Me.BtnRpmMinus25.Location = New System.Drawing.Point(149, 144)
        Me.BtnRpmMinus25.Name = "BtnRpmMinus25"
        Me.BtnRpmMinus25.Size = New System.Drawing.Size(25, 25)
        Me.BtnRpmMinus25.TabIndex = 74
        '
        'BtnRpmPlus25
        '
        Me.BtnRpmPlus25.Animated = True
        Me.BtnRpmPlus25.BorderColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.BorderRadius = 5
        Me.BtnRpmPlus25.BorderThickness = 1
        Me.BtnRpmPlus25.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRpmPlus25.FillColor = System.Drawing.Color.Transparent
        Me.BtnRpmPlus25.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnRpmPlus25.ForeColor = System.Drawing.Color.White
        Me.BtnRpmPlus25.Image = Global.sFRAC.My.Resources.Resources.up_arrow
        Me.BtnRpmPlus25.Location = New System.Drawing.Point(149, 108)
        Me.BtnRpmPlus25.Name = "BtnRpmPlus25"
        Me.BtnRpmPlus25.Size = New System.Drawing.Size(25, 25)
        Me.BtnRpmPlus25.TabIndex = 73
        '
        'PanelRPM
        '
        Me.PanelRPM.BackColor = System.Drawing.Color.Transparent
        Me.PanelRPM.BorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelRPM.BorderRadius = 6
        Me.PanelRPM.BorderThickness = 1
        Me.PanelRPM.Controls.Add(Me.BtnResetRPM)
        Me.PanelRPM.Controls.Add(Me.BtnIdle)
        Me.PanelRPM.Controls.Add(Me.LblRPMActual)
        Me.PanelRPM.Controls.Add(Me.LblRPMDeseada)
        Me.PanelRPM.Controls.Add(Me.LblRPM3)
        Me.PanelRPM.Controls.Add(Me.LblRPM1)
        Me.PanelRPM.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.PanelRPM.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.PanelRPM.FillColor = System.Drawing.Color.Transparent
        Me.PanelRPM.Location = New System.Drawing.Point(10, 43)
        Me.PanelRPM.Name = "PanelRPM"
        Me.PanelRPM.Size = New System.Drawing.Size(128, 174)
        Me.PanelRPM.TabIndex = 72
        '
        'BtnResetRPM
        '
        Me.BtnResetRPM.Animated = True
        Me.BtnResetRPM.BorderColor = System.Drawing.Color.White
        Me.BtnResetRPM.BorderRadius = 5
        Me.BtnResetRPM.BorderThickness = 1
        Me.BtnResetRPM.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnResetRPM.FillColor = System.Drawing.Color.Transparent
        Me.BtnResetRPM.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnResetRPM.ForeColor = System.Drawing.Color.White
        Me.BtnResetRPM.Location = New System.Drawing.Point(18, 62)
        Me.BtnResetRPM.Name = "BtnResetRPM"
        Me.BtnResetRPM.Size = New System.Drawing.Size(92, 70)
        Me.BtnResetRPM.TabIndex = 97
        Me.BtnResetRPM.Text = "Reset RPM"
        Me.BtnResetRPM.Visible = False
        '
        'BtnIdle
        '
        Me.BtnIdle.Animated = True
        Me.BtnIdle.BorderColor = System.Drawing.Color.White
        Me.BtnIdle.BorderRadius = 5
        Me.BtnIdle.BorderThickness = 1
        Me.BtnIdle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnIdle.FillColor = System.Drawing.Color.Transparent
        Me.BtnIdle.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnIdle.ForeColor = System.Drawing.Color.White
        Me.BtnIdle.Location = New System.Drawing.Point(19, 138)
        Me.BtnIdle.Name = "BtnIdle"
        Me.BtnIdle.Size = New System.Drawing.Size(90, 30)
        Me.BtnIdle.TabIndex = 69
        Me.BtnIdle.Text = "IDLE"
        '
        'LblRPMActual
        '
        Me.LblRPMActual.AutoSize = True
        Me.LblRPMActual.Font = New System.Drawing.Font("Montserrat", 15.0!)
        Me.LblRPMActual.ForeColor = System.Drawing.Color.White
        Me.LblRPMActual.Location = New System.Drawing.Point(54, 105)
        Me.LblRPMActual.Name = "LblRPMActual"
        Me.LblRPMActual.Size = New System.Drawing.Size(20, 27)
        Me.LblRPMActual.TabIndex = 23
        Me.LblRPMActual.Text = "-"
        '
        'LblRPMDeseada
        '
        Me.LblRPMDeseada.AutoSize = True
        Me.LblRPMDeseada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LblRPMDeseada.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblRPMDeseada.ForeColor = System.Drawing.Color.White
        Me.LblRPMDeseada.Location = New System.Drawing.Point(58, 60)
        Me.LblRPMDeseada.Name = "LblRPMDeseada"
        Me.LblRPMDeseada.Size = New System.Drawing.Size(12, 16)
        Me.LblRPMDeseada.TabIndex = 22
        Me.LblRPMDeseada.Text = "-"
        '
        'LblRPM3
        '
        Me.LblRPM3.AutoSize = True
        Me.LblRPM3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblRPM3.ForeColor = System.Drawing.Color.White
        Me.LblRPM3.Location = New System.Drawing.Point(18, 86)
        Me.LblRPM3.Name = "LblRPM3"
        Me.LblRPM3.Size = New System.Drawing.Size(91, 16)
        Me.LblRPM3.TabIndex = 21
        Me.LblRPM3.Text = "RPM Actuales"
        '
        'LblRPM1
        '
        Me.LblRPM1.AutoSize = True
        Me.LblRPM1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblRPM1.ForeColor = System.Drawing.Color.White
        Me.LblRPM1.Location = New System.Drawing.Point(15, 35)
        Me.LblRPM1.Name = "LblRPM1"
        Me.LblRPM1.Size = New System.Drawing.Size(97, 16)
        Me.LblRPM1.TabIndex = 20
        Me.LblRPM1.Text = "RPM Deseadas"
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.LblRpm)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(128, 26)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'LblRpm
        '
        Me.LblRpm.AutoSize = True
        Me.LblRpm.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.LblRpm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LblRpm.Location = New System.Drawing.Point(49, 4)
        Me.LblRpm.Name = "LblRpm"
        Me.LblRpm.Size = New System.Drawing.Size(36, 16)
        Me.LblRpm.TabIndex = 19
        Me.LblRpm.Text = "RPM"
        '
        'BtnErrores
        '
        Me.BtnErrores.Animated = True
        Me.BtnErrores.BorderColor = System.Drawing.Color.White
        Me.BtnErrores.BorderRadius = 5
        Me.BtnErrores.BorderThickness = 1
        Me.BtnErrores.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnErrores.FillColor = System.Drawing.Color.Transparent
        Me.BtnErrores.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.BtnErrores.ForeColor = System.Drawing.Color.White
        Me.BtnErrores.Location = New System.Drawing.Point(209, 14)
        Me.BtnErrores.Name = "BtnErrores"
        Me.BtnErrores.Size = New System.Drawing.Size(84, 20)
        Me.BtnErrores.TabIndex = 92
        Me.BtnErrores.Text = "Errores"
        '
        'PbAutomatic
        '
        Me.PbAutomatic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PbAutomatic.Image = Global.sFRAC.My.Resources.Resources.automatic1
        Me.PbAutomatic.ImageRotate = 0!
        Me.PbAutomatic.Location = New System.Drawing.Point(211, 161)
        Me.PbAutomatic.Name = "PbAutomatic"
        Me.PbAutomatic.Size = New System.Drawing.Size(50, 50)
        Me.PbAutomatic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbAutomatic.TabIndex = 96
        Me.PbAutomatic.TabStop = False
        Me.PbAutomatic.Visible = False
        '
        'TmrReconnect
        '
        Me.TmrReconnect.Interval = 1000
        '
        'TmrErrorHandle
        '
        Me.TmrErrorHandle.Interval = 600
        '
        'TmrCheck
        '
        Me.TmrCheck.Interval = 1000
        '
        'TmrRead
        '
        Me.TmrRead.Interval = 1000
        '
        'TmrBackUp
        '
        Me.TmrBackUp.Interval = 1000
        '
        'TmrOp
        '
        Me.TmrOp.Interval = 1000
        '
        'BtnEKill
        '
        Me.BtnEKill.Animated = True
        Me.BtnEKill.BorderColor = System.Drawing.Color.White
        Me.BtnEKill.BorderRadius = 5
        Me.BtnEKill.CheckedState.FillColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BtnEKill.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnEKill.FillColor = System.Drawing.Color.Transparent
        Me.BtnEKill.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnEKill.ForeColor = System.Drawing.Color.White
        Me.BtnEKill.Image = Global.sFRAC.My.Resources.Resources.E_STOP
        Me.BtnEKill.ImageOffset = New System.Drawing.Point(1, 0)
        Me.BtnEKill.ImageSize = New System.Drawing.Size(49, 49)
        Me.BtnEKill.Location = New System.Drawing.Point(246, 78)
        Me.BtnEKill.Name = "BtnEKill"
        Me.BtnEKill.Size = New System.Drawing.Size(50, 50)
        Me.BtnEKill.TabIndex = 190
        '
        'FourFrac_1280x840
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.Controls.Add(Me.BtnEKill)
        Me.Controls.Add(Me.PbAutomatic)
        Me.Controls.Add(Me.BtnErrores)
        Me.Controls.Add(Me.Guna2Panel11)
        Me.Controls.Add(Me.BtnAjustes)
        Me.Controls.Add(Me.BtnStartEngine)
        Me.Controls.Add(Me.LblErrorAdmisible)
        Me.Controls.Add(Me.LblErrorGrave)
        Me.Controls.Add(Me.Guna2Panel9)
        Me.Controls.Add(Me.Guna2Panel10)
        Me.Controls.Add(Me.BtnMinusGear)
        Me.Controls.Add(Me.BtnPlusGear)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.Controls.Add(Me.BtnRpmMinus50)
        Me.Controls.Add(Me.BtnRpmPlus50)
        Me.Controls.Add(Me.BtnRpmMinus25)
        Me.Controls.Add(Me.BtnRpmPlus25)
        Me.Controls.Add(Me.PanelRPM)
        Me.DoubleBuffered = True
        Me.Name = "FourFrac_1280x840"
        Me.Size = New System.Drawing.Size(296, 638)
        Me.Guna2Panel11.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.ResumeLayout(False)
        Me.Guna2CustomGradientPanel11.PerformLayout()
        Me.Guna2Panel9.ResumeLayout(False)
        Me.Guna2Panel9.PerformLayout()
        Me.Guna2CustomGradientPanel9.ResumeLayout(False)
        Me.Guna2CustomGradientPanel9.PerformLayout()
        Me.Guna2Panel10.ResumeLayout(False)
        Me.Guna2Panel10.PerformLayout()
        Me.Guna2CustomGradientPanel10.ResumeLayout(False)
        Me.Guna2CustomGradientPanel10.PerformLayout()
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Guna2Panel1.PerformLayout()
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.PanelRPM.ResumeLayout(False)
        Me.PanelRPM.PerformLayout()
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        CType(Me.PbAutomatic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents Guna2Panel11 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnEmpezarTrabajo As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnSemiAuto As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnEncenderLuces As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblOpciones As Label
    Public WithEvents BtnAjustes As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnStartEngine As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblErrorAdmisible As Label
    Public WithEvents LblErrorGrave As Label
    Public WithEvents Guna2Panel9 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Label2 As Label
    Public WithEvents Label1 As Label
    Public WithEvents BtnTripZero As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnTrip As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblPCorteActual As Label
    Public WithEvents LblPCorteDeseada As Label
    Public WithEvents LblPCorte3 As Label
    Public WithEvents LblPCorte1 As Label
    Public WithEvents LblPCorte As Label
    Public WithEvents Guna2Panel10 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblCaudal2 As Label
    Public WithEvents LblCaudalActual As Label
    Public WithEvents LblCaudal As Label
    Public WithEvents BtnMinusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnPlusGear As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents LblMarchaActual As Label
    Public WithEvents LblMarchaDeseada As Label
    Public WithEvents LblMarcha3 As Label
    Public WithEvents LblMarcha1 As Label
    Public WithEvents LblMarcha As Label
    Public WithEvents BtnRpmMinus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus50 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmMinus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnRpmPlus25 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PanelRPM As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnIdle As Guna.UI2.WinForms.Guna2Button
    Public WithEvents LblRPMActual As Label
    Public WithEvents LblRPMDeseada As Label
    Public WithEvents LblRPM3 As Label
    Public WithEvents LblRPM1 As Label
    Public WithEvents LblRpm As Label
    Public WithEvents BtnErrores As Guna.UI2.WinForms.Guna2Button
    Public WithEvents BtnFC As Guna.UI2.WinForms.Guna2Button
    Public WithEvents PbAutomatic As Guna.UI2.WinForms.Guna2PictureBox
    Public WithEvents BtnResetRPM As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel11 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel9 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel10 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents TmrReconnect As Timer
    Public WithEvents TmrErrorHandle As Timer
    Public WithEvents TmrCheck As Timer
    Public WithEvents TmrRead As Timer
    Friend WithEvents TmrBackUp As Timer
    Friend WithEvents TmrOp As Timer
    Public WithEvents BtnEKill As Guna.UI2.WinForms.Guna2Button
End Class
