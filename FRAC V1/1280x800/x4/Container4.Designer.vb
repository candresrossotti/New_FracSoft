﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Container4_1280x840
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.Guna2Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel1 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd1 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel1 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize1 = New Guna.UI2.WinForms.Guna2Button()
        Me.M1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Title1 = New System.Windows.Forms.Label()
        Me.CloseButton1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel2 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd2 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel2 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize2 = New Guna.UI2.WinForms.Guna2Button()
        Me.Title2 = New System.Windows.Forms.Label()
        Me.CloseButton2 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.M2 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel5 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel3 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd3 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel4 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize3 = New Guna.UI2.WinForms.Guna2Button()
        Me.Title3 = New System.Windows.Forms.Label()
        Me.CloseButton3 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.M3 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel9 = New Guna.UI2.WinForms.Guna2Panel()
        Me.Panel4 = New Guna.UI2.WinForms.Guna2Panel()
        Me.BtnAdd4 = New Guna.UI2.WinForms.Guna2Button()
        Me.Guna2CustomGradientPanel5 = New Guna.UI2.WinForms.Guna2CustomGradientPanel()
        Me.Maximize4 = New Guna.UI2.WinForms.Guna2Button()
        Me.Title4 = New System.Windows.Forms.Label()
        Me.CloseButton4 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.M4 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.Guna2Panel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Guna2CustomGradientPanel1.SuspendLayout()
        Me.Guna2Panel2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Guna2CustomGradientPanel2.SuspendLayout()
        Me.Guna2Panel5.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Guna2CustomGradientPanel4.SuspendLayout()
        Me.Guna2Panel9.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Guna2CustomGradientPanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6R
        Me.Guna2BorderlessForm1.DragForm = False
        Me.Guna2BorderlessForm1.HasFormShadow = False
        Me.Guna2BorderlessForm1.ResizeForm = False
        Me.Guna2BorderlessForm1.TransparentWhileDrag = True
        '
        'Guna2Panel1
        '
        Me.Guna2Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel1.BorderRadius = 6
        Me.Guna2Panel1.BorderThickness = 1
        Me.Guna2Panel1.Controls.Add(Me.Panel1)
        Me.Guna2Panel1.Controls.Add(Me.Guna2CustomGradientPanel1)
        Me.Guna2Panel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Guna2Panel1.Name = "Guna2Panel1"
        Me.Guna2Panel1.Size = New System.Drawing.Size(298, 663)
        Me.Guna2Panel1.TabIndex = 69
        '
        'Panel1
        '
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel1.Controls.Add(Me.BtnAdd1)
        Me.Panel1.Location = New System.Drawing.Point(1, 23)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(296, 638)
        Me.Panel1.TabIndex = 57
        '
        'BtnAdd1
        '
        Me.BtnAdd1.AutoRoundedCorners = True
        Me.BtnAdd1.BorderRadius = 49
        Me.BtnAdd1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd1.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd1.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd1.ForeColor = System.Drawing.Color.White
        Me.BtnAdd1.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd1.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd1.Location = New System.Drawing.Point(99, 292)
        Me.BtnAdd1.Name = "BtnAdd1"
        Me.BtnAdd1.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd1.TabIndex = 0
        '
        'Guna2CustomGradientPanel1
        '
        Me.Guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel1.BorderRadius = 6
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.Maximize1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.M1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.Title1)
        Me.Guna2CustomGradientPanel1.Controls.Add(Me.CloseButton1)
        Me.Guna2CustomGradientPanel1.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel1.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel1.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel1.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel1.Name = "Guna2CustomGradientPanel1"
        Me.Guna2CustomGradientPanel1.Size = New System.Drawing.Size(298, 23)
        Me.Guna2CustomGradientPanel1.TabIndex = 19
        '
        'Maximize1
        '
        Me.Maximize1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize1.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize1.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize1.Enabled = False
        Me.Maximize1.FillColor = System.Drawing.Color.Transparent
        Me.Maximize1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize1.ForeColor = System.Drawing.Color.White
        Me.Maximize1.Location = New System.Drawing.Point(249, 1)
        Me.Maximize1.Name = "Maximize1"
        Me.Maximize1.Size = New System.Drawing.Size(20, 20)
        Me.Maximize1.TabIndex = 73
        Me.Maximize1.UseTransparentBackground = True
        '
        'M1
        '
        Me.M1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M1.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M1.Enabled = False
        Me.M1.FillColor = System.Drawing.Color.Transparent
        Me.M1.IconColor = System.Drawing.Color.White
        Me.M1.Location = New System.Drawing.Point(249, 1)
        Me.M1.Name = "M1"
        Me.M1.Size = New System.Drawing.Size(20, 20)
        Me.M1.TabIndex = 1
        '
        'Title1
        '
        Me.Title1.AutoSize = True
        Me.Title1.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Title1.ForeColor = System.Drawing.Color.White
        Me.Title1.Location = New System.Drawing.Point(127, 3)
        Me.Title1.Name = "Title1"
        Me.Title1.Size = New System.Drawing.Size(45, 16)
        Me.Title1.TabIndex = 19
        Me.Title1.Text = "Titulo1"
        '
        'CloseButton1
        '
        Me.CloseButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton1.Enabled = False
        Me.CloseButton1.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton1.IconColor = System.Drawing.Color.White
        Me.CloseButton1.Location = New System.Drawing.Point(275, 1)
        Me.CloseButton1.Name = "CloseButton1"
        Me.CloseButton1.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton1.TabIndex = 72
        '
        'Guna2Panel2
        '
        Me.Guna2Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel2.BorderRadius = 6
        Me.Guna2Panel2.BorderThickness = 1
        Me.Guna2Panel2.Controls.Add(Me.Panel2)
        Me.Guna2Panel2.Controls.Add(Me.Guna2CustomGradientPanel2)
        Me.Guna2Panel2.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel2.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel2.Location = New System.Drawing.Point(316, 12)
        Me.Guna2Panel2.Name = "Guna2Panel2"
        Me.Guna2Panel2.Size = New System.Drawing.Size(298, 663)
        Me.Guna2Panel2.TabIndex = 70
        '
        'Panel2
        '
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel2.Controls.Add(Me.BtnAdd2)
        Me.Panel2.Location = New System.Drawing.Point(1, 23)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(296, 638)
        Me.Panel2.TabIndex = 57
        '
        'BtnAdd2
        '
        Me.BtnAdd2.AutoRoundedCorners = True
        Me.BtnAdd2.BorderRadius = 49
        Me.BtnAdd2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd2.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd2.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd2.ForeColor = System.Drawing.Color.White
        Me.BtnAdd2.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd2.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd2.Location = New System.Drawing.Point(99, 292)
        Me.BtnAdd2.Name = "BtnAdd2"
        Me.BtnAdd2.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd2.TabIndex = 0
        '
        'Guna2CustomGradientPanel2
        '
        Me.Guna2CustomGradientPanel2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel2.BorderRadius = 6
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.Maximize2)
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.Title2)
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.CloseButton2)
        Me.Guna2CustomGradientPanel2.Controls.Add(Me.M2)
        Me.Guna2CustomGradientPanel2.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel2.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel2.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel2.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel2.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel2.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel2.Name = "Guna2CustomGradientPanel2"
        Me.Guna2CustomGradientPanel2.Size = New System.Drawing.Size(298, 23)
        Me.Guna2CustomGradientPanel2.TabIndex = 19
        '
        'Maximize2
        '
        Me.Maximize2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize2.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize2.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize2.Enabled = False
        Me.Maximize2.FillColor = System.Drawing.Color.Transparent
        Me.Maximize2.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize2.ForeColor = System.Drawing.Color.White
        Me.Maximize2.Location = New System.Drawing.Point(249, 1)
        Me.Maximize2.Name = "Maximize2"
        Me.Maximize2.Size = New System.Drawing.Size(20, 20)
        Me.Maximize2.TabIndex = 74
        Me.Maximize2.UseTransparentBackground = True
        '
        'Title2
        '
        Me.Title2.AutoSize = True
        Me.Title2.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Title2.ForeColor = System.Drawing.Color.White
        Me.Title2.Location = New System.Drawing.Point(127, 3)
        Me.Title2.Name = "Title2"
        Me.Title2.Size = New System.Drawing.Size(45, 16)
        Me.Title2.TabIndex = 19
        Me.Title2.Text = "Titulo1"
        '
        'CloseButton2
        '
        Me.CloseButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton2.Enabled = False
        Me.CloseButton2.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton2.IconColor = System.Drawing.Color.White
        Me.CloseButton2.Location = New System.Drawing.Point(275, 1)
        Me.CloseButton2.Name = "CloseButton2"
        Me.CloseButton2.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton2.TabIndex = 72
        Me.CloseButton2.UseTransparentBackground = True
        '
        'M2
        '
        Me.M2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M2.Enabled = False
        Me.M2.FillColor = System.Drawing.Color.Transparent
        Me.M2.IconColor = System.Drawing.Color.White
        Me.M2.Location = New System.Drawing.Point(249, 1)
        Me.M2.Name = "M2"
        Me.M2.Size = New System.Drawing.Size(20, 20)
        Me.M2.TabIndex = 73
        '
        'Guna2Panel5
        '
        Me.Guna2Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel5.BorderRadius = 6
        Me.Guna2Panel5.BorderThickness = 1
        Me.Guna2Panel5.Controls.Add(Me.Panel3)
        Me.Guna2Panel5.Controls.Add(Me.Guna2CustomGradientPanel4)
        Me.Guna2Panel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel5.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel5.Location = New System.Drawing.Point(620, 12)
        Me.Guna2Panel5.Name = "Guna2Panel5"
        Me.Guna2Panel5.Size = New System.Drawing.Size(298, 663)
        Me.Guna2Panel5.TabIndex = 71
        '
        'Panel3
        '
        Me.Panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel3.Controls.Add(Me.BtnAdd3)
        Me.Panel3.Location = New System.Drawing.Point(1, 23)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(296, 638)
        Me.Panel3.TabIndex = 57
        '
        'BtnAdd3
        '
        Me.BtnAdd3.AutoRoundedCorners = True
        Me.BtnAdd3.BorderRadius = 49
        Me.BtnAdd3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd3.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd3.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd3.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd3.ForeColor = System.Drawing.Color.White
        Me.BtnAdd3.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd3.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd3.Location = New System.Drawing.Point(99, 292)
        Me.BtnAdd3.Name = "BtnAdd3"
        Me.BtnAdd3.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd3.TabIndex = 0
        '
        'Guna2CustomGradientPanel4
        '
        Me.Guna2CustomGradientPanel4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel4.BorderRadius = 6
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.Maximize3)
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.Title3)
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.CloseButton3)
        Me.Guna2CustomGradientPanel4.Controls.Add(Me.M3)
        Me.Guna2CustomGradientPanel4.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel4.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel4.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel4.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel4.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel4.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel4.Name = "Guna2CustomGradientPanel4"
        Me.Guna2CustomGradientPanel4.Size = New System.Drawing.Size(298, 23)
        Me.Guna2CustomGradientPanel4.TabIndex = 19
        '
        'Maximize3
        '
        Me.Maximize3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize3.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize3.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize3.Enabled = False
        Me.Maximize3.FillColor = System.Drawing.Color.Transparent
        Me.Maximize3.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize3.ForeColor = System.Drawing.Color.White
        Me.Maximize3.Location = New System.Drawing.Point(249, 1)
        Me.Maximize3.Name = "Maximize3"
        Me.Maximize3.Size = New System.Drawing.Size(20, 20)
        Me.Maximize3.TabIndex = 75
        Me.Maximize3.UseTransparentBackground = True
        '
        'Title3
        '
        Me.Title3.AutoSize = True
        Me.Title3.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Title3.ForeColor = System.Drawing.Color.White
        Me.Title3.Location = New System.Drawing.Point(127, 3)
        Me.Title3.Name = "Title3"
        Me.Title3.Size = New System.Drawing.Size(45, 16)
        Me.Title3.TabIndex = 19
        Me.Title3.Text = "Titulo1"
        '
        'CloseButton3
        '
        Me.CloseButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton3.Enabled = False
        Me.CloseButton3.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton3.IconColor = System.Drawing.Color.White
        Me.CloseButton3.Location = New System.Drawing.Point(275, 1)
        Me.CloseButton3.Name = "CloseButton3"
        Me.CloseButton3.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton3.TabIndex = 72
        '
        'M3
        '
        Me.M3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M3.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M3.Enabled = False
        Me.M3.FillColor = System.Drawing.Color.Transparent
        Me.M3.IconColor = System.Drawing.Color.White
        Me.M3.Location = New System.Drawing.Point(249, 1)
        Me.M3.Name = "M3"
        Me.M3.Size = New System.Drawing.Size(20, 20)
        Me.M3.TabIndex = 74
        '
        'Guna2Panel9
        '
        Me.Guna2Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2Panel9.BorderRadius = 6
        Me.Guna2Panel9.BorderThickness = 1
        Me.Guna2Panel9.Controls.Add(Me.Panel4)
        Me.Guna2Panel9.Controls.Add(Me.Guna2CustomGradientPanel5)
        Me.Guna2Panel9.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(94, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(157, Byte), Integer))
        Me.Guna2Panel9.FillColor = System.Drawing.Color.Transparent
        Me.Guna2Panel9.Location = New System.Drawing.Point(925, 12)
        Me.Guna2Panel9.Name = "Guna2Panel9"
        Me.Guna2Panel9.Size = New System.Drawing.Size(298, 663)
        Me.Guna2Panel9.TabIndex = 72
        '
        'Panel4
        '
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel4.Controls.Add(Me.BtnAdd4)
        Me.Panel4.Location = New System.Drawing.Point(1, 23)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(296, 638)
        Me.Panel4.TabIndex = 57
        '
        'BtnAdd4
        '
        Me.BtnAdd4.AutoRoundedCorners = True
        Me.BtnAdd4.BorderRadius = 49
        Me.BtnAdd4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAdd4.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd4.FillColor = System.Drawing.Color.Transparent
        Me.BtnAdd4.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BtnAdd4.ForeColor = System.Drawing.Color.White
        Me.BtnAdd4.Image = Global.sFRAC.My.Resources.Resources.add_400px
        Me.BtnAdd4.ImageSize = New System.Drawing.Size(90, 90)
        Me.BtnAdd4.Location = New System.Drawing.Point(99, 292)
        Me.BtnAdd4.Name = "BtnAdd4"
        Me.BtnAdd4.Size = New System.Drawing.Size(100, 100)
        Me.BtnAdd4.TabIndex = 0
        '
        'Guna2CustomGradientPanel5
        '
        Me.Guna2CustomGradientPanel5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.Guna2CustomGradientPanel5.BorderRadius = 6
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.Maximize4)
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.Title4)
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.CloseButton4)
        Me.Guna2CustomGradientPanel5.Controls.Add(Me.M4)
        Me.Guna2CustomGradientPanel5.CustomBorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Guna2CustomGradientPanel5.CustomBorderThickness = New System.Windows.Forms.Padding(1, 1, 1, 0)
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomLeft = False
        Me.Guna2CustomGradientPanel5.CustomizableEdges.BottomRight = False
        Me.Guna2CustomGradientPanel5.FillColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor2 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor3 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel5.FillColor4 = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.Guna2CustomGradientPanel5.Location = New System.Drawing.Point(0, 0)
        Me.Guna2CustomGradientPanel5.Name = "Guna2CustomGradientPanel5"
        Me.Guna2CustomGradientPanel5.Size = New System.Drawing.Size(298, 23)
        Me.Guna2CustomGradientPanel5.TabIndex = 19
        '
        'Maximize4
        '
        Me.Maximize4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Maximize4.DisabledState.BorderColor = System.Drawing.Color.DarkGray
        Me.Maximize4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray
        Me.Maximize4.DisabledState.FillColor = System.Drawing.Color.Transparent
        Me.Maximize4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.Maximize4.Enabled = False
        Me.Maximize4.FillColor = System.Drawing.Color.Transparent
        Me.Maximize4.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Maximize4.ForeColor = System.Drawing.Color.White
        Me.Maximize4.Location = New System.Drawing.Point(249, 1)
        Me.Maximize4.Name = "Maximize4"
        Me.Maximize4.Size = New System.Drawing.Size(20, 20)
        Me.Maximize4.TabIndex = 76
        Me.Maximize4.UseTransparentBackground = True
        '
        'Title4
        '
        Me.Title4.AutoSize = True
        Me.Title4.Font = New System.Drawing.Font("Montserrat", 9.0!)
        Me.Title4.ForeColor = System.Drawing.Color.White
        Me.Title4.Location = New System.Drawing.Point(127, 3)
        Me.Title4.Name = "Title4"
        Me.Title4.Size = New System.Drawing.Size(45, 16)
        Me.Title4.TabIndex = 19
        Me.Title4.Text = "Titulo1"
        '
        'CloseButton4
        '
        Me.CloseButton4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CloseButton4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CloseButton4.Enabled = False
        Me.CloseButton4.FillColor = System.Drawing.Color.Transparent
        Me.CloseButton4.IconColor = System.Drawing.Color.White
        Me.CloseButton4.Location = New System.Drawing.Point(275, 1)
        Me.CloseButton4.Name = "CloseButton4"
        Me.CloseButton4.Size = New System.Drawing.Size(20, 20)
        Me.CloseButton4.TabIndex = 72
        '
        'M4
        '
        Me.M4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M4.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox
        Me.M4.Enabled = False
        Me.M4.FillColor = System.Drawing.Color.Transparent
        Me.M4.IconColor = System.Drawing.Color.White
        Me.M4.Location = New System.Drawing.Point(249, 1)
        Me.M4.Name = "M4"
        Me.M4.Size = New System.Drawing.Size(20, 20)
        Me.M4.TabIndex = 75
        '
        'Container4_1280x840
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1235, 685)
        Me.Controls.Add(Me.Guna2Panel9)
        Me.Controls.Add(Me.Guna2Panel5)
        Me.Controls.Add(Me.Guna2Panel2)
        Me.Controls.Add(Me.Guna2Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Container4_1280x840"
        Me.Text = "Container4"
        Me.Guna2Panel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.ResumeLayout(False)
        Me.Guna2CustomGradientPanel1.PerformLayout()
        Me.Guna2Panel2.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.ResumeLayout(False)
        Me.Guna2CustomGradientPanel2.PerformLayout()
        Me.Guna2Panel5.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel4.PerformLayout()
        Me.Guna2Panel9.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Guna2CustomGradientPanel5.ResumeLayout(False)
        Me.Guna2CustomGradientPanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents Title1 As Label
    Public WithEvents Title4 As Label
    Public WithEvents Title3 As Label
    Public WithEvents Title2 As Label
    Public WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Public WithEvents Guna2Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel1 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnAdd1 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel1 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2Panel9 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel4 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnAdd4 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel5 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2Panel5 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel3 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnAdd3 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel4 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents Guna2Panel2 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents Panel2 As Guna.UI2.WinForms.Guna2Panel
    Public WithEvents BtnAdd2 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Guna2CustomGradientPanel2 As Guna.UI2.WinForms.Guna2CustomGradientPanel
    Public WithEvents CloseButton4 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents CloseButton3 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents CloseButton2 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents CloseButton1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M4 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M3 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M2 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents M1 As Guna.UI2.WinForms.Guna2ControlBox
    Public WithEvents Maximize1 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Maximize4 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Maximize3 As Guna.UI2.WinForms.Guna2Button
    Public WithEvents Maximize2 As Guna.UI2.WinForms.Guna2Button
End Class
