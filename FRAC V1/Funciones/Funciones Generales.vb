﻿Module Funciones

    ''----------------------------------------------------------------------------------------------------------------------------------------------------''
    ''----------------------------------------------------------------------------------------------------------------------------------------------------''
    ''                                                                FUNCIONES GENERALES                                                                 ''
    ''----------------------------------------------------------------------------------------------------------------------------------------------------''
    ''----------------------------------------------------------------------------------------------------------------------------------------------------''

    ''Sub para el manejo de luces
    Public Sub HandleLuces(ByVal sender As Object, ByVal Frac As Integer)
        If sender.Checked = True Then
            ISystem.MoxaSend(FracMoxaSendStartAddress("Luces_StartAddress"), FracGeneralValues("LucesON"), Frac)
            sender.Text = LanguageSelect.DynamicLanguageRM.GetString("Luces.Apagar")
        Else
            ISystem.MoxaSend(FracMoxaSendStartAddress("Luces_StartAddress"), FracGeneralValues("LucesOFF"), Frac)
            sender.Text = LanguageSelect.DynamicLanguageRM.GetString("Luces.Encender")
        End If
    End Sub

    ''Sub para iniciar todas las ventanas
    Public Sub InitWindows()
        Dim Screens As Screen() = Screen.AllScreens()                           ''Buscamos las pantallas disponibles
        If My.Settings.TotalScreens = 1 Then
            ''Desactiamos la pantalla para evitar que la toquen/seteamos la ubicacion
            Principal.Enabled = False
            Principal.Location = Screen.PrimaryScreen.WorkingArea.Location
            Principal.Show()
        ElseIf My.Settings.TotalScreens = 2 Then
            ''Desactiamos la pantalla para evitar que la toquen/seteamos la ubicacion
            ''Si quieren ver dos pantallas pero solo conectaron una iniciamos todo como se pueda
            Secundaria.Enabled = False
            If Screens.Length > 1 Then
                Secundaria.Location = Screens(0).WorkingArea.Location
            Else
                Secundaria.Location = Screen.PrimaryScreen.WorkingArea.Location
            End If

            Principal.Enabled = False
            Principal.Location = Screen.PrimaryScreen.WorkingArea.Location

            Secundaria.Show()
            Principal.Show()

        ElseIf My.Settings.TotalScreens = 3 Then

            ''Desactiamos la pantalla para evitar que la toquen/seteamos la ubicacion
            ''Si quieren ver dos pantallas pero solo conectaron una iniciamos todo como se pueda

            Secundaria.Enabled = False
            Principal.Enabled = False
            Tercera.Enabled = False

            Select Case Screens.Length
                Case 1
                    Tercera.Location = Screen.PrimaryScreen.WorkingArea.Location
                    Secundaria.Location = Screen.PrimaryScreen.WorkingArea.Location
                    Principal.Location = Screen.PrimaryScreen.WorkingArea.Location
                Case 2
                    Tercera.Location = Screen.AllScreens(1).WorkingArea.Location
                    Secundaria.Location = Screen.AllScreens(1).WorkingArea.Location
                    Principal.Location = Screen.PrimaryScreen.WorkingArea.Location
                Case 3
                    Tercera.Location = Screen.AllScreens(2).WorkingArea.Location
                    Secundaria.Location = Screen.AllScreens(1).WorkingArea.Location
                    Principal.Location = Screen.PrimaryScreen.WorkingArea.Location
            End Select
            Tercera.Show()
            Secundaria.Show()
            Principal.Show()
        End If
    End Sub

    ''Agregamos el primer control al primer contenedor de la primera pantalla
    Public Sub AddFirstControl(currentControl As Integer)
        Select Case currentControl
            Case 1
                Container1_1.BtnAdd.Visible = False
                Container1_1.Title1.Text = CurrentFracName
                Container1_1.CloseButton1.Enabled = True
                CenterObject(Container1_1.Title1)
                Dim C1 = OneFrac.GetClass()
                Container1_1.Panel1.Controls.Add(C1)
                LSelect.OneFracLanguageSet(C1)
                C1.Show()
            Case 2
                Container2_1.BtnAdd1.Visible = False
                Container2_1.Title1.Text = CurrentFracName
                Container2_1.Maximize1.Enabled = True
                Container2_1.CloseButton1.Enabled = True
                CenterObject(Container2_1.Title1)
                Dim C2 = TwoFrac.GetClass()
                Container2_1.Panel1.Controls.Add(C2)
                LSelect.TwoFracLanguageSet(C2)
                C2.Show()
            Case 4
                Container4_1.BtnAdd1.Visible = False
                Container4_1.Title1.Text = CurrentFracName
                Container4_1.Maximize1.Enabled = True
                Container4_1.CloseButton1.Enabled = True
                CenterObject(Container4_1.Title1)
                Dim C4 = FourFrac.GetClass()
                Container4_1.Panel1.Controls.Add(C4)
                LSelect.FourFracLanguageSet(C4)
                C4.Show()
            Case 8
                Container8_1.BtnAdd1.Visible = False
                Container8_1.Title1.Text = CurrentFracName
                Container8_1.Maximize1.Enabled = True
                Container8_1.CloseButton1.Enabled = True
                CenterObject(Container8_1.Title1)
                Dim C8 = EightFrac.GetClass()
                Container8_1.Panel1.Controls.Add(C8)
                LSelect.EightFracLanguageSet(C8)
                C8.Show()
        End Select
    End Sub


    ''----------------------------------------------------------------------------------------------------------------------------------------------------''
    ''----------------------------------------------------------------------------------------------------------------------------------------------------''
    ''                                                                FUNCIONES GENERALES                                                                 ''
    ''----------------------------------------------------------------------------------------------------------------------------------------------------''
    ''----------------------------------------------------------------------------------------------------------------------------------------------------''


    ''ASIGNAR A CADA VARIABLE LO LEIDO
    Public Sub FracDataAssign(ByVal resultDisplay As Integer(), ByVal nFrac As Integer)
        Try
            ''Viatran       
            FracData.DblVFrac("TRIP_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("TRIP_StartAddress")) + PZero(nFrac)).ToString("F0")
            FracData.DblVFrac("Viatran_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("Viatran_StartAddress")) + PZero(nFrac)).ToString("F0")
            FracData.DblVFrac("Viatran_RAW_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("Viatran_RAW_StartAddress")).ToString("F0")
            FracData.DblVFrac("Viatran_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("Viatran_Alarm_Active_StartAddress")).ToString("F0")
            FracData.DblVFrac("Viatran_Scaled_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("Viatran_Scaled_Alarm_Active_StartAddress")).ToString("F0")

            ''Temperatura LUB LP
            FracData.DblVFrac("TemperaturaLUB_LP_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("TemperaturaLUB_LP_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("TemperaturaLUB_LP_RAW_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("TemperaturaLUB_LP_RAW_StartAddress")).ToString("F0")
            FracData.DblVFrac("TemperaturaLUB_LP_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("TemperaturaLUB_LP_Alarm_Active_StartAddress")).ToString("F0")
            FracData.DblVFrac("TemperaturaLUB_LP_Scaled_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("TemperaturaLUB_LP_Scaled_Alarm_Active_StartAddress")).ToString("F0")

            ''Temperatura LUB HP
            FracData.DblVFrac("TemperaturaLUB_HP_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("TemperaturaLUB_HP_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("TemperaturaLUB_HP_RAW_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("TemperaturaLUB_HP_RAW_StartAddress")).ToString("F0")
            FracData.DblVFrac("TemperaturaLUB_HP_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("TemperaturaLUB_HP_Alarm_Active_StartAddress")).ToString("F0")
            FracData.DblVFrac("TemperaturaLUB_HP_Scaled_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("TemperaturaLUB_HP_Scaled_Alarm_Active_StartAddress")).ToString("F0")

            ''Presion LUB LP
            FracData.DblVFrac("PresionLUB_LP_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("PresionLUB_LP_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("PresionLUB_LP_RAW_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresionLUB_LP_RAW_StartAddress")).ToString("F0")
            FracData.DblVFrac("PresionLUB_LP_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresionLUB_LP_Alarm_Active_StartAddress")).ToString("F0")
            FracData.DblVFrac("PresionLUB_LP_Scaled_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresionLUB_LP_Scaled_Alarm_Active_StartAddress")).ToString("F0")

            ''Presion LUB HP
            FracData.DblVFrac("PresionLUB_HP_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("PresionLUB_HP_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("PresionLUB_HP_RAW_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresionLUB_HP_RAW_StartAddress")).ToString("F0")
            FracData.DblVFrac("PresionLUB_HP_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresionLUB_HP_Alarm_Active_StartAddress")).ToString("F0")
            FracData.DblVFrac("PresionLUB_HP_Scaled_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresionLUB_HP_Scaled_Alarm_Active_StartAddress")).ToString("F0")

            ''Presion Succion                                                                                                                                                                
            FracData.DblVFrac("PresSuc_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("PresSuc_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("PresSuc_RAW_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresSuc_RAW_StartAddress")).ToString("F0")
            FracData.DblVFrac("PresSuc_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresSuc_Alarm_Active_StartAddress")).ToString("F0")
            FracData.DblVFrac("PresSuc_Scaled_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresSuc_Scaled_Alarm_Active_StartAddress")).ToString("F0")

            ''Presion Transmision                                                                                                                                                            
            FracData.DblVFrac("PresTrans_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("PresTrans_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("PresTrans_RAW_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresTrans_RAW_StartAddress")).ToString("F0")
            FracData.DblVFrac("PresTrans_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresTrans_Alarm_Active_StartAddress")).ToString("F0")
            FracData.DblVFrac("PresTrans_Scaled_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("PresTrans_Scaled_Alarm_Active_StartAddress")).ToString("F0")

            ''Caudal                                                                                                                                                                         
            FracData.DblVFrac("Caudal_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("Caudal_StartAddress")) / 100 * FC(nFrac)).ToString("F1")
            FracData.DblVFrac("Caudal_Scaled_Alarm_Active_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("Caudal_Scaled_Alarm_Active_StartAddress")).ToString("F0")

            ''Valores de RPM                                                                                                                                                                 
            FracData.DblVFrac("RPMSalidaTransmision_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("RPMSalidaTransmision_StartAddress")).ToString("F1")
            FracData.DblVFrac("RPMMotor_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("RPMMotor_StartAddress")).ToString("F0")
            FracData.DblVFrac("RPMDeseada_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("RPMDeseada_StartAddress")).ToString("F0")

            ''Motor                                                                                                                                                                          
            FracData.DblVFrac("TemperaturaAguaMotor_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("TemperaturaAguaMotor_StartAddress")).ToString("F1")
            FracData.DblVFrac("TemperaturaAceiteMotor_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("TemperaturaAceiteMotor_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("PresionAceiteMotor_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("PresionAceiteMotor_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("FuelRate_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("FuelRate_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("Voltaje_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("Voltaje_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("CombustibleTanque_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("CombustibleTanque_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("HorasMotor_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("HorasMotor_StartAddress")).ToString("F1")
            FracData.DblVFrac("PorcentajeCarga_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("PorcentajeCarga_StartAddress")) / 100).ToString("F2")

            ''Bomba
            FracData.DblVFrac("HHPMotor_" + nFrac.ToString()) = (FracData.DblVFrac("Viatran_" + nFrac.ToString()) * FracData.DblVFrac("Caudal_" + nFrac.ToString()) / 40.8).ToString("F1")

            ''Transmision
            FracData.DblVFrac("TemperaturaAceiteTransmision_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("TemperaturaAceiteTransmision_StartAddress")) / 10).ToString("F1")
            FracData.DblVFrac("LockUp_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("LockUp_StartAddress")).ToString("F0")

            ''Display Marcha
            Dim MarchaDeseada As Integer = resultDisplay(FracMoxaReceiveStartAddress("MarchaDeseada_StartAddress"))
            If MarchaDeseada = 0 Then
                FracData.StrVFrac("MarchaDeseada_" + nFrac.ToString()) = "N"
            ElseIf MarchaDeseada = -1 Then
                FracData.StrVFrac("MarchaDeseada_" + nFrac.ToString()) = "P"
            Else
                FracData.StrVFrac("MarchaDeseada_" + nFrac.ToString()) = MarchaDeseada
            End If

            MarchaActualInt = resultDisplay(FracMoxaReceiveStartAddress("MarchaActual_StartAddress"))
            If MarchaActualInt = 0 Then
                FracData.StrVFrac("MarchaActual_" + nFrac.ToString()) = "N"
            ElseIf MarchaActualInt = -1 Then
                FracData.StrVFrac("MarchaActual_" + nFrac.ToString()) = "P"
            Else
                FracData.StrVFrac("MarchaActual_" + nFrac.ToString()) = MarchaActualInt
            End If

            ''Errores
            FracData.DblVFrac("LamparaRoja_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("LamparaRoja_StartAddress")).ToString("F0")
            FracData.DblVFrac("LamparaAmbar_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("LamparaAmbar_StartAddress")).ToString("F0")
            FracData.DblVFrac("ErrActual_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("ErrActual_StartAddress")).ToString("F0")
            FracData.DblVFrac("SPN_" + nFrac.ToString()) = (resultDisplay(FracMoxaReceiveStartAddress("SPN_HIGH_StartAddress")) * 65536 + resultDisplay(FracMoxaReceiveStartAddress("SPN_LOW_StartAddress"))).ToString("F0")
            FracData.DblVFrac("FMI_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("FMI_StartAddress")).ToString("F0")
            FracData.DblVFrac("OC_1" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("OC_StartAddress")).ToString("F0")
            FracData.DblVFrac("Direccion_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("Direccion_StartAddress")).ToString("F0")
            FracData.DblVFrac("TotalErr_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("TotalErr_StartAddress")).ToString("F0")

            ''Reset Acelerador
            FracData.DblVFrac("EstadoAcelerador_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("EstadoAcelerador_StartAddress")).ToString("F0")

            ''Extra
            FracData.DblVFrac("Refrigerante_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("Refrigerante_StartAddress")).ToString("F0")
            FracData.DblVFrac("ActiveFrac_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("ActiveFrac_StartAddress")).ToString("F0")
            FracData.DblVFrac("Luces_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("Luces_StartAddress")).ToString("F0")

            '' Acumulados
            FracData.DblVFrac("Caudal_" + nFrac.ToString() + "_Acum") = (resultDisplay(FracMoxaReceiveStartAddress("CAUDAL_ACUM_StartAddress")) / 100).ToString("F1")
            FracData.DblVFrac("Combustible_" + nFrac.ToString() + "_Acum") = (resultDisplay(FracMoxaReceiveStartAddress("COMBUSTIBLE_ACUM_StartAddress")) / 10).ToString("F1")

            ''Generalizador de alarma
            FracData.DblVFrac("ActiveAlarm_" + nFrac.ToString()) = Not (FracData.DblVFrac("Viatran_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("Viatran_Scaled_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("TemperaturaLUB_LP_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("TemperaturaLUB_LP_Scaled_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("TemperaturaLUB_HP_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("TemperaturaLUB_HP_Scaled_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("PresionLUB_LP_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("PresionLUB_LP_Scaled_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("PresionLUB_HP_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("PresionLUB_HP_Scaled_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("PresSuc_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("PresSuc_Scaled_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("PresTrans_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("PresTrans_Scaled_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("Caudal_Scaled_Alarm_Active_" + nFrac.ToString()) Or
                                                                    FracData.DblVFrac("Refrigerante_" + nFrac.ToString()))

        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Public Sub GetMaintenance(ByVal resultDisplay As Integer(), ByVal nFrac As Integer)
        Try
            FracData.DblVFrac("MantTargetMuestraAceite_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetMuestraAceite_StartAddress"))
            FracData.DblVFrac("MantTargetRefrigerante_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetRefrigerante_StartAddress"))
            FracData.DblVFrac("MantTargetAceiteMotor_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetAceiteMotor_StartAddress"))
            FracData.DblVFrac("MantTargetAceiteTransmision_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetAceiteTransmision_StartAddress"))
            FracData.DblVFrac("MantTargetFiltroCombustible_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetFiltroCombustible_StartAddress"))
            FracData.DblVFrac("MantTargetLuzValvulas_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetLuzValvulas_StartAddress"))
            FracData.DblVFrac("MantTargetFiltroAire_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetFiltroAire_StartAddress"))
            FracData.DblVFrac("MantTargetLimpiarDucto_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetLimpiarDucto_StartAddress"))
            FracData.DblVFrac("MantTargetCambioCorrea_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetCambioCorrea_StartAddress"))
            FracData.DblVFrac("MantTargetMangueraSilicona_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetMangueraSilicona_StartAddress"))
            FracData.DblVFrac("MantTargetFiltrosBlowBy_" + nFrac.ToString()) = resultDisplay(FracMoxaReceiveStartAddress("MantTargetFiltrosBlowBy_StartAddress"))
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Funcion para hacer IDLE ALL FRACS
    Public Async Sub IdleALL()
        Try
            ''Motor en IDLE
            For Fracs = 1 To ConnectedFracsID.Count
                Dim CurrentFrac As Integer = ConnectedFracsID(Fracs - 1).ToString()
                ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), FracData.SelectedEngines("SelectedEngine_" + CurrentFrac.ToString())("MinRPM"), CurrentFrac)
                Await Task.Delay(50)
                ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), FracGeneralValues("Marcha0"), CurrentFrac)
            Next
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub




    ''Funcion para encontrar fracs duplicados
    Public Function GetDuplicateFracs(ByVal FracID As Integer) As Boolean
        Return ConnectedFracsID.GroupBy(Function(m) m).Where(Function(g) g.Count() > 1).Select(Function(g) g.Key).ToList.Contains(FracID)
    End Function

    ''Function para saber si ya se esta escribiendo un CSV, no repetirlo
    Public Function GetDuplicateCSV(ByVal Frac As Integer) As Boolean
        Return ActiveCSV.GroupBy(Function(m) m).Where(Function(g) g.Count() > 1).Select(Function(g) g.Key).ToList.Contains(Frac)
    End Function

    ''Funcion para generar CSV
    Public Function CSV_OPERATION_INIT(ByRef CSV_OPERATION As CSV, ByVal Frac As Integer) As Boolean
        Try
            CSV_OPERATION = New CSV(ISystem.GetFracName(Frac), CSVOperationPath, GenerateExcel, CSVFlushInterval)
            Dim Fields As ICollection(Of String) = {"Hora", "EngineRPM", "TransRPM", "Presion", "Caudal", "EngCoolantTemp", "EngOilTemp",
                                                    "EngOilPres", "TransOilTemp", "TransOilPres", "LubTemp", "SuccPres", "LubPres", "EngLoad"}
            CSV_OPERATION.WriteFields(Fields)
            CSV_OPERATION.GenerateCaudalGraph = GenerateCaudalGraph
            CSV_OPERATION.GenerateEngTransGraph = GenerateEngTransGraph
            CSV_OPERATION.GeneratePresCaudGraph = GeneratePresCaudGraph
            CSV_OPERATION.GeneratePresionGraph = GeneratePresionGraph
            CSV_OPERATION.GenerateRPMGraph = GenerateRPMGraph
            CSV_OPERATION.GeneratePumpGraph = GeneratePumpGraph

            ActiveCSV.Add(Frac)
            Return True
        Catch ex As Exception
            LogError(ex)
            Return False
        End Try
    End Function

    ''Sub para generar CSV de operacion general
    Public Sub GeneralCSV_OPERATION_INIT(ByRef CSV_OPERATION As CSV)

        CSV_OPERATION = New CSV("General", CSVOperationPath, False, CSVFlushInterval)
        Dim Fields As ICollection(Of String) = {"Hora", "CaudalTotal", "CombTotal", "Presion", "Barriles"}
        CSV_OPERATION.WriteFields(Fields)
        CSV_OPERATION.GenerateCaudalGraph = False
        CSV_OPERATION.GenerateEngTransGraph = False
        CSV_OPERATION.GeneratePresCaudGraph = False
        CSV_OPERATION.GeneratePresionGraph = False
        CSV_OPERATION.GenerateRPMGraph = False
        CSV_OPERATION.GeneratePumpGraph = False

    End Sub

    ''Funcion para escribir el csv
    Public Sub WriteGeneralCSVRecord(ByVal CSV_OPERATION As CSV)
        Dim Record As ICollection(Of Object) = {
            TimeString.ToString(),
            Caudal,
            CombustibleTotal,
            PresionMaxima,
            Totalizador
        }
        CSV_OPERATION.WriteRecord(Record)
    End Sub

    ''Funcion para escribir el csv
    Public Function WriteCSVRecord(ByVal CSV_OPERATION As CSV, ByVal Frac As Integer) As Task
        Dim Record As ICollection(Of String) = {
            TimeString.ToString(),
            FracData.DblVFrac("RPMMotor_" + Frac.ToString()),
            FracData.DblVFrac("RPMSalidaTransmision_" + Frac.ToString()),
            FracData.DblVFrac("Viatran_" + Frac.ToString()),
            FracData.DblVFrac("Caudal_" + Frac.ToString()),
            FracData.DblVFrac("TemperaturaAguaMotor_" + Frac.ToString()),
            FracData.DblVFrac("TemperaturaAceiteMotor_" + Frac.ToString()),
            FracData.DblVFrac("PresionAceiteMotor_" + Frac.ToString()),
            FracData.DblVFrac("TemperaturaAceiteTransmision_" + Frac.ToString()),
            FracData.DblVFrac("PresTrans_" + Frac.ToString()),
            FracData.DblVFrac("TemperaturaLUB_LP_" + Frac.ToString()),
            FracData.DblVFrac("PresSuc_" + Frac.ToString()),
            FracData.DblVFrac("PresionLUB_HP_" + Frac.ToString()),
            FracData.DblVFrac("PorcentajeCarga_" + Frac.ToString())
        }
        CSV_OPERATION.WriteRecord(Record)
        Return Task.CompletedTask
    End Function

    ''Funcion para resetear valores generales
    Public Sub ResetGValues()
        PresionMaxima = 0
        Totalizador = 0
        CombustibleTotal = 0
        For ThisFrac = 0 To ConnectedFracsID.Count - 1
            FracData.DblVFrac("BarrilesTotales_" + ConnectedFracsID(ThisFrac).ToString()) = 0
        Next
    End Sub


End Module
