﻿Public Module Funciones_Fracturador
    ''' <summary>
    ''' Funcion asincronica para detener todos los fracturadores
    ''' </summary>
    Public Async Sub StopAll()
        Try
            ''StopAll
            For Fracs = 1 To ConnectedFracsID.Count
                ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerON"), Fracs)
                Await Task.Delay(50)
                ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerOFF"), Fracs)
            Next
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Funcion para hacer estop a un fracturador solo
    ''' </summary>
    ''' <param name="Frac">ID del fracturador</param>
    Public Async Sub EKill(ByVal Frac As Integer)
        ISystem.MoxaSend(FracMoxaSendStartAddress("E-Kill_StartAddress"), 1, Frac)
        Await Task.Delay(100)
        ISystem.MoxaSend(FracMoxaSendStartAddress("E-Kill_StartAddress"), 0, Frac)
    End Sub

    ''' <summary>
    ''' Funcion para mandar fracturador a parking
    ''' </summary>
    ''' <param name="Frac">ID del fracturador</param>
    Public Sub SetParking(ByVal Frac As Integer)
        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), FracGeneralValues("Parking"), Frac)
    End Sub

    ''' <summary>
    ''' Funcion para subir de marcha
    ''' </summary>
    ''' <param name="Frac">ID del fracturador</param>
    Public Sub GearPlus(ByVal Frac As Integer)
        Dim marcha As Integer

        Select Case FracData.StrVFrac("MarchaDeseada_" + Frac.ToString())
            Case "N"
                marcha = FracGeneralValues("Marcha1")
            Case "P"
                marcha = FracGeneralValues("Marcha0")
            Case Else
                Dim MarchaReal As Integer = GetInteger(FracData.StrVFrac("MarchaActual_" + Frac.ToString()))
                Dim MaxMarcha As Integer = FracData.SelectedTransmissions("SelectedTransmission_" + Frac.ToString()).Item("Gears")
                marcha = FracGeneralValues("Marcha" + Math.Min(Math.Max(MarchaReal + 1, 0), MaxMarcha).ToString())
        End Select
        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, Frac)
    End Sub

    ''' <summary>
    ''' Funcion para bajar de marcha
    ''' </summary>
    ''' <param name="Frac">ID del fracturador</param>
    Public Sub GearMinus(ByVal Frac As Integer)
        Dim marcha As Integer

        Select Case FracData.StrVFrac("MarchaDeseada_" + Frac.ToString())
            Case "N"
                marcha = FracGeneralValues("Parking")
            Case "P"
                Exit Sub
            Case Else
                Dim MarchaReal As Integer = GetInteger(FracData.StrVFrac("MarchaActual_" + Frac.ToString()))
                marcha = FracGeneralValues("Marcha" + Math.Max(MarchaReal - 1, 0).ToString())
        End Select

        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), marcha, Frac)
    End Sub


    ''' <summary>
    ''' Funcion para prender o apagar el motor
    ''' </summary>
    ''' <param name="EngineON">Estado del motor</param>
    ''' <param name="Frac">ID del fracturador</param>
    Public Async Sub EngineStart(ByVal EngineON As Boolean, ByVal Frac As Integer)
        If EngineON = False Then
            ISystem.MoxaSend(FracMoxaSendStartAddress("Arranque_StartAddress"), FracGeneralValues("ArranqueON"), Frac)
            Await Task.Delay(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("Arranque_StartAddress"), FracGeneralValues("ArranqueOFF"), Frac)
        Else
            ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerON"), Frac)
            Await Task.Delay(100)
            ISystem.MoxaSend(FracMoxaSendStartAddress("ApagarMotor_StartAddress"), FracGeneralValues("DetenerOFF"), Frac)
        End If
    End Sub

    ''' <summary>
    ''' Funcion para hacer reset RPM
    ''' </summary>
    ''' <param name="Frac">ID del fracturador</param>
    Public Async Sub ResetRPMDo(ByVal Frac As Integer)
        ISystem.MoxaSend(FracMoxaSendStartAddress("ResetRPM_StartAddress"), 1, Frac)
        Await Task.Delay(100)
        ISystem.MoxaSend(FracMoxaSendStartAddress("ResetRPM_StartAddress"), 0, Frac)
    End Sub

    ''' <summary>
    ''' Funcion para mandar un fracturador a IDLE
    ''' </summary>
    Public Async Sub IDLE(ByVal Frac As Integer)
        ''Motor en IDLE
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), FracData.SelectedEngines("SelectedEngine_" + Frac.ToString()).Item("MinRPM"), Frac)
        Await Task.Delay(100)
        ISystem.MoxaSend(FracMoxaSendStartAddress("GEAR_StartAddress"), FracGeneralValues("Marcha0"), Frac)
    End Sub

    ''' <summary>
    ''' Funcion para cambiar las RPM
    ''' </summary>
    ''' <param name="RPMAmount">Nuevo RPM</param>
    ''' <param name="Frac">ID del fracturador</param>
    Public Sub ChangeRPM(ByVal RPMAmount As Integer, ByVal Frac As Integer)
        Dim EngineMinimumRPM = FracData.SelectedEngines("SelectedEngine_" + Frac.ToString()).Item("MinRPM")
        Dim EngineMaximumRPM = FracData.SelectedEngines("SelectedEngine_" + Frac.ToString()).Item("MaxRPM")
        Dim RPM As Integer = Clamp(EngineMinimumRPM, EngineMaximumRPM, RPMAmount)
        ISystem.MoxaSend(FracMoxaSendStartAddress("RPM_StartAddress"), RPM, Frac)
    End Sub

End Module
