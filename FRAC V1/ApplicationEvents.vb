﻿Imports Microsoft.VisualBasic.ApplicationServices

Namespace My
    ' Los siguientes eventos están disponibles para MyApplication:
    ' Inicio: Se genera cuando se inicia la aplicación, antes de que se cree el formulario de inicio.
    ' Apagado: Se genera después de haberse cerrado todos los formularios de aplicación.  Este evento no se genera si la aplicación termina de forma anómala.
    ' UnhandledException: Se genera si la aplicación encuentra una excepción no controlada.
    ' StartupNextInstance: Se genera cuando se inicia una aplicación de instancia única y dicha aplicación está ya activa. 
    ' NetworkAvailabilityChanged: Se genera cuando se conecta o desconecta la conexión de red.
    Partial Friend Class MyApplication
        Private Sub MyApplication_UnhandledException(sender As Object, e As UnhandledExceptionEventArgs) Handles Me.UnhandledException
            'avisoTitulo = LanguageSelect.DynamicLanguageRM.GetString("avisoTitulo.Error")
            'avisoStr = LanguageSelect.DynamicLanguageRM.GetString("ErrorDesconocido")
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog(Me)
            LogError(e.Exception)
            'e.ExitApplication = False
        End Sub
    End Class
End Namespace
